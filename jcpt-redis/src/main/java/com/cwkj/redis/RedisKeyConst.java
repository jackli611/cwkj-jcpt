package com.cwkj.redis;

/**
 * Redis数据库Key值列表
 * @author ljc
 *
 */
public class RedisKeyConst {

	/** 基础平台用户登录SESSION ID**/
	public static final String JCPT_USER_SID="JCPT_USER_SID:";
	/** 基础平台用户登录缓存 **/
	public static final String JCPT_USER_CACHE="JCPT_USER_CACHE:";
	/** 基础平台系统字典缓存**/
	public static final String JCPT_DICT_SYS="JCPT_DICT_SYS:";
}
