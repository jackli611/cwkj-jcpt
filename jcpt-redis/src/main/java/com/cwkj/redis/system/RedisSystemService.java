package com.cwkj.redis.system;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cwkj.redis.RedisManager;
import com.cwkj.redis.SerializeUtils;

public class RedisSystemService  {

	private static Logger logger = LoggerFactory.getLogger(RedisSystemService.class);
	/**
	 * shiro-redis的session对象前缀
	 */
	private RedisManager redisManager;
	
	/**
	 * The Redis key prefix for the sessions 
	 */
	private String keyPrefix = "JCPT_SYS_CACHE:";
	
	private boolean disableRedisStatus()
	{
		return (this.redisManager!=null && this.redisManager.getEnableStatus()==1)?false:true;
	}
	
	public Object query(String strKey){
		if(strKey == null || disableRedisStatus()){
			logger.error("key is null");
			return null;
		}
		byte[] btyes = this.redisManager.get(this.getByteKey(strKey));
		Object objec = SerializeUtils.deserialize(btyes);
		logger.debug("根据key从Redis中获取对象 key [" + objec + "]");
		return objec;
	}
	
	public void save(String strKey,Object object){
		if(strKey == null || object == null || disableRedisStatus()){
			logger.error("key or object is null");
			return;
		}
		byte[] key = getByteKey(strKey);
		byte[] value = SerializeUtils.serialize(object);
		this.redisManager.set(key, value, 0);
	}

	public void update(String strKey,Object object){
		delete(strKey);
		save(strKey, object);
	}
	
	public void delete(String strKey) {
		if(strKey == null || disableRedisStatus()){
			logger.error("key is null");
			return;
		}
		redisManager.del(this.getByteKey(strKey));
	}
	
	/**
	 * 获得byte[]型的key
	 * @param key
	 * @return
	 */
	private byte[] getByteKey(Serializable key){
		String preKey = this.keyPrefix + key;
		return preKey.getBytes();
	}

	public RedisManager getRedisManager() {
		return redisManager;
	}

	public void setRedisManager(RedisManager redisManager) {
		this.redisManager = redisManager;
		
		/**
		 * 初始化redisManager
		 */
		this.redisManager.init();
	}

	/**
	 * Returns the Redis session keys
	 * prefix.
	 * @return The prefix
	 */
	public String getKeyPrefix() {
		return keyPrefix;
	}

	/**
	 * Sets the Redis sessions key 
	 * prefix.
	 * @param keyPrefix The prefix
	 */
	public void setKeyPrefix(String keyPrefix) {
		this.keyPrefix = keyPrefix;
	}
	
	
}