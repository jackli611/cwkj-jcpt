package com.cwkj.redis;

import java.util.HashMap;
import java.util.Map;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 *  Redis连接池工厂
 * @author ljc
 *
 */
public class RedisPoolFactory {
	
	private static Map<String,JedisPool> jedisPoolMap=null;
	
	/**
	 * 获取连接池
	 * @param host IP
	 * @param port 端口
	 * @param timeout 连接超时时间
	 * @param password 密码
	 * @param jedisPoolConfig 配置
	 * @return
	 */
	public static JedisPool getJedisPool(String host,Integer port,Integer timeout,String password,JedisPoolConfig jedisPoolConfig)
	{
		JedisPool jedisPool =null;
		if(jedisPoolMap==null)
		{
			jedisPoolMap=new HashMap<String, JedisPool>();
			
			return jedisPool;
		}else {
			jedisPool = jedisPoolMap.get(host);
		}
		if( jedisPool == null)
		{
			JedisPoolConfig config=jedisPoolConfig;
			if(config==null)
			{
				config=new JedisPoolConfig();
			}
			if(password != null && !"".equals(password)){
				jedisPool = new JedisPool(config, host, port, timeout, password);
			}else if(timeout != 0){
				jedisPool = new JedisPool(config, host, port,timeout);
			}else{
				jedisPool = new JedisPool(config, host, port);
			}
			jedisPoolMap.put(host, jedisPool);
		}
		return jedisPool;
	}

}