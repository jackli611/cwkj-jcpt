package com.cwkj.redis;

import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis服务管理
 * @author ljc
 * @version 1.0
 */
public class RedisManager {
	
	private String host = "127.0.0.1";
	
	private int port = 6379;
	
	// 0 - never expire
	private int expire = 0;
	
	//timeout for jedis try to connect to redis server, not expire time! In milliseconds
	private int timeout = 0;
	
	/** 开启状态 */
	private int enableStatus=1;
	
	private String password = "";
	private JedisPoolConfig jedisPoolConfig=null;
	
	private  JedisPool jedisPool = null;
	
	public RedisManager(){
		
	}
	
	/**
	 * 初始化方法
	 */
	public void init(){
		if(this.getEnableStatus()==1)
		{
		getJedisPool();
		}
	}
	
	private JedisPool getJedisPool()
	{
		if(this.getEnableStatus()!=1)
		{
			throw new IllegalArgumentException("没有开启Redis服务，请检查开启状态值设置是否正确！");
		}
		if(jedisPool==null)
		{
			jedisPool= RedisPoolFactory.getJedisPool(host, port, timeout, password,jedisPoolConfig);
		}
		return jedisPool;
	}
	
	/**
	 * get value from redis
	 * @param key
	 * @return
	 */
	public byte[] get(byte[] key){
		byte[] value = null;
		Jedis jedis = getJedisPool().getResource();
		try{
			value = jedis.get(key);
		}finally{
			getJedisPool().returnResource(jedis);
		}
		return value;
	}
	
	/**
	 * set 
	 * @param key
	 * @param value
	 * @return
	 */
	public byte[] set(byte[] key,byte[] value){
		Jedis jedis = getJedisPool().getResource();
		try{
			jedis.set(key,value);
			if(this.expire != 0){
				jedis.expire(key, this.expire);
		 	}
		}finally{
			 getJedisPool().returnResource(jedis);
		}
		return value;
	}
	
	/**
	 * set 
	 * @param key
	 * @param value
	 * @param expire
	 * @return
	 */
	public byte[] set(byte[] key,byte[] value,int expire){
		Jedis jedis =  getJedisPool().getResource();
		try{
			jedis.set(key,value);
			if(expire != 0){
				jedis.expire(key, expire);
		 	}
		}finally{
			 getJedisPool().returnResource(jedis);
		}
		return value;
	}
	
	/**
	 * del
	 * @param key
	 */
	public void del(byte[] key){
		Jedis jedis =  getJedisPool().getResource();
		try{
			jedis.del(key);
		}finally{
			 getJedisPool().returnResource(jedis);
		}
	}
	
	/**
	 * flush
	 */
	public void flushDB(){
		Jedis jedis =  getJedisPool().getResource();
		try{
			jedis.flushDB();
		}finally{
			 getJedisPool().returnResource(jedis);
		}
	}
	
	/**
	 * size
	 */
	public Long dbSize(){
		Long dbSize = 0L;
		Jedis jedis =  getJedisPool().getResource();
		try{
			dbSize = jedis.dbSize();
		}finally{
			 getJedisPool().returnResource(jedis);
		}
		return dbSize;
	}

	/**
	 * keys
	 * @param regex
	 * @return
	 */
	public Set<byte[]> keys(String pattern){
		Set<byte[]> keys = null;
		Jedis jedis =  getJedisPool().getResource();
		try{
			keys = jedis.keys(pattern.getBytes());
		}finally{
			 getJedisPool().returnResource(jedis);
		}
		return keys;
	}
	
	/**
	 * redis服务IP
	 * @return
	 */
	public String getHost() {
		return host;
	}

	/**
	 * redis服务IP
	 * @param host
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * 端口
	 * @return
	 */
	public int getPort() {
		return port;
	}

	/**
	 * 端口
	 * @param port
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * 默认有效缓存时间（单位秒）
	 * @return
	 */
	public int getExpire() {
		return expire;
	}

	/**
	 * 默认有效缓存时间（单位秒）
	 * @param expire
	 */
	public void setExpire(int expire) {
		this.expire = expire;
	}

	/**
	 * 连接超时时间
	 * @return
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * 连接超时时间
	 * @param timeout
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 * 密码
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 密码
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 *  jedisPool配置
	 * @return   jedisPool配置
	 */
	public JedisPoolConfig getJedisPoolConfig() {
		return jedisPoolConfig;
	}

	/**
	 *  jedisPool配置
	 * @param jedisPoolConfig  jedisPool配置
	 */
	public void setJedisPoolConfig(JedisPoolConfig jedisPoolConfig) {
		this.jedisPoolConfig = jedisPoolConfig;
	}

	/**
	 * 开启状态
	 * @return 
	 */
	public int getEnableStatus() {
		return enableStatus;
	}
	

	/**
	 * 开启状态
	 * @param enableStatus 
	 */
	public void setEnableStatus(int enableStatus) {
		this.enableStatus = enableStatus;
	}
}