
package com.cwkj.scf.model.sms;

/** 
 *  
 * @author: harry
 */
public enum SMSBusinessType {

    // 业务类型： 修改手机号，注册验证 ：Check, 还款 ，放款 ， 投资 ： notify, 绑卡：bindCard

    check, notify, register, recharge, login,resetPsw,bindCard

}
