package com.cwkj.scf.model.sms;

public enum MessageTemplate {
	/**
	 * SMS_159621481 短信验证码 ; 
	 * SMS_159621486 收款模板
	 * SMS_161685134 找回密码
	 */
	SMS_159621481,SMS_159621486,SMS_161685134;

}
