
package com.cwkj.scf.model.sms;

/** 
 *  消息类型
 * @author: harry
 */
public enum MessageFormatType {
	
     SIMPLE_ONLY_CODE, COMPOSITE
}
