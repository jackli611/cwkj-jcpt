package com.cwkj.scf.service.sms.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.dao.sms.TMessageMapper;
import com.cwkj.scf.model.sms.MessageFormatType;
import com.cwkj.scf.model.sms.TMessage;
import com.cwkj.scf.model.sms.TMessageExample;
import com.cwkj.scf.service.sms.ISmsService;
import com.cwkj.sms.util.IdentifyCodeGenerator;

/**
 * 
 * @author harry
 * 
 */
@Service("smsServiceImpl")
public class SMSServiceImpl implements ISmsService {
	
	private Logger logger = Logger.getLogger(SMSServiceImpl.class);

	@Autowired
	private TMessageMapper messageDao;
	
	private boolean checkCanSend(TMessage message)  {
		try {
			TMessageExample example = new TMessageExample();
			example.createCriteria().andRecieversEqualTo(message.getRecievers())
									.andCreateTimeGreaterThanOrEqualTo(DateUtils.getCurrentDate());
			List<TMessage>  msgLst = messageDao.selectByExample(example );
			if(null != msgLst && msgLst.size()>=SmsConfig.getDayMaxSendCnt()) {
				return false;
			}
			return true;
		}catch(Exception e) {
			logger.error(e);
		}
		return false;
	}


	@Override
	public BaseResult<String> send(TMessage message) {
		
		if(!message.valid()) {
			throw new BusinessException("无效的消息格式");
		}
		/**
		 * 	根据配置是否发送,如果不发送直接返回
		 */
		if(!SmsConfig.getIsSend()) {
			return new BaseResult(BaseResult.CODE_SUCCESS,"发送成功");
		}
		if(!checkCanSend(message)) {
			return new BaseResult(BaseResult.CODE_FAILE,"超出发送次数");
		}
		
		if(MessageFormatType.SIMPLE_ONLY_CODE.name().equals(message.getMessageFormatType())) {
			//查找是否存在上次发送了还没有使用的验证码，防止用户重复点击延迟到达，验证混乱
			TMessageExample example = new TMessageExample();
			example.createCriteria().andRecieversEqualTo(message.getRecievers())
									.andBusinessTypeEqualTo(message.getBusinessType())
									.andIsValidateEqualTo("T");
			List<TMessage>  msgLst = messageDao.selectByExample(example );
			if(null == msgLst || msgLst.size()<1) {
				message.setMessage(IdentifyCodeGenerator.generateIdentifyCode(6));
			}else {
				message.setMessage(msgLst.get(0).getMessage());
			}
		}
		message.setIsValidate("T");
		messageDao.insertSelective(message);
		
		ISMSSender smsSender = SMSSenderFactory.getSMSSender(message.getSmsType());
        BaseResult<String> result = smsSender.send(message);
        
        if (result.resultSuccess() && StringUtil.isNotBlank(result.getData())) {
            message.setSendId(result.getData());
            messageDao.updateByPrimaryKeySelective(message);
        }
        return result;
	}

	@Override
	public boolean checkSms(String phone, String businessType, String smsCode) {
		/**
		 * 	如果配置不发送直接通过
		 */
		if(!SmsConfig.getIsSend()) {
			return true;
		}
		
		if(StringUtil.isBlank(smsCode)) { return false;	}
		if(StringUtil.isBlank(phone)) { return false;	}
		if(StringUtil.isBlank(businessType)) { return false;	}
		
		TMessageExample example = new TMessageExample();
		example.createCriteria().andRecieversEqualTo(phone)
								.andBusinessTypeEqualTo(businessType)
								.andIsValidateEqualTo("T");
		List<TMessage>  msgLst = messageDao.selectByExample(example );
		if(null == msgLst || msgLst.size()<1) {return false;}
		TMessage mg=msgLst.get(0);
		mg.setIsValidate("F");
		messageDao.updateByPrimaryKeySelective(mg);
		return msgLst.get(0).getMessage().equals(smsCode);
	}

}
