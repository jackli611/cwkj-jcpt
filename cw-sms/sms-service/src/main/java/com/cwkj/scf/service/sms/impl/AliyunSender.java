package com.cwkj.scf.service.sms.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.JsonUtils;
import com.cwkj.scf.model.sms.MessageFormatType;
import com.cwkj.scf.model.sms.MessageTemplate;
import com.cwkj.scf.model.sms.SMSBusinessType;
import com.cwkj.scf.model.sms.TMessage;

/**
 * Created on 17/6/7.
 * 短信API产品的DEMO程序,工程中包含了一个SmsDemo类，直接通过
 * 执行main函数即可体验短信产品API功能(只需要将AK替换成开通了云通信-短信产品功能的AK即可)
 * 工程依赖了2个jar包(存放在工程的libs目录下)
 * 1:aliyun-java-sdk-core.jar
 * 2:aliyun-java-sdk-dysmsapi.jar
 *
 */
class AliyunSender implements ISMSSender{

    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
    static final String accessKeyId = SmsConfig.getSmsAccount();
    static final String accessKeySecret = SmsConfig.getSmsKey();
   
    static final String signName = "小物橙";

    public static SendSmsResponse sendSms(Long requestId,String mobile,String message,String smsTemplateName) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(mobile);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(signName);
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(smsTemplateName);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(message);

        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");

        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId(String.valueOf(requestId==null?1L:requestId));

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);

        return sendSmsResponse;
    }


    public static QuerySendDetailsResponse querySendDetails(String bizId) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象
        QuerySendDetailsRequest request = new QuerySendDetailsRequest();
        //必填-号码
        request.setPhoneNumber("15000000000");
        //可选-流水号
        request.setBizId(bizId);
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.setSendDate(ft.format(new Date()));
        //必填-页大小
        request.setPageSize(10L);
        //必填-当前页码从1开始计数
        request.setCurrentPage(1L);

        //hint 此处可能会抛出异常，注意catch
        QuerySendDetailsResponse querySendDetailsResponse = acsClient.getAcsResponse(request);

        return querySendDetailsResponse;
    }

    public static void main(String[] args) throws ClientException, InterruptedException {

        //发短信
    	TMessage message = new TMessage(); 
    	message.setMessageFormatType(MessageFormatType.SIMPLE_ONLY_CODE.name());
		message.setMessageTemplate(MessageTemplate.SMS_159621481.name());
		message.setBusinessType(SMSBusinessType.register.name());
		message.setMessage("123458");
		message.setRecievers("13058177027");
		
    	AliyunSender  s = new AliyunSender();
    	BaseResult response = s.send(message);
        System.out.println("短信接口返回的数据----------------:"+response.getMsg()+"code:"+response.getCode());
//        System.out.println("Code=" + response.getCode());
//        System.out.println("Message=" + response.getMessage());
//        System.out.println("RequestId=" + response.getRequestId());
//        System.out.println("BizId=" + response.getBizId());

        Thread.sleep(3000L);

    }


	@Override
	public BaseResult send( TMessage message)  {
		try {
			String messageContent = message.getMessage();
			if(MessageFormatType.SIMPLE_ONLY_CODE.name().equals(message.getMessageFormatType())) {
				Map<String,String> smsMsgMap = new HashMap<String,String>();
				smsMsgMap.put("code", messageContent);
				messageContent = JsonUtils.toJSONString(smsMsgMap);
			}
			
			SendSmsResponse response = sendSms(message.getMessageId(),
											   message.getRecievers(),
											   messageContent,
											   message.getMessageTemplate() );
			if (!response.getCode().equals("OK")) {
				return new BaseResult(BaseResult.CODE_FAILE,response.getMessage());
			}
			return new BaseResult(BaseResult.CODE_SUCCESS,"发送成功");
		} catch (ClientException e) {
			e.printStackTrace();
			throw new BusinessException(e);
		}
	}
}
