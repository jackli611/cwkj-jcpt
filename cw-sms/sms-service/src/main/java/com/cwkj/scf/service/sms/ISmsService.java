package com.cwkj.scf.service.sms;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.model.sms.TMessage;

public interface ISmsService {

	/**
	 * 	发送短信消息
	 * @param message
	 */
	public BaseResult<String> send(TMessage message);

	/**
	 * 	验证短信验证码
	 * @param phone 接受的手机号
	 * @param businessType 业务场景
	 * @param smsCode 用户输入收到验证码
	 * @return
	 */
	public boolean checkSms(String phone, String businessType, String smsCode);

}
