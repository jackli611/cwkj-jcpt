package com.cwkj.scf.service.sms.impl;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.cwkj.jcpt.util.JsonUtils;
import com.cwkj.scf.model.sms.TMessage;

import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 短信发送 ， 包可见，不让外部直接调用
 * 
 * @author harry
 * 
 */
@Component("smsMessageBuilder")
class SMSMessageBuilder {
	
	private Logger logger = Logger.getLogger(SMSMessageBuilder.class);

    private final static String            DEFAULT_TEMPLATE = "sms_template_mobile_check.ftl";

    @Autowired
    private FreeMarkerConfigurationFactory freeMarkerConfigurer;


    public String buildMessage(TMessage message) {


        String messageTemplate = message.getMessageTemplate();
        String messageContent = null;
        if (null == messageTemplate || "".equals(messageTemplate)) {
            messageTemplate = DEFAULT_TEMPLATE;
        }

        try {
            Template tpl = freeMarkerConfigurer.createConfiguration().getTemplate(messageTemplate);
            Map<String,String> messageMap = JsonUtils.jsonToMap(message.getMessage());
            messageContent = FreeMarkerTemplateUtils.processTemplateIntoString(tpl, messageMap);
            logger.info("messageText:"+messageContent);
        } catch (IOException e) {
            logger.error(e);
            return null;
        } catch (TemplateException e) {
            logger.error(e);
            return null;
        }catch (Throwable t) {
            logger.error(t);
            return null;
        }
        if (null == messageContent) {
        	messageContent = message.getMessage();
        }
        return messageContent;
    }

}
