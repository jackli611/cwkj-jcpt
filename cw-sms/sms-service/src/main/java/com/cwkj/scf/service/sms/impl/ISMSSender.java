package com.cwkj.scf.service.sms.impl;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.model.sms.TMessage;

interface ISMSSender {

    /**
     * @param requestId, 请id，可以是messageid
     * @param phone
             *            接收的电话
     * @param content
             *            发送的短信内容
     * @param msgid
              *            发送成功返回发送的消息ID
     * @return 返回 -1 代表调用失败， 否则代表 发送成功后的消息ID
     */
	public BaseResult send( TMessage message);

}
