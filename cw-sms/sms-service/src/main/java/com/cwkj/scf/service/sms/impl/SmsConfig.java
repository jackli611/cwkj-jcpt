package com.cwkj.scf.service.sms.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 	短信配置
 * @author harry
 *
 */
@Component("smsConfig")
public class SmsConfig {
	
	/**
	 * 	最大发送次数单日
	 */
	private static int  dayMaxSendCnt;
	/**
	 * 	是否发送
	 */
	private static boolean  isSend;
	
	/**
	 * 	短信供应商分配给的密码
	 */
	private static String smsKey;
	
	/**
	 * 	短信供应商分配账号
	 */
	private static String smsAccount;

	public static int getDayMaxSendCnt() {
		return dayMaxSendCnt;
	}

	@Value("${sms.dayMaxSendCnt}")
	public  void setDayMaxSendCnt(int dayMaxSendCnt) {
		SmsConfig.dayMaxSendCnt = dayMaxSendCnt;
	}

	
	public static boolean getIsSend() {
		return isSend;
	}
	
	@Value("${sms.isSend}")
	public  void setIsSend(boolean isSend) {
		SmsConfig.isSend = isSend;
	}

	public static String getSmsKey() {
		return smsKey;
	}

	@Value("${sms.smsKey}")
	public  void setSmsKey(String smsKey) {
		SmsConfig.smsKey = smsKey;
	}

	public static String getSmsAccount() {
		return smsAccount;
	}

	@Value("${sms.smsAccount}")
	public void setSmsAccount(String smsAccount) {
		SmsConfig.smsAccount = smsAccount;
	}

	
	
}
