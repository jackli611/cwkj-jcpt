package com.cwkj.scf.service.yinsheng;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.YinshengContants;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.model.transinfo.TransInfoDo;
import com.cwkj.scf.service.transinfo.HuiShangTransInfoService;

@Service("yinshengFacadeService")
public class YinshengFacadeServiceImpl implements IYinshengFacadeService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	   
    @Autowired
    private HuiShangTransInfoService transinfoService;
    
    @Autowired
    private IYinShengService  yinshengService;
	   
	
	
	/**
	 * 	快捷消费申请
	 * @param bo
	 * @return
	 */
	@Override
	public BaseResult<Map> deduct(DeductForm bo) {
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		paramMap.put("tranCode", TranCodeEnum.DEDUCT_MONEY.getCode());
		paramMap.put("orderId", bo.getOrderNo());
    	TransInfoDo tfDo = this.transinfoService.queryTransInfo(paramMap);
    	if(tfDo != null) {
    		if("SUCCESS".equals(tfDo.getResponseResult())){
    			//return Result;
    		}else if("FAIL".equals(tfDo.getResponseResult())){
    			//return Result;
    		}else {
    			
    		}
    	}
		
		logger.info("["+YinshengContants.YINSHENG_NAME+"]-"+TranCodeEnum.DEDUCT_MONEY.getDesc() +"-"+YinshengContants.YINSHENG_IN_PARAM+":"+ JSON.toJSONString(bo));
		BaseResult<Map> bsm = yinshengService.deduct(bo,TranCodeEnum.DEDUCT_MONEY);
		logger.info("["+YinshengContants.YINSHENG_NAME+"]-"+TranCodeEnum.DEDUCT_MONEY.getDesc() +"-"+YinshengContants.YINSHENG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}
	

}
