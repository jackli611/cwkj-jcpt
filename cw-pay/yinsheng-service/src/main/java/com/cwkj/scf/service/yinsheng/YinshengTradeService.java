package com.cwkj.scf.service.yinsheng;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.model.payform.BaseForm;

/**
 * ClassName: HuishangTradeService <br>
 * Description: 调用徽商公共服务
 * @author harry
 * @version
 */
public interface YinshengTradeService {

	
	/**
	 * 调用徽商接口
	 * @param  bizOrigJson 业务原数据json
	 * @param jsonReqData  发送给徽商的 业务数据
	 * @param reqHeader 交易请求的头
	 * @param tranCode  交易代码
	 * @param reqNo 请求流水号
	 * @return
	 */
	public BaseResult call(BaseForm bizOrigForm ,
							   TranCodeEnum tranCode);

	
}

	