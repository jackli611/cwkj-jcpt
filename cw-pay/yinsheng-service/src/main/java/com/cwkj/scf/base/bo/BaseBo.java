package com.cwkj.scf.base.bo;

import org.springframework.util.Assert;

public class BaseBo implements java.io.Serializable {
	
	/**
	 * method String(128) 接口名称。不可空 ysepay.online.fastpay 
	   partner_id String(20)  商户在银盛支付平台开设的用户号	[商户号]  不可空
	   timestamp String(19) 发送请求的时间，格式"yyyy-MM-dd HH:mm:ss" 不可空2014-07-24 03:07:50 timestamp
	   charset String(10) 商户网站使用的编码格式，如utf-8、	gbk、gb2312 等。	不可空GBK 新增
	   sign_type String(10) RSA 不可空 RSA 
	   sign String(256) RSA 签名字符串，再用Base64 编码 不可空 
	   notify_url String(500)    银盛支付服务器主动通知商户网站里 指定的页面http 路径，支持多个url 进行异步通知，多个url 用分隔符“,”	分开，格式如：url1,url2,url3 不可空http://api.test.ysepay.net/atinterface/receive_return.htm
	   version String(3) 接口版本不可空3.0 Version
	   tran_type String(1) 交易类型，说明：1 或者空：即时到账，	2：担保交易 	可空 2  
	 */
	
	
	private String method="";
	private String partner_id="";
	private String timestamp;
	private String charset = "utf-8";
	private String sign_type = "RSA";
	private String sign;
	private String notify_url;
	private String version = "3.0";
	private String tran_type = "1";
	private String biz_content;
	
	
	
	
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(String partner_id) {
		this.partner_id = partner_id;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTran_type() {
		return tran_type;
	}

	public void setTran_type(String tran_type) {
		this.tran_type = tran_type;
	}


	public String getBiz_content() {
		return biz_content;
	}

	public void setBiz_content(String biz_content) {
		this.biz_content = biz_content;
	}

	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		Assert.hasText(this.method, "接口名称不能为空");
		Assert.hasText(this.partner_id, "商户号不能为空");
		Assert.hasText(this.timestamp, "发送请求的时间不能为空");
		Assert.hasText(this.charset, "编码格式不能为空");
		Assert.hasText(this.sign_type, "sign type不能为空");
		Assert.hasText(this.sign, "签名字符串不能为空");
		Assert.hasText(this.notify_url, "通知商户网站不能为空");
		Assert.hasText(this.version, "接口版本不能为空");
	}



	

}
