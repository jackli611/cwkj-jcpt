package com.cwkj.scf.common.enums;

/**
 * ClassName: TranCodeEnum <br>
 * Description: 实际报文编号
 * @author harry
 * @version
 */
public enum TranCodeEnum {

	/**
	 * 实名认证
	 */
	USER_REAL_NAME_AUTH_3001("3001","实名认证【3001】"),
	SIGN_QUERY("3490","签约查询【3490】"),
	SIGN_DONE("3491","签约【3491】"),
	SIGN_CONFIRM("3492","签约确认【3492】"),
	DEDUCT_MONEY("3612","代扣记账【3612】"),
	TRADE_STATUS_QUERY("3405","交易状态查询【3405】"),
	WITHDRAW("2501","资金提现【2501】"),
	REMAIN_AMOUNT("5863","余额查询【5863】"),
	QUERY_TRADE_DETAIL("5824","交易明细查询【5824】"),
	WITHDRAW_NOTIFY("3446","提现冲正推送通知【3446】");
	
	
	private String code;
	private String desc;
	
	private TranCodeEnum(String value, String desc) {
		this.code = value;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
}

	