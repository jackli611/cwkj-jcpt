package com.cwkj.scf.service.yinsheng;

import java.util.Map;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.model.payform.DeductForm;

public interface IYinShengService {

	/**
	 * 申请扣款
	 * @param bo
	 * @param deductMoney
	 * @return
	 */
	BaseResult<Map> deduct(DeductForm bo, TranCodeEnum deductMoney);	

}
