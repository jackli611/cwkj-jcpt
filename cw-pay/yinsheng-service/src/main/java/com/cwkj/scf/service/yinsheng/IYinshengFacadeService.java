package com.cwkj.scf.service.yinsheng;

import java.util.Map;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.model.payform.DeductForm;

public interface IYinshengFacadeService {
	
	/**
	 * 	快捷消费申请
	 * @param bo
	 * @return
	 */
	BaseResult<Map> deduct(DeductForm bo);

}
