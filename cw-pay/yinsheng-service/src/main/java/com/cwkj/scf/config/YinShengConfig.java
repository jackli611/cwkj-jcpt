package com.cwkj.scf.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("huiShangConfig")
public class YinShengConfig {
	
	private static String uri ;

	private static String signPrivatePath ;

	private static String signPublicPath ;

	private static String decryptPath ;

	private static String encryptPath ;
	
	private static String acctNo ;
	//充值回调地址
	private static String deductCallBackUrl;

	public static String charset = "utf-8";

	public static String format;

	public static String signType;

	public static String partnerId;
	
	
	public static String getUri() {
		return uri;
	}

	public static String getSignPrivatePath() {
		return signPrivatePath;
	}

	public static String getSignPublicPath() {
		return signPublicPath;
	}

	public static String getDecryptPath() {
		return decryptPath;
	}

	public static String getEncryptPath() {
		return encryptPath;
	}

	public static String getAcctNo() {
		return acctNo;
	}
	
	
	@Value("${huishang.uri}")
	public  void setUri(String uri) {
		YinShengConfig.uri = uri;
	}

	@Value("${huishang.signPrivatePath}")
	public  void setSignPrivatePath(String signPrivatePath) {
		YinShengConfig.signPrivatePath = signPrivatePath;
	}

	@Value("${huishang.signPublicPath}")
	public  void setSignPublicPath(String signPublicPath) {
		YinShengConfig.signPublicPath = signPublicPath;
	}

	@Value("${huishang.decryptPath}")
	public  void setDecryptPath(String decryptPath) {
		YinShengConfig.decryptPath = decryptPath;
	}
	@Value("${huishang.encryptPath}")
	public  void setEncryptPath(String encryptPath) {
		YinShengConfig.encryptPath = encryptPath;
	}

	@Value("${huishang.acctNo}")
	public  void setAcctNo(String acctNo) {
		YinShengConfig.acctNo = acctNo;
	}


	public static String getDeductCallBackUrl() {
		return deductCallBackUrl;
	}
	
	@Value("${huishang.deductCallBackUrl}")
	public void setDeductCallBackUrl(String deductCallBackUrl) {
		YinShengConfig.deductCallBackUrl = deductCallBackUrl;
	}
	
}
