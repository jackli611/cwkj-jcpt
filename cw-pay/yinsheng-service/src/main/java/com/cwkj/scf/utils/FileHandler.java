package com.cwkj.scf.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FileHandler {

	/**
	 * 加密,读取旧的文件的内容加密后写入新的文件
	 * 
	 * @param file1
	 * @param file2
	 * @param keyPath
	 *            密钥路径
	 * @return 返回文件内容
	 */
	public static String encryptHand(File file1, File file2, String keyPath) {
		String result = "";
		String line = "";
		byte[] bt;
		String encryptResultStr = "";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file1), "GBK"));
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file2), "GBK"));
			if ((line = br.readLine()) != null) {
				bt = RSAUtils.encryptByAES(result + line, keyPath);// 逐行加密
				encryptResultStr = RSAUtils.parseByte2HexStr(bt);
				bw.write(encryptResultStr);
			}
			while ((line = br.readLine()) != null) {
				bt = RSAUtils.encryptByAES(result + line, keyPath);// 逐行加密
				encryptResultStr = RSAUtils.parseByte2HexStr(bt);
				bw.newLine();
				bw.write(encryptResultStr);
			}
			bw.newLine();
			bw.flush();
			bw.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void decryptHand(File file1, File file2, String fileKey) {
		String line = "";
		byte[] decryptFrom = null;
		byte[] decryptResult = null;

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file1), "GBK"));
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file2), "GBK"));
			if ((line = br.readLine()) != null) {
				// 解密
				String result = "";
				decryptFrom = RSAUtils.parseHexStr2Byte(result + line);
				decryptResult = RSAUtils.decryptByAES(decryptFrom, fileKey);// 逐行解密
				result = new String(decryptResult, "GBK");
				bw.write(result);
			}
			while ((line = br.readLine()) != null) {
				// 解密
				String result = "";
				decryptFrom = RSAUtils.parseHexStr2Byte(result + line);
				decryptResult = RSAUtils.decryptByAES(decryptFrom, fileKey);// 逐行解密
				result = new String(decryptResult, "GBK");
				bw.newLine();
				bw.write(result);
			}
			bw.flush();
			bw.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
     * 生成摘要文件
     *
     * @param file
     * @param tmp
     * @throws IOException
     */
    public static void transFormToTmpFile(File file, File tmp) {
    	BufferedReader br = null;
    	BufferedWriter bw = null;
        try {
        	br = new BufferedReader(new InputStreamReader(new FileInputStream(file.getPath()), "GBK"));
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmp.getPath()), "GBK"));
            String line = "";
            while ((line = br.readLine()) != null) {
                if (!("").equals(line)) {
                    bw.write(line);
                    bw.flush();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
    }
}
