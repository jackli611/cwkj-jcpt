package com.cwkj.scf.service.yinsheng;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.scf.base.bo.BaseBo;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.config.YinShengConfig;
import com.cwkj.scf.model.payform.BaseForm;
import com.cwkj.scf.service.respcode.IThirdRespCodeService;
import com.cwkj.scf.service.transinfo.HuiShangTransInfoService;

/**
 * Description: 调用徽商公共服务
 * @author harry
 */
@Service
public class YinshengTradeServiceImpl implements YinshengTradeService{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Autowired
    private HuiShangTransInfoService transInfoService;
    @Autowired
    private IThirdRespCodeService respCodeService;
    
 

    public BaseResult<Map> call1(BaseBo requestBase ,
    						   TranCodeEnum tranCode) {
    	
    	BaseResult ret = new BaseResult();
    	String response = null;
    	try {
    		
    		
    		requestBase.setTimestamp(DateUtils.format(new Date(), "yyyy-MM-dd HH:MI:SS"));
    		
    		/**
    		 *  1. url编码
    		 *  2. sign ，  sign 过程 排序，拼接字段， rsa sign
    		 *  3. post
    		 */
    		
    		Map<String, String> param = new TreeMap<String,String>();
    		//url编码
    		param.put("biz_content", URLEncoder.encode(requestBase.getBiz_content(), YinShengConfig.charset));
    		param.put("charset", URLEncoder.encode(requestBase.getCharset(), YinShengConfig.charset));
    		param.put("notify_url", URLEncoder.encode(requestBase.getNotify_url(), YinShengConfig.charset));
    		param.put("partner_id", URLEncoder.encode(requestBase.getPartner_id(), YinShengConfig.charset));
    		param.put("method", URLEncoder.encode(requestBase.getMethod(), YinShengConfig.charset));
    		param.put("sign_type", URLEncoder.encode(requestBase.getSign_type(), YinShengConfig.charset));
    		param.put("timestamp", URLEncoder.encode(requestBase.getTimestamp(), YinShengConfig.charset));
    		param.put("version", URLEncoder.encode(requestBase.getVersion(), YinShengConfig.charset));
    		String needSignParam = YinShengSignUtil.buildSignString(param);
    		String sign = YinShengSignUtil.sign(needSignParam);
    		param.put("sign", sign);
    		
    		
    		
    		//post
    		//String retVal = HttpUtil.httpPost(YinShengConfig.getUri(), param);
    		//logger.info("银盛  return msg:"+retVal);
    		
			// 签名
//			String signVal = YinShengSignUtil.sign(jsonReqData);
//			logger.debug("签名后台的数据：reqNo:{},请求数据：{}",reqNo,jsonReqData);
			/*
			// 组装请求数据
			String sendJson = HuiShangUtil.buildSendJson(bankCode,coinstCode, data);
			//交易报文记录
			transInfoService.saveTransInfo(bizOrigForm.getUserId(), 
										   bizOrigForm.getOrderNo(),
										   bizOrigForm.getTRXDATE(),
										   bizOrigForm.getTRXTIME(),
										   reqNo,
										   tranCode.getCode(),
										   bizOrigForm.getAMOUNT(),
										   JSON.toJSONString(bizOrigForm),
										   sendJson);
			logger.debug("发送请求至：{},reqdata:{}" , HuiShangConfig.getUri(),sendJson);
			// 发送请求
			response = HttpClientUtil.requestBodyPost(HuiShangConfig.getUri(),  sendJson);
			logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+tranCode.getDesc() +"结果>>>>"+response);
			
			Map jsonMap = JSONObject.parseObject(response,Map.class);
			// 解密
			Map responseMap = HuiShangUtil.decryptResponse(jsonMap);
			// 验签
			boolean verifyResult = HuiShangUtil.checkResponseSign(responseMap);
			
			if(verifyResult==false) {
				throw new RuntimeException("调用返回没有验证通过");
			}
			//返回调用结果
			ret = HuiShangUtil.buildResult(responseMap);
			if(StringUtil.isBlank(ret.getMsg())) {
				String msg = respCodeService.selectMsgByCode(ret.getCode());
				ret.setMsg(msg);
			}
			//交易返回报文记录
			transInfoService.updateTransInfoResponse(reqNo, 
													 JSON.toJSONString(responseMap),
													 ret.getCode(),
													 ret.getMsg(),
													 new Date());
			*/
//		} catch (UnsupportedEncodingException e) {
//			logger.error(reqNo,e);
//			transInfoService.updateTransInfoError(reqNo,e.getMessage());
		} catch (Exception e) {
//			logger.error(reqNo,e);
//			transInfoService.updateTransInfoError(reqNo,e.getMessage());
		}finally {
			return ret;
		}
	}



	@Override
	public BaseResult call(BaseForm bizOrigForm, TranCodeEnum tranCode) {
		// TODO Auto-generated method stub
		return null;
	}

}

	