package com.cwkj.scf.service.yinsheng;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Map;

import com.cwkj.scf.base.bo.BaseBo;

import sun.misc.BASE64Decoder;

public class YinShengSignUtil {
	
	/**
	 * 实例化私钥
	 *
	 * @s 私钥
	 * @return
	 */
	public static PrivateKey getPrivateKey(String s) throws Exception {
		PrivateKey privateKey = null;
		PKCS8EncodedKeySpec priPKCS8;
		priPKCS8 = new PKCS8EncodedKeySpec(new BASE64Decoder().decodeBuffer(s));
		KeyFactory keyf = KeyFactory.getInstance("RSA");
		privateKey = keyf.generatePrivate(priPKCS8);
		return privateKey;
	}

	public static String rsaSign(PrivateKey privateKey, String content,    String charset) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnsupportedEncodingException{
		Signature signet =    java.security.Signature.getInstance("SHA1WithRSA");
		signet.initSign(privateKey);
		signet.update(content.getBytes(charset));
		byte[] signed = signet.sign();			
		return new String(Base64.getEncoder().encode(signed),charset);
	}

	public static String sign(String needSignParam) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String buildSignString(Map<String, String> param) {
		// TODO Auto-generated method stub
		return null;
	}

}
