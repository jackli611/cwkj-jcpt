package com.cwkj.scf.service.transinfo;

import java.util.Date;
import java.util.Map;

import com.cwkj.scf.model.transinfo.TransInfoDo;

/**
 * @author harry
 */
public interface HuiShangTransInfoService {

	public int saveTransInfo(TransInfoDo transInfoDo);
	
	
	public int updateTransInfo(TransInfoDo transInfoDo);
	
	public int updateTransInfoBythirdLogNo(TransInfoDo transInfoDo);
	
	public TransInfoDo queryTransInfo(Map<String,Object> paramMap);


	public int updateTransInfoError(String reqNo, String message);

	public int updateTransInfoResponse(String reqNo, 
									   String response,
									   String reponseCode,
									   String responseMsg,
									   Date responseTime);

	public int saveTransInfo(String userId, 
							  String orderNo, 
							  String trxdate, 
							  String trxtime, 
							  String reqNo, 
							  String code,
							  String amt,
							  String jsonString, 
							  String sendJson);
}

	