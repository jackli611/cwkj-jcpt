package com.cwkj.scf.service.transinfo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cwkj.scf.dao.transinfo.ITransInfoDao;
import com.cwkj.scf.model.transinfo.TransInfoDo;

/**
 * @author harry
 */
@Service("transInfoService")
public class HuiShangTransInfoServiceImpl implements HuiShangTransInfoService{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ITransInfoDao transInfoDao;
	private final static String BASETABLENAME = "trans_info";//分表名字
	private final static int TABLENUMBER = 100;//分表个数
	private final static String UNDERLINE = "_";
	
	@Override
	public int saveTransInfo(TransInfoDo transInfoDo) {
		try {
			//根据thirdLogNo查找分表
			String tableName = getTableName(transInfoDo.getThirdLogNo());
			String tableNumber = tableName.split(UNDERLINE)[tableName.split(UNDERLINE).length-1];
			transInfoDo.setTableNumber(tableNumber);
			//保存记录
			return transInfoDao.insertTransInfo(tableName,transInfoDo);
		} catch (Exception e) {
			String errMsg = "保存数据异常-"+JSON.toJSONString(transInfoDo);
			logger.error(errMsg,e);
		}
		return 0;
	}

	@Override
	public int saveTransInfo(String userId, 
							  String orderNo, 
							  String trxdate, 
							  String trxtime, 
							  String reqNo, 
							  String code,
							  String amt,
							  String bizOrigJson, 
							  String sendJson) {
		
		TransInfoDo transInfoDo = new TransInfoDo();
		transInfoDo.setThirdLogNo(reqNo);
		transInfoDo.setOrderId(orderNo);
		transInfoDo.setThirdCustId(userId);
		transInfoDo.setTranCode(code.trim());
		transInfoDo.setRequestTime(trxtime);
		transInfoDo.setRequestDate(trxdate);
		transInfoDo.setRequestContent(sendJson);
		if(StringUtils.isNotBlank(amt)) {
			transInfoDo.setTranAmt(new BigDecimal(amt));
		}
		transInfoDo.setOrig(bizOrigJson);
		return saveTransInfo(transInfoDo);
		
	}
	
	
	@Override
	public int updateTransInfo(TransInfoDo transInfoDo) {
		try {
			//根据thirdLogNo查找分表
			String tableName = getTableName(transInfoDo.getThirdLogNo());
			String tableNumber = tableName.split(UNDERLINE)[tableName.split(UNDERLINE).length-1];
			transInfoDo.setTableNumber(tableNumber);
			return transInfoDao.updateTransInfo(tableName, transInfoDo);
		} catch (Exception e) {
			String errMsg = "更新数据异常-"+JSON.toJSONString(transInfoDo);
			logger.error(errMsg,e);
		}
		return 0;
	}

	@Override
	public int updateTransInfoBythirdLogNo(TransInfoDo transInfoDo) {
		try {
			//根据thirdLogNo查找分表
			String tableName = getTableName(transInfoDo.getThirdLogNo());
			String tableNumber = tableName.split(UNDERLINE)[tableName.split(UNDERLINE).length-1];
			transInfoDo.setTableNumber(tableNumber);
			return transInfoDao.updateTransInfoBythirdLogNo(tableName, transInfoDo);
		} catch (Exception e) {
			String errMsg = "更新数据异常-"+JSON.toJSONString(transInfoDo);
			logger.error(errMsg,e);
		}
		return 0;
	}
	
	/**
	 * 错误记录
	 */
	@Override
	public int updateTransInfoError(String reqNo, String message) {
		TransInfoDo transInfoDo = new TransInfoDo();
		transInfoDo.setThirdLogNo(reqNo);
		transInfoDo.setResponseContent(message);
		transInfoDo.setResponseTime(new Date());
		transInfoDo.setRspCode("SYSERR");
		return this.updateTransInfoBythirdLogNo(transInfoDo);
	}

	/**
	 * 返回信息记录
	 */
	@Override
	public int updateTransInfoResponse(String reqNo, 
									   String response,
									   String reponseCode,
									   String responseMsg,
									   Date responseTime
									   ) {
		TransInfoDo transInfoDo = new TransInfoDo();
		transInfoDo.setThirdLogNo(reqNo);
		transInfoDo.setResponseContent(response);
		transInfoDo.setResponseTime(responseTime);
		transInfoDo.setRspCode(reponseCode);
		transInfoDo.setRspMsg(responseMsg);
		if("1".equals(reponseCode)) {
			transInfoDo.setResponseResult("SUCCESS");
		}
		return this.updateTransInfoBythirdLogNo(transInfoDo);
	}
	
	
	@Override
	public TransInfoDo queryTransInfo(Map<String, Object> paramMap) {
		return transInfoDao.queryTransInfo(paramMap);
	}
	
	/**
	 * getTable:组装分表名称. <br>
	 *
	 * @author harry
	 * @param value
	 * @param clazzName
	 * @param tableNumber
	 * @return
	 */
	private static String getTable(long value, String clazzName, int tableNumber) {
		if (value < 0) {
			return null;
		}
		String endNum = null;
		if (tableNumber < 10) {
			return clazzName;
		} else if (tableNumber == 10) {
			endNum = String.format("%01d", value % 10);
		} else if (tableNumber == 100) {
			endNum = String.format("%02d", value % 100);
		} else if (tableNumber == 1000) {
			endNum = String.format("%03d", value % 1000);
		} else if (tableNumber == 10000) {
			endNum = String.format("%04d", value % 10000);
		}
		
		if (endNum != null) {
			return new StringBuilder(clazzName).append(UNDERLINE)
					.append(endNum).toString();
		}
		return clazzName;
	}
	
	
	/**
	 * getTableNumberByDigest:获取第几个分表. <br>
	 *
	 * @author harry
	 * @param str
	 * @return
	 */
	public static <T> int getTableNumberByDigest(String str) {
		if (str == null || str.trim() == "") {
			return -1;
		}
		byte[] md5Digest = DigestUtils.md5(str.getBytes());
		int i = md5Digest[0] + (md5Digest[1]<<8);
		return Math.abs(i);
	}
	
	
	/**
	 * getTableName:获取表名. <br>
	 *
	 * @author harry
	 * @Date 2017年12月4日下午5:52:21 <br>
	 * @param str
	 * @return
	 */
	public static String getTableName(String str) {
		return getTable(getTableNumberByDigest(str),BASETABLENAME,TABLENUMBER);
	}
	
	public static void main(String[] args) {
		System.out.println(getTableName(UUID.randomUUID().toString()));
	}


}

	