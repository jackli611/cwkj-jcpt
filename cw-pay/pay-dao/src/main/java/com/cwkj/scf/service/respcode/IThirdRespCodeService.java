package com.cwkj.scf.service.respcode;

public interface IThirdRespCodeService {
	
	String selectMsgByCode(String code);

}
