package com.cwkj.scf.service.respcode;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cwkj.scf.dao.respcode.ThirdResponseCodeMsgMapper;
import com.cwkj.scf.model.respcode.ThirdResponseCodeMsg;
import com.cwkj.scf.model.respcode.ThirdResponseCodeMsgExample;

@Service("thirdRespCodeService")
class ThirdRespCodeServiceImpl implements IThirdRespCodeService {
	@Resource
	private ThirdResponseCodeMsgMapper respCodeMapper;


	@Override
	public String selectMsgByCode(String code) {
		ThirdResponseCodeMsgExample example = new ThirdResponseCodeMsgExample();
		example.createCriteria().andCodeEqualTo(code);
		List<ThirdResponseCodeMsg> repLst =  respCodeMapper.selectByExample(example );
		if(null != repLst && repLst.size()>0) {
			return repLst.get(0).getRemark();
		}
		return "";
	}

}
