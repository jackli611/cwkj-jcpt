package com.cwkj.scf.dao.transinfo;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cwkj.scf.model.transinfo.TransInfoDo;

@Repository
public interface ITransInfoDao {

    public int insertTransInfo(@Param("tableName") String tableName,@Param("do")TransInfoDo transInfoDo);
    
    public int updateTransInfo(@Param("tableName") String tableName,@Param("do")TransInfoDo transInfoDo);
    
    public int updateTransInfoBythirdLogNo(@Param("tableName") String tableName,@Param("do")TransInfoDo transInfoDo);
    
    public TransInfoDo queryTransInfo(Map<String,Object> paramMap);
}
