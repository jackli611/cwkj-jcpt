package com.cwkj.scf.service.pay.wx;

import com.cwkj.scf.model.payform.DeductForm;

public class WxDeductForm extends DeductForm {

	private String signUrl;
	private String wxAuthCode;
	private String channelCode;
	private String openId;
	

	public String getSignUrl() {
		return signUrl;
	}

	public void setSignUrl(String signUrl) {
		this.signUrl = signUrl;
	}

	public String getWxAuthCode() {
		return wxAuthCode;
	}

	public void setWxAuthCode(String wxAuthCode) {
		this.wxAuthCode = wxAuthCode;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	
	
}
