package com.cwkj.scf.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WxConfig {
	
	private static String appId;
	private static String secret;
	private static String mchId;
	private static String certFile;
	private static String mchSecret;
	
	private static String notifyUrl ="http://loan.xwckeji.com/wxpaybycodenotify.do";

	public  static String getAppId() {
		 return WxConfig.appId;
	}

	public  static String getSecret() {
		return WxConfig.secret;
	}

	public static String getMchId() {
		return mchId;
	}

	@Value("${wx.appId}")
	public  void setAppId(String appId) {
		WxConfig.appId = appId;
	}
	@Value("${wx.secret}")
	public  void setSecret(String secret) {
		WxConfig.secret = secret;
	}
	
	@Value("${wx.mchId}")
	public  void setMchId(String mchId) {
		WxConfig.mchId = mchId;
	}

	public static String getCertFile() {
		return certFile;
	}

	@Value("${wx.certFile}")
	public  void setCertFile(String certFile) {
		WxConfig.certFile = certFile;
	}

	public static String getNotifyUrl() {
		return notifyUrl;
	}

	public static String getMchSecret() {
		return mchSecret;
	}

	
	@Value("${wx.mchSecret}")
	public  void setMchSecret(String mchSecret) {
		WxConfig.mchSecret = mchSecret;
	}
	
}
