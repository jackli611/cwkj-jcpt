package com.cwkj.scf.service.pay.wx;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.JsonUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.config.WxConfig;
import com.cwkj.scf.dao.account.AccountFlowLogMapper;
import com.cwkj.scf.dao.account.AccountTradeFlowMapper;
import com.cwkj.scf.model.account.AccountFlowLogWithBLOBs;
import com.cwkj.scf.model.account.AccountTradeFlow;
import com.cwkj.scf.model.account.AccountTradeFlowExample;
import com.cwkj.scf.wxpay.util.JSSDKSignUtil;
import com.github.wxpay.sdk.MyWxConfig;
import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConfig;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
@Service("wxPayService")
public class WxPayServiceImpl implements IWxPayService {
	
    private final static Logger logger = Logger.getLogger(WxPayServiceImpl.class);

   
    @Autowired
    private AccountTradeFlowMapper tradeFlowService;
    @Autowired
    private AccountFlowLogMapper flowLogService;


    /* (non-Javadoc)
	 * @see com.cwkj.scf.service.pay.impl.IWxPayService#prePay(com.cwkj.scf.service.pay.impl.WxDeductForm)
	 */
    @Override
	public BaseResult<Map> prePay(WxDeductForm deductForm) {
    	BaseResult<Map> mv = new BaseResult<Map>();
    	try {
    		
    		
    		String payType = deductForm.getPayFeeCode();
    		String preTradeNo = deductForm.getOrderNo()+"_"+payType;
    		String totalFee = deductForm.getAMOUNT();
    		
    		String body = deductForm.getPayFeeName();
    		
    		AccountTradeFlow record ;
    		//初始化微信配置
    		WXPayConfig config = new MyWxConfig(WxConfig.getAppId(),WxConfig.getMchId(),WxConfig.getMchSecret(),WxConfig.getCertFile(),WxConfig.getSecret());
    		WXPay wxpay = new WXPay(config, false);
    		
    		
    		record = buildTradeRecord(deductForm.getUserId(), deductForm.getOrderNo(),  payType, preTradeNo, totalFee, body);
    		
    		//是否已存生成预付订单号， itemId保存的就是预付订单号
    		if(null == record.getItemId() || record.getItemId().trim().length()<=0) {
	    		savePrepay(deductForm.getUserId(),
	    				   deductForm.getOrderNo(), 
	    				   preTradeNo, 
	    				   totalFee, 
	    				   record, 
	    				   wxpay,
	    				   deductForm.getUSR_IP(),
	    				   deductForm.getWxAuthCode(),
	    				   deductForm.getOpenId());
    		}
    		
    		Map<String,String> retMap = signChoosePayJsMethod(record, wxpay);
            
            Map<String,String> signMap = signWXConfigJs(deductForm.getSignUrl(),record);
            
            Map<String,Map> resultData = new HashMap<String,Map>();
            resultData.put("payData", retMap);
            resultData.put("signData", signMap);
    		mv.setData(resultData);
            mv.setCode(BaseResult.CODE_SUCCESS);
            
    	} catch (Exception e) {
            logger.error("wx pay error:", e);
            mv.setCode(BaseResult.CODE_EXCEPTION);
            mv.setMsg(e.getMessage());
        }
        return mv;
	}

	private AccountTradeFlow buildTradeRecord(String userId ,String orderNo, String payType, String preTradeNo, String totalFee, String body) {
		AccountTradeFlow record;
		//检查交易记录是否存在
		AccountTradeFlowExample example = new AccountTradeFlowExample();
		example.createCriteria().andOrderIdEqualTo(orderNo).andBizFlowEqualTo(preTradeNo);
		List<AccountTradeFlow> tradeLst = tradeFlowService.selectByExample(example);
		if(null == tradeLst || tradeLst.size()<1) {
			record = savePayTradeRecord(userId, orderNo, "platform",payType, preTradeNo, totalFee, body);
		}else {
			record = tradeLst.get(0);
		}
		return record;
	}



	private Map<String,String> signChoosePayJsMethod(AccountTradeFlow record, WXPay wxpay) throws Exception {
		//获取微信 js choosewxpay sign 签名和参数
		Map<String,String> reqMap = new HashMap<String,String>();
		reqMap.put("package", "prepay_id="+record.getItemId());
		Map<String, String> signInfo = wxpay.signJsPageData(reqMap);
		logger.info("sign js page data result=======================================:"+signInfo);

		AccountFlowLogWithBLOBs record3 = new AccountFlowLogWithBLOBs();
		record3.setFlowId(String.valueOf(record.getId()));
		record3.setRequestParams(JsonUtils.toJSONString(reqMap));
		record3.setRespContent(JsonUtils.toJSONString(signInfo));
		record3.setBizFlow(record.getBizFlow());
		record3.setBizType(record.getBizType());
		record3.setOrderId(record.getOrderId());
		record3.setItemId(record.getItemId());
		record3.setTradeType("choosewxpay");
		record3.setCreateTime(new Date());
		record3.setLogStatus("T");
		flowLogService.insertSelective(record3);
		
		if(null != signInfo){
			String val = signInfo.get("package");
			//页面不能用package关键字
			signInfo.put("packageStr", val);
		}
		// end 获取微信 js choosewxpay sign 签名和参数
		return signInfo;
		
	}

	private Map<String,String> signWXConfigJs(String signUrl, AccountTradeFlow record)
			throws UnsupportedEncodingException, Exception {
		//wx config js sign
		//支付页面的url
		signUrl=URLDecoder.decode(signUrl, "utf8");
		Map<String, String> jssignInfo = JSSDKSignUtil.getSignInfo(JSSDKSignUtil.getWxAccessToken(WxConfig.getAppId(),WxConfig.getSecret()), signUrl );
		logger.debug("sign js config  data result=======================================:"+jssignInfo);
		AccountFlowLogWithBLOBs record4 = new AccountFlowLogWithBLOBs();
		record4.setFlowId(String.valueOf(record.getId()));
		record4.setRequestParams(signUrl);
		record4.setRespContent(JsonUtils.toJSONString(jssignInfo));
		record4.setBizFlow(record.getBizFlow());
		record4.setBizType(record.getBizType());
		record4.setOrderId(record.getOrderId());
		record4.setItemId(record.getItemId());
		record4.setTradeType("jsapiSign");
		record4.setCreateTime(new Date());
		record4.setLogStatus("T");
		flowLogService.insertSelective(record4);
		
		jssignInfo.put("appid", WxConfig.getAppId());
		return jssignInfo;
		//end wx config js sign
	}

	
	
	private void savePrepay(String userId , String orderId, String preTradeNo, String totalFee,
			AccountTradeFlow record, WXPay wxpay,String ip,String wxAuthCode,String openId) throws Exception {
		
		Map<String, String> reqData = new HashMap<String,String>();
		/*
		//获取微信授权code, 获取openId
		String code = wxAuthCode;
		reqData.put("auth_code", code);
		String openIdResponse = wxpay.authCodeToOpenid(reqData );
		logger.debug("get opendId return:===============================:"+openIdResponse);
		AccountFlowLogWithBLOBs record1 = new AccountFlowLogWithBLOBs();
		record1.setFlowId(String.valueOf(record.getId()));
		record1.setRequestParams(JsonUtils.toJSONString(reqData));
		record1.setRespContent(openIdResponse);
		record1.setBizFlow(record.getBizFlow());
		record1.setBizType(record.getBizType());
		record1.setOrderId(record.getOrderId());
		record1.setItemId(record.getItemId());
		record1.setTradeType("getOpenId");
		record1.setCreateTime(new Date());
		record1.setLogStatus("T");
		flowLogService.insertSelective(record1 );
		Map<String, String> openIdMap = JsonUtils.jsonToMap(openIdResponse);
		String openId = openIdMap.get("openid");
		*/
		//end opendId
		
		
		//get prepay_id
		reqData.clear();
		reqData.put("device_info", userId);
		reqData.put("body", record.getMemo());
		reqData.put("out_trade_no", preTradeNo);
		reqData.put("total_fee", totalFee);
		reqData.put("spbill_create_ip", ip);
		reqData.put("time_start", DateUtils.format(new Date(), "yyyyMMddHHmmss"));
		reqData.put("product_id", orderId);
		reqData.put("notify_url",WxConfig.getNotifyUrl() );
		reqData.put("trade_type", "JSAPI");
		reqData.put("openid", openId);
		Map<String, String> orderPayMap = wxpay.unifiedOrder(reqData );
		logger.info("prePayId order return ==================================："+orderPayMap);
		//end get prepay_id
		//save prepayId
		String prePayId =  orderPayMap.get("prepay_id");
		record.setItemId(prePayId);
		if(!orderPayMap.containsKey("prepay_id")) {
			logger.error("获取wx pay prepay_id error");
		}else {
			AccountTradeFlow tmpTradeRecord = new  AccountTradeFlow();
			tmpTradeRecord.setId(record.getId());
			tmpTradeRecord.setItemId(prePayId);
			tradeFlowService.updateByPrimaryKeySelective(tmpTradeRecord);
		}
		
		AccountFlowLogWithBLOBs record2 = new AccountFlowLogWithBLOBs();
		record2.setFlowId(String.valueOf(record.getId()));
		record2.setRequestParams(JsonUtils.toJSONString(reqData));
		record2.setRespContent(JsonUtils.toJSONString(orderPayMap));
		record2.setBizFlow(record.getBizFlow());
		record2.setBizType(record.getBizType());
		record2.setOrderId(record.getOrderId());
		record2.setItemId(record.getItemId());
		record2.setTradeType("createPrePayOrder");
		record2.setCreateTime(new Date());
		record2.setLogStatus("T");
		flowLogService.insertSelective(record2);
	}

	private AccountTradeFlow savePayTradeRecord( String userId, 
												 String orderNo,
												 String payChannel,
												 String payType, 
												 String preTradeNo, 
												 String totalFee, 
												 String body) {
		AccountTradeFlow record;
		//保存预付订单
		record = new AccountTradeFlow();
		record.setAccountId("cash");
		record.setBizType(payType);
		record.setBizFlow(preTradeNo);
		record.setOutMember(userId);
		record.setInMember("platform");
		record.setInAccount("wxaccount");
		record.setCreateTime(new Date());
		record.setRemark("个贷");
		record.setMemo(body);
		record.setTradeType("pay");
		record.setTradeAmount(new BigDecimal(totalFee));
		record.setTradeStatus("prepayorder");
		record.setTradeStartTime(new Date());
		record.setUserId(userId);
		record.setOrderId(orderNo);
		record.setMerchantChannel(payChannel==null?"platform":payChannel);
		record.setMerchantCode(payChannel==null?"platform":payChannel);
		tradeFlowService.insertSelective(record);
		return record;
	}

	@Override
	public void wxNotify(String wxResponse){
		//记录微信异步通知
		AccountFlowLogWithBLOBs record1 = new AccountFlowLogWithBLOBs();
		record1.setRespContent(wxResponse);
		record1.setTradeType("wxpaynotify");
		record1.setCreateTime(new Date());
		record1.setLogStatus("T");
		flowLogService.insertSelective(record1 );
	}
    
   
}
