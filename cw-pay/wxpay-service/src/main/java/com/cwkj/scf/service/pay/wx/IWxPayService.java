package com.cwkj.scf.service.pay.wx;

import java.util.Map;

import com.cwkj.jcpt.util.BaseResult;

public interface IWxPayService {

	/**
	 *	支付之前准备支付数据，微信支付采用h5，先获取 js签名
	 * @return
	 */
	BaseResult<Map> prePay(WxDeductForm deductForm);

	/**
	 * 微信支付回调
	 * @param wxresponse wx xml
	 */
	void wxNotify(String wxResponse);

}