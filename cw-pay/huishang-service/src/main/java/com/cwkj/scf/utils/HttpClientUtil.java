package com.cwkj.scf.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.scf.common.HuiShangConstants;

public class HttpClientUtil {
	
	private static Logger logger = LoggerFactory.getLogger(HuiShangUtil.class);

    /**
     * @param url
     * @param body
     * @return
     * @throws Exception
     */
    public static String requestBodyPost(String url, String body) throws Exception {
        
        logger.debug("请求发送地址：{}" , url);
		HttpHandler http = new HttpHandler();
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair(HuiShangConstants.HUISHANG_REQDATA, body));
		HttpEntity reqEntity = new UrlEncodedFormEntity(parameters, HuiShangConstants.HUISHANG_CHARSET_UTF_8);
		return http.doPost(url, reqEntity, "UTF-8");
		
    }
}
