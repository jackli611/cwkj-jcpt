package com.cwkj.scf.huishang.api;

import java.math.BigDecimal;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.base.bo.BaseBo;
import com.cwkj.scf.model.payform.BaseForm;

/**
 * 	代扣
 * @author harry
 *
 */
public class WithDrawForm extends BaseForm{
	
	
	/**
	 * 
	 *  序号	中文名称	变量名	字段类型	最大长度	必填标志	变量格式	字段路径	备注
		HEAD.REQ	报文头							
		F1	电子账户	ACCT_NO	A	19-Jan-00	M			
		F2	绑定卡卡号	CARD_BIND	A	1-Feb-00	M	esb校验		
		F3	币种	CURRENCY	A	3-Jan-00	M	4-Jun-00		
		F4	金额	AMOUNT	A	12-Jan-00	M	精确到分		100即1元
		F5	证件类型	KEYTYPE	A	2-Jan-00	M	"01 - 18位身份证 20 - 其它 25 - 企业社会信用代码"		
		F6	证件号码	IDNO	A	18-Jan-00	M			
		F7	姓名	SURNAME	A	29-Feb-00	M			
		F8	手机号码	MOBILE	A	12-Jan-00	M			
		F9	是否免密	PINFLAGE	A	1-Jan-00	M	"Y -- 校验密码  	N -- 不校验密码"		填N
		F10	电子账户密码	PINDATA	A	16-Jan-00	M			
		F11	是否指定路由	ROUT_FLAG	A	1-Jan-00	O	"Y -- 指定路由		N -- 自动路由"		 
		F12	路由代码	ROUT_CODE	A	2-Jan-00	C	"G1-大额		N1-超网	CP-Chinapay"		当ROUT_FLAG为Y时，必填
		F13	对公账户标识	BUSINESS_ACCOUNT_FLAG	A	1-Jan-00	M	"Y -- 对公账户提现	N -- 银行卡提现"		对公账户提现时，必填
		F14	开户银行联行号	BANK_CNAPS	A	20-Jan-00	C	人民银行分配的12位联行号		当ROUT_CODE为Gx时，该字段必填
		F15	开户银行代码	BANK_CODE	A	20-Jan-00	C			当BUSINESS_ACCOUNT_FLAG为Y且ROUT_CODE不为Gx时，该字段必填，填写总行代码(枚举值参见BANK_CODE）
		F16	开户银行英文缩写	BANK_NAME_EN	A	20-Jan-00	O			保留
		F17	开户银行中文名称	BANK_NAME_CN	A	19-Feb-00	O			当BUSINESS_ACCOUNT_FLAG为Y且ROUT_CODE为CP时，该字段必填，精确到分支行
		F18	开户行省份	ISS_BANK_PROVINCE	A	20-Jan-00	O			当BUSINESS_ACCOUNT_FLAG为Y且ROUT_CODE为CP时，该字段必填，填写收款卡号的省份
		F19	开户行城市	ISS_BANK_CITY	A	19-Feb-00	O			当BUSINESS_ACCOUNT_FLAG为Y且ROUT_CODE为CP时，该字段必填，填写收款卡号的城市
		F20	手续费	FEE	A	8-Jan-00	O			
		F21	备注	ADD_COMMENT	A	7-May-00	O			
		F22	自定义交易描述	TXNDESC	A	9-Feb-00	O			
		F23	短信验证码	SMS_CODE	A	8-Jan-00	O			
		F24	短信序列号	SMS_SEQ	A	4-Jan-00	O			
		F25	保留域	RESERVED	A	19-Mar-00	O			
	 *  
	 *  
	*/
	
	
	/**
	 * 	绑定卡卡号
	 */
	
	private String CARD_BIND;
	
	/**
	 *  币种
	 */
	private String CURRENCY = "156";
	
	
	
	/**
	 * 证件类型:01 身份证  - 18位身份证 20 - 其它 25 - 企业社会信用代码
	 */
	private String KEYTYPE = "01"; 
	/**
	 * 证件号码
	 */
	private String IDNO;
	/**
	 * 姓名
	 */
	private String SURNAME;
	/**
	 * 手机号
	 */
	private String MOBILE;
	/**
	 * 	是否免密
	 */
	private String PINFLAGE="N";
	/**
	 * 电子账户密码
	 */
	private String PINDATA;
	/**
	 * 	是否指定路由
	 */
	private String ROUT_FLAG = "N";
	/**
	 * 	路由代码 "G1-大额		N1-超网	CP-Chinapay"		当ROUT_FLAG为Y时，必填
	 */
	private String ROUT_CODE;
	
	/**
	 * 	Y-对公账户,N-非对公账户
	 */
	private String BUSINESS_ACCOUNT_FLAG = "N";
	
	/**
	 * 
	 * 	开户银行联行号	BANK_CNAPS	A	20-Jan-00	C	人民银行分配的12位联行号		当ROUT_CODE为Gx时，该字段必填
	 */
	private String BANK_CNAPS;
	
	/**
	 *	 当BUSINESS_ACCOUNT_FLAG为Y时，该字段必填
	 */
	private String BANK_CODE;
	/**
	 * 	银行英文名称
	 */
	private String BANK_NAME_EN;
	/**
	 * 	银行中文名称
	 */
	private String BANK_NAME_CN;
	/**
	 * 	开户行省份
	 */
	private String ISS_BANK_PROVINCE;
	/**
	 * 	开户行城市
	 */
	private String ISS_BANK_CITY;
	
	/**
	 * 	手续费
	 */
	private String FEE;
	
	/**
	 * 	备注
	 */
	private String ADD_COMMENT;
	

	/**
	 * 	自定义交易描述
	 */
	private String TXNDESC;
	
	/**
	 * 	短信验证码
	 */
	private String SMS_CODE;
	/**
	 * 	短信序列号
	 */
	private String SMS_SEQ;
	
	/**
	 * 保留域
	 */
	private String RESERVED;
	
	@JSONField(name = "KEYTYPE")
	public String getKEYTYPE() {
		return KEYTYPE;
	}
	public void setKEYTYPE(String kEYTYPE) {
		KEYTYPE = kEYTYPE;
	}
	
	@JSONField(name = "IDNO")
	public String getIDNO() {
		return IDNO;
	}
	public void setIDNO(String iDNO) {
		IDNO = iDNO;
	}
	
	@JSONField(name = "MOBILE")
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	
	@JSONField(name = "CARD_BIND")
	public String getCARD_BIND() {
		return CARD_BIND;
	}
	public void setCARD_BIND(String cARD_BIND) {
		CARD_BIND = cARD_BIND;
	}
	
	
	@JSONField(name = "CURRENCY")
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	
	
	@JSONField(name = "BANK_CODE")
	public String getBANK_CODE() {
		return BANK_CODE;
	}
	public void setBANK_CODE(String bANK_CODE) {
		BANK_CODE = bANK_CODE;
	}
	@JSONField(name = "BANK_NAME_EN")
	public String getBANK_NAME_EN() {
		return BANK_NAME_EN;
	}
	public void setBANK_NAME_EN(String bANK_NAME_EN) {
		BANK_NAME_EN = bANK_NAME_EN;
	}
	@JSONField(name = "BANK_NAME_CN")
	public String getBANK_NAME_CN() {
		return BANK_NAME_CN;
	}
	public void setBANK_NAME_CN(String bANK_NAME_CN) {
		BANK_NAME_CN = bANK_NAME_CN;
	}
	@JSONField(name = "ISS_BANK_PROVINCE")
	public String getISS_BANK_PROVINCE() {
		return ISS_BANK_PROVINCE;
	}
	public void setISS_BANK_PROVINCE(String iSS_BANK_PROVINCE) {
		ISS_BANK_PROVINCE = iSS_BANK_PROVINCE;
	}
	@JSONField(name = "ISS_BANK_CITY")
	public String getISS_BANK_CITY() {
		return ISS_BANK_CITY;
	}
	public void setISS_BANK_CITY(String iSS_BANK_CITY) {
		ISS_BANK_CITY = iSS_BANK_CITY;
	}
	@JSONField(name = "SMS_CODE")
	public String getSMS_CODE() {
		return SMS_CODE;
	}
	public void setSMS_CODE(String sMS_CODE) {
		SMS_CODE = sMS_CODE;
	}
	@JSONField(name = "SMS_SEQ")
	public String getSMS_SEQ() {
		return SMS_SEQ;
	}
	public void setSMS_SEQ(String sMS_SEQ) {
		SMS_SEQ = sMS_SEQ;
	}
	@JSONField(name = "BUSINESS_ACCOUNT_FLAG")
	public String getBUSINESS_ACCOUNT_FLAG() {
		return BUSINESS_ACCOUNT_FLAG;
	}
	public void setBUSINESS_ACCOUNT_FLAG(String bUSINESS_ACCOUNT_FLAG) {
		BUSINESS_ACCOUNT_FLAG = bUSINESS_ACCOUNT_FLAG;
	}
	@JSONField(name = "FEE")
	public String getFEE() {
		return FEE;
	}
	public void setFEE(String fEE) {
		FEE = fEE;
	}
	@JSONField(name = "RESERVED")
	public String getRESERVED() {
		return RESERVED;
	}
	public void setRESERVED(String rESERVED) {
		RESERVED = rESERVED;
	}
	@JSONField(name = "SURNAME")
	public String getSURNAME() {
		return SURNAME;
	}
	public void setSURNAME(String sURNAME) {
		SURNAME = sURNAME;
	}
	@JSONField(name = "PINFLAGE")
	public String getPINFLAGE() {
		return PINFLAGE;
	}
	public void setPINFLAGE(String pINFLAGE) {
		PINFLAGE = pINFLAGE;
	}
	@JSONField(name = "PINDATA")
	public String getPINDATA() {
		return PINDATA;
	}
	public void setPINDATA(String pINDATA) {
		PINDATA = pINDATA;
	}
	@JSONField(name = "ROUT_FLAG")
	public String getROUT_FLAG() {
		return ROUT_FLAG;
	}
	public void setROUT_FLAG(String rOUT_FLAG) {
		ROUT_FLAG = rOUT_FLAG;
	}
	@JSONField(name = "ROUT_CODE")
	public String getROUT_CODE() {
		return ROUT_CODE;
	}
	public void setROUT_CODE(String rOUT_CODE) {
		ROUT_CODE = rOUT_CODE;
	}
	@JSONField(name = "BANK_CNAPS")
	public String getBANK_CNAPS() {
		return BANK_CNAPS;
	}
	public void setBANK_CNAPS(String bANK_CNAPS) {
		BANK_CNAPS = bANK_CNAPS;
	}
	@JSONField(name = "ADD_COMMENT")
	public String getADD_COMMENT() {
		return ADD_COMMENT;
	}
	public void setADD_COMMENT(String aDD_COMMENT) {
		ADD_COMMENT = aDD_COMMENT;
	}
	@JSONField(name = "TXNDESC")
	public String getTXNDESC() {
		return TXNDESC;
	}
	public void setTXNDESC(String tXNDESC) {
		TXNDESC = tXNDESC;
	}
	
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.CARD_BIND, "绑定卡卡号不能为空");
		Assert.hasText(this.CURRENCY, "币种不能为空");
		Assert.hasText(this.getAMOUNT(), "金额不能为空");
		Assert.hasText(this.KEYTYPE, "KEYTYPE证件类型不能为空");
		Assert.hasText(this.IDNO, "证件号码不能为空");
		Assert.hasText(this.SURNAME, "姓名不能为空");
		Assert.hasText(this.MOBILE, "手机号码不能为空");
		Assert.hasText(this.PINFLAGE, "是否免密不能为空");
		Assert.hasText(this.ADD_COMMENT, "备注不能为空");
		
		if("Y".equals(this.ROUT_FLAG)) {
			Assert.hasText(this.ROUT_CODE, "路由代码不能为空");
		}
		if("Y".equals(this.BUSINESS_ACCOUNT_FLAG) && !"Gx".equals(this.ROUT_CODE)) {
			Assert.hasText(this.BANK_CODE, "开户银行代码不能为空");
		}
		
		if("Gx".equals(this.ROUT_CODE)) {
			Assert.hasText(this.BANK_CNAPS, "开户银行联行号不能为空");
		}
		
		//Y -- 对公账户提现
		if("Y".equals(this.BUSINESS_ACCOUNT_FLAG) && "CP".equals(this.ROUT_CODE)) {
			Assert.hasText(this.BANK_NAME_CN, "开户银行中文名称不能为空");
		}
		if("Y".equals(this.BUSINESS_ACCOUNT_FLAG) && "CP".equals(this.ROUT_CODE)) {
			Assert.hasText(this.ISS_BANK_PROVINCE, "开户行省份不能为空");
		}
		if("Y".equals(this.BUSINESS_ACCOUNT_FLAG) && "CP".equals(this.ROUT_CODE)) {
			Assert.hasText(this.ISS_BANK_CITY, "开户行城市不能为空");
		}
		
		
		if((new BigDecimal(this.getAMOUNT())).compareTo(BigDecimal.ZERO)<=0) {
			Assert.isTrue(false, "交易金额必须大于0");
		}
		
		//金额必须是分为单位，就不能有小数
		if(this.getAMOUNT().contains(".")) {
			Assert.isTrue(false, "交易金额必须是以分为单位");
		}
	}
	
}
