package com.cwkj.scf.service.balance;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.AccountBalanceForm;
import com.cwkj.scf.huishang.api.QueryTradeDetailForm;

/**
 * 	余额查询
 */
public interface IAccountBalanceService {


    /**
     * 	余额查询【5863】
     *
     * @return
     */
    public BaseResult getRemainAmount(AccountBalanceForm form,TranCodeEnum tce);
    
    
    /**
     * 	交易明细查询【5824】
     *
     * @return
     */
    public BaseResult queryTradeDetail(QueryTradeDetailForm form,TranCodeEnum tce);
}
