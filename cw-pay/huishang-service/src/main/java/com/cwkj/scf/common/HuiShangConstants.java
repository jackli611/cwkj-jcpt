package com.cwkj.scf.common;
/**
 * ClassName: HuiShangConstants <br>
 * Description: 常量
 * @author harry
 * @version
 */
public class HuiShangConstants {

	public static final String HUISHANG_NAME = "徽商接口";
	public static final String HUISHANG_IN_PARAM = "入参";
	public static final String HUISHANG_OUT_PARAM = "出参";
	public static final String HUISHANG_SUCCESS = "成功";
	public static final String HUISHANG_FAIL = "失败";
	public static final String HUISHANG_RSPCODE_SUCCESS= "00000000";//响应码-成功
	public static final String HUISHANG_RSPCODE_TIMEOUT= "GW3002";//响应码-超时
	public static final String HUISHANG_CHARSET_UTF_8= "UTF-8";//编码
	public static final String HUISHANG_RESPONSERESULT_SUCCESS = "SUCCESS";
	public static final String HUISHANG_RESPONSERESULT_FAIL = "FAIL";
	public static final String HUISHANG_RESPONSERESULT_PROCESSING = "PROCESSING";
	public static final String HUISHANG_RESPONSERESULT_EXCEPTION = "EXCEPTION";
	
	//request data
	public static final String HUISHANG_REQDATA = "reqdata";
	
	
}

	