package com.cwkj.scf.service.sign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.SignConfirmForm;
import com.cwkj.scf.huishang.api.SignDoneForm;
import com.cwkj.scf.huishang.api.SignQueryForm;
import com.cwkj.scf.service.huishang.BaseService;

@Service("bankSignService")
public class BankSignServiceImpl extends BaseService implements IBankSignService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
   
       
    /**
     * 	签约查询【3490】
     *
     * @return
     */
	@Override
	public BaseResult signQuery(SignQueryForm form, TranCodeEnum tce) {
		//请求徽商json报文
		SignQueryBo dest = new SignQueryBo();
		return this.doTrade(form, dest, tce);
	}


	@Override
	public BaseResult doSign(SignDoneForm form, TranCodeEnum tce) {

		//请求徽商json报文
		SignDoneBo dest = new SignDoneBo();
		return this.doTrade(form, dest, tce);
	}


	@Override
	public BaseResult confirmSign(SignConfirmForm form, TranCodeEnum tce) {
		//请求徽商json报文
		SignConfirmBo dest = new SignConfirmBo();
		return this.doTrade(form, dest, tce);
	}
    
}
