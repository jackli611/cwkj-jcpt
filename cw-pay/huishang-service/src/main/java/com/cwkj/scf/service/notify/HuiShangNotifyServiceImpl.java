package com.cwkj.scf.service.notify;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.service.huishang.BaseService;
import com.cwkj.scf.utils.HuiShangUtil;

/**
 * 	提现冲正通知
 * @author harry
 *
 */
@Service("withdrawNotifyService")
public class HuiShangNotifyServiceImpl extends BaseService implements IWithDrawNotifyService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
   


	@Override
	public BaseResult withdrawNotify(WithDrawNotifyForm form, TranCodeEnum tce) {

		Map jsonMap = JSONObject.parseObject(form.getEncryptData(),Map.class);
		// 解密
		Map responseMap;
		try {
			responseMap = HuiShangUtil.decryptResponse(jsonMap);
			// 验签
			boolean verifyResult = HuiShangUtil.checkResponseSign(responseMap);
			
			if(verifyResult==false) {
				throw new RuntimeException("调用返回没有验证通过");
			}
			//返回调用结果
			return HuiShangUtil.buildResult(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

    
}
