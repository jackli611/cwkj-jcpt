package com.cwkj.scf.service.sign;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.base.bo.BaseBo;
import com.cwkj.scf.config.HuiShangConfig;

/**
 * 	签约查询、签约、签约确认基类
 * @author harry
 * 
 *
 */
public class SignBo extends BaseBo {
	
	
	private String  ACCT_NO = HuiShangConfig.getAcctNo();
	private String  BIND_CARD_NO;
	private String  ID_CODE;
	private String  ID_TYPE = "01";
	private String  NAME;
	private String  PHONE_NO;
	private String  RESERVED;
	
	@JSONField(name = "ACCT_NO")
	public String getACCT_NO() {
		return ACCT_NO;
	}
	public void setACCT_NO(String aCCT_NO) {
		ACCT_NO = aCCT_NO;
	}
	
	@JSONField(name = "BIND_CARD_NO")
	public String getBIND_CARD_NO() {
		return BIND_CARD_NO;
	}
	public void setBIND_CARD_NO(String bIND_CARD_NO) {
		BIND_CARD_NO = bIND_CARD_NO;
	}
	
	@JSONField(name = "ID_CODE")
	public String getID_CODE() {
		return ID_CODE;
	}
	public void setID_CODE(String iD_CODE) {
		ID_CODE = iD_CODE;
	}
	
	@JSONField(name = "ID_TYPE")
	public String getID_TYPE() {
		return ID_TYPE;
	}
	public void setID_TYPE(String iD_TYPE) {
		ID_TYPE = iD_TYPE;
	}
	
	@JSONField(name = "NAME")
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	@JSONField(name = "PHONE_NO")
	public String getPHONE_NO() {
		return PHONE_NO;
	}
	public void setPHONE_NO(String pHONE_NO) {
		PHONE_NO = pHONE_NO;
	}
	@JSONField(name = "RESERVED")
	public String getRESERVED() {
		return RESERVED;
	}
	public void setRESERVED(String rESERVED) {
		RESERVED = rESERVED;
	}
	
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.ACCT_NO, "电子账户不能为空");
		Assert.hasText(this.BIND_CARD_NO, "银行卡不能为空");
		Assert.hasText(this.ID_CODE, "证件号码不能为空");
		Assert.hasText(this.ID_TYPE, "证件类型不能为空");
		Assert.hasText(this.NAME, "姓名不能为空");
		Assert.hasText(this.PHONE_NO, "手机号不能为空");
	}
}
