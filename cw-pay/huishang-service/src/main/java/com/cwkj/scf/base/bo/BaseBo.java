package com.cwkj.scf.base.bo;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.config.HuiShangConfig;

public class BaseBo implements java.io.Serializable {
	
	

	/**
	 *  报文头定义							
		序号	中文名称	变量名	类型	最大长度	必填标志	变量格式	ESB处理内容	备注
		F1	交易代码	TRXCODE	n	4	M	4位交易代码		请求方填写
		F2	银行代码	BANKCODE	n	8	M	8位银行代码		请求方填写
		F3	交易日期	TRXDATE	n	8	M	YYYYMMDD		请求方填写
		F4	交易时间	TRXTIME	n	6	M	hhmmss		请求方填写
		F5	合作单位编号	COINSTCODE	ans	6	M	 		固定值000348
		F6	合作单位渠道	COINSTCHANNEL	ans	6	M	 		固定值000002
		F7	交易流水号	SEQNO	n	20	M	8 ~ 20 位数字流水号		"推荐填写规则为:		YYYYMMDD+hhmmss+6位的流水号"
		F8	ESB内部渠道	SOURCE	ans	2	M		填写内部渠道号	固定值KB
		F9	应答码	RETCODE	an	8	O			填空
		F10	应答码描述	RETMSG	ans	60	O			填空
		F11	报文头保留域	HEADRESERVED	ans	50	C			可选
	 */
	
	private String TRXCODE;
	private String BANKCODE =HuiShangConfig.getBANKCODE();
	private String TRXDATE;
	private String TRXTIME;
	private String COINSTCODE = HuiShangConfig.getCoinstCode();
	private String COINSTCHANNEL = HuiShangConfig.getCoinstChannel();
	/**
	 * 发送给第三方的交易流水， 要求唯一
	 */
	private String SEQNO; 
	private String SOURCE =HuiShangConfig.getSource();
	private String RETCODE;
	private String RETMSG;
	private String HEADRESERVED ;
	
	@JSONField(name = "TRXCODE")
	public String getTRXCODE() {
		return TRXCODE;
	}
	public void setTRXCODE(String tRXCODE) {
		TRXCODE = tRXCODE;
	}
	
	@JSONField(name = "BANKCODE")
	public String getBANKCODE() {
		return BANKCODE;
	}
	public void setBANKCODE(String bANKCODE) {
		BANKCODE = bANKCODE;
	}
	
	@JSONField(name = "TRXDATE")
	public String getTRXDATE() {
		return TRXDATE;
	}
	public void setTRXDATE(String tRXDATE) {
		TRXDATE = tRXDATE;
	}
	@JSONField(name = "TRXTIME")
	public String getTRXTIME() {
		return TRXTIME;
	}
	public void setTRXTIME(String tRXTIME) {
		TRXTIME = tRXTIME;
	}
	@JSONField(name = "COINSTCODE")
	public String getCOINSTCODE() {
		return COINSTCODE;
	}
	public void setCOINSTCODE(String cOINSTCODE) {
		COINSTCODE = cOINSTCODE;
	}
	@JSONField(name = "COINSTCHANNEL")
	public String getCOINSTCHANNEL() {
		return COINSTCHANNEL;
	}
	public void setCOINSTCHANNEL(String cOINSTCHANNEL) {
		COINSTCHANNEL = cOINSTCHANNEL;
	}
	@JSONField(name = "SEQNO")
	public String getSEQNO() {
		return SEQNO;
	}
	public void setSEQNO(String sEQNO) {
		SEQNO = sEQNO;
	}
	
	@JSONField(name = "SOURCE")
	public String getSOURCE() {
		return SOURCE;
	}
	public void setSOURCE(String sOURCE) {
		SOURCE = sOURCE;
	}
	@JSONField(name = "RETCODE")
	public String getRETCODE() {
		return RETCODE;
	}
	public void setRETCODE(String rETCODE) {
		RETCODE = rETCODE;
	}
	@JSONField(name = "RETMSG")
	public String getRETMSG() {
		return RETMSG;
	}
	public void setRETMSG(String rETMSG) {
		RETMSG = rETMSG;
	}
	@JSONField(name = "HEADRESERVED")
	public String getHEADRESERVED() {
		return HEADRESERVED;
	}
	public void setHEADRESERVED(String hEADRESERVED) {
		HEADRESERVED = hEADRESERVED;
	}
	
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		Assert.hasText(this.TRXCODE, "交易代码不能为空");
		Assert.hasText(this.BANKCODE, "银行代码BANKCODE不能为空");
		Assert.hasText(this.COINSTCODE, "合作单位编号不能为空");
		Assert.hasText(this.TRXDATE, "交易日期不能为空");
		Assert.hasText(this.TRXTIME, "交易时间不能为空");
		Assert.hasText(this.COINSTCHANNEL, "合作单位渠道不能为空");
		Assert.hasText(this.SEQNO, "交易流水不能为空");
		Assert.hasText(this.SOURCE, "ESB内部渠道不能为空");
	}
	

}
