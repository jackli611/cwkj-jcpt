package com.cwkj.scf.service.deduct;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.service.huishang.BaseService;

/**
 * 	代扣服务
 * @author harry
 *
 */
@Service("deductService")
public class DeductServiceImpl extends BaseService implements IDeductService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
   
    
    /**
     * 	代扣记账【3612】
     *
     * @return
     */
    @Override
    public BaseResult<Map> deduct(DeductForm form, TranCodeEnum tce) {
    	
    	//请求徽商json报文
    	DeductBo destTradeBo = new DeductBo();
    	//元转分
    	BigDecimal amt = new BigDecimal(form.getAMOUNT());
    	form.setAMOUNT(amt.toString());
    	return this.doTrade(form, destTradeBo, tce);
    }

    
}
