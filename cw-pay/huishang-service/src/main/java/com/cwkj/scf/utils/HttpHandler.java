package com.cwkj.scf.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpHandler {
    
    
	private CloseableHttpClient httpClient;
	private RequestConfig requestConfig;
	public HttpHandler() {
        SSLContext sslContext;
        try {
            sslContext = SSLContexts.custom()
                    .useTLS()
                    .build();
            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(
                sslContext,
                new String[]{"TLSv1.1","TLSv1.2"},   
                null,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            httpClient = HttpClients.custom()
                .setSSLSocketFactory(f)
                .build();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

            
    }
	
	public String doPost(String url, HttpEntity reqEntity,String encoding){
		HttpEntity resEntity = null;
		InputStream inputStream = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		String line = null;
		String dot = "";
		StringBuffer resultString = new StringBuffer();
		
		
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Cache-Control", "no-cache");
		httpPost.setHeader("Connection", "Keep-Alive");
		httpPost.setHeader("User-Agent", "NetFox");
		httpPost.setHeader("Cache-Control", "no-cache");
		httpPost.setHeader("token","123456");
		httpPost.setConfig(requestConfig);
		httpPost.addHeader(HttpHeaders.CONTENT_ENCODING, encoding);
		httpPost.setEntity(reqEntity);
		
		try {
			CloseableHttpResponse response = httpClient.execute(
					httpPost, HttpClientContext.create());
			if( null != response ){
				resEntity = response.getEntity();
				if( null != resEntity )
				{
					inputStream = resEntity.getContent();
					isr = new InputStreamReader(inputStream ,encoding);
					br = new BufferedReader(isr);  
					while( (line = br.readLine()) != null )
					{  
						resultString.append(dot + line);
					}
				}
			}
		}catch (ClientProtocolException ex) {
		}catch (IOException e) {
		}finally{
			try {
				httpClient.close();
			} catch (IOException e) {
			}
		}
		return resultString.toString();
	}
	

	public String doPostByFile(String url, Map<String, String> request,String encoding, File file,String fileName) {
        CloseableHttpResponse response = null;
        HttpEntity resEntity = null;
        String result = "";
        
        
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Cache-Control", "no-cache");
        httpPost.setHeader("Connection", "Keep-Alive");
        httpPost.setHeader("User-Agent", "NetFox");
        httpPost.setHeader("token","123456");
        httpPost.setConfig(requestConfig);
        httpPost.addHeader(HttpHeaders.CONTENT_ENCODING, encoding);
        
        //post模式且带上传文件
        MultipartEntityBuilder mEntityBuilder = MultipartEntityBuilder.create();
        for (String key : request.keySet()) {
            mEntityBuilder.addTextBody(key, request.get(key));
        }
        //增加文件参数，strParaFileName是参数名，使用本地文件
        mEntityBuilder.addBinaryBody(fileName, file);

        // 设置请求体
        httpPost.setEntity(mEntityBuilder.build());
        try {
            response = httpClient.execute(
                    httpPost, HttpClientContext.create());
            resEntity = response.getEntity();
            if( null != resEntity )
            {
                result = EntityUtils.toString(resEntity, encoding);
            }
        }catch (ClientProtocolException ex) {
        }catch (IOException e) {
        }finally{
            try {
                httpPost.releaseConnection();
                if(response !=null) {
                    EntityUtils.consume(response.getEntity());
                    response.close();
                }
            } catch (IOException e) {
            }
        }
        return result;
    }
}
