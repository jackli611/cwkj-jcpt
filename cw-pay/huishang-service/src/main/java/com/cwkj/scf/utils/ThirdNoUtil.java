package com.cwkj.scf.utils;

import java.util.Date;
import java.util.Random;

import org.apache.commons.lang.time.FastDateFormat;;;


public class ThirdNoUtil {

    public static String getThirdNo() {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        builder.append(FastDateFormat.getInstance("yyyyMMddHHmmss").format(new Date()));
        builder.append(random.nextInt(899999)+100000);
        return builder.toString();
    }
    
    public static String getOrderId() {
    	Random random = new Random();
    	StringBuilder builder = new StringBuilder();
    	builder.append(FastDateFormat.getInstance("yyyyMMddHHmmss").format(new Date()));
    	builder.append(random.nextInt(89999999)+10000000);
    	return builder.toString();
    }
    
    public static void main(String[] args) {
    	System.out.println(getThirdNo());
    	System.out.println(getOrderId());
	}
}
