package com.cwkj.scf.service.sign;

/**
 * 	签约查询
 * @author harry
 * 
 *
 */
public class SignQueryBo extends SignBo {
	
	/**
	 * 
	 * 序号	中文名称	变量名	字段类型	最大长度	必填标志	变量格式	字段路径	备注
		HEAD.REQ								
		F1	电子账号	ACCT_NO	A	18-Jan-00	M			
		F2	绑定卡号	BIND_CARD_NO	A	1-Feb-00	M			
		F3	证件号码	ID_CODE	A	18-Jan-00	M			
		F4	证件类型	ID_TYPE	A	4-Jan-00	M			
		F5	姓名	NAME	A	29-Feb-00	M			
		F6	手机号码	PHONE_NO	A	11-Jan-00	M			
		F7	保留字段	RESERVED	A	9-Apr-00	C			暂不上送值
		补充说明	
	 * 
	 */
	
}
