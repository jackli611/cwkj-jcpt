package com.cwkj.scf.huishang.api;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.config.HuiShangConfig;
import com.cwkj.scf.model.payform.BaseForm;

/**
 * 	余额查询
 * @author harry
 *
 */
public class AccountBalanceForm extends BaseForm{
	
	
	/**
	 * I0002	余额查询	 			XML	5863	万科物业	直销银行ESB
										
		I0002.REQ	请求报文							
		序号	中文名称	变量名	类型	最大长度	必填标志	变量格式	ESB处理内容	备注
		HEAD.REQ								
		F1	电子账号	CARDNBR	A	19	M			
		F2	是否检查密码标志	PINFLAG	A	1	M	"0-不检查密码 1-检查取现密码"		上送0
		F3	密码	PIN	A	8	C			 
		F4	保留域	RESERVED	A	200	O			
		F5	第三方保留域	TRDRESV	A	100	O			
										
		I0002.RSP	应答报文							
		序号	中文名称	变量名	类型	最大长度	必填标志	变量格式	ESB处理内容	备注
		HEAD.RSP	报文头							
		F1	电子账号	CARDNBR	A	19	M			
		F2	持卡人姓名	NAME	A	60	C			
		F3	可用余额	AVAIL_BAL	N	17,2	C			100即1元
		F4	账面余额	CURR_BAL	N	17,2	C			100即1元
		F5	保留域	RESERVED	A	400	C			
		F6	第三方保留域	TRDRESV	A	100	C			
										
		补充说明								
										
		I0002	余额查询	 			XML	5863	 	直销银行ESB
	 */
	
	
	/**
	 * 	电子账号
	 */
	
	private String CARDNBR = HuiShangConfig.getAcctNo();
	
	
	/**
	 * 	是否免密
	 */
	private String PINFLAG="0";
	/**
	 * 电子账户密码
	 */
	private String PIN;
	
	/**
	 * 保留域
	 */
	private String RESERVED;
	
	/*
	 * 第三方保留域
	 */
	private String TRDRESV;
	
	
	
	@JSONField(name = "RESERVED")
	public String getRESERVED() {
		return RESERVED;
	}
	public void setRESERVED(String rESERVED) {
		RESERVED = rESERVED;
	}
	
	@JSONField(name = "PINFLAG")
	public String getPINFLAG() {
		return PINFLAG;
	}
	public void setPINFLAG(String pINFLAG) {
		PINFLAG = pINFLAG;
	}
	@JSONField(name = "PIN")
	public String getPIN() {
		return PIN;
	}
	public void setPIN(String pIN) {
		PIN = pIN;
	}
	@JSONField(name = "CARDNBR")
	public String getCARDNBR() {
		return CARDNBR;
	}
	public void setCARDNBR(String cARDNBR) {
		CARDNBR = cARDNBR;
	}
	@JSONField(name = "TRDRESV")
	public String getTRDRESV() {
		return TRDRESV;
	}
	public void setTRDRESV(String tRDRESV) {
		TRDRESV = tRDRESV;
	}
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.CARDNBR, "电子账户不能为空");
	}
	
}
