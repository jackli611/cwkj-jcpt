package com.cwkj.scf.service.deduct;

import org.springframework.util.Assert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.base.bo.BaseBo;
import com.cwkj.scf.config.HuiShangConfig;

/**
 * 	代扣
 * @author harry
 *
 */
public class DeductBo extends BaseBo{
	
	
	/**
	 * 
	 *  I0006.REQ	请求报文							
		序号	中文名称	变量名	字段类型	最大长度	必填标志	变量格式	字段路径	备注
		HEAD.REQ	报文头							
		F1	电子账户	ACCT_NO	A	19-Jan-00	M			
		F2	绑定卡卡号	CARD_BIND	A	19-Jan-00	M	esb校验		
		F3	币种	CURRENCY	A	3-Jan-00	M	4-Jun-00		
		F4	金额	AMOUNT	A	12,2	M	"无小数点,最后一位	表示""分"""		一元填写为100
		F5	证件类型	KEYTYPE	A	2-Jan-00	M	"01 - 18位身份证	20 - 其它	25 - 企业社会信用代码"		
		F6	证件号码	IDNO	A	18-Jan-00	M			
		F7	姓名	NAME	A	29-Feb-00	M			
		F8	手机号码	MOBILE	A	12-Jan-00	M			
		F9	ESB代发实名认证标志	AUTH_FLAG	A	1-Jan-00	C	 		保留
		F10	实名认证流水号	AUTHSEQ	A	6-Jan-00	C			保留
		F11	开户银行代码	BANK_CODE	A	20-Jan-00	C			当BUSINESS_ACCOUNT_FLAG为Y时，该字段必填，
		F12	开户银行英文缩写	BANK_NAME_EN	A	20-Jan-00	O			保留
		F13	开户银行中文名称	BANK_NAME_CN	A	19-Feb-00	O			保留
		F14	开户行省份	ISS_BANK_PROVINCE	A	20-Jan-00	O			保留
		F15	开户行城市	ISS_BANK_CITY	A	19-Feb-00	O			保留
		F16	回调地址	CALL_BACK_ADDRESS	A	12-Sep-00	O			充值结果通知地址,按需使用
		F17	短信验证码	SMS_CODE	A	8-Jan-00	O			充值时短信验证,按需使用
		F18	短信序列号	SMS_SEQ	A	4-Jan-00	O			充值时短信验证,按需使用
		F19	客户ip	USR_IP	A	1-Feb-00	O			根据ESB需要填写该字段
		F20	对公账户充值标识	BUSINESS_ACCOUNT_FLAG	A	1-Jan-00	C	Y-对公账户,N-非对公账户		
		F21	手续费	FEE	A	8-Jan-00	O			
		F22	保留域	RESERVED	A	31-Mar-00	O
	 *  
	*/
	
	/**
	 *   电子账户
	 * 
	 */
	private String  ACCT_NO = HuiShangConfig.getAcctNo();
	/**
	 * 	绑定卡卡号
	 */
	
	private String CARD_BIND;
	
	/**
	 *  币种
	 */
	private String CURRENCY;
	
	/**
	 *   金额 ,无小数点,单位分
	 */
	private String AMOUNT;
	
	/**
	 * 证件类型:01 身份证
	 */
	private String KEYTYPE = "01"; 
	/**
	 * 证件号码
	 */
	private String IDNO;
	/**
	 * 姓名
	 */
	private String NAME;
	/**
	 * 手机号
	 */
	private String MOBILE;
	
	/**
	 * ESB代发实名认证标志
	 */
	private String AUTH_FLAG;
	
	/**
	 * 	实名认证流水号
	 */
	private String AUTHSEQ;
	
	/**
	 *	 当BUSINESS_ACCOUNT_FLAG为Y时，该字段必填
	 */
	private String BANK_CODE;
	/**
	 * 	银行英文名称
	 */
	private String BANK_NAME_EN;
	/**
	 * 	银行中文名称
	 */
	private String BANK_NAME_CN;
	/**
	 * 	开户行省份
	 */
	private String ISS_BANK_PROVINCE;
	/**
	 * 	开户行城市
	 */
	private String ISS_BANK_CITY;
	/**
	 * 	回调地址
	 */
	private String CALL_BACK_ADDRESS;
	/**
	 * 	短信验证码
	 */
	private String SMS_CODE;
	/**
	 * 	短信序列号
	 */
	private String SMS_SEQ;
	/**
	 * 	客户ip
	 */
	private String USR_IP;
	/**
	 * 	Y-对公账户,N-非对公账户
	 */
	private String BUSINESS_ACCOUNT_FLAG = "N";
	/**
	 * 	手续费
	 */
	private String FEE;
	/**
	 * 保留域
	 */
	private String RESERVED;
	
	@JSONField(name = "KEYTYPE")
	public String getKEYTYPE() {
		return KEYTYPE;
	}
	public void setKEYTYPE(String kEYTYPE) {
		KEYTYPE = kEYTYPE;
	}
	
	@JSONField(name = "IDNO")
	public String getIDNO() {
		return IDNO;
	}
	public void setIDNO(String iDNO) {
		IDNO = iDNO;
	}
	
	@JSONField(name = "MOBILE")
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	
	@JSONField(name = "CARD_BIND")
	public String getCARD_BIND() {
		return CARD_BIND;
	}
	public void setCARD_BIND(String cARD_BIND) {
		CARD_BIND = cARD_BIND;
	}
	@JSONField(name = "ACCT_NO")
	public String getACCT_NO() {
		return ACCT_NO;
	}
	public void setACCT_NO(String aCCT_NO) {
		ACCT_NO = aCCT_NO;
	}
	
	@JSONField(name = "CURRENCY")
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	
	@JSONField(name = "AMOUNT")
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	
	@JSONField(name = "NAME")
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	
	@JSONField(name = "AUTH_FLAG")
	public String getAUTH_FLAG() {
		return AUTH_FLAG;
	}
	public void setAUTH_FLAG(String aUTH_FLAG) {
		AUTH_FLAG = aUTH_FLAG;
	}
	
	@JSONField(name = "AUTHSEQ")
	public String getAUTHSEQ() {
		return AUTHSEQ;
	}
	public void setAUTHSEQ(String aUTHSEQ) {
		AUTHSEQ = aUTHSEQ;
	}
	
	@JSONField(name = "BANK_CODE")
	public String getBANK_CODE() {
		return BANK_CODE;
	}
	public void setBANK_CODE(String bANK_CODE) {
		BANK_CODE = bANK_CODE;
	}
	
	@JSONField(name = "BANK_NAME_EN")
	public String getBANK_NAME_EN() {
		return BANK_NAME_EN;
	}
	public void setBANK_NAME_EN(String bANK_NAME_EN) {
		BANK_NAME_EN = bANK_NAME_EN;
	}
	
	@JSONField(name = "BANK_NAME_CN")
	public String getBANK_NAME_CN() {
		return BANK_NAME_CN;
	}
	public void setBANK_NAME_CN(String bANK_NAME_CN) {
		BANK_NAME_CN = bANK_NAME_CN;
	}
	
	@JSONField(name = "ISS_BANK_PROVINCE")
	public String getISS_BANK_PROVINCE() {
		return ISS_BANK_PROVINCE;
	}
	public void setISS_BANK_PROVINCE(String iSS_BANK_PROVINCE) {
		ISS_BANK_PROVINCE = iSS_BANK_PROVINCE;
	}
	@JSONField(name = "ISS_BANK_CITY")
	public String getISS_BANK_CITY() {
		return ISS_BANK_CITY;
	}
	public void setISS_BANK_CITY(String iSS_BANK_CITY) {
		ISS_BANK_CITY = iSS_BANK_CITY;
	}
	@JSONField(name = "CALL_BACK_ADDRESS")
	public String getCALL_BACK_ADDRESS() {
		return CALL_BACK_ADDRESS;
	}
	public void setCALL_BACK_ADDRESS(String cALL_BACK_ADDRESS) {
		CALL_BACK_ADDRESS = cALL_BACK_ADDRESS;
	}
	@JSONField(name = "SMS_CODE")
	public String getSMS_CODE() {
		return SMS_CODE;
	}
	public void setSMS_CODE(String sMS_CODE) {
		SMS_CODE = sMS_CODE;
	}
	@JSONField(name = "SMS_SEQ")
	public String getSMS_SEQ() {
		return SMS_SEQ;
	}
	public void setSMS_SEQ(String sMS_SEQ) {
		SMS_SEQ = sMS_SEQ;
	}
	@JSONField(name = "USR_IP")
	public String getUSR_IP() {
		return USR_IP;
	}
	public void setUSR_IP(String uSR_IP) {
		USR_IP = uSR_IP;
	}
	@JSONField(name = "BUSINESS_ACCOUNT_FLAG")
	public String getBUSINESS_ACCOUNT_FLAG() {
		return BUSINESS_ACCOUNT_FLAG;
	}
	public void setBUSINESS_ACCOUNT_FLAG(String bUSINESS_ACCOUNT_FLAG) {
		BUSINESS_ACCOUNT_FLAG = bUSINESS_ACCOUNT_FLAG;
	}
	@JSONField(name = "FEE")
	public String getFEE() {
		return FEE;
	}
	public void setFEE(String fEE) {
		FEE = fEE;
	}
	@JSONField(name = "RESERVED")
	public String getRESERVED() {
		return RESERVED;
	}
	public void setRESERVED(String rESERVED) {
		RESERVED = rESERVED;
	}
	
	

	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.ACCT_NO, "电子账户不能为空");
		Assert.hasText(this.CARD_BIND, "绑定卡卡号不能为空");
		Assert.hasText(this.CURRENCY, "币种不能为空");
		Assert.hasText(this.AMOUNT, "金额不能为空");
		Assert.hasText(this.KEYTYPE, "KEYTYPE证件类型不能为空");
		Assert.hasText(this.IDNO, "证件号码不能为空");
		Assert.hasText(this.NAME, "姓名不能为空");
		Assert.hasText(this.MOBILE, "手机号码不能为空");
		
		if("Y".equals(this.BUSINESS_ACCOUNT_FLAG) ) {
			Assert.hasText(this.BANK_CODE, "开户银行代码不能为空");
		}
		
	}
	
	
}
