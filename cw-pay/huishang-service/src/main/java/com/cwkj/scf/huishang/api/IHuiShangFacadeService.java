package com.cwkj.scf.huishang.api;

import java.util.Map;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.service.notify.WithDrawNotifyForm;

/**
 * 	徽商对外发布的接口
 * @author harry
 *
 */
public interface IHuiShangFacadeService {

	/**
	 * 	实名认证
	 * @param form
	 * @return
	 */
	BaseResult<Map> realNameAuth(UserRealNameAuthForm form);
	/**
	 * 	签约查询
	 * @param form
	 * @return
	 */
	BaseResult<Map> signQuery(SignQueryForm form);
	/**
	 * 	签约
	 * @param form
	 * @return
	 */
	BaseResult<Map> doSign(SignDoneForm form);
	
	/**
	 * 	签约确认
	 * @param form
	 * @return
	 */
	BaseResult<Map> confirmSign(SignConfirmForm form);
	
	/**
	 * 	代扣
	 * @param form
	 * @return
	 */
	BaseResult<Map> deduct(DeductForm form);
	/**
	 *  提现\退款
	 * @param form
	 * @return
	 */
	BaseResult<Map> withdraw(WithDrawForm form);
	
	/**
	 * 代扣之前调用此方法：
	 *   这个方法会  1. 查询是否需要签约 如果不需要签约直接返回
	 *   
	 *           2. 如果需要签约，调用签约的方法， 触发第三方支付发送签约短信,返回token，并且平台要记录 token和用户及用户卡信息
	 *           
	 *           3. 但是付款页面有短信验证码这个控件，所以如果不需要签约需要平台自己发送短信
	 *           
	 * @param form
	 * @return
	 */
	BaseResult<Map> GetSignSmsCode(SignConfirmForm form);
	
	
	/**
	 * 交易状态查询【3405】
	 * @param form
	 * @return
	 */
	BaseResult<Map> queryTradeStatus(TradeQueryForm form);
	
	
	/**
	 *  平台余额查询
	 * @param form
	 * @return
	 */
	BaseResult<Map>  getRemainAmount(AccountBalanceForm form);
	
	/**
	 *  交易明细查询
	 * @param form
	 * @return
	 */
	BaseResult<Map>  queryTradeDetail(QueryTradeDetailForm form);
	
	/**
	 *  提现冲正通知
	 * @param form
	 * @return
	 */
    BaseResult<Map>  withdrawNotify(WithDrawNotifyForm form);

}
