package com.cwkj.scf.huishang.api;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 	签约确认
 * @author harry
 *
 */
public class SignConfirmForm extends SignForm {
	
	/**
	 * 
	 * 	序号	中文名称	变量名	字段类型	最大长度	必填标志	变量格式	字段路径	备注
		HEAD.REQ								
		F1	电子账号	ACCT_NO	A	18-Jan-00	M			
		F2	绑定卡号	BIND_CARD_NO	A	1-Feb-00	M			
		F3	证件号码	ID_CODE	A	18-Jan-00	M			
		F4	证件类型	ID_TYPE	A	4-Jan-00	M			
		F5	姓名	NAME	A	29-Feb-00	M			
		F6	手机号码	PHONE_NO	A	11-Jan-00	M			
		F7	短信验证码	SMS_CODE	N	6-Jan-00	M			
		F8	签约令牌	SIGN_TOKEN	A	1-Feb-00	M			前序交易3490中返回的值
		F9	保留字段	RESERVED	A	9-Apr-00	C			暂不上送值
		补充说明	
	 * 
	 * 
	 * 
	 */
	
	

	private String  SMS_CODE;
	private String  SIGN_TOKEN;
	
	@JSONField(name = "SIGN_TOKEN")
	public String getSIGN_TOKEN() {
		return SIGN_TOKEN;
	}
	public void setSIGN_TOKEN(String sIGN_TOKEN) {
		SIGN_TOKEN = sIGN_TOKEN;
	}

	@JSONField(name = "SMS_CODE")
	public String getSMS_CODE() {
		return SMS_CODE;
	}

	public void setSMS_CODE(String sMS_CODE) {
		SMS_CODE = sMS_CODE;
	}
	
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.SIGN_TOKEN, "签约令牌不能为空");
		Assert.hasText(this.SMS_CODE, "短信验证码不能为空");
	}
	
	

}
