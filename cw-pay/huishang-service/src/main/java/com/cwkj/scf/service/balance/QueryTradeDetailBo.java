package com.cwkj.scf.service.balance;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.base.bo.BaseBo;

/**
 * 	交易明细查询
 * @author harry
 *
 */
public class QueryTradeDetailBo extends BaseBo{
	
	
	/**
	 * I0002.REQ	请求报文							
		序号	中文名称	变量名	类型	最大长度	必填标志	变量格式	ESB处理内容	备注
		HEAD.REQ								
		F1	卡号	CARDNBR	A	19-Jan-00	M			
		F2	是否检查密码标志	PINFLAG	A	1-Jan-00	M			"0:不检查密码	1:检查取现密码	2:检查查询密码"
		F3	密码	PIN	A	8-Jan-00	C			ANSI98格式，详见“信息安全处理”PIN加密部分
		F4	起始日期	STARTDATE	N	8-Jan-00	C			自然日期
		F5	结束日期	ENDDATE	N	8-Jan-00	C			空
		F6	翻页标志	RTN_IND	A	1-Jan-00	C			"首次查询上送空；翻页查询上送1；"
		F7	交易日期	NX_INPD	N	8-Jan-00	C			翻页控制使用；首次查询上送空；翻页查询时上送上页返回的最后一条记录交易日期；
		F8	自然日期	NX_RELD	N	8-Jan-00	C			翻页控制使用；首次查询上送空；翻页查询时上送上页返回的最后一条记录消费日期；
		F9	交易时间	NX_INPT	N	8-Jan-00	C			翻页控制使用；首次查询上送空；翻页查询时上送上页返回的最后一条记录交易时间；
		F10	交易流水号	NX_TRNN	N	6-Jan-00	C			翻页控制使用；首次查询上送空；翻页查询时上送上页返回的最后一条记录交易号；
		F11	交易类型	TRANTYPE	A	4-Jan-00	C			"空：全部交易类型 7777：基金分红	9999：查非分红	其他值：指定交易类型"
		F12	按交易种类查询标志	TYPE_FLAG	A	1-Jan-00	C			"18域为9999时有效	1：转入交易；2：转出交易；3：转入及转出交易；"
		F13	保留域	RESERVED	A	18-Jul-00	C	
	 */
	
	
	/**
	 * 	电子账号
	 */
	
	private String CARDNBR;
	
	
	/**
	 * 	是否免密
	 */
	private String PINFLAG="0";
	
	/**
	 * 电子账户密码
	 */
	private String PIN;
	/**
	 * 起止日期
	 */
	private String STARTDATE;
	/**
	 * 结束日期
	 */
	private String ENDDATE;
	/**
	 * 翻页标志
	 */
	private String RTN_IND;
	/**
	 * 交易日期
	 */
	private String NX_INPD;
	/**
	 * 自然日期
	 */
	private String NX_RELD;
	/**
	 * 交易时间
	 */
	private String NX_INPT;
	/**
	 * 交易流水号
	 */
	private String NX_TRNN;
	/**
	 * 交易类型
	 */
	private String TRANTYPE;
	/**
	 * 按交易种类查询标志
	 */
	private String TYPE_FLAG;
	
	
	/**
	 * 保留域
	 */
	private String RESERVED;
	
	
	
	@JSONField(name = "RESERVED")
	public String getRESERVED() {
		return RESERVED;
	}
	public void setRESERVED(String rESERVED) {
		RESERVED = rESERVED;
	}
	
	@JSONField(name = "PIN")
	public String getPIN() {
		return PIN;
	}
	public void setPIN(String pIN) {
		PIN = pIN;
	}
	@JSONField(name = "CARDNBR")
	public String getCARDNBR() {
		return CARDNBR;
	}
	public void setCARDNBR(String cARDNBR) {
		CARDNBR = cARDNBR;
	}
	@JSONField(name = "PINFLAG")
	public String getPINFLAG() {
		return PINFLAG;
	}
	public void setPINFLAG(String pINFLAG) {
		PINFLAG = pINFLAG;
	}
	@JSONField(name = "STARTDATE")
	public String getSTARTDATE() {
		return STARTDATE;
	}
	public void setSTARTDATE(String sTARTDATE) {
		STARTDATE = sTARTDATE;
	}
	@JSONField(name = "ENDDATE")
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}
	@JSONField(name = "RTN_IND")
	public String getRTN_IND() {
		return RTN_IND;
	}
	public void setRTN_IND(String rTN_IND) {
		RTN_IND = rTN_IND;
	}
	@JSONField(name = "NX_INPD")
	public String getNX_INPD() {
		return NX_INPD;
	}
	public void setNX_INPD(String nX_INPD) {
		NX_INPD = nX_INPD;
	}
	@JSONField(name = "NX_RELD")
	public String getNX_RELD() {
		return NX_RELD;
	}
	public void setNX_RELD(String nX_RELD) {
		NX_RELD = nX_RELD;
	}
	@JSONField(name = "NX_INPT")
	public String getNX_INPT() {
		return NX_INPT;
	}
	public void setNX_INPT(String nX_INPT) {
		NX_INPT = nX_INPT;
	}
	@JSONField(name = "NX_TRNN")
	public String getNX_TRNN() {
		return NX_TRNN;
	}
	public void setNX_TRNN(String nX_TRNN) {
		NX_TRNN = nX_TRNN;
	}
	@JSONField(name = "TRANTYPE")
	public String getTRANTYPE() {
		return TRANTYPE;
	}
	public void setTRANTYPE(String tRANTYPE) {
		TRANTYPE = tRANTYPE;
	}
	@JSONField(name = "TYPE_FLAG")
	public String getTYPE_FLAG() {
		return TYPE_FLAG;
	}
	public void setTYPE_FLAG(String tYPE_FLAG) {
		TYPE_FLAG = tYPE_FLAG;
	}
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.CARDNBR, "电子账户不能为空");
	}
	
}
