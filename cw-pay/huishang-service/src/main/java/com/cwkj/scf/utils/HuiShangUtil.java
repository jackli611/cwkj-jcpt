package com.cwkj.scf.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.HuiShangConstants;
import com.cwkj.scf.config.HuiShangConfig;

/**
 * @author harry
 */
public class HuiShangUtil {
	
	private static Logger logger = LoggerFactory.getLogger(HuiShangUtil.class);
	
	public static String getCurrentTrxDate() {
		return FastDateFormat.getInstance("yyyyMMdd").format(new Date());		
	}
	
	public static String getCurrentTrxTime() {
		return FastDateFormat.getInstance("HHmmss").format(new Date());		
		
	}
	
	
	/**
	 *  请求参数签名
	 * @param reqMap
	 * @return
	 * @throws Exception
	 */
	public static String  sign(String jsonReqData) throws Exception{
		String sign1 = RSAUtils.MD5WithRSASign(jsonReqData.getBytes("UTF-8"),
											   RSAUtils.getSignPrivateKey4Client(HuiShangConfig.getSignPrivatePath()));
		logger.debug("请求参数{} 签名值{}",jsonReqData,sign1);
		return sign1;
	}
	
	/**
	 * 对业务操作的请求数据加密
	 * @param reqJson
	 * @param signVal
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws Exception
	 */
	public static String encrypt(String reqJson,String signVal) throws UnsupportedEncodingException, Exception {
		Map<String,String> reqDataMap = new HashMap<String,String>();
		reqDataMap.put("PARAMS", reqJson);
		reqDataMap.put("SIGN", signVal);
		String js = JSONObject.toJSONString(reqDataMap);
		String encryptionKey4Server = RSAUtils.getVerifyKey4Client(HuiShangConfig.getEncryptPath()); // 获取加密公钥字符串
		String data = RSAUtils.encryptRSAByte(js.getBytes("UTF-8"),
											  encryptionKey4Server);// 进行摘要并对摘要进行加密
		
		logger.debug("请求参数{} 加密{}",reqJson,data);
		return data;
	}
	
	

	/**
	 * 返回调用结果
	 * @param responseMap
	 */
	public static BaseResult buildResult(Map<String,Object> responseMap) {
		
		BaseResult<Map> ret = new BaseResult<Map>();
		String responseJson = (String)responseMap.get("PARAMS");
		Map<String,Object> retMap = JSON.parseObject(responseJson, Map.class);
		String  retMsg = (String)retMap.get("RETMSG");
		String  retCode = (String) retMap.get("RETCODE");
		if(HuiShangConstants.HUISHANG_RSPCODE_SUCCESS.equals(retCode)) {
			ret.setCode(BaseResult.CODE_SUCCESS);
		}else {
			ret.setCode(retCode);
		}
		ret.setMsg(retMsg);
		
		Map<String,Object> retContentMap = JSON.parseObject(responseJson, Map.class);
		ret.setData(retContentMap);
		return ret;
	}

	/**
	 * 对徽商返回的报文验签
	 * @param responseMap
	 * @return
	 * @throws Exception
	 * @throws UnsupportedEncodingException
	 */
	public static boolean checkResponseSign(Map responseMap) throws Exception, UnsupportedEncodingException {
		String responseParams = (String) responseMap.get("PARAMS");
		String responseSign = (String) responseMap.get("SIGN");
		if(null == responseParams) {
			throw new RuntimeException("验签失败，第三方没有结果返回");
		}
		
		logger.debug("responseParams:{} " , responseParams);
		boolean verifyResult = RSAUtils.MD5WithRSAVerify(responseParams.getBytes("UTF-8"),
													     RSAUtils.getVerifyKey4Client(HuiShangConfig.getSignPublicPath()),
															   responseSign);
		
		if (verifyResult==false) {
			logger.debug("返回结果{}验证签名失败...",responseMap);
		} else {
			logger.debug("返回结果{}验证签名成功...",responseMap);
		}
		return verifyResult;
	}


	/**
	 * 对徽商返回的结果解密
	 * @param jsonMap
	 * @return
	 * @throws Exception
	 */
	public static Map decryptResponse(Map jsonMap) throws Exception {
		String resultData = (String)jsonMap.get("responseData");
		if(StringUtils.isBlank(resultData)) {
			return Collections.EMPTY_MAP;
		}
		String decryptKey4Server = RSAUtils.getSignPrivateKey4Client(HuiShangConfig.getDecryptPath());
		String result = RSAUtils.decryptRSAs(resultData, decryptKey4Server);// 对请求数据解密
		Map responseMap = (Map) parseJSON2Map(result);
		logger.debug("responseMap：{}" , responseMap);
		return responseMap;
	}


	/**
	 * 构建发送给徽商的json报文
	 * @param reqMap
	 * @param data
	 * @return
	 */
	public static String buildSendJson(String bankCode,
			   						   String coinstCode, 
			   						   String encryptData) {
		// 组装请求数据
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("BANKCODE", bankCode);
		requestMap.put("COINSTCODE", coinstCode);
		requestMap.put("reqMap", encryptData);
		String packetHeader =  JSONObject.toJSONString(requestMap);
		logger.debug("报文:{}",packetHeader);
		return packetHeader;
		
	}

	


	/**
	 * json转map
	 * 
	 * @param jsonStr
	 *            源数据
	 * 
	 *            return map
	 * */
	public static Map<String, Object> parseJSON2Map(String jsonStr) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 最外层解析
		JSONObject json = JSONObject.parseObject(jsonStr);
		for (Object k : json.keySet()) {
			Object v = json.get(k);
			// 如果内层还是数组的话，继续解析
			if (v instanceof JSONArray) {
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				Iterator<Object> it = ((JSONArray) v).iterator();
				while (it.hasNext()) {
					JSONObject json2 = (JSONObject)it.next();
					list.add(parseJSON2Map(json2.toString()));
				}
				map.put(k.toString(), list);
			} else {
				map.put(k.toString(), v);
			}
		}
		return map;
	}

	public static void main(String[] args) throws Exception {
		String request = "rwr";
		// 签名
		LinkedHashMap reqMap = new LinkedHashMap();
		reqMap.put("test", request);
		String sign1 = sign(JSONObject.toJSONString(reqMap));
		System.out.println(sign1);
	}

}