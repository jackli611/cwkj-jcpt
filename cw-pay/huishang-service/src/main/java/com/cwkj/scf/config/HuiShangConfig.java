package com.cwkj.scf.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("huiShangConfig")
public class HuiShangConfig {
	
	private static String uri ;

	private static String signPrivatePath ;

	private static String signPublicPath ;

	private static String decryptPath ;

	private static String encryptPath ;
	
	private static String acctNo  ;
	private static String BANKCODE ;
	private static String coinstCode;
	private static String coinstChannel;
	private static String source;
	//充值回调地址
	private static String deductCallBackUrl;
	
	
	public static String getUri() {
		return uri;
	}

	public static String getSignPrivatePath() {
		return signPrivatePath;
	}

	public static String getSignPublicPath() {
		return signPublicPath;
	}

	public static String getDecryptPath() {
		return decryptPath;
	}

	public static String getEncryptPath() {
		return encryptPath;
	}

	public static String getAcctNo() {
		return acctNo;
	}
	public static String getCoinstChannel() {
		return coinstChannel;
	}
	public static String getSource() {
		return source;
	}
	public static String getCoinstCode() {
		return coinstCode;
	}
	public static String getBANKCODE() {
		return BANKCODE;
	}
	
	@Value("${huishang.uri}")
	public  void setUri(String uri) {
		HuiShangConfig.uri = uri;
	}

	@Value("${huishang.signPrivatePath}")
	public  void setSignPrivatePath(String signPrivatePath) {
		HuiShangConfig.signPrivatePath = signPrivatePath;
	}

	@Value("${huishang.signPublicPath}")
	public  void setSignPublicPath(String signPublicPath) {
		HuiShangConfig.signPublicPath = signPublicPath;
	}

	@Value("${huishang.decryptPath}")
	public  void setDecryptPath(String decryptPath) {
		HuiShangConfig.decryptPath = decryptPath;
	}
	@Value("${huishang.encryptPath}")
	public  void setEncryptPath(String encryptPath) {
		HuiShangConfig.encryptPath = encryptPath;
	}

	@Value("${huishang.acctNo}")
	public  void setAcctNo(String acctNo) {
		HuiShangConfig.acctNo = acctNo;
	}

	@Value("${huishang.BANKCODE}")
	public  void setBANKCODE(String bANKCODE) {
		HuiShangConfig.BANKCODE = bANKCODE;
	}
	@Value("${huishang.coinstCode}")
	public  void setCoinstCode(String coinstCode) {
		HuiShangConfig.coinstCode = coinstCode;
	}
	@Value("${huishang.coinstChannel}")
	public  void setCoinstChannel(String coinstChannel) {
		HuiShangConfig.coinstChannel = coinstChannel;
	}
	@Value("${huishang.source}")
	public  void setSource(String source) {
		HuiShangConfig.source = source;
	}

	public static String getDeductCallBackUrl() {
		return deductCallBackUrl;
	}
	
	@Value("${huishang.deductCallBackUrl}")
	public void setDeductCallBackUrl(String deductCallBackUrl) {
		HuiShangConfig.deductCallBackUrl = deductCallBackUrl;
	}
	
}
