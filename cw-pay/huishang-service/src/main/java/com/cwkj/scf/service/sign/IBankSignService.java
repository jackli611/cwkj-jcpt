package com.cwkj.scf.service.sign;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.SignConfirmForm;
import com.cwkj.scf.huishang.api.SignDoneForm;
import com.cwkj.scf.huishang.api.SignQueryForm;

public interface IBankSignService {

	/**
	 * 	签约查询
	 * @param bo
	 * @param signQuery
	 * @return
	 */
	BaseResult signQuery(SignQueryForm bo, TranCodeEnum signQuery);


	/**
	 * 	签约
	 * @param bo
	 * @param signQuery
	 * @return
	 */
	BaseResult doSign(SignDoneForm bo, TranCodeEnum signDone);


	/**
	 * 	签约确认
	 * @param bo
	 * @param signQuery
	 * @return
	 */
	BaseResult confirmSign(SignConfirmForm bo, TranCodeEnum signConfirm);

}
