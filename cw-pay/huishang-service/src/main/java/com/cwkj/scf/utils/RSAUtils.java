package com.cwkj.scf.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class RSAUtils {
	// 算法
	private static final String ALGORITHM = "RSA";
	// RSA最大加密明文大小
	private static final int MAX_ENCRYPT_BLOCK = 245;
	// RSA最大解密密文大小
	private static final int MAX_DECRYPT_BLOCK = 256;

	public static byte[] decryptBASE64(String key) throws Exception {
		return (new BASE64Decoder()).decodeBuffer(key);
	}

	public static String encryptBASE64(byte[] key) throws Exception {
		return (new BASE64Encoder()).encodeBuffer(key);
	}

	/**
	 * 根据私钥文件读取签名私钥
	 * 
	 * @param signPrivatePath
	 * @return
	 */
	public static String getSignPrivateKey4Client(String signPrivatePath) {
		StringBuffer privateBuffer = new StringBuffer();
		try {
			InputStream inputStream = new FileInputStream(signPrivatePath);
			InputStreamReader inputReader = new InputStreamReader(inputStream);
			BufferedReader bufferReader = new BufferedReader(inputReader);
			// 读取一行
			String line = "";
			while ((line = bufferReader.readLine()) != null) {
				privateBuffer.append(line);
			}
			bufferReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return privateBuffer.toString();
	}

	/**
	 * 根据公钥文件读取验签公钥
	 * 
	 * @param signPublicPath
	 * @return
	 */
	public static String getVerifyKey4Client(String signPublicPath) {
		String key = "";
		try {
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			InputStream in = new FileInputStream(signPublicPath);
			// 生成一个证书对象并使用从输入流 inStream 中读取的数据对它进行初始化。
			Certificate c = cf.generateCertificate(in);
			PublicKey publicKey = c.getPublicKey();
			key = new BASE64Encoder().encodeBuffer(publicKey.getEncoded());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return key;
	}

	// 初始化密钥
	public static SecretKey getKey(String strKey) {
		try {
			KeyGenerator _generator = KeyGenerator.getInstance("AES");
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			secureRandom.setSeed(strKey.getBytes());
			_generator.init(128, secureRandom);
			return _generator.generateKey();
		} catch (Exception e) {
			throw new RuntimeException(" 初始化密钥出现异常 ");
		}
	}

	/**
	 * 实例化公钥
	 * 
	 * @s 公钥
	 * @return
	 */
	public static PublicKey getPubKey(String s) throws Exception {
		PublicKey publicKey = null;
		java.security.spec.X509EncodedKeySpec bobPubKeySpec = new java.security.spec.X509EncodedKeySpec(
				new BASE64Decoder().decodeBuffer(s));
		// RSA对称加密算法
		java.security.KeyFactory keyFactory;
		keyFactory = java.security.KeyFactory.getInstance("RSA");
		// 取公钥匙对象
		publicKey = keyFactory.generatePublic(bobPubKeySpec);

		return publicKey;
	}

	/**
	 * 实例化私钥
	 *
	 * @s 私钥
	 * @return
	 */
	public static PrivateKey getPrivateKey(String s) throws Exception {
		PrivateKey privateKey = null;
		PKCS8EncodedKeySpec priPKCS8;
		priPKCS8 = new PKCS8EncodedKeySpec(new BASE64Decoder().decodeBuffer(s));
		KeyFactory keyf = KeyFactory.getInstance("RSA");
		privateKey = keyf.generatePrivate(priPKCS8);
		return privateKey;
	}

	/**
	 * 加密
	 *
	 * @param content
	 *            需要加密的内容
	 * @param keyPath
	 *            加密密码
	 * @return
	 */
	public static byte[] encryptByAES(String content, String keyPath) throws NoSuchAlgorithmException {

		SecretKeySpec key = null;
		try {
			SecretKey secretKey = getKey(keyPath);
			byte[] enCodeFormat = secretKey.getEncoded();
			key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");// 创建密码器
			byte[] byteContent = content.getBytes("GBK");
			cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
			byte[] result = cipher.doFinal(byteContent);
			return result; // 加密
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 解密
	 * 
	 * @param content
	 *            待解密内容
	 * @param keyPath
	 *            解密密钥
	 * @return
	 */
	public static byte[] decryptByAES(byte[] content, String keyPath) throws NoSuchAlgorithmException {

		SecretKeySpec key = null;
		try {
			SecretKey secretKey = getKey(keyPath);
			byte[] enCodeFormat = secretKey.getEncoded();
			key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");// 创建密码器
			cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
			byte[] result = cipher.doFinal(content);
			return result; // 加密
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将16进制转换为二进制
	 * 
	 * @param hexStr
	 * @return
	 */
	public static byte[] parseHexStr2Byte(String hexStr) {
		if (hexStr.length() < 1)
			return null;
		byte[] result = new byte[hexStr.length() / 2];
		for (int i = 0; i < hexStr.length() / 2; i++) {
			int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
			int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
			result[i] = (byte) (high * 16 + low);
		}
		return result;
	}

	/**
	 * 将二进制转换成16进制
	 * 
	 * @param buf
	 * @return
	 */
	public static String parseByte2HexStr(byte buf[]) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < buf.length; i++) {
			String hex = Integer.toHexString(buf[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}

	/**
	 * 根据公钥字符串加密方法
	 *
	 * @param source
	 *            源数据
	 * @param publicKey
	 *            公钥字符串
	 * @return
	 * @throws Exception
	 */
	public static String encryptRSAByte(byte[] source, String publicKey) throws Exception {
		/** 得到Cipher对象来实现对源数据的RSA加密 */
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, getPubKey(publicKey));
		int inputLen = source.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(source, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(source, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		out.close();
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(out.toByteArray());
	}

	/**
	 * 根据私钥字符串解密算法
	 *
	 * @param source
	 *            密文
	 * @param privateKey
	 *            私钥字符串
	 * @return
	 * @throws Exception
	 */
	public static String decryptRSAs(String source, String privateKey) throws Exception {
		/** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(privateKey));
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] b1 = decoder.decodeBuffer(source);
		int inputLen = b1.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(b1, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(b1, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		/** 执行解密操作 */
		out.close();
		return new String(out.toByteArray(), "UTF-8");
	}

	/**
	 * 根据私钥字符串解密算法
	 *
	 * @param source
	 *            密文
	 * @param privateKey
	 *            私钥字符串
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptRSAByte(String source, String privateKey) throws Exception {
		/** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(privateKey));
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] b1 = decoder.decodeBuffer(source);
		int inputLen = b1.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(b1, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(b1, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		/** 执行解密操作 */
		out.close();
		return out.toByteArray();
	}

	public static String MD5WithRSASign(byte[] b, String privateKey) throws Exception {
		byte[] keyBytes = decryptBASE64(privateKey);
		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PrivateKey privateKey2 = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
		Signature signature = Signature.getInstance("MD5withRSA");
		signature.initSign(privateKey2);
		signature.update(b);
		return encryptBASE64(signature.sign());
	}

	public static boolean MD5WithRSAVerify(byte[] b, String publicKey, String sign) throws Exception {
		byte[] keyBytes = decryptBASE64(publicKey);
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey2 = keyFactory.generatePublic(x509EncodedKeySpec);
		Signature signature = Signature.getInstance("MD5withRSA");
		signature.initVerify(publicKey2);
		signature.update(b);
		return signature.verify(decryptBASE64(sign));
	}

	/**
	 * 获取文件的MD5值
	 * 
	 * @param 文件名
	 * @return md5值
	 * @throws Exception
	 */
	public static byte[] getFileMD5String(File file) throws Exception {
		FileInputStream in = new FileInputStream(file);
		FileChannel ch = in.getChannel();
		MappedByteBuffer byteBuffer = ch.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
		MessageDigest messagedigest = MessageDigest.getInstance("MD5");
		messagedigest.update(byteBuffer);
		ch.close();
		in.close();
		return messagedigest.digest();
	}
}
