package com.cwkj.scf.service.withdraw;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.WithDrawForm;

/**
 * 	提现、退款
 */
public interface IWithDrawService {


    /**
     * 	提现【2501】
     *
     * @return
     */
    public BaseResult withdraw(WithDrawForm bo,TranCodeEnum tce);
}
