package com.cwkj.scf.service.huishang;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.HuiShangConstants;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.AccountBalanceForm;
import com.cwkj.scf.huishang.api.IHuiShangFacadeService;
import com.cwkj.scf.huishang.api.QueryTradeDetailForm;
import com.cwkj.scf.huishang.api.SignConfirmForm;
import com.cwkj.scf.huishang.api.SignDoneForm;
import com.cwkj.scf.huishang.api.SignQueryForm;
import com.cwkj.scf.huishang.api.TradeQueryForm;
import com.cwkj.scf.huishang.api.UserRealNameAuthForm;
import com.cwkj.scf.huishang.api.WithDrawForm;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.model.transinfo.TransInfoDo;
import com.cwkj.scf.service.account.IAccountService;
import com.cwkj.scf.service.balance.IAccountBalanceService;
import com.cwkj.scf.service.deduct.IDeductService;
import com.cwkj.scf.service.notify.IWithDrawNotifyService;
import com.cwkj.scf.service.notify.WithDrawNotifyForm;
import com.cwkj.scf.service.sign.IBankSignService;
import com.cwkj.scf.service.tranquery.ITradeQueryService;
import com.cwkj.scf.service.transinfo.HuiShangTransInfoService;
import com.cwkj.scf.service.withdraw.IWithDrawService;
import com.cwkj.scf.utils.HuiShangUtil;
import com.cwkj.scf.utils.ThirdNoUtil;


/**
 * Description: 对外接口 <br>
 * @author harry
 */
@Service("facadeService")
public class HuiShangFacadeServiceImpl implements IHuiShangFacadeService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IBankSignService  bankSignService;
    @Autowired
    private IDeductService  deductService;
    @Autowired
    private IWithDrawService	withdrawService;
    @Autowired
    private ITradeQueryService  tradeQueryService;
    @Autowired
    private HuiShangTransInfoService transinfoService;
    
    @Autowired
    private IAccountBalanceService accountBalanceService;
    @Autowired
    private IWithDrawNotifyService withdrawNotifyService;
    
    


    /**
     * 	实名认证
     */
	public BaseResult<Map> realNameAuth(UserRealNameAuthForm bo) {
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.USER_REAL_NAME_AUTH_3001.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(bo));
		BaseResult<Map> bsm = accountService.realNameAuth(bo,TranCodeEnum.USER_REAL_NAME_AUTH_3001);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.USER_REAL_NAME_AUTH_3001.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	
	}


	/**
	 * 	签约
	 */
	@Override
	public BaseResult<Map> signQuery(SignQueryForm bo) {
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.SIGN_QUERY.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(bo));
		BaseResult<Map> bsm = bankSignService.signQuery(bo,TranCodeEnum.SIGN_QUERY);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.SIGN_QUERY.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}
	@Override
	public BaseResult<Map> doSign(SignDoneForm bo) {
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.SIGN_DONE.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(bo));
		BaseResult<Map> bsm = bankSignService.doSign(bo,TranCodeEnum.SIGN_DONE);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.SIGN_DONE.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}
	@Override
	public BaseResult<Map> confirmSign(SignConfirmForm bo) {
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.SIGN_CONFIRM.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(bo));
		BaseResult<Map> bsm = bankSignService.confirmSign(bo,TranCodeEnum.SIGN_CONFIRM);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.SIGN_CONFIRM.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}
	
	/**
	 * 	代扣
	 * @param bo
	 * @return
	 */
	@Override
	public BaseResult<Map> deduct(DeductForm bo) {
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		paramMap.put("tranCode", TranCodeEnum.DEDUCT_MONEY.getCode());
		paramMap.put("orderId", bo.getOrderNo());
    	TransInfoDo tfDo = this.transinfoService.queryTransInfo(paramMap);
    	
    	BaseResult<Map> result= new BaseResult<Map>();
    	if(tfDo != null) {
    		if("SUCCESS".equals(tfDo.getResponseResult())){
    			result.setCode(BaseResult.CODE_SUCCESS);
    			Map<String,Object> data = new HashMap<String,Object>();
    			data.put("orderId", bo.getOrderNo());
    			result.setData(data);
    			return result;
    		}else {
    			result.setCode(BaseResult.CODE_FAILE);
    			result.setMsg("有一条未完成的支付");
    			return result;
    			//查询接口，徽商不能返回正确结果， 明细查询，是查询一天的交易不可，暂时不查询
    			/*
    			TradeQueryForm form = new TradeQueryForm();
    			form.setSEQNO_ORI(tfDo.getThirdLogNo());
    			form.setTRXCODE_ORI(tfDo.getTranCode());
    			form.setTRXDATE_ORI(tfDo.getRequestDate());
    			form.setTRXTIME_ORI(tfDo.getRequestTime());
    			form.setOrderNo(ThirdNoUtil.getOrderId());
    			form.setUserId(bo.getUserId());
    			form.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
    			form.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
				//查询
    			BaseResult<Map> queryResult = this.queryTradeStatus(form );
    			if(queryResult.resultSuccess()) {
    				Map<String,Object> retMap = queryResult.getData();
    				String tranStastu = (String)retMap.get("TRAN_STAT");
    				if("".equals(tranStastu)) {
    					//return;
    				}else if("".equals(tranStastu)) {
    					//return 
    				}
    			}else {
    				//return result;
    			}
    			*/
    		}
    	}
		
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.DEDUCT_MONEY.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(bo));
		BaseResult<Map> bsm = deductService.deduct(bo,TranCodeEnum.DEDUCT_MONEY);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.DEDUCT_MONEY.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}
	
	/**
	 * 	提现，退款
	 * @param bo
	 * @return
	 */
	@Override
	public BaseResult<Map> withdraw(WithDrawForm form) {
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.WITHDRAW.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(form));
		BaseResult<Map> bsm = withdrawService.withdraw(form,TranCodeEnum.WITHDRAW);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.WITHDRAW.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}
	
	
	/**
	 * 	交易状态查询【3405】
	 * @param bo
	 * @return
	 */
	@Override
	public BaseResult<Map> queryTradeStatus(TradeQueryForm form) {
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.TRADE_STATUS_QUERY.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(form));
		BaseResult<Map> bsm = tradeQueryService.queryTradeStatus(form,TranCodeEnum.TRADE_STATUS_QUERY);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.TRADE_STATUS_QUERY.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
		
	}


	@Override
	public BaseResult<Map> GetSignSmsCode(SignConfirmForm form) {
		SignQueryForm sqf = new SignQueryForm();
		BeanUtils.copyProperties(form, sqf);
		final BaseResult<Map> queryRet = this.signQuery(sqf);
		logger.info("signQuery return1:"+queryRet.getData());
		if(queryRet.resultSuccess()) {
			Map<String,String> retMap = queryRet.getData();
			String isSign = retMap.get("SIGN_NEEDED" );
			if("Y".equals(isSign)) {
				String signtoken = retMap.get("SIGN_TOKEN" );
				SignDoneForm doneSign = new SignDoneForm();
				BeanUtils.copyProperties(form, doneSign);
				doneSign.setSIGN_TOKEN(signtoken);
				BaseResult<Map> sendRet = this.doSign(doneSign );
				if(!sendRet.resultSuccess()) {
					queryRet.setMsg(sendRet.getMsg());
					queryRet.setCode(sendRet.getCode());
				}
			}
		}
		logger.info("signQuery return2:"+queryRet.getData());
		return queryRet;
	}

	@Override
	public BaseResult<Map> getRemainAmount(AccountBalanceForm form) {
		
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.REMAIN_AMOUNT.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(form));
		BaseResult<Map> bsm = accountBalanceService.getRemainAmount(form, TranCodeEnum.REMAIN_AMOUNT);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.REMAIN_AMOUNT.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}


	@Override
	public BaseResult<Map> queryTradeDetail(QueryTradeDetailForm form) {
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.QUERY_TRADE_DETAIL.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(form));
		BaseResult<Map> bsm = accountBalanceService.queryTradeDetail(form, TranCodeEnum.QUERY_TRADE_DETAIL);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.QUERY_TRADE_DETAIL.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}


	@Override
	public BaseResult<Map> withdrawNotify(WithDrawNotifyForm form) {
		
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.WITHDRAW_NOTIFY.getDesc() +"-"+HuiShangConstants.HUISHANG_IN_PARAM+":"+ JSON.toJSONString(form));
		BaseResult<Map> bsm = withdrawNotifyService.withdrawNotify(form, TranCodeEnum.WITHDRAW_NOTIFY);
		logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+TranCodeEnum.WITHDRAW_NOTIFY.getDesc() +"-"+HuiShangConstants.HUISHANG_OUT_PARAM+":"+ JSON.toJSONString(bsm));
		return bsm;
	}
	
	
}
