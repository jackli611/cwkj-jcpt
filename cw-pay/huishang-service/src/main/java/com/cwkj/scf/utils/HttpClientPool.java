package com.cwkj.scf.utils;

import java.nio.charset.CodingErrorAction;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Consts;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

public enum HttpClientPool {
	INSTANCE;
	private PoolingHttpClientConnectionManager cm;
	private RequestConfig defaultRequestConfig;
	
	private HttpClientPool(){
		cm = new PoolingHttpClientConnectionManager();
	    // 将最大连接数增加到200
	    cm.setMaxTotal(200);
	    // 将每个路由基础的连接增加到20
	    cm.setDefaultMaxPerRoute(20);
	    
	    defaultRequestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.BEST_MATCH)
                .setExpectContinueEnabled(true)
                .setStaleConnectionCheckEnabled(true)
                .setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
                .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC))
                .build();
	}
	
	public CloseableHttpClient getHttpClient(){			
		return HttpClients.custom()
	            .setConnectionManager(cm)
	            .build();
	}
	
	public CloseableHttpClient getHttpsClient() {
		HttpClientBuilder builder = HttpClients.custom();
		
		try {
			ssl(builder);
		} catch (Exception e) {
			e.printStackTrace();
		}
		builder.setDefaultCookieStore(new BasicCookieStore());
        ConnectionConfig connectionConfig = ConnectionConfig.custom()
            .setMalformedInputAction(CodingErrorAction.IGNORE)
            .setUnmappableInputAction(CodingErrorAction.IGNORE)
            .setCharset(Consts.UTF_8)
            .build();
        builder.setDefaultConnectionConfig(connectionConfig);
        
        
		return builder.build();
	}
	
	public RequestConfig getRequestConfig(int connectTimout,int socketTimeout,int connectionRequestTimeout){
		return RequestConfig.copy(defaultRequestConfig)
		 .setSocketTimeout(socketTimeout)
		 .setConnectTimeout(connectTimout)
		 .setConnectionRequestTimeout(connectionRequestTimeout)
		 .build();
	}
	
	private static void ssl(HttpClientBuilder builder) throws Exception{
		SSLContext sslcontext = null;
		javax.net.ssl.TrustManager[] trustAllCerts = null;
		X509TrustManager tm = null;
		SSLConnectionSocketFactory sslsf = null;
		
		try{
			
				sslcontext = SSLContexts.createDefault();
		        trustAllCerts = new javax.net.ssl.TrustManager[1];
		        tm = getX509TrustManager();
		        trustAllCerts[0] = tm;
		        sslcontext.init(null, trustAllCerts, null);
		        sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] {"TLSv1.1"},null, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//		        sslsf = new SSLConnectionSocketFactory(sslcontext ,SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		        builder.setSSLSocketFactory(sslsf);
			
		}
		finally{
			sslcontext = null;
			trustAllCerts = null;
			tm = null;
			sslsf = null;
		}
	}
	

	public static X509TrustManager getX509TrustManager(){
		return new X509TrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}
			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};
	}
}
