package com.cwkj.scf.service.sign;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 	签约
 * @author harry
 *
 */
public class SignDoneBo extends SignBo {
	
	/**
	 * 
	 * 序号	中文名称	变量名	字段类型	最大长度	必填标志	变量格式	字段路径	备注
		HEAD.REQ								
		F1	电子账号	ACCT_NO	A	18	M			
		F2	绑定卡号	BIND_CARD_NO	A	32	M			
		F3	证件号码	ID_CODE	A	18	M			
		F4	证件类型	ID_TYPE	A	4	M			
		F5	姓名	NAME	A	60	M			
		F6	手机号码	PHONE_NO	A	11	M			
		F7	签约令牌	SIGN_TOKEN	A	32	M			前序交易3490中返回的值
		F8	保留字段	RESERVED	A	100	C			暂不上送值
		补充说明								
	 * 
	 */
	
	

	private String  SIGN_TOKEN;
	
	
	@JSONField(name = "SIGN_TOKEN")
	public String getSIGN_TOKEN() {
		return SIGN_TOKEN;
	}
	public void setSIGN_TOKEN(String sIGN_TOKEN) {
		SIGN_TOKEN = sIGN_TOKEN;
	}
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.SIGN_TOKEN, "签约令牌不能为空");
	}

}
