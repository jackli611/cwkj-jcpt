package com.cwkj.scf.service.notify;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.config.HuiShangConfig;
import com.cwkj.scf.model.payform.BaseForm;

/**
 * 	提现冲正通知
 * @author harry
 *
 */
public class WithDrawNotifyForm extends BaseForm{
	
	
	
	/**
	 * 	合作单位编号
	 */
	
	private String COINSTCODE;
	
	/**
	 *  加密数据
	 */
	private String encryptData;
	
	
	
	@JSONField(name = "COINSTCODE")
	public String getCOINSTCODE() {
		return COINSTCODE;
	}
	public void setCOINSTCODE(String cOINSTCODE) {
		COINSTCODE = cOINSTCODE;
	}
	@JSONField(name = "encryptData")
	public String getEncryptData() {
		return encryptData;
	}
	public void setEncryptData(String encryptData) {
		this.encryptData = encryptData;
	}
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.COINSTCODE, "合作单位编号不能为空");
		Assert.hasText(this.encryptData, "加密数据不能为空");
		Assert.isTrue(this.COINSTCODE.equals(HuiShangConfig.getCoinstCode()),"无效的合作单位编号");
	}
	
}
