package com.cwkj.scf.service.tranquery;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.TradeQueryForm;

/**
 * 交易状态查询
 */
public interface ITradeQueryService {


    /**
     * 交易状态查询【3405】
     *
     * @return
     */
    public BaseResult queryTradeStatus(TradeQueryForm form,TranCodeEnum tce);
}
