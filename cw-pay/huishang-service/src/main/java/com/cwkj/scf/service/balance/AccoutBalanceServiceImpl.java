package com.cwkj.scf.service.balance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.AccountBalanceForm;
import com.cwkj.scf.huishang.api.QueryTradeDetailForm;
import com.cwkj.scf.service.huishang.BaseService;

/**
 * 	提现接口
 * @author harry
 *
 */
@Service("balanceService")
public class AccoutBalanceServiceImpl extends BaseService implements IAccountBalanceService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
   
    
    /**
     * 	余额查询【5863】
     *
     * @return
     */
    public BaseResult getRemainAmount(AccountBalanceForm form,TranCodeEnum tce){
    	
    	//请求徽商json报文
    	AccountBalanceBo destTradeBo = new AccountBalanceBo();
    	return this.doTrade(form, destTradeBo, tce);
    }


	@Override
	public BaseResult queryTradeDetail(QueryTradeDetailForm form, TranCodeEnum tce) {
		QueryTradeDetailBo destTradeBo = new QueryTradeDetailBo(); 
		return this.doTrade(form, destTradeBo, tce);
	}

    
}
