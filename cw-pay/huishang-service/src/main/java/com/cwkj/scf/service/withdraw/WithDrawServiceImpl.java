package com.cwkj.scf.service.withdraw;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.WithDrawForm;
import com.cwkj.scf.service.huishang.BaseService;

/**
 * 	提现接口
 * @author harry
 *
 */
@Service("withDrawService")
public class WithDrawServiceImpl extends BaseService implements IWithDrawService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
   
    
    /**
     * 	提现【2501】
     *
     * @return
     */
    @Override
    public BaseResult withdraw(WithDrawForm form, TranCodeEnum tce) {
    	
    	//请求徽商json报文
    	WithDrawBo destTradeBo = new WithDrawBo();
    	//元转分
    	//BigDecimal amt = new BigDecimal(form.getAMOUNT());
    	//form.setAMOUNT(amt.toString());
    	form.setAMOUNT("1");
    	return this.doTrade(form, destTradeBo, tce);
    }

    
}
