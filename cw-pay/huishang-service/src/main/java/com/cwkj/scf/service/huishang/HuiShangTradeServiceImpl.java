package com.cwkj.scf.service.huishang;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.common.HuiShangConstants;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.config.HuiShangConfig;
import com.cwkj.scf.model.payform.BaseForm;
import com.cwkj.scf.service.respcode.IThirdRespCodeService;
import com.cwkj.scf.service.transinfo.HuiShangTransInfoService;
import com.cwkj.scf.utils.HttpClientUtil;
import com.cwkj.scf.utils.HuiShangUtil;

/**
 * Description: 调用徽商公共服务
 * @author harry
 */
@Service
public class HuiShangTradeServiceImpl implements HuishangTradeService{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Autowired
    private HuiShangTransInfoService transInfoService;
    @Autowired
    private IThirdRespCodeService respCodeService;
    
    

    public BaseResult<Map> call(BaseForm bizOrigForm ,
    						   String jsonReqData,
    						   TranCodeEnum tranCode,
    						   String reqNo,
    						   String bankCode,
    						   String coinstCode) {
    	
    	BaseResult ret = new BaseResult();
    	String response = null;
    	try {
			// 签名
			String signVal = HuiShangUtil.sign(jsonReqData);
			logger.debug("签名后台的数据：reqNo:{},请求数据：{}",reqNo,jsonReqData);
			// 加密
			String data = HuiShangUtil.encrypt(jsonReqData,signVal);
			// 组装请求数据
			String sendJson = HuiShangUtil.buildSendJson(bankCode,coinstCode, data);
			//交易报文记录
			transInfoService.saveTransInfo(bizOrigForm.getUserId(), 
										   bizOrigForm.getOrderNo(),
										   bizOrigForm.getTRXDATE(),
										   bizOrigForm.getTRXTIME(),
										   reqNo,
										   tranCode.getCode(),
										   bizOrigForm.getAMOUNT(),
										   JSON.toJSONString(bizOrigForm),
										   sendJson);
			logger.debug("发送请求至：{},reqdata:{}" , HuiShangConfig.getUri(),sendJson);
			// 发送请求
			response = HttpClientUtil.requestBodyPost(HuiShangConfig.getUri(),  sendJson);
			logger.info("["+HuiShangConstants.HUISHANG_NAME+"]-"+tranCode.getDesc() +"结果>>>>"+response);
			
			Map jsonMap = JSONObject.parseObject(response,Map.class);
			// 解密
			Map responseMap = HuiShangUtil.decryptResponse(jsonMap);
			// 验签
			boolean verifyResult = HuiShangUtil.checkResponseSign(responseMap);
			
			if(verifyResult==false) {
				throw new RuntimeException("调用返回没有验证通过");
			}
			//返回调用结果
			ret = HuiShangUtil.buildResult(responseMap);
			if(StringUtil.isBlank(ret.getMsg())) {
				String msg = respCodeService.selectMsgByCode(ret.getCode());
				ret.setMsg(msg);
			}
			//交易返回报文记录
			transInfoService.updateTransInfoResponse(reqNo, 
													 JSON.toJSONString(responseMap),
													 ret.getCode(),
													 ret.getMsg(),
													 new Date());
			
		} catch (UnsupportedEncodingException e) {
			logger.error(reqNo,e);
			transInfoService.updateTransInfoError(reqNo,e.getMessage());
		} catch (Exception e) {
			logger.error(reqNo,e);
			transInfoService.updateTransInfoError(reqNo,e.getMessage());
		}finally {
			return ret;
		}
	}

}

	