package com.cwkj.scf.service.tranquery;

import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.TradeQueryForm;
import com.cwkj.scf.service.huishang.BaseService;

@Service("tradeQueryService")
public class TradeQueryServiceImpl extends BaseService implements ITradeQueryService  {

    /**
     * 交易状态查询
     * @param form
     * @param tce
     * @return
     */
	@Override
	public BaseResult queryTradeStatus(TradeQueryForm form, TranCodeEnum tce) {
		TradeQueryBo destBo = new TradeQueryBo();
		return this.doTrade(form, destBo, tce);
	}
    
}
