package com.cwkj.scf.service.tranquery;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.base.bo.BaseBo;

public class TradeQueryBo extends BaseBo {
	
	/**
	 * 序号	中文名称	变量名	字段类型	最大长度	必填标志	变量格式	字段路径	备注
		HEAD.REQ								
		F1	原交易代码	TRXCODE_ORI	A	4-Jan-00	M			"7601-电子账户充值;		2501-电子账户提现"
		F2	原交易日期	TRXDATE_ORI	A	8-Jan-00	M			取值为原交易报文头中的TRAN_DATE
		F3	原交易时间	TRXTIME_ORI	A	8-Jan-00	M			取值为原交易报文头中的TRAN_TIME
		F4	原交易流水号	SEQNO_ORI	A	20-Jan-00	M			取值为原交易报文头中的SEQNO
		F5	保留域	RESERVED	A	9-Feb-00	O	
	 * 
	 */

	
	private String TRXCODE_ORI;
	private String TRXDATE_ORI;
	private String TRXTIME_ORI;
	private String SEQNO_ORI;
	private String RESERVED;
	
	
	@JSONField(name = "TRXCODE_ORI")
	public String getTRXCODE_ORI() {
		return TRXCODE_ORI;
	}
	public void setTRXCODE_ORI(String tRXCODE_ORI) {
		TRXCODE_ORI = tRXCODE_ORI;
	}
	@JSONField(name = "TRXDATE_ORI")
	public String getTRXDATE_ORI() {
		return TRXDATE_ORI;
	}
	public void setTRXDATE_ORI(String tRXDATE_ORI) {
		TRXDATE_ORI = tRXDATE_ORI;
	}
	@JSONField(name = "TRXTIME_ORI")
	public String getTRXTIME_ORI() {
		return TRXTIME_ORI;
	}
	public void setTRXTIME_ORI(String tRXTIME_ORI) {
		TRXTIME_ORI = tRXTIME_ORI;
	}
	@JSONField(name = "SEQNO_ORI")
	public String getSEQNO_ORI() {
		return SEQNO_ORI;
	}
	public void setSEQNO_ORI(String sEQNO_ORI) {
		SEQNO_ORI = sEQNO_ORI;
	}
	@JSONField(name = "RESERVED")
	public String getRESERVED() {
		return RESERVED;
	}
	public void setRESERVED(String rESERVED) {
		RESERVED = rESERVED;
	}

	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		Assert.hasText(this.TRXCODE_ORI, "原交易类型不能为空");
		Assert.hasText(this.SEQNO_ORI, "原交易流水号不能为空");
		Assert.hasText(this.TRXDATE_ORI, "原交易日期不能为空");
		Assert.hasText(this.TRXTIME_ORI, "原交时间不能为空");
	}
	
}
