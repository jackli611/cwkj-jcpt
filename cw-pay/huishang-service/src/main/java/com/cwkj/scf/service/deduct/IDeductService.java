package com.cwkj.scf.service.deduct;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.model.payform.DeductForm;

/**
 * 	代扣
 */
public interface IDeductService {


    /**
     * 	代扣记账【3612】
     *
     * @return
     */
    public BaseResult deduct(DeductForm bo,TranCodeEnum tce);
}
