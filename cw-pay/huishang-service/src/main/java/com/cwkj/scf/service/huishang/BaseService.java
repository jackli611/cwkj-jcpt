package com.cwkj.scf.service.huishang;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.base.bo.BaseBo;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.model.payform.BaseForm;
import com.cwkj.scf.utils.HuiShangUtil;
import com.cwkj.scf.utils.ThirdNoUtil;

/**
 * 	接口实现基类
 * @author harry
 *
 */
@Component("baseService")
public  class BaseService {

	@Autowired
    private HuishangTradeService tradeService;

    
    /**
     * 	默认实现
     *
     * @return
     */
    protected BaseResult<Map> doTrade(BaseForm form, BaseBo destTradeBo,TranCodeEnum tce) {
        	//业务数据的原始数据
        	form.validate();
        	
        	//请求徽商json报文
        	BeanUtils.copyProperties(form, destTradeBo);
        	destTradeBo.setTRXCODE(tce.getCode());
        	destTradeBo.setSEQNO(ThirdNoUtil.getThirdNo()); //20长度
        	if(StringUtils.isBlank(destTradeBo.getTRXDATE())) {
        		destTradeBo.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
        	}
        	if(StringUtils.isBlank(destTradeBo.getTRXTIME())) {
        		destTradeBo.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
        	}
			//验证数据
        	destTradeBo.validate();
        	
			String jsonReqData = JSON.toJSONString(destTradeBo);
			
			return tradeService.call(form,
										   jsonReqData,
										   tce,
										   destTradeBo.getSEQNO(),
										   destTradeBo.getBANKCODE(),
										   destTradeBo.getCOINSTCODE());
    }

    
}
