package com.cwkj.scf.service.notify;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;

/**
 * 	提现冲正通知
 */
public interface IWithDrawNotifyService {

    
    /**
     * 	提现冲正通知【3446】
     *
     * @return
     */
    public BaseResult withdrawNotify(WithDrawNotifyForm form,TranCodeEnum tce);
}
