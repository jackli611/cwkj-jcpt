package com.cwkj.scf.service.account;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.UserRealNameAuthForm;

/**
 * 用户实名认证
 */
public interface IAccountService {


    /**
     * 用户实名认证【3001】
     *
     * @return
     */
    public BaseResult realNameAuth(UserRealNameAuthForm bo,TranCodeEnum tce);
}
