package com.cwkj.scf.huishang.api;

import org.springframework.util.Assert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.cwkj.scf.model.payform.BaseForm;

/**
 * 实名认证
 * @author harry
 *
 */
public class UserRealNameAuthForm extends BaseForm{
	
	
	/**
	 * 报文结构
	 *  HEAD.REQ		
		F1	证件类型	KEYTYPE
		F2	证件号码	IDNO
		F3	姓名	SURNAME
		F4	手机号	MOBILE
		F5	绑定卡号	CARD_BIND
		F6	保留域	RESERVE 
	*/
	
	/**
	 * 证件类型:01 身份证
	 */
	private String KEYTYPE = "01"; 
	/**
	 * 证件号码
	 */
	private String IDNO;
	/**
	 * 姓名
	 */
	private String SURNAME;
	/**
	 * 手机号
	 */
	private String MOBILE;
	/**
	 * CARD_BIND
	 */
	
	private String CARD_BIND;
	/**
	 * 保留域
	 */
	private String RESERVE;
	
	@JSONField(name = "KEYTYPE")
	public String getKEYTYPE() {
		return KEYTYPE;
	}
	public void setKEYTYPE(String kEYTYPE) {
		KEYTYPE = kEYTYPE;
	}
	
	@JSONField(name = "IDNO")
	public String getIDNO() {
		return IDNO;
	}
	public void setIDNO(String iDNO) {
		IDNO = iDNO;
	}
	
	@JSONField(name = "SURNAME")
	public String getSURNAME() {
		return SURNAME;
	}
	public void setSURNAME(String sURNAME) {
		SURNAME = sURNAME;
	}
	
	@JSONField(name = "MOBILE")
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	
	@JSONField(name = "CARD_BIND")
	public String getCARD_BIND() {
		return CARD_BIND;
	}
	public void setCARD_BIND(String cARD_BIND) {
		CARD_BIND = cARD_BIND;
	}
	@JSONField(name = "RESERVE")
	public String getRESERVE() {
		return RESERVE;
	}
	public void setRESERVE(String rESERVE) {
		RESERVE = rESERVE;
	}
	
	
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		super.validate();
		Assert.hasText(this.KEYTYPE, "证件类型不能为空");
		Assert.hasText(this.IDNO, "证件号码不能为空");
		Assert.hasText(this.SURNAME, "姓名不能为空");
		Assert.hasText(this.MOBILE, "手机号不能为空");
		Assert.hasText(this.CARD_BIND, "银行卡不能为空");
	}
	
	
	public static void main(String[] args) {
		UserRealNameAuthForm bo = new UserRealNameAuthForm();
		bo.setCARD_BIND("62281579534555");
		bo.setIDNO("43282319760819511X");
		bo.setKEYTYPE("1");
		bo.setMOBILE("13692177359");
		bo.setSURNAME("张运华");
		String jsonReqData = JSON.toJSONString(bo);
		System.out.println(jsonReqData);
		
		System.out.println(JSON.parseObject(jsonReqData, UserRealNameAuthForm.class));
	}

	@Override
	public String toString() {
		return "UserRealNameAuthBo [KEYTYPE=" + KEYTYPE + ", IDNO=" + IDNO + ", SURNAME=" + SURNAME + ", MOBILE="
				+ MOBILE + ", CARD_BIND=" + CARD_BIND + ", RESERVE=" + RESERVE + "]";
	}

}
