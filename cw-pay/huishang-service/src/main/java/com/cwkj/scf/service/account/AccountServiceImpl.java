package com.cwkj.scf.service.account;

import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.UserRealNameAuthForm;
import com.cwkj.scf.service.huishang.BaseService;

@Service("accountService")
public class AccountServiceImpl extends BaseService implements IAccountService  {

    /**
     * 	用户实名认证【3001】
     *
     * @return
     */
    @Override
    public BaseResult realNameAuth(UserRealNameAuthForm form,TranCodeEnum tce) {
    	UserRealNameAuthBo destTradeBo = new UserRealNameAuthBo();
    	return this.doTrade(form, destTradeBo, tce);
    }
    
}
