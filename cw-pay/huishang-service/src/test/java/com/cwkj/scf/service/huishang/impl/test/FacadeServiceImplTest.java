package com.cwkj.scf.service.huishang.impl.test;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSON;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.base.test.BaseTest;
import com.cwkj.scf.common.HuiShangConstants;
import com.cwkj.scf.common.enums.TranCodeEnum;
import com.cwkj.scf.huishang.api.IHuiShangFacadeService;
import com.cwkj.scf.huishang.api.SignConfirmForm;
import com.cwkj.scf.huishang.api.SignDoneForm;
import com.cwkj.scf.huishang.api.SignQueryForm;
import com.cwkj.scf.huishang.api.TradeQueryForm;
import com.cwkj.scf.huishang.api.UserRealNameAuthForm;
import com.cwkj.scf.huishang.api.WithDrawForm;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.model.transinfo.TransInfoDo;
import com.cwkj.scf.service.transinfo.HuiShangTransInfoService;
import com.cwkj.scf.utils.HuiShangUtil;
import com.cwkj.scf.utils.ThirdNoUtil;

/**
 * @author harry
 */
public class FacadeServiceImplTest extends BaseTest{

	@Autowired
	private IHuiShangFacadeService facadeService;
	@Autowired
	private HuiShangTransInfoService transinfoService;

	private Logger logger = Logger.getLogger(this.getClass());

	@Rollback(false)
	@Test
	public void testRealNameAuth() {
		
		UserRealNameAuthForm bo = new UserRealNameAuthForm();
		//bo.setCARD_BIND("6217241236456789015");
		bo.setCARD_BIND("6217751236456789015");
		bo.setIDNO("43282319760819511X");
		bo.setKEYTYPE("1");
		bo.setMOBILE("13692177359");
		bo.setSURNAME("张运华");
		bo.setOrderNo(ThirdNoUtil.getOrderId());
		bo.setUserId("-1");
		bo.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		bo.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		BaseResult bsm = facadeService.realNameAuth(bo );
		logger.info("bsm:"+bsm);
	}
	
	@Rollback(false)
	@Test
	public void testSignQuery() {
		
		SignQueryForm bo = new SignQueryForm();
		bo.setBIND_CARD_NO("6227007200340766924");
		bo.setID_CODE("43282319760819511X");
		bo.setNAME("张运华");
		bo.setPHONE_NO("13692177359");
		bo.setOrderNo(ThirdNoUtil.getOrderId());
		bo.setUserId("-1");
		bo.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		bo.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		BaseResult bsm = facadeService.signQuery(bo);
		logger.info("bsm:"+bsm);
	}
	
	/**
	 * 	签约
	 */
	@Rollback(false)
	@Test
	public void testDoSign() {
		
		SignDoneForm bo = new SignDoneForm();
		bo.setBIND_CARD_NO("6227007200340766924");
		bo.setID_CODE("43282319760819511X");
		bo.setNAME("张运华");
		bo.setPHONE_NO("13692177359");
		bo.setOrderNo(ThirdNoUtil.getOrderId());
		bo.setUserId("-1");
		bo.setSIGN_TOKEN("TJ");
		bo.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		bo.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		BaseResult bsm = facadeService.doSign(bo);
		logger.info("bsm:"+bsm);
	}
	
	/**
	 * 	签约确认
	 */
	@Rollback(false)
	@Test
	public void testConfirmSign() {
		
		SignConfirmForm bo = new SignConfirmForm();
		bo.setBIND_CARD_NO("6227007200340766924");
		bo.setID_CODE("43282319760819511X");
		bo.setNAME("张运华");
		bo.setPHONE_NO("13692177359");
		bo.setOrderNo(ThirdNoUtil.getOrderId());
		bo.setUserId("-1");
		bo.setSIGN_TOKEN("TJ");
		bo.setSMS_CODE("245961");
		bo.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		bo.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		BaseResult bsm = facadeService.confirmSign(bo);
		logger.info("bsm:"+bsm);
	}
	
	/**
	 * 	代扣
	 */
	@Rollback(false)
	@Test
	public void testDeduct() {
		
		DeductForm df = new DeductForm();
		df.setCARD_BIND("6227007200340766924");
		df.setAMOUNT("1");
		df.setIDNO("43282319760819511X");
		df.setNAME("张运华");
		df.setMOBILE("13692177359");
		
		df.setOrderNo(ThirdNoUtil.getOrderId());
		df.setUserId("-1");
		df.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		df.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		BaseResult bsm = facadeService.deduct(df);
		logger.info("bsm:"+bsm);
	}
	/**
	 * 	提现\退款
	 */
	@Rollback(false)
	@Test
	public void testWithdraw() {
		
		WithDrawForm bo = new WithDrawForm();
		
		bo.setCARD_BIND("6227007200340766924");
		bo.setAMOUNT("100");
		bo.setIDNO("43282319760819511X");
		bo.setSURNAME("张运华");
		bo.setMOBILE("13692177359");
		bo.setPINFLAGE("N");
		bo.setADD_COMMENT("订金");
		bo.setOrderNo(ThirdNoUtil.getOrderId());
		bo.setUserId("-1");
		bo.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		bo.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		BaseResult bsm = facadeService.withdraw(bo);
		logger.info("bsm:"+bsm);
	}
	
	
	/**
	 * 	交易状态查询
	 */
	@Rollback(false)
	@Test
	public void testTradeQuery() {
		String orderId = "2019040417550936354601";
		Map<String, Object> paramMap = new HashMap<String,Object>();
		paramMap.put("tranCode", TranCodeEnum.SIGN_CONFIRM.getCode());
		paramMap.put("orderId", orderId );
    	TransInfoDo tfDo = this.transinfoService.queryTransInfo(paramMap);
    	if(tfDo != null) {

			TradeQueryForm form = new TradeQueryForm();
			form.setSEQNO_ORI(tfDo.getThirdLogNo());
			form.setTRXCODE_ORI(tfDo.getTranCode());
			form.setTRXDATE_ORI(tfDo.getRequestDate());
			form.setTRXTIME_ORI(tfDo.getRequestTime());
			form.setOrderNo(ThirdNoUtil.getOrderId());
			form.setUserId("-1");
			form.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
			form.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
			//查询
			BaseResult<Map> queryResult = facadeService.queryTradeStatus(form );
			logger.info("queryResult:"+queryResult);
			if(HuiShangConstants.HUISHANG_RESPONSERESULT_SUCCESS.equals(queryResult.getCode())) {
				Map<String,Object> retMap = queryResult.getData();
				String tranStastu = (String) retMap.get("TRAN_STAT");
				if("".equals(tranStastu)) {
					//return;
				}else if("".equals(tranStastu)) {
					//return 
				}
			}else {
				//return result;
			}
		
    	}
	}
	
	
	
	
	/**
	 * 	提现\退款
	 */
	@Rollback(false)
	@Test
	public void testGetSignSmsCode() {
		
		SignConfirmForm bo = new SignConfirmForm();
		bo.setBIND_CARD_NO("6227007200340766924");
		bo.setID_CODE("43282319760819511X");
		bo.setNAME("张运华");
		bo.setPHONE_NO("13692177359");
		bo.setSIGN_TOKEN("TJ");
		bo.setSMS_CODE("123456");
		bo.setOrderNo(ThirdNoUtil.getOrderId());
		bo.setUserId("-1");
		bo.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		bo.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		BaseResult bsm = facadeService.GetSignSmsCode(bo);
		logger.info("bsm:"+bsm);
		
		
	}

}

	