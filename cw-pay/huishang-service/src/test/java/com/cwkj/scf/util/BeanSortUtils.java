package com.cwkj.scf.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * ClassName: SortUtils <br>
 * @author harry
 */
public class BeanSortUtils {

	public static String sort(Object model) {
		SortedMethod annotation = model.getClass().getAnnotation(SortedMethod.class);
		if(null != annotation) {
			String[] sortArray = annotation.sort();
			StringBuffer content = new StringBuffer();
			
			for(String key : sortArray){
				
				Field[] field = model.getClass().getDeclaredFields(); // 获取实体类的所有属性，返回Field数组
				Field[] superField = model.getClass().getSuperclass().getDeclaredFields();
				Field[] allField = concat(field, superField);
				try {
					for (int j = 0; j < allField.length; j++) { // 遍历所有属性
						String name = allField[j].getName(); // 获取属性的名字
						if(name.equals(key)){
							name = name.substring(0, 1).toUpperCase() + name.substring(1); // 将属性的首字符大写，方便构造get，set方法
							Method m = model.getClass().getMethod("get" + name);
							Object value = m.invoke(model); // 调用getter方法获取属性值
							value = value == null ? "" : value;
							content.append(value).append("&");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			return content.toString();
		}
		return null;
	}
	
	
	static Field[] concat(Field[] a, Field[] b) {  
		Field[] c= new Field[a.length+b.length];  
	    System.arraycopy(a, 0, c, 0, a.length);  
	    System.arraycopy(b, 0, c, a.length, b.length);  
	    return c;  
	}  
}

	