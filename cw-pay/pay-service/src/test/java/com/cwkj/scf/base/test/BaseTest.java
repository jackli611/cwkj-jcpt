package com.cwkj.scf.base.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * ClassName: BaseTest <br>
 * @author harry
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations= {"file:src/test/resources/spring/spring.xml"}) 
public class BaseTest {

	static {
		System.setProperty("catalina.home", "G:\\run_time\\tomcat\\apache-tomcat-7.0.62");
	}
	
}

	