package com.cwkj.scf.service.pay;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.huishang.api.AccountBalanceForm;
import com.cwkj.scf.huishang.api.QueryTradeDetailForm;
import com.cwkj.scf.huishang.api.SignConfirmForm;
import com.cwkj.scf.huishang.api.UserRealNameAuthForm;
import com.cwkj.scf.huishang.api.WithDrawForm;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.service.pay.wx.WxDeductForm;

public interface IPayService {
	
	BaseResult<Map> prePay(WxDeductForm deductForm);

	void wxNotify(String wxResponse);

	BaseResult<Map> realAuth(UserRealNameAuthForm form);

	BaseResult<Map> getPaySmscode(SignConfirmForm signForm);

	BaseResult<Map> checkSmsCode(SignConfirmForm signForm);

	BaseResult<Map> submitPay(DeductForm deductForm);

	/**
	 * 提现、退款
	 * @param form
	 * @return
	 */
	BaseResult<Map> withdraw(WithDrawForm form);
	
	/**
	 * 余额查询
	 * @param form
	 * @return
	 */
	BaseResult<Map> getPlatformTotalAmt(AccountBalanceForm form);
	
	/**
	 * 交易明细查询
	 * @param form
	 * @return
	 */
	BaseResult<Map> queryTradeDtl(QueryTradeDetailForm form);

}
