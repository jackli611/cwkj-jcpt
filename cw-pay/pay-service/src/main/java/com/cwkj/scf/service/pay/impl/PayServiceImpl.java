package com.cwkj.scf.service.pay.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.scf.huishang.api.AccountBalanceForm;
import com.cwkj.scf.huishang.api.IHuiShangFacadeService;
import com.cwkj.scf.huishang.api.QueryTradeDetailForm;
import com.cwkj.scf.huishang.api.SignConfirmForm;
import com.cwkj.scf.huishang.api.UserRealNameAuthForm;
import com.cwkj.scf.huishang.api.WithDrawForm;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.service.pay.IPayService;
import com.cwkj.scf.service.pay.wx.IWxPayService;
import com.cwkj.scf.service.pay.wx.WxDeductForm;

@Service("payService")
public class PayServiceImpl implements IPayService {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IHuiShangFacadeService huishangService;
	
	@Autowired
	private IWxPayService wxPayService;
	
	@Override
	public BaseResult<Map> prePay(WxDeductForm deductForm) {
		return wxPayService.prePay(deductForm);
	}

	@Override
	public void wxNotify(String wxResponse) {
		wxPayService.wxNotify(wxResponse);
	}

	@Override
	public BaseResult<Map> realAuth(UserRealNameAuthForm form) {
		return huishangService.realNameAuth(form);
	}

	@Override
	public BaseResult<Map> getPaySmscode(SignConfirmForm signForm) {
		return huishangService.GetSignSmsCode(signForm);
	}

	@Override
	public BaseResult<Map> checkSmsCode(SignConfirmForm signForm) {
		return huishangService.confirmSign(signForm);		
	}

	@Override
	public BaseResult<Map> submitPay(DeductForm deductForm) {
		return huishangService.deduct(deductForm);
		
	}
	@Override
	public BaseResult<Map> withdraw(WithDrawForm form) {
		return huishangService.withdraw(form);
		
	}
	@Override
	public BaseResult<Map> getPlatformTotalAmt(AccountBalanceForm form) {
		return huishangService.getRemainAmount(form);		
	}
	@Override
	public BaseResult<Map> queryTradeDtl(QueryTradeDetailForm form) {
		return huishangService.queryTradeDetail(form);	
	}

}
