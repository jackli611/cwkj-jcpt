package com.cwkj.scf.model.payform;

import org.springframework.util.Assert;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 	接受来自UI或调用者提交form参数  , IHuiShangFaceService接口的参数
 * 	@author harry
 *
 */
public class BaseForm implements java.io.Serializable {
	
	private String TRXDATE;
	private String TRXTIME;
	private String HEADRESERVED ;
	/**
	 *   金额 ,无小数点,单位分
	 */
	private String AMOUNT;
	
	/**
	 * 业务订单流水，会关联一个第三方请求流水号reqNo,这两个流水都会记录在日志表
	 */
	private String orderNo;
	private String userId;
	/**
	 * 业务操作识别码
	 */
	private String bizCode;
	/**
	 * 支付费用编码
	 */
	private String payFeeCode;
	/**
	 * 支付费用名称
	 */
	private String payFeeName;
	/**
	 * 第三方支付名称
	 */
	private String thirdPayName;
	
	
	@JSONField(name = "AMOUNT")
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	@JSONField(name = "TRXDATE")
	public String getTRXDATE() {
		return TRXDATE;
	}
	public void setTRXDATE(String tRXDATE) {
		TRXDATE = tRXDATE;
	}
	@JSONField(name = "TRXTIME")
	public String getTRXTIME() {
		return TRXTIME;
	}
	public void setTRXTIME(String tRXTIME) {
		TRXTIME = tRXTIME;
	}
	
	@JSONField(name = "HEADRESERVED")
	public String getHEADRESERVED() {
		return HEADRESERVED;
	}
	public void setHEADRESERVED(String hEADRESERVED) {
		HEADRESERVED = hEADRESERVED;
	}
	
	@JSONField(name = "orderNo")
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	@JSONField(name = "userId")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@JSONField(name = "bizCode")
	public String getBizCode() {
		return bizCode;
	}
	public void setBizCode(String bizCode) {
		this.bizCode = bizCode;
	}
	@JSONField(name = "payFeeCode")
	public String getPayFeeCode() {
		return payFeeCode;
	}
	public void setPayFeeCode(String payFeeCode) {
		this.payFeeCode = payFeeCode;
	}
	@JSONField(name = "payFeeName")
	public String getPayFeeName() {
		return payFeeName;
	}
	public void setPayFeeName(String payFeeName) {
		this.payFeeName = payFeeName;
	}
	@JSONField(name = "thirdPayName")
	public String getThirdPayName() {
		return thirdPayName;
	}
	public void setThirdPayName(String thirdPayName) {
		this.thirdPayName = thirdPayName;
	}
	/**
	 * 	验证数据完整性
	 */
	public void validate() {
		Assert.hasText(this.orderNo, "交易流水orderNo不能为空");
		Assert.hasText(this.userId, "交易用户id不能为空");
		Assert.hasText(this.TRXDATE, "交易日期不能为空");
		Assert.hasText(this.TRXTIME, "交易时间不能为空");
	}
	

}
