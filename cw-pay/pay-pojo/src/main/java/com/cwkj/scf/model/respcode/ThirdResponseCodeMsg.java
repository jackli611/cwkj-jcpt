package com.cwkj.scf.model.respcode;

import java.io.Serializable;
import java.util.Date;

public class ThirdResponseCodeMsg implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column third_response_code_msg.id
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column third_response_code_msg.code
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    private String code;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column third_response_code_msg.msg
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    private String msg;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column third_response_code_msg.remark
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    private String remark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column third_response_code_msg.channel
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    private String channel;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column third_response_code_msg.createtime
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    private Date createtime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column third_response_code_msg.id
     *
     * @return the value of third_response_code_msg.id
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column third_response_code_msg.id
     *
     * @param id the value for third_response_code_msg.id
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column third_response_code_msg.code
     *
     * @return the value of third_response_code_msg.code
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public String getCode() {
        return code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column third_response_code_msg.code
     *
     * @param code the value for third_response_code_msg.code
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column third_response_code_msg.msg
     *
     * @return the value of third_response_code_msg.msg
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public String getMsg() {
        return msg;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column third_response_code_msg.msg
     *
     * @param msg the value for third_response_code_msg.msg
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column third_response_code_msg.remark
     *
     * @return the value of third_response_code_msg.remark
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column third_response_code_msg.remark
     *
     * @param remark the value for third_response_code_msg.remark
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column third_response_code_msg.channel
     *
     * @return the value of third_response_code_msg.channel
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public String getChannel() {
        return channel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column third_response_code_msg.channel
     *
     * @param channel the value for third_response_code_msg.channel
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column third_response_code_msg.createtime
     *
     * @return the value of third_response_code_msg.createtime
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column third_response_code_msg.createtime
     *
     * @param createtime the value for third_response_code_msg.createtime
     *
     * @mbg.generated Thu Apr 25 17:17:21 CST 2019
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}