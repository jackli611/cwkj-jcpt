package com.cwkj.scf.model.transinfo;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.ibatis.type.Alias;


/**
 * @author harry
 */
@Alias("transInfoDo")
public class TransInfoDo {

	private Long id;
	private String thirdCustId;
    private String orderNo;
    private String thirdLogNo;
    private String orderId;//商户支付订单号:用于充值
    private String tranCode;
    private BigDecimal tranAmt;//交易金额
    private BigDecimal chargeFee;//手续费
    private BigDecimal freeamount;//优惠金额
    private BigDecimal totalAmount;//总扣费金额
    private String orig;//原始请求参数
    private String requestTime;
    private String requestDate;
    private String requestContent;
    private String rspCode;
    private String rspMsg;
    private String responseResult;
    private Date responseTime;
    private String responseContent;
    private Date createTime;
    private Date updateTime;
    private String remark;
    private String tableNumber;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getThirdCustId() {
		return thirdCustId;
	}
	public void setThirdCustId(String thirdCustId) {
		this.thirdCustId = thirdCustId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getThirdLogNo() {
		return thirdLogNo;
	}
	public void setThirdLogNo(String thirdLogNo) {
		this.thirdLogNo = thirdLogNo;
	}
	public String getTranCode() {
		return tranCode;
	}
	public void setTranCode(String tranCode) {
		this.tranCode = tranCode;
	}
	public String getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getRequestContent() {
		return requestContent;
	}
	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}
	public String getRspCode() {
		return rspCode;
	}
	public void setRspCode(String rspCode) {
		this.rspCode = rspCode;
	}
	public String getRspMsg() {
		return rspMsg;
	}
	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}
	public String getResponseResult() {
		return responseResult;
	}
	public void setResponseResult(String responseResult) {
		this.responseResult = responseResult;
	}
	public Date getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}
	public String getResponseContent() {
		return responseContent;
	}
	public void setResponseContent(String responseContent) {
		this.responseContent = responseContent;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTableNumber() {
		return tableNumber;
	}
	public void setTableNumber(String tableNumber) {
		this.tableNumber = tableNumber;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public BigDecimal getTranAmt() {
		return tranAmt;
	}
	public void setTranAmt(BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	public BigDecimal getChargeFee() {
		return chargeFee;
	}
	public void setChargeFee(BigDecimal chargeFee) {
		this.chargeFee = chargeFee;
	}
	public BigDecimal getFreeamount() {
		return freeamount;
	}
	public void setFreeamount(BigDecimal freeamount) {
		this.freeamount = freeamount;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getOrig() {
		return orig;
	}
	public void setOrig(String orig) {
		this.orig = orig;
	}
    
}

	