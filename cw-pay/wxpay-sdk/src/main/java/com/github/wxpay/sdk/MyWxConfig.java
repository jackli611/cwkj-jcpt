package com.github.wxpay.sdk;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class MyWxConfig extends WXPayConfig {
	
	private String appId;
	private String mchId;
	private String key;
	private String certFile;
	
	private String appIdMap2Secret;

	public MyWxConfig (String appId, String mchId, String key,String certFile,String appIdMap2Secret) {
		this.appId = appId;
		this.mchId = mchId;
		this.key = key;
		this.certFile = certFile;
		this.appIdMap2Secret =  appIdMap2Secret;
	}

	@Override
	String getAppID() {
		return this.appId;
	}

	@Override
	String getMchID() {
		return this.mchId;
	}

	@Override
	String getKey() {
		return this.key;
	}

	
	@Override
	String getAppIdMap2Secret() {
		return appIdMap2Secret;
	}


	@Override
	InputStream getCertStream() {
		try {
			return new FileInputStream(this.certFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	IWXPayDomain getWXPayDomain() {
		return new WXPayDomainImpl();
	}

}
