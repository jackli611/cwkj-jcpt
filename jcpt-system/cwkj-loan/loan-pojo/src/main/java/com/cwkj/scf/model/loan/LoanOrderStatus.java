package com.cwkj.scf.model.loan;

public enum LoanOrderStatus {
	
	DRAFT("DRAFT","草稿"),
	PENDING("PENDING","待审批(应付方一审)"),
	PROCESSING("PROCESSING","待审批(应付方二审)"),
	TREATY("TREATY","待资金方审批"),
	REPAYING("REPAYING","已放款"),
	REPAYED("REPAYED","已结清"),
	AUDITED("AUDITED","待放款"),
	NOPASS("NOPASS","请修改"),
	INVALID("INVALID","审批不通过");
	
	private String statusCode;
	private String statusName;
	
	private LoanOrderStatus(String statusCode , String statusName) {
		this.statusCode = statusCode;
		this.statusName = statusName;
	}


	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	
}
