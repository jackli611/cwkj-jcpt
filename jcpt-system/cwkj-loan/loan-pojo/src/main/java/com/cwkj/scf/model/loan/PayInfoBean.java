package com.cwkj.scf.model.loan;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayInfoBean implements Serializable{
	
	private String payName;
	private String payAmt;
	private Long loanid;
	/**
	 * PAY001 定金；PAY002尾款
	 */
	private String payStepCode;
	
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public String getPayAmt() {
		return payAmt;
	}
	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}
	public Long getLoanid() {
		return loanid;
	}
	public void setLoanid(Long loanid) {
		this.loanid = loanid;
	}
	public String getPayStepCode() {
		return payStepCode;
	}
	public void setPayStepCode(String payStepCode) {
		this.payStepCode = payStepCode;
	}
	
	public String getWxPayAmt() {
		return (new BigDecimal(this.payAmt)).multiply(new BigDecimal("100")).toString();
	}

}
