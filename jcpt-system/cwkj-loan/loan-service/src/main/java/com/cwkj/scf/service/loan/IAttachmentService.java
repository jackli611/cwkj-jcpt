package com.cwkj.scf.service.loan;

import com.cwkj.scf.model.loan.Attachment;

public interface IAttachmentService {
	
	 Attachment selectByPrimaryKey(Long id);

}
