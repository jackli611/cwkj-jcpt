package com.cwkj.scf.service.loan.impl;

import java.util.Date;
import java.util.Random;

import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;

public class OrderCodeUtil {

	private static Random rand = new Random(System.currentTimeMillis());
	
    public static String generateOrderCode(String productCode, String userId) {
        String date = DateUtils.format(new Date(),"yyMMdd");
        String orderCodePri = productCode +userId+ date;
        //String key = "orderCode_" + productCode + date;
        //long cacheId = localCacheService.increase(key, 1);
        int id = rand.nextInt(100);
        String code = StringUtil.leftPadZeroToLength(id, 5);
        return orderCodePri + code;
    }
}
