package com.cwkj.scf.service.loan.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cwkj.jcpt.common.file.FileSavingUtils;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.scf.dao.loan.AttachmentMapper;
import com.cwkj.scf.dao.loan.LoanorderMapper;
import com.cwkj.scf.dao.loan.OrderauditMapper;
import com.cwkj.scf.dao.loan.OrderbankMapper;
import com.cwkj.scf.dao.loan.SysUserAuthMapper;
import com.cwkj.scf.model.loan.Attachment;
import com.cwkj.scf.model.loan.AttachmentExample;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.LoanPersonExample;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.LoanorderExample;
import com.cwkj.scf.model.loan.Orderaudit;
import com.cwkj.scf.model.loan.Orderbank;
import com.cwkj.scf.model.loan.SysUserAuth;
import com.cwkj.scf.model.loan.SysUserAuthExample;
import com.cwkj.scf.service.loan.ILoanOrderService;

@Service("loanOrderService")
public class LoanOrderServiceImpl implements ILoanOrderService {
	
	@Resource
	private LoanorderMapper loanOrderMapper;
	
	@Resource
	private LoanOrderPersonService loanOrderPersonService;
	
	@Resource
	private AttachmentMapper attachmentMapper;
	
	@Resource
	private OrderbankMapper bankMapper;
	
	@Resource
	private OrderauditMapper orderauditMapper;
	
	@Resource
	private SysUserAuthMapper  sysUserAuthDao;
	
	@Override
	public Loanorder selectOrderAndAttachmentByPrimaryKey(Long loanid) {
		Loanorder order =  loanOrderMapper.selectByPrimaryKey(loanid);
		AttachmentExample example = new AttachmentExample();
		example.createCriteria().andLoanidEqualTo(loanid);
		List<Attachment> acctLst = attachmentMapper.selectByExample(example);
		if(!acctLst.isEmpty()) {
			order.setAttachments(JSON.toJSONString(acctLst));
		}
		return order;
	}
	
	@Override
	public Loanorder selectByPrimaryKey(Long loanid) {
		return loanOrderMapper.selectByPrimaryKey(loanid);
	}
	
	@Override
	public List<Loanorder> selectByExample(LoanorderExample example){
		return loanOrderMapper.selectByExample(example);
	}

	/**
	 * 分页查询
	 */
	@Override
	public PageDo<Loanorder> queryOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem) {
		 if(selectItem==null){
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<Loanorder> pageBean=new PageDo<Loanorder>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(loanOrderMapper.queryListPage(selectItem));
		return pageBean;
	}
	
	
	/**
	 * 分页查询
	 */
	@Override
	public PageDo<Loanorder> queryPersonalOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem) {
		if(selectItem==null){
			selectItem=new HashMap<String,Object>();
		}
		PageDo<Loanorder> pageBean=new PageDo<Loanorder>(pageIndex, pageSize);
		selectItem.put("page", pageBean);
		pageBean.setPage(loanOrderMapper.queryPersonalOrderListPage(selectItem));
		return pageBean;
	}

	/**
	 * 保存订单
	 */
	@Override
	public Integer updateOrder(Loanorder order) {
		
		Date currentDate = new Date();
		
		if(order.getLoanid() != null) {
			order.setUpdatetime(currentDate);
			loanOrderMapper.updateByPrimaryKeySelective(order);
		}else {
			if(StringUtils.isBlank(order.getLoanstatus())) {
				order.setLoanstatus("PENDING");
			}
			if(StringUtils.isBlank(order.getOrdercode())) {
				order.setOrdercode(OrderCodeUtil.generateOrderCode("SCF", order.getUserid().toString()));
			}
			order.setApplydate(currentDate);
			order.setCreatetime(currentDate);
			order.setUpdatetime(currentDate);
			loanOrderMapper.insertSelective(order);
		}
		//维护订单附件
		updateOrderAttachments(order);
		//维护放款账户
		updateOrderBank(order);
		
		//维护借款人信息
		updatePersonInfo(order);
		return 1;
		
	}

	private void updatePersonInfo(Loanorder order) {
		LoanPerson person = order.getPerson();
		if(null == person) {
			return;
		}
		
		person.setLoanid(order.getLoanid());
		person.setUserid(order.getUserid());
		Date currentDate = new Date();
		person.setCreatetime(currentDate );
		person.setUpdatetime(currentDate);
		
		if(person.getPersonid() != null) {
			loanOrderPersonService.updateByPrimaryKeySelective(person);
		}else {
			loanOrderPersonService.insertSelective(person);
		}
	}

	private void updateOrderBank(Loanorder order) {
		Orderbank bank = order.getBank();
		if(null == bank) {
			return;
		}
		
		bank.setLoanid(order.getLoanid());
		bank.setStatus("0");
		Date currentDate = new Date();
		bank.setCreatetime(currentDate );
		bank.setUpdatetime(currentDate);
		bank.setOrdercode(order.getOrdercode());	
	}

	/**
	 * 	维护订单附件
	 * @param order
	 */
	private void updateOrderAttachments(Loanorder order) {
		List<Attachment> attachLst = order.getAttachments();
		if(attachLst == null || attachLst.size()<1) {return;}
		
		Date currentDate = new Date();
		
		for(Attachment att : attachLst) {
			att.setLoanid(order.getLoanid());
			att.setUserid(order.getUserid());
			att.setUpdatetime(currentDate);
			att.setFiletype(FileSavingUtils.getExtName(att.getOriginalfilename()));
			if(att.getId() != null) {
				attachmentMapper.updateByPrimaryKeySelective(att);
			}else {
				att.setCreatetime(currentDate);
				attachmentMapper.insertSelective(att);				
			}
		}
		
	}

	@Override
	public Integer auditOrder(Orderaudit audit) {
		
		Date currentDate = new Date();
		Loanorder order = new Loanorder();
		order.setLoanid(audit.getLoanid());
		order.setUpdateuserid(audit.getUserid());
		order.setUpdatetime(new Date());
		
		audit.setCreatetime(currentDate);
		audit.setUpdatetime(currentDate);
		
		Loanorder checkOrder = this.selectByPrimaryKey(audit.getLoanid());
		if(checkOrder.getLoanstatus().equals("REPAYING") || checkOrder.getLoanstatus().equals("REPAYED") ) {
			throw new BusinessException("此订单不允许再审批");
		}
		
		if(Orderaudit.audit_action_unagree == (audit.getResultstatus().intValue())) {
			order.setLoanstatus("INVALID");
		}else if(Orderaudit.audit_action_back == (audit.getResultstatus().intValue())) {
			order.setLoanstatus("NOPASS");
		}else {
			getOrderAuditNextStatus(audit,checkOrder,order);
		}
		
		loanOrderMapper.updateByPrimaryKeySelective(order);
		orderauditMapper.insertSelective(audit);
		return 1;
	}
	
	
	private void getOrderAuditNextStatus(Orderaudit audit,Loanorder checkOrder,Loanorder order) {
		if("SCF".equals(checkOrder.getProdcode())) {
			auditSCFOrder(audit,checkOrder,order);
		}else if("PERSONAL_XY".equals(checkOrder.getProdcode())){
			auditPernalOrder(audit,checkOrder,order);
		}
		
	}

	private void auditSCFOrder(Orderaudit audit,Loanorder checkOrder,Loanorder order)  {
			if(audit.getStep().equals("step1") ) {
				if(!checkOrder.getLoanstatus().equals("PENDING")) {
					throw new BusinessException("无效的审批状态");
				}
				order.setLoanstatus("PROCESSING");
			}
			if(audit.getStep().equals("step2")) {
				if(!checkOrder.getLoanstatus().equals("PROCESSING")) {
					throw new BusinessException("无效的审批状态");
				}
				order.setLoanstatus("AUDITED");
			}
			if(audit.getStep().equals("step3")) {
				if(!checkOrder.getLoanstatus().equals("AUDITED")) {
					throw new BusinessException("无效的审批状态");
				}
				order.setLoanstatus("REPAYING");
			}
	}
	
	private void auditPernalOrder(Orderaudit audit,Loanorder checkOrder,Loanorder order) {

		if(audit.getStep().equals("PAY002") ) {
			order.setLoanstatus("PAY002");
		}
		if(audit.getStep().equals("TREATY") ) {
			order.setLoanstatus("TREATY");
		}
		
		if(audit.getStep().equals("BACKMONEY")) {
			if(checkOrder.getLoanstatus().equals("BACKMONEY")) {
				throw new BusinessException("订单已是退款中");
			}
			if(checkOrder.getLoanstatus().equals("BACKMONEYOVER")) {
				throw new BusinessException("订单已是退款中");
			}
			order.setLoanstatus("BACKMONEY");
		}
		
		if(audit.getStep().equals("BACKMONEYOVER")) {
			
			if(checkOrder.getLoanstatus().equals("BACKMONEYOVER")) {
				throw new BusinessException("订单已是退款中");
			}
			order.setLoanstatus("BACKMONEYOVER");
		}
	}
	
	
	@Override
	public int updateOrderStatusByKeyAndOldStatus(Long loanid, String newStatus, String oldStatus) {
		AssertUtils.isNotNull(loanid, "更新订单状态出错：无效的loanid");
		AssertUtils.isNotNull(newStatus, "更新订单状态出错：无效的newStatus");
		AssertUtils.isNotNull(oldStatus, "更新订单状态出错：无效的oldStatus");
		return loanOrderMapper.updateOrderStatusByKeyAndOldStatus(loanid, newStatus, oldStatus);
	}
	
	
	/**
	 * 
	 * 订单个人信息API
	 * 
	 */

    /**
     * 新增
     */
    public int insertOrderPersonSelective(LoanPerson record) {
    	return loanOrderPersonService.insertSelective(record);
    }

    /**
     * 查询
     */
    public List<LoanPerson> selectOrderPersonByExample(LoanPersonExample example){
    	return loanOrderPersonService.selectByExample(example);
    }

    /**
     * 根据主键查询
     */
    public LoanPerson selectOrderPersonByPrimaryKey(Long personid) {
    	return loanOrderPersonService.selectByPrimaryKey(personid);
    }

    /**
     * 根据特定条件更新
     */
    public int updateOrderPersonByExampleSelective(LoanPerson record, LoanPersonExample example) {
    	return loanOrderPersonService.updateByExampleSelective(record, example);
    }


    /**
     * 更新主键更新
     */
    public int updateOrderPersonByPrimaryKeySelective(LoanPerson record) {
    	return loanOrderPersonService.updateByPrimaryKeySelective(record);
    }

	@Override
	public SysUserAuth findRealAuth(SysUserAuthExample example) {
		List<SysUserAuth>  lst = sysUserAuthDao.selectByExample(example);
		if(lst != null && !lst.isEmpty()) {
			return lst.get(0);
		}else {
			return null;
		}
	}

	@Override
	public int updateRealAuthByUserId(SysUserAuth sysUserAuth) {
		SysUserAuthExample example = new SysUserAuthExample();
    	example.createCriteria().andUseridEqualTo(sysUserAuth.getUserid())
		.andIdnoEqualTo(sysUserAuth.getIdno())
		.andRealnameEqualTo(sysUserAuth.getRealname())
		.andMobileEqualTo(sysUserAuth.getMobile())
		.andCardnumEqualTo(sysUserAuth.getCardnum());
    	SysUserAuth sua = this.findRealAuth(example);
    	if(null == sua) {
    		sysUserAuthDao.insertSelective(sysUserAuth);
    	}else {
    		sysUserAuthDao.updateByExampleSelective(sysUserAuth, example);
    	}
    	
    	LoanPerson record = new LoanPerson();
    	record.setRealname(sysUserAuth.getRealname());
    	record.setIdno(sysUserAuth.getIdno());
    	LoanPersonExample pexa = new LoanPersonExample();
    	pexa.createCriteria().andUseridEqualTo(Long.valueOf(sysUserAuth.getUserid())).andLoanidEqualTo(sysUserAuth.getLoanId());
    	//更新订单个人信息
    	List<LoanPerson> personLst = loanOrderPersonService.selectByExample(pexa);
    	if(personLst == null || personLst.size()<1) {
    		return 0;
    	}
    	
    	record.setPersonid(personLst.get(0).getPersonid());
    	return loanOrderPersonService.updateByPrimaryKeySelective(record);
    	
	}

	@Override
	public Integer platformChangeOrderStatus(Loanorder order, Orderaudit audit) {
		loanOrderMapper.updateByPrimaryKeySelective(order);
		orderauditMapper.insertSelective(audit);
		return 1;
	}

	/**
	 * EDN 订单个人信息API
	 * 
	 */

}
