package com.cwkj.scf.service.loan;

import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.loan.OrderNavigateDo;

public interface IOrderNavigateService {

	PageDo<OrderNavigateDo> queryOrderNavigateListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

}
