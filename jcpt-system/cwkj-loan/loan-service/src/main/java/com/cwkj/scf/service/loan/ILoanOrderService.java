package com.cwkj.scf.service.loan;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.LoanPersonExample;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.LoanorderExample;
import com.cwkj.scf.model.loan.Orderaudit;
import com.cwkj.scf.model.loan.SysUserAuth;
import com.cwkj.scf.model.loan.SysUserAuthExample;

public interface ILoanOrderService {

	/**
	 * 供应链订单分页
	 * @param pageIndex
	 * @param pageSize
	 * @param selectItem
	 * @return
	 */
	PageDo<Loanorder> queryOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);
	
	/**
	 * 个贷订单分页
	 * @param pageIndex
	 * @param pageSize
	 * @param selectItem
	 * @return
	 */
	PageDo<Loanorder> queryPersonalOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	Integer updateOrder(Loanorder order);

	List<Loanorder> selectByExample(LoanorderExample example);

	Loanorder selectByPrimaryKey(Long loanid);

	/**
	 * 查询订单基本信息和附件
	 * @param loanid
	 * @return
	 */
	Loanorder selectOrderAndAttachmentByPrimaryKey(Long loanid);
	/**
	 * 订单审批
	 * @param audit
	 * @return
	 */
	Integer auditOrder(Orderaudit audit);
	
	
	/**
	 * 
	 * 订单个人信息API
	 * 
	 */
    /**
     * 新增
     */
    int insertOrderPersonSelective(LoanPerson record);

    /**
     * 查询
     */
    List<LoanPerson> selectOrderPersonByExample(LoanPersonExample example);

    /**
     * 根据主键查询
     */
    LoanPerson selectOrderPersonByPrimaryKey(Long personid);
    /**
     * 更新主键更新
     */
    int updateOrderPersonByPrimaryKeySelective(LoanPerson record);

    /**
     * 	更新订单状态
     * @param loanid
     * @param newStatus
     * @param oldStatus
     * @return
     */
	int updateOrderStatusByKeyAndOldStatus(Long loanid, String newStatus, String oldStatus);

	/**
	 * 查询实名信息
	 * @param example
	 * @return
	 */
	SysUserAuth findRealAuth(SysUserAuthExample example);

	/**
	 *  更新实名认证信息
	 * @param sysUserAuth
	 * @return
	 */
	int updateRealAuthByUserId(SysUserAuth sysUserAuth);

	/**
	 * 	平台跟单，修改订单状态
	 */
	Integer platformChangeOrderStatus(Loanorder order, Orderaudit audit);

    /**
	 * 
	 * end 订单个人信息API
	 * 
	 */
}
