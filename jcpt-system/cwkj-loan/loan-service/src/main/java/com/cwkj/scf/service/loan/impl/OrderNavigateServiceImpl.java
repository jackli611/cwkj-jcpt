package com.cwkj.scf.service.loan.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.dao.loan.IOrderNavigateDao;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.OrderNavigateDo;
import com.cwkj.scf.service.loan.IOrderNavigateService;

@Service("orderNavigateService")
public class OrderNavigateServiceImpl implements IOrderNavigateService {
	
	@Autowired
	private IOrderNavigateDao  orderNavigateDao;

	@Override
	public PageDo<OrderNavigateDo> queryOrderNavigateListPage(Long pageIndex, 
															  Integer pageSize,
															  Map<String, Object> selectItem) {
		

		 if(selectItem==null){
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<OrderNavigateDo> pageBean=new PageDo<OrderNavigateDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(orderNavigateDao.queryListPage(selectItem));
		return pageBean;
	}
	

}
