package com.cwkj.scf.service.loan.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.scf.dao.loan.AttachmentMapper;
import com.cwkj.scf.model.loan.Attachment;
import com.cwkj.scf.service.loan.IAttachmentService;

@Service("attachmentService")
public class AttachmentServiceImpl implements IAttachmentService{

	@Autowired
	AttachmentMapper attachmentMapper;
	
	@Override
	public Attachment selectByPrimaryKey(Long id) {
		return attachmentMapper.selectByPrimaryKey(id);
	}
	
	

}
