package com.cwkj.scf.service.loan.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cwkj.scf.dao.loan.LoanPersonMapper;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.LoanPersonExample;

@Service("loanOrderPersonService")
public class LoanOrderPersonService {	
	
	@Resource
	private LoanPersonMapper loanPersonMapper;
	
	

    /**
     * 新增
     */
    int insertSelective(LoanPerson record) {
    	return loanPersonMapper.insertSelective(record);
    }

    /**
     * 查询
     */
    List<LoanPerson> selectByExample(LoanPersonExample example){
    	return loanPersonMapper.selectByExample(example);
    }

    /**
     * 根据主键查询
     */
    LoanPerson selectByPrimaryKey(Long personid) {
    	return loanPersonMapper.selectByPrimaryKey(personid);
    }

    /**
     * 根据特定条件更新
     */
    int updateByExampleSelective(LoanPerson record, LoanPersonExample example) {
    	return loanPersonMapper.updateByExampleSelective(record, example);
    }


    /**
     * 更新主键更新
     */
    int updateByPrimaryKeySelective(LoanPerson record) {
    	return loanPersonMapper.updateByPrimaryKeySelective(record);
    }



    
	
}
