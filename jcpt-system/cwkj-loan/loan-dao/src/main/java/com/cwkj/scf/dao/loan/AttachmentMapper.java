package com.cwkj.scf.dao.loan;

import com.cwkj.scf.model.loan.Attachment;
import com.cwkj.scf.model.loan.AttachmentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository("attachmentMapper")
public interface AttachmentMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    long countByExample(AttachmentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int deleteByExample(AttachmentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int insert(Attachment record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int insertSelective(Attachment record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    List<Attachment> selectByExample(AttachmentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    Attachment selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int updateByExampleSelective(@Param("record") Attachment record, @Param("example") AttachmentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int updateByExample(@Param("record") Attachment record, @Param("example") AttachmentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int updateByPrimaryKeySelective(Attachment record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table attachment
     *
     * @mbg.generated Tue Jan 15 10:38:31 CST 2019
     */
    int updateByPrimaryKey(Attachment record);
}