package com.cwkj.scf.dao.loan;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cwkj.scf.model.loan.OrderNavigateDo;

@Repository("orderNavigateDao")
public interface IOrderNavigateDao {

	List<OrderNavigateDo> queryListPage(Map<String, Object> selectItem);

}
