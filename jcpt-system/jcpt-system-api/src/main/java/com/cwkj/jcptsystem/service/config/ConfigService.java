package com.cwkj.jcptsystem.service.config;

import java.io.Serializable;

import com.cwkj.jcptsystem.model.config.BaseUrlConfig;
import com.cwkj.jcptsystem.model.config.SmsConfig;


/**
 * 配置服务
 * @author ljc
 *
 */
public interface ConfigService {
	
//	
//	/**
//	 * 获取配置对象
//	 * @param obj
//	 * @return
//	 */
//	<T extends Serializable> T getConfig(Class<T> obj);
	
	/**
	 * 基础服务请求地址配置
	 * @return
	 */
	BaseUrlConfig baseUrlConfig();
	
	/**
	 * 短信配置
	 * @return
	 */
	SmsConfig smsConfig();
	
	
	/**
	 * 获取配置值
	 * @param keyName
	 * @return
	 */
	String getValue(String keyName);

}
