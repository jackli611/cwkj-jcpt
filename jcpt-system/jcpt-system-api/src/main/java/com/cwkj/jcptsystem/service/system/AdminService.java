package com.cwkj.jcptsystem.service.system;

import com.cwkj.jcptsystem.model.system.AdminDo;

/**
 * 系统权限用户管理
 * @author ljc
 *
 */
public interface AdminService {

	/**
	 * 根据用户名获取用户
	 * @param userName
	 * @return
	 */
	public AdminDo findAdminByUserName(String userName);
}
