/**
 * Project Name:jcpt-common <br>
 * File Name:CodeListDo.java <br>
 * Package Name:com.hehenian.jcpt.model.system <br>
 * @author anxymf
 * Date:2016年12月7日上午9:09:25 <br>
 * Copyright (c) 2016, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcptsystem.model.system;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;
import org.hibernate.validator.constraints.NotBlank;


/**
 * ClassName: DictSystemInfoDo <br>
 * Description: 数据字典
 * @version
 * @since JDK 1.6
 */
@Alias("dictSystemInfoDo")
public class DictSystemInfoDo implements Serializable{
	
	public static final String ISVALID_T="T";
	public static final String ISVALID_F="F";

	/** 字典ID */
	private String codeId;
	
	/** 字典类型 */
	@NotBlank(message = "字典类型不能为空.") 
	private String codeType;
	
	/** 字典值 */
	@NotBlank(message = "字典值不能为空.") 
	private String codeValue;
	
	/** 字典名称 */
	@NotBlank(message = "字典名称不能为空.") 
	private String codeName;
	
	/** 是否有效(T为有效，F为无效)  */ 
	private String isValid;
	
	/** 创建日期  */
	private Date createTime; 
	
	/**  
	 * 获取字典ID  
	 * @return codeId 字典ID  
	 */
	public String getCodeId() {
		return codeId;
	}

	/**  
	 * 设置字典ID  
	 * @param codeId 字典ID  
	 */
	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	/**  
	 * 获取字典类型  
	 * @return codeType 字典类型  
	 */
	public String getCodeType() {
		return codeType;
	}

	/**  
	 * 设置字典类型  
	 * @param codeType 字典类型  
	 */
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	/**  
	 * 获取字典值  
	 * @return codeValue 字典值  
	 */
	public String getCodeValue() {
		return codeValue;
	}

	/**  
	 * 设置字典值  
	 * @param codeValue 字典值  
	 */
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	/**  
	 * 获取字典名称  
	 * @return codeName 字典名称  
	 */
	public String getCodeName() {
		return codeName;
	}

	/**  
	 * 设置字典名称  
	 * @param codeName 字典名称  
	 */
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	/**  
	 * 获取是否有效(T为有效，F为无效)  
	 * @return isValid 是否有效(T为有效，F为无效)  
	 */
	public String getIsValid() {
		return isValid;
	}

	/**  
	 * 设置是否有效(T为有效，F为无效)  
	 * @param isValid 是否有效(T为有效，F为无效)  
	 */
	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	/**  
	 * 获取创建日期  
	 * @return createTime 创建日期  
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**  
	 * 设置创建日期  
	 * @param createTime 创建日期  
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * TODO 简单描述该方法的实现功能（可选）.
	 */
	@Override
	public String toString() {
		return "DictSystemInfoDo [codeValue=" + codeValue + ", codeName="
				+ codeName + "]";
	}
	
	
	
}

	