package com.cwkj.jcptsystem.service.system;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.alibaba.fastjson.JSONArray;

/**
 * Excel服务
 * @author ljc
 *
 */
public interface ExcelService {
	
	
	CellStyle createTitleStyle(SXSSFWorkbook workbook);

	CellStyle createHeaderStyle(SXSSFWorkbook workbook);

	CellStyle createCellStyle(SXSSFWorkbook workbook);

	void setWidth(int colWidth, SXSSFSheet sheet, Map<String, String> headMap, String[] properties, String[] headers);

	void exportExcel(String title, Map<String, String> headMap, List list, String datePattern, int colWidth,
			OutputStream out);
	
	byte [] exportExcel(String title, Map<String, String> headMap, List list, String datePattern, int colWidth);

	byte [] exportExcel(String title, Map<String, String>[] headMap, List list, String datePattern, int colWidth);

	void exportExcel(String title, Map<String, String> headMap, JSONArray jsonArray, String datePattern, int colWidth,
			OutputStream out);

}
