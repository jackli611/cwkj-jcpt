package com.cwkj.jcptsystem.model.config;

import java.io.Serializable;

/**
 * 基本服务地址
 * @author ljc
 *
 */
public class BaseUrlConfig implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 贷款端服务 */
	private String dk;
	/** 小贷服务 */
	private String xd;
	/** 定时任务服务 */
	private String schedule;
	/** 基础平台API(jcpt-api)服务地址 */
	private String jcptApi;
	/** 基础平台电子协议(jcpt-agreement)服务地址 */
	private String jcptAgreement;
	/** 贷款借款人证件获取服务地址 **/
	private String loanCert;
	
	/** 钱生花预约订单通知地址 */
	private String qshOrder;
	
	/** 彩富人生预约订单通知地址 */
	private String cfrsOrder;
	
	
	/**  
	 * 贷款端服务  
	 * @return
	 */
	public String getDk() {
		return dk;
	}
	
	/**  
	 * 贷款端服务  
	 * @param dk 贷款端服务  
	 */
	public void setDk(String dk) {
		this.dk = dk;
	}
	
	/**  
	 * 小贷服务  
	 * @return
	 */
	public String getXd() {
		return xd;
	}
	
	/**  
	 * 小贷服务  
	 * @param xd 小贷服务  
	 */
	public void setXd(String xd) {
		this.xd = xd;
	}
	
	/**  
	 * 定时任务服务  
	 * @return
	 */
	public String getSchedule() {
		return schedule;
	}
	
	/**  
	 * 定时任务服务  
	 * @param schedule 定时任务服务  
	 */
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	
	/**  
	 * 基础平台API(jcpt-api)服务地址  
	 * @return
	 */
	public String getJcptApi() {
		return jcptApi;
	}
	
	/**  
	 * 基础平台API(jcpt-api)服务地址  
	 * @param jcptApi 基础平台API(jcpt-api)服务地址  
	 */
	public void setJcptApi(String jcptApi) {
		this.jcptApi = jcptApi;
	}
	
	/**  
	 * 基础平台电子协议(jcpt-agreement)服务地址  
	 * @return
	 */
	public String getJcptAgreement() {
		return jcptAgreement;
	}
	
	/**  
	 * 基础平台电子协议(jcpt-agreement)服务地址  
	 * @param jcptAgreement 基础平台电子协议(jcpt-agreement)服务地址  
	 */
	public void setJcptAgreement(String jcptAgreement) {
		this.jcptAgreement = jcptAgreement;
	}

	/**
	 * 贷款借款人证件获取服务地址
	 * @return
	 */
	public String getLoanCert() {
		return loanCert;
	}

	/**
	 * 贷款借款人证件获取服务地址
	 * @param loanCert   贷款借款人证件获取服务地址
	 */
	public void setLoanCert(String loanCert) {
		this.loanCert = loanCert;
	}

	/**
	 * @return the 钱生花预约订单通知地址
	 */
	public String getQshOrder() {
		return qshOrder;
	}

	/**
	 * @param 钱生花预约订单通知地址 the qshOrder to set
	 */
	public void setQshOrder(String qshOrder) {
		this.qshOrder = qshOrder;
	}

	/**
	 * @return the 彩富人生预约订单通知地址
	 */
	public String getCfrsOrder() {
		return cfrsOrder;
	}

	/**
	 * @param 彩富人生预约订单通知地址 the cfrsOrder to set
	 */
	public void setCfrsOrder(String cfrsOrder) {
		this.cfrsOrder = cfrsOrder;
	}
	
	
	
}
