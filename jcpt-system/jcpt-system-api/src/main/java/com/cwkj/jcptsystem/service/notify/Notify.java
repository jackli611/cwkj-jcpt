package com.cwkj.jcptsystem.service.notify;

import java.io.Serializable;

/**
 * 通知
 * @author ljc
 * @version 1.0
 */
public abstract class Notify implements Serializable{
	
	private static final long serialVersionUID = 1L;
	/**是否异步*/
    protected boolean async;
    /** 消息发送者 */
    protected String sender;

    /** 消息接收者： 短信多个接受人之间用英文逗号分隔,邮件用英文;分隔 */
    protected String recievers;

    /**  消息内容 */
    protected Object message;
    
    /** 消息模板  */
    protected String messageTemplate;
    
    
    
	/**  
	 * 是否异步  
	 * @return
	 */
	public boolean isAsync() {
		return async;
	}

	/**  
	 * 是否异步  
	 * @param async 是否异步  
	 */
	public void setAsync(boolean async) {
		this.async = async;
	}
	
	/**  
	 * 消息发送者  
	 * @return
	 */
	public String getSender() {
		return sender;
	}

	/**  
	 * 消息发送者  
	 * @param sender 消息发送者  
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**  
	 * 消息接收者：短信多个接受人之间用英文逗号分隔邮件用英文;分隔  
	 * @return
	 */
	public String getRecievers() {
		return recievers;
	}
	
	/**  
	 * 消息接收者：短信多个接受人之间用英文逗号分隔邮件用英文;分隔  
	 * @param recievers 消息接收者 
	 */
	public void setRecievers(String recievers) {
		this.recievers = recievers;
	}

	/**  
	 * 消息内容  
	 * @return
	 */
	public Object getMessage() {
		return message;
	}
	
	/**  
	 * 消息内容  
	 * @param content 消息内容  
	 */
	public void setMessage(Object message) {
		this.message = message;
	}
	

	/**  
	 * 消息模板  
	 * @return
	 */
	public String getMessageTemplate() {
		return messageTemplate;
	}

	/**  
	 * 消息模板  
	 * @param messageTemplate 消息模板  
	 */
	public void setMessageTemplate(String messageTemplate) {
		this.messageTemplate = messageTemplate;
	}
	

	/**
	 * 抄送列表
	 * @return
	 */
	public abstract String getCcList();

	/**
	 * 主题
	 * @return
	 */
    public abstract String getSubject();
    
    /**
     * 附件
     * @return
     */
    public abstract String getFilePath();
	
    @Override
    public String toString()
    {
    	return "Notify:[async="+async+",sender="+sender+",recievers="+recievers+",message="+message+"]";
    }
    
}
