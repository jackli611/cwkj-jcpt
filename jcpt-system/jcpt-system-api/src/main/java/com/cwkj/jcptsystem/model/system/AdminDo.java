package com.cwkj.jcptsystem.model.system;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 系统用户.
 * @author ljc
 * @version  v1.0
 */
@Alias("adminDo")
public class AdminDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键. */
	private Long id;

	/** 用户名. */
	private String userName;

	/** 密码. */
	private String password;

	/** 状态 1. */
	private Integer enable;

	/** 最后登录时间. */
	private Date lastTime;

	/** 最后登录IP. */
	private String lastIP;

	/** 角色编号. */
	private Long roleId;

	/** 真实姓名. */
	private String realName;

	/** 手机号. */
	private String telphone;

	/** QQ. */
	private String qq;

	/** 邮箱. */
	private String email;

	/** 头像. */
	private String img;

	/** 是否为组长. */
	private String isLeader;

	/** 性别0女1男. */
	private Integer sex;

	/** 身份证号码. */
	private String card;

	/** 描述. */
	private String summary;

	/** 身份. */
	private Integer nativePlacePro;

	/** 城市. */
	private Integer nativePlaceCity;

	/** 联系地址. */
	private String address;

	/** 创建时间. */
	private Date addDate;

	/** 奖励提成. */
	private BigDecimal moneys;

	/** 管理员汇付客户号. */
	private String usrCustId;

	/** 商户子账户号. */
	private String subAcct;

	/** 商户子账户金额. */
	private BigDecimal subAcctMoney;

	/** 统计渠道来源. */
	private String statsource;


	/**
	 * 主键.
	 * @param id
	 * 主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 主键.
	 * @return 主键
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * 用户名.
	 * @param userName
	 * 用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 用户名.
	 * @return 用户名
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * 密码.
	 * @param password
	 * 密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 密码.
	 * @return 密码
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * 状态 1.
	 * @param enable
	 * 状态 1
	 */
	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	/**
	 * 状态 1.
	 * @return 状态 1
	 */
	public Integer getEnable() {
		return this.enable;
	}

	/**
	 * 最后登录时间.
	 * @param lastTime
	 * 最后登录时间
	 */
	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}

	/**
	 * 最后登录时间.
	 * @return 最后登录时间
	 */
	public Date getLastTime() {
		return this.lastTime;
	}

	/**
	 * 最后登录IP.
	 * @param lastIP
	 * 最后登录IP
	 */
	public void setLastIP(String lastIP) {
		this.lastIP = lastIP;
	}

	/**
	 * 最后登录IP.
	 * @return 最后登录IP
	 */
	public String getLastIP() {
		return this.lastIP;
	}

	/**
	 * 角色编号.
	 * @param roleId
	 * 角色编号
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	/**
	 * 角色编号.
	 * @return 角色编号
	 */
	public Long getRoleId() {
		return this.roleId;
	}

	/**
	 * 真实姓名.
	 * @param realName
	 * 真实姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * 真实姓名.
	 * @return 真实姓名
	 */
	public String getRealName() {
		return this.realName;
	}

	/**
	 * 手机号.
	 * @param telphone
	 * 手机号
	 */
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getTelphone() {
		return this.telphone;
	}

	/**
	 * QQ.
	 * @param qq
	 * QQ
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * QQ.
	 * @return QQ
	 */
	public String getQq() {
		return this.qq;
	}

	/**
	 * 邮箱.
	 * @param email
	 * 邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 邮箱.
	 * @return 邮箱
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * 头像.
	 * @param img
	 * 头像
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * 头像.
	 * @return 头像
	 */
	public String getImg() {
		return this.img;
	}

	/**
	 * 是否为组长.
	 * @param isLeader
	 * 是否为组长
	 */
	public void setIsLeader(String isLeader) {
		this.isLeader = isLeader;
	}

	/**
	 * 是否为组长.
	 * @return 是否为组长
	 */
	public String getIsLeader() {
		return this.isLeader;
	}

	/**
	 * 性别0女1男.
	 * @param sex
	 * 性别0女1男
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * 性别0女1男.
	 * @return 性别0女1男
	 */
	public Integer getSex() {
		return this.sex;
	}

	/**
	 * 身份证号码.
	 * @param card
	 * 身份证号码
	 */
	public void setCard(String card) {
		this.card = card;
	}

	/**
	 * 身份证号码.
	 * @return 身份证号码
	 */
	public String getCard() {
		return this.card;
	}

	/**
	 * 描述.
	 * @param summary
	 * 描述
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * 描述.
	 * @return 描述
	 */
	public String getSummary() {
		return this.summary;
	}

	/**
	 * 身份.
	 * @param nativePlacePro
	 * 身份
	 */
	public void setNativePlacePro(Integer nativePlacePro) {
		this.nativePlacePro = nativePlacePro;
	}

	/**
	 * 身份.
	 * @return 身份
	 */
	public Integer getNativePlacePro() {
		return this.nativePlacePro;
	}

	/**
	 * 城市.
	 * @param nativePlaceCity
	 * 城市
	 */
	public void setNativePlaceCity(Integer nativePlaceCity) {
		this.nativePlaceCity = nativePlaceCity;
	}

	/**
	 * 城市.
	 * @return 城市
	 */
	public Integer getNativePlaceCity() {
		return this.nativePlaceCity;
	}

	/**
	 * 联系地址.
	 * @param address
	 * 联系地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 联系地址.
	 * @return 联系地址
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * 创建时间.
	 * @param addDate
	 * 创建时间
	 */
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getAddDate() {
		return this.addDate;
	}

	/**
	 * 奖励提成.
	 * @param moneys
	 * 奖励提成
	 */
	public void setMoneys(BigDecimal moneys) {
		this.moneys = moneys;
	}

	/**
	 * 奖励提成.
	 * @return 奖励提成
	 */
	public BigDecimal getMoneys() {
		return this.moneys;
	}

	/**
	 * 管理员汇付客户号.
	 * @param usrCustId
	 * 管理员汇付客户号
	 */
	public void setUsrCustId(String usrCustId) {
		this.usrCustId = usrCustId;
	}

	/**
	 * 管理员汇付客户号.
	 * @return 管理员汇付客户号
	 */
	public String getUsrCustId() {
		return this.usrCustId;
	}

	/**
	 * 商户子账户号.
	 * @param subAcct
	 * 商户子账户号
	 */
	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}

	/**
	 * 商户子账户号.
	 * @return 商户子账户号
	 */
	public String getSubAcct() {
		return this.subAcct;
	}

	/**
	 * 商户子账户金额.
	 * @param subAcctMoney
	 * 商户子账户金额
	 */
	public void setSubAcctMoney(BigDecimal subAcctMoney) {
		this.subAcctMoney = subAcctMoney;
	}

	/**
	 * 商户子账户金额.
	 * @return 商户子账户金额
	 */
	public BigDecimal getSubAcctMoney() {
		return this.subAcctMoney;
	}

	/**
	 * 统计渠道来源.
	 * @param statsource
	 * 统计渠道来源
	 */
	public void setStatsource(String statsource) {
		this.statsource = statsource;
	}

	/**
	 * 统计渠道来源.
	 * @return 统计渠道来源
	 */
	public String getStatsource() {
		return this.statsource;
	}


	@Override
	public String toString()
	{
		return "AdminDo ["+",id="+id
+",userName="+userName
+",password="+password
+",enable="+enable
+",lastTime="+lastTime
+",lastIP="+lastIP
+",roleId="+roleId
+",realName="+realName
+",telphone="+telphone
+",qq="+qq
+",email="+email
+",img="+img
+",isLeader="+isLeader
+",sex="+sex
+",card="+card
+",summary="+summary
+",nativePlacePro="+nativePlacePro
+",nativePlaceCity="+nativePlaceCity
+",address="+address
+",addDate="+addDate
+",moneys="+moneys
+",usrCustId="+usrCustId
+",subAcct="+subAcct
+",subAcctMoney="+subAcctMoney
+",statsource="+statsource
+"]";
	}

}
