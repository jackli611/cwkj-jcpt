package com.cwkj.jcptsystem.service.notify;

import java.net.MalformedURLException;
import java.net.URL;

import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;

import com.cwkj.jcpt.util.StringUtil;

/**
 * 电子邮件通知
 * @author ljc
 * @version 1.0
 */
public class MailNotify extends Notify{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 抄送：多个接受人之间用英文逗号分隔	 */
	private String ccList;
	
	/** 邮件主题 */
	private String subject;
	
    /** 附件路径 */
    private String filePath;
	
    /** 附件名称 */
	private String fileName;

	/**  
	 * 抄送：多个接受人之间用英文逗号分隔  
	 * @return
	 */
	public String getCcList() {
		return ccList;
	}

	/**  
	 * 抄送：多个接受人之间用英文逗号分隔  
	 * @param ccList 抄送 
	 */
	public void setCcList(String ccList) {
		this.ccList = ccList;
	}

	/**  
	 * 邮件主题  
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**  
	 * 邮件主题  
	 * @param subject 邮件主题  
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	
	
	/**  
	 * 附件路径  
	 * @return
	 */
	public String getFilePath() {
		return filePath;
	}
	

	/**  
	 * 附件路径  
	 * @param filePath 附件路径  
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	

	/**  
	 * 附件名称  
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}
	

	/**  
	 * 附件名称  
	 * @param fileName 附件名称  
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	    
	public DataSource getDataSource() {
		if (StringUtil.isNotBlank(this.filePath)) {
			if (this.filePath.toLowerCase().indexOf("http") == 0 && this.filePath.toLowerCase().startsWith("http")) {
				try {
					return new URLDataSource(new URL(this.filePath));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		}
		return new FileDataSource(this.filePath);
	}
	    
    @Override
    public String toString()
    {
    	return "Notify:[async="+async+",sender="+sender+",recievers="+recievers+",message="+message+",subject="+subject+",ccList="+ccList+"]";
    }
		
}
