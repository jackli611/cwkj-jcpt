package com.cwkj.jcptsystem.service.system;


import java.io.Serializable;

import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.HtmlMenu;
import com.cwkj.jcptsystem.model.system.SysPermissionDo;
import com.cwkj.jcptsystem.service.system.MenuListHandler;

/**
* Html格式菜单处理
* @author ljc
* @version 1.0
*/
public class HtmlMenuHandler implements Serializable, MenuListHandler<HtmlMenu>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 目录序号	 */
	private Integer menuIndex=0;

	@Override
	public HtmlMenu handler(SysPermissionDo sysPer) {
		HtmlMenu sb=null;
//  String sid = SecurityUtils.getSubject().getSession().getId().toString();
		if(sysPer.getDataType()!=null )
		{
			if(sysPer.getDataType()==1){
				sb=new HtmlMenu();
				String pathStart=sysPer.getUrl().startsWith("http")?"":"..";
				
				if(StringUtil.isNotEmpty(sysPer.getIcon()))
				{
					sb.setTitleHtml("<a href=\"javascript:;\" eventdata=\""+pathStart+sysPer.getUrl()+"\"><i class=\""+sysPer.getIcon()+"\"></i><span>"+sysPer.getName()+"</span></a>");
				}else{
					sb.setTitleHtml("<a href=\"javascript:;\" eventdata=\""+pathStart+sysPer.getUrl()+"\"><span>"+sysPer.getName()+"</span></a>");
				}
			}else if( sysPer.getDataType()==3){
				sb=new HtmlMenu();
				if(menuIndex==0){
					sb.setActive(true);
				}
				menuIndex++;
				if(StringUtil.isNotEmpty(sysPer.getIcon()))
				{
					sb.setTitleHtml("<a href=\"javascript:;\"><i class=\""+sysPer.getIcon()+"\"></i><span>"+sysPer.getName()+"</span><span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a>");
				}else{
					sb.setTitleHtml("<a href=\"javascript:;\"><span>"+sysPer.getName()+"</span><span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a>");
				}
			}
		}
		
		return sb;
	}

	@Override
	public void linkRelations(HtmlMenu parent, HtmlMenu child) {
		if(child!=null && parent!=null)
		{
		parent.addChildren(child);
		}
	}

}
