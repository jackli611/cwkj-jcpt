package com.cwkj.jcptsystem.model.config;

import java.io.Serializable;

/**
 * 短信配置
 * @author ljc
 *
 */
public class SmsConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** 生效 */
	private String validate;
	/** 服务地址 */
	private String url;
	/** md5密钥 */
	private String md5key;
	/**
	 * 生效 
	 * @return validate
	 */
	public String getValidate() {
		return validate;
	}
	
	/**
	 * 生效  
	 * @param validate 生效 
	 */
	public void setValidate(String validate) {
		this.validate = validate;
	}
	
	/**
	 * 服务地址 
	 * @return url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * 服务地址  
	 * @param url 服务地址 
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * md5密钥 
	 * @return md5key
	 */
	public String getMd5key() {
		return md5key;
	}
	
	/**
	 * md5密钥  
	 * @param md5key md5密钥 
	 */
	public void setMd5key(String md5key) {
		this.md5key = md5key;
	}
	
	
}
