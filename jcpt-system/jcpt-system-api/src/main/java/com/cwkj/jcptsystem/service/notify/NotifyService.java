package com.cwkj.jcptsystem.service.notify;

/**
 * 通知服务（邮件、短信等）
 * @author ljc
 * @version 1.0
 */
public interface NotifyService { 
	
	/**
	 * 发送通知（短信、邮件等）
	 * @param notifyDo  发送对象 notify的实现类 SMSNotifyDo, MailNotifyDo
	 */
    public boolean send(Notify notify);
    
}
