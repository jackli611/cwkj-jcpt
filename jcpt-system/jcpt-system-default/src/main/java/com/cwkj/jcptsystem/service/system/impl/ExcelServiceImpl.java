package com.cwkj.jcptsystem.service.system.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcptsystem.component.system.DictSystemInfoComponent;
import com.cwkj.jcptsystem.service.system.ExcelService;

/**
 * @author ljc
 */
@Service("excelService")
public class ExcelServiceImpl implements ExcelService{
	
    public static String NO_DEFINE = "no_define";//未定义的字段
    
    public static String DEFAULT_DATE_PATTERN="yyyy年MM月dd日";//默认日期格式
    
    public static int DEFAULT_COLOUMN_WIDTH = 17;
	
	 @Resource
	    private  DictSystemInfoComponent dictSystemInfoComponent; 
	    
//	    public static ExcelServiceImpl excelUtil;
	    
//	    @PostConstruct 
//	    public void init() { 
//	    	excelUtil = this; 
//	    	excelUtil.dictSystemInfoComponent = this.dictSystemInfoComponent; 
//	    } 
	    
	    /**
	     * 
	     * createTitleStyle:设置表头样式. <br>
	     *
	     * @author anxymf
	     * Date:2017年1月10日下午4:27:16 <br>
	     * @param workbook
	     * @return
	     */
	    public  CellStyle createTitleStyle(SXSSFWorkbook workbook){
	        CellStyle titleStyle = workbook.createCellStyle();
	        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	        Font titleFont = workbook.createFont();
	        titleFont.setFontHeightInPoints((short) 20);
	        titleFont.setBoldweight((short) 700);
	        titleStyle.setFont(titleFont);
	        return titleStyle;
	    }
	    
	    /**
	     * 
	     * createHeaderStyle:设置列头样式. <br>
	     *
	     * @author anxymf
	     * Date:2017年1月10日下午4:27:31 <br>
	     * @param workbook
	     * @return
	     */
	    public  CellStyle createHeaderStyle(SXSSFWorkbook workbook){
	        CellStyle headerStyle = workbook.createCellStyle();
	        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	        headerStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
	        headerStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
	        headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	        headerStyle.setFillForegroundColor(IndexedColors.TURQUOISE.getIndex());// 设置背景色
	        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	        Font headerFont = workbook.createFont();
	        headerFont.setFontHeightInPoints((short) 12);
	        headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        headerStyle.setFont(headerFont);
	        return headerStyle;
	    }
    public  CellStyle createHeaderStyle1(SXSSFWorkbook workbook){
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        headerStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());// 设置背景色
        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        headerStyle.setFont(headerFont);
        return headerStyle;
    }
    public  CellStyle createHeaderStyle2(SXSSFWorkbook workbook){
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		//垂直
		headerStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        // 设置背景色
        headerStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        headerStyle.setFont(headerFont);
        return headerStyle;
    }
	    
	    /**
	     * 
	     * createCellStyle:设置单元格样式. <br>
	     *
	     * @author anxymf
	     * Date:2017年1月10日下午4:27:34 <br>
	     * @param workbook
	     * @return
	     */
	    public  CellStyle createCellStyle(SXSSFWorkbook workbook){
	        CellStyle cellStyle = workbook.createCellStyle();
	        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
	        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
	        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
	        cellStyle.setWrapText(true);  
	        Font cellFont = workbook.createFont();
	        cellFont.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
	        cellStyle.setFont(cellFont);
	        return cellStyle;
	    }
	    
	    /**
	     * 
	     * setWidth:设置列宽. <br>
	     *
	     * @author anxymf
	     * Date:2017年1月10日下午4:27:41 <br>
	     * @param colWidth
	     * @param sheet
	     * @param headMap
	     * @param properties
	     * @param headers
	     */
	    public  void setWidth(int colWidth,SXSSFSheet sheet,Map<String, String> headMap,String[] properties,String[] headers){
	        int minBytes = colWidth<DEFAULT_COLOUMN_WIDTH?DEFAULT_COLOUMN_WIDTH:colWidth;//至少字节数
	        int[] arrColWidth = new int[headMap.size()];
	        int ii = 0;
	        for (Iterator<String> iter = headMap.keySet().iterator(); iter
					.hasNext();) {
	            String fieldName = iter.next();
	            properties[ii] = fieldName;
	            headers[ii] = headMap.get(fieldName);
	            int bytes = headers[ii].getBytes().length;
	            arrColWidth[ii] =  bytes < minBytes ? minBytes : bytes;
	            sheet.setColumnWidth(ii,arrColWidth[ii]*256);
	            ii++;
	        } 
	    }
	    
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public  void exportExcel(String title,Map<String, String> headMap,List list,String datePattern,int colWidth, OutputStream out){
	        JSONArray ja = new JSONArray();
	        ja.addAll(list);
	    	exportExcel(title, headMap, ja, datePattern, colWidth, out);
	    }
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public  byte [] exportExcel(String title,Map<String, String> headMap,List list,String datePattern,int colWidth){
			ByteArrayOutputStream os = new ByteArrayOutputStream();
	        JSONArray ja = new JSONArray();
	        ja.addAll(list);
	    	exportExcel(title, headMap, ja, datePattern, colWidth, os);
	    	return os.toByteArray();
	    }
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public  byte [] exportExcel(String title,Map<String, String>[] headMap,List list,String datePattern,int colWidth){
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			JSONArray ja = new JSONArray();
			ja.addAll(list);
			exportExcel2(title, headMap, ja, datePattern, colWidth, os);
			return os.toByteArray();
		}

	    /**
	     * 导出Excel 2007 OOXML (.xlsx)格式
	     * @param title 标题行
	     * @param headMap 属性-列头
	     * @param jsonArray 数据集
	     * @param datePattern 日期格式，传null值则默认 年月日
	     * @param colWidth 列宽 默认 至少17个字节
	     * @param out 输出流
	     */
	    public  void exportExcel(String title,Map<String, String> headMap,JSONArray jsonArray,String datePattern,int colWidth, OutputStream out) {
	        if(datePattern==null) datePattern = DEFAULT_DATE_PATTERN;
	        
	        //字典集合
	        Map<String,Map<String,String>> codeListMap = new HashMap<String,Map<String,String>>(); 
	        //声明一个工作薄
	        SXSSFWorkbook workbook = new SXSSFWorkbook(1000);//缓存
	        workbook.setCompressTempFiles(true);
	        XSSFDataFormat format =  (XSSFDataFormat) workbook.createDataFormat();  
	        //表头样式
	        CellStyle titleStyle = createTitleStyle(workbook);
	        //列头样式
	        CellStyle headerStyle = createHeaderStyle(workbook);
	        //单元格样式
	        CellStyle cellStyle = createCellStyle(workbook);
	        //生成一个(带标题)表格
	        SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet();
	        //设置列宽
	        String[] properties = new String[headMap.size()];
	        String[] headers = new String[headMap.size()];
	        setWidth(colWidth, sheet, headMap, properties, headers);
	        // 遍历集合数据，产生数据行
	        int rowIndex = 0;
	        for (Object obj : jsonArray) {
	            if(rowIndex == 65535 || rowIndex == 0){
	                if ( rowIndex != 0 ) {
	                	sheet = (SXSSFSheet) workbook.createSheet();//如果数据超过了，则在第二页显示
	                	setWidth(colWidth, sheet, headMap, properties, headers);
	                }
	                SXSSFRow titleRow = (SXSSFRow) sheet.createRow(0);//表头 rowIndex=0
	                titleRow.createCell(0).setCellValue(title);
	                titleRow.getCell(0).setCellStyle(titleStyle);
	                sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, headMap.size() - 1));
	                SXSSFRow headerRow = (SXSSFRow) sheet.createRow(1); //列头 rowIndex =1
	                for(int i=0;i<headers.length;i++)
	                {
	                    headerRow.createCell(i).setCellValue(headers[i]);
	                    headerRow.getCell(i).setCellStyle(headerStyle);
	                }
	                rowIndex = 2;//数据内容从 rowIndex=2开始
	            }
	            JSONObject jo = (JSONObject) JSONObject.toJSON(obj);
	            SXSSFRow dataRow = (SXSSFRow) sheet.createRow(rowIndex);
	            for (int i = 0; i < properties.length; i++)
	            {
	                SXSSFCell newCell = (SXSSFCell) dataRow.createCell(i);
	                String[] props = properties[i].split(";");
	                String[] attrs = props[0].split("\\.");
	                JSONObject jsobj = null;
	                Object o = null;
	                for(int x=0;x<attrs.length;x++){
	                	if(attrs.length == 1){
	                		o = jo.get(attrs[x]);
	                	}else{
	                		if(x == 0){
	                			jsobj = (JSONObject) jo.get(attrs[x]);
	                			if(jsobj == null){
	                				break;
	                			}
	                		}else if(x == attrs.length - 1){
	                			if(jsobj == null){
	                				break;
	                			}
	                			o = jsobj.get(attrs[x]);
	                    	}else{
	                    		if(jsobj == null){
	                				break;
	                			}
	                			jsobj = (JSONObject) jsobj.get(attrs[x]);
	                		}
	                	}
	                }
//	                cellStyle.setDataFormat(format.getFormat("@"));
	                if(props.length == 1){
	                	 if(o==null) 
	                		 newCell.setCellValue("");
	                     else if(o instanceof Date) {
	                    	 newCell.setCellValue(new SimpleDateFormat(datePattern).format(o));
	                     }  else if(o instanceof Float || o instanceof Double || o instanceof BigDecimal) {
	                    	 newCell.setCellType(SXSSFCell.CELL_TYPE_NUMERIC);
	                    	 cellStyle.setDataFormat(format.getFormat("0.00")); 
	                    	 newCell.setCellValue(new BigDecimal(o.toString()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
	                     }  else {
	                    	 newCell.setCellValue(o.toString());
	                     }
	                }else{
	                	 o = String.valueOf(o);
	                	if(!codeListMap.containsKey(props[1])){
	                		codeListMap.put(props[1], dictSystemInfoComponent.queryCodeListToMap(props[1]));
	                	}
	                	Map<String, String> codeList = codeListMap.get(props[1]);
	                	newCell.setCellValue(codeList.get(o));
	                }    
	                newCell.setCellStyle(cellStyle);
	            }
	            rowIndex++; 
	        }
	        try {
	            workbook.write(out);
	            workbook.dispose();
	            out.flush();
	            out.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	public  void exportExcel2(String title,Map<String, String>[] headMap,JSONArray jsonArray,String datePattern,int colWidth, OutputStream out) {
		if(datePattern==null) datePattern = DEFAULT_DATE_PATTERN;

		//字典集合
		Map<String,Map<String,String>> codeListMap = new HashMap<String,Map<String,String>>();
		//声明一个工作薄
		SXSSFWorkbook workbook = new SXSSFWorkbook(1000);
		workbook.setCompressTempFiles(true);
		XSSFDataFormat format =  (XSSFDataFormat) workbook.createDataFormat();
		//表头样式
		CellStyle titleStyle = createTitleStyle(workbook);
		//列头样式
		CellStyle headerStyle = createHeaderStyle(workbook);
        CellStyle headerStyle1 = createHeaderStyle1(workbook);
        CellStyle headerStyle2 = createHeaderStyle2(workbook);
		//单元格样式
		CellStyle cellStyle = createCellStyle(workbook);
		//生成一个(带标题)表格
		SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet();
        Map<String,String> headMap0 = headMap[0];
        Map<String,String> headMap1 = headMap[1];
        Map<String,String> headMap2 = headMap[2];
		//设置列宽
        String[] headers0 = new String[headMap0.size()];
        String[] properties0 = new String[headMap0.size()];
        setWidth(colWidth, sheet, headMap0, properties0, headers0);
        String[] headers1 = new String[headMap1.size()];
        String[] properties1 = new String[headMap1.size()];
        setWidth(colWidth, sheet, headMap1, properties1, headers1);
		String[] headers2 = new String[headMap2.size()];
        String[] properties2 = new String[headMap2.size()];
		setWidth(colWidth, sheet, headMap2, properties2, headers2);
        SXSSFRow titleRow = (SXSSFRow) sheet.createRow(0);
        titleRow.createCell(0).setCellValue(title);
        titleRow.getCell(0).setCellStyle(titleStyle);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, headMap2.size() - 1));
        //列头 rowIndex =1
        SXSSFRow headerRow0 = (SXSSFRow) sheet.createRow(1);
        //  “0,2,0,0”  ===>  “起始行，截止行，起始列，截止列”
        // 合并单元格
        String[] headnum0 = { "1,1,1,11", "1,1,12,24"};
        for(int i=0;i<headers0.length;i++)
            {   if(i==0) {
                headerRow0.createCell(i).setCellValue("日期");
                headerRow0.getCell(i).setCellStyle(headerStyle2);
                headerRow0.createCell(i+1).setCellValue(headers0[i]);
                headerRow0.getCell(i+1).setCellStyle(headerStyle);
            }else{
                headerRow0.createCell(i+11).setCellValue(headers0[i]);
                headerRow0.getCell(i+11).setCellStyle(headerStyle);
              }
        }
        // 动态合并单元格
        for (int i = 0; i < headnum0.length; i++) {
            //sheet.autoSizeColumn(i, true);
            String[] temp = headnum0[i].split(",");
            Integer startrow = Integer.parseInt(temp[0]);
            Integer overrow = Integer.parseInt(temp[1]);
            Integer startcol = Integer.parseInt(temp[2]);
            Integer overcol = Integer.parseInt(temp[3]);
            sheet.addMergedRegion(new CellRangeAddress(startrow, overrow, startcol, overcol));
        }
        SXSSFRow headerRow1 = (SXSSFRow) sheet.createRow(2);
        // 合并单元格
        String[] headnum1= { "2,2,1,3", "2,2,4,6","2,2,7,9","2,2,10,12", "2,2,13,16","2,2,17,20","2,2,21,24"};

        for(int i=0;i<headers1.length;i++)
        {   if(i == 0){
            headerRow1.createCell(i+1).setCellValue(headers1[i]);
            headerRow1.getCell(i+1).setCellStyle(headerStyle2);
            }
            if(i>0 && i<5){
                headerRow1.createCell(3*i+1).setCellValue(headers1[i]);
                headerRow1.getCell(3*i+1).setCellStyle(headerStyle2);
            }
            if(i>4){
                headerRow1.createCell(3*i+(i-3)).setCellValue(headers1[i]);
                headerRow1.getCell(3*i+(i-3)).setCellStyle(headerStyle2);
            }

        }
        // 动态合并单元格
        for (int i = 0; i < headnum1.length; i++) {
            //sheet.autoSizeColumn(i, true);
            String[] temp = headnum1[i].split(",");
            Integer startrow = Integer.parseInt(temp[0]);
            Integer overrow = Integer.parseInt(temp[1]);
            Integer startcol = Integer.parseInt(temp[2]);
            Integer overcol = Integer.parseInt(temp[3]);
            sheet.addMergedRegion(new CellRangeAddress(startrow, overrow, startcol, overcol));
        }

        SXSSFRow headerRow2 = (SXSSFRow) sheet.createRow(3);
        for(int i=0;i<headers2.length;i++)
        {
            headerRow2.createCell(i).setCellValue(headers2[i]);
            headerRow2.getCell(i).setCellStyle(headerStyle1);
            sheet.addMergedRegion(new CellRangeAddress(1, 3, 0, 0));
        }
		// 遍历集合数据，产生数据行
		int rowIndex = 0;
		for (Object obj : jsonArray) {
			if(rowIndex == 65535 || rowIndex == 0){
				if ( rowIndex != 0 ) {
					//如果数据超过了，则在第二页显示
					sheet = (SXSSFSheet) workbook.createSheet();
					setWidth(colWidth, sheet, headMap2, properties2, headers2);
				}
				//数据内容从 rowIndex=2开始
				rowIndex = 4;
			}
			JSONObject jo = (JSONObject) JSONObject.toJSON(obj);
			SXSSFRow dataRow = (SXSSFRow) sheet.createRow(rowIndex);
			for (int i = 0; i < properties2.length; i++)
			{
				SXSSFCell newCell = (SXSSFCell) dataRow.createCell(i);
				String[] props = properties2[i].split(";");
				String[] attrs = props[0].split("\\.");
				JSONObject jsobj = null;
				Object o = null;
				for(int x=0;x<attrs.length;x++){
					if(attrs.length == 1){
						o = jo.get(attrs[x]);
					}else{
						if(x == 0){
							jsobj = (JSONObject) jo.get(attrs[x]);
							if(jsobj == null){
								break;
							}
						}else if(x == attrs.length - 1){
							if(jsobj == null){
								break;
							}
							o = jsobj.get(attrs[x]);
						}else{
							if(jsobj == null){
								break;
							}
							jsobj = (JSONObject) jsobj.get(attrs[x]);
						}
					}
				}
//	                cellStyle.setDataFormat(format.getFormat("@"));
				if(props.length == 1){
					if(o==null)
						newCell.setCellValue("");
					else if(o instanceof Date) {
						newCell.setCellValue(new SimpleDateFormat(datePattern).format(o));
					}  else if(o instanceof Float || o instanceof Double || o instanceof BigDecimal) {
						newCell.setCellType(SXSSFCell.CELL_TYPE_NUMERIC);
						cellStyle.setDataFormat(format.getFormat("0.00"));
						newCell.setCellValue(new BigDecimal(o.toString()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
					}  else {
						newCell.setCellValue(o.toString());
					}
				}else{
					o = String.valueOf(o);
					if(!codeListMap.containsKey(props[1])){
						codeListMap.put(props[1], dictSystemInfoComponent.queryCodeListToMap(props[1]));
					}
					Map<String, String> codeList = codeListMap.get(props[1]);
					newCell.setCellValue(codeList.get(o));
				}
				newCell.setCellStyle(cellStyle);
			}
			rowIndex++;
		}
		try {
			workbook.write(out);
			workbook.dispose();
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	   
}
