package com.cwkj.jcptsystem.component.system.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.component.system.SysNotificationComponent;
import com.cwkj.jcptsystem.dao.system.SysNotificationDao;
import com.cwkj.jcptsystem.model.system.SysNotificationDo;

/**
 * 系统通知.
 * @author ljc
 * @version  v1.0
 */
@Component("sysNotificationComponent")
public class SysNotificationComponentImpl  implements SysNotificationComponent{
	
	private SysNotificationDao sysNotificationDao;
	
	@Autowired
	public void setSysNotificationDao(SysNotificationDao sysNotificationDao)
	{
		this.sysNotificationDao=sysNotificationDao;
	}
	
	
	@Override
	public Integer insertSysNotification(SysNotificationDo sysNotification)
	{
		return sysNotificationDao.insert(sysNotification);
	}
	
	@Override
	public List<SysNotificationDo> querySysNotificationList(Map<String,Object> selectItem)
	{
		return sysNotificationDao.queryList(selectItem);
	}

	@Override
	public PageDo<SysNotificationDo> querySysNotificationListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<SysNotificationDo> pageBean=new PageDo<SysNotificationDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(sysNotificationDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateSysNotificationById(SysNotificationDo sysNotification)
	{
		return sysNotificationDao.updateById(sysNotification);
	}

	@Override
	public Integer deleteSysNotificationById(String id)
	{
		return sysNotificationDao.deleteById(id);
	}

	@Override
	public SysNotificationDo findSysNotificationById(String id)
	{
		return sysNotificationDao.findById(id);
	}	
}
