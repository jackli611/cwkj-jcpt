package com.cwkj.jcptsystem.service.notify.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcptsystem.service.notify.MailNotify;
import com.cwkj.jcptsystem.service.notify.Notify;
import com.cwkj.jcptsystem.service.notify.NotifyService;

/**
 * @author ljc
 * @version 1.0
 */
@Service("notifyService")
public class NotifyServiceImpl implements NotifyService{
	
	@Autowired
	private NotifyService mailNotifyService;

	@Override
	public boolean send(Notify notify) {
		 
		if (notify instanceof MailNotify) {
			return mailNotifyService.send(notify);
		}
		return false;
	}
	
}
