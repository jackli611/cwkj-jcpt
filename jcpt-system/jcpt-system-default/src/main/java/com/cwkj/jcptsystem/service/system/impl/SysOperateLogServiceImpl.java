package com.cwkj.jcptsystem.service.system.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.component.system.SysOperateLogComponent;
import com.cwkj.jcptsystem.model.system.SysOperateLogDo;
import com.cwkj.jcptsystem.service.system.SysOperateLogService;

/** 
 * 用户操作日志
 * @author: harry
 * @date: 2017年7月14日 上午6:35:59  
 */
@Service("sysOperateLogService")
public class SysOperateLogServiceImpl implements SysOperateLogService {

	@Autowired
	private SysOperateLogComponent sysOperateLogComponent;
	
	@Override
	public Integer insertSysOperateLog(SysOperateLogDo sysOperateLogDo) {
		return sysOperateLogComponent.insertSysOperateLog(sysOperateLogDo);
	}

	@Override
	public List<SysOperateLogDo> querySysOperateLogList(Map<String, Object> selectItem) {
		return sysOperateLogComponent.querySysOperateLogList(selectItem);
	}

	@Override
	public PageDo<SysOperateLogDo> querySysOperateLogListPage(Long pageIndex,
			Integer pageSize, Map<String, Object> selectItem) {
		return sysOperateLogComponent.querySysOperateLogListPage(pageIndex, pageSize, selectItem);
	}

	@Override
	public Integer updateSysOperateLogById(SysOperateLogDo sysOperateLogDo) {
		return sysOperateLogComponent.updateSysOperateLogById(sysOperateLogDo);
	}

	@Override
	public Integer deleteSysOperateLogById(Long id) {
		return sysOperateLogComponent.deleteSysOperateLogById(id);
	}

	@Override
	public SysOperateLogDo findSysOperateLogById(Long id) {
		return sysOperateLogComponent.findSysOperateLogById(id);
	}

}
