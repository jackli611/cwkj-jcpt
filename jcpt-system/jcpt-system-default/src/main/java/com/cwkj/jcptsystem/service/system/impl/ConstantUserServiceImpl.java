package com.cwkj.jcptsystem.service.system.impl;

import org.springframework.stereotype.Service;

import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.ConstantUserService;

/**
 * 常量用户
 * @author ljc
 * @version 1.0
 */
@Service("constantUserService")
public class ConstantUserServiceImpl implements ConstantUserService{

	@Override
	public SysUserDo autoLoanUser() {
		SysUserDo sysUser=new SysUserDo();
		sysUser.setRealName("system");
		sysUser.setId(-100);
		sysUser.setUserName("system");
		return sysUser;
	}

}
