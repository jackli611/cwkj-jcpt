/**
 * Project Name:jcpt-common <br>
 * File Name:CityInfoDao.java <br>
 * Package Name:com.cwkj.jcpt.dao.system <br>
 * @author anxymf
 * Date:2017年1月6日上午9:48:16 <br>
 * Copyright (c) 2017, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcptsystem.dao.system;



import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cwkj.jcptsystem.model.system.ParamDo;

/**
 * ClassName: CityInfoDao <br>
 * Description: TODO
 * @author anxymf
 * Date:2017年1月6日上午9:48:16 <br>
 * @version
 * @since JDK 1.6
 */
@Repository
public interface ParamDao{

	 /**
     * 
     * findById:根据Id查询. <br>
     * @param id
     * @return
     */
    public ParamDo queryById(String id);
	
	/**
	 * 
	 * insert:新增. <br>
	 * @param t 
	 * @return
	 */
    public Integer insert(ParamDo t);
    
    /**
     * 
     * update:更新. <br>
     * @param t
     * @return
     */
    public Integer update(ParamDo t);

    
    /**
     * 
     * deleteById:根据Id删除. <br>
     * @param id
     * @return
     */
    public Integer delete(String id);
    
    /**
     * 
     * queryListPage:分页查询. <br>
     * @param selectItem 查询参数map
     * @return
     */
    public List<ParamDo> queryListPage(Map<String, Object> selectItem);
	
	String queryValueByCode(String paramCode);
		
	
}

	