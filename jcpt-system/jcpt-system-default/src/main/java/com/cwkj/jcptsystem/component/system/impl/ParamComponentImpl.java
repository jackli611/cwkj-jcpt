/**
 * Project Name:jcpt-common <br>
 * File Name:ParamComponentImpl.java <br>
 * Package Name:com.cwkj.jcpt.component.system.impl <br>
 * @author anxymf
 * Date:2017年1月11日上午11:15:58 <br>
 * Copyright (c) 2017, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcptsystem.component.system.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.component.system.ParamComponent;
import com.cwkj.jcptsystem.dao.system.ParamDao;
import com.cwkj.jcptsystem.model.system.ParamDo;

/**
 * ClassName: ParamComponentImpl <br>
 * Description: TODO
 * @author anxymf
 * Date:2017年1月11日上午11:15:58 <br>
 * @version
 * @since JDK 1.6
 */
@Component
public class ParamComponentImpl implements ParamComponent {
	
	@Resource
	private ParamDao paramDao;
	
	
	@Override
	public Integer insert(ParamDo paramDo) {
		paramDo.setCreateTime(new Date());
		return paramDao.insert(paramDo) ;
	}

	@Override
	public Integer update(ParamDo paramDo) {
		return paramDao.update(paramDo);
	}

	@Override
	public Integer delete(String paramId) {
		return paramDao.delete(paramId);
	}

	@Override
	public ParamDo queryById(String paramId) {
		return paramDao.queryById(paramId);
	}

	@Override
	public PageDo<ParamDo> queryListPage(Map<String, Object> selectItem) {
		String pageIndex = (String)selectItem.get("pageIndex");
		String pageSize = (String)selectItem.get("pageSize");
		PageDo<ParamDo> page = new PageDo<ParamDo>(Long.valueOf(pageIndex),
				Integer.valueOf(pageSize));
		selectItem.put("page", page);
		page.setPage(paramDao.queryListPage(selectItem));
		return page;
	}

	@Override
	public String queryValueByCode(String paramCode) {
		return paramDao.queryValueByCode(paramCode);
	}
}

	