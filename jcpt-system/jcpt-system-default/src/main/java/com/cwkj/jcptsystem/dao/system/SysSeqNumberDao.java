package com.cwkj.jcptsystem.dao.system;

import java.util.Map;

import org.springframework.stereotype.Repository;


/**
 * 系统全局唯一ID
 * @author ljc
 * @version 1.0
 */
@Repository("sysSeqNumberDao")
public interface SysSeqNumberDao {

	public Long callDaySeqnumber(Map<String, Object> parameterMap);
}

