package com.cwkj.jcptsystem.dao.system;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import com.cwkj.jcptsystem.model.system.AdminDo;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
@Repository("adminDao")
public interface AdminDao{
	
	/**
	 * 添加.
	 * @param admin
	 * @return
	 */
	public Integer insert(AdminDo admin);
	
	/**
	 * 获取数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AdminDo> queryList(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改.
	 * @param admin
	 * @return
	 */
	public Integer updateById(AdminDo admin);

	/**
	 * 根据Id删除.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Long id);

	/**
	 * 根据Id获取.
	 * @param id
	 * @return
	 */
	public AdminDo findById(Long id);
 	
	/**
	 * 根据用户名获取
	 * @param userName
	 * 用户名
	 * @return
	 */
	public AdminDo findAdminByUserName(String userName);
}
