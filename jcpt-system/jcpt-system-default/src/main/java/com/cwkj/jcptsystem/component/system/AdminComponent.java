package com.cwkj.jcptsystem.component.system;

import java.util.List;
import java.util.Map;

import com.cwkj.jcptsystem.model.system.AdminDo;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
public interface AdminComponent {
	
	/**
	 * 添加.
	 * @param admin
	 * @return
	 */
	public int insertAdmin(AdminDo admin);
	
	/**
	 * 获取数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AdminDo> queryAdminList(Map<String,Object> selectItem);


	/**
	 * 根据Id修改.
	 * @param admin
	 * @return
	 */
	public int updateAdminById(AdminDo admin);

	/**
	 * 根据Id删除.
	 * @param id
	 * @return
	 */
	public int deleteAdminById(Long id);

	/**
	 * 根据Id获取.
	 * @param id
	 * @return
	 */
	public AdminDo findAdminById(Long id);	
	
	public AdminDo findAdminByUserName(String userName);
}
