package com.cwkj.jcptsystem.service.notify.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.service.notify.MailNotify;
import com.cwkj.jcptsystem.service.notify.Notify;
import com.cwkj.jcptsystem.service.notify.NotifyService;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 邮件通知服务
 * @author ljc
 * @version 1.0
 */
public class MailNotifyServiceImpl implements NotifyService{
	
	private static Logger logger=LoggerFactory.getLogger(MailNotifyServiceImpl.class);

	private final static String DEFAULT_TEMPLATE="mail_template_default.ftl";
	private JavaMailSenderImpl mailSender;
	private TaskExecutor taskExecutor;
	private Configuration freeMarkerConfigurer;
	
	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	public void setTaskExecutor(TaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}
	
	public void setFreeMarkerConfigurer(
			Configuration freeMarkerConfigurer) {
		this.freeMarkerConfigurer = freeMarkerConfigurer;
	}
	

	@Override
	public boolean send(final Notify notify) {
		logger.info("发送邮件:{}",notify.toString());
		if(notify.isAsync()){
			taskExecutor.execute(new Runnable(){
				public void run() {
					logger.info("异步发送邮件:{}",notify);
					sendMail(notify);
				}					
			});
            return true;
		}else{			
			//发送
			return sendMail(notify);
		}
	}
	
	/**
	 * 发送邮件
	 * 
	 */
    public boolean sendMail(Notify notify) {
		
		try {
			final MimeMessage msg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");
			helper.setFrom(mailSender.getUsername(),"小物橙");
			if(notify.getRecievers().contains(";")){
				String[] recieverArray = notify.getRecievers().split(";");
				helper.setTo(recieverArray);
			}else if(notify.getRecievers().contains(",")){
				String[] recieverArray = notify.getRecievers().split(",");
				helper.setTo(recieverArray);
			}else{
				helper.setTo(notify.getRecievers());
			}
			 
			helper.setSubject(notify.getSubject());
			helper.setSentDate(new Date());
			if(notify.getCcList() != null && !("null".equals(notify.getCcList()))&& !("".equals(notify.getCcList()))){
				if( notify.getCcList().contains(";")){
					String[] ccArray = notify.getCcList().split(";");
					helper.setCc(ccArray);
				}else if(notify.getCcList().contains(",")){
					String[] ccArray = notify.getCcList().split(",");
					helper.setCc(ccArray);
				}else{
					helper.setCc(notify.getCcList());
				}
			}
			
			
			String messageTemplate = notify.getMessageTemplate();
			if(null == messageTemplate || "".equals(messageTemplate)){
				messageTemplate=DEFAULT_TEMPLATE;
			}
			Template tpl = freeMarkerConfigurer.getTemplate(messageTemplate);
			String htmlText = null;
			Map<String, Object> content=null;
			if(notify.getMessage()!=null)
			{
				if (notify.getMessage() instanceof Map) {
					 content=(Map)notify.getMessage();
				}else{
					content=new HashMap<String,Object>();
					content.put("content", notify.getMessage());
				}
			}else{
				content.put("content", "");
			}
			htmlText = FreeMarkerTemplateUtils.processTemplateIntoString(tpl, content);
			helper.setText(htmlText, true);		
			
			if(notify.getFilePath() != null) {
				sendContext(msg,notify,htmlText);
			}
			
			//发送
			mailSender.send(msg);
			
            return true;
			
		} catch (NullPointerException e) {
			e.printStackTrace();
			logger.error("邮件发送异常", e);
		} catch (MessagingException e) {
			e.printStackTrace();
			logger.error("邮件发送异常", e);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.error("邮件发送异常", e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("邮件发送异常", e);
		} catch (TemplateException e) {
			e.printStackTrace();
			logger.error("邮件发送异常", e);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("邮件发送异常",e);
		}
        return false;
	}
    
    protected void sendContext(MimeMessage msg,Notify notify,String htmlText) throws MessagingException, UnsupportedEncodingException {

    	Multipart multipart = new MimeMultipart();
		BodyPart contentPart = new MimeBodyPart();
		if(StringUtil.isBlank(htmlText)){
			contentPart.setText("");
		}else{
			contentPart.setContent(htmlText,"text/html; charset=utf-8");  
		}
		multipart.addBodyPart(contentPart);
		if(notify instanceof MailNotify)
		{
			MailNotify mailNotify=(MailNotify)notify;
			if(mailNotify.getDataSource()!=null)
			{
				// 添加附件
				BodyPart messageBodyPart = new MimeBodyPart();
				//DataSource source = new FileDataSource(message.getFilePath());		
				// 添加附件的内容
				messageBodyPart.setDataHandler(new DataHandler(mailNotify.getDataSource()));
				// 添加附件的标题
				// 这里很重要，通过下面的Base64编码的转换可以保证你的中文附件标题名在发送时不会变成乱码
				sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
				messageBodyPart.setFileName("=?UTF-8?B?" + enc.encode((mailNotify.getFileName()).getBytes("UTF-8")) + "?=");
				multipart.addBodyPart(messageBodyPart);
			}
		}

		msg.setContent(multipart);
		msg.saveChanges();
    }

}
