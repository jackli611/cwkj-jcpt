package com.cwkj.jcptsystem.service.config.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.stereotype.Service;
import org.springframework.util.StringValueResolver;

import com.cwkj.jcptsystem.model.config.BaseUrlConfig;
import com.cwkj.jcptsystem.model.config.SmsConfig;
import com.cwkj.jcptsystem.service.config.ConfigService;

/**
 * 配置服务
 * @author ljc
 *
 */
@Service("configService")
public class ConfigServiceImpl implements ConfigService,EmbeddedValueResolverAware {
	
//	@Value("${sms.validate}")
	private String smsValidate;
//	@Value("${sms.url}")
	private String smsUrl;
//	@Value("${sms.md5key}")
	private String smsMd5Key;
	private String baseUrl_DK;
	private String baseUrl_XD;
	private String baseUrl_schedule;
	private String baseUrl_jcptApi;
	private String baseUrl_jcptAgreement;
	private String baseUrl_loanCert;
	
	private StringValueResolver stringValueResolver;
 

//	@Override
//	public <T extends Serializable> T getConfig(Class<T> obj) {
//		String objName=obj.getName();
//			if( SmsConfig.class.getName().equals(objName))
//			{
//				SmsConfig config=new SmsConfig();
//				config.setMd5key(smsMd5Key);
//				config.setUrl(smsUrl);
//				config.setValidate(smsValidate);
//				return (T)config;
//			}
//			if(BaseUrlConfig.class.getName().equals(objName))
//			{
//				BaseUrlConfig baseUrl=new BaseUrlConfig();
//				baseUrl.setDk(baseUrl_DK);
//				baseUrl.setXd(baseUrl_XD);
//				baseUrl.setSchedule(baseUrl_schedule);
//				baseUrl.setJcptApi(baseUrl_jcptApi);
//				baseUrl.setJcptAgreement(baseUrl_jcptAgreement);
//				return (T)baseUrl;
//			}
//		return null;
//	}

	@Override
	public String getValue(String keyName) {
		String name = "${" + keyName + "}";
	    return stringValueResolver.resolveStringValue(name);
	}

	@Override
	public void setEmbeddedValueResolver(StringValueResolver resolver) {
		 stringValueResolver = resolver;
	}

	@Override
	public BaseUrlConfig baseUrlConfig() {
		BaseUrlConfig baseUrl=new BaseUrlConfig();
		baseUrl.setDk(baseUrl_DK);
		baseUrl.setXd(baseUrl_XD);
		baseUrl.setSchedule(baseUrl_schedule);
		baseUrl.setJcptApi(baseUrl_jcptApi);
		baseUrl.setJcptAgreement(baseUrl_jcptAgreement);
		baseUrl.setLoanCert(baseUrl_loanCert);
		return baseUrl;
	}

	@Override
	public SmsConfig smsConfig() {
		SmsConfig config=new SmsConfig();
		config.setMd5key(smsMd5Key);
		config.setUrl(smsUrl);
		config.setValidate(smsValidate);
		return config;
	}

}
