package com.cwkj.jcptsystem.service.system.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcptsystem.component.system.AdminComponent;
import com.cwkj.jcptsystem.model.system.AdminDo;
import com.cwkj.jcptsystem.service.system.AdminService;

/**
 * 
 * @author ljc
 *
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService{

	@Autowired
	private AdminComponent adminComponent;
	
	/**
	 * 根据用户名获取用户
	 * @param userName
	 * @return
	 */
	public AdminDo findAdminByUserName(String userName)
	{
		return adminComponent.findAdminByUserName(userName);
	}
}
