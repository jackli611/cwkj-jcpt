package com.cwkj.jcptsystem.component.system.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcptsystem.component.system.AdminComponent;
import com.cwkj.jcptsystem.dao.system.AdminDao;
import com.cwkj.jcptsystem.model.system.AdminDo;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
@Component("adminComponent")
public class AdminComponentImpl  implements AdminComponent{
	
	@Autowired
	private AdminDao adminDao;
	
	/**
	 * 添加.
	 * @param admin
	 * @return
	 */
	public int insertAdmin(AdminDo admin)
	{
		return adminDao.insert(admin);
	}
	
	/**
	 * 获取数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AdminDo> queryAdminList(Map<String,Object> selectItem)
	{
		return adminDao.queryList(selectItem);
	}


	/**
	 * 根据Id修改.
	 * @param admin
	 * @return
	 */
	public int updateAdminById(AdminDo admin)
	{
		return adminDao.updateById(admin);
	}

	/**
	 * 根据Id删除.
	 * @param id
	 * @return
	 */
	public int deleteAdminById(Long id)
	{
		return adminDao.deleteById(id);
	}

	/**
	 * 根据Id获取.
	 * @param id
	 * @return
	 */
	public AdminDo findAdminById(Long id)
	{
		return adminDao.findById(id);
	}

	@Override
	public AdminDo findAdminByUserName(String userName) {
		return adminDao.findAdminByUserName(userName);
	}	
}
