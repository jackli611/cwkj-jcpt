/**
 * Project Name:jcpt-common <br>
 * File Name:AppProcessComponent.java <br>
 * Package Name:com.cwkj.jcpt.component.system <br>
 * @author anxymf
 * Date:2017年8月7日下午4:33:09 <br>
 * Copyright (c) 2017, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcptsystem.component.system;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.model.system.ProcessInfoDo;
import com.cwkj.jcptsystem.model.system.SysUserDo;

/**
 * ClassName: AppProcessComponent <br>
 * Description: TODO
 * @author anxymf
 * Date:2017年8月7日下午4:33:09 <br>
 * @version
 * @since JDK 1.6
 */
public interface AppProcessComponent {
	
	public PageDo<SysUserDo> queryAdminListPage(Map<String, Object> selectItem);
	
	public List<ProcessInfoDo> queryList();
	
	public List<String> queryProcessIdsByUserId(Long userId);	
	
	public void update(String[] processIds,Long userId);
	
}

	