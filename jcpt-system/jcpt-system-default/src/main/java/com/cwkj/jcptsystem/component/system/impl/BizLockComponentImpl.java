package com.cwkj.jcptsystem.component.system.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.component.system.BizLockComponent;
import com.cwkj.jcptsystem.dao.system.LockDao;
import com.cwkj.jcptsystem.model.system.LockDo;

/**
 * 全局业务锁
 * @author ljc
 * @version 1.0
 */
/**
 * @author ljc
 * @version 1.0
 */
@Component("bizLockComponent")
public class BizLockComponentImpl implements BizLockComponent{

	@Autowired
	private LockDao lockDao;
	
	@Override
	public LockDo findLock(String lockNum) {
		return lockDao.findLock(lockNum);
	}

	@Override
	public Integer addLock(LockDo lock) {
		lock.setCreateTime(new Date());
		return lockDao.addLock(lock);
	}

	@Override
	public Integer deleteLock(String lockNum) {
		return lockDao.delLock(lockNum);
	}

	@Override
	public PageDo<LockDo> queryLockListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem) {
		
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<LockDo> pageBean=new PageDo<LockDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(lockDao.queryListPage(selectItem));
		return pageBean;
	}

	@Override
	public Integer deleteLockByLockNum(String lockNum) {
		return lockDao.deleteByLockNum(lockNum);
	}

}
