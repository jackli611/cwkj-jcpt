/**
 * Project Name:jcpt-common <br>
 * File Name:ParamComponent.java <br>
 * Package Name:com.cwkj.jcpt.component.system <br>
 * @author anxymf
 * Date:2017年1月11日上午11:15:39 <br>
 * Copyright (c) 2017, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcptsystem.component.system;

import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.model.system.ParamDo;

/**
 * ClassName: ParamComponent <br>
 * Description: TODO
 * @author anxymf
 * Date:2017年1月11日上午11:15:39 <br>
 * @version
 * @since JDK 1.6
 */
public interface ParamComponent {
	
	Integer insert(ParamDo paramDo);
	
	Integer update(ParamDo paramDo);
	
	Integer delete(String paramId);
	
	ParamDo queryById(String paramId);
	
	PageDo<ParamDo> queryListPage(Map<String, Object> selectItem);
	
	String queryValueByCode(String paramCode);
		
}

	