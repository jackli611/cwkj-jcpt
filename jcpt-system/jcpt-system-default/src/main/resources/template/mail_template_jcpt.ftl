<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd ">     
<html xmlns="http://www.w3.org/1999/xhtml ">     
<head>     
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${title!""}</title>
	<style type="text/css">         
		* {
			margin:0;
		}
		body {
			padding:60px;
			margin:0;
			font-family:"Microsoft YaHei";
			hight:500px;
		}
		p{
			line-height: 25px;
		}
		.title {
			text-align: center;
			margin-bottom:10px;
		}
		.greeting {
			margin-top:10px;
			margin-bottom:10px;
		}
		.company{
			font-size:14px;
			color:#999999;
			float:left;
			padding-top:80px;
		}
		.table-data {
			font-size:14px;
			color:#000;
			border-bottom:1px solid #000;
			border-right:1px solid #000;
		}
		.ps {
			margin-top:10px;
			color:#000;
			font-size:14px;
			font-weight: bold;
			margin-bottom:5px;
		}
		.paragraph {
			font-size:14px;
			color:#000;
			line-height: 25px;
			margin-bottom:5px;
		}
		.paragraph_br {
			font-size:14px;
			color:#000;
			line-height: 25px;
			margin-bottom:5px;
		}
		.ptitle{
		
		}
		table {  
            border: 1px solid #000;  
            padding:0;   
            margin:8px auto; 
			margin-left: 0px;
            border-collapse: collapse;  
        }  
          
        td {  
            border: 1px solid #000;  
            background: #fff;  
            font-size:12px;  
            padding: 3px 3px 3px 8px;  
            color: #4f6b72;  
        } 
        .td_title{
	        text-align:center;
	        font-size:14px;
			color:#000;
			border-bottom:1px solid #000;
			border-right:1px solid #000;
        }
        .filling_text {
				text-decoration: underline;
				padding:0 2px;
				text-indent: 28px;
			}
       	div {
			margin-bottom:20px;
		}
		div.line{
			border-top:1px solid #686868;
			padding-top:15px;
		}
		.w200{
			width:200px;
		}
		.w300{
			width:300px;
		}
		.w400{
			width:400px;
		}
	</style>
</head>
<body>
<h2 class="title">${title!""}</h2>
<h3 class="greeting">${greeting!""}</h3>
<p class="paragraph">
${content!""}
</p>
<div class="company">
<p>
${company!""}
</p>
</div>
</body>
</html>