<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="value"  %>
<%@ attribute name="selection"  %>

<c:if test="${selection == true}">
    <label><input type="checkbox" value="105" name="customerTypeGroup" class="input-control">业主（中小企业主）</label>
    <label><input type="checkbox" value="104" name="customerTypeGroup" class="input-control">业主（白领）</label>
    <label><input type="checkbox" value="103" name="customerTypeGroup" class="input-control">业主（个体户）</label>
    <label><input type="checkbox" value="102" name="customerTypeGroup" class="input-control">业主（其他）</label>
    <label><input type="checkbox" value="101" name="customerTypeGroup" class="input-control">白领（上班族）</label>
    <label><input type="checkbox" value="100" name="customerTypeGroup" class="input-control">其他</label>
</c:if>
<c:if test="${selection == false}">
<c:set var="checkboxList" value="${fn:split(value, '-')}" />
<c:forEach items="${checkboxList}" var="modelObj" varStatus="status">
    <c:if test="${status.index>0}">,</c:if>
    <c:if test="${modelObj eq '100'}">其他</c:if>
    <c:if test="${modelObj eq '101'}">白领(上班族)</c:if>
    <c:if test="${modelObj eq '102'}">业主(其他)</c:if>
    <c:if test="${modelObj eq '103'}">业主(个体户)</c:if>
    <c:if test="${modelObj eq '104'}">业主(白领)</c:if>
    <c:if test="${modelObj eq '105'}">业主(中小企业主)</c:if>
</c:forEach>
</c:if>