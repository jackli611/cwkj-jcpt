<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
<title><tool:code type="SYSTEM_NAME"  selection="false" value="zh_cn"></tool:code></title>
<link rel="stylesheet" href="../res/css/AdminLTE.min.css">
<link rel="stylesheet" href="../res/css/all-skins.min.css">
<%@ include file="/WEB-INF/view/common/include/mainHeader.jsp" %>
<script type="text/javascript" src="../res/js/app.js"></script>
<script type="text/javascript" src="../res/js/menu.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<script type="text/javascript">
function loadData()
{
	  $.ajax({
		    type: 'post',
		    url: 'resetPassword.do',
		    data: $("#form_password").serialize(),
		    success: function(data) {
		    	$("#datalist").prop('outerHTML', data);
		    }
		});
			 
}
</script>
<style type="text/css">
/* .skin-blue .main-header .navbar { */
/*     background-color: #efefef !important; */
/*     border-bottom:  1px solid #e5e5e5; */
/* } */
.skin-blue .main-header .navbar .sidebar-toggle {
    color:  #fff !important;
}
.skin-blue .main-header .navbar .nav>li>a {
    color: #fff !important;
}
.navbar-nav>.user-menu>.dropdown-menu {
    border-top-right-radius: 0;
    border-top-left-radius: 0;
    padding: 1px 0 0 0;
    border-top-width: 0;
    width: 80px !important;
}
.navbar-nav>.user-menu>.dropdown-menu li a {
   padding-top:14px;
   padding-bottom:14px;
}
#datalist{
padding: 10px;
}
/* .navbar-nav>li>a { */
/*    padding: 14px 15px; */
/* } */
a.dropdown-toggle{
   padding-top:14px !important;
   padding-bottom:15px !important;
}

#mainNavTabs>.selected{
border-bottom: 2px solid #fff4f4;
background-color: #fff !important;
/* background-color: #a8d9ff !important; */
}
 
#mainNavTabs>li>a{
 color:#000;
 border-right: 1px solid #ddd;
}
#mainNavTabs>.selected>a{
 color:#000;
}
 
#mainNavTabsPanel .navbar {
    position: relative;
    min-height: 46px;
    margin-bottom:2px;
    border: 1px solid transparent;
}
 
#mainNavTabsPanel  .navbar-nav > li > a {
    padding: 13px 8px !important;
}

#mainNavTabs  .navbar-nav > li:HOVER{
background-color:#fff !important;
}

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="font-weight: normal;">
<!-- Site wrapper -->
<div class="wrapper" id="rrapp" >
  <header class="main-header">
        <a class="logo">
<!--       mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><img alt="" src="<c:url value="/res/images/platform/main_logo_mini.png"/>" style="max-height:48px; margin: 8px auto;"></b></span>
<!--       logo for regular state and mobile devices -->
      <span class="logo-lg" style="float:left;"><b><img alt="" src="<c:url value="/res/images/platform/main_logo_lg.png"/>" style="max-height: 48px; margin: 8px auto;"></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
       <div class="navbar-header">
            <a  class="navbar-brand" style="padding: 15px 15px;"><b><tool:code type="SYSTEM_NAME"  selection="false" value="zh_cn"></tool:code></b></a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>
      <div class="navbar-custom-menu" >
        <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  >
         <i class="fa fa-user"></i>&nbsp;&nbsp;<span class="text-username">
         <secure:principal property="realName" emptyProperty="userName"></secure:principal>
         <b class="caret"></b></span>
         </a>
           <ul class="dropdown-menu">
           
		  <li><a  href="../platform/resetPassword.do" data-toggle="modal"><i class="fa fa-lock"></i> &nbsp;修改密码</a></li>
          <li><a href="../platform/logout.do"><i class="fa fa-sign-out"></i> &nbsp;退出系统</a></li>
           </ul>
        </li>
		 
		</ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="control-sidebar control-sidebar-light">
  </aside>
  <aside class="main-sidebar">
   <section class="sidebar navbar">
   <ul class="sidebar-menu menu" >
    <c:out value="${menuHtml}" escapeXml="false" />
   </ul>
   </section>
  </aside>
  
  <!-- =============================================== -->
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper"  style="overflow: hidden; padding: 0px; ">
   <div id="mainNavTabsPanel" style="overflow: hidden; float: left;">
   <nav class="navbar navbar-static-top" role="navigation" style="z-index:1;">
	  <ul class="nav navbar-nav" id="mainNavTabs">
      </ul>
      </nav>
   </div>
<!--   <div id="mainNavTabsPanel" style="overflow: hidden;clear:both; float: left;"> -->
<!-- 	   <ul class="nav navbar-nav" id="mainNavTabs"> -->
<!--       </ul> -->
<!--    </div> -->
<!--    <div id="mainFrameWrapper"> -->
<!--    </div> -->
<!--   <iframe id="mainFrame" src="about:blank" style="border: 0px; padding: 0px; margin: 0px; width:100%;" ></iframe> -->
  <div id="mainFrameWrapper">
   </div>
  </div>
 
  <div class="control-sidebar-bg"></div>
  
  <!-- 修改密码 -->
<div id="passwordLayer" style="display: none;">
	<form class="form-horizontal">
	<div class="form-group">
		<div class="form-group">
		   	<div class="col-sm-2 control-label">账号</div>
		    <span class="label label-success" style="vertical-align: bottom;">{{user.username}}</span>
		</div>
		<div class="form-group">
		   	<div class="col-sm-2 control-label">原密码</div>
		   	<div class="col-sm-10">
		      <input type="password" class="form-control" v-model="password" placeholder="原密码"/>
		    </div>
		</div>
		<div class="form-group">
		   	<div class="col-sm-2 control-label">新密码</div>
		   	<div class="col-sm-10">
		      <input type="text" class="form-control" v-model="newPassword" placeholder="新密码"/>
		    </div>
		</div>
	</div>
	</form>
</div>

</div>
<!-- ./wrapper -->


 
</body>
</html>