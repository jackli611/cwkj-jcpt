<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
<title><tool:code type="SYSTEM_NAME"  selection="false" value="zh_cn"></tool:code></title>
<link rel="stylesheet" href="../res/css/AdminLTE.min.css">
<link rel="stylesheet" href="../res/css/all-skins.min.css">
<%@ include file="/WEB-INF/view/common/include/mainHeader.jsp" %>
<script type="text/javascript" src="../res/js/app.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
 
<style type="text/css">
.col-md-3{
width: 25%;
}
.small-box .icon{
padding-top: 20px;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="font-weight: normal; padding: 20px; background: #f9f9f9;">
<section id="components" data-spy="scroll" data-target="#scrollspy-components">
	<div class="row">
	<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>
              <p>企业</p>
            </div>
            <div class="icon">
              <i class="fas fa-puzzle-piece"></i>
            </div>
            <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        	<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>
              <p>供应商</p>
            </div>
            <div class="icon">
              <i class="fas fa-database"></i>
            </div>
            <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        	<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>
              <p>订单</p>
            </div>
            <div class="icon">
              <i class="fas fa-layer-group"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>
              <p>合同总额</p>
            </div>
            <div class="icon">
              <i class="fas fa-layer-group"></i>
            </div>
            <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
 </div>
 </section>
</body>
</html>