<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 </script>
</head>
<body>
<div id="activity" >
 <table class="table table-bordered ">
						<tr >
							<td>批次号</td>
							<th  ><c:out value="${payInfo.batchId}"/></th>
							<td >应付单号</th>
				<th><c:out value="${payInfo.payNo }"/></th>
				<td>到期日</th>
				<th><f:formatDate value="${payInfo.payDate}"  pattern="yyyy-MM-dd"/></th>
						</tr>
			<tr>
				<td >企业</th>
				<th><c:out value="${payInfo.companyName }"/></th>
				<td>供应商</th>
				<th><c:out value="${payInfo.supplierName }"/></th>
				<td>应付金额</td>
				<th> 
				<c:out value="${payInfo.amount }"/>
				</th>
			</tr>
			<tr>
				<td>合同编号</td>
				<th><c:out value="${payInfo.contractNo }"/></th>
				 <td>合同日期</td>
				<th >
				<f:formatDate value="${payInfo.contractDate}"  pattern="yyyy-MM-dd"/>
				</th>
				<td>合同金额</td>
				<th colspan="3">
				<c:out value="${payInfo.contractAmount }"/>
				</th>
			</tr>
			<tr>
				<td>支付状态</td>
				<th colspan="5">
				<tool:code type="payStatus" selection="false" value="${payInfo.payStatus }"></tool:code>
				</th>
			</tr>
			 
				</table>
 
</div>
 
</body>
</html>