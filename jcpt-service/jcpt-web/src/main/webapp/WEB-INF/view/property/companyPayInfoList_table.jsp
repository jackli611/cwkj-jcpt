<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>批次号</th>
<th>应付单号</th>
<th>到期日</th>
<th>企业</th>
<th>供应商</th>
<th>应付金额</th>
<th>支付状态</th>
<th>合同编号</th>
<th>合同日期</th>
<th>合同金额</th>
<th>创建时间</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="12">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.batchId}"/></td>
<td ><c:out value="${modelObj.payNo}"/></td>
<td ><f:formatDate value="${modelObj.payDate}"  pattern="yyyy-MM-dd"/></td>
<td ><c:out value="${modelObj.companyName}"/></td>
<td ><c:out value="${modelObj.supplierName}"/></td>
<td ><c:out value="${modelObj.amount}"/></td>
<td ><tool:code type="payStatus" selection="false" value="${modelObj.payStatus }"></tool:code></td>
<td ><c:out value="${modelObj.contractNo}"/></td>
<td ><f:formatDate value="${modelObj.contractDate}"  pattern="yyyy-MM-dd"/></td>
<td ><c:out value="${modelObj.contractAmount}"/></td>
<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
                                   
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   