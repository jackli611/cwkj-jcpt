<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>供应商名称</th>
<th>核心企业</th>
<th>手机号</th>
<th>法人主体</th>
<th>法人身份证号码</th>
<th>组织机构代码</th>
<th>小物橙审批状态</th>
<th>核心企业审批状态</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="12">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.names}"/></td>
<td ><c:out value="${modelObj.propertyOrgName}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:out value="${modelObj.legal}"/></td>
<td ><c:out value="${modelObj.legalIdNo}"/></td>
<td ><c:out value="${modelObj.organizationCode}"/></td>
<td >
	<tool:code type="supplierPlatformStatus" selection="false" value="${modelObj.platformStatus}"></tool:code></td>
<td >
	<c:if test="${modelObj.platformStatus==102}">
	<tool:code type="supplierStatus" selection="false" value="${modelObj.status}"></tool:code>
	</c:if>
</td>
                                    <td>
	                                    <div class="dropdown">
										  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
										  <ul class="dropdown-menu pull-right">
										  <secure:hasPermission name="supplier_add">
										    <li><a href="#" onclick="openWindow({'title':'供应商信息浏览','url':'viewSupplier.do?id=<c:out value="${modelObj.id}"/>'})"   >浏览</a></li>
										    </secure:hasPermission>

										      <li class="divider"></li>
										     <li>   <a href="#" onclick="openWindow({'title':'供应商审批','url':'<c:url value="/property/auditSupplierPlatformStatus.do?supplierId=${modelObj.id}"/>'})"   >审核</a></li>
										 
										  </ul>
										</div>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   