<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %> 
</head>
<body>
<div id="activity" >
 <div class="container">
  <div class="row">
<div class="col-md-6 ml-auto ">
  <table class="table table-none-border">
<tr>
<td>公司名称:</td>
<th><c:out value="${propertyOrg.names}"/></th>
</tr>
<tr>
<td>公司简称:</td>
<th><c:out value="${propertyOrg.shortName }"/></th>
</tr>
<tr>
<td>组织机构代码:</td>
<th><c:out value="${propertyOrg.organizationCode}"/></th>
</tr>
<tr>
<td>法人主体:</td>
<th><c:out value="${propertyOrg.legal}"/></th>
</tr>
<tr>
<td>法人身份证号码:</td>
<th><c:out value="${propertyOrg.legalIdNo}"/></th>
</tr>
<tr>
<td>手机号:</td>
<th><c:out value="${propertyOrg.mobile}"/></th>
</tr>
<tr>
<td>有效状态:</td>
<th ><c:if test="${propertyOrg.status==1}">合作中</c:if><c:if test="${propertyOrg.status==2}">取消合作</c:if></th>
</tr>
<tr>
<td>创建时间:</td>
<th ><f:formatDate value="${propertyOrg.createTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>修改时间:</td>
<th ><f:formatDate value="${propertyOrg.updateTime}"  pattern="yyyy-MM-dd"/></t>
</tr> 
<tr>
<td>备注:</td>
<th><c:out value="${propertyOrg.remark}"/></th>
</tr>
</table>
</div>

<div class="col-md-6 ml-auto">
<div >
<c:if test="${not empty  propertyOrg.businessLicense}">
<img data-toggle="lightbox" src="businessLicenseFile.do?id=<c:out value="${propertyOrg.id}"/>"
 data-image="businessLicenseFile.do?id=<c:out value="${propertyOrg.id}"/>" data-caption="营业执照" class="img-rounded" alt="" width="200">
 <p>（营业执照）</p>
</c:if>
</div>
</div>

</div>
</div>

</div>
</body>
</html>