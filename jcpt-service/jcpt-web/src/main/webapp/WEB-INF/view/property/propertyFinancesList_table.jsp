<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>资金机构</th>
<th>授信额度</th>
<th>匹配日期</th>
<th>还款方式</th>
<th>担保方式</th>
<th>有效状态</th>
<th>利息</th>
<th>操作</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
<tr class=gvItem>
<td align="center" colspan="9">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty pagedata.page }">
<c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><tool:financeOrg selection="false" type="finance" value="${modelObj.financeId}"/></td>
<td ><c:out value="${modelObj.creditAmount}"/></td>
<td ><f:formatDate value="${modelObj.metaDate}"  pattern="yyyy-MM-dd"/></td>
<td ><c:out value="${modelObj.paymentType}"/></td>
<td ><c:out value="${modelObj.guaranteeType}"/></td>
<td ><c:out value="${modelObj.status}"/></td>
<td ><c:out value="${modelObj.interestRate}"/></td>
        <td>
            <div class="dropdown">
			  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
			  <ul class="dropdown-menu pull-right">
			   <li><a href="##" onclick="openWindow({'title':'物业的资金机构浏览','url':'viewPropertyFinances.do?id=<c:out value="${modelObj.id}"/>'})"   >浏览</a></li>
			   <li><a href="##" onclick="openWindow({'title':'物业的资金机构编辑','url':'editPropertyFinances.do?id=<c:out value="${modelObj.id}"/>'})"   >编辑</a></li>
			   <li><a href="##" onclick="deletePropertyFinances('<c:out value="${modelObj.id}"/>')">删除</a></li>
			  </ul>
			</div>
		</td>
 </tr>
</c:forEach>
</tbody>
</table>
	
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>

	
   