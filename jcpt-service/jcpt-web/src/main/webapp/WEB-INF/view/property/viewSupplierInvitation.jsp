<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %> 
</head>
<body>
<div id="activity" >
  <table class="table table-bordered ">
<tr>
<td>邀请码</td>
<th><c:out value="${supplierInvitation.inviteCode}"/></th>
</tr>
<tr>
<td>访问地址</td>
<th><c:out value="${supplierInvitation.links}"/></th>
</tr>
<tr>
<td>登记时间</td>
<th ><f:formatDate value="${supplierInvitation.signUpTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>有效到期时间</td>
<th ><f:formatDate value="${supplierInvitation.expiryDate}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>手机号</td>
<th><c:out value="${supplierInvitation.mobile}"/></th>
</tr>
<tr>
<td>备注</td>
<th><c:out value="${supplierInvitation.remark}"/></th>
</tr>
<tr>
<td>修改时间</td>
<th ><f:formatDate value="${supplierInvitation.updateTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>创建时间</td>
<th ><f:formatDate value="${supplierInvitation.createTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
</table>

</div>
</body>
</html>