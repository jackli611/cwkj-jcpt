<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 </script>
</head>
<body>
<div id="activity" >
 <table class="table table-bordered ">
						<tr >
							<td>等级</td>
							<th  ><tool:code type="levels" selection="false" value="${company.levels}"></tool:code></th>
							<td >企业名称</th>
				<th><c:out value="${company.names }"/></th>
				<td>手机号</th>
				<th><c:out value="${company.mobile }"/></th>
						</tr>
			<tr>
				<td >法人</th>
				<th><c:out value="${company.legal }"/></th>
				<td>法人身份证号码</th>
				<th><c:out value="${company.legalIdNo }"/></th>
				<td>组织机构代码</td>
				<th> 
				<c:out value="${company.organizationCode }"/>
				</th>
			</tr>
			<tr>
				
				<td>邮箱</td>
				<th colspan="3">
				<c:out value="${company.email }"/>
				</th>
				 <td>审批状态</td>
				<th >
				<tool:code type="approvalStatus" selection="false" value="${company.approvalStatus}"></tool:code>
				</th>
			</tr>
			 
				</table>
 
</div>
 
</body>
</html>