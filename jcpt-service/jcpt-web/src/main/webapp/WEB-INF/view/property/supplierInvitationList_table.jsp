<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>核心企业</th>
<th>邀请码</th>
<th>邀请码地址</th>
<th>操作</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
<tr class=gvItem>
<td align="center" colspan="5">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty pagedata.page }">
<c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.propertyOrg}"/></td>
<td ><c:out value="${modelObj.inviteCode}"/></td>
<td style="white-space:normal;"><a href="<c:out value="${supplierInviteLink}"/><c:out value="${modelObj.inviteCode}"/>" target="_blank"><c:out value="${supplierInviteLink}"/><c:out value="${modelObj.inviteCode}"/></a></td>
        <td>
            <div class="dropdown">
			  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
			  <ul class="dropdown-menu pull-right">
			   <li><a href="##" onclick="openWindow({'title':'供应商邀请注册浏览','url':'viewSupplierInvitation.do?id=<c:out value="${modelObj.id}"/>'})"   >浏览</a></li>
			   <li><a href="##" onclick="openWindow({'title':'供应商邀请注册编辑','url':'editSupplierInvitation.do?id=<c:out value="${modelObj.id}"/>'})"   >编辑</a></li>
			   <li><a href="##" onclick="deleteSupplierInvitation('<c:out value="${modelObj.id}"/>')">删除</a></li>
			  </ul>
			</div>
		</td>
 </tr>
</c:forEach>
</tbody>
</table>
	
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>

	
   