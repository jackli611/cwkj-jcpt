<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url){
	 
	 $.ajax({
         type:"POST",
         dataType:"json",
         url:url,
         data: $("#form_1").serialize(),
         async:false,
         success:function(data){
             if(data.code == "1"){
            	 alertLayer(data.msg);
            	 setTimeout( function(){
            		 window.parent.location.reload();//刷新父页面
                     parent.layer.close(layer.index);//关闭弹出层
				}, 2000);
            	 
             }else{
            	 alertLayer(data.msg);
             }
         }
     });
	 
 }
 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
<div class="form-group  form-inline">
<label for="names" class="monospaced_lg">企业名称:</label>
<input type="text" id="names" name="names" class="form-control w400" value="<c:out value="${company.names}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="organizationCode" class="monospaced_lg">组织机构代码:</label>
<input type="text" id="organizationCode" name="organizationCode" class="form-control w400" value="<c:out value="${company.organizationCode}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="legal" class="monospaced_lg">法人主体:</label>
<input type="text" id="legal" name="legal" class="form-control w400" value="<c:out value="${company.legal}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="legalIdNo" class="monospaced_lg">法人身份证号码:</label>
<input type="text" id="legalIdNo" name="legalIdNo" class="form-control w400" value="<c:out value="${company.legalIdNo}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="mobile" class="monospaced_lg">手机号:</label>
<input type="text" id="mobile" name="mobile" class="form-control w400" value="<c:out value="${company.mobile}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="email" class="monospaced_lg">邮箱:</label>
<input type="text" id="email" name="email" class="form-control w400" value="<c:out value="${company.email}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="leves" class="monospaced_lg">等级:</label>
<select class="form-control" name="levelsStr">
<option value="" >请选择</option>
<tool:code type="levels" selection="true" value="${company.levels}"></tool:code>
</select>
</div>  
 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<secure:hasPermission name="company_add">
<c:if test="${empty company.id }">
<input type="button" onclick='submitFn("addCompanyByAjax.do")' class="btn" id="addBtn" value="保存" />
</c:if>
</secure:hasPermission>
<secure:hasPermission name="company_edit">
<c:if test="${not empty company.id }">
<input type="hidden"  name="id" class="form-control w400" value="<c:out value="${company.id}"/>" />
<input type="button" onclick='submitFn("editCompanyByAjax.do")' class="btn" id="modifyBtn" value="保存" />
</c:if>
</secure:hasPermission>
</div>
</div>
			</form>
		</div>
	</div>
</body>
</html>