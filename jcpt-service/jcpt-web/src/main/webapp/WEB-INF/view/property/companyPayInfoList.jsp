<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	 queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"companyPayInfoList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<input type="hidden" name="companyId" value="<c:out value="${companyId }"/>" >
				 <div class="form-group">
			 	<label for="startDate">录入时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			 <div class="form-group">
			 	<label for="batchId">批次号：</label>
			  	<input id="batchId" class="form-control" type="text" name="batchId"    />
			</div>
			 <div class="form-group">
			 	<label for="payNo">应付单号：</label>
			  	<input id="payNo" class="form-control" type="text" name="payNo"    />
			</div>
			<div class="form-group">
			 	<label for="payStatusStr">支付状态：</label>
			  <select name="payStatusStr" class="form-control">
<option value="">请选择</option>
<tool:code type="payStatus" selection="true" value=""></tool:code>
</select>
			</div>
			<div class="form-group">
			 	<label for="supplierId">供应商：</label>
			 <select name="supplierId" class="form-control">
<option value="">请选择</option>
<tool:supplier type="supplier" selection="true" value=""></tool:supplier>
</select>
			</div>
			<div class="form-group">
				<input type="submit" class="btn" id="queryBtn" value="查询" />
			</div>
			</form>
		</div>
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
		<table class="table table-bordered">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>批次号</th>
<th>应付单号</th>
<th>到期日</th>
<th>企业</th>
<th>供应商</th>
<th>应付金额</th>
<th>支付状态</th>
<th>合同编号</th>
<th>合同日期</th>
<th>合同金额</th>
<th>创建时间</th>
			</tr>
			</thead>
			<tbody  >
			    
	</tbody>
	</table>
		</div>
	</div>
</body>
</html>