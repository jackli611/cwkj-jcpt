<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
<div class="form-group  form-inline">
<label for="batchId" class="monospaced_lg">批次号:</label>
<input type="text" id="batchId" name="batchId" class="form-control w400" value="<c:out value="${payInfo.batchId}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="payNo" class="monospaced_lg">应付单号:</label>
<input type="text" id="payNo" name="payNo" class="form-control w400" value="<c:out value="${payInfo.payNo}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="payDate" class="monospaced_lg">到期日:</label>
<input type="text" id="payDate" name="payDateStr" class="form-control w400"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${payInfo.payDate}"  pattern="yyyy-MM-dd"/>"    />
</div> 
<div class="form-group  form-inline">
<label for="companyId" class="monospaced_lg">企业:</label>
<select name="companyId" class="form-control">
<option value="">请选择</option>
<tool:company type="company" selection="true" value="${payInfo.companyId }"></tool:company>
</select>
</div> 
<div class="form-group  form-inline">
<label for="supplierId" class="monospaced_lg">供应商:</label>
<select name="supplierId" class="form-control">
<option value="">请选择</option>
<tool:supplier type="supplier" selection="true" value="${payInfo.supplierId }"></tool:supplier>
</select>
</div> 
<div class="form-group  form-inline">
<label for="amount" class="monospaced_lg">应付金额:</label>
<input type="text" id="amount" name="amountStr" class="form-control w400" value="<c:out value="${payInfo.amount}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="payStatus" class="monospaced_lg">支付状态:</label>
<select name="payStatusStr" class="form-control">
<option value="">请选择</option>
<tool:code type="payStatus" selection="true" value="${payInfo.payStatus }"></tool:code>
</select>
</div> 
<div class="form-group  form-inline">
<label for="contractNo" class="monospaced_lg">合同编号:</label>
<input type="text" id="contractNo" name="contractNo" class="form-control w400" value="<c:out value="${payInfo.contractNo}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="contractDate" class="monospaced_lg">合同日期:</label>
<input type="text" id="contractDate" name="contractDateStr" class="form-control w400"   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${payInfo.payDate}"  pattern="yyyy-MM-dd"/>"  />
</div> 
<div class="form-group  form-inline">
<label for="contractAmount" class="monospaced_lg">合同金额:</label>
<input type="text" id="contractAmount" name="contractAmountStr" class="form-control w400" value="<c:out value="${payInfo.contractAmount}"/>" />
</div> 
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<input type="button" onclick='submitFn("addPayInfo.do")' class="btn" id="addBtn" value="新增" />
<input type="button" onclick='submitFn("editPayInfo.do")' class="btn" id="modifyBtn" value="修改" />
</div>
			</form>
		</div>
	</div>
</body>
</html>