<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
	<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>分配产品编码</th>
				<th>产品名称</th>
				<th>还款方式</th>
				<th>利率等级</th>
				<th>利率</th>
				<th>最小借款周期</th>
				<th>最大借款周期</th>
				<th>状态</th>
				<th>操作</th>
			</tr>
			</thead>
			
 			<c:if test="${empty pagedata.page}">
 				<tbody  >
	             <tr class=gvItem>
	                 <td align="center" colspan="10">查询数据为空！</td>
	             </tr>
	            </tbody>
             </c:if>
             
                        
             <c:if test="${not empty pagedata.page }">
                    <tbody>
                    <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                     <tr >
                            <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
							<td ><c:out value="${modelObj.productcode}"/></td>
							<td ><c:out value="${modelObj.productname}"/></td>
							<td ><c:out value="${modelObj.repayname}"/></td>
							<td ><c:out value="${modelObj.levels}"/></td>
							<td ><c:out value="${modelObj.rate}"/>%</td>
							<td ><c:out value="${modelObj.minperoid}"/></td>
							<td ><c:out value="${modelObj.maxperoid}"/></td>
							<td >
								<c:if test="${modelObj.status eq 1}"> 有效</c:if>
								<c:if test="${modelObj.status eq 0}"> 无效</c:if>
							</td>
							<td>
						          <div class="dropdown">
								  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
								  <ul class="dropdown-menu pull-right">
								  
								    <secure:hasPermission name="company_edit">
								    <li><a href="#" onclick="javascript:location.href='toUpdateCompanyProduct.do?propertyOrgId=<c:out value="${modelObj.propertyorgid}"/>&assignId=<c:out value="${modelObj.assignid}"/>';">编辑</a></li>
								    </secure:hasPermission>
								  </ul>
								</div>
							</td>
                     </tr>
                    </c:forEach>
                     </tbody>
               </c:if>
	</table>
	
	 <c:if test="${not empty pagedata.page }">
	 <ul class="pager" id="pager">
	 </c:if>
    </ul>

                            