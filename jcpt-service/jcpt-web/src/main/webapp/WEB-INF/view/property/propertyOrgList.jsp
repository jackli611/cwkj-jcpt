<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	 queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"propertyOrgList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 function propertyOrgStatus(id,status)
 {
	 confirmLayer({msg:"确定要取消合作？",yesFn:function(){
		 loadJsonData({url:"propertyOrgStatus.do?id="+id+"&status="+status,complete:function(resultData){
			 if(resultData.code)
				 {
				 queryStart();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
 }
 
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
				 <div class="form-group">
			 	<label for="startDate">时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			<div class="form-group">
			<secure:hasPermission name="propertyorg_list_query">
			<input type="submit" class="btn btn-primary" id="queryBtn" value="查询" />
			</secure:hasPermission>
			<secure:hasPermission name="propertyorg_add">
			<input type="button" class="btn btn-primary" id="addBtn" onclick="openWindow({title:'新增核心企业',url:'addPropertyOrg.do'})" value="新增核心企业" />
			</secure:hasPermission>
			</div>
			</form>
		</div>
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
		<table class="table table-bordered">
		<thead>
			<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>企业名称</th>
<th>企业简称</th>
<th>组织机构代码</th>
<th>法人主体</th>
<th>法人身份证号码</th>
<th>手机号</th>
<th>状态</th>
<th>创建时间</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
			    
	</tbody>
	</table>
		</div>
	</div>
</body>
</html>