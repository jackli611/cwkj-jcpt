<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
<title><tool:code type="SYSTEM_NAME"  selection="false" value="zh_cn"></tool:code></title>
<%@ include file="/WEB-INF/view/common/include/mainHeader.jsp" %>
 <link rel="stylesheet" href="<c:url value="/res/css/theme-keen.css"/>" />
 <script type="text/javascript" src="<c:url value="/res/js/theme-keen.js"/>"></script>
<script type="text/javascript">
function loadData()
{
	  $.ajax({
		    type: 'post',
		    url: 'resetPassword.do',
		    data: $("#form_password").serialize(),
		    success: function(data) {
		    	$("#datalist").prop('outerHTML', data);
		    }
		});
			 
}

</script>
<style type="text/css">
.k-aside-black .navbar-menu li a{
color:#7d8092 !important;
 border: 0px !important;
  border-radius:0px !important;
}

.k-aside-black .nav-primary>li.active>a, .nav-primary>li.active>a:focus, .nav-primary>li.active>a:hover{
background-color: white !important;
}
.k-aside-white .navbar-menu li a{
color:#000000 !important;
 border-right: 0px !important;
 border-radius:0px !important;
}
</style>
</head>
<body  style="font-weight: normal;">
<!-- 左侧菜单栏 -->
<div class="k-aside k-aside--fixed k-aside-black" id="k_aside">
<div class="container">
<div class="k-aside_brand row" id="k_aside_brand"  style="">
	<div class="k-aside_brand-logo">
		<a href="#">
			<img alt="Logo" src="../res/images/logo/logo-320.png">
		</a>
	</div>
</div>
<div class="row navbar-menu">
<div class="k-aside--mtop">
<ul class="nav nav-primary nav-stacked navbar-menu">
 <li class="active"><a>供应链</a></li>
 <li><a>钱包</a></li>
 <li><a>白条</a></li>
</ul>
</div>
</div>
</div>
</div>


<!-- 二级菜单 -->
<div class="k-aside k-aside--3fixed k-aside-white" style="border-right: 1px solid #e4e4e4;">
<div class="container">
<div class="row navbar-menu">
<ul class="nav nav-primary nav-stacked navbar-menu k-aside--mtop">
 <li class="active"><a>上游供应商</a></li>
 <li><a>供应商管理</a></li>
 <li><a>借款管理</a></li>
 <li><a>企业信息</a></li>
 <li><a>企业注册</a></li>
</ul>
</div>
</div>
</div>

<!-- 主栏目 -->
<div class="k-wrapper" style="background-color: white;">
<div class="container">
<div class="row">

<H1>内容</H1>
</div>
</div>
</div>
 
 <!-- 顶部栏目 -->
<div class="k-aside--2fixed k-aside-white" id="k_bar_top">
<div class="container">
<div class="row">
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
  <ul class="nav navbar-nav navbar-right">
  <li><a href="your/nice/url">帮助</a></li>
  <li class="dropdown">
          <a href="##" class="dropdown-toggle" data-toggle="dropdown">用户 <b class="caret"></b></a>
          <ul class="dropdown-menu" role="menu">
            <li><a  href="../property/resetPassword.do" data-toggle="modal"><i class="fa fa-lock"></i> &nbsp;修改密码</a></li>
          <li><a href="../property/logout.do"><i class="fa fa-sign-out"></i> &nbsp;退出系统</a></li>
          </ul>
        </li>
  </ul>
 </div>
 </nav>
</div>
</div>
</div>
</body>
</html>