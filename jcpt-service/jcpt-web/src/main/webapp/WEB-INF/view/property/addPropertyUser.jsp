<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">
	 function dealLoadCodeFn(code) {
		 if(code==1)
		 {
			 if(parent.closeWindow)
			 {
				 parent.loadData();
				 window.setTimeout(parent.closeWindow,1000);
			 }

		 }
	 }
 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
			<input type="hidden" name="propertyId" value="<c:out value="${propertyUser.propertyId}"/>"/>
<div class="form-group  form-inline">
${propertyUser.id}
<c:if test="${not empty propertyUser.id}">
<label for="mobile" class="monospaced_lg">(登录系统账户)手机号:</label>
<input type="text" id="mobile" name="mobile" readonly="readonly" class="form-control w400" value="<c:out value="${propertyUser.mobile}"/>" />
</c:if>
<c:if test="${empty propertyUser.id}">
<label for="mobile" class="monospaced_lg">(登录系统账户)手机号:</label>
<input type="text" id="mobile" name="mobile" class="form-control w400" value="<c:out value="${propertyUser.mobile}"/>" />
<div class="form-group  form-inline">
	<label for="password" class="monospaced_lg">密码:</label>
	<input type="password" id="password" name="password" class="form-control w400"   />
</div>
<div class="form-group  form-inline">
	<label for="password2" class="monospaced_lg">确认密码:</label>
	<input type="password" id="password2" name="password2" class="form-control w400"   />
</div>
</c:if>
</div>

<div class="form-group  form-inline">
<label for="realName" class="monospaced_lg">姓名:</label>
<input type="text" id="realName" name="realName" class="form-control w400" value="<c:out value="${propertyUser.realName}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="email" class="monospaced_lg">邮箱:</label>
<input type="text" id="email" name="email" class="form-control w400" value="<c:out value="${propertyUser.email}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="sexStr" class="monospaced_lg">性别:</label>
<select name="sexStr" class="form-control">
<option value="">请选择</option>
<tool:code type="sex" selection="true" value="${propertyUser.sex}"></tool:code>
</select>
</div> 
<div class="form-group  form-inline">
<label for="userType" class="monospaced_lg">用户类型:</label>
<select name="userType" class="form-control">
<option value="">请选择</option>
<tool:code type="propertyUserType" selection="true" value="${propertyUser.userType}"></tool:code>
</select>
</div> 
<div class="form-group  form-inline">
<label for="position" class="monospaced_lg">职务:</label>
<select name="position" class="form-control">
<option value="">请选择</option>
<tool:code type="propertyUserPosition" selection="true" value="${propertyUser.position}"></tool:code>
</select>
</div> 
<div class="form-group  form-inline">
<label for="buildUserTag" class="monospaced_lg">能否创建账户:</label>
<select name="buildUserTagStr" class="form-control">
<option value="">请选择</option>
<tool:code type="buildUserTag" selection="true" value="${propertyUser.buildUserTag}"></tool:code>
</select>
</div> 
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<secure:hasPermission name="propertyuser_add">
<c:if test="${empty propertyUser.id}">
<input type="button" onclick='submitFn("addPropertyUser.do")' class="btn" id="addBtn" value="保存" />
</c:if>
</secure:hasPermission>
<secure:hasPermission name="propertyuser_edit">
<c:if test="${ not empty propertyUser.id}">
<input type="hidden" name="id" value="<c:out value="${propertyUser.id}"/>"/>
<input type="button" onclick='submitFn("editPropertyUser.do")' class="btn" id="modifyBtn" value="保存" />
</c:if>
</secure:hasPermission>
</div>
			</form>
		</div>
	</div>
</body>
</html>