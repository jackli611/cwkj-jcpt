<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="/WEB-INF/view/common/include/header.jsp" %>
    <!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
    <title></title>

    <link rel="stylesheet" href="<c:url value="/res/css/theme-keen.css"/>" />
    <script type="text/javascript" src="<c:url value="/res/js/theme-keen.js"/>"></script>
</head>
<body>
<div id="promptMsg"></div>
<div class="container-fluid">

<div class="panel panel-default k_container_bg_color">
    <div class="panel-body">
        <h4 class="title">基本信息</h4>
        <div class="pull-left">
            <table class="table table-none-border k_panel_margin">
                <tr ><td>供应商编号: <c:out value="${supplier.supplierNo }"/></td></tr>
                <tr><td >供应商名称:<c:out value="${supplier.names }"/></td></tr>
                <tr><td>手机号:<c:out value="${supplier.mobile }"/></td></tr>
                <tr><td >法人:<c:out value="${supplier.legal }"/></td></tr>
                <tr><td>法人身份证号码:<c:out value="${supplier.legalIdNo }"/></td></tr>
                <tr><td>组织机构代码:<c:out value="${supplier.organizationCode }"/></td></tr>
<%--                <tr><td>银行卡账户名:<c:out value="${supplier.bankAccount }"/>	</td></tr>--%>
<%--                <tr><td >银行卡号:<c:out value="${supplier.bankCardno }"/></td></tr>--%>
<%--                <tr><td >银行名称:<c:out value="${supplier.bankName }"/></td>	</tr>--%>
                <tr><td >地址:<c:out value="${supplier.address }"/></td></tr>
                <tr><td >联系人:<c:out value="${supplier.contactPerson }"/></td></tr>
                <tr><td >邮箱:<c:out value="${supplier.email }"/></td></tr>
                <tr><td >企业简介:<c:out value="${supplier.companyProfile }"/></td></tr>
            </table>
        </div>
        <div class="pull-right">
            <img class="img-rounded" data-toggle="lightbox" data-image="../attachment.do?id=<c:out value="${supplier.businessLicense}"/>" alt="" style="max-width: 130px; max-height: 190px; box-shadow:  0px 3px 2px #e4e4e4;" src="../attachment.do?id=<c:out value="${supplier.businessLicense}"/>" alt="">
        </div>

    </div>
</div>

<c:if test="${not empty supplierAuditList }">
    <div class="panel panel-default k_container_bg_color">
        <div class="panel-body">
            <h4 class="title">注册审批信息</h4>
            <div class="pull-left">
                <table class="table table-bordered k_panel_margin k_table_bg_color">
                    <thead>
                    <tr >
                        <th style="width:60px; min-width: 60px;">序号</th>
                        <th>有效状态</th>
                        <th>备注</th>
                        <th>创建时间</th>
                    </tr>
                    </thead>
                    <c:forEach items="${supplierAuditList}" var="modelObj" varStatus="status">
                        <tr >
                            <td ><c:out value="${status.index+1}"/></td>
                            <td ><tool:code type="supplierPlatformStatus" selection="false" value="${modelObj.status}"></tool:code></td>
                            <td ><c:out value="${modelObj.remark}"/></td>
                            <td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
                        </tr>
                    </c:forEach>
                </table>

            </div>
        </div></div>
</c:if>



<c:if test="${supplier.platformStatus == 101}">
    <div class="panel panel-default" id="auditPanel">
        <div class="form-panel ">
            <form id="form_auditSupplier" name="form"    method="post"  class="form-horizontal">

                <div class="form-group"  data-toggle="buttons">
                    <label class="btn statusArr k_btn k_btn_line" >
                        <input type="radio"  name="status" value="104"> 不通过，打回修改
                    </label>
                    <label class="btn k-btn-pd statusArr k_btn k_btn_line" >
                        <input type="radio" name="status" value="103"> 拒绝
                    </label>
                    <label class="btn k-btn-pd statusArr k_btn k_btn_line" >
                        <input type="radio" name="status" value="102"> 审批通过
                    </label>

                </div>
                <div class="form-group">
                    <textarea id="remark" name="remark" class="form-control new-comment-text" placeholder="审批说明"></textarea>
                </div>
                <div class="form-group">
                    <input type="hidden" id="supplierId" name="supplierId" value="<c:out value="${supplierId}"/>" />
                    <input type="button" onclick='submitFn("auditSupplierPlatformStatus.do")' class="btn btn-primary" id="modifyBtn" value="提交" />
                </div>
            </form>
        </div>
    </div>
    </div>


    <script type="text/javascript">

        function submitFn(url)
        {
            loadJsonData({url:url,data:$("#form_auditSupplier").serialize(),complete:function(obj){
                    alertLayer(obj.msg);
                    if(obj.code==1)
                    {
                        $("#auditPanel").remove();
                        if(parent.closeWindow)
                        {
                            parent.loadData();
                            window.setTimeout(parent.closeWindow,1000);
                        }
                    }
                }});

        }

    </script>

</c:if>

</div>
</body>
</html>