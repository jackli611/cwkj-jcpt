<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>企业名称</th>
<th>组织机构代码</th>
<th>法人主体</th>
<th>法人身份证号码</th>
<th>手机号</th>
<th>审批状态</th>
<th>邮箱</th>
<th>等级</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="10">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.names}"/></td>
<td ><c:out value="${modelObj.organizationCode}"/></td>
<td ><c:out value="${modelObj.legal}"/></td>
<td ><c:out value="${modelObj.legalIdNo}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td >
<tool:code type="approvalStatus" selection="false" value="${modelObj.approvalStatus}"></tool:code>
</td>
<td ><c:out value="${modelObj.email}"/></td>
<td >
<tool:code type="levels" selection="false" value="${modelObj.levels}"></tool:code>
</td>

                                    <td>
	                                    <div class="dropdown">
										  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
										  <ul class="dropdown-menu">
										  <secure:hasPermission name="company_view">
										    <li><a href="#" onclick="openWindow({'title':'企业信息浏览','url':'viewCompany.do?id=<c:out value="${modelObj.id}"/>'})" >浏览</a></li>
										    </secure:hasPermission>
										    <secure:hasPermission name="company_edit">
										    <li><a href="#" onclick="openWindow({url:'editCompany.do?id=<c:out value="${modelObj.id}"/>'});">编辑</a></li>
										    </secure:hasPermission>
										    <secure:hasPermission name="company_delete">
										    <li><a href="#" onclick="deleteCompany('<c:out value="${modelObj.id}"/>');">删除</a></li>
										    </secure:hasPermission>
										      <li class="divider"></li>
										     <li><a href="#" onclick="openWindow({url:'companyPayInfoList.do?companyId=<c:out value="${modelObj.id}"/>'});">所有支付订单</a></li>
										  </ul>
										</div>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   