<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>登录账号</th>
<th>物业机构</th>
<th>用户姓名</th>
<th>手机号</th>
<th>邮箱</th>
<th>性别</th>
<th>用户类型</th>
<th>创建者</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="10">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.userName}"/></td>

<td >
<tool:propertyOrg type="propertyOrg" selection="false" value="${modelObj.propertyId }"></tool:propertyOrg>
</td>
<td ><c:out value="${modelObj.realName}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:out value="${modelObj.email}"/></td>
<td ><tool:code type="sex" selection="false" value="${modelObj.sex}"></tool:code></td>
<td >
	<tool:code type="propertyUserType" selection="false" value="${modelObj.userType}"></tool:code>
</td>
<td ><c:out value="${modelObj.parentRealName}"/></td>
                                    <td>
	                                    <div class="dropdown">
										  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
										  <ul class="dropdown-menu pull-right">
										  <secure:hasPermission name="propertyuser_edit">
										    <li><a href="javascript:openWindow({url:'editPropertyUser.do?id=<c:out value="${modelObj.id}"/>',complete:loadData});">编辑</a></li>
										    </secure:hasPermission>
										  </ul>
										</div>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   