<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/mainHeader.jsp"%>
<style type="text/css">
.page-header {
	border-bottom: 1px solid #a2a2a2;
	padding: 5px 0;
	background-color: #f1f1f1;
	margin: 0px 0px;
}

.page-header-container {
	background-color: #f1f1f1;
	padding: 0px;
}

.page-left {
	float: left;
	width: 201px;
	background-color: #ffffff;
	border-right: 1px solid #a2a2a2;
}

.page-main {
	float: left;
}

.header {
	min-height: 40px;
	z-index: 1000;
	border-radius: 0;
	border: 0px;
	margin: 0px;
}

.pagecontainer {
	overflow: hidden;
}

.nav-primary>li>a {
	border-right: 0px;
}

.menu>.nav>li>.nav>li>a {
	border-right: 0px;
}

.menu>.nav>li.show:last-child>.nav>li:last-child>a {
	border-bottom-right-radius: 0px;
	border-bottom-left-radius: 0px;
}
</style>

</head>
<body style=" background: #f5f7fa url(../res/images/property/background.png) center;">
<header >
<div class="container">
 <div class="navbar-header">
 <a class="navbar-brand"><img src="../res/images/logo/logo-xwc-platform.png"/></a>
 </div>
</div>
</header>

<div id="loginpanel"  style="width:100%;height:500px; position: static; top:50%">

<div  class="container-fixed-sm"
		style="width:600px; height: 220px; padding:20px; opacity:0.85;box-shadow:0 1px 1px rgba(0, 0, 0, 0.1); ">
		<div class="panel">
			<div class="panel-heading ">
				<h4>登录</h4>
			</div>
			<div class="panel-body">
				<div class="form-panel">

					<form action="login.do" method="POST" class="form-horizontal">
						<div class="form-inline">
							<div style="color: red; font-size: 22px;" class="ml20">${message_login}</div>

						</div>
						<div class="form-inline">
							<div class="form-group">
								<label for="_username">账号：</label><input type="text"
									name="username" id="_username" autofocus="autofocus"
									class="form-control w400" />
							</div>
						</div>
						<div class="form-inline">
							<div class="form-group">
								<label for="_password">密码：</label><input type="password"
									name="password" id="_password" class="form-control w400" />
							</div>
						</div>
						<div class="form-inline">
							<div class="form-group">
								<label for="_verifyCode">验证：</label><input type="text"
									name="verifyCode" id="_verifyCode" class="form-control"
									style="width: 300px" /> &nbsp;&nbsp; <img id="verifyCodeImage"
									onclick="reloadVerifyCode()" src="getVerifyCodeImage.do"
									style="cursor: pointer;" title="点击重新成生验证码" />
							</div>
						</div>

						<input type="submit" value="登录" class="btn-primary btn"
							style="width: 120px; margin-left: 30px;" />
					</form>
				</div>
			</div>
		</div>
	</div>
	 
	</div>
	<script type="text/javascript">
$(function(){
	var width=$(window).width();
	var height=$(window).height();
	var l_top=(height-350)/2;
	if(l_top>80)
		{
		l_top=l_top-50;
		}
	$("#loginpanel").css("margin-top",l_top+'px');
	
	if(self != top) { 
		　　top.location.href=window.location.href;
	}
});
function reloadVerifyCode(){  
	 var today=new Date();
    $('#verifyCodeImage').attr('src', 'getVerifyCodeImage.do?12'+today.getSeconds());  
}  

</script>
</body>
</html>