<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"propertyFinancesList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 function deletePropertyFinances(id)
 {
	 confirmLayer({msg:"确定要删除？",yesFn:function(){
		 loadJsonData({url:"deletePropertyFinances.do?id="+id,complete:function(resultData){
			 if(resultData.code)
				 {
				 queryStart();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
 }
 
 </script>
</head>
<body>
<div id="activity">
<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<input type="hidden" name="propertyId" value="<c:out value="${propertyId }"/>" >
				 <div class="form-group">
			 	<label for="startDate">时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			<div class="form-group">
			<input type="submit" class="btn btn-primary" id="queryBtn" value="查询" />
			<%-- 
			<input type="button" class="btn btn-primary" id="addBtn" onclick="openWindow({ close:queryStart,url:'addPropertyFinances.do?propertyId=<c:out value="${propertyId }"/>'})" value="新增" />
			--%>
			</div>
			</form>
</div>
	
<div id="datalist" class="datalist">
<table class="table table-bordered">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>ID</th>
<th>物业机构ID</th>
<th>资金机构ID</th>
<th>授信额度</th>
<th>匹配日期</th>
<th>还款方式</th>
<th>担保方式</th>
<th>有效状态</th>
<th>利息</th>
<th>附件</th>
<th>创建时间</th>
<th>修改时间</th>
<th>操作记录ID</th>

<th>操作</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div>
</body>
</html>