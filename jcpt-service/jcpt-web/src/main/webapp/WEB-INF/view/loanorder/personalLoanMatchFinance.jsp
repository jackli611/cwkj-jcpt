<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <%@ include file="/WEB-INF/view/common/include/header.jsp" %>
    <script type="text/javascript">
        var financeWinId=null;
        function callBackFinanceProduct(id)
        {

            loadJsonData({url:"addPersonalLoanMatchFinance.do",data:{productId:id,loanId:'<c:out value="${loanId}"/>'},complete:function(resultData){
                    closeWindow(financeWinId);
                    if(resultData.code)
                    {
                        loadData();
                    }else {
                        alertLayer(resultData.msg);
                    }
                }});

        }
        function loadData()
        {
            loadTableData({id:"datalist",url:"personalLoanMatchFinance.do",data:{loanId:'<c:out value="${loanId}"/>'}});
        }

        function financeOrgChange() {
            var financeId=$("#financeId").val();
            if(financeId!=""){
                financeWinId=openWindow({url:"../products/financesPersonalLoanProductList.do?financeId="+financeId});
            }

        }

        function toCustomerTypeText(value)
        {
            var result=value;
            if(undefined!=value && value!="null")
            {
                var text=value.replace(/\-100/g,",其他").replace(/\-101/g,",白领（上班族）").replace(/\-102/g,",业主（其他）").replace(/\-103/g,",业主（个体户）").replace(/\-104/g,",业主（白领）").replace(/\-105/g,",业主（中小企业主）");
                if(text.length>0)
                {
                    text=text.substr(1);
                }
                result=text;
            }
            return result;
        }
    </script>
</head>
<body>
<div id="activity">
    <div class="form-panel">
        <form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
            <input type="hidden" name="pageIndex" value="1" id="pageIndex">
            <input type="hidden" name="pageSize" value="20" >
            <input type="hidden" id="loanId" name="loanId" value="<c:out value="${loanId}"/>" >
<div class="form-group">
    <label>添加资金机构</label>
    <select name="financeId" id="financeId" onchange="financeOrgChange()">
        <option value="">请选择</option>
        <c:forEach items="${financeOrgDoList}" var="modelObj" varStatus="status">
            <option value="<c:out value="${modelObj.id}"/>"><c:out value="${modelObj.names}"/></option>
        </c:forEach>

    </select>

</div>
        </form>
    </div>

    <div id="datalist" class="datalist">
        <table class="table table-bordered table-hover">
            <thead>
            <tr >
                <th style="width:60px; min-width: 60px;">序号</th>
                <th>资金机构</th>
                <th>产品名称</th>
                <th>产品类型</th>
                <th>客户类型</th>
                <th>费用说明</th>
                <th>征信</th>
                <th>办理说明</th>
                <th>匹配时间</th>
            </tr>
            </thead>
            <tbody  >
            <c:if test="${empty productOrderList}">
            <tr class=gvItem>
                <td align="center" colspan="10">没有匹配记录！</td>
            </tr>
            </tbody>
        </table>
        </c:if>
        <c:if test="${not empty productOrderList }">
        <c:forEach items="${productOrderList}" var="modelObj" varStatus="status">
            <tr >
                <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
                <td ><c:out value="${modelObj.financeName}"/></td>
                <td ><c:out value="${modelObj.productName}"/></td>
                <td ><tool:code type="personalLoanProductType" selection="false" value="${modelObj.productType}"></tool:code></td>
                <td >
                    <xwckj:personalLoanProductType value="${modelObj.customerType}" selection="false" />
                </td>
                <td ><c:out value="${modelObj.fees}"/></td>
                <td ><c:out value="${modelObj.credit}"/></td>
                <td ><c:out value="${modelObj.transaction}"/></td>
                <td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
            </tr>
        </c:forEach>
        </tbody>
        </table>
        </c:if>
    </div>
</div>
</body>
</html>