<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
    <th style="width:60px; min-width: 60px;">序号</th>
    <th>资金机构</th>
    <th>产品名称</th>
    <th>产品类型</th>
    <th>客户类型</th>
    <th>费用说明</th>
    <th>征信</th>
    <th>办理说明</th>
    <th>匹配时间</th>
</tr>
</thead>
<tbody  >
<c:if test="${empty productOrderList}">
    <tr class=gvItem>
        <td align="center" colspan="10">没有匹配记录！</td>
    </tr>
    </tbody>
    </table>
</c:if>
<c:if test="${not empty productOrderList }">
    <c:forEach items="${productOrderList}" var="modelObj" varStatus="status">
        <tr >
            <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
            <td ><c:out value="${modelObj.financeName}"/></td>
            <td ><c:out value="${modelObj.productName}"/></td>
            <td ><tool:code type="personalLoanProductType" selection="false" value="${modelObj.productType}"></tool:code></td>
            <td > <xwckj:personalLoanProductType value="${modelObj.customerType}" selection="false" /></td>
            <td ><c:out value="${modelObj.fees}"/></td>
            <td ><c:out value="${modelObj.credit}"/></td>
            <td ><c:out value="${modelObj.transaction}"/></td>
            <td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
        </tr>
    </c:forEach>
    </tbody>
    </table>
</c:if>
