<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>资金机构</th>

<th>有效状态</th>
<th>操作</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
<tr class=gvItem>
<td align="center" colspan="9">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty pagedata.page }">
<c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
		<td >${modelObj.shortName}</td>
		
		<td ><c:out value="${modelObj.status}"/></td>
        <td>
            <a href="##" onclick="matchOrderFn('${modelObj.id}','${modelObj.id}')"   >将当前订单分配给此资金方</a>
		</td>
 </tr>
</c:forEach>
</tbody>
</table>
	
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>
<script type="text/javascript">
	function matchOrderFn(finaceId,propertyFinanceId){
		var loanId = $("#loanId").val();
		confirmLayer({msg:"确定将订单分配给此资金方？",yesFn:function(){
			 loadJsonData({url:"saveMatchFinance.do?loanId="+loanId+"&finaceId="+finaceId+"&propertyFinanceId="+propertyFinanceId,complete:function(resultData){
				 if(resultData.code){
					 queryStart();
				  }
				 alertLayer(resultData.msg);
			 }});
		 }});
		return false;
	}
</script>

	
   