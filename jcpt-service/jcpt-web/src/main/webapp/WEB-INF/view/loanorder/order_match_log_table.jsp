<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="hhn" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tool" uri="/WEB-INF/taglib/tool.tld"%>


<table class="table table-none-border k_panel_margin">
	<c:if test="${not empty matchLogLst}">
	<c:forEach items="${matchLogLst }" var="item">
	<tr>
		<td>订单号</td><td>${item.ordercode}</td><td>资金机构</td><td>${item.shortName}</td><td>操作用户</td><td>${item.userName}</td><td>操作日期</td><td><f:formatDate value="${item.createtime}" pattern="yyyy-MM-dd HH:mm:ss"/> </td>
	</tr>
	</c:forEach>
	</c:if>
	
	<c:if test="${empty matchLogLst}">
	<tr>
		<td>暂无匹配记录</td>
	</tr>
	</c:if>
	
</table>