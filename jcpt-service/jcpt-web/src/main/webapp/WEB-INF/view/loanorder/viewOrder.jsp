<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<div class="container-fluid">
	<div class="page-header">
		  <a class="active btn" onclick="javascript:history.go(-1);">返回</a>
		  <a class="btn" onclick="openWindow({url:'<c:url value="/loanorder/ordermatch.do"/>?propertyId=${propertyDo.id}&loanId=${order.loanid}'});">匹配资金方</a>
		  <a class="btn" onclick="openWindow({url:'<c:url value="/audit/audit.do"/>?propertyId=${propertyDo.id}&loanId=${order.loanid}'});">订单进度修改</a>
	</div>

	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">核心企业基本信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>企业名称:${propertyDo.names}</td>
				</tr>
				<tr>
					<td>合作状态:<c:if test="${propertyDo.status eq 1 }">合作中</c:if>
							   <c:if test="${propertyDo.status eq 2 }">未合作</c:if>
					</td>
				</tr>
				<tr>
					<td>合作日期:<f:formatDate value="${propertyDo.createTime}" pattern="yyyy-MM-dd"/> </td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	
	<div id="person">
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">资金方基本信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>金融机构:${financeOrgDo.names}</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">借款信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				
				<tr>
					<td>申请借款日期:<f:formatDate value="${order.applydate}" pattern="yyyy-MM-dd"/></td>
				</tr>
				<tr>
					<td>申请借款金额:<f:formatNumber value="${order.applyamount}" pattern="#,#00.0#"/></td>
				</tr>
				<tr>
					<td>借款期限:${order.loanperiod}期</td>
				</tr>
				<tr>
					<td>借款利率:<f:formatNumber value="${order.applyrate}" pattern="#,#0.0#"/>%(利息由手续费和资金方利息构成)</td>
				</tr>
				<tr>
					<td>综合利息: <f:formatNumber value="${order.applyamount*order.applyrate/100}" pattern="#,#00.0#"/></td>
				</tr>
				<tr>
					<td>还款方式:${repayName}</td>
				</tr>
				<tr>
					<td>订单状态:
						<c:if test="${order.loanstatus eq 'PENDING'}"> 待审批(应付方一审)</c:if>
						<c:if test="${order.loanstatus eq 'PROCESSING'}"> 待审批(应付方二审)</c:if>
						<c:if test="${order.loanstatus eq 'AUDITED'}"> 待放款</c:if>
						<c:if test="${order.loanstatus eq 'REPAYING'}"> 还款中</c:if>
						<c:if test="${order.loanstatus eq 'DRAFT'}">草稿</c:if>
						<c:if test="${order.loanstatus eq 'NOPASS'}">请修改</c:if>
						<c:if test="${order.loanstatus eq 'INVALID'}">审批不通过</c:if>
					</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	<%-- 
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">放款账户信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>账户</td>
				</tr>
				<tr>
					<td>银行名称:</td>
				</tr>
				<tr>
					<td>放款日期:</td>
				</tr>
				
			</table>
		</div>
		</div>
	</div>
	--%>
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">附件</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
		  	<c:forEach items="${attachments}" var="attItem">
				<tr>
					<td>
						<span>附件名：</span><span>${attItem.certificatename}</span>
						<span>文件格式：</span><span>${attItem.filetype}</span>
						<span>文件类别：</span><span>合同</span>
						<span><a onclick="showAttachment('${attItem.id}');">查看附件</a></span><span>${attItem.originalfilename}</span>						
					</td>
				</tr>
		  	</c:forEach>
			</table>
		</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		//加载订单个人信息
		var orderHeaderParam = {"loanId":"${order.loanid}"};
		var prodcode = '${order.prodcode}';
		if(prodcode == 'PERSONAL_XY'){
			$("#person").load("<c:url value='/loanorder/getLoanPerson.do'/>",orderHeaderParam);
		}
		//end 加载订单个人信息
	});
</script>

<script type="text/javascript">

function showAttachment(descFileId){
	  openWindow({url:'<c:url value="/toViewAttachment.do?attachmentId="/>'+descFileId});
}

<!--
$(function() {
    $('[data-toggle="lightbox"]').lightbox();
});
//-->
</script>