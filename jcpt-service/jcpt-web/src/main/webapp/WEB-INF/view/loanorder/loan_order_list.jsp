<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
$(function(){
	queryStart();
})


 function loadData()
 {
 	loadTableData({id:"datalist",url:"<c:url value='/loanorder/orderList.do'/>",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 

 
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<input type="hidden" name="id" value="${id}" >
			<input type="hidden" name="orgType" value="${orgType}" >
			
				
			 <div class="form-group">
			 	<label for="names">借款企业名称：</label>
			  	<input id="names" class="form-control" type="text" name="names"    />
			</div>
			<div class="form-group">
			 	<label for="queryLoanstatus">订单状态：</label>
			  	<select id="queryLoanstatus" name="queryLoanstatus">
			  		<option value="">请选择</option>
			  		<option value="PENDING">待供应商一审</option>
			  		<option value="PROCESSING">待供应商二审</option>
			  		<option value="AUDITED">待放款</option>
			  	</select>
			</div>
			 
			 <div class="form-group">
			 <input type="submit" class="btn" id="queryBtn" value="查询" />
			
			</div>
			</form>
		</div>
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
			<table class="table table-bordered">
				<thead>
				<tr >
					<th style="width:60px; min-width: 60px;">序号</th>
					<th>借款订单号</th>
					<th>申请日期</th>
					<th>借款企业</th>
					<th>应付金额</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
				</thead>
				<tbody  >
				    
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>