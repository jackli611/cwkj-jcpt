<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
	<table class="table table-hover">
		<thead>
			<tr >
				<th style="width:30px; min-width: 30px;">序号</th>
				<th>借款订单号</th>
				<th>申请日期</th>
				<th>借款企业</th>
				<th>应付金额</th>
				<th>状态</th>
				<th>操作</th>
			</tr>
			</thead>
			
 			<c:if test="${empty pagedata.page}">
 				<tbody  >
	             <tr class=gvItem>
	                 <td align="center" colspan="6">查询数据为空！</td>
	             </tr>
	            </tbody>
             </c:if>
             
                        
             <c:if test="${not empty pagedata.page }">
                    <tbody>
                    <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                     <tr >
                            <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
							<td ><c:out value="${modelObj.ordercode}"/></td>
							<td ><f:formatDate value="${modelObj.applydate}"  pattern="yyyy-MM-dd"/></td>
							<td ><c:out value="${modelObj.suppliername}"/></td>
							<td ><c:out value="${modelObj.payamount}"/></td>
							<td >
							    <%--
								<c:if test="${order.loanstatus eq 'TREATY'}"> 待付尾款</c:if>
								<c:if test="${modelObj.loanstatus eq 'PENDING'}"> 待审批</c:if>
								<c:if test="${modelObj.loanstatus eq 'PROCESSING'}"> 待审批</c:if>
								<c:if test="${modelObj.loanstatus eq 'AUDITED'}"> 待放款</c:if>
								<c:if test="${modelObj.loanstatus eq 'REPAYING'}"> 还款中</c:if>
								<c:if test="${modelObj.loanstatus eq 'PAY001'}"> 已付定金</c:if>
								<c:if test="${modelObj.loanstatus eq 'PAY002'}"> 已付尾款</c:if>
								<c:if test="${modelObj.loanstatus eq 'DRAFT'}">草稿</c:if>
								<c:if test="${modelObj.loanstatus eq 'NOPASS'}">请修改</c:if>
								<c:if test="${modelObj.loanstatus eq 'INVALID'}">审批不通过</c:if>
								 --%>
								<tool:code type="scfloanstatus" selection="false" value="${modelObj.loanstatus}"></tool:code>
							</td>
							<td>
						          <a href="<c:url value="/loanorder/viewOrder.do?loanid="/>${modelObj.loanid}"  >查看详情</a>
							</td>
                     </tr>
                    </c:forEach>
                     </tbody>
               </c:if>
	</table>
	
	 <c:if test="${not empty pagedata.page }">
	 <ul class="pager" id="pager"></ul>
	 </c:if>
<script type="text/javascript">
	function showOrder(loanId){
		  openWindow({url:'<c:url value="/loanorder/viewOrder.do?loanid="/>'+loanId});
	}
</script>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script> 
    