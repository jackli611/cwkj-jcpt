<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url){
	 $("#remark").val($.trim($("#remark").text()));
	 $("#form_1").attr("action",url);
	
	 $("#form_1").submit();
	 /*
	 $.ajax({
         type:"POST",
         dataType:"json",
         url:url,
         data: $("#form_1").serialize(),
         async:false,
         success:function(data){
             if(data.code == "1"){
            	 alertLayer("审批成功");
            	 setTimeout( function(){
            		 window.parent.location.reload();//刷新父页面
                     parent.layer.close(auditWinIndex);//关闭弹出层
				}, 4000);
            	 
             }else{
            	 alertLayer(data.msg);
             }
         }
     });
	 */
	 
 }

 </script>
        
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
				<input type="hidden" id="loanid" name="loanid" value="${loanid}">
				<input type="hidden" id="step" name="step" value="${step}">
				
				<div class="form-group  form-inline">
					<label for="approveChoose" class="monospaced_lg">审批动作:</label>
					<select class="form-control" name="resultstatus"  id="resultstatus">
						<tool:code type="scfloanstatus" selection="true" value="${order.loanstatus}"></tool:code>
						
					</select>
				</div>
				<div class="form-group  form-inline">
					<label for="rate" class="monospaced_lg">备注:</label>
					<textarea rows="8" cols="30" id="remark" name="remark">
						
					</textarea>
				</div>
				
				 <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<input type="button" onclick='submitFn("<c:url value='/audit/saveAudit.do'/>")' class="btn" id="auditBtn" value="确定" />
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
$(function(){
	$("#resultstatus").on("change",function(){
		var checkText=$("#resultstatus").find("option:selected").text(); //获取Select选择的
		$("#remark").text(checkText);
	});
});
</script>