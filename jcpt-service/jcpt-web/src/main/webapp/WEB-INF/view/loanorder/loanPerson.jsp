<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/taglib/tool.tld" prefix="tool" %>

<div class="panel panel-default k_container_bg_color">
  <div class="panel-body">
  	<h4 class="title">借款用户基本信息</h4>
	<div class="pull-left">
	  	<table class="table table-none-border k_panel_margin">
			<c:choose>
				<c:when test="${order.loantype eq 4 }">
				<tr><td><span>最近一年收入：${person.incomeAmt}万元</span></td></tr>
				<tr><td><span>当前负债总额：${person.debtAmt}万元</span></td></tr>
				<tr><td><span>最近一年纳税：${person.taxesAmt}万元</span></td></tr>
				<tr><td><span>最近一年企业所得税：${person.incomeTaxAmt}万元</span></td></tr>
				<tr><td><span>企业在人民银行的逾期次数：${person.overdueTime}</span></td></tr>
				<tr><td><span>大股东在人民银行的逾期次数：${person.shareholderOverdueTime}</span></td></tr>
			</c:when>
				<c:when test="${order.loantype eq 2 }">
					<tr>
						<td><span>被抵押红本房在哪里：<tool:code type="whereHouse" selection="false" value="${person.wherehouse}"></tool:code></span></td>
					</tr>
					<tr>
						<td><span>红本房类型：<tool:code type="redBookType" selection="false" value="${person.redbooktype}"></tool:code></span></td>
					</tr>
					<tr>
						<td><span>被抵押房子价格：${order.person.houseprice}</span></td>
					</tr>
					<tr><td><span>职业：<tool:code type="job" selection="false" value="${person.job}"></tool:code></span></td></tr>
					<tr><td><span>是否有社保：<tool:code type="insurance" selection="false" value="${person.insurance}"></tool:code></span></td></tr>
					<tr><td><span>是否有公积金：<tool:code type="providentfund" selection="false" value="${person.providentfund}"></tool:code></span></td></tr>
					<tr><td><span>人行逾期次数：<tool:code type="creditreport" selection="false" value="${person.creditreport}"></tool:code></span></td></tr>
					<tr><td><span>所住小区/楼：${person.community}</span></td></tr>
				</c:when>
				<c:when test="${order.loantype eq 3 }">
					<tr>
						<td><span>红本被抵押在哪里： ${order.whereRedBook}"></span></td>
					</tr>
					<tr><td><span>职业：<tool:code type="job" selection="false" value="${person.job}"></tool:code></span></td></tr>
					<tr><td><span>是否有社保：<tool:code type="insurance" selection="false" value="${person.insurance}"></tool:code></span></td></tr>
					<tr><td><span>是否有公积金：<tool:code type="providentfund" selection="false" value="${person.providentfund}"></tool:code></span></td></tr>
					<tr><td><span>人行逾期次数：<tool:code type="creditreport" selection="false" value="${person.creditreport}"></tool:code></span></td></tr>
					<tr><td><span>所住小区/楼：${person.community}</span></td></tr>
				</c:when>
				<c:otherwise>
					<tr><td><strong>姓名：${person.realname}</strong></td></tr>
					<tr><td><strong>身份证：${person.idno}</strong></td></tr>
					<tr>
						<td><span>是否抵押房子：<tool:code type="deductHouse" selection="false" value="${person.deducthouse}"></tool:code></span></td>
					</tr>
					<tr>
						<td><span>是否有房：<tool:code type="hashouse" selection="false" value="${person.hashouse}"></tool:code>(红本)</span></td>
					</tr>
					<tr><td><span>职业：<tool:code type="job" selection="false" value="${person.job}"></tool:code></span></td></tr>
					<tr><td><span>是否有社保：<tool:code type="insurance" selection="false" value="${person.insurance}"></tool:code></span></td></tr>
					<tr><td><span>是否有公积金：<tool:code type="providentfund" selection="false" value="${person.providentfund}"></tool:code></span></td></tr>
					<tr><td><span>人行逾期次数：<tool:code type="creditreport" selection="false" value="${person.creditreport}"></tool:code></span></td></tr>
					<tr><td><span>所住小区/楼：${person.community}</span></td></tr>
				</c:otherwise>
			</c:choose>



		</table>
	</div>
	</div>
</div>