<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">ID</th>
				<th>企业名称</th>
				<th>类型</th>
				<th>产品</th>
				<th>合作日期</th>
				<th>合作状态</th>
				<th>操作</th>
			</tr>
		</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="6">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <c:if test="${modelObj.id ne 'platform' }">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
									<td ><c:out value="${modelObj.names}"/></td>
									<td ><c:out value="${modelObj.orgTypeName}"/></td>
									<td >
										<c:if test="${productCode eq 'SCF' }">供应链金融</c:if>
										<c:if test="${productCode eq 'PERSONAL_XY' }">个贷</c:if>
									</td>
									<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
									<td ><c:if test="${modelObj.status==1}">合作中</c:if><c:if test="${modelObj.status==2}">取消合作</c:if></td>

                                    <td>
	                                  <a href="<c:url value="/loanorder/toOrderListPage.do?"/>productCode=${productCode}&id=<c:out value="${modelObj.id}"/>&orgType=${modelObj.orgType}" >查看订单</a>
									</td>
                             </tr>
                             </c:if>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>

	
   