<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
<div class="container-fluid">
	<div class="page-header">
		 <ol >
		  <li class="btn" onclick="javascript:history.go(-1);">返回</li>
		  <li class="btn" onclick="openWindow({url:'<c:url value="/loanorder/personalLoanMatchFinance.do"/>?loanId=${order.loanid}&currentOrderProductCode=${order.prodcode}'});">匹配资金方</li>
		  <li class="btn" onclick="changeOrderStatus({'loanid':'${order.loanid}','step':'TREATY','resultstatus':'0','stepname':'启动支付尾款 ' , 'tipMsg':'确定启动支付尾款？' });">贷款完成，启动支付尾款</li>
<%--		  <li class="btn" onclick="backMoney({'loanid':'${order.loanid}','step':'BACKMONEY','resultstatus':'0','stepname':'开启退款中 ' , 'tipMsg':'确定退款？' });">开启退款中</li>--%>
		  <li class="btn" onclick="backMoney({'loanid':'${order.loanid}','step':'BACKMONEYOVER','resultstatus':'0','stepname':'退款完成 ' , 'tipMsg':'确定已退款完成？' });">退款</li>
		</ol> 
	</div>

	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">核心企业基本信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>企业名称:${propertyDo.names}</td>
				</tr>
				<tr>
					<td>合作状态:<c:if test="${propertyDo.status eq 1 }">合作中</c:if>
							   <c:if test="${propertyDo.status eq 2 }">未合作</c:if>
					</td>
				</tr>
				<tr>
					<td>合作日期:<f:formatDate value="${propertyDo.createTime}" pattern="yyyy-MM-dd"/> </td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	
	<div id="person">
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">资金方基本信息</h4>
		<div class="pull-left" id="financeProductList">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>金融机构:${financeOrgDo.names}</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">借款信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>借款类型：</td>
					<c:choose>
		        		<c:when test="${order.loantype eq 1 }">信用贷</c:when>
		        		<c:when test="${order.loantype eq 2 }">房屋抵押</c:when>
		        		<c:when test="${order.loantype eq 3 }">赎回红本</c:when>
		        		<c:when test="${order.loantype eq 4 }">企业信用贷</c:when>
		        		<c:when test="${order.loantype eq 5 }">抵押置换</c:when>
		        		<c:otherwise>智能借款</c:otherwise>
		        	</c:choose>
				</tr>
				<tr>
					<td>申请借款日期:<f:formatDate value="${order.applydate}" pattern="yyyy-MM-dd"/></td>
				</tr>
				<tr>
					<td>申请借款金额:<f:formatNumber value="${order.applyamount}" pattern="#,#00.0#"/></td>
				</tr>
				<tr>
					<td>借款期限:
						<c:choose>
		                	<c:when test="${order.loantype eq 3 }">
		            			<tool:code type="howLongForLoan" selection="false" value="${order.loanperiod}"></tool:code> 
		                	</c:when>
		                	<c:when test="${order.loantype eq 4 }">
		            			<tool:code type="howLongForLoan" selection="false" value="${order.loanperiod}"></tool:code> 
		                	</c:when>
		                	<c:otherwise>
		            			<tool:code type="loanPeriod" selection="false" value="${order.loanperiod}"></tool:code> 
		                	</c:otherwise>
		                </c:choose>
					</td>
				</tr>
<%--				<tr>--%>
<%--					<td>借款利率:<f:formatNumber value="${order.applyrate}" pattern="#,#00.0#"/>%(利息由手续费和资金方利息构成)</td>--%>
<%--				</tr>--%>
<%--				<tr>--%>
<%--					<td>综合利息: <f:formatNumber value="${order.applyamount*order.applyrate/100}" pattern="#,#00.0#"/></td>--%>
<%--				</tr>--%>
				<tr>
					<td>还款方式:${repayName}</td>
				</tr>
				<tr>
					<td>订单状态:
						<tool:code type="personloanstatus" selection="false" value="${order.loanstatus}"></tool:code>
					</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
<%--	<div class="panel panel-default k_container_bg_color">--%>
<%--	  <div class="panel-body">--%>
<%--	  	<h4 class="title">放款账户信息</h4>--%>
<%--		<div class="pull-left">--%>
<%--		  <table class="table table-none-border k_panel_margin">--%>
<%--				<tr>--%>
<%--					<td>账户</td>--%>
<%--				</tr>--%>
<%--				<tr>--%>
<%--					<td>银行名称:</td>--%>
<%--				</tr>--%>
<%--				<tr>--%>
<%--					<td>放款日期:</td>--%>
<%--				</tr>--%>
<%--				--%>
<%--			</table>--%>
<%--		</div>--%>
<%--		</div>--%>
<%--	</div>--%>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">合同</h4>
		<div class="pull-left">
		   <a href="../../agreement/serverPersonAgrt.do?loanId=<c:out value="${order.loanid}"/>" target="_blank">居间服务协议</a>
		   <a href="../../agreement/registerPersonAgrt.do?loanId=<c:out value="${order.loanid}"/>" target="_blank">平台会员注册协议</a>
		</div>
		</div>
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">匹配记录</h4>
		<div class="pull-left" id="matchLog">
		
		</div>
	  </div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		//加载订单个人信息
		var orderHeaderParam = {"loanId":"${order.loanid}"};
		$("#person").load("<c:url value='/personorder/getLoanPerson.do'/>",orderHeaderParam);
		//end 加载订单个人信息
		
		//加载匹配记录
		var orderHeaderParam = {"loanId":"${order.loanid}"};
		$("#matchLog").load("<c:url value='/loanorder/getMatchLog.do'/>",orderHeaderParam);
		//end 加载匹配记录
		
	});

</script>

<script type="text/javascript">

function showAttachment(descFileId){
	  openWindow({url:'<c:url value="/toViewAttachment.do?attachmentId="/>'+descFileId});
}

function loadFinanceProductList()
{
	loadTableData({id:"financeProductList",url:"personalLoanMatchFinance.do",data:{loanId:'<c:out value="${order.loanid}"/>'}});
}

<!--
$(function() {
    $('[data-toggle="lightbox"]').lightbox();
	loadFinanceProductList();
});
//-->
</script>
<script type="text/javascript">
	function changeOrderStatus(paramData){
		
		var r=confirm(paramData.tipMsg)
		if (r==true){
			 loadJsonData({url:'<c:url value="/audit/saveAuditByAjx.do?"/>',"data":paramData,complete:function(resultData){
				 if(resultData.code == 1){
					 alertLayer('操作成功');	 
				  }else{
				 	alertLayer(resultData.msg);
				  }
			 }});
		 
		}
		return false;
	}
	
	
	function backMoney(paramData){
		var r=confirm(paramData.tipMsg)
		if (r==true){
			 loadJsonData({url:'<c:url value="/pay/drawback.do?"/>',"data":paramData,complete:function(resultData){
				 if(resultData.code == 1){
					 alertLayer('操作成功');	 
				  }else{
				 	alertLayer(resultData.msg);
				  }
			 }});
		 
		}
		return false;
	}
</script>