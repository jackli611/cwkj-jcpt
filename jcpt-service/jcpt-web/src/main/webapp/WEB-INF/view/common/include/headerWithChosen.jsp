<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp"%>
<link rel="stylesheet" href="<c:url value="/res/css/zui.min.css"/>">
<link rel="stylesheet" href="<c:url value="/res/css/doc.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/res/lib/datatable/zui.datatable.min.css"/>">
<link rel="stylesheet" href="<c:url value="/res/lib/viewer/view.css"/>">
<link rel="stylesheet" href="<c:url value="/res/css/font-5.5-all.min.css"/>">
<link rel="stylesheet" href="<c:url value="/res/css/platform.css"/>">
<link rel="stylesheet" href="<c:url value="/res/css/global.css"/>">
<link rel="stylesheet" href="<c:url value="/res/lib/chosen/chosen.min.css"/>">
<script type="text/javascript" src="<c:url value="/res/lib/chosen/chosen.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/lib/jquery/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/lib/jquery/jquery.form.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/zui.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/lib/datatable/zui.datatable.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/lib/layer/layer.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/lib/My97DatePicker/WdatePicker.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/lib/viewer/viewer-jquery.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/lib/chosen/chosen.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/highcharts.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/jqPaginator.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/jcptTools.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/combox.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/dateutil.js"/>"></script>

<style type="text/css">
.chosen-container-single .chosen-single div {
	top: 10px !important;
}

</style>