<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <%@ include file="/WEB-INF/view/common/include/header.jsp" %>

    <script type="text/javascript">


        function checkedFinanceProduct(id)
        {
            parent.callBackFinanceProduct(id);
        }




    </script>
</head>
<body>
<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
<div>
    <table class="table table-bordered table-hover">
        <thead>
        <tr >
            <th style="width:60px; min-width: 60px;">选择</th>
            <th>产品名称</th>
            <th>产品类型</th>
            <th>客户类型</th>
            <th>费用说明</th>
            <th>征信</th>
            <th>办理说明</th>
        </tr>
        </thead>
        <tbody  >
        <c:if test="${empty financeProductList}">
            <tr class=gvItem>
                <td align="center" colspan="7">查询数据为空！</td>
            </tr>

        </c:if>
        <c:if test="${not empty financeProductList }">
            <c:forEach items="${financeProductList}" var="modelObj" varStatus="status">
                <tr >
                    <td ><input type="radio" value="<c:out value="${modelObj.id}"/>" onclick="checkedFinanceProduct('<c:out value="${modelObj.id}"/>')"></td>
                    <td ><c:out value="${modelObj.productName}"/></td>
                    <td ><tool:code type="personalLoanProductType" selection="false" value="${modelObj.productType}"></tool:code></td>
                    <td > <xwckj:personalLoanProductType value="${modelObj.customerType}"  selection="false"/></td>
                    <td ><c:out value="${modelObj.fees}"/></td>
                    <td ><c:out value="${modelObj.credit}"/></td>
                    <td ><c:out value="${modelObj.transaction}"/></td>
                </tr>
            </c:forEach>
        </c:if>
        </tbody>
    </table>
</div>
</body>
</html>