<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>主键</th>
<th>资金机构ID</th>
<th>产品名称</th>
<th>产品类型</th>
<th>客户类型</th>
<th>费用说明</th>
<th>征信</th>
<th>办理说明</th>
<th>创建时间</th>
<th>修改时间</th>
<th>操作记录ID</th>

<th>操作</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
<tr class=gvItem>
<td align="center" colspan="12">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty pagedata.page }">
<c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
        <td ><c:out value="${modelObj.id}"/></td>
<td ><c:out value="${modelObj.financeId}"/></td>
<td ><c:out value="${modelObj.productName}"/></td>
<td ><c:out value="${modelObj.productType}"/></td>
<td ><c:out value="${modelObj.customerType}"/></td>
<td ><c:out value="${modelObj.fees}"/></td>
<td ><c:out value="${modelObj.credit}"/></td>
<td ><c:out value="${modelObj.transaction}"/></td>
<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
<td ><f:formatDate value="${modelObj.updateTime}"  pattern="yyyy-MM-dd"/></td>
<td ><c:out value="${modelObj.recordUserId}"/></td>

        <td>
            <div class="dropdown">
			  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
			  <ul class="dropdown-menu pull-right">
			   <li><a href="##" onclick="openWindow({'title':'资金机构个贷产品浏览','url':'viewPersonalLoanProduct.do?id=<c:out value="${modelObj.id}"/>'})"   >浏览</a></li>
			   <li><a href="##" onclick="openWindow({'title':'资金机构个贷产品编辑','url':'editPersonalLoanProduct.do?id=<c:out value="${modelObj.id}"/>'})"   >编辑</a></li>
			   <li><a href="##" onclick="deletePersonalLoanProduct('<c:out value="${modelObj.id}"/>')">删除</a></li>
			  </ul>
			</div>
		</td>
 </tr>
</c:forEach>
</tbody>
</table>
	
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>

	
   