<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %> 
</head>
<body>
<div id="activity" >
  <table class="table table-bordered "><tr>
<td>主键</td>
<th><c:out value="${personalLoanProduct.id}"/></th>
</tr>
<tr>
<td>资金机构ID</td>
<th><c:out value="${personalLoanProduct.financeId}"/></th>
</tr>
<tr>
<td>产品名称</td>
<th><c:out value="${personalLoanProduct.productName}"/></th>
</tr>
<tr>
<td>产品类型</td>
<th><c:out value="${personalLoanProduct.productType}"/></th>
</tr>
<tr>
<td>客户类型</td>
<th><c:out value="${personalLoanProduct.customerType}"/></th>
</tr>
<tr>
<td>费用说明</td>
<th><c:out value="${personalLoanProduct.fees}"/></th>
</tr>
<tr>
<td>征信</td>
<th><c:out value="${personalLoanProduct.credit}"/></th>
</tr>
<tr>
<td>办理说明</td>
<th><c:out value="${personalLoanProduct.transaction}"/></th>
</tr>
<tr>
<td>创建时间</td>
<th ><f:formatDate value="${personalLoanProduct.createTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>修改时间</td>
<th ><f:formatDate value="${personalLoanProduct.updateTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>操作记录ID</td>
<th><c:out value="${personalLoanProduct.recordUserId}"/></th>
</tr>
</table>

</div>
</body>
</html>