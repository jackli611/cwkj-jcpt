<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

<script type="text/javascript">
$(function(){
	$('input[name="levelsStr"]').on("click",function(){
		var levelVal = $('input[name="levelsStr"]:checked').val();
		if(levelVal == 1 ){
			$("#prodCodeDiv").hide();
		}
		
		if(levelVal == 2 ){
			$("#prodCodeDiv").show();
		}
		
	});
});
</script>

 <script type="text/javascript">

 function dealLoadCodeFn(code) {
	 if(code==1)
	 {
	 	if(parent.closeWindow)
		{
			parent.loadData();
			window.setTimeout(parent.closeWindow,1000);
		}

	 }
 }
 
 function submitFn(url)
 {
	 var requiredvalue=true;
	 $("input.requiredvalue,select.requiredvalue").each(function(){
		 if($(this).val()=="")
		 {
			 var msgObj=$(this);
			 TipsLayer({msg: msgObj.attr("placeholder")+" 不能为空", obj: msgObj});
			 requiredvalue=false;
			 return false;
		 }
	 });
	 var levels= $('input[name="levelsStr"]:checked').val();
	 if(!levels || levels==""){
		 TipsLayer({msg: $("#levelsRadio").attr("placeholder")+" 不能为空", obj: $("#levelsRadio")});
		 requiredvalue=false;
		 return false;
	 }
	 
	 //如果是二級要求輸入上級產品
	 if("2" == levels){
		 parentCd = $("#parentCode").val();
		 if(!parentCd || parentCd == ""){
			 TipsLayer({msg: "二級产品需要指定一级产品", obj: $("#parentCode")});
			 requiredvalue = false;
		 }
		 //驗證編碼規則
		 var productCode = $("#productCode").val();
		 if(productCode.indexOf(parentCd) != 0) {
			 TipsLayer({msg: "产品编码需以上級产品编码开头加中划线分隔", obj: $("#productCode")});
			 requiredvalue = false;
		 }
	 }

	 if(requiredvalue)
	 {
		 // loadJsonData({url:url,data:$("#form_1").serialize(),complete:function(resultData){
			// 	 alertLayer(resultData.msg);
			// 	 if(resultData.code == 1)
			// 	 {
			// 		 parent.loadData();
			// 		 parent.closeWindow();
			// 	 }
			//  }});

		 $("#form_1").attr("action",url);
		 $("#form_1").submit();
	 }
	 return false;
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
		 
		 		<div class="form-group  form-inline">
					<label for="levels" class="monospaced_lg"><span class="required" >级别:</span></label>
					<label  class="radio-inline"><input type="radio"  <c:if test="${product.levels==1 }">checked="checked"</c:if>  name="levelsStr" value="1" />一级</label>
					<label  class="radio-inline"><input type="radio" id="levelsRadio" placeholder="级别"  <c:if test="${product.levels==2 }">checked="checked"</c:if>   name="levelsStr" value="2" />二级</label>
				</div>
				
				<div class="form-group  form-inline" id="parentDiv"   >
					<label for="parentCode" class="monospaced_lg">一級产品编码:</label>
					<select class="form-control" name="parentCode" id="parentCode">
						<tool:code type="productCode" selection="true" value="${product.parentCode}"></tool:code>
					</select>
					
				</div>
				<div class="form-group  form-inline" id="prodCodeDiv" <c:if test="${product.levels==1 }">style="display: none;"</c:if> >
					 <label for="productCode" class="monospaced_lg"><span class="required" >二级产品编码:</span></label>
					 <input type="text" id="productCode" name="productCode" class="form-control w400" placeholder="产品编码" value="<c:out value="${product.productCode}"/>" />
				</div>
				
		
				<div class="form-group  form-inline">
					<label for="productName" class="monospaced_lg"><span class="required" >产品名称:</span></label>
					<input type="text" id="productName" name="productName" class="form-control w400 requiredvalue" placeholder="产品名称" value="<c:out value="${product.productName}"/>" />
				</div> 
				
				 
				<div class="form-group  form-inline" dis>
					<label for="userType" class="monospaced_lg">用户类型:</label>
					<label  class="checkbox-inline"><input type="checkbox"  <c:if test="${product.enterpriseProduct==1}">checked="checked"</c:if>  name="enterpriseProductStr" value="1" />企业</label>
					<label  class="checkbox-inline"><input type="checkbox" <c:if test="${product.personalProduct==1}">checked="checked"</c:if>   name="personalProductStr" value="1" />个人</label>
				</div> 
				 
				<div class="col-sm-offset-2 col-sm-10">
					<c:if test="${empty product.productCode && empty product.parentCode }">
					<input type="button" onclick='submitFn("addProduct.do")' class="btn" id="addBtn" value="新增" />
					</c:if>
					<c:if test="${not empty product.productCode || not empty  product.parentCode }">
					<input type="button" onclick='submitFn("editProduct.do")' class="btn" id="modifyBtn" value="修改" />
					</c:if>
				</div>
			</form>
		</div>
	</div>
</body>
</html>