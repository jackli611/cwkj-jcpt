<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
 	var customerType="";
 	$("input[name=customerTypeGroup]").each(function () {
		var checked=$(this).prop("checked")?$(this).prop("checked"):$(this).attr("checked");
		if(checked)
		{
			customerType+="-"+$(this).val();
		}
	})
	 $("#customerType").val(customerType);

	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 function writeCustomerTypeText(value)
 {
 	if(undefined!=value && value!="null")
	{
		var text=value.replace(/\-100/g,",其他").replace(/\-101/g,",白领（上班族）").replace(/\-102/g,",业主（其他）").replace(/\-103/g,",业主（个体户）").replace(/\-104/g,",业主（白领）").replace(/\-105/g,",业主（中小企业主）");
		if(text.length>0)
		{
			text=text.substr(1);
		}
		document.write(text);
	}else{
		document.write(text);
	}
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
				<input type="hidden" id="financeId" name="financeId" class="form-control w400" value="<c:out value="${personalLoanProduct.financeId}"/>" />

<div class="form-group  form-inline">
<label for="productName" class="monospaced_lg">产品名称:</label>
<input type="text" id="productName" name="productName" class="form-control w400" value="<c:out value="${personalLoanProduct.productName}"/>" />
</div> 
<div class="form-group  form-inline">
<label class="monospaced_lg">产品类型:</label>
	<select class="form-control" name="productType">
		<option value="" >请选择</option>
		<c:forEach items="${productLst}" var="item">
			<option value="${item.productCode}">${item.productName}</option>
		</c:forEach>
	</select>
</div>
<div class="form-group  form-inline">
<label for="customerType" class="monospaced_lg">客户类型:</label>
	<xwckj:personalLoanProductType value="${modelObj.customerType}" selection="true" />
<input type="hidden" id="customerType" name="customerType" value="<c:out value="${personalLoanProduct.customerType}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="fees" class="monospaced_lg">费用说明:</label>
<textarea   id="fees" name="fees" class="form-control w400"><c:out value="${personalLoanProduct.fees}"/></textarea>
</div> 
<div class="form-group  form-inline">
<label for="credit" class="monospaced_lg">征信:</label>
	<textarea  id="credit" name="credit" class="form-control w400" ><c:out value="${personalLoanProduct.credit}"/></textarea>
</div> 
<div class="form-group  form-inline">
<label for="transaction" class="monospaced_lg">办理说明:</label>
	<textarea id="transaction" name="transaction" class="form-control w400" ><c:out value="${personalLoanProduct.transaction}"/></textarea>
</div> 

 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<input type="button" onclick='submitFn("addPersonalLoanProduct.do")' class="btn btn-primary" id="addBtn" value="保存" />
</div>
			</form>
		</div>
	</div>

<div>

	<table class="table table-bordered table-hover">
		<thead>
		<tr >
			<th style="width:60px; min-width: 60px;">序号</th>
			<th>产品名称</th>
			<th>产品类型</th>
			<th>客户类型</th>
			<th>费用说明</th>
			<th>征信</th>
			<th>办理说明</th>
		</tr>
		</thead>
		<tbody  >
		<c:if test="${empty financeProductList}">
		<tr class=gvItem>
			<td align="center" colspan="7">查询数据为空！</td>
		</tr>

	</c:if>
	<c:if test="${not empty financeProductList }">
	<c:forEach items="${financeProductList}" var="modelObj" varStatus="status">
		<tr >
			<td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
			<td ><c:out value="${modelObj.productName}"/></td>
			<td ><tool:code type="personalLoanProductType" selection="false" value="${modelObj.productType}"></tool:code></td>
			<td ><xwckj:personalLoanProductType value="${modelObj.customerType}" selection="false" /></td>
			<td ><c:out value="${modelObj.fees}"/></td>
			<td ><c:out value="${modelObj.credit}"/></td>
			<td ><c:out value="${modelObj.transaction}"/></td>
		</tr>
	</c:forEach>
	</c:if>
	</tbody>
	</table>
</div>
</body>
</html>