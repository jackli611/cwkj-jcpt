<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>姓名</th>
<th>手机号</th>
<th>车牌号</th>
<th>驾驶证持有人姓名</th>
<th>住址</th>
<th>是否有ETC卡</th>
<th>ETC卡类型</th>
<th>申请时间</th>
<th>操作</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
<tr class=gvItem>
<td align="center" colspan="10">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty pagedata.page }">
<c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.realName}"/></td>
<td ><c:out value="${modelObj.telphone}"/></td>
<td ><c:out value="${modelObj.carno}"/></td>
<td ><c:out value="${modelObj.driverUserName}"/></td>
<td ><c:out value="${modelObj.address}"/></td>
<td ><c:out value="${modelObj.usingCard}"/></td>
<td ><c:out value="${modelObj.cardType}"/></td>
<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>

        <td>
            <div class="dropdown">
			  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
			  <ul class="dropdown-menu pull-right">
			   <li><a href="##" onclick="openWindow({'title':'ETC基本信息浏览','url':'viewEtcProfile.do?id=<c:out value="${modelObj.id}"/>'})"   >浏览</a></li>
			  </ul>
			</div>
		</td>
 </tr>
</c:forEach>
</tbody>
</table>
	
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>

	
   