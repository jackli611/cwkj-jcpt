<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"etcProfileList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }

 
 </script>
</head>
<body>
<div id="activity">
<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
				 <div class="form-group">
			 	<label for="startDate">申请时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
				<div class="form-group">
					<label>手机号：</label>
					<input  class="form-control" type="text" name="telphone" />
				</div>

			<div class="form-group">
			<input type="submit" class="btn btn-primary" id="queryBtn" value="查询" />
			<input type="button" class="btn btn-primary" id="addBtn" onclick="openWindow({url:'addEtcProfile.do'})" value="新增" />
			</div>
			</form>
</div>
	
<div id="datalist" class="datalist">
</div>
</div>
</body>
</html>