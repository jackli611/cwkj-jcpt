<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
			 <div class="form-group  form-inline">
<label for="realName" class="monospaced_lg">姓名:</label>
<input type="text" id="realName" name="realName" class="form-control w400" value="<c:out value="${etcProfile.realName}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="telphone" class="monospaced_lg">手机号:</label>
<input type="text" id="telphone" name="telphone" class="form-control w400" value="<c:out value="${etcProfile.telphone}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="carno" class="monospaced_lg">车牌号:</label>
<input type="text" id="carno" name="carno" class="form-control w400" value="<c:out value="${etcProfile.carno}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="driverUserName" class="monospaced_lg">驾驶证持有人姓名:</label>
<input type="text" id="driverUserName" name="driverUserName" class="form-control w400" value="<c:out value="${etcProfile.driverUserName}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="address" class="monospaced_lg">住址:</label>
<input type="text" id="address" name="address" class="form-control w400" value="<c:out value="${etcProfile.address}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="usingCard" class="monospaced_lg">是否有ETC卡:</label>
<input type="text" id="usingCard" name="usingCard" class="form-control w400" value="<c:out value="${etcProfile.usingCard}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="cardType" class="monospaced_lg">ETC卡类型:</label>
<input type="text" id="cardType" name="cardType" class="form-control w400" value="<c:out value="${etcProfile.cardType}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="userId" class="monospaced_lg">用户ID:</label>
<input type="text" id="userId" name="userId" class="form-control w400" value="<c:out value="${etcProfile.userId}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="applyStatus" class="monospaced_lg">申请状态:</label>
<input type="text" id="applyStatus" name="applyStatus" class="form-control w400" value="<c:out value="${etcProfile.applyStatus}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="createTime" class="monospaced_lg">创建时间:</label>
<input type="text" id="createTime" name="createTime" class="form-control w400" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${etcProfile.createTime}"  pattern="yyyy-MM-dd"/>"  />
</div> 
<div class="form-group  form-inline">
<label for="updateTime" class="monospaced_lg">更新时间:</label>
<input type="text" id="updateTime" name="updateTime" class="form-control w400" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${etcProfile.updateTime}"  pattern="yyyy-MM-dd"/>"  />
</div> 
 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<c:if test="${empty etcProfile.id}" >
<input type="button" onclick='submitFn("addEtcProfile.do")' class="btn btn-primary" id="addBtn" value="保存" />
</c:if>
<c:if test="${not empty etcProfile.id}" >
<input type="hidden" id="id" name="id" value="<c:out value="${etcProfile.id}"/>" />
<input type="button" onclick='submitFn("editEtcProfile.do")' class="btn btn-primary" id="modifyBtn" value="保存" />
</c:if>
</div>
			</form>
		</div>
	</div>
</body>
</html>