<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %> 
</head>
<body>
<div id="activity" >
  <table class="table table-bordered ">
<tr>
<td>姓名</td>
<th><c:out value="${etcProfile.realName}"/></th>
</tr>
<tr>
<td>手机号</td>
<th><c:out value="${etcProfile.telphone}"/></th>
</tr>
<tr>
<td>车牌号</td>
<th><c:out value="${etcProfile.carno}"/></th>
</tr>
<tr>
<td>驾驶证持有人姓名</td>
<th><c:out value="${etcProfile.driverUserName}"/></th>
</tr>
<tr>
<td>住址</td>
<th><c:out value="${etcProfile.address}"/></th>
</tr>
<tr>
<td>是否有ETC卡</td>
<th><c:out value="${etcProfile.usingCard}"/></th>
</tr>
<tr>
<td>ETC卡类型</td>
<th><c:out value="${etcProfile.cardType}"/></th>
</tr>
<tr>
<td>申请状态</td>
<th><c:out value="${etcProfile.applyStatus}"/></th>
</tr>
<tr>
<td>申请时间</td>
<th ><f:formatDate value="${etcProfile.createTime}"  pattern="yyyy-MM-dd"/></th>
</tr>
</table>

</div>
</body>
</html>