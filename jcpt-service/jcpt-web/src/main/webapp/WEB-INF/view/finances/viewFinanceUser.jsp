<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %> 
</head>
<body>
<div id="activity" >
  <table class="table table-bordered "><tr>
<td>ID</td>
<th><c:out value="${financeUser.id}"/></th>
</tr>
<tr>
<td>系统账户ID</td>
<th><c:out value="${financeUser.userId}"/></th>
</tr>
<tr>
<td>父级账户ID</td>
<th><c:out value="${financeUser.parentUserId}"/></th>
</tr>
<tr>
<td>金融机构ID</td>
<th><c:out value="${financeUser.financeId}"/></th>
</tr>
<tr>
<td>用户姓名</td>
<th><c:out value="${financeUser.realName}"/></th>
</tr>
<tr>
<td>手机号</td>
<th><c:out value="${financeUser.mobile}"/></th>
</tr>
<tr>
<td>邮箱</td>
<th><c:out value="${financeUser.email}"/></th>
</tr>
<tr>
<td>性别</td>
<th><c:out value="${financeUser.sex}"/></th>
</tr>
<tr>
<td>可创建账户标识</td>
<th><c:out value="${financeUser.buildUserTag}"/></th>
</tr>
<tr>
<td>创建时间</td>
<th ><f:formatDate value="${financeUser.createTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>修改时间</td>
<th ><f:formatDate value="${financeUser.updateTime}"  pattern="yyyy-MM-dd"/></t>
</tr>
<tr>
<td>操作记录ID</td>
<th><c:out value="${financeUser.recordUserId}"/></th>
</tr>
</table>

</div>
</body>
</html>