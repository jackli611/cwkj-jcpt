<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">
 function dealLoadCodeFn(code) {
	 if(code==1)
	 {
		 if(parent.closeWindow)
		 {
			 parent.loadData();
			 window.setTimeout(parent.closeWindow,1000);
		 }

	 }
 }
 
 function submitFn(url){
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
<input type="hidden" id="propertyId" name="financeId" class="form-control w400" value="<c:out value="${propertyFinances.financeId}"/>" />
<div class="form-group  form-inline">
<label for="propertyId" class="monospaced_lg">核心企业:</label>
<select name="propertyId">
<tool:propertyOrg type="propertyLst" selection="true" value="${propertyFinances.financeId}"></tool:propertyOrg>
</select>
</div> 
<div class="form-group  form-inline">
<label for="creditAmount" class="monospaced_lg">授信额度:</label>
<input type="text" id="creditAmount" name="creditAmount" class="form-control w400 numDecText" value="<c:out value="${propertyFinances.creditAmount}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="metaDateStr" class="monospaced_lg">匹配日期:</label>
<input type="text" id="metaDateStr" name="metaDateStr" class="form-control w400" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${propertyFinances.metaDate}"  pattern="yyyy-MM-dd"/>"  />
</div> 
<%--<div class="form-group  form-inline">--%>
<%--<label for="paymentType" class="monospaced_lg">还款方式:</label>--%>
<%--<select name="paymentType">--%>
<%--<option value="">请选择</option>--%>
<%--<tool:code type="paymentType" selection="true" value="${propertyFinances.paymentType}"></tool:code>--%>
<%--</select>--%>
<%--</div> --%>
<div class="form-group  form-inline">
<label for="guaranteeType" class="monospaced_lg">担保方式:</label>
<select name="guaranteeType">
<option value="">请选择</option>
<tool:code type="guaranteeType" selection="true" value="${propertyFinances.guaranteeType}"></tool:code>
</select>
</div> 
<%--<div class="form-group  form-inline">--%>
<%--<label for="interestRate" class="monospaced_lg">利息:</label>--%>
<%--<input type="text" id="interestRate" name="interestRate" class="form-control w400 numDecText" value="<c:out value="${propertyFinances.interestRate}"/>" />--%>
<%--</div> --%>
 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<c:if test="${empty propertyFinances.id}" >
<input type="button" onclick='submitFn("addPropertyFinances.do")' class="btn btn-primary" id="addBtn" value="保存" />
</c:if>
<c:if test="${not empty propertyFinances.id}" >
<input type="hidden" id="id" name="id" value="<c:out value="${propertyFinances.id}"/>" />
<input type="button" onclick='submitFn("editPropertyFinances.do")' class="btn btn-primary" id="modifyBtn" value="保存" />
</c:if>
</div>
			</form>
		</div>
	</div>
</body>
</html>