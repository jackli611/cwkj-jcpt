<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url){
	 $("#productname").val($("#productcode  option:selected").text());
	 $("#repayname").val($("#repaycode  option:selected").text());
	 $.ajax({
         type:"POST",
         dataType:"json",
         url:url,
         data: $("#form_1").serialize(),
         async:false,
         success:function(data){
             if(data.code == "1"){
            	 alertLayer(data.msg);
            	 loadData();
            	 
             }else{
            	 alertLayer(data.msg);
             }
         }
     });
 }

 function loadData()
 {
 	loadTableData({id:"datalist",url:"companyProductList.do",data:{propertyOrgId:'${companyProduct.propertyorgid}'}});
 }
 $(function () {
	 loadData();
 })

 </script>
        
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
				<input type="hidden" id="propertyorgid" name="propertyorgid" value="${companyProduct.propertyorgid}">
				<input type="hidden" id="assignid" name="assignid" value="${companyProduct.assignid}">
				<input type="hidden" id="productname" name="productname" value="${companyProduct.productname}">
				<input type="hidden" id="repayname" name="repayname" value="${companyProduct.repayname}">
				<input type="hidden" id="levels" name="levels" value="1">
				<div class="form-group  form-inline">
					<label for="productcode" class="monospaced_lg">分配产品:</label>
					<select class="form-control" name="productcode" id="productcode">
						<option value="" >请选择</option>
						<c:forEach items="${productLst}" var="prod">
							<option value="${prod.productCode}"  <c:if test="${companyProduct.productcode eq prod.productCode }"> selected="selected"</c:if> >${prod.productName}</option>
						</c:forEach>
					</select>
				</div>
				
				<div class="form-group  form-inline">
					<label for="repaycode" class="monospaced_lg">还款方式:</label>
					<select class="form-control" name="repaycode" id="repaycode">
					<option value="" >请选择</option>
						<tool:code type="repayCode" selection="true" value="${companyProduct.repaycode}"></tool:code>
					</select>
				</div>
<%--				<div class="form-group  form-inline">--%>
<%--					<label for="leves" class="monospaced_lg">利率等级:</label>--%>
<%--					<select class="form-control" name="levels">--%>
<%--						<tool:code type="levels" selection="true" value="${companyProduct.levels}"></tool:code>--%>
<%--					</select>--%>
<%--				</div>--%>
				<div class="form-group  form-inline">
					<label for="rate" class="monospaced_lg">利率:</label>
					<input type="text" id="rate" name="rate" class="form-control w400" value="<c:out value="${companyProduct.rate}"/>" />%
				</div> 
				<div class="form-group  form-inline">
					<label for="minPeroid" class="monospaced_lg">最小借款周期:</label>
					<input type="text" id="minperoid" name="minperoid" class="form-control w400" value="<c:out value="${companyProduct.minperoid}"/>" />(月)
				</div>
				<div class="form-group  form-inline">
					<label for="maxPeroid" class="monospaced_lg">最大借款周期:</label>
					<input type="text" id="maxperoid" name="maxperoid" class="form-control w400" value="<c:out value="${companyProduct.maxperoid}"/>" />(月)
				</div>
				<div class="form-group  form-inline">
					<label for="status" class="monospaced_lg">状态:</label>
					<select class="form-control" name="status">
						<option value="1"  <c:if test="${companyProduct.status eq '1' }"> selected="selected"</c:if> >有效</option>
						<option value="0"  <c:if test="${companyProduct.status eq '0' }"> selected="selected"</c:if> >无效</option>
					</select>
				</div>
				
				 <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<secure:hasPermission name="company_add">
						<c:if test="${empty companyProduct.assignid }">
						<input type="button" onclick='submitFn("saveCompanyProductByAjax.do")' class="btn" id="addBtn" value="保存" />
						</c:if>
						</secure:hasPermission>
						<secure:hasPermission name="company_edit">
						<c:if test="${not empty companyProduct.assignid }">
						<input type="button" onclick='submitFn("saveCompanyProductByAjax.do")' class="btn" id="modifyBtn" value="保存" />
						</c:if>
						</secure:hasPermission>
					</div>
				</div>
			</form>
			
			<div id="datalist" class="datalist">
				<table class="table table-bordered">
					<thead>
						<tr >
							<th style="width:60px; min-width: 60px;">序号</th>
							<th>分配产品编码</th>
							<th>分配产品名称</th>
							<th>还款方式</th>
							<th>利率等级</th>
							<th>利率</th>
							<th>借款周期</th>
							<th>状态</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
			</table>
			</div>
		
		</div>
	</div>
</body>
</html>