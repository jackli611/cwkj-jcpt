<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
    <th style="width:60px; min-width: 60px;">序号</th>
    <th>申请人姓名</th>
    <th>申请人手机号</th>
    <th>申请时间</th>
    <th>产品类型</th>
    <th>订单编号</th>
    <th>订单状态</th>
    <th>订单来源</th>
    <th>一级推荐人手机号</th>
    <th>二级推荐人手机号</th>
    <th>操作</th>
</tr>
</thead>
<tbody  >
<c:if test="${empty pagedata.page}">
    <tr class=gvItem>
        <td align="center" colspan="11">查询数据为空！</td>
    </tr>
    </tbody>
    </table>
</c:if>
<c:if test="${not empty pagedata.page }">
    <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
        <tr >
            <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
            <td ><c:out value="${modelObj.orderRealName}"/></td>
            <td ><c:out value="${modelObj.orderUserTelphone}"/></td>
            <td ><f:formatDate value="${modelObj.orderTime}" pattern="yyyy-MM-dd" /></td>
            <td ><tool:code type="orderType" value="${modelObj.orderType}" selection="false"/></td>
            <td ><c:out value="${modelObj.orderCode}"/></td>
            <td ><tool:code type="personloanstatus" value="${modelObj.orderStatus}" selection="false"/>
               </td>
            <td ><tool:code type="orderSource" value="${modelObj.orderSource}" selection="false"/></td>
            <td ><c:out value="${modelObj.oneLevelShareTelphone}"/></td>
            <td ><c:out value="${modelObj.twoLeavelShareTelphone}"/></td>

            <td>
                <div class="dropdown">
                    <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
                    <ul class="dropdown-menu pull-right">
                            <li><a href="#" onclick="openWindow({'title':'订单详情','url':'../loanorder/viewOrder.do?loanid=<c:out value="${modelObj.loanId}"/>'})" >订单详情</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    </c:forEach>
    </tbody>
    </table>

    <ul class="pager" id="pager">
    </ul>
    <script type="text/javascript">
        <!--
        $(function(){
            jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
        });
        //-->
    </script>
</c:if>


