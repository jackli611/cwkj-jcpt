<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
    <div>
    	<span>平台电子号：<c:out value="${huishangflows.CARDNBR}"/></span>
    	<span>平台电子账户名：<c:out value="${huishangflows.NAME}"/></span>
    </div>
   
	<table class="table table-hover">
		<thead>
			<tr >
				<th style="width:30px; min-width: 30px;">序号</th>
				<th>入帐日期</th>
				<th>交易日期</th>
				<th>自然日期</th>
				<th>交易时间</th>
				<th>交易流水号</th>
				<th>交易类型</th>
				<th>冲正/撤销标志</th>
				<th>交易金额</th>
				<th>交易金额符号</th>
				<th>授权代码</th>
				<th>交易描述</th>
				<th>交易后余额</th>
				<th>摘要</th>
				<th>对手交易账号</th>
				<th>保留域</th>
			</tr>
			</thead>
			
 			<c:if test="${empty huishangflows.SUBPACKS}">
 				<tbody  >
	             <tr class=gvItem>
	                 <td align="center" colspan="8">查询数据为空！</td>
	             </tr>
	            </tbody>
             </c:if>
             
                        
             <c:if test="${not empty huishangflows.SUBPACKS }">
                    <tbody>
                    <c:forEach items="${huishangflows.SUBPACKS}" var="modelObj" varStatus="status">
                     <tr >
                            <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
							<td ><c:out value="${modelObj.HS_VALD}"/></td>
							<td ><c:out value="${modelObj.HS_INPD}"/></td>
							<td ><c:out value="${modelObj.HS_RELD}"/></td>
							<td ><c:out value="${modelObj.HS_INPT}"/></td>
							<td ><c:out value="${modelObj.HS_TRNN}"/></td>
							<td ><c:out value="${modelObj.TRANTYPE}"/></td>
							<td ><c:out value="${modelObj.O_R_FLAG}"/></td>
							<td ><c:out value="${modelObj.BILL_AMT}"/></td>
							<td ><c:out value="${modelObj.BILL_AMS}"/></td>
							<td ><c:out value="${modelObj.AUTHCODE}"/></td>
							<td ><c:out value="${modelObj.DESLINE}"/></td>
							<td ><c:out value="${modelObj.CURR_BAL}"/></td>
							<td ><c:out value="${modelObj.NOTE}"/></td>
							<td ><c:out value="${modelObj.FORCARDNBR}"/></td>
							<td ><c:out value="${modelObj.RESERVED}"/></td>
							<c:set var="NX_TRNN" value="${modelObj.HS_TRNN}"></c:set>
							<c:set var="NX_INPT" value="${modelObj.HS_INPT}"></c:set>
							<c:set var="NX_RELD" value="${modelObj.HS_RELD}"></c:set>
							<c:set var="NX_INPD" value="${modelObj.HS_INPD}"></c:set>
                     </tr>
                    </c:forEach>
                     </tbody>
               </c:if>
	</table>
	
	 <c:if test="${huishangflows.RTN_IND eq 1}">
	 <ul class="pager" id="pager"></ul>
	 </c:if>

<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:10,pageIndex:1,change:toPage});
});
//-->
$("#RTN_IND").val('${huishangflows.RTN_IND}');
$("#NX_TRNN").val('${NX_TRNN}');
$("#NX_INPT").val('${NX_INPT}');
$("#NX_RELD").val('${NX_RELD}');
$("#NX_INPD").val('${NX_INPD}');
</script>