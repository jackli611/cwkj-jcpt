<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			
			
		<div id="datalist" class="datalist">
			<table class="table table-bordered">
				<thead>
					<tr >
						<th colspan="6">平台账户余额</th>
					</tr>
				</thead>
				<tbody>
					<tr >
						<td>平台账户名称</td>
						<td>${platformAmt.NAME}</td>
						<td>可用余额</td>
						<td>${platformAmt.AVAIL_BAL}元</td>
						<td>账面余额</td>
						<td>${platformAmt.CURR_BAL}元</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</body>
</html>