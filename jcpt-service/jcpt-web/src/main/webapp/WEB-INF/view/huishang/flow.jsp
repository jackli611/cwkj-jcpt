<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
$(function(){
	queryStart();
})


 function loadData()
 {
 	loadTableData({id:"datalist",url:"<c:url value='/finanice/huishang/tradeDtlList.do'/>",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 

 
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			
			
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<input type="hidden" name="RTN_IND"  id="RTN_IND" >
			<input type="hidden" name="NX_TRNN"  id="NX_TRNN" >
			<input type="hidden" name="NX_INPT"  id="NX_INPT" >
			<input type="hidden" name="NX_RELD"  id="NX_RELD" >
			<input type="hidden" name="NX_INPD"  id="NX_INPD" >

			<div class="form-group">
			 	<label for="names">起止日期：</label>
			  	<input id="STARTDATE" class="form-control" type="text" name="STARTDATE"    />
			</div>
			<div class="form-group">
			 	<label for="names">截止日期：</label>
			  	<input id="ENDDATE" class="form-control" type="text" name="ENDDATE"    />
			</div>
			
			<div class="form-group">
			 	<input type="submit" class="btn" id="queryBtn" value="查询" />
			</div>
			</form>
		</div>
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
			<table class="table table-bordered">
				<thead>
				<tr >
					<th style="width:60px; min-width: 60px;">序号</th>
					<th>入帐日期</th>
					<th>交易日期</th>
					<th>自然日期</th>
					<th>交易时间</th>
					<th>交易流水号</th>
					<th>交易类型</th>
					<th>冲正/撤销标志</th>
					<th>交易金额</th>
					<th>交易金额符号</th>
					<th>授权代码</th>
					<th>交易描述</th>
					<th>交易后余额</th>
					<th>摘要</th>
					<th>对手交易账号</th>
					<th>保留域</th>
				</tr>
				</thead>
				<tbody  >
				    
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>