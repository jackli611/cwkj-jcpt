<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <style type="text/css">
 .repaytype22{
 background-color: #F33;
 color:#fff;
 }
 </style>
 <script type="text/javascript">
 $(function(){
/* 	 $("#queryBtn").click(queryStart); */
 });
 
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
			<form id="form_1" name="form"  action="addPermission.do"  method="post"  class="form-horizontal">
			<input type="hidden" name="parentId" value="<c:out value="${parentId}" />"> 
			 <div class="form-group form-inline">
				<label for="permissionName" class="monospaced_lg">名称:</label>
				 <input type="text" id="permissionName" name="name" class="form-control w400" value="<c:out value="${sysPermission.name }" />" />
				</div>
			
			 <div class="form-group form-inline">
			 	<label for="permissionType"  class="monospaced_lg">类型：</label>
				<select class="form-control" name="dataType">
				<option value="1" <c:if test="${sysPermission.dataType==1}">selected="selected"</c:if> >菜单</option>
				<option value="3" <c:if test="${sysPermission.dataType==3}">selected="selected"</c:if>>菜单目录</option>
			    <option value="2" <c:if test="${sysPermission.dataType==2}">selected="selected"</c:if>>代码</option>
				</select>			  	
			 </div>
			 
			 <div class="form-group form-inline">
			 <label for="permissionUrl"  class="monospaced_lg">菜单链接地址:</label>
			 <input type="text" class="form-control w400" id="permissionUrl" name="url" value="<c:out value="${sysPermission.url }" />"  />
			 </div>
			 
			 <div class="form-group form-inline">
			 <label for="permissionCode"  class="monospaced_lg">权限代码:</label>
			 <input type="text" class="form-control w400" id="permissionCode" name="perCode" value="<c:out value="${sysPermission.perCode }" />"  />
			 </div>
			 
			 <div class="form-group form-inline">
			 <label for="permissionIndexs"  class="monospaced_lg">排序:</label>
			 <input type="text" class="form-control w400" id="permissionIndexs" name="indexs" value="<c:out value="${sysPermission.indexs }" />"  />
			 </div>
			 <div class="form-group form-inline col-md-7">
				<center><input type="submit" class="btn btn-primary" id="queryBtn" value="保存" /></center>
			</div>
			</form>
		</div>
		<div id="statictable">
		</div>
		 
	</div>
</body>
</html>