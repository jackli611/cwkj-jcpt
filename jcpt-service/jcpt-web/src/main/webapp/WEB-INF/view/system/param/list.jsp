<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp"%>
<script type="text/javascript">
	$(function() {
		//查询
		$("#queryBtn").click(queryStart);
		$("#addBtn").click(function(){
			preShow("");
		});	
	});

	//查询
	function queryStart() {
		toPage(1);
	}

	//翻页
	function toPage(pageIndex) {
		$("#pageIndex").val(pageIndex);
		loadData();
	}

	//加载数据
	function loadData() {
		loadTableData({
			id : "datalist",
			url : "listData.do",
			data : $("#form").serialize()
		});
	}
	
	function preShow(paramId){
		openWindow({
			title:paramId==""?"新增系统参数":"修改系统参数",
			url:"preShow.do?paramId="+paramId,
			width:400,
			height:290,
			close:loadData
		});
	}
	
	function deleteData(paramId) {
		confirmLayer({
			msg : "是否确认删除?",
			yesFn : function(index) {
				$.ajaxSettings.async = false; //设置ajax同步
				loadJsonData({
					url : "delete.do",
					complete : showJsonData,
					data : {
						"paramId" : paramId
					}
				});
				loadData();
				$.ajaxSettings.async = true;//设置ajax异步
				closeWindow(index);
			}
		});
	}
</script>
</head>
<body>
	<div id="activity">
		<!-- 显示操作信息 -->
		<div id="promptMsg"></div>
		<!-- 查询条件 -->
		<div class="form-panel">
			<form id="form" name="form" method="post" class="form-inline">
				<div class="row">
					<div class="form-group col-md-3">
						<label>参数名称:</label> 
						<input type="text"  name="paramName" class="form-control" />
					</div>
					<div class="form-group  col-md-3">
						<label >参数编码:</label> 
						<input type="text"  name="paramCode" class="form-control" />
					</div>
	
					<div class="form-group col-md-3">
						<input type="hidden" id="pageIndex" name="pageIndex" /> 
						<secure:hasPermission name="system_param_list"> 
							<input type="button" class="btn btn-primary" id="queryBtn" value="查询" />
						</secure:hasPermission>
						<secure:hasPermission name="system_param_add">
							<input type="button" class="btn btn-primary" id="addBtn" value="新增" />
						</secure:hasPermission>
					</div>
				</div>
			</form>
		</div>
		<!-- 数据显示 -->
		<div id="datalist" class="datalist">
			<table class="table  table-bordered datatable">
				<thead>
					<tr>
						<th>序号</th>
						<th>参数名称</th>
						<th>参数编码</th>
						<th>参数值</th>
						<th>操作</th>
					</tr>
				</thead>
			</table>
		</div>
		
	</div>
</body>
</html>