<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	 $("#queryBtn").click(queryStart);
	 $("#addBtn").click(addRootPermission);
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"listPermissionData.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
	toPage(1);
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 function addRootPermission()
 {
	 addPermission();
 }
 function addPermission(id)
 {
	 var pid="0";
	 if(id!=null)
		 {
		 pid=id;
		 }
	 var param="parentId="+pid;
	 openWindow({url:"addPermission.do?"+param,close:loadData,width:600,height:400,title:"添加权限"});
 }
 function deletePermission(id)
 {
	 confirmLayer({msg:"确定删除？",yesFn:function(){
		 loadJsonData({url:"deletePermission.do?id="+id,complete:function(dataResult){
			 alertLayer(dataResult.msg);
			 if(dataResult.code)
				 {
				 	loadData();
				 }
		 }});
	 }})
	
 }
 function queryChildMenu(parentId)
 {
	// $("#parentId").val(parentId);
	 queryStart();
 }
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"   method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			 <div class="form-group">
				<label for="permissionName">名称:</label>
				 <input type="text" id="permissionName" name="name" class="form-control" />
				</div>
				 <div class="form-group">
				<label for="permissionPerCode">权限代码:</label>
				 <input type="text" id="permissionPerCode" name="perCode" class="form-control" />
				</div>
				<div class="form-group">
				<label for="permissionPerCode">类型:</label>
				 <select class="form-control" name="dataType">
				 <option value="">全部</option>
				 <option value="1">菜单</option>
				 <option value="3">菜单目录</option>
				 <option value="2">权限代码</option>
				 </select>
				</div>
				<div class="form-group">
			<input type="button" class="btn btn-primary" id="queryBtn" value="查询" />
			<secure:hasPermission name="system_permission_add">
			<input type="button" class="btn btn-primary" id="addBtn" value="添加" />
			</secure:hasPermission>
			</div>
			</form>
		</div>
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
<table class="table table-bordered">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>名称</th>
				 <th>类型</th>
				 <th>菜单链接地址</th>
				 <th>权限代码</th>
				 <th>排序</th>
				 <th>创建时间</th>
				 <th>操作</th>
			</tr>
			</thead>
			<tbody  >
			    
	</tbody>
	</table>
		</div>
	</div>
</body>
</html>