<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
<style type="text/css">
.menuFolder{color:#4c4c4c;}
.menuLink{color:#4c4c4c;}
.menuCode{color:#888888;}
</style>

 <script type="text/javascript">
window.TREEDATA=null;
 $(function(){
	 loadJsonData({url:"menuData.do",complete:function(treeData){
		 window.TREEDATA=treeData;
		  var myTree = $('#myTree').tree({data:window.TREEDATA, itemCreator: function($li, item) {
			  var icon='';
			  var cssClass='';
			  var objcOpts=$.extend({icon:null,id:null,dataType:0,url:''},item);
			  if(objcOpts.icon!=null && objcOpts.icon!='')
		  		{
		  			icon='<i class="'+item.icon+'"  ></i>&nbsp;';
		  		} 
			  if(item.dataType==1)
				  {
				  	cssClass='menuLink';
				  }else if(item.dataType==2)
					  {
					  icon='<i class="fa fa-code"  ></i>&nbsp;';
						cssClass='menuCode';
					  }else if(item.dataType==3)
						  {
							cssClass='menuFolder';
						  }
			  
		        $li.append($('<a/>',{'href': item.url,'class':cssClass}).html(icon+item.title));

		    }});
	 }});
	 var winW=$(window).width()-251;
	 var winH=$(window).height();
	 $("#datalist").css("height",winH+"px");
 });
 function reloadTreeData()
 {
	 loadJsonData({url:"menuData.do",complete:function(treeData){
		 window.TREEDATA=treeData;
		 var myTree = $('#myTree').data('zui.tree');
		 myTree.reload(window.TREEDATA);
	 }});
 }
 function menuUrl(id)
 {
	 openWindow({url:"editPermissionTree.do?parentId="+id, width:780,height:450,title:"菜单 / 权限编辑",close:function(index){
		 reloadTreeData();
	 }});
 }
 </script>
</head>
<body>
	<div id="activity">
		<div id="datalist"  style="height: 100%;  position: absolute;left:100px; top:10px;">
<ul class="tree tree-lines  " id="myTree"></ul>
		</div>
	</div>
</body>
</html>