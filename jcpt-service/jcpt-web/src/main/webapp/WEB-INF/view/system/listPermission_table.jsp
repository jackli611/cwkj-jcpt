<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>名称</th>
				 <th>类型</th>
				 <th>菜单链接地址</th>
				 <th>权限代码</th>
				 <th>排序</th>
				 <th>创建时间</th>
				 <th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="8">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
                                    <td ><c:out value="${modelObj.name}"/></td>
                                    <td >
                                    <c:choose>
                                    <c:when test="${modelObj.dataType==1 }">菜单</c:when>
                                    <c:when test="${modelObj.dataType==2 }">权限代码</c:when>
                                    <c:when test="${modelObj.dataType==3 }">菜单目录</c:when>
                                    <c:otherwise><c:out value="${modelObj.dataType}"/></c:otherwise>
                                    </c:choose>
                                    </td>
                                    <td ><c:out value="${modelObj.url}"/></td>
                                    <td ><c:out value="${modelObj.perCode}"/></td>
                                    <td ><c:out value="${modelObj.indexs}"/></td>
                                    <td >  <f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
                                    <td>
										<div class="dropdown">
											<a class="btn-link dropdown-toggle" data-toggle="dropdown">操作 <span
												class="caret"></span></a>
											<ul class="dropdown-menu pull-right" role="menu">
												<secure:hasPermission name="system_permission_add">
													<li><a class="btn-link"
														onclick="addPermission(${modelObj.id})">添加子权限</a></li>
												</secure:hasPermission>
												<secure:hasPermission name="system_permission_delete">
													<li><a class="btn-link"
														onclick="deletePermission(${modelObj.id})">删除</a></li>
												</secure:hasPermission>
											</ul>
										</div>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   