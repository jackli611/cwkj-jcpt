<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>用户名</th>
				<th>姓名</th>
				<th>手机号</th>
				 <th>锁定状态</th>
				 <th>注销状态</th>
				 <th>注册时间</th>
				 <th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="6">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
                                    <td ><c:out value="${modelObj.userName}"/></td>
                                    <td ><c:out value="${modelObj.realName}"/></td>
                                    <td ><c:out value="${modelObj.telphone}"/></td>
                                    <td >
                                    <c:choose>
                                    <c:when test="${modelObj.locked==2 }">锁定</c:when>
                                    <c:otherwise>没锁定</c:otherwise>
                                    </c:choose>
                                    </td>
                                     <td >
                                    <c:choose>
                                    <c:when test="${modelObj.status==1 }">未注销</c:when>
                                    <c:otherwise>已注销</c:otherwise>
                                    </c:choose>
                                    </td>
                                    <td >  <f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
                                    <td>
                                    <div class="dropdown">
											<a class="btn-link dropdown-toggle" data-toggle="dropdown">操作 <span
												class="caret"></span></a>
											<ul class="dropdown-menu pull-right" role="menu">
								   <secure:hasPermission name="system_user_edit"><li><a class="btn-link"  data-toggle="modal" href="addUser.do?id=${modelObj.id }">修改</a></li></secure:hasPermission>
                                   <secure:hasPermission name="system_user_role_setting"> <li><a class="btn-link"  data-toggle="modal" href="roleListWithUserId.do?userId=<c:out value="${modelObj.id}"/>">角色</a></li></secure:hasPermission>
                                   <secure:hasPermission name="system_user_password_reset"> <li><a href="javaScript:void(0)" onclick="resetPassword('${modelObj.id}')">重置密码</a></li></secure:hasPermission>
                                    <secure:hasPermission name="system_user_lock">
                                   <li><a class="btn-link" onclick="lockUser('<c:out value="${modelObj.id}"/>','<c:out value="${modelObj.locked}"/>')">
									<c:choose>
                                    <c:when test="${modelObj.locked==2 }">解锁</c:when>
                                    <c:otherwise>锁定</c:otherwise>
                                    </c:choose>
									</a></li>
									</secure:hasPermission>
									<secure:hasPermission name="system_user_delete">
									 <c:choose>
                                    <c:when test="${modelObj.status==1 }"><li><a class="btn-link" onclick="delUser('<c:out value="${modelObj.id}"/>')">注销</a></li></c:when>
                                    </c:choose>
                                    </secure:hasPermission>

                                   </ul>
                                   </div>
                                    </td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   