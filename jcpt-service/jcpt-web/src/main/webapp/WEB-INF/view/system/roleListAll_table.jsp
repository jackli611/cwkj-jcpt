<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>名称</th>
				 <th>是否生效</th>
				 <th>创建时间</th>
				 <th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="5">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                              <tr >
                                    <td ><c:out value="${status.index+1}"/></td>
                                    <td ><c:out value="${modelObj.name}"/></td>
                                    <td >
                                    <c:choose>
                                    <c:when test="${modelObj.available==1 }">有效</c:when>
                                    <c:otherwise>无效</c:otherwise>
                                    </c:choose>
                                    </td>
                                    <td >  <f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
                                    <td>
                                    <div class="dropdown">
											<a class="btn-link dropdown-toggle" data-toggle="dropdown">操作 <span
												class="caret"></span></a>
											<ul class="dropdown-menu" role="menu">
                                    <secure:hasPermission name="system_role_delete">
                                    <li><a onclick="delRole('<c:out value="${modelObj.id }" />','<c:out value="${modelObj.name }" />')" class="btn-link">删除</a>
                                    </li>
                                    </secure:hasPermission>
                                    <secure:hasPermission name="system_role_edit">
                                    <li><a  data-toggle="modal"  href="editRole.do?id=<c:out value="${modelObj.id}" />"  class="btn-link">编辑</a>
                                    </li>
                                    </secure:hasPermission>
                                    <secure:hasPermission name="system_role_permission_edit">
                                    <li>
                                    <a onclick="openWindow({url:'permissionMap.do?roleId=<c:out value="${modelObj.id}" />',title:'权限设置'});"  class="btn-link">权限</a>
                                    </li>
                                    </secure:hasPermission>
                                    <secure:hasPermission name="system_user_list">
                                    <li>
                                    <a onclick="openWindow({url:'queryRoleUserList.do?roleId=<c:out value="${modelObj.id}" />',title:'角色用户',width:900});"  class="btn-link">用户</a>
                                    </li>
                                    </secure:hasPermission>
                                    </ul>
                                    </div>
                                    </td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   