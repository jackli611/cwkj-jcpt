<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>唯一标识锁</th>
<th>创建时间</th>
<th>备注</th>
<th>操作</th>

			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="5">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
                                    <td ><c:out value="${modelObj.lockNum}"/></td>
<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
<td ><c:out value="${modelObj.remark}"/></td>

                                    <td>
	                                    <div class="dropdown">
										  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
										  <ul class="dropdown-menu">
										    <li><a href="javascript:deleteLock('<c:out value="${modelObj.lockNum}"/>');">删除</a></li>
										  </ul>
										</div>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   