<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

<script type="text/javascript">
	$(function(){
		$("#addBtn").click(function(){
			loadData("add.do");
		});
		$("#updateBtn").click(function(){
			loadData("update.do");
		});
		

	});
	
	function loadData(url){
		loadJsonData({
			url : url,
			complete : function(data){
				if(data.code == 1){
					data["isIframe"]=true;
					showJsonData(data);
					parent.closeWindow(parent.openwindowLastIndex);
				}else{
					showJsonData(data);
				}
			},
			data :$("#form").serialize()
		});
	}
</script>
	<div id="activity" >
	<!-- 显示操作信息 -->
	<div id="promptMsg"></div>
	<div class="form-panel">
		<form id="form" name="form"  method="post"  class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2">字典类型:</label>
				<div class="col-sm-10">
					<input type="text"  name="codeType" class="form-control" value="${model.codeType}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 text-right">字典名称:</label>
				<div class="col-sm-10">
					<input type="text"  name="codeName" class="form-control" value="${model.codeName}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 text-right">字典值:</label>
				<div class="col-sm-10">
					<input type="text"  name="codeValue" class="form-control" value="${model.codeValue}" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2  col-sm-8 text-center">
					<input type="hidden" name="codeId" value="${model.codeId}" />
					<c:if test="${model == null}">
						<secure:hasPermission name="system_dict_add">
							<input type="button" class="btn btn-primary" id="addBtn" value="保存" />
						</secure:hasPermission>
					</c:if>
					<c:if test="${model != null}">
						<secure:hasPermission name="system_dict_edit">
							<input type="button" class="btn btn-primary" id="updateBtn" value="保存" />
						</secure:hasPermission>
					</c:if>
				</div>
			</div>
		</form>
	</div>		 
	</div>
