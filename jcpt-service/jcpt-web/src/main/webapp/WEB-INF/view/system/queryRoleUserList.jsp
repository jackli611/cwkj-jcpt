<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	 $("#queryBtn").click(queryStart);
	 queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"queryRoleUserList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
	toPage(1);
	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
  
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<input type="hidden" name="roleId" value="<c:out value="${ roleId}" />"  >
			 <div class="form-group">
				<label for="userName">用户名:</label>
				 <input type="text" id="userName" name="userName" class="form-control" />
				</div>
				<div class="form-group">
				<label for="realName">姓名:</label>
				 <input type="text" id="realName" name="realName" class="form-control" />
				</div>
				 <div class="form-group">
			 	<label for="startDate">注册时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			<div class="form-group">
			<input type="submit" class="btn btn-primary" id="queryBtn" value="查询" />
			</div>
				
			</form>
			</div>
		 
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
<table class="table table-bordered">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>用户名</th>
				<th>姓名</th>
				 <th>锁定状态</th>
				 <th>注册时间</th>
				 <th>操作</th>
			</tr>
			</thead>
			<tbody  >
			    
	</tbody>
	</table>
		</div>
	</div>
</body>
</html>