<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
	 function loadData()
	 {
	 	loadTableData({id:"datalist",url:"roleListAll.do",data:$("#form_1").serialize()});
	 }
	 function queryStart()
	 {
			 	toPage(1);
			 	return false;
	 } 
	 
	 function toPage(pageIndex)
	 {
		 $("#defaultAlertPanel").html("");
		 $("#pageIndex").val(pageIndex);
		 loadData();
	 }
	 
 function editRole()
 {
	 loadTableData({url:"editRole.do",data:$("#form_editRole").serialize(),id:"editRolePage"})
 }
 function addRole()
 {
	 loadTableData({url:"addRole.do",data:$("#form_addRole").serialize(),id:"addRolePage"})
 }
 function delRole(id,msg)
 {
	 confirmLayer({msg:"确定要删除\""+msg+"\"角色",yesFn:function(){
	 loadJsonData({url:"delRole.do?id="+id,complete:function(data){
		 loadData();
		 alertLayer(data.msg,{id:"defaultAlertPanel",icon:data.code});
		
	 }});
	 }});
 }
 </script>
</head>
<body>
<div id="defaultAlertPanel"></div>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<input type="hidden" name="roleId" value="<c:out value="${ roleId}" />"  >
			 <div class="form-group">
				<label for="userName">名称:</label>
				 <input type="text" id="userName" name="name" class="form-control" />
				</div>
				<div class="form-group">
				<label for="realName">状态:</label>
				<select name="available">
				<option value=""> 全部</option>
				<option value="1"> 有效</option>
				<option value="0"> 无效</option>
				</select>
				</div>
			<div class="form-group">
			<input type="submit" class="btn btn-primary" id="queryBtn" value="查询" />
			<secure:hasPermission name="system_role_add">
		<a class="btn btn-primary" data-toggle="modal" href="addRole.do">增加角色</a>
		</secure:hasPermission>
			</div>
				
			</form>
	
	</div>
		<div id="datalist" class="datalist">
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>名称</th>
				 <th>是否生效</th>
				 <th>创建时间</th>
				 <th>操作</th>
			</tr>
			</thead>
			 
	</table>
		</div>
	</div>

</body>
</html>