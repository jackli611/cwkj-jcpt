<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>

	<div id="addRolePage">
	<div class="form-panel">
			  <form id="form_addRole" name="form"    action="addRole.do" method="post" class="form-horizontal"  >
         <div class="form-group">
				<label for="addRole_roleName" class="col-sm-2">角色名称:</label>
				<div class="col-sm-10">
				 <input type="text" id="addRole_roleName" name="name" class="form-control" value="<c:out value="${sysRole.name }" />" />
				 </div>
				</div>
		<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="available" value="1" <c:if test="${sysRole.available==1}">checked=checked</c:if> /> 生效
        </label>
        </div>
    </div>
  </div>
  <div class="form-group">
			 <div class="col-sm-offset-2 col-sm-10">
			<input type="button" class="btn btn-primary" onclick="addRole()" id="submitBtn" value="确定" />
			</div></div>
        </form>
		</div>
	 
		 
	</div>
<script type="text/javascript">
$('#triggerModal').on('hide.zui.modal', function() {
	 loadData();
	})
	 
</script>