<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/header.jsp"%><html>
<script type="text/javascript">
	$(function(){
		$("#addBtn").click(function(){
			loadData("add.do");
		});
		$("#updateBtn").click(function(){
			loadData("update.do");
		});
		

	});
	
	function loadData(url){
		loadJsonData({
			url : url,
			complete : function(data){
				if(data.code == 1){
					data["isIframe"]=true;
					showJsonData(data);
					parent.closeWindow(parent.openwindowLastIndex);
				}else{
					showJsonData(data);
				}
			},
			data :$("#form").serialize()
		});
	}
</script>
<div id="activity">
	<!-- 显示操作信息 -->
	<div id="promptMsg"></div>
	<div class="form-panel">
		<form id="form" name="form" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-4 text-right">参数编码:</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="paramCode"
						value="${model.paramCode}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 text-right">参数名称:</label>
				<div class="col-sm-6">
					<input type="text" class="form-control " name="paramName"
						value="${model.paramName}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 text-right">参数值:</label>
				<div class="col-sm-6">
					<input type="text" class="form-control " name="paramValue"
						value="${model.paramValue}" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2  col-sm-8 text-center">
					<input type="hidden" name="paramId"
						value="${model.paramId}" />
					<c:if test="${model == null}">
						<secure:hasPermission name="system_param_add">
							<input type="button" class="btn btn-primary" id="addBtn"
								value="保存" />
						</secure:hasPermission>
					</c:if>
					<c:if test="${model != null}">
						<secure:hasPermission name="system_param_edit">
							<input type="button" class="btn btn-primary" id="updateBtn"
								value="保存" />
						</secure:hasPermission>
					</c:if>
				</div>
			</div>
		</form>
	</div>
</div>