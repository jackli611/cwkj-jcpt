<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp"%>
<!--显示数据-->
<table class="table table-bordered table-hover datatable" >
	<thead>
		<tr>
			<th>序号</th>
			<th>参数名称</th>
			<th>参数编码</th>
			<th>参数值</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
	<c:if test="${empty pagedata.page}">
		<tr>
			<td align="center" colspan="5">查询数据为空！</td>
		</tr>
	</c:if>
	<c:if test="${not empty pagedata.page }">
	<c:forEach items="${pagedata.page}" var="model" varStatus="status">
		<tr>
			<td>${pagedata.startOfPage+status.index+1}</td>
			<td>${model.paramName}</td>
			<td>${model.paramCode}</td>
			<td>${model.paramValue}</td>
			<td>
				<secure:hasPermission name="system_param_edit">
					<a href="javaScript:void(0)" onclick="preShow('${model.paramId}')">修改</a>
				</secure:hasPermission>
				<secure:hasPermission name="system_param_delete">
					<a href="javaScript:void(0)" onclick="deleteData('${model.paramId}')">删除</a>
				</secure:hasPermission>
			</td>
		</tr>
	</c:forEach>
	</c:if>
	</tbody>
</table>
<!--分页-->
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
	$(function() {
		//分页控件
		jqPager({
			id : "pager",
			totalPages : ${pagedata.totalPage},
			pageIndex : ${pagedata.pageIndex},
			change : toPage
		});
	});
</script>


