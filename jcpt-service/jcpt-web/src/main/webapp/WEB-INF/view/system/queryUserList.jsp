<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	 $("#queryBtn").click(queryStart);
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"queryUserList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
	toPage(1);
	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 function editUser()
 {
	 //loadTableData({url:"addUser.do",data:$("#form_editUser").serialize(),id:"editUserPage"})
	 loadJsonData({url:"addUser.do",data:$("#form_editUser").serialize(),id:"editUserPage",complete:function(data){
		 window.alertLayer(data.msg,{id:"alertContent",icon:data.code});
		 $("#triggerModal").modal('hide');
		 loadData();
	 }});
 }
 
 function addUserRoleList(userId)
 {
	 $("#alertContent").html("");
	 var roleListStr="";
	 $("input[name='rolelist']:checked").each(function(){

// 		    if ("checked" == $(this).attr("checked")) {
		    	roleListStr+=","+ $(this).attr('value') ;
// 		 		 alert($(this).attr('value'));
// 		    }
	 });
	 
	 if(roleListStr!="")
		 {
		 	roleListStr=(roleListStr.substring(1));
		 }
	 	loadJsonData({url:"modifyUserRoles.do",data:{roleListStr:roleListStr,userId:userId},complete:function(data){
		 window.alertLayer(data.msg,{id:"alertContent",icon:data.code});
	 
	 }});
// 		    alert(roleListStr);
 }
 function lockUser(userId,lock)
 {
 
	 loadJsonData({url:"modifyUserLock.do",data:{isLock:lock==1?true:false,userId:userId},complete:function(data){
		 window.alertLayer(data.msg,{id:"alertContent",icon:data.code});
		 loadData();
	 }});
	 
 }
 function delUser(userId)
 {
 	
	 loadJsonData({url:"deleteUser.do",data:{userId:userId},complete:function(data){
		 window.alertLayer(data.msg,{id:"alertContent",icon:data.code});
		 loadData();
	 }});
	 
 }
 
 function resetPassword(userId) {
		promptLayer({
			title : "请输入重置后的密码",
			formType:3,
			isRequired:false,
			yesFn : function(value, index) {
				$.ajaxSettings.async = false; //设置ajax同步
				loadJsonData({
					url : "resetPassword.do",
					complete : showJsonData,
					data : {
						"userId" : userId,
						"password" : value
					}
				});
				loadData();
				$.ajaxSettings.async = true;//设置ajax异步
				closeWindow(index);
			}
		});
	}
 
 </script>
</head>
<body>
<div id="alertContent"></div>

	<div id="activity">
	<div   id="promptMsg"></div>
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<input type="hidden" name="parentId" value="0" id="parentId" >
			 <div class="form-group">
				<label for="userName">用户名:</label>
				 <input type="text" id="userName" name="userName" class="form-control" />
				</div>
				 <div class="form-group">
				<label for="realName">姓名:</label>
				 <input type="text" id="realName" name="realName" class="form-control" />
				</div>
				 <div class="form-group">
				<label for="telphone">手机号:</label>
				 <input type="text" id="telphone" name="telphone" class="form-control" />
				</div>
				 <div class="form-group">
			 	<label for="startDate">注册时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			<div class="form-group">
			<input type="submit" class="btn btn-primary" id="queryBtn" value="查询" />
			<secure:hasPermission name="system_user_add"><a class="btn btn-primary" href="addUser.do"  data-toggle="modal" >添加</a></secure:hasPermission>
			</div>
				
			</form>
			</div>
		 
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
<table class="table table-bordered">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>用户名</th>
				<th>姓名</th>
				<th>手机号</th>
				 <th>锁定状态</th>
				 <th>创建时间</th>
				 <th>操作</th>
			</tr>
			</thead>
			<tbody  >
			    
	</tbody>
	</table>
		</div>
	</div>
</body>
</html>