<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<div id="alertContent">
</div>	 
 <c:if test="${empty roleList}">
未创建角色
</c:if>
<c:if test="${not empty roleList}">
<div class="form-group" >
<c:forEach items="${roleList}" var="modelObj" varStatus="status">
                   
<label class="checkbox-inline" style="width:150px;">
  <input type="checkbox" name="rolelist" <c:if test="${not empty modelObj.userId }"> checked="checked" </c:if> value="<c:out value="${modelObj.id}"/>" />${modelObj.name}
  </label>

                       
</c:forEach>
</div>
<div class="form-group"> <input type="button" class="btn btn-primary" value="保存" onclick="addUserRoleList('<c:out value="${userId }" />')" /></div>
</c:if>
	