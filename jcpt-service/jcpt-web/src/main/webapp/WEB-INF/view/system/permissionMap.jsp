<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
 });
 </script>
</head>
<body>
<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
	<form id="form_1"   method="post"  action="" class="form-inline">
		<input type="hidden" name="roleId" value="<c:out value="${roleId }" />" /> 
		<div id="datalist" class="datalist">
		<table  class="table table-bordered">
		  <c:forEach items="${permissionNodeList}" var="rootNode" varStatus="status">
                             <tr >
                                    <td style="width:40px;" ><c:out value="${status.index+1}"/></td>
                                    <td  style="width:120px;" >
                                      <xwckj:checkBox id="${rootNode.id }" text="${rootNode.name }" name="permissionIds" value="${rootNode.id }" checked="${rootNode.hasPermission}"></xwckj:checkBox>
                                     
                                    </td>
                                    <td style="text-align: left;" >
                                    <ul class="unstyled">
                                      <c:forEach items="${rootNode.children}" var="childrenNode" varStatus="childrenStatus">
                                      <li class='menu_type_<c:out value="${childrenNode.dataType }" />'>
                                      <xwckj:checkBox id="${childrenNode.id }" text="${childrenNode.name }" name="permissionIds" value="${childrenNode.id }" checked="${childrenNode.hasPermission}"></xwckj:checkBox>
											<c:if test="${not empty childrenNode.children}">
											
											<ul class="unstyled">
											 <c:forEach items="${childrenNode.children}" var="thirdChildrenNode" varStatus="thirdChildrenStatus">
                                      <li class='menu_type_<c:out value="${thirdChildrenNode.dataType }" />'>
                                      <xwckj:checkBox id="${thirdChildrenNode.id }" text="${thirdChildrenNode.name }" name="permissionIds" value="${thirdChildrenNode.id }" checked="${thirdChildrenNode.hasPermission}"></xwckj:checkBox>
		                                     <c:if test="${not empty thirdChildrenNode.children}">
													<ul class="unstyled" >
													 <c:forEach items="${thirdChildrenNode.children}" var="fourChildrenNode" varStatus="fourChildrenStatus">
		                                      <li class='menu_type_<c:out value="${fourChildrenNode.dataType }" />' >
		                                      <xwckjF:checkBox id="${fourChildrenNode.id }" text="${fourChildrenNode.name }" name="permissionIds" value="${fourChildrenNode.id }" checked="${fourChildrenNode.hasPermission}"></xwckjF:checkBox>
		                                      </li>
		                                      </c:forEach>
													</ul>
													</c:if> 
                                      </li>
                                      </c:forEach>
											</ul>
											</c:if>                                      
                                      </li>
                                      </c:forEach>
                                      </ul>
                                    </td>
                             </tr>
                            </c:forEach>
                            </table>
		</div>
		<secure:hasPermission name="system_role_permission_edit">
		<input type="submit" class="btn btn-primary mt10" value="保存" />
		</secure:hasPermission>
			</form>
			</div>
	</div>
</body>
</html>