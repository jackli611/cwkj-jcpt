<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>

	<div id="editUserPage">
	<div class="form-panel">
			  <form id="form_editUser" name="form"    action="addUser.do" method="post" class="form-horizontal"  >
			  
			<c:if test="${empty sysUser.id }">
				<div class="form-group">
				<label for="addRole_roleName" class="col-sm-2">用户名:</label>
				<div class="col-sm-10">
				 <input type="text" id="addRole_roleName" name="userName" class="form-control" value="<c:out value="${sysUser.userName }" />" />
				 </div>
				</div>
				 <div class="form-group">
				<label for="addRole_realName" class="col-sm-2">姓名:</label>
				<div class="col-sm-10">
				 <input type="text" id="addRole_realName" name="realName" class="form-control" value="<c:out value="${sysUser.realName }" />" />
				 </div>
				</div>
				 <div class="form-group">
				<label for="addRole_telphone" class="col-sm-2">手机号:</label>
				<div class="col-sm-10">
				 <input type="text" id="addRole_telphone" name="telphone" class="form-control" value="<c:out value="${sysUser.telphone }" />" />
				 </div>
				</div>
				 <div class="form-group">
				<label for="addUser_password" class="col-sm-2">密码:</label>
				<div class="col-sm-10">
				 <input type="password" id="addUser_password" name="password" class="form-control" value="" />
				 </div>
				</div>
				<div class="form-group">
				<label for="addUser_password2" class="col-sm-2">再次密码:</label>
				<div class="col-sm-10">
				 <input type="password" id="addUser_password2" name="password2" class="form-control" value="" />
				 </div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
				      <div class="checkbox">
				        <label>
				          <input type="checkbox" name="locked" value="2" <c:if test="${sysUser.locked==2}">checked=checked</c:if> /> 锁定
				        </label>
				        </div>
				    </div>
			  </div>
			</c:if>
				
			<c:if test="${not empty sysUser.id }">
				<input type="hidden" name="id" value="${sysUser.id }" />
				<div class="form-group">
				<label for="addRole_roleName" class="col-sm-2">用户名:</label>
				<div class="col-sm-10">
				 <input type="text" id="addRole_roleName" name="userName" class="form-control" readonly="readonly" value="<c:out value="${sysUser.userName }" />" />
				 </div>
				</div>
				 <div class="form-group">
				<label for="addRole_realName" class="col-sm-2">姓名:</label>
				<div class="col-sm-10">
				 <input type="text" id="addRole_realName" name="realName" class="form-control" value="<c:out value="${sysUser.realName }" />" />
				 </div>
				</div>
				 <div class="form-group">
				<label for="addRole_telphone" class="col-sm-2">手机号:</label>
				<div class="col-sm-10">
				 <input type="text" id="addRole_telphone" name="telphone" class="form-control" value="<c:out value="${sysUser.telphone }" />" />
				 </div>
				</div>
				
			</c:if>
		
  <div class="form-group">
			 <div class="col-sm-offset-2 col-sm-10">
			<input type="button" class="btn btn-primary" onclick="editUser()" id="submitBtn" value="确定" />
			</div></div>
        </form>
		</div>
	 
		 
	</div>
<script type="text/javascript">
$('#triggerModal').on('hide.zui.modal', function() {
	queryStart();
	})
	 
</script>