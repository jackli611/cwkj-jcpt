/**
 * Project Name:jcpt-web <br>
 * File Name:UtilController.java <br>
 * Package Name:com.hehenian.jcpt.controller.system <br>
 * @author anxymf
 * Date:2017年2月23日下午3:41:42 <br>
 * Copyright (c) 2017, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcpt.controller.system;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.StringUtil;



/**
 * ClassName: UtilController <br>
 * Description: 系统统一处理帮助类
 * @author anxymf
 * Date:2017年2月23日下午3:41:42 <br>
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping("/sys/util/")
public class SysUtilController {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/downloadProgress.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String downloadProgress(HttpSession session,String shadeKey){
		Enumeration<String> enumeration =  session.getAttributeNames();
		boolean flag = false;
		while(enumeration.hasMoreElements()){
		  if(enumeration.nextElement().equalsIgnoreCase(shadeKey)){
				flag = true;
		   }
		}
		if(flag){
			return new JsonView(JsonView.CODE_FAILE, "").toJsonString();
		}
		return new JsonView(JsonView.CODE_SUCCESS, "").toJsonString();
	}
	
	@RequestMapping("/getImage.do")
	public void getImage(String url,HttpServletRequest request, HttpServletResponse response) throws Exception{  
	    String JPG="image/jpeg;charset=GB2312";  
	    if(!StringUtil.isEmpty(url)){  
	        // 本地文件路径  
	        File file = new File(url);  
//	        // 获取输出流  
	        OutputStream outputStream = response.getOutputStream();  
	        FileInputStream fileInputStream = new FileInputStream(file);  
//	        // 读数据  
	        byte[] data = new byte[fileInputStream.available()];  
	        fileInputStream.read(data);  
	        fileInputStream.close();  
//	        // 回写  
	        response.setContentType(JPG);  
	        outputStream.write(data);  
	        outputStream.flush();  
	        outputStream.close();  
	    }  
	}  
	
}

	