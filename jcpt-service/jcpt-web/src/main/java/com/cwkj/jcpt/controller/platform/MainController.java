package com.cwkj.jcpt.controller.platform;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcptsystem.model.system.HtmlMenu;
import com.cwkj.jcptsystem.service.system.HtmlMenuHandler;
import com.cwkj.jcptsystem.service.system.SysPermissionService;

/**
 * 平台
 * 
 * @author ljc
 * 
 */
@Controller
@RequestMapping("/platform/")
public class MainController extends BaseAction {

  private static Logger log = LoggerFactory.getLogger(MainController.class);
  @Autowired
  private SysPermissionService sysPermissionService;

  @RequestMapping("/main.do")
  public ModelAndView index(HttpServletRequest request,
      HttpServletResponse response) {
    ModelAndView model = new ModelAndView("platform/main");
    try {
      model.addObject("menuHtml", menuHtmlData());
    } catch (Exception e) {
      setPromptException(model, e);
    }
    return model;
  }

  @RequestMapping(value = "menuHtmlData.do", produces = "text/plain;charset=UTF-8")
  @ResponseBody
  public String menuHtmlData() {
    try {
      StringBuilder sb = new StringBuilder();
      List<HtmlMenu> list = null;
      if (this.isSuperSysUser()) {
//        list = sysPermissionService.findChildMenuList(new HtmlMenuHandler(),
//            null);
        list = sysPermissionService.findChildMenuListByUser(new HtmlMenuHandler(),
        		null,this.currentUser());
      } else {
//        list = sysPermissionService.findUserMenuList(new HtmlMenuHandler(),
//            this.currentUser().getId());
        list = sysPermissionService.findUserMenuListByUser(new HtmlMenuHandler(),  this.currentUser());
      }
      if (list != null) {
        log.debug("获取总数据记录数有：" + list.size());
        for (HtmlMenu item : list) {
          if (item != null) {
            sb.append(item.toHtml());
          }
        }
      }

      String jsonData = sb.toString();
      log.debug("菜单数据：" + jsonData);
      return jsonData;
    } catch (Exception e) {
      log.error("获取html菜单数据异常", e);
    }
    return null;
  }
}
