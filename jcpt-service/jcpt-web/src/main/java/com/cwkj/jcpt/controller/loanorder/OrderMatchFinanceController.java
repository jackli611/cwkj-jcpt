package com.cwkj.jcpt.controller.loanorder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.scf.model.products.PersonalLoanProductOrderDo;
import com.cwkj.scf.service.products.PersonalLoanProductOrderService;
import com.cwkj.scf.service.products.PersonalLoanProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.scf.model.finances.FinanceOrgDo;
import com.cwkj.scf.model.loan.OrderMatchLog;
import com.cwkj.scf.model.property.PropertyFinancesDo;
import com.cwkj.scf.service.finances.FinanceOrgService;
import com.cwkj.scf.service.loan.ILoanOrderService;
import com.cwkj.scf.service.property.PropertyFinancesService;

/**
 * 訂單匹配资金机构
 * @author harry
 */
@Controller
@RequestMapping(value="/loanorder/")
public class OrderMatchFinanceController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(OrderMatchFinanceController.class);
	
	private PropertyFinancesService propertyFinancesService;
	@Autowired
	private FinanceOrgService financeOrgService;
	@Autowired
	private PersonalLoanProductService personalLoanProductService;
	@Autowired
	private PersonalLoanProductOrderService personalLoanProductOrderService;


	@Autowired
	public void setPropertyFinancesService(PropertyFinancesService propertyFinancesService)
	{
		this.propertyFinancesService=propertyFinancesService;
	}
	
	/**
	 * 物业的资金机构数据分页
	 * @return
	 */
	@RequestMapping(value="/ordermatch.do",method=RequestMethod.GET)
	public ModelAndView ordermatch(HttpServletRequest request,String propertyId,String loanId)
	{
		ModelAndView view=new ModelAndView("loanorder/orderMatchList");
		view.addObject("propertyId", propertyId);
		view.addObject("loanId", loanId);
		view.addObject("currentOrderProductCode", request.getParameter("currentOrderProductCode"));
		return view;
	}
	
	/**
	 * 物业的资金机构数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/propertyFinancesList.do",method=RequestMethod.POST)
	public ModelAndView propertyFinancesList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		String product = request.getParameter("currentOrderProductCode");
		if("PERSONAL_XY".equalsIgnoreCase(product)) {
			return financesList_methodPost(request, pageIndex, pageSize, startDate, endDate);
		}
		
		ModelAndView view=new ModelAndView("loanorder/orderMatchList_table");
		try{ 
			 view.addObject("currentOrderProductCode", request.getParameter("currentOrderProductCode"));
			 
			 Map<String, Object> selectItem=getRequestToParamMap(request);
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 PageDo<PropertyFinancesDo> pagedata= propertyFinancesService.queryPropertyFinancesListPage(pageIndex,pageSize, selectItem);
			 view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取物业的资金机构数据分页异常", e);
		}
		return view;
	}
	
	
	/**
	 * 个贷分配资金机构数据
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/getMatchLog.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView getMatchLog(HttpServletRequest request,Long loanId)
	{
		ModelAndView view=new ModelAndView("loanorder/order_match_log_table");
		try{ 
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 selectItem.put("loanid", loanId);
			 List<Map<String,Object>> matchLogLst = propertyFinancesService.selectMatchLog(selectItem);
			 view.addObject("matchLogLst",matchLogLst);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取金融机构数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 个贷分配资金机构数据
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	public ModelAndView financesList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("loanorder/personal_orderMatchList_table");
		view.addObject("currentOrderProductCode", request.getParameter("currentOrderProductCode"));
		
		try{ 
			Map<String, Object> selectItem=new HashMap<String, Object>();
			setDateBetweemToMap(selectItem, startDate, endDate);
			pageIndex=pageIndex==null?1L:pageIndex;
			pageSize=pageSize==null?20:pageSize;
			PageDo<FinanceOrgDo> pagedata= financeOrgService.queryFinanceOrgListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取金融机构数据分页异常", e);
		}
		return view;
	}
	
	
	
	 /**
	 * 保存订单和资金机构匹配
	 * @param request
	 * @param propertyFinances
	 * @return
	 */
	@RequestMapping(value="/saveMatchFinance.do",method=RequestMethod.POST)
	public JsonView<PropertyFinancesDo> saveMatchFinance(HttpServletRequest request,String finaceId,String propertyFinanceId,Long loanId)
	{
		JsonView<PropertyFinancesDo> view= JsonView.successJsonView();
		try{
			 
			AssertUtils.isNotBlank(finaceId, "资金机构不能为空");
			AssertUtils.notNull(loanId, "订单不能为空");
			AssertUtils.isNotBlank(propertyFinanceId, "资金机构授信记录不能为空");			
			int result = propertyFinancesService.saveMatchOrder(loanId,finaceId,propertyFinanceId,currentUser().getId());
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view,1, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业的资金机构异常（POST）",e);
		}
		
		return view;
	}


	/**
	 * 个贷资金方匹配
	 * @return
	 */
	@RequestMapping(value="/personalLoanMatchFinance.do",method=RequestMethod.GET)
	public ModelAndView personalLoanMatchFinance_mothedGet(HttpServletRequest request,String propertyId,String loanId)
	{
		ModelAndView view=new ModelAndView("loanorder/personalLoanMatchFinance");
		view.addObject("propertyId", propertyId);
		view.addObject("loanId", loanId);
		view.addObject("currentOrderProductCode", request.getParameter("currentOrderProductCode"));
		try {
			List<FinanceOrgDo> financeOrgDoList = personalLoanProductService.personalLoanFinanceOrgList();
			List<PersonalLoanProductOrderDo> productOrderList=personalLoanProductOrderService.queryByLoanId(loanId);
			view.addObject("financeOrgDoList",financeOrgDoList);
			view.addObject("productOrderList",productOrderList);

		}catch (Exception ex)
		{
			logger.error("个贷资金方匹配异常",ex);
			setPromptException(view,ex);
		}
		return view;
	}

	/**
	 * 个贷订单金融产品
	 * @param request
	 * @param loanId
	 * @return
	 */
	@RequestMapping(value="/personalLoanMatchFinance.do",method=RequestMethod.POST)
	public ModelAndView personalLoanMatchFinance_mothedPost(HttpServletRequest request,String loanId)
	{
		ModelAndView view=new ModelAndView("loanorder/personalLoanMatchFinance_table");
		view.addObject("loanId", loanId);
		try {
			List<FinanceOrgDo> financeOrgDoList = personalLoanProductService.personalLoanFinanceOrgList();
			List<PersonalLoanProductOrderDo> productOrderList=personalLoanProductOrderService.queryByLoanId(loanId);
			view.addObject("financeOrgDoList",financeOrgDoList);
			view.addObject("productOrderList",productOrderList);

		}catch (Exception ex)
		{
			logger.error("个贷资金方匹配异常",ex);
			setPromptException(view,ex);
		}
		return view;
	}


	/**
	 *添加个贷订单金融产品
	 * @param request
	 * @param productId
	 * @param loanId
	 * @return
	 */
	@RequestMapping(value="/addPersonalLoanMatchFinance.do")
	public JsonView<String> addPersonalLoanMatchFinance_methodPost(HttpServletRequest request,Long productId,String loanId)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.isNotBlank(loanId, "请选择订单");
			AssertUtils.notNull(productId, "请选择金融产品");
			PersonalLoanProductOrderDo peroductOrder=new PersonalLoanProductOrderDo();
			peroductOrder.setLoanId(loanId);
			peroductOrder.setProductId(productId);
			peroductOrder.setRecordUserId(currentUser().getId());
			Integer result=personalLoanProductOrderService.insertPersonalLoanProductOrder(peroductOrder);
			AssertUtils.isTrue(result.equals(1),"保存失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加个贷金融产品订单异常",e);
		}
		return view;
	}





}
