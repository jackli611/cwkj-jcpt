package com.cwkj.jcpt.controller.etc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.scf.model.etc.EtcProfileDo;
import com.cwkj.scf.service.etc.EtcProfileService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;

/**
 * ETC基本信息.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/etc/")
public class EtcProfileController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(EtcProfileController.class);
	
	private EtcProfileService etcProfileService;
	
	@Autowired
	public void setEtcProfileService(EtcProfileService etcProfileService)
	{
		this.etcProfileService=etcProfileService;
	}
	
	/**
	 * ETC基本信息数据分页
	 * @return
	 */
	@RequestMapping(value="/etcProfileList.do",method=RequestMethod.GET)
	public ModelAndView etcProfileList_methodGet()
	{
		ModelAndView view=new ModelAndView("etc/etcProfileList");
		return view;
	}
	
	/**
	 * ETC基本信息数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/etcProfileList.do",method=RequestMethod.POST)
	public ModelAndView etcProfileList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("etc/etcProfileList_table");
		try{ 
			 Map<String, Object> selectItem=getRequestToParamMap(request);
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 selectItem.put("applyStatus",1);
			PageDo<EtcProfileDo> pagedata= etcProfileService.queryEtcProfileListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取ETC基本信息数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增ETC基本信息
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addEtcProfile.do",method=RequestMethod.GET)
	public ModelAndView addEtcProfile_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("etc/addEtcProfile");
		try{
			view.addObject("etcProfile", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加ETC基本信息异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增ETC基本信息
	 * @param request
	 * @param etcProfile
	 * @return
	 */
	@RequestMapping(value="/addEtcProfile.do",method=RequestMethod.POST)
	public ModelAndView addEtcProfile_methodPost(HttpServletRequest request,EtcProfileDo etcProfile)
	{
		ModelAndView view=new ModelAndView("etc/addEtcProfile");
		try{
			 
			Integer result= etcProfileService.insertEtcProfile(etcProfile);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加ETC基本信息异常（POST）",e);
		}finally {
			view.addObject("etcProfile", etcProfile);
		}
		return view;
	}
	
	 /**
	 * 浏览ETC基本信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewEtcProfile.do",method=RequestMethod.GET)
	public ModelAndView viewEtcProfile_methodGet(HttpServletRequest request,Long id)
	{
		ModelAndView view =new ModelAndView("etc/viewEtcProfile");
		try{
			EtcProfileDo etcProfile= etcProfileService.findEtcProfileById(id);
			view.addObject("etcProfile", etcProfile);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览ETC基本信息异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑ETC基本信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editEtcProfile.do",method=RequestMethod.GET)
	public ModelAndView editEtcProfile_methodGet(HttpServletRequest request,Long id)
	{
		ModelAndView view =new ModelAndView("etc/addEtcProfile");
		try{
			EtcProfileDo etcProfile= etcProfileService.findEtcProfileById(id);
			view.addObject("etcProfile", etcProfile);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑ETC基本信息异常（GET）",e);
		}
		return view;
	}
	

	 
}
