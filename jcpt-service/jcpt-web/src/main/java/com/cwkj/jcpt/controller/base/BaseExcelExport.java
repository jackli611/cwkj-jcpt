package com.cwkj.jcpt.controller.base;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.cwkj.jcpt.util.DateUtils;

/**
 * Excel导出
 * @author ljc
 * @version 1.0
 */
public class BaseExcelExport {

	/**
	 * 表头样式
	 * @param workbook
	 * @return
	 */
	protected  HSSFCellStyle headStyle(HSSFWorkbook workbook)
	{
		HSSFCellStyle sheetStyle = workbook.createCellStyle();    
	    sheetStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    
	    sheetStyle.setLeftBorderColor(HSSFColor.BLACK.index);
	    sheetStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	    sheetStyle.setRightBorderColor(HSSFColor.BLACK.index);
	    sheetStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
	    sheetStyle.setBottomBorderColor(HSSFColor.BLACK.index);
	    sheetStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	    sheetStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
	    sheetStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	    sheetStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
	    return sheetStyle;
	}
 
	
	/**
	 * 表身样式
	 * @param workbook
	 * @return
	 */
	protected  HSSFCellStyle bodyStyle(HSSFWorkbook workbook)
	{
		HSSFCellStyle sheetStyle = workbook.createCellStyle();    
		sheetStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		
		sheetStyle.setLeftBorderColor(HSSFColor.BLACK.index);
		sheetStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		sheetStyle.setRightBorderColor(HSSFColor.BLACK.index);
		sheetStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		sheetStyle.setBottomBorderColor(HSSFColor.BLACK.index);
		sheetStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		sheetStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		sheetStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		sheetStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		return sheetStyle;
	}
	
	
	protected  String getDateStr(Date date)
	{
		String result="";
		if(date!=null)
		{
			result=  DateUtils.format(date, "yyyy-MM-dd");
		}
		return result;
	}
	
	protected  String getBigDecimalStr(BigDecimal bigdecimal)
	{
		String result="";
		if(bigdecimal!=null)
		{
			result=bigdecimal.toString();
		}
		
		return result;
	}
	
	protected  String getAmountFormatStr(BigDecimal bigdecimal)
	{
		String result="";
		if(bigdecimal!=null)
		{
			DecimalFormat df=new DecimalFormat("#0.00");
			result=df.format(bigdecimal.setScale(2,RoundingMode.HALF_DOWN).doubleValue());
		}
		
		return result;
	}
	
	protected  String getIntegerStr(Integer bigdecimal)
	{
		String result="";
		if(bigdecimal!=null)
		{
			result=bigdecimal.toString();
		}
		return result;
	}
	
	 /**
	  * excel输出
	 * @param response
	 * @param wb
	 * @param fileName
	 * @throws IOException
	 */
	public void export(HttpServletResponse response,HSSFWorkbook wb, String fileName) throws IOException {
			// 设置response的编码方式
			response.setContentType("application/octet-stream");

			// 设置附加文件名
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

			OutputStream output =response.getOutputStream();
			wb.write(output);

			output.flush();
			output.close();

	}
}
