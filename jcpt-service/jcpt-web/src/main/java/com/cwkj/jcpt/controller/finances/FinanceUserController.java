package com.cwkj.jcpt.controller.finances;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.scf.model.finances.FinanceUserDo;
import com.cwkj.scf.service.finances.FinanceUserService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysRoleService;
import com.cwkj.jcptsystem.service.system.SysUserService;

/**
 * 金融机构用户.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/finances/")
public class FinanceUserController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(FinanceUserController.class);
	
	private FinanceUserService financeUserService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	public void setFinanceUserService(FinanceUserService financeUserService)
	{
		this.financeUserService=financeUserService;
	}
	
	/**
	 * 金融机构用户数据分页
	 * @return
	 */
	@RequestMapping(value="/financeUserList.do",method=RequestMethod.GET)
	public ModelAndView financeUserList_methodGet(String financeId)
	{
		ModelAndView view=new ModelAndView("finances/financeUserList");
		view.addObject("financeId", financeId);
		return view;
	}
	
	/**
	 * 金融机构用户数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/financeUserList.do",method=RequestMethod.POST)
	public ModelAndView financeUserList_methodPost(HttpServletRequest request,String financeId,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("finances/financeUserList_table");
		try{ 
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 selectItem.put("financeId", financeId);
			PageDo<FinanceUserDo> pagedata= financeUserService.queryWithSysUserListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取金融机构用户数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增金融机构用户
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addFinanceUser.do",method=RequestMethod.GET)
	public ModelAndView addFinanceUser_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("finances/addFinanceUser");
		try{
			String financeId=request.getParameter("financeId");
			view.addObject("financeUser", null);
			view.addObject("financeId", financeId);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加金融机构用户异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增金融机构用户
	 * @param request
	 * @param financeUser
	 * @return
	 */
	@RequestMapping(value="/addFinanceUser.do",method=RequestMethod.POST)
	public ModelAndView addFinanceUser_methodPost(HttpServletRequest request,FinanceUserDo financeUser)
	{
		ModelAndView view=new ModelAndView("finances/addFinanceUser");
		try{
			String buildUserTagStr=request.getParameter("buildUserTagStr");
			 String sexStr=request.getParameter("sexStr");
			 String password=SystemConst.SYS_USER_PASSWORD_DEFAULT;
			 if(StringUtil.isNotBlank(sexStr))
			 {
				 financeUser.setSex(Integer.valueOf(sexStr));
			 }
			 if(StringUtil.isNotBlank(buildUserTagStr))
			 {
				 financeUser.setBuildUserTag(Integer.valueOf(buildUserTagStr));
			 }
			 
			 if(StringUtil.isNotBlank(financeUser.getUserName()))
			 {//添加系统账号
				 SysUserDo user=sysUserService.findSysUserByUserName(financeUser.getUserName());
					if(user!=null)
					{
						throw new BusinessException("账户名已被使用");
					}
					user=new SysUserDo();
					user.setLocked(1);//未锁定
					user.setUserName(financeUser.getUserName());
					user.setRealName(financeUser.getRealName());
					user.setTelphone(financeUser.getMobile());
					String psw=DigestUtils.md5Hex(password+ SystemConst.PASS_KEY); 
					user.setPassword(psw);
					user.setPlatformId(currentPlatformId());
				int dbRes=sysUserService.insertSysUser(user);
				if(dbRes>0)
				{
					sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_FINANCES}, user.getId());
				}
				financeUser.setUserId(user.getId());
				
			 }
			 financeUser.setRecordUserId(currentUser().getId());
			 financeUser.setParentUserId(financeUser.getRecordUserId());
			Integer result= financeUserService.insertFinanceUser(financeUser);
			AssertUtils.isTrue(result==1, "添加用户失败[err:001]");//err:001,数据库新增记录失败
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加金融机构用户异常（POST）",e);
		}finally {
			view.addObject("financeUser", financeUser);
			view.addObject("financeId", financeUser.getFinanceId());
		}
		return view;
	}
	
	 /**
	 * 浏览金融机构用户
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewFinanceUser.do",method=RequestMethod.GET)
	public ModelAndView viewFinanceUser_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("finances/viewFinanceUser");
		try{
			FinanceUserDo financeUser= financeUserService.findFinanceUserById(id);
			view.addObject("financeUser", financeUser);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览金融机构用户异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑金融机构用户
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editFinanceUser.do",method=RequestMethod.GET)
	public ModelAndView editFinanceUser_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("finances/addFinanceUser");
		try{
			FinanceUserDo financeUser= financeUserService.findFinanceUserById(id);
			view.addObject("financeUser", financeUser);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑金融机构用户异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑金融机构用户
	 * @param request
	 * @param financeUser
	 * @return
	 */
	@RequestMapping(value="/editFinanceUser.do",method=RequestMethod.POST)
	public ModelAndView editFinanceUser_methodPost(HttpServletRequest request,FinanceUserDo  financeUser)
	{
		ModelAndView view =new ModelAndView("finances/addFinanceUser");
		try{
			AssertUtils.isNotBlank(financeUser.getId(), "参数无效");
			Integer result= financeUserService.updateFinanceUserById(financeUser);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑金融机构用户异常（GET）",e);
		}finally {
			view.addObject("financeUser", financeUser);
		}
		return view;
	}
	
	/**
	 * 删除金融机构用户
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deleteFinanceUser.do")
	public JsonView<String> deleteFinanceUser_methodPost(HttpServletRequest request,String id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.isNotBlank(id, "参数无效");
			Integer result=financeUserService.deleteFinanceUserById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除金融机构用户异常",e);
		}
		return view;
	}
	 
}
