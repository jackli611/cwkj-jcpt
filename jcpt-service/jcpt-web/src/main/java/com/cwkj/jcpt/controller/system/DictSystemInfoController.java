/**
 * Project Name:jcpt-web <br>
 * File Name:DictSystemInfoController.java <br>
 * Package Name:com.hehenian.jcpt.controller.system <br>
 * @author anxymf
 * Date:2016年12月7日上午10:33:34 <br>
 * Copyright (c) 2016, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcpt.controller.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.DictSystemInfoDo;
import com.cwkj.jcptsystem.service.system.DictSystemInfoService;
import com.cwkj.redis.RedisKeyConst;
import com.cwkj.redis.system.RedisSystemService;
/**
 * 系统信息字典
 * @version
 */
@Controller
@RequestMapping("/system/dictSystemInfo")
public class DictSystemInfoController extends BaseAction {
	
	private static Logger log=LoggerFactory.getLogger(DictSystemInfoController.class);
	
	/** 系统信息字典服务  */
	@Resource
	private DictSystemInfoService dictSystemInfoService;
	
	/** redis */
	@Resource
	private RedisSystemService redisSystemService;
	
	
	
	/**
	 * 
	 * list:系统信息字典查询页面. <br>
	 * @return
	 */
	@RequestMapping(value = "/list.do", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/system/dictSystemInfo/list");
	}
	
	/**
	 * 
	 * listData:系统信息字典查询数据. <br>
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/listData.do")
	public ModelAndView listData(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("/system/dictSystemInfo/listData");
		try {
			checkPermission(PermissionCode.SYSTEM_DICT_LIST);
			Map<String,Object> selectItem = getRequestToParamMap(request);
			selectItem.put("isValid",DictSystemInfoDo.ISVALID_T);
			model.addObject("pagedata",dictSystemInfoService.queryListPage(selectItem));
		} catch (BusinessException e) {
			setPromptException(model, e);
		} catch (Exception e) {
			log.error("获取系统信息字典列表异常", e);
			setPromptException(model, e);
		}
		return model;
	}
	
	/**
	 * 
	 * preShow:新增修改显示页面. <br>
	 * @param codeId 字典ID
	 * @return
	 */
	@RequestMapping(value = "/preShow.do")
	public ModelAndView preShow(String codeId){
		ModelAndView model = new ModelAndView("/system/dictSystemInfo/preShow");
		try{
			if(StringUtil.isNotEmpty(codeId)){
				checkPermission(PermissionCode.SYSTEM_DICT_EDIT);
				model.addObject("model",dictSystemInfoService.queryById(codeId));
			}else{
				checkPermission(PermissionCode.SYSTEM_DICT_ADD);
			}
		} catch (BusinessException e) {
			setPromptException(model, e);
		} catch (Exception e) {
			log.error("获取系统信息字典记录异常", e);
			setPromptException(model, e);
		}
		return model;
	}
	
	/**
	 * 
	 * add:新增系统信息字典. <br>
	 * @param dictSystemInfoDo 字典对象
	 * @return
	 */
	@RequestMapping(value = "/add.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<DictSystemInfoDo> add(DictSystemInfoDo dictSystemInfoDo){
		JsonView<DictSystemInfoDo> view = new JsonView<DictSystemInfoDo>();
		try {
			checkPermission(PermissionCode.SYSTEM_DICT_ADD);
			dictSystemInfoDo.setIsValid(DictSystemInfoDo.ISVALID_T);
			Integer model = dictSystemInfoService.insert(dictSystemInfoDo);
			if(model>0){
				String codeType = dictSystemInfoDo.getCodeType();
				redisSystemService.setKeyPrefix(RedisKeyConst.JCPT_DICT_SYS);
				redisSystemService.update(codeType,dictSystemInfoService.queryCodeList(codeType));
			}
			setPromptMessage(view,view.CODE_SUCCESS, "操作成功");
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			log.error("新增系统信息字典提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "新增系统信息字典提交异常");
		}
		return view;
	}
	
	/**
	 * 
	 * update:修改系统信息字典. <br>
	 * @param dictSystemInfoDo 字典对象
	 * @return
	 */
	@RequestMapping(value = "/update.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<DictSystemInfoDo> update(DictSystemInfoDo dictSystemInfoDo){
		JsonView<DictSystemInfoDo> view = new JsonView<DictSystemInfoDo>();
		try {
			checkPermission(PermissionCode.SYSTEM_DICT_EDIT);
			Integer model = dictSystemInfoService.update(dictSystemInfoDo);
			if(model>0){
				String codeType = dictSystemInfoDo.getCodeType();
				redisSystemService.setKeyPrefix(RedisKeyConst.JCPT_DICT_SYS);
				redisSystemService.update(codeType,dictSystemInfoService.queryCodeList(codeType));
			}
			setPromptMessage(view,view.CODE_SUCCESS, "操作成功");
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			log.error("修改系统信息字典提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "修改系统信息字典提交异常");
		}
		return view;
	}
	
	/**
	 * 
	 * delete:删除系统信息字典. <br>
	 *
	 * @author anxymf
	 * Date:2017年2月12日下午4:31:50 <br>
	 * @param codeId 字典ID
	 * @return
	 */
	@RequestMapping(value = "/delete.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<DictSystemInfoDo> delete(String codeId,String codeType){
		JsonView<DictSystemInfoDo> view = new JsonView<DictSystemInfoDo>();
		try {
			checkPermission(PermissionCode.SYSTEM_DICT_DELETE);
			Integer model = dictSystemInfoService.delete(codeId);
			if(model>0){
				redisSystemService.setKeyPrefix(RedisKeyConst.JCPT_DICT_SYS);
				redisSystemService.delete(codeType);
			}
			setPromptMessage(view,view.CODE_SUCCESS, "操作成功");
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			log.error("删除系统信息字典提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "删除系统信息字典提交异常");
		}
		return view;
	}
	
	@RequestMapping(value = "/updateCache.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<DictSystemInfoDo> updateCache(){
		JsonView<DictSystemInfoDo> view = new JsonView<DictSystemInfoDo>();
		try {
			
			checkPermission(PermissionCode.SYSTEM_DICT_CACHE);
			List<DictSystemInfoDo> list = dictSystemInfoService.queryAll();
			Map<String,List<DictSystemInfoDo>> map = new HashMap<String, List<DictSystemInfoDo>>();
			for(DictSystemInfoDo dict : list){
				List<DictSystemInfoDo> dicts = null;
				String  codeType = dict.getCodeType();
				if(map.containsKey(codeType)){
					dicts = map.get(codeType);
					dicts.add(dict);
				}else{
					dicts = new ArrayList<DictSystemInfoDo>();
					dicts.add(dict);
				}
				map.put(codeType, dicts);
			}
			for (Map.Entry<String, List<DictSystemInfoDo>> entry : map.entrySet()) {
				   redisSystemService.update(entry.getKey(),entry.getValue());
			}
			view.setCode(JsonView.CODE_SUCCESS);
			view.setMsg("更新缓存成功");
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			log.error("更新缓存成功提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "更新缓存失败");
		}
		return view;
	} 
	
}

	