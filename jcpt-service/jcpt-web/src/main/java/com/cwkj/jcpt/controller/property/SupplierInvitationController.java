package com.cwkj.jcpt.controller.property;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.service.property.PropertyOrgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.scf.model.property.SupplierInvitationDo;
import com.cwkj.scf.service.property.SupplierInvitationService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcptsystem.service.system.ParamService;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;

/**
 * 供应商邀请注册.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class SupplierInvitationController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(SupplierInvitationController.class);
	
	private SupplierInvitationService supplierInvitationService;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	@Autowired
	private ParamService paramService;
	@Autowired
	private PropertyOrgService propertyOrgService;

	
	@Autowired
	public void setSupplierInvitationService(SupplierInvitationService supplierInvitationService)
	{
		this.supplierInvitationService=supplierInvitationService;
	}

	/**
	 * 供应商邀请注册数据分页
	 * @return
	 */
	@RequestMapping(value="/supplierInvitationCode.do",method=RequestMethod.GET)
	public ModelAndView supplierInvitationList_methodGet(String propertyId)
	{
		ModelAndView view=new ModelAndView("property/supplierInvitationCode");

		try {
			PropertyOrgDo propertyOrg=propertyOrgService.findPropertyOrgById(propertyId);
			AssertUtils.notNull(propertyOrg,"无效的请求参数");
			SupplierInvitationDo supplierInvitation = supplierInvitationService.findByPropertyId(propertyId);
			String supplierInviteLink = paramService.queryValueByCode("SupplierInviteLink");
			if (supplierInvitation == null) {
				supplierInvitation = new SupplierInvitationDo();
				supplierInvitation.setInviteCode(sysSeqNumberService.daySeqnumberString());
				supplierInvitation.setLinks(supplierInviteLink + supplierInvitation.getInviteCode());
				supplierInvitation.setPropertyId(propertyId);
				supplierInvitation.setRecordUserId(currentUser().getId());
				//supplierInvitation.setExpiryDate(DateUtils.parse(expiryDateStr));
				Integer result = supplierInvitationService.insertSupplierInvitation(supplierInvitation);
				AssertUtils.isTrue(result == 1, "获取邀请注册码失败[error:002]");
			}
			supplierInvitation.setLinks(supplierInviteLink + supplierInvitation.getInviteCode());
			view.addObject("supplierInvitation", supplierInvitation);
		}catch (BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception ex)
		{
			logger.error("获取核心企业邀请码异常",ex);
			setPromptMessage(view, "获取邀请注册码失败[error:001]");
		}
		return view;
	}
	 
}
