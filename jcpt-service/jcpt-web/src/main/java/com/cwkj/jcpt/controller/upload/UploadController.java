package com.cwkj.jcpt.controller.upload;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.file.FileService;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.HttpServletResponseUtil;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.model.loan.Attachment;
import com.cwkj.scf.service.loan.IAttachmentService;
import com.cwkj.word.util.DocToPdf;


/**
 * 通用上传接口
 * 
 * @author harry
 */
@Controller
public class UploadController extends BaseAction {
	
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UploadController.class);

	@Autowired
	private FileService fileService;
	
	@Autowired
	private IAttachmentService attachmentService;
	
	
	/** 
	 * 
	 * @api {post} /upload/fileupload.do MultipartFile文件不带参
	 * @apiName fileupload  
	 * @apiGroup Common 
	 * @apiVersion 1.0.0 
	 * @apiDescription  MultipartFile文件不带参
	 *  
	 * @apiUse RETURN_MESSAGE
	 * @apiSuccess {String} filePath 文件保存的绝对路径 
	 * @apiSuccessExample Success-Response: 
	 *  HTTP/1.1 200 OK 
	 * {
	 *	"model": {
	 *		"filePath": "/upload/sc/images/20161227005210KOI5P4ew.png",
	 *		"viewPath": "http://127.0.0.1:90/upload/sc/images/20161227005210KOI5P4ew.png"
	 *	},
	 *	"success": true,
	 *	"errorMessage": null,
	 *	"resultCode": 200
	 *  }
	 *   
	 *  @apiError 305 对应<code>305</code> 图片保存失败  
	 *  @apiUse ERROR_405
	 */  
	@RequestMapping(value = "/fileupload")
	@ResponseBody
	public JsonView<String> imgUpload(HttpServletRequest request, HttpServletResponse response) {
		
		
		try {
			//读取上传的文件
			MultipartFile imgFile = null; 
			if (request instanceof MultipartHttpServletRequest) {
		            MultipartHttpServletRequest multipartHttpRequest = (MultipartHttpServletRequest) request;
		            Map<String, MultipartFile> multFileMap = multipartHttpRequest.getFileMap();
		            Iterator it = multFileMap.values().iterator();
		            imgFile = (MultipartFile)it.next();
			 }
			
			if(null == imgFile) {
				return JsonView.successJsonView("上传失败，没有获取到上传的文件");
			}
		
			// 文件保存服务器 返回保存后的磁盘路径
			String imgFilePath = fileService.saveFileByIntputStream(imgFile.getInputStream(), 
															 		imgFile.getOriginalFilename());
			
			if (StringUtils.isBlank(imgFilePath)) {
				return JsonView.successJsonView("图片保存失败");
			}
			return JsonView.successJsonView("图片保存成功", imgFilePath);
		}  catch (Exception e) {
			logger.error(e);
		}
		return JsonView.failureJsonView("上传失败!");
	}
	
	
	/**
	 * 	查看附件
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/toViewAttachment.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView showProtocolTemplate(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("attachments/viewpdf");
		try{ 
			String attachmentId = request.getParameter("attachmentId");
			if(null != attachmentId) {
				Attachment attachment =attachmentService.selectByPrimaryKey(Long.valueOf(attachmentId));
				view.addObject("attachmentId", attachmentId);
				view.addObject("attachment", attachment);
				
				if( StringUtil.isNotBlank(attachment.getFiletype()) ) {
					String ext = attachment.getFiletype().toLowerCase();
					if(ext.startsWith("jpg")||ext.startsWith("png")||ext.startsWith("jpeg")) {
						view.setViewName("attachments/viewimg");
					}else if(ext.startsWith("doc")) {
						view.setViewName("attachments/viewdoc");
					}
				}
			}
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取查看附件异常", e);
		}
		return view;
	}
	
	
    
    
    @RequestMapping(value="/viewattachment.do",method= {RequestMethod.GET,RequestMethod.POST})
    public void viewattachment(HttpServletRequest request, HttpServletResponse response){
        try {
            String idStr=request.getParameter("attachmentId");
            Long id=null;
            if(StringUtil.isEmpty(idStr))
            {
                return ;
            }else{
                try{
                    id=Long.valueOf(idStr);
                }catch(Exception ex){
                    logger.error("获取附件异常",ex);
                    return ;
                }
            }
            Attachment attachment =attachmentService.selectByPrimaryKey(id);
            logger.debug(attachment.getDestfilepath());
            File file=new File(attachment.getDestfilepath());
            if(file.exists()){
            	
                if(StringUtil.isNotBlank(attachment.getFiletype())){
                	String ext = attachment.getFiletype().toLowerCase();
                	
                    //设置MIME类型
                    response.setHeader("Pragma", "no-cache");
                    response.setHeader("Cache-Control", "no-cache");
                    response.setDateHeader("Expires", 0);
                    response.setContentType("application/octet-stream;charset=utf-8");
                }else {
                    response.setHeader("Content-disposition", "attachment; filename=" + attachment.getOriginalfilename());
                }
                
                /*
                if("docx".equalsIgnoreCase(attachment.getFiletype())|| "doc".equalsIgnoreCase(attachment.getFiletype())) {
                	DocToPdf.makePdfByXcode(new FileInputStream(file), response.getOutputStream());
                }else {
                	HttpServletResponseUtil.responseFile(response,file);
                }
                */
                HttpServletResponseUtil.responseFile(response,file);
            }
        }catch (Exception e) {
            logger.error("获取附件失败",e);
        }

    }

}
