package com.cwkj.jcpt.controller.platform;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcptsystem.model.system.HtmlMenu;
import com.cwkj.jcptsystem.service.system.HtmlMenuHandler;
import com.cwkj.jcptsystem.service.system.SysPermissionService;

/**
 * 平台
 * 
 * @author ljc
 * 
 */
@Controller
@RequestMapping("/platform/")
public class DashboardController extends BaseAction {

  private static Logger log = LoggerFactory.getLogger(DashboardController.class);
  
	@RequestMapping(value="/dashboard.do",method=RequestMethod.GET)
	public ModelAndView dashboard_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("platform/dashboard");
		try{
			view.addObject("propertyOrg", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业机构异常（GET）",e);
		}
		return view;
	}
}
