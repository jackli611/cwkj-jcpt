package com.cwkj.jcpt.controller.loanorder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.jcptsystem.service.system.SysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.DictSystemInfoDo;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.DictSystemInfoService;
import com.cwkj.scf.model.finances.FinanceOrgDo;
import com.cwkj.scf.model.loan.Attachment;
import com.cwkj.scf.model.loan.LoanOrderStatus;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.LoanPersonExample;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.finances.FinanceOrgService;
import com.cwkj.scf.service.loan.ILoanOrderService;
import com.cwkj.scf.service.property.PropertyOrgService;
import com.cwkj.scf.service.property.SupplierService;

/**
 * 个贷订单管理
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/personorder/")
public class PersonalOrderController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(PersonalOrderController.class);
	
	@Autowired
	private ILoanOrderService loanOrderSercice;
	@Autowired
	private SysUserService sysUserService;
	

	
	
	/**
	 * 	查询订单用户信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getLoanPerson.do")
	public ModelAndView getLoanPerson(HttpServletRequest request,Long loanId){
		ModelAndView view=  new ModelAndView("loanorder/loanPerson");
		 
		try{
			Loanorder order = loanOrderSercice.selectOrderAndAttachmentByPrimaryKey(loanId );
			view.addObject("order",order);
			view.addObject("loanid",loanId);
			LoanPersonExample example = new LoanPersonExample();
			example.createCriteria().andLoanidEqualTo(loanId);
			List<LoanPerson> personLst = loanOrderSercice.selectOrderPersonByExample(example );
			if(personLst !=null &&personLst.size()>0) {
				view.addObject("person",personLst.get(0));
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}

	public void serverContract(HttpServletRequest request,Long loanId)
	{

		try{
			LoanPersonExample example = new LoanPersonExample();
			example.createCriteria().andLoanidEqualTo(loanId);

			List<LoanPerson> personLst = loanOrderSercice.selectOrderPersonByExample(example );
			AssertUtils.isTrue(personLst !=null &&personLst.size()>0,"订单借款人信息不存在");
			LoanPerson loanPerson=personLst.get(0);
			String realName=loanPerson.getRealname();
			String idcardNo=loanPerson.getIdno();
			AssertUtils.notNull(loanPerson.getUserid(),"订单所属账号信息错误");
			SysUserDo sysUserDo=sysUserService.findSysUserById(loanPerson.getUserid().intValue());
			String mobile=sysUserDo.getTelphone();

			Loanorder order = loanOrderSercice.selectOrderAndAttachmentByPrimaryKey(loanId );
			AssertUtils.notNull(order,"借款订单不存在");




		}catch (Exception ex)
		{
			logger.error("获取个贷居间合同异常",ex);
		}
	}
	 
}
