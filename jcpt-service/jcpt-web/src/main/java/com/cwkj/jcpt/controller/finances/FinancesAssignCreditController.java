package com.cwkj.jcpt.controller.finances;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.model.property.PropertyFinancesDo;
import com.cwkj.scf.service.property.PropertyFinancesService;

/**
 * 资金机构給核心其他授信
 * @author harry
 */
@Controller
@RequestMapping(value="/finances/")
public class FinancesAssignCreditController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(FinancesAssignCreditController.class);
	
	private PropertyFinancesService propertyFinancesService;

	private Object promptCode;
	
	@Autowired
	public void setPropertyFinancesService(PropertyFinancesService propertyFinancesService)
	{
		this.propertyFinancesService=propertyFinancesService;
	}
	
	/**
	 * 物业的资金机构数据分页
	 * @return
	 */
	@RequestMapping(value="/propertyFinancesList.do",method=RequestMethod.GET)
	public ModelAndView propertyFinancesList_methodGet(String financeOrgId)
	{
		ModelAndView view=new ModelAndView("finances/propertyFinancesList");
		view.addObject("financeId", financeOrgId);
		return view;
	}
	
	/**
	 * 物业的资金机构数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/propertyFinancesList.do",method=RequestMethod.POST)
	public ModelAndView propertyFinancesList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("finances/propertyFinancesList_table");
		try{ 
			 Map<String, Object> selectItem=getRequestToParamMap(request);
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<PropertyFinancesDo> pagedata= propertyFinancesService.queryPropertyFinancesListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取物业的资金机构数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增物业的资金机构
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addPropertyFinances.do",method=RequestMethod.GET)
	public ModelAndView addPropertyFinances_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("finances/addPropertyFinances");
		try{
			String financeId=request.getParameter("financeId");
			PropertyFinancesDo propertyFinances=null;
			if(StringUtil.isNotBlank(financeId)){
				propertyFinances=new PropertyFinancesDo();
				propertyFinances.setFinanceId(financeId);
			}
			view.addObject("propertyFinances", propertyFinances);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("资金机构添加核心企業异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增物业的资金机构
	 * @param request
	 * @param propertyFinances
	 * @return
	 */
	@RequestMapping(value="/addPropertyFinances.do",method=RequestMethod.POST)
	public ModelAndView addPropertyFinances_methodPost(HttpServletRequest request,PropertyFinancesDo propertyFinances)
	{
		ModelAndView view=new ModelAndView("finances/addPropertyFinances");
		try{
			 
			AssertUtils.isNotBlank(propertyFinances.getFinanceId(), "资金机构不能为空");
			AssertUtils.isNotBlank(propertyFinances.getPropertyId(), "核心企业不能为空");
			String metaDateStr=request.getParameter("metaDateStr");
			if(StringUtil.isNotBlank(metaDateStr))
			{
				propertyFinances.setMetaDate(DateUtils.parse(metaDateStr));
			}
			propertyFinances.setStatus(PropertyFinancesDo.STATUS_VALID);
			propertyFinances.setRecordUserId(currentUser().getId());
			Integer result= propertyFinancesService.insertPropertyFinances(propertyFinances);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view,1, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业的资金机构异常（POST）",e);
		}finally {
			view.addObject("propertyFinances", propertyFinances);
		}
		return view;
	}
	
	 /**
	 * 浏览物业的资金机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewPropertyFinances.do",method=RequestMethod.GET)
	public ModelAndView viewPropertyFinances_methodGet(HttpServletRequest request,Integer id)
	{
		ModelAndView view =new ModelAndView("finances/viewPropertyFinances");
		try{
			PropertyFinancesDo propertyFinances= propertyFinancesService.findPropertyFinancesById(id);
			view.addObject("propertyFinances", propertyFinances);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览物业的资金机构异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑物业的资金机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editPropertyFinances.do",method=RequestMethod.GET)
	public ModelAndView editPropertyFinances_methodGet(HttpServletRequest request,Integer id)
	{
		ModelAndView view =new ModelAndView("finances/addPropertyFinances");
		try{
			PropertyFinancesDo propertyFinances= propertyFinancesService.findPropertyFinancesById(id);
			view.addObject("propertyFinances", propertyFinances);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业的资金机构异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑物业的资金机构
	 * @param request
	 * @param propertyFinances
	 * @return
	 */
	@RequestMapping(value="/editPropertyFinances.do",method=RequestMethod.POST)
	public ModelAndView editPropertyFinances_methodPost(HttpServletRequest request,PropertyFinancesDo  propertyFinances)
	{
		ModelAndView view =new ModelAndView("finances/addPropertyFinances");
		try{
			AssertUtils.notNull(propertyFinances.getId(), "参数无效");
			Integer result= propertyFinancesService.updatePropertyFinancesById(propertyFinances);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业的资金机构异常（GET）",e);
		}finally {
			view.addObject("propertyFinances", propertyFinances);
		}
		return view;
	}
	
	/**
	 * 删除物业的资金机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deletePropertyFinances.do")
	public JsonView<String> deletePropertyFinances_methodPost(HttpServletRequest request,Integer id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.notNull(id, "参数无效");
			Integer result=propertyFinancesService.deletePropertyFinancesById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除物业的资金机构异常",e);
		}
		return view;
	}
	 
}
