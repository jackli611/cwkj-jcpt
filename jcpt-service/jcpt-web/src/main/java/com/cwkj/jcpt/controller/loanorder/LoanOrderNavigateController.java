package com.cwkj.jcpt.controller.loanorder;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.model.loan.OrderNavigateDo;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.service.loan.IOrderNavigateService;

/**
 * 订单导航
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/loanorder/")
public class LoanOrderNavigateController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(LoanOrderNavigateController.class);
	@Autowired
	private IOrderNavigateService orderNavigateService;
	
	
	/**
	 * 导航跳转到订单列表页面
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/orderNavigate.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView orderNavigate(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("loanorder/order_navigate_list");
		try{
			view.addObject("productCode", ProductEnum.PROD_SCF.getProductCode());
			request.getSession().setAttribute("navigateProductCode", ProductEnum.PROD_SCF.getProductCode());
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("跳转到订单导航页面异常", e);
		}
		return view;
	}
	
	/**
	 * 导航跳转到订单列表页面
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/orderPersonNavigate.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView orderPersonNavigate(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("loanorder/order_navigate_list");
		try{ 
			//查看个贷产品
			view.addObject("productCode", ProductEnum.PROD_PERSONAL_XY.getProductCode());
			request.getSession().setAttribute("navigateProductCode", ProductEnum.PROD_PERSONAL_XY.getProductCode());
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("跳转到订单导航页面异常", e);
		}
		return view;
	}
	

		/**
		 * 订单导航入口数据分页
		 * @param request
		 * @param pageIndex
		 * @param pageSize
		 * @param startDate  
		 * @param endDate  
		 * @return
		 */
		@RequestMapping(value="/orderNavigateTable.do",method= {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView orderNavigateTable(HttpServletRequest request,
											   Long pageIndex,
											   Integer pageSize,
											   String startDate,
											   String endDate){
			
			ModelAndView view=new ModelAndView("loanorder/order_navigate_table");
			String productCode = request.getParameter("productCode");
			view.addObject("productCode", productCode);
			try{ 
				checkPermission(PermissionCode.PROPERTYORG_LIST_QUERY);
				 Map<String, Object> selectItem=new HashMap<String, Object>();
				 String names = request.getParameter("names");
				 setDateBetweemToMap(selectItem, startDate, endDate);
				 pageIndex=pageIndex==null?1L:pageIndex;
				 pageSize=pageSize==null?20:pageSize;
				 if(StringUtil.isNotBlank(names)) {
					 selectItem.put("names", names);
				 }
				PageDo<OrderNavigateDo> pagedata= orderNavigateService.queryOrderNavigateListPage(pageIndex,pageSize, selectItem);
				view.addObject("pagedata",pagedata);
				
			}catch(BusinessException e)
			{
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("获取订单导航分页异常", e);
			}
			return view;
		}
}
