package com.cwkj.jcpt.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.ParamService;
import com.cwkj.jcptsystem.service.system.SysUserService;

/**
 * 用户管理
 * @author ljc
 * @version 1.0
 */
@Controller
@RequestMapping(value="/system/")
public class SysUserController extends BaseAction{
	private static Logger logger=LoggerFactory.getLogger(SysUserController.class);
	@Autowired
	private SysUserService sysUserService;
	
	@Resource
	private ParamService paramService;
	
	
	@RequestMapping(value="/queryUserList.do",method=RequestMethod.GET)
	public ModelAndView queryUserList_methodGet()
	{
		ModelAndView view=new ModelAndView("system/queryUserList");
		return view;
	}
	
	@RequestMapping(value="/queryUserList.do",method=RequestMethod.POST)
	public ModelAndView queryUserList_methodPost(HttpServletRequest request,String startDate,String endDate, Long pageIndex,Integer pageSize)
	{
		ModelAndView view=new ModelAndView("system/queryUserList_table");
		try{
			checkPermission(PermissionCode.system_user_list);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 selectItem.put("userName", request.getParameter("userName"));
			 selectItem.put("realName", request.getParameter("realName"));
			 selectItem.put("telphone", request.getParameter("telphone"));
			 setDateBetweemToMap(selectItem,startDate,endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 if(SystemConst.PLATFORM_DEPOSITORY== currentPlatformId())
			 {
				 selectItem.put("platformId",SystemConst.PLATFORM_DEPOSITORY);
			 }
			 PageDo<SysUserDo> result= sysUserService.querySysUserListPage(pageIndex, pageSize, selectItem);
			 view.addObject("pagedata",result);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询用户数据分页异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/addUser.do",method=RequestMethod.GET)
	public ModelAndView addUser_methodGet(Integer id)
	{
		ModelAndView view=new ModelAndView("system/editUser");
		
		try{
			SysUserDo sysUser = sysUserService.findSysUserById(id);
			view.addObject("sysUser", sysUser);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("根据用户ID获取用户异常", e);
		}
		
		return view;
	}
	
	@RequestMapping(value="/addUser.do",method=RequestMethod.POST)
	public JsonView<String> addUser(SysUserDo user,String password2)
	{
		if(null != user.getId()){//修改用户
			return editUser(user,password2);
		}
		
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.system_user_add);
			checkEmpty(user.getUserName(), "用户名不能为空");
			checkEmpty(user.getTelphone(), "手机号不能为空");
			checkEmpty(user.getPassword(), "用户密码不能为空");
			SysUserDo dbUser=sysUserService.findSysUserByUserName(user.getUserName());
			if(dbUser!=null)
			{
				throw new BusinessException("用户名已存在");
			}
			if(!Pattern.compile(SystemConst.REGEX_TELEPHONE).matcher(user.getTelphone()).matches()){
				throw new BusinessException("手机号格式不正确");
			}
			if(!Pattern.compile(SystemConst.REGEX_PWD).matcher(user.getPassword()).matches()){
				throw new BusinessException("密码不少于8位,必须包括大写、小写和数字");
			}
			if(!user.getPassword().equals(password2))
			{
				throw new BusinessException("两次密码不一致");
			}
			
			if(user.getLocked()==null)
			{
				user.setLocked(1);//未锁定
			}
			String psw=DigestUtils.md5Hex(password2+ SystemConst.PASS_KEY); 
			user.setPassword(psw);
			user.setPlatformId(currentPlatformId());
			sysUserService.insertSysUser(user);
			view.setCode(view.CODE_SUCCESS);
			view.setMsg("操作成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加用户异常", e);
		}
		return view;
	}
	
	/** 
	 * 修改用户
	 * @author: harry
	 * @date: 2017年7月27日 下午5:53:36 
	 * @param user
	 * @param password2
	 * @return 
	 */
	public JsonView<String> editUser(SysUserDo user,String password2)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.system_user_edit);
			checkEmpty(user.getTelphone(), "手机号不能为空");
			if(!Pattern.compile(SystemConst.REGEX_TELEPHONE).matcher(user.getTelphone()).matches()){
				throw new BusinessException("手机号格式不正确");
			}
			if(user.getLocked()==null)
			{
				user.setLocked(1);//未锁定
			}
			SysUserDo newUser = new SysUserDo();
			newUser.setId(user.getId());
			newUser.setRealName(user.getRealName());
			newUser.setTelphone(user.getTelphone());
			newUser.setLocked(user.getLocked());
			sysUserService.updateSysUserById(newUser);
			view.setCode(view.CODE_SUCCESS);
			view.setMsg("操作成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("修改用户异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/modifyUserRoles.do",method=RequestMethod.POST)
	public JsonView<String> modifyUserRoles(String roleListStr,Integer userId)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.system_user_role_setting);
			checkEmpty(userId, "请先选择用户");
			String[] roles=null;
			if(StringUtil.isNotEmpty(roleListStr))
			{
				roles=roleListStr.split(",");
			}
			sysUserService.modifyUserRoles(roles, userId);
			view.setCode(view.CODE_SUCCESS);
			view.setMsg("操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("修改用户角色异常",e);
		}
		return view;
	}
	
	@RequestMapping(value="/queryRoleUserList.do",method=RequestMethod.GET)
	public ModelAndView queryRoleUserList_methodGet(HttpServletRequest request,String roleId)
	{
		ModelAndView view=new ModelAndView("system/queryRoleUserList");
		try{
			view.addObject("roleId", roleId);
		}catch (Exception e) {
			logger.error("获取角色用户集异常",e);
		}
		return view;
	}
	
	@RequestMapping(value="/queryRoleUserList.do",method=RequestMethod.POST)
	public ModelAndView queryRoleUserList_methodPost(HttpServletRequest request,String startDate,String endDate, Long pageIndex,Integer pageSize)
	{
		ModelAndView view=new ModelAndView("system/queryRoleUserList_table");
		try{
			checkPermission(PermissionCode.system_role_permission_edit);
			String roleId=request.getParameter("roleId");
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 selectItem.put("userName", request.getParameter("userName"));
			 selectItem.put("realName", request.getParameter("realName"));
			 setDateBetweemToMap(selectItem,startDate,endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 if(SystemConst.PLATFORM_DEPOSITORY == currentPlatformId())
			 {
				 selectItem.put("platformId", SystemConst.PLATFORM_DEPOSITORY);
			 }
			 PageDo<SysUserDo> result= sysUserService.queryUserByRoleIdListPage(roleId, pageIndex, pageSize, selectItem);
			 view.addObject("pagedata",result);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询用户数据分页异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/modifyUserLock.do",method=RequestMethod.POST)
	public JsonView<String> modifyUserLock(String roleListStr,Integer userId,Boolean isLock)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.system_user_lock);
			checkEmpty(userId, "请先选择用户");
			AssertUtils.notNull(isLock, "请选择锁定状态");
			Integer dbResult=sysUserService.updateUserLockStatus(userId, isLock);
			AssertUtils.isTrue(dbResult!=null && dbResult.intValue()>0,"保存用户锁定状态失败");
			view.setCode(view.CODE_SUCCESS);
			view.setMsg("操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("修改用户锁定状态异常",e);
		}
		return view;
	}
	
	@RequestMapping(value="/deleteUser.do",method=RequestMethod.POST)
	public JsonView<String> deleteUser(String roleListStr,Integer userId)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.system_user_delete);
			checkEmpty(userId, "请先选择用户");
			Integer dbResult=sysUserService.deleteSysUserById(userId);
			AssertUtils.isTrue(dbResult!=null && dbResult.intValue()>0,"注销用户失败");
			view.setCode(view.CODE_SUCCESS);
			view.setMsg("操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("注销用户失败异常",e);
		}
		return view;
	}
	
	/**
	 * 
	 * releaseLoan:(这里用一句话描述这个方法的作用). <br>
	 *
	 * @author anxymf
	 * Date:2017年4月26日下午3:32:10 <br>
	 * @param userId
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/resetPassword.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<String> resetPassword(Integer userId,@RequestParam(defaultValue="")String password){
		JsonView<String> view = new JsonView<String>();
		try {
			checkPermission(PermissionCode.system_user_password_reset);
			if(StringUtil.isBlank(password)){
				password = paramService.queryValueByCode("DEFAULT_PASSWORD");
				password = StringUtil.isNotBlank(password)?password:"Aa123456";
			}
			String newPsw=DigestUtils.md5Hex(password.trim()+SystemConst.PASS_KEY);
			if(sysUserService.resetPassword(userId, newPsw,null) == 1){//管理员重置密码不需要修改用户的modifyPwdTime
				view.setCode(JsonView.CODE_SUCCESS);
				view.setMsg("重置密码成功");
			}else{
				view.setCode(JsonView.CODE_SUCCESS);
				view.setMsg("重置密码失败");
			}
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			logger.error("重置密码提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "重置密码提交异常");
		}
		return view;
	}
	
	
}
