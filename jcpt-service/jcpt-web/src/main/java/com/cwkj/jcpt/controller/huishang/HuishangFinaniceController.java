package com.cwkj.jcpt.controller.huishang;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.pay.PayCommponent;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.huishang.api.AccountBalanceForm;
import com.cwkj.scf.huishang.api.QueryTradeDetailForm;
import com.cwkj.scf.utils.HuiShangUtil;

/**
 *  徽商对账
 * @author harry
 *
 */
@Controller
@RequestMapping(value="/finanice/huishang")
public class HuishangFinaniceController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(HuishangFinaniceController.class);
	

    @Autowired
    private PayCommponent payCommponent;
	
    
    
    /**
     * 	徽商流水
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value="/platformAmt.do")
    public ModelAndView getPlatformAmt(HttpServletRequest request,Long loanid){
    	ModelAndView view=  new ModelAndView("huishang/platformAmt");
    	try{
    		AccountBalanceForm form = new AccountBalanceForm();
    		form.setOrderNo("-1");
    		form.setUserId(this.currentUser().getId().toString());
    		form.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
    		form.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
    		BaseResult<Map> result = payCommponent.getPlatformTotalAmt(form);
    		AssertUtils.isTrue(result.resultSuccess(),"查询余额失败:"+result.getMsg());
    		System.out.println(result.getData());
    		view.addObject("platformAmt", result.getData());
    		
    	}catch (BusinessException e) {
    		setPromptException(view, e);
    	}catch (Exception e) {
    		setPromptException(view, e);
    		logger.error("查询徽商流水出错",e);
    	}
    	return view;
    }
	/**
	 * 	徽商流水
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/flow.do")
	public ModelAndView flow(HttpServletRequest request,Long loanid){
		ModelAndView view=  new ModelAndView("huishang/flow");
		return view;
	}
	
	
	
	/**
	 * 借款订单数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/tradeDtlList.do")
	public ModelAndView tradeDtlList(HttpServletRequest request,
										Long pageIndex,
										Integer pageSize,
										String startDate,
										String endDate){
		
		String viewName="huishang/flowlist_table";
		
		ModelAndView view=new ModelAndView(viewName);
		
		try{
			
			QueryTradeDetailForm form = new QueryTradeDetailForm();
			String STARTDATE= request.getParameter("STARTDATE");
			String ENDDATE= request.getParameter("ENDDATE");
			
			String NX_TRNN= request.getParameter("NX_TRNN");
			String NX_INPT= request.getParameter("NX_INPT");
			String NX_RELD= request.getParameter("NX_RELD");
			String NX_INPD= request.getParameter("NX_INPD");
			String RTN_IND= request.getParameter("RTN_IND");
			
			if(StringUtil.isNotBlank(NX_INPD)) {
				form.setNX_INPD(NX_INPD);
			}
			if(StringUtil.isNotBlank(NX_INPT)) {
				form.setNX_INPT(NX_INPT);
			}
			if(StringUtil.isNotBlank(NX_TRNN)) {
				form.setNX_TRNN(NX_TRNN);
			}
			if(StringUtil.isNotBlank(NX_RELD)) {
				form.setNX_RELD(NX_RELD);
			}
			if("1".equals(RTN_IND)) {
				form.setRTN_IND(RTN_IND);
			}
			if(StringUtil.isNotBlank(STARTDATE)) {
				form.setSTARTDATE(STARTDATE);
			}
			if(StringUtil.isNotBlank(ENDDATE)) {
				form.setENDDATE(ENDDATE);
			}
			
			form.setOrderNo("-1");
			form.setUserId(this.currentUser().getId().toString());
			form.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
			form.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
			BaseResult<Map> result = payCommponent.queryTradeDtl(form);
			AssertUtils.isTrue(result.resultSuccess(),"明细查询失败:"+result.getMsg());
			 
			Map<String,Object> retMap = result.getData();
//			String  rowListJson = (String)retMap.get("SUBPACKS");
//			List<Map> rows = JSONArray.parseArray(rowListJson, Map.class);
			view.addObject("huishangflows",retMap);
//			view.addObject("rows",rows);
			PageDo<Map> pagedata= new PageDo();
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取借款订单数据分页异常", e);
		}
		return view;
	}
	
	
}
