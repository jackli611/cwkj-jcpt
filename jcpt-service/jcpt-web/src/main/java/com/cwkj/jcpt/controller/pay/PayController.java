package com.cwkj.jcpt.controller.pay;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.loanorder.LoanOrderAuditController;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.scf.model.loan.Loanorder;

/**
 * 退款
 * @author harry
 *
 */
@Controller
@RequestMapping(value="/pay/")
public class PayController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(PayController.class);
	

    @Autowired
    private PayCommponent payCommponent;
	
	
	/**
	 * 	退款 json返回
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/drawback.do")
	public JsonView<Loanorder> drawback(HttpServletRequest request)
	{
		JsonView<Loanorder> view=  JsonView.successJsonView();
		try{
			BaseResult<Map> result = payCommponent.withdraw(request,this.currentUser());
			AssertUtils.isTrue(result.resultSuccess(),"退款失败:"+result.getMsg());
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("退款出错",e);
		}
		return view;
	}
	
}
