package com.cwkj.jcpt.controller.products;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cwkj.scf.model.products.PersonalLoanProductOrderDo;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.service.products.PersonalLoanProductOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.scf.model.products.PersonalLoanProductDo;
import com.cwkj.scf.service.products.PersonalLoanProductService;
import com.cwkj.scf.service.products.ProductService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;

/**
 * 资金机构个贷产品.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/products/")
public class PersonalLoanProductController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(PersonalLoanProductController.class);
	@Autowired
	private ProductService productService;
	
	private PersonalLoanProductService personalLoanProductService;
	@Autowired
	private PersonalLoanProductOrderService personalLoanProductOrderService;
	
	@Autowired
	public void setPersonalLoanProductService(PersonalLoanProductService personalLoanProductService)
	{
		this.personalLoanProductService=personalLoanProductService;
	}
	
	/**
	 * 资金机构个贷产品数据分页
	 * @return
	 */
	@RequestMapping(value="/personalLoanProductList.do",method=RequestMethod.GET)
	public ModelAndView personalLoanProductList_methodGet()
	{
		ModelAndView view=new ModelAndView("products/personalLoanProductList");
		return view;
	}
	
	/**
	 * 资金机构个贷产品数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/personalLoanProductList.do",method=RequestMethod.POST)
	public ModelAndView personalLoanProductList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("products/personalLoanProductList_table");
		try{ 
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<PersonalLoanProductDo> pagedata= personalLoanProductService.queryPersonalLoanProductListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取资金机构个贷产品数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增资金机构个贷产品
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/financesPersonalLoanProduct.do",method=RequestMethod.GET)
	public ModelAndView addPersonalLoanProduct_methodGet(HttpServletRequest request,String financeId)
	{
		ModelAndView view=new ModelAndView("products/addPersonalLoanProduct");
		try{
			view.addObject("financeId",financeId);
			PersonalLoanProductDo personalLoanProductDo=new PersonalLoanProductDo();
			personalLoanProductDo.setFinanceId(financeId);

			view.addObject("personalLoanProduct", personalLoanProductDo);

			List<PersonalLoanProductDo> financeProductList= personalLoanProductService.queryByFinanceId(financeId);
			view.addObject("financeProductList",financeProductList);
			
			
			//产品列表
			Map<String, Object> selectItem = new HashMap<String,Object>();
			selectItem.put("levels", 2);
			selectItem.put("parentCode", ProductEnum.PROD_PERSONAL_XY.getProductCode());
			List<ProductDo> productLst = productService.queryProductList(selectItem);
			view.addObject("productLst",productLst);
			

		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加资金机构个贷产品异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增资金机构个贷产品
	 * @param request
	 * @param personalLoanProduct
	 * @return
	 */
	@RequestMapping(value="/addPersonalLoanProduct.do",method=RequestMethod.POST)
	public ModelAndView addPersonalLoanProduct_methodPost(HttpServletRequest request,PersonalLoanProductDo personalLoanProduct)
	{
		ModelAndView view=new ModelAndView("products/addPersonalLoanProduct");
		List<PersonalLoanProductDo> financeProductList=null;
		try{
			personalLoanProduct.setRecordUserId(currentUser().getId());
			Integer result= personalLoanProductService.insertPersonalLoanProduct(personalLoanProduct);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
			financeProductList= personalLoanProductService.queryByFinanceId(personalLoanProduct.getFinanceId());
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加资金机构个贷产品异常（POST）",e);
		}finally {
			view.addObject("personalLoanProduct", personalLoanProduct);
			view.addObject("financeProductList",financeProductList);
		}
		return view;
	}
	
	 /**
	 * 浏览资金机构个贷产品
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewPersonalLoanProduct.do",method=RequestMethod.GET)
	public ModelAndView viewPersonalLoanProduct_methodGet(HttpServletRequest request,Long id)
	{
		ModelAndView view =new ModelAndView("products/viewPersonalLoanProduct");
		try{
			PersonalLoanProductDo personalLoanProduct= personalLoanProductService.findPersonalLoanProductById(id);
			view.addObject("personalLoanProduct", personalLoanProduct);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览资金机构个贷产品异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑资金机构个贷产品
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editPersonalLoanProduct.do",method=RequestMethod.GET)
	public ModelAndView editPersonalLoanProduct_methodGet(HttpServletRequest request,Long id)
	{
		ModelAndView view =new ModelAndView("products/addPersonalLoanProduct");
		try{
			PersonalLoanProductDo personalLoanProduct= personalLoanProductService.findPersonalLoanProductById(id);
			view.addObject("personalLoanProduct", personalLoanProduct);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑资金机构个贷产品异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑资金机构个贷产品
	 * @param request
	 * @param personalLoanProduct
	 * @return
	 */
	@RequestMapping(value="/editPersonalLoanProduct.do",method=RequestMethod.POST)
	public ModelAndView editPersonalLoanProduct_methodPost(HttpServletRequest request,PersonalLoanProductDo  personalLoanProduct)
	{
		ModelAndView view =new ModelAndView("products/addPersonalLoanProduct");
		try{
			AssertUtils.notNull(personalLoanProduct.getId(), "参数无效");
			Integer result= personalLoanProductService.updatePersonalLoanProductById(personalLoanProduct);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑资金机构个贷产品异常（GET）",e);
		}finally {
			view.addObject("personalLoanProduct", personalLoanProduct);
		}
		return view;
	}
	
	/**
	 * 删除资金机构个贷产品
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deletePersonalLoanProduct.do")
	public JsonView<String> deletePersonalLoanProduct_methodPost(HttpServletRequest request,Long id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.notNull(id, "参数无效");
			Integer result=personalLoanProductService.deletePersonalLoanProductById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除资金机构个贷产品异常",e);
		}
		return view;
	}

	/**
	 * 资金机构个贷产品列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/financesPersonalLoanProductList.do",method=RequestMethod.GET)
	public ModelAndView financesPersonalLoanProductList_methodGet(HttpServletRequest request,String financeId)
	{
		ModelAndView view=new ModelAndView("products/financesPersonalLoanProductList");
		try{
			view.addObject("financeId",financeId);
			PersonalLoanProductDo personalLoanProductDo=new PersonalLoanProductDo();
			personalLoanProductDo.setFinanceId(financeId);

			view.addObject("personalLoanProduct", personalLoanProductDo);
			List<PersonalLoanProductDo> financeProductList= personalLoanProductService.queryByFinanceId(financeId);
			view.addObject("financeProductList",financeProductList);
			
			
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("资金机构个贷产品异常（GET）",e);
		}
		return view;
	}




}
