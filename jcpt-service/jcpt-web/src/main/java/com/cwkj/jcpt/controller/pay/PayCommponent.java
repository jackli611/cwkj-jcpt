package com.cwkj.jcpt.controller.pay;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.IPUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.component.system.ParamComponent;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.scf.config.HuiShangConfig;
import com.cwkj.scf.huishang.api.AccountBalanceForm;
import com.cwkj.scf.huishang.api.QueryTradeDetailForm;
import com.cwkj.scf.huishang.api.SignConfirmForm;
import com.cwkj.scf.huishang.api.UserRealNameAuthForm;
import com.cwkj.scf.huishang.api.WithDrawForm;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.LoanPersonExample;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.LoanorderExample;
import com.cwkj.scf.model.loan.Orderaudit;
import com.cwkj.scf.model.loan.PayInfoBean;
import com.cwkj.scf.model.loan.SysUserAuth;
import com.cwkj.scf.model.loan.SysUserAuthExample;
import com.cwkj.scf.model.payform.DeductForm;
import com.cwkj.scf.model.sms.MessageFormatType;
import com.cwkj.scf.model.sms.MessageTemplate;
import com.cwkj.scf.model.sms.SMSBusinessType;
import com.cwkj.scf.model.sms.TMessage;
import com.cwkj.scf.service.loan.ILoanOrderService;
import com.cwkj.scf.service.pay.IPayService;
import com.cwkj.scf.service.pay.wx.WxDeductForm;
import com.cwkj.scf.service.sms.ISmsService;
import com.cwkj.scf.utils.HuiShangUtil;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;


/**
 * 	支付跟其他服务的接口桥接，避免各个服务相互依赖
 *  controller层都通过 paycommponent 调用不直接引用 payService
 * 	@author harry
 *
 */
@Component("payCommponent")
public class PayCommponent  {
	
	@Autowired
	private IPayService payService;
	@Autowired
    private ILoanOrderService orderService;
	
	@Autowired
	private ParamComponent paramComponent;
	
    
    @Autowired
    private ISmsService smsService;
	
    private final static Logger logger = Logger.getLogger(PayCommponent.class) ;
    
	/**
	 * 	获取支付通道：目前wx 和huishang
	 * 
	 * @return
	 */
	public String getPayChannel() {
		String channel = paramComponent.queryValueByCode("payChanngel");
		Assert.hasText(channel, "没有配置支付通道");
		return channel;
	}

	public BaseResult<Map> prePay(WxDeductForm  deductForm) {
		return payService.prePay(deductForm);
	}

	/**
	 * 	支付回调通知
	 * @param wxResponse wx xml
	 * @param string
	 * @throws Exception 
	 */
	public void wxNotify(String wxResponse) {
		payService.wxNotify(wxResponse);
		// xml转换为map
        Map<String, String> resultMap = null;
		try {
			resultMap = WXPayUtil.xmlToMap(wxResponse);
		} catch (Exception e) {
			 WXPayUtil.getLogger().error("wxnotify: xml to map");
			 throw new BusinessException("wx notify is not well xml");
		}
        
		boolean isSuccess = false;
        if (WXPayConstants.SUCCESS.equalsIgnoreCase(resultMap.get(WXPayConstants.RESULT_CODE))) {
        	isSuccess = true;
            WXPayUtil.getLogger().info("wxnotify: pay success");
//            if (WXPayUtil.isSignatureValid(resultMap, WxConfig.getMchSecret())) {
//
//                // 订单处理 操作 orderconroller 的回写操作
//                WXPayUtil.getLogger().info("wxnotify:微信支付----验证签名成功");
//
//                // 通知微信.异步确认成功.必写.不然会一直通知后台.八次之后就认为交易失败了.
//                resXml = resSuccessXml;
//                isSuccess = true;
//            } else {
//                WXPayUtil.getLogger().error("wxnotify:微信支付----判断签名错误");
//            }
        } else {
            WXPayUtil.getLogger().error("wxnotify: pay error msg is ：" + resultMap.get(WXPayConstants.RESULT_MSG));
            return;
        }

             
        
        // 回调方法，处理业务 - 修改订单状态
        if(isSuccess) {
            String orderCode = resultMap.get("out_trade_no");
            WXPayUtil.getLogger().info("wxnotify:ordercode===>" + orderCode);
            if(StringUtil.isBlank(orderCode)) {
            	WXPayUtil.getLogger().info("wxnotify:not found orderCode===>" + orderCode);
            	throw new BusinessException("not found orderCode");
            }
            String orderStrArr[] = orderCode.split("_");
            if(orderStrArr.length<2) {
            	throw new BusinessException("not well format for orderCode");
            }
            orderCode = orderStrArr[0];
            String payStatus = orderStrArr[1];
            LoanorderExample example = new LoanorderExample();
            example.createCriteria().andOrdercodeEqualTo(orderCode);
			List<Loanorder> orderLst = orderService.selectByExample(example );
			if(null == orderLst || orderLst.size()<1) {
				throw new BusinessException("not found orderCode by:"+orderStrArr[1]);
			}
			Loanorder payOrder = orderLst.get(0);
            int updateResult = orderService.updateOrderStatusByKeyAndOldStatus(payOrder.getLoanid(),payStatus,payOrder.getPrevStatus(payStatus));
            if (updateResult > 0) {
                WXPayUtil.getLogger().info("wxnotify: update order success");
            } else {
                WXPayUtil.getLogger().error("wxnotify:update order error");
            }
        }
	}

	/**
     *	获取支付短信：如果不需要签约需要平台自己发送短信
	 * @param currentUser 
     * @return
     */
    public  BaseResult<Map>  gethspaySmscode(HttpServletRequest request, SysUserDo currentUser) {
    	
    	//开始实名
    	UserRealNameAuthForm unf = buildRealNameForm(request,currentUser);
    	SysUserAuthExample example = new SysUserAuthExample();
    	example.createCriteria().andUseridEqualTo(currentUser.getId())
    							.andIdnoEqualTo(unf.getIDNO())
    							.andRealnameEqualTo(unf.getSURNAME())
    							.andMobileEqualTo(unf.getMOBILE())
    							.andCardnumEqualTo(unf.getCARD_BIND());
    	SysUserAuth  sysUserAuth = orderService.findRealAuth(example);
    	if(sysUserAuth == null || !"T".equals(sysUserAuth.getIscheck())) {
    		BaseResult<Map> ret = payService.realAuth(unf);
    		if(!ret.resultSuccess()) {
    			if(StringUtil.isBlank(ret.getMsg())) {
    				ret.setMsg("实名认证失败，请检查您的银行卡和姓名及手机号是否一致");
    			}
    			return ret;
    		}
			sysUserAuth.setUserid(currentUser.getId());
			sysUserAuth.setIscheck("T");
			sysUserAuth.setLoanId(Long.valueOf(unf.getOrderNo()));
			orderService.updateRealAuthByUserId(sysUserAuth);
    	}
    	//end 实名
    	
    	//开始短信
    	SignConfirmForm signForm = buildSignConfirmForm(request,currentUser); 
    	BaseResult<Map> ret  = payService.getPaySmscode(signForm);
    	if(!ret.resultSuccess()) {
    		if(StringUtil.isBlank(ret.getMsg())) {
				ret.setMsg("发送短信失败，请重新获取短信验证码");
			}
			return ret;
    	}
    	logger.info("huishang send sms return:"+ret.getData());
    	Map<String,String>  smsRetMap = (ret.getData());
		if("N".equals(smsRetMap.get("SIGN_NEEDED"))) {//如果不需要短信，平台发送短信
    		TMessage message = new TMessage();
    		message.setMessageFormatType(MessageFormatType.SIMPLE_ONLY_CODE.name());
			message.setMessageTemplate(MessageTemplate.SMS_159621481.name());
			message.setBusinessType(SMSBusinessType.bindCard.name());
			message.setRecievers(signForm.getPHONE_NO());
			BaseResult result = smsService.send(message);
			if(!ret.resultSuccess()) {
				if(StringUtil.isBlank(ret.getMsg())) {
					ret.setMsg("发送短信失败，请重新获取短信验证码");
				}
				return result;
			}
		}
    	//end 短信
		return ret;
   	
    }
    
    /**
     *	退款或提现
	 * @param currentUser 
     * @return
     */
    public BaseResult<Map> withdraw(HttpServletRequest request,SysUserDo currentUser) {
    	
    	String loanidStr = request.getParameter("loanid");
		Long loanId = Long.valueOf(loanidStr);

		Loanorder order=orderService.selectByPrimaryKey(loanId);
		Assert.notNull(order, "无效订单Id");
		PayInfoBean payInfo = order.calDrawBackAmt();
		Assert.notNull(payInfo.getPayAmt(), "不能重复退款");
		
		
		LoanPersonExample example = new LoanPersonExample();
		example.createCriteria().andLoanidEqualTo(loanId).andUseridEqualTo(order.getUserid());
		List<LoanPerson> personLst = orderService.selectOrderPersonByExample(example );
		Assert.notEmpty(personLst, "找不到退款人信息");
		LoanPerson person = personLst.get(0);
		SysUserAuthExample example1 = new SysUserAuthExample();
    	example1.createCriteria().andUseridEqualTo(order.getUserid().intValue())
    							 .andIdnoEqualTo(person.getIdno())
    							 .andIscheckEqualTo("T");
    	SysUserAuth  sysUserAuth = orderService.findRealAuth(example1);
    	Assert.notNull(sysUserAuth, "找不到退款人信息");
    	
    	WithDrawForm withdrawForm = new WithDrawForm();
    	withdrawForm.setAMOUNT(payInfo.getPayAmt());
    	withdrawForm.setADD_COMMENT(payInfo.getPayName());
    	withdrawForm.setCARD_BIND(sysUserAuth.getCardnum());
    	withdrawForm.setIDNO(sysUserAuth.getIdno());
    	withdrawForm.setMOBILE(sysUserAuth.getMobile());
    	withdrawForm.setSURNAME(sysUserAuth.getRealname());
    	withdrawForm.setBizCode(payInfo.getPayStepCode());
    	withdrawForm.setOrderNo(String.valueOf(order.getLoanid()));
    	withdrawForm.setUserId(order.getUserid().toString());
    	withdrawForm.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
    	withdrawForm.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
    	BaseResult<Map> ret  = payService.withdraw(withdrawForm);
    	//支付成功更新订单状态
    	if(ret.resultSuccess()) {
    		
    		Loanorder drawbackOrder = new Loanorder();
    		drawbackOrder.setLoanid(loanId);
    		drawbackOrder.setLoanstatus(payInfo.getPayStepCode());
    		Orderaudit auditRec = new Orderaudit();
    		auditRec.setAuditusername(currentUser.getUserName());
    		auditRec.setUserid(Long.valueOf(currentUser.getId()));
    		auditRec.setLoanid(loanId);
    		auditRec.setRemark(payInfo.getPayAmt());
    		auditRec.setResultstatus(0);
    		auditRec.setStep(payInfo.getPayStepCode());
    		auditRec.setStepname(payInfo.getPayName());
			
			int updateResult = orderService.platformChangeOrderStatus(drawbackOrder,auditRec );
            if (updateResult > 0) {
                 WXPayUtil.getLogger().info("huishang 退款: update order success");
            } else {
                 WXPayUtil.getLogger().error("huishang 退款成功： but update order error:"+withdrawForm.getOrderNo());
            }
    	}
		return ret;
    	
    }
    
    
	/**
     *	支付页面提交:验证短信，发起支付
	 * @param currentUser 
     * @return
     */
    public BaseResult<Map> submitPay(HttpServletRequest request, SysUserDo currentUser) {
    	
    	BaseResult<Map> retMap = new BaseResult<Map>();
    	String needSign = request.getParameter("needSign");
    	String mobile = request.getParameter("mobile");
    	String smsCode = request.getParameter("smscode");
    	
    	if(StringUtil.isBlank(smsCode)) {
			retMap.setCode(BaseResult.CODE_EXCEPTION);
			retMap.setMsg("短信验证码不能为空");
			return retMap;
		}
		if(StringUtil.isBlank(mobile)) {
			retMap.setCode(BaseResult.CODE_EXCEPTION);
			retMap.setMsg("手机号不能为空");
			return retMap;
		}
		
    	
    	if("Y".equals(needSign)) {
    		SignConfirmForm signForm = buildSignConfirmForm(request,currentUser); 
    		BaseResult<Map> ret =payService.checkSmsCode(signForm);
    		if(!ret.resultSuccess()) {
    			if(StringUtil.isBlank(ret.getMsg())) {
					ret.setMsg("短信验证失败，请重新获取");
				}
    			return ret;
    		}
    	}else {
			boolean result = smsService.checkSms(mobile, SMSBusinessType.bindCard.name(), smsCode);
			if(!result) {
				retMap.setCode(BaseResult.CODE_EXCEPTION);
				retMap.setMsg("无效的短信验证码");
				return retMap;
			}
    	}
    	
    	DeductForm deductForm = buildDeductForm(request,currentUser);
    	BaseResult<Map> ret  = payService.submitPay(deductForm);
    	//支付成功更新订单状态
    	if(ret.resultSuccess()) {
    		String prevStatus = (new Loanorder()).getPrevStatus(deductForm.getBizCode());
    		 int updateResult = orderService.updateOrderStatusByKeyAndOldStatus(Long.valueOf(deductForm.getOrderNo()),
    				 															deductForm.getBizCode(),
    				 															prevStatus);
             if (updateResult > 0) {
                 WXPayUtil.getLogger().info("huishang: update order success"+deductForm.getOrderNo());
             } else {
                 WXPayUtil.getLogger().error("huishang  pay success but update order error:"+deductForm.getOrderNo());
             }
    	}
		return ret;
    	
    }

	private SignConfirmForm buildSignConfirmForm(HttpServletRequest request,SysUserDo currentUser) {
		SignConfirmForm sff = new SignConfirmForm();
		sff.setPHONE_NO(request.getParameter("mobile"));
		sff.setBIND_CARD_NO(request.getParameter("bankcardNo"));
		sff.setID_CODE(request.getParameter("idNo"));
		sff.setNAME(request.getParameter("realname"));
		sff.setSMS_CODE(request.getParameter("smscode"));
		sff.setSIGN_TOKEN(request.getParameter("signToken"));	
		
		sff.setOrderNo(request.getParameter("loanid"));
		sff.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		sff.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		sff.setUserId(String.valueOf(currentUser.getId()));
		return sff;
	}

	private DeductForm buildDeductForm(HttpServletRequest request,SysUserDo currentUser) {
		DeductForm df = new DeductForm();
		df.setIDNO(request.getParameter("idNo"));
		df.setCARD_BIND(request.getParameter("bankcardNo"));
		df.setNAME(request.getParameter("realname"));
		df.setMOBILE(request.getParameter("mobile"));
		df.setBANK_CODE(request.getParameter("bankName"));
		df.setBANK_NAME_CN(request.getParameter("bankName"));
		df.setCALL_BACK_ADDRESS(HuiShangConfig.getDeductCallBackUrl());
		df.setSMS_CODE(request.getParameter("smscode"));
		df.setSMS_SEQ(request.getParameter("smsseq"));
		df.setUSR_IP(IPUtils.getIP(request));
		df.setAMOUNT(request.getParameter("payAmt"));
		
		String loanId = request.getParameter("loanid");
		Loanorder  order = orderService.selectByPrimaryKey(Long.valueOf(loanId));
		PayInfoBean payInfo = order.calPayAmt();
		if("PAY001".equals(payInfo.getPayStepCode())) {
			df.setAMOUNT(payInfo.getPayAmt());
		}
		df.setBizCode(payInfo.getPayStepCode());
		df.setOrderNo(request.getParameter("loanid"));
		df.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		df.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		df.setUserId(String.valueOf(currentUser.getId()));
		return df;
	}
	
    
	private UserRealNameAuthForm buildRealNameForm(HttpServletRequest request,SysUserDo currentUser) {
		UserRealNameAuthForm uaf = new UserRealNameAuthForm();
		uaf.setSURNAME(request.getParameter("realname"));
		uaf.setIDNO(request.getParameter("idNo"));
		uaf.setMOBILE(request.getParameter("mobile"));
		uaf.setCARD_BIND(request.getParameter("bankcardNo"));
		
		uaf.setOrderNo(request.getParameter("loanid"));
		uaf.setTRXDATE(HuiShangUtil.getCurrentTrxDate());
		uaf.setTRXTIME(HuiShangUtil.getCurrentTrxTime());
		uaf.setUserId(String.valueOf(currentUser.getId()));
		return uaf;
	}
	
	/**
	 * 徽商平台账户余额查询
	 * @param form
	 * @return
	 */
	public BaseResult<Map> getPlatformTotalAmt(AccountBalanceForm form) {
		return payService.getPlatformTotalAmt(form);		
	}
	/**
	 * 交易明细查询
	 * @param form
	 * @return
	 */
	public BaseResult<Map> queryTradeDtl(QueryTradeDetailForm form) {
		return payService.queryTradeDtl(form);	
	}

}
