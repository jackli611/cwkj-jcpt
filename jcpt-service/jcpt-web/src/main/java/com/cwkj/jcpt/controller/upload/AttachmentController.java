package com.cwkj.jcpt.controller.upload;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.HttpServletResponseUtil;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysAttachmentDo;
import com.cwkj.jcptsystem.service.system.SysAttachmentService;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.service.config.CommonConfigService;

/**
 * 附件公共类
 * @author ljc
 */
@Controller
public class AttachmentController  extends BaseAction {
    private static Logger logger= LoggerFactory.getLogger(AttachmentController.class);

    @Autowired
    private SysSeqNumberService sysSeqNumberService;
    //@Autowired
    @Autowired
    private SysAttachmentService sysAttachmentService;
    @Autowired
    private CommonConfigService commonConfigService;


    @RequestMapping(value="/uploadAttachment.do",method= RequestMethod.POST)
    public JsonView<String> uploadAttachment_methodPost(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request, String groupType)
    {
        JsonView<String> view=new JsonView<String>();
        try{
            AssertUtils.isNotBlank(groupType, "参数错误：[error:001]");
            AssertUtils.notNull(file,"上传图片文件不能为空");

            SysAttachmentDo attachment=new SysAttachmentDo();
            attachment.setCheckedStatus(SysAttachmentDo.CHECKEDSTATUS_INIT);

           // String storeFileRootPath=paramService.queryValueByCode("StoreFileRootPath");
            String storeFileRootPath=commonConfigService.getUploadFileConfig().getStoreRootPath();
            String path=storeFileRootPath+groupType+"/";
            String saveFileName=sysSeqNumberService.daySeqnumberString("BL");
            int pointIndex=file.getOriginalFilename().lastIndexOf(".");
            String fileType=null;
            if(pointIndex>0)
            {
                saveFileName+=file.getOriginalFilename().substring( pointIndex);
                fileType=file.getOriginalFilename().substring( pointIndex+1);
            }

            File targetFile = new File(path, saveFileName);
            if(!targetFile.exists()){
                targetFile.mkdirs();
            }
            attachment.setFileName(file.getOriginalFilename());
            attachment.setCheckedStatus(SysAttachmentDo.CHECKEDSTATUS_INIT);
            attachment.setFilePath(path+saveFileName);
            attachment.setFileType(fileType);
            attachment.setGroupType(groupType);
            attachment.setCreateUserId(currentUser().getId());
            Integer dbRes=  sysAttachmentService.insertSysAttachment(attachment);
            AssertUtils.isTrue(dbRes!=null && dbRes>0,"保存失败");
            view.setData(attachment.getId().toString());
            //保存
            file.transferTo(targetFile);

            setPromptMessage(view, JsonView.CODE_SUCCESS,"保存成功");

        }catch(BusinessException e)
        {
            setPromptException(view, e);
        }catch (Exception e) {
            logger.error("上传Excel文件异常", e);
            setPromptException(view, e);
        }
        return view;
    }

    @RequestMapping(value="/attachment.do",method=RequestMethod.GET)
    public void attachment_methodGet(HttpServletRequest request, HttpServletResponse response)
    {
        try {
            String idStr=request.getParameter("id");
            Long id=null;
            if(StringUtil.isEmpty(idStr))
            {
                return ;
            }else{
                try{
                    id=Long.valueOf(idStr);
                }catch(Exception ex){
                    logger.error("获取附件异常",ex);
                    return ;
                }
            }
            SysAttachmentDo attachment =sysAttachmentService.findSysAttachmentById(id);
            File file=new File(attachment.getFilePath());
            if(file.exists()){
                if(StringUtil.isNotBlank(attachment.getFileType()) && StringUtil.hasIn(attachment.getFileType().toLowerCase(),"png","jpg","jpeg","gif"))
                {
                    //设置MIME类型
                    response.setHeader("Pragma", "no-cache");
                    response.setHeader("Cache-Control", "no-cache");
                    response.setDateHeader("Expires", 0);
                    response.setContentType("image/"+attachment.getFileType());
                }else {
                    response.setHeader("Content-disposition", "attachment; filename=" + attachment.getFileName());
                }
                HttpServletResponseUtil.responseFile(response,file);
            }
        }catch (Exception e) {
            logger.error("获取附件失败",e);
        }

    }


    /**
     * 对公开放的上传接口
     * @param file
     * @param request
     * @return
     */
    @RequestMapping(value="/uploadAttachmentPublic.do",method= RequestMethod.POST)
    public JsonView<String> uploadAttachmentPublic_methodPost(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request)
    {
        JsonView<String> view=new JsonView<String>();
        try{
            String groupType="public";
            AssertUtils.isNotBlank(groupType, "参数错误：[error:001]");
            AssertUtils.notNull(file,"上传图片文件不能为空");

            SysAttachmentDo attachment=new SysAttachmentDo();
            attachment.setCheckedStatus(SysAttachmentDo.CHECKEDSTATUS_INIT);

            // String storeFileRootPath=paramService.queryValueByCode("StoreFileRootPath");
            String storeFileRootPath=commonConfigService.getUploadFileConfig().getStoreRootPath();
            String path=storeFileRootPath+groupType+"/";
            String saveFileName=sysSeqNumberService.daySeqnumberString("PUB");
            int pointIndex=file.getOriginalFilename().lastIndexOf(".");
            String fileType=null;
            if(pointIndex>0)
            {
                saveFileName+=file.getOriginalFilename().substring( pointIndex);
                fileType=file.getOriginalFilename().substring( pointIndex+1);
            }

            File targetFile = new File(path, saveFileName);
            if(!targetFile.exists()){
                targetFile.mkdirs();
            }
            attachment.setFileName(file.getOriginalFilename());
            attachment.setCheckedStatus(SysAttachmentDo.CHECKEDSTATUS_INIT);
            attachment.setFilePath(path+saveFileName);
            attachment.setFileType(fileType);
            attachment.setGroupType(groupType);
            attachment.setCreateUserId(currentUser().getId());
            Integer dbRes=  sysAttachmentService.insertSysAttachment(attachment);
            AssertUtils.isTrue(dbRes!=null && dbRes>0,"保存失败");
            view.setData(attachment.getId().toString());
            //保存
            file.transferTo(targetFile);

            setPromptMessage(view, JsonView.CODE_SUCCESS,"保存成功");

        }catch(BusinessException e)
        {
            setPromptException(view, e);
        }catch (Exception e) {
            logger.error("上传Excel文件异常", e);
            setPromptException(view, e);
        }
        return view;
    }

    /**
     * 对外开放的接口：获取对公开放上传的附件
     * @param request
     * @param response
     */
    @RequestMapping(value="/attachmentPublic.do",method=RequestMethod.GET)
    public void publicAttachment_methodGet(HttpServletRequest request, HttpServletResponse response)
    {
        try {
            String idStr=request.getParameter("id");
            Long id=null;
            if(StringUtil.isEmpty(idStr))
            {
                return ;
            }else{
                try{
                    id=Long.valueOf(idStr);
                }catch(Exception ex){
                    logger.error("获取附件异常",ex);
                    return ;
                }
            }
            SysAttachmentDo attachment =sysAttachmentService.findSysAttachmentById(id);
            if(attachment!=null && "public".equals(attachment.getGroupType())) {
                File file = new File(attachment.getFilePath());
                if (file.exists()) {
                    if(StringUtil.isNotBlank(attachment.getFileType()) && StringUtil.hasIn(attachment.getFileType().toLowerCase(),"png","jpg","jpeg","gif"))
                    {
                        //设置MIME类型
                        response.setHeader("Pragma", "no-cache");
                        response.setHeader("Cache-Control", "no-cache");
                        response.setDateHeader("Expires", 0);
                        response.setContentType("image/" + attachment.getFileType());
                    } else {
                        response.setHeader("Content-disposition", "attachment; filename=" + attachment.getFileName());
                    }
                    HttpServletResponseUtil.responseFile(response, file);
                }
            }
        }catch (Exception e) {
            logger.error("获取附件失败",e);
        }

    }


}
