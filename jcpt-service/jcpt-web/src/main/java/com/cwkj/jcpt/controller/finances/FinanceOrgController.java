package com.cwkj.jcpt.controller.finances;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.scf.model.finances.FinanceOrgDo;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.service.finances.FinanceOrgService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;

/**
 * 金融机构.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/finances/")
public class FinanceOrgController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(FinanceOrgController.class);
	
	private FinanceOrgService financeOrgService;
	
	@Autowired
	public void setFinanceOrgService(FinanceOrgService financeOrgService)
	{
		this.financeOrgService=financeOrgService;
	}
	
	/**
	 * 金融机构数据分页
	 * @return
	 */
	@RequestMapping(value="/financeOrgList.do",method=RequestMethod.GET)
	public ModelAndView financeOrgList_methodGet()
	{
		ModelAndView view=new ModelAndView("finances/financeOrgList");
		return view;
	}
	
	/**
	 * 金融机构数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/financeOrgList.do",method=RequestMethod.POST)
	public ModelAndView financeOrgList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("finances/financeOrgList_table");
		try{ 
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<FinanceOrgDo> pagedata= financeOrgService.queryFinanceOrgListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取金融机构数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增金融机构
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addFinanceOrg.do",method=RequestMethod.GET)
	public ModelAndView addFinanceOrg_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("finances/addFinanceOrg");
		try{
			view.addObject("financeOrg", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加金融机构异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增金融机构
	 * @param request
	 * @param financeOrg
	 * @return
	 */
	@RequestMapping(value="/addFinanceOrg.do",method=RequestMethod.POST)
	public ModelAndView addFinanceOrg_methodPost(HttpServletRequest request,FinanceOrgDo financeOrg)
	{
		ModelAndView view=new ModelAndView("finances/addFinanceOrg");
		try{
			
			 checkPermission(PermissionCode.FINANCEORG_ADD);
			AssertUtils.isNotBlank(financeOrg.getNames(), "机构名称不能为空");
			financeOrg.setRecordUserId(currentUser().getId());
			financeOrg.setStatus(FinanceOrgDo.STATUS_VALID);
			financeOrg.setRecordUserId(currentUser().getId());
			Integer result= financeOrgService.insertFinanceOrg(financeOrg);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加金融机构异常（POST）",e);
		}finally {
			view.addObject("financeOrg", financeOrg);
		}
		return view;
	}
	
	 /**
	 * 浏览金融机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewFinanceOrg.do",method=RequestMethod.GET)
	public ModelAndView viewFinanceOrg_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("finances/viewFinanceOrg");
		try{
			FinanceOrgDo financeOrg= financeOrgService.findFinanceOrgById(id);
			view.addObject("financeOrg", financeOrg);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览金融机构异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑金融机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editFinanceOrg.do",method=RequestMethod.GET)
	public ModelAndView editFinanceOrg_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("finances/addFinanceOrg");
		try{
			FinanceOrgDo financeOrg= financeOrgService.findFinanceOrgById(id);
			view.addObject("financeOrg", financeOrg);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑金融机构异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑金融机构
	 * @param request
	 * @param financeOrg
	 * @return
	 */
	@RequestMapping(value="/editFinanceOrg.do",method=RequestMethod.POST)
	public ModelAndView editFinanceOrg_methodPost(HttpServletRequest request,FinanceOrgDo  financeOrg)
	{
		ModelAndView view =new ModelAndView("finances/addFinanceOrg");
		try{
			AssertUtils.isNotBlank(financeOrg.getId(), "参数无效");
			Integer result= financeOrgService.updateFinanceNameInfo(financeOrg);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑金融机构异常（GET）",e);
		}finally {
			view.addObject("financeOrg", financeOrg);
		}
		return view;
	}
	
	/**
	 * 删除金融机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deleteFinanceOrg.do")
	public JsonView<String> deleteFinanceOrg_methodPost(HttpServletRequest request,String id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.isNotBlank(id, "参数无效");
			Integer result=financeOrgService.deleteFinanceOrgById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除金融机构异常",e);
		}
		return view;
	}
	
	/**
	 * 删除金融机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/financeOrgStatus.do")
	public JsonView<String> financeOrgStatus_methodPost(HttpServletRequest request,String id,Integer status)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.isNotBlank(id, "参数无效[error:001]");
			AssertUtils.notNull(status, "参数无效[error:002]");
			Integer result=financeOrgService.updateStatus(id, status);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("修改合作状态金融机构异常",e);
		}
		return view;
	}
	 
}
