package com.cwkj.jcpt.controller.loanorder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.DictSystemInfoDo;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.DictSystemInfoService;
import com.cwkj.scf.model.finances.FinanceOrgDo;
import com.cwkj.scf.model.loan.Attachment;
import com.cwkj.scf.model.loan.LoanOrderStatus;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.finances.FinanceOrgService;
import com.cwkj.scf.service.loan.ILoanOrderService;
import com.cwkj.scf.service.property.PropertyOrgService;
import com.cwkj.scf.service.property.SupplierService;

/**
 * 借款订单申请
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/loanorder/")
public class LoanOrderController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(LoanOrderController.class);
	
	@Autowired
	private ILoanOrderService loanOrderSercice;
	
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private PropertyOrgService propertyOrgService;
	
	@Autowired
	private FinanceOrgService financeOrgService;
	
	//数据字典
	@Autowired
	private DictSystemInfoService dictSystemInfoService;
	
	
	/**
	 * 	查询订单用来编辑
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/toEdit.do")
	public ModelAndView toEdit(HttpServletRequest request,Long loanid)
	{
		ModelAndView view=  new ModelAndView("loadmodel/addOrderStep1");
		
		 if(this.hasRole(SystemConst.SYS_ROLE_PROPERTYS_NAME) && !this.hasRole(SystemConst.SYS_ROLE_SUPPLIER_NAME) && !this.hasRole(SystemConst.SYS_ROLE_FINANCES_NAME)) {
				return  new ModelAndView("platform/main_refuse");
		 }
		 
		try{
			view.addObject("loanid",loanid);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	
	
	/**
	 * 	订单详情
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewOrder.do")
	public ModelAndView viewOrder(HttpServletRequest request,Long loanid){
		ModelAndView view=  new ModelAndView("loanorder/viewOrder");
		try{
			String prodCode = (String)request.getSession().getAttribute("navigateProductCode");
			if(prodCode.equals(ProductEnum.PROD_SCF.getProductCode())) {
				return viewScfOrder(request,loanid,prodCode);
			}
			if(prodCode.equals(ProductEnum.PROD_PERSONAL_XY.getProductCode())) {
				return viewPersonOrder(request,loanid,prodCode);
			}
			
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	
	/**
	 * 	订单详情
	 * @param request
	 * @param id
	 * @return
	 */
	private ModelAndView viewScfOrder(HttpServletRequest request,Long loanid,String productCode){
		ModelAndView view=  new ModelAndView("loanorder/viewOrder");
		try{
			view.addObject("loanid",loanid);
			if(loanid != null) {
				Loanorder order = loanOrderSercice.selectOrderAndAttachmentByPrimaryKey(loanid );
				if(null != order) {
					List<Attachment>  attcLst = order.getAttachments();
					view.addObject("attachments", attcLst);
					view.addObject("order",order);
					//查找核心企业
					if(null != order.getPropertyid()) {
						PropertyOrgDo propertyDo = propertyOrgService.findPropertyOrgById(order.getPropertyid());
						view.addObject("propertyDo", propertyDo);
					}
					//查找供应商
					if(null != order.getSupperid()) {
						SupplierDo supplierDo = supplierService.findSupplierById(order.getSupperid());
						view.addObject("supplierDo", supplierDo);
					}
					
					//查找资金方
					if(null != order.getFinorgid()) {
						FinanceOrgDo financeOrgDo = financeOrgService.findFinanceOrgById(order.getFinorgid());
						view.addObject("financeOrgDo", financeOrgDo);
					}
					
					//查询数据库
					DictSystemInfoDo repayNameDict = dictSystemInfoService.queryCodeName("repayCode",String.valueOf(order.getRepaytype()));
					if(null != repayNameDict) {
						view.addObject("repayName", repayNameDict.getCodeName());
					}
				}
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	
	/**
	 * 	个贷订单详情
	 * @param request
	 * @param id
	 * @return
	 */
	private ModelAndView viewPersonOrder(HttpServletRequest request,Long loanid,String productCode){
		String viewName = "loanorder/viewOrder_"+productCode;
		ModelAndView view=  new ModelAndView(viewName);
		try{
			view.addObject("loanid",loanid);
			if(loanid != null) {
				Loanorder order = loanOrderSercice.selectOrderAndAttachmentByPrimaryKey(loanid );
				if(null != order) {
					List<Attachment>  attcLst = order.getAttachments();
					view.addObject("attachments", attcLst);
					view.addObject("order",order);
					//查找核心企业
					if(null != order.getPropertyid()) {
						PropertyOrgDo propertyDo = propertyOrgService.findPropertyOrgById(order.getPropertyid());
						view.addObject("propertyDo", propertyDo);
					}
					//查找供应商
					if(null != order.getSupperid()) {
						SupplierDo supplierDo = supplierService.findSupplierById(order.getSupperid());
						view.addObject("supplierDo", supplierDo);
					}
					
					//查找资金方
					if(null != order.getFinorgid()) {
						FinanceOrgDo financeOrgDo = financeOrgService.findFinanceOrgById(order.getFinorgid());
						view.addObject("financeOrgDo", financeOrgDo);
					}
					
					//查询数据库
					DictSystemInfoDo repayNameDict = dictSystemInfoService.queryCodeName("repayCode",String.valueOf(order.getRepaytype()));
					if(null != repayNameDict) {
						view.addObject("repayName", repayNameDict.getCodeName());
					}
				}
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	
	/**
	 * 	查询订单用来编辑
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getOrder.do")
	public JsonView<Loanorder> getOrder(HttpServletRequest request,Long loanid)
	{
		JsonView<Loanorder> view=  JsonView.successJsonView();
		try{
			
			SysUserDo currentUser  = this.currentUser();
			 
			if(loanid != null) {
				Loanorder order = loanOrderSercice.selectOrderAndAttachmentByPrimaryKey(loanid );
				if(null != order) {
					view.setData(order);
				}
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	
	
	
	/**
	 * 	保存订单
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/saveOrder.do")
	public JsonView<String> saveOrder(HttpServletRequest request,Loanorder order)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			SysUserDo currentUser  = this.currentUser();
			order.setUserid(Long.valueOf(currentUser.getId()));
			if(StringUtil.isBlank(order.getProdcode())){
				order.setProdcode(ProductEnum.PROD_SCF.getProductCode());
			}
			
			//对公借款
			order.setOrdertype(Loanorder.publicOrder);
			//修改后重新编辑
			if(LoanOrderStatus.NOPASS.getStatusCode().equals(order.getLoanstatus())) {
				order.setLoanstatus(LoanOrderStatus.PENDING.getStatusCode());
			}
			
			Integer result= loanOrderSercice.updateOrder(order);
			AssertUtils.isTrue(result.equals(1),"保存出错");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			
			setPromptException(view, e);
			logger.error("保存订单申请第一步异常",e);
		}
		return view;
	}
	
	
	
	
	/**
	 * 	查看借款协议模板
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/showProtocolTemplate.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView showProtocolTemplate(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("loanorder/protocol_template");
		try{ 
			
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取查看借款协议模板异常", e);
		}
		return view;
	}
	
	
	/**
	 * 借款订单数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/orderList.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView orderList(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate){
		
		String pcode = (String)request.getSession().getAttribute("navigateProductCode");
		if(ProductEnum.PROD_SCF.getProductCode().equals(pcode)) {
			return scfOrderList(request,pageIndex,pageSize,startDate,endDate);
		}else if(ProductEnum.PROD_PERSONAL_XY.getProductCode().equals(pcode)) {
			return personOrderList(request,pageIndex,pageSize,startDate,endDate);		
		}
		return null;
		
	}
	
	/**
	 * 借款订单数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	public ModelAndView personOrderList(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		
		String pcode = (String)request.getSession().getAttribute("navigateProductCode");
		
		String viewName="loanorder/orderList_table_" + pcode;
		
		ModelAndView view=new ModelAndView(viewName);
		
		try{
			Map<String, Object> selectItem=new HashMap<String, Object>();
			
			String id= request.getParameter("id");
			view.addObject("id",id);
			
			String orgType= request.getParameter("orgType");
			view.addObject("orgType",orgType);
			
			 String loanStatus = request.getParameter("queryLoanstatus");
			 if(StringUtil.isNotBlank(loanStatus)) {
				 selectItem.put("loanstatus", loanStatus);
			 }
			 
			 if("1".equals(orgType)) {
				 selectItem.put("propertyid", id);
			 }else if("2".equals(orgType)) {
				 selectItem.put("finorgid", id);
			 }
			 
			 
			 String names = request.getParameter("names");
			 if(StringUtil.isNotBlank(names)) {
				 selectItem.put("suppliername", names);
			 }
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 selectItem.put("prodcode", pcode);
			 
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 PageDo<Loanorder> pagedata= loanOrderSercice.queryPersonalOrderListPage(pageIndex,pageSize, selectItem);
			 view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取借款订单数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 借款订单数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	private ModelAndView scfOrderList(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("loanorder/orderList_table");
		
		try{
			String id= request.getParameter("id");
			view.addObject("id",id);
			
			String orgType= request.getParameter("orgType");
			view.addObject("orgType",orgType);
			
			Map<String, Object> selectItem=new HashMap<String, Object>();
			 String loanStatus = request.getParameter("queryLoanstatus");
			 if(StringUtil.isNotBlank(loanStatus)) {
				 selectItem.put("loanstatus", loanStatus);
			 }
			 
			 if("1".equals(orgType)) {
				 selectItem.put("propertyid", id);
			 }else if("2".equals(orgType)) {
				 selectItem.put("finorgid", id);
			 }
			 
			 String names = request.getParameter("names");
			 if(StringUtil.isNotBlank(names)) {
				 selectItem.put("suppliername", names);
			 }
			 selectItem.put("prodcode", ProductEnum.PROD_SCF.getProductCode());
			 
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 PageDo<Loanorder> pagedata= loanOrderSercice.queryOrderListPage(pageIndex,pageSize, selectItem);
			 view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取借款订单数据分页异常", e);
		}
		return view;
	}
	
	
	/**
	 * 跳转到订单列表页面
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/toOrderListPage.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView toOrderListPage(HttpServletRequest request)
	{
		String pcode = (String)request.getParameter("navigateProductCode");
		
		if(pcode == null) {
			pcode = (String)request.getSession().getAttribute("navigateProductCode");
		}else {
			request.getSession().setAttribute("navigateProductCode", pcode);
		}
		
		if(pcode == null) {
			pcode = ProductEnum.PROD_PERSONAL_XY.getProductCode();
		}
		if(ProductEnum.PROD_SCF.getProductCode().equals(pcode)) {
			return toSCFOrderListPage(request);
		}else if(ProductEnum.PROD_PERSONAL_XY.getProductCode().equals(pcode)) {
			return toPersonOrderListPage(request);			
		}
		return null;
	}
	
	
	/**
	 * 跳转到订单列表页面
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	private ModelAndView toSCFOrderListPage(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("loanorder/loan_order_list");
		try{ 
			String propertyId = request.getParameter("id");
			view.addObject("id", propertyId);
			String orgType = request.getParameter("orgType");
			view.addObject("orgType", orgType);
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("跳转到订单列表页面异常", e);
		}
		return view;
	}
	/**
	 * 跳转到订单列表页面
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	private ModelAndView toPersonOrderListPage(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("loanorder/loan_order_list");
		try{ 
			String propertyId = request.getParameter("id");
			if(StringUtil.isNotBlank(propertyId)) {
				view.addObject("id", propertyId);
			}
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("跳转到订单列表页面异常", e);
		}
		return view;
	}
	
	 
}
