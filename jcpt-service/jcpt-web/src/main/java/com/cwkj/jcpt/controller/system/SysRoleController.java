package com.cwkj.jcpt.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysRoleDo;
import com.cwkj.jcptsystem.service.system.SysPermissionService;
import com.cwkj.jcptsystem.service.system.SysRoleService;

/**
 * 角色
 * @author ljc
 * @version 1.0
 */
@Controller
@RequestMapping(value="/system/")
public class SysRoleController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(SysRoleController.class);
	
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysPermissionService sysPermissionService;
	
	/**
	 * 角色列表（全部数据）
	 * @return
	 */
	@RequestMapping(value="/roleListAll.do",method=RequestMethod.GET)
	public ModelAndView roleListAll(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("system/roleListAll");
		try{
			checkPermission(PermissionCode.system_role_list);
			Map<String, Object> selectItem=new HashMap<String, Object>();
			List<SysRoleDo> datalist=	sysRoleService.querySysRoleList(selectItem);
			view.addObject("datalist", datalist);
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取角色列表异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/roleListAll.do",method=RequestMethod.POST)
	public ModelAndView roleListAll_MethodPOST(HttpServletRequest request,String name,Integer available,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("system/roleListAll_table");
		try{
			checkPermission(PermissionCode.system_role_list);
			Map<String, Object> selectItem=new HashMap<String, Object>();
			setDateBetweemToMap(selectItem, startDate, endDate);
			selectItem.put("name", name);
			selectItem.put("available", available);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<SysRoleDo> pagedata= sysRoleService.querySysRoleListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取角色列表异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/editRole.do",method=RequestMethod.GET)
	public ModelAndView editRole_method_get(String id)
	{
		ModelAndView view=new ModelAndView("system/editRole");
		try{
			checkPermission(PermissionCode.system_role_edit);
			SysRoleDo sysRole=null;
			 if(StringUtil.isNotEmpty(id))
			 {
				 sysRole=sysRoleService.findSysRoleById(id);
			 }
			 view.addObject("sysRole", sysRole);
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("新增角色初始页面异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/editRole.do",method=RequestMethod.POST)
	public ModelAndView editRole_method_post(SysRoleDo sysRole)
	{
		ModelAndView view=new ModelAndView("system/editRole");
		try{
			checkEmpty(sysRole.getName(), "角色名称不能为空");
			checkEmpty(sysRole.getId(), "参数错误");
			if(sysRole.getAvailable()==null)
			{
				sysRole.setAvailable(0);
			}
			if(StringUtil.isNotEmpty(sysRole.getId()))
			{
				//检查权限
				checkPermission(PermissionCode.system_role_edit);
				sysRoleService.updateSysRoleById(sysRole);
			} 
			setPromptMessage(view, "操作成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("新增角色异常", e);
		}finally {
			view.addObject("sysRole", sysRole);
		}
		return view;
	}
	
	@RequestMapping(value="/addRole.do",method=RequestMethod.GET)
	public ModelAndView addRole_method_get(String id)
	{
		ModelAndView view=new ModelAndView("system/addRole");
		try{
			checkPermission(PermissionCode.system_role_edit);
			SysRoleDo sysRole=null;
			 if(StringUtil.isNotEmpty(id))
			 {
				 sysRole=sysRoleService.findSysRoleById(id);
			 }
			 view.addObject("sysRole", sysRole);
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("新增角色初始页面异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/addRole.do",method=RequestMethod.POST)
	public ModelAndView addRole_method_post(SysRoleDo sysRole)
	{
		ModelAndView view=new ModelAndView("system/addRole");
		try{
			checkEmpty(sysRole.getName(), "角色名称不能为空");
			if(sysRole.getAvailable()==null)
			{
				sysRole.setAvailable(0);
			}
			if(StringUtil.isNotEmpty(sysRole.getId()))
			{
				//检查权限
				checkPermission(PermissionCode.system_role_edit);
				sysRoleService.updateSysRoleById(sysRole);
			}else{
				//检查权限
				checkPermission(PermissionCode.system_role_add);
				sysRoleService.insertSysRole(sysRole);
			}
			view.addObject("sysRole", sysRole);
			setPromptMessage(view, "操作成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("新增角色异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/delRole.do",method=RequestMethod.POST)
	public JsonView<String> deleteRole_post(String id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			//权限检验
			checkPermission(PermissionCode.system_role_delete);
			checkEmpty(id, "参数错误");
			 
			if(StringUtil.isNotEmpty(id))
			{
				sysRoleService.deleteSysRoleById(id);
			}
			setPromptMessage(view,view.CODE_SUCCESS, "操作成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除角色异常", e);
		}
		return view;
		
	}
	
	@RequestMapping(value="/roleListWithUserId.do",method=RequestMethod.GET)
	public ModelAndView roleListWithUserId(Integer userId)
	{
		ModelAndView view =new ModelAndView("system/roleListWithUserId");
		try{
			List<SysRoleDo> roleList= sysRoleService.getRoleListWithUserId(userId);
			view.addObject("roleList", roleList);
			view.addObject("userId", userId);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("根据用户ID获取所有角色列表异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/permissionMap.do",method=RequestMethod.GET)
	public ModelAndView permissionMap(HttpServletRequest request,String roleId)
	{
		ModelAndView view=new ModelAndView("system/permissionMap");
		try{
//			PermissionNodeHandler permissionNodeHandler=new PermissionNodeHandler();
//			permissionNodeHandler.setPermissionList( sysRoleService.queryPermissionByRoleId(roleId));
//			sysPermissionService.findChildMenuList(permissionNodeHandler, null);
			if(SystemConst.PLATFORM_DEPOSITORY== currentPlatformId())
			{
				view.addObject("permissionNodeList", sysPermissionService.findPermissionNodeByUser(roleId, currentUser()));
			}else {
				view.addObject("permissionNodeList", sysPermissionService.findPermissionNode(roleId));
			}
			
			view.addObject("roleId",roleId);
		}catch (Exception e) {
			logger.error("获取权限地图异常",e);
			setPromptException(view, e);
		}
		return view;
	}
	
	@RequestMapping(value="/permissionMap.do",method=RequestMethod.POST)
	public ModelAndView rolePermissionMap_methodPost(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("system/permissionMap");
		try{
			 String roleId=request.getParameter("roleId");
			logger.info("roleId:{}",roleId);
			String[] permissionIds=request.getParameterValues("permissionIds");
			Long[] permissionIdArr=null;
			if(permissionIds!=null && permissionIds.length>0)
			{
				 permissionIdArr=new Long[permissionIds.length];
				for(int i=0,k=permissionIds.length;i<k;i++)
				{
					 String permissionId=permissionIds[i];
					permissionIdArr[i]=Long.valueOf(permissionId);
					logger.info("permissionId:{}",permissionId);
				}
			}
			sysRoleService.saveRolePermissions(roleId, permissionIdArr);
			view.addObject("roleId",roleId);
			
//			PermissionNodeHandler permissionNodeHandler=new PermissionNodeHandler();
//			permissionNodeHandler.setPermissionList( sysRoleService.queryPermissionByRoleId(roleId));
//			sysPermissionService.findChildMenuList(permissionNodeHandler, null);
			if(SystemConst.PLATFORM_DEPOSITORY== currentPlatformId())
			{
				view.addObject("permissionNodeList", sysPermissionService.findPermissionNodeByUser(roleId, currentUser()));
			}else {
				view.addObject("permissionNodeList", sysPermissionService.findPermissionNode(roleId));
			}
//			view.addObject("permissionNodeList", sysPermissionService.findPermissionNode(roleId));
			setPromptMessage(view, "操作成功");
		}catch (Exception e) {
			logger.error("角色权限关系保存异常", e);
		}
		return view;
	}
	
}
