package com.cwkj.jcpt.controller.products;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.service.products.ProductService;

/**
 * 	产品分配
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class ProductAssignController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(ProductAssignController.class);
	
	private ProductService productService;
	
	@Autowired
	public void setProductService(ProductService productService)
	{
		this.productService=productService;
	}
	
	
	/**
	 * 	企业分配的产品
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/companyProductList.do",method=RequestMethod.POST)
	public ModelAndView companyProductList(HttpServletRequest request,Long pageIndex,Integer pageSize)
	{
		ModelAndView view=new ModelAndView("property/companyProductList_table");
		try{ 
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 String propertyOrgId=request.getParameter("propertyOrgId");			
			 if(StringUtil.isNotBlank(propertyOrgId))
			 {
				 selectItem.put("propertyOrgId", propertyOrgId);
			 }

			PageDo<PropertyorgProduct> pagedata= productService.queryCompanyProductListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取企业分配的产品分页异常", e);
		}
		return view;
	}
	
	
	/**
	 * 	为核心企业分配产品
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/toUpdateCompanyProduct.do",method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView toUpdateCompanyProduct(HttpServletRequest request,String propertyOrgId,Integer assignId )
	{
		ModelAndView view=new ModelAndView("property/asignCompanyProduct");
		PropertyorgProduct assignProduct  = null; 
		try{
			if(null != assignId) {
				assignProduct = productService.findAssignProductById(assignId);
			}
			if(assignProduct != null &&  !assignProduct.getPropertyorgid().equals(propertyOrgId)) {
				throw new BusinessException("你不能编辑当前的产品");
			}
			
			if(assignProduct == null) {
				assignProduct  = new PropertyorgProduct();
				assignProduct.setAssignid(assignId);
				assignProduct.setPropertyorgid(propertyOrgId);
			}
		
			view.addObject("companyProduct", assignProduct);
			
			//产品列表
			Map<String, Object> selectItem = new HashMap<String,Object>();
			selectItem.put("levels", 1);
			selectItem.put("productCode", ProductEnum.PROD_SCF.getProductCode());
			List<ProductDo> productLst = productService.queryProductList(selectItem);
			view.addObject("productLst",productLst);
			
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("为核心企业分配产品",e);
		}
		return view;
	}
	
	/**
	 * 	为核心企业分配产品
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/saveCompanyProduct.do",method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView saveCompanyProduct(HttpServletRequest request,PropertyorgProduct assignProduct)
	{
		ModelAndView view=new ModelAndView("property/asignCompanyProduct");
		try{
			productService.insertAssignProduct(assignProduct);
			view.addObject("assignProduct", assignProduct);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("保存核心企业分配产品",e);
		}
		return view;
	}
	/**
	 * 	为核心企业分配产品
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/saveCompanyProductByAjax.do",method={RequestMethod.GET,RequestMethod.POST})
	public JsonView<PropertyorgProduct> saveCompanyProductByAjax(HttpServletRequest request,PropertyorgProduct assignProduct)
	{
		JsonView<PropertyorgProduct> view= JsonView.successJsonView("保存成功");
				
		try{
			if(assignProduct.getAssignid() != null) {
				productService.updateAssignProduct(assignProduct);
			}else {
				
				productService.insertAssignProduct(assignProduct);
			}
			
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("保存核心企业分配产品",e);
		}
		return view;
	}
	 
}
