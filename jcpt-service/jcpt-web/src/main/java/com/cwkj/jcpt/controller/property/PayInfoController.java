package com.cwkj.jcpt.controller.property;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.scf.model.property.PayInfoDo;
import com.cwkj.scf.service.property.PayInfoService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;

/**
 * 支付信息.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class PayInfoController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(PayInfoController.class);
	
	private PayInfoService payInfoService;
	
	@Autowired
	public void setPayInfoService(PayInfoService payInfoService)
	{
		this.payInfoService=payInfoService;
	}
	
	/**
	 * 支付信息数据分页
	 * @return
	 */
	@RequestMapping(value="/payInfoList.do",method=RequestMethod.GET)
	public ModelAndView payInfoList_methodGet()
	{
		ModelAndView view=new ModelAndView("property/payInfoList");
		return view;
	}
	
	/**
	 * 支付信息数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/payInfoList.do",method=RequestMethod.POST)
	public ModelAndView payInfoList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/payInfoList_table");
		try{ 
			checkPermission(PermissionCode.PAYINFO_LIST_QUERY);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 
			 String batchId=request.getParameter("batchId");
			 String payNo=request.getParameter("payNo");
			 String payStatusStr=request.getParameter("payStatusStr");
			 String companyId=request.getParameter("companyId");
			 String supplierId=request.getParameter("supplierId");
			 if(StringUtil.isNotBlank(batchId))
			 {
				 selectItem.put("batchId", batchId);
			 }
			 if(StringUtil.isNotBlank(payNo))
			 {
				 selectItem.put("payNo", payNo);
			 }
			 if(StringUtil.isNotBlank(payStatusStr))
			 {
				 selectItem.put("payStatus", Integer.valueOf(payStatusStr));
			 }
			 if(StringUtil.isNotBlank(companyId))
			 {
				 selectItem.put("companyId", companyId);
			 }
			 if(StringUtil.isNotBlank(supplierId))
			 {
				 selectItem.put("supplierId", supplierId);
			 }
			 
			PageDo<PayInfoDo> pagedata= payInfoService.queryPayInfoListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取支付信息数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增支付信息
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addPayInfo.do",method=RequestMethod.GET)
	public ModelAndView addPayInfo_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("property/addPayInfo");
		try{
			checkPermission(PermissionCode.PAYINFO_ADD);
			view.addObject("payInfo", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加支付信息异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增支付信息
	 * @param request
	 * @param payInfo
	 * @return
	 */
	@RequestMapping(value="/addPayInfo.do",method=RequestMethod.POST)
	public ModelAndView addPayInfo_methodPost(HttpServletRequest request,PayInfoDo payInfo)
	{
		ModelAndView view=new ModelAndView("property/addPayInfo");
		try{
			checkPermission(PermissionCode.PAYINFO_ADD);
			 String payDateStr=request.getParameter("payDateStr");
			 String amountStr=request.getParameter("amountStr");
			 String payStatusStr=request.getParameter("payStatusStr");
			 String contractDateStr=request.getParameter("contractDateStr");
			 String contractAmountStr=request.getParameter("contractAmountStr");
			 if(StringUtil.isNotBlank(payDateStr))
			 {
				 payInfo.setPayDate(DateUtils.parse(payDateStr));
			 }
			 if(StringUtil.isNotBlank(amountStr))
			 {
				 payInfo.setAmount(new BigDecimal(amountStr));
			 }
			 if(StringUtil.isNotBlank(payStatusStr))
			 {
				 payInfo.setPayStatus(Integer.valueOf(payStatusStr));
			 }
			 if(StringUtil.isNotBlank(contractDateStr))
			 {
				 payInfo.setContractDate(DateUtils.parse(contractDateStr));
			 }
			 if(StringUtil.isNotBlank(contractAmountStr))
			 {
				 payInfo.setContractAmount(new BigDecimal(contractAmountStr));
			 }
			 payInfo.setRecordUserId(currentUser().getId());
			Integer result= payInfoService.insertPayInfo(payInfo);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加支付信息异常（POST）",e);
		}finally {
			view.addObject("payInfo", payInfo);
		}
		return view;
	}
	
	 /**
	 * 浏览支付信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewPayInfo.do",method=RequestMethod.GET)
	public ModelAndView viewPayInfo_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/viewPayInfo");
		try{
			checkPermission(PermissionCode.PAYINFO_VIEW);
			PayInfoDo payInfo= payInfoService.findPayInfoById(id);
			view.addObject("payInfo", payInfo);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览支付信息异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑支付信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editPayInfo.do",method=RequestMethod.GET)
	public ModelAndView editPayInfo_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/addPayInfo");
		try{
			checkPermission(PermissionCode.PAYINFO_EDIT);
			PayInfoDo payInfo= payInfoService.findPayInfoById(id);
			view.addObject("payInfo", payInfo);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑支付信息异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑支付信息
	 * @param request
	 * @param payInfo
	 * @return
	 */
	@RequestMapping(value="/editPayInfo.do",method=RequestMethod.POST)
	public ModelAndView editPayInfo_methodPost(HttpServletRequest request,PayInfoDo  payInfo)
	{
		ModelAndView view =new ModelAndView("property/addPayInfo");
		try{
			checkPermission(PermissionCode.PAYINFO_EDIT);
			AssertUtils.isNotBlank(payInfo.getId(), "参数无效");
			 payInfo.setRecordUserId(currentUser().getId());
			Integer result= payInfoService.updatePayInfoById(payInfo);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑支付信息异常（GET）",e);
		}finally {
			view.addObject("payInfo", payInfo);
		}
		return view;
	}
	
	/**
	 * 删除支付信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deletePayInfo.do")
	public JsonView<String> deletePayInfo_methodPost(HttpServletRequest request,String id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.PAYINFO_DELETE);
			AssertUtils.isNotBlank(id, "参数无效");
			Integer result=payInfoService.deletePayInfoById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除支付信息异常",e);
		}
		return view;
	}
	
	

	/**
	 * 支付信息数据分页
	 * @return
	 */
	@RequestMapping(value="/companyPayInfoList.do",method=RequestMethod.GET)
	public ModelAndView companyPayInfoList_methodGet(String companyId)
	{
		ModelAndView view=new ModelAndView("property/companyPayInfoList");
		try {
			view.addObject("companyId", companyId);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑支付信息异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 支付信息数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/companyPayInfoList.do",method=RequestMethod.POST)
	public ModelAndView companyPayInfoList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/companyPayInfoList_table");
		try{ 
			checkPermission(PermissionCode.PAYINFO_LIST_QUERY);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 String batchId=request.getParameter("batchId");
			 String payNo=request.getParameter("payNo");
			 String payStatusStr=request.getParameter("payStatusStr");
			 String companyId=request.getParameter("companyId");
			 String supplierId=request.getParameter("supplierId");
			 AssertUtils.isNotBlank(companyId, "请求参数错误");
			 selectItem.put("companyId", companyId);
			 
			 if(StringUtil.isNotBlank(batchId))
			 {
				 selectItem.put("batchId", batchId);
			 }
			 if(StringUtil.isNotBlank(payNo))
			 {
				 selectItem.put("payNo", payNo);
			 }
			 if(StringUtil.isNotBlank(payStatusStr))
			 {
				 selectItem.put("payStatus", Integer.valueOf(payStatusStr));
			 }
				
			 if(StringUtil.isNotBlank(supplierId))
			 {
				 selectItem.put("supplierId", supplierId);
			 }
			 
			PageDo<PayInfoDo> pagedata= payInfoService.queryPayInfoListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取支付信息数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 支付信息数据分页
	 * @return
	 */
	@RequestMapping(value="/supplierPayInfoList.do",method=RequestMethod.GET)
	public ModelAndView supplierPayInfoList_methodGet(String supplierId)
	{
		ModelAndView view=new ModelAndView("property/supplierPayInfoList");
		try {
			view.addObject("supplierId", supplierId);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑支付信息异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 支付信息数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/supplierPayInfoList.do",method=RequestMethod.POST)
	public ModelAndView supplierPayInfoList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/supplierPayInfoList_table");
		try{ 
			checkPermission(PermissionCode.PAYINFO_LIST_QUERY);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 String batchId=request.getParameter("batchId");
			 String payNo=request.getParameter("payNo");
			 String payStatusStr=request.getParameter("payStatusStr");
			 String companyId=request.getParameter("companyId");
			 String supplierId=request.getParameter("supplierId");
			 AssertUtils.isNotBlank(supplierId, "请求参数错误");
			 selectItem.put("supplierId", supplierId);
			 
			 if(StringUtil.isNotBlank(batchId))
			 {
				 selectItem.put("batchId", batchId);
			 }
			 if(StringUtil.isNotBlank(payNo))
			 {
				 selectItem.put("payNo", payNo);
			 }
			 if(StringUtil.isNotBlank(payStatusStr))
			 {
				 selectItem.put("payStatus", Integer.valueOf(payStatusStr));
			 }
			 if(StringUtil.isNotBlank(companyId))
			 {
				 selectItem.put("companyId", companyId);
			 }
			 
			PageDo<PayInfoDo> pagedata= payInfoService.queryPayInfoListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取支付信息数据分页异常", e);
		}
		return view;
	}
	 
}
