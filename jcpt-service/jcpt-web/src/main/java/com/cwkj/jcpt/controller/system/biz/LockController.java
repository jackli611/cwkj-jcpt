package com.cwkj.jcpt.controller.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.LockDo;
import com.cwkj.jcptsystem.service.system.BizLockService;

/**
 * 防止并发锁表.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/system/biz/")
public class LockController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(LockController.class);
	
	@Autowired
	private BizLockService bizLockService;
	
	 
	
	/**
	 * 防止并发锁表数据分页
	 * @return
	 */
	@RequestMapping(value="/lockList.do",method=RequestMethod.GET)
	public ModelAndView lockList_methodGet()
	{
		ModelAndView view=new ModelAndView("system/biz/lockList");
		return view;
	}
	
	/**
	 * 防止并发锁表数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/lockList.do",method=RequestMethod.POST)
	public ModelAndView lockList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("system/biz/lockList_table");
		try{ 
			//权限检验
			checkPermission(PermissionCode.SYSTEM_BIZLOCK_LIST);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<LockDo> pagedata= bizLockService.queryLockListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取防止并发锁表数据分页异常", e);
		}
		return view;
	}
	  
	 
	/**
	 * 删除防止并发锁表
	 * @param request
	 * @param lockNum
	 * @return
	 */
	@RequestMapping(value="/deleteLock.do")
	public JsonView<String> deleteLock_methodPost(HttpServletRequest request,String lockNum)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			//权限检验
			checkPermission(PermissionCode.SYSTEM_BIZLOCK_DEL);
			AssertUtils.notEmptyStr(lockNum, "参数无效");
			Integer result=bizLockService.deleteLockByLockNum(lockNum);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除防止并发锁表异常",e);
		}
		return view;
	}
	 
}
