package com.cwkj.jcpt.controller.products;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.service.products.ProductService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;

/**
 * 商品.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/products/")
public class ProductController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(ProductController.class);
	
	private ProductService productService;
	
	@Autowired
	public void setProductService(ProductService productService)
	{
		this.productService=productService;
	}
	
	/**
	 * 商品数据分页
	 * @return
	 */
	@RequestMapping(value="/productList.do",method=RequestMethod.GET)
	public ModelAndView productList_methodGet()
	{
		ModelAndView view=new ModelAndView("products/productList");
		return view;
	}
	
	/**
	 * 商品数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/productList.do",method=RequestMethod.POST)
	public ModelAndView productList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("products/productList_table");
		try{ 
			 Map<String, Object> selectItem=getRequestToParamMap(request);
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<ProductDo> pagedata= productService.queryProductListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取商品数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增商品
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addProduct.do",method=RequestMethod.GET)
	public ModelAndView addProduct_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("products/addProduct");
		try{
			view.addObject("product", null);
			//putViewOneLevelProducts(view);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加商品异常（GET）",e);
		}
		return view;
	}

	private void putViewOneLevelProducts(ModelAndView view) {
		Map<String, Object> selectItem = new HashMap<String,Object>();
		selectItem.put("levels", 1);
		List<ProductDo> levelOneProductList = productService.queryProductList(selectItem );
		view.addObject("levelOneProductList", levelOneProductList);
	}
	
	 /**
	 * 新增商品
	 * @param request
	 * @param product
	 * @return
	 */
	@RequestMapping(value="/addProduct.do",method=RequestMethod.POST)
	public ModelAndView addProduct_methodPost(HttpServletRequest request,ProductDo product )
	{
		ModelAndView view=new ModelAndView("products/addProduct");
		try{
			String enterpriseProductStr=request.getParameter("enterpriseProductStr");
			String personalProductStr=request.getParameter("personalProductStr");
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				product.setLevels(Integer.valueOf(levelsStr));
			}
			if(StringUtil.isNotBlank(enterpriseProductStr))
			{
				product.setEnterpriseProduct(Integer.valueOf(enterpriseProductStr));
			}
			if(StringUtil.isNotBlank(personalProductStr))
			{
				product.setPersonalProduct(Integer.valueOf(personalProductStr));
			}
			AssertUtils.isNotBlank(levelsStr,"请选择级别");
			
			if("1".equals(levelsStr)) {
				product.setProductCode(product.getParentCode());
				product.setParentCode(null);
			}
			
			AssertUtils.isNotBlank(product.getProductCode(),"请输入产品编码");
			product.setRecordId(currentUser().getId());
			 
			ProductDo scfProduct = productService.findProductById(product.getProductCode());
			AssertUtils.isNull(scfProduct, "产品已存在，不能新增");
			Integer result= productService.insertProduct(product);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, PROMPTCODE_OK,"操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加商品异常（POST）",e);
		}finally {
			view.addObject("product", product);
		}
		return view;
	}
	
	 /**
	 * 浏览商品
	 * @param request
	 * @param productCode
	 * @return
	 */
	@RequestMapping(value="/viewProduct.do",method=RequestMethod.GET)
	public ModelAndView viewProduct_methodGet(HttpServletRequest request,String productCode)
	{
		ModelAndView view =new ModelAndView("products/viewProduct");
		try{
			ProductDo product= productService.findProductById(productCode);
			view.addObject("product", product);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览商品异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑商品
	 * @param request
	 * @param productCode
	 * @return
	 */
	@RequestMapping(value="/editProduct.do",method=RequestMethod.GET)
	public ModelAndView editProduct_methodGet(HttpServletRequest request,String productCode)
	{
		ModelAndView view =new ModelAndView("products/addProduct");
		try{
			ProductDo product= productService.findProductById(productCode);
			view.addObject("product", product);
			
			if(1 == (product.getLevels().intValue())) {
				product.setParentCode(product.getProductCode());
				product.setProductCode(null);
			}
			
			//putViewOneLevelProducts(view);
			
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑商品异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑商品
	 * @param request
	 * @param product
	 * @return
	 */
	@RequestMapping(value="/editProduct.do",method=RequestMethod.POST)
	public ModelAndView editProduct_methodPost(HttpServletRequest request,ProductDo  product)
	{
		ModelAndView view=new ModelAndView("products/addProduct");
		try{
			String enterpriseProductStr=request.getParameter("enterpriseProductStr");
			String personalProductStr=request.getParameter("personalProductStr");
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				product.setLevels(Integer.valueOf(levelsStr));
			}
			if(StringUtil.isNotBlank(enterpriseProductStr))
			{
				product.setEnterpriseProduct(Integer.valueOf(enterpriseProductStr));
			}
			if(StringUtil.isNotBlank(personalProductStr))
			{
				product.setPersonalProduct(Integer.valueOf(personalProductStr));
			}
			if("1".equals(levelsStr)) {
				product.setProductCode(product.getParentCode());
				product.setParentCode(null);
			}
			
			AssertUtils.isNotBlank(levelsStr,"请选择级别");
			AssertUtils.notNull(product.getProductCode(), "参数无效");
			Integer result= productService.updateProductById(product);
			AssertUtils.isTrue(result.equals(1), "操作失败");
			setPromptMessage(view, PROMPTCODE_OK,"操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑商品异常（GET）",e);
		}finally {
			view.addObject("product",product);
		}
		return view;
	}
	
	/**
	 * 删除商品
	 * @param request
	 * @param productCode
	 * @return
	 */
	@RequestMapping(value="/deleteProduct.do")
	public JsonView<String> deleteProduct_methodPost(HttpServletRequest request,String productCode)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.notNull(productCode, "参数无效");
			Integer result=productService.deleteProductById(productCode);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除商品异常",e);
		}
		return view;
	}
	 
}
