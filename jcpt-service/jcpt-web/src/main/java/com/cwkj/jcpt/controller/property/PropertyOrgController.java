package com.cwkj.jcpt.controller.property;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.HttpServletResponseUtil;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.property.CompanyDo;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.service.config.CommonConfigService;
import com.cwkj.scf.service.config.UploadFileConfig;
import com.cwkj.scf.service.products.ProductService;
import com.cwkj.scf.service.property.PropertyOrgService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;

/**
 * 物业机构.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class PropertyOrgController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(PropertyOrgController.class);
	
	private PropertyOrgService propertyOrgService;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	@Autowired
	private CommonConfigService commonConfigService;
	@Autowired
	private ProductService productService;
	
	@Autowired
	public void setPropertyOrgService(PropertyOrgService propertyOrgService)
	{
		this.propertyOrgService=propertyOrgService;
	}
	
	private String storeFileRootPath()
	{
		return commonConfigService.getUploadFileConfig().getStoreRootPath();
	}
	
	/**
	 * 物业机构数据分页
	 * @return
	 */
	@RequestMapping(value="/propertyOrgList.do",method=RequestMethod.GET)
	public ModelAndView propertyOrgList_methodGet()
	{
		ModelAndView view=new ModelAndView("property/propertyOrgList");
		return view;
	}
	
	/**
	 * 物业机构数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/propertyOrgList.do",method=RequestMethod.POST)
	public ModelAndView propertyOrgList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/propertyOrgList_table");
		try{ 
			checkPermission(PermissionCode.PROPERTYORG_LIST_QUERY);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<PropertyOrgDo> pagedata= propertyOrgService.queryPropertyOrgListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取物业机构数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增物业机构
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addPropertyOrg.do",method=RequestMethod.GET)
	public ModelAndView addPropertyOrg_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("property/addPropertyOrg");
		try{
			view.addObject("propertyOrg", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业机构异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 新增物业机构--基本信息页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/propertyBaseInfo.do",method=RequestMethod.GET)
	public ModelAndView propertyBaseInfo(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("property/propertyBaseInfo");
		try{
			view.addObject("propertyOrg", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业机构异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增物业机构
	 * @param request
	 * @param propertyOrg
	 * @return
	 */
	@RequestMapping(value="/addPropertyOrg.do",method=RequestMethod.POST)
	public ModelAndView addPropertyOrg_methodPost(HttpServletRequest request,PropertyOrgDo propertyOrg)
	{
		ModelAndView view=new ModelAndView("property/addPropertyOrg");
		try{
			checkPermission(PermissionCode.PROPERTYORG_ADD);
			AssertUtils.isNotBlank(propertyOrg.getNames(), "机构名称不能为空");
			 propertyOrg.setRecordUserId(currentUser().getId());
			 propertyOrg.setStatus(PropertyOrgDo.STATUS_VALID);
			 
			Integer result= propertyOrgService.insertPropertyOrg(propertyOrg);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, PROMPTCODE_OK,"操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业机构异常（POST）",e);
		}finally {
			view.addObject("propertyOrg", propertyOrg);
		}
		return view;
	}
	/**
	 * 新增物业机构
	 * @param request
	 * @param propertyOrg
	 * @return
	 */
	@RequestMapping(value="/addPropertyOrgByAjax.do",method=RequestMethod.POST)
	public JsonView<PropertyOrgDo> addPropertyOrgByAjax(HttpServletRequest request,PropertyOrgDo propertyOrg)
	{
		JsonView<PropertyOrgDo> view = JsonView.successJsonView("操作成功");
		try{
			checkPermission(PermissionCode.PROPERTYORG_ADD);
			AssertUtils.isNotBlank(propertyOrg.getNames(), "机构名称不能为空");
			propertyOrg.setRecordUserId(currentUser().getId());
			propertyOrg.setStatus(PropertyOrgDo.STATUS_VALID);
			
			Integer result= propertyOrgService.insertPropertyOrg(propertyOrg);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业机构异常（POST）",e);
		}
		return view;
	}
	
	 /**
	 * 浏览物业机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewPropertyOrg.do",method=RequestMethod.GET)
	public ModelAndView viewPropertyOrg_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/viewPropertyOrg");
		try{
			checkPermission(PermissionCode.PROPERTYORG_VIEW);
			PropertyOrgDo propertyOrg= propertyOrgService.findPropertyOrgById(id);
			view.addObject("propertyOrg", propertyOrg);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览物业机构异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑物业机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editPropertyOrg.do",method=RequestMethod.GET)
	public ModelAndView editPropertyOrg_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/addPropertyOrg");
		try{
			checkPermission(PermissionCode.PROPERTYORG_EDIT);
			PropertyOrgDo propertyOrg= propertyOrgService.findPropertyOrgById(id);
			view.addObject("propertyOrg", propertyOrg);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业机构异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑物业机构
	 * @param request
	 * @param propertyOrg
	 * @return
	 */
	@RequestMapping(value="/editPropertyOrg.do",method=RequestMethod.POST)
	public ModelAndView editPropertyOrg_methodPost(HttpServletRequest request,PropertyOrgDo  propertyOrg)
	{
		ModelAndView view =new ModelAndView("property/addPropertyOrg");
		try{
			checkPermission(PermissionCode.PROPERTYORG_EDIT);
			AssertUtils.isNotBlank(propertyOrg.getId(), "参数无效");
			Integer result= propertyOrgService.updatePropertyNameInfo(propertyOrg);
			AssertUtils.isTrue(result.equals(1), "操作失败");
			setPromptMessage(view, PROMPTCODE_OK,"操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业机构异常（GET）",e);
		}finally {
			view.addObject("propertyOrg", propertyOrg);
		}
		return view;
	}
	
	/**
	 * 编辑物业机构
	 * @param request
	 * @param propertyOrg
	 * @return
	 */
	@RequestMapping(value="/editPropertyOrgByAjax.do",method=RequestMethod.POST)
	public JsonView<PropertyOrgDo> editPropertyOrgByAjax(HttpServletRequest request,PropertyOrgDo  propertyOrg)
	{
		JsonView<PropertyOrgDo> view = JsonView.successJsonView("操作成功");
		try{
			checkPermission(PermissionCode.PROPERTYORG_EDIT);
			AssertUtils.isNotBlank(propertyOrg.getId(), "参数无效");
			Integer result= propertyOrgService.updatePropertyNameInfo(propertyOrg);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业机构异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 删除物业机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/propertyOrgStatus.do")
	public JsonView<String> propertyOrgStatus_methodPost(HttpServletRequest request,String id,Integer status)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.PROPERTYORG_DELETE);
			AssertUtils.isNotBlank(id, "参数无效[error:001]");
			AssertUtils.notNull(status, "参数无效[error:002]");
			Integer result=propertyOrgService.updateStatus(id, status);
			AssertUtils.isTrue(result.equals(1),"修改失败:[error:003]");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除物业机构异常",e);
		}
		return view;
	}
	
	 /**
		 * 浏览物业机构
		 * @param request
		 * @param id
		 * @return
		 */
		@RequestMapping(value="/uploadBusinessLicense.do",method=RequestMethod.GET)
		public ModelAndView uploadBusinessLicense_methodGet(HttpServletRequest request,String id)
		{
			ModelAndView view =new ModelAndView("property/uploadBusinessLicense");
			try{
				checkPermission(PermissionCode.PROPERTYORG_VIEW);
				PropertyOrgDo propertyOrg= propertyOrgService.findPropertyOrgById(id);
				view.addObject("propertyOrg", propertyOrg);
			}catch (BusinessException e) {
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("浏览物业机构异常（GET）",e);
			}
			return view;
		}
		
		@RequestMapping(value="/uploadBusinessLicense.do",method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		@ResponseBody
		public String uploadExcelFile_methodPost(@RequestParam(value = "file", required = false) MultipartFile file,HttpServletRequest request,String id)
		{
//			JsonView<String> view=null;
			JSONObject result=new JSONObject();
			try{
				AssertUtils.notNull(file,"上传图片文件不能为空");
			 
				String imageDirName="businessLicense/";
					String path=storeFileRootPath()+imageDirName;
					 String saveFileName=sysSeqNumberService.daySeqnumberString("BL");
					 int pointIndex=file.getOriginalFilename().lastIndexOf(".");
				        if(pointIndex>0)
				        {
				        	saveFileName+=file.getOriginalFilename().substring( pointIndex);
				        }
					 File targetFile = new File(path, saveFileName);  
				        if(!targetFile.exists()){  
				            targetFile.mkdirs();  
				        }   
				        //保存  
		      file.transferTo(targetFile);  
		      Integer dbRes=propertyOrgService.updateBusinessLicense(id, imageDirName+saveFileName);
		      AssertUtils.isTrue(dbRes!=null && dbRes>0,"更新失败");
		      result.put("result", "ok");
		      result.put("message", "操作成功");
			}catch (BusinessException e) {
				logger.info("上传营业执照失败:{}",e.getMessage());
				 result.put("result", "failed");
			      result.put("message", e.getMessage());
			}catch (Exception e) {
				logger.error("上传营业执照异常",e);
				 result.put("result", "failed");
			      result.put("message", e.getMessage());
			}
			return result.toString();
		}
		
		/**
		 * 获取图片（营业执照）
		 * @param request
		 * @param response
		 * @param id
		 */
		@RequestMapping("/businessLicenseFile.do")
		public void businessLicenseFile(HttpServletRequest request,HttpServletResponse response,String id)  
		{
			try {
				File file=null;
				PropertyOrgDo propertyOrg=propertyOrgService.findPropertyOrgById(id);
				String contentType="JPEG";
				if(propertyOrg!=null && StringUtil.isNotBlank(propertyOrg.getBusinessLicense()))
				{
					file=new File(storeFileRootPath()+propertyOrg.getBusinessLicense());
					String[] imgtype=propertyOrg.getBusinessLicense().split(".");
					if(imgtype.length>1)
					{
					contentType=imgtype[imgtype.length-1];
					}
				}else {
					file=null;
				}
			
			if(file.exists()){
				//设置MIME类型
				response.setHeader("Pragma", "no-cache");
				response.setHeader("Cache-Control", "no-cache");
				response.setDateHeader("Expires", 0);
				response.setContentType("image/"+contentType);
				HttpServletResponseUtil.responseFile(response,file);
			}
			}catch (Exception e) {
				logger.error("获取营业执照异常",e);
			}
		}
		
		
		/**
		 * 企业订单入口数据分页
		 * @param request
		 * @param pageIndex
		 * @param pageSize
		 * @param startDate  
		 * @param endDate  
		 * @return
		 */
		@RequestMapping(value="/queryCompanyListForOrder.do",method= {RequestMethod.POST,RequestMethod.GET})
		public ModelAndView queryCompanyListForOrder(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
		{
			ModelAndView view=new ModelAndView("property/companyListForOrder_table");
			try{ 
				checkPermission(PermissionCode.PROPERTYORG_LIST_QUERY);
				 Map<String, Object> selectItem=new HashMap<String, Object>();
				 setDateBetweemToMap(selectItem, startDate, endDate);
				 pageIndex=pageIndex==null?1L:pageIndex;
				 pageSize=pageSize==null?20:pageSize;
				PageDo<PropertyOrgDo> pagedata= propertyOrgService.queryPropertyOrgListPage(pageIndex,pageSize, selectItem);
				view.addObject("pagedata",pagedata);
				
			}catch(BusinessException e)
			{
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("获取企业数据分页异常", e);
			}
			return view;
		}
}
