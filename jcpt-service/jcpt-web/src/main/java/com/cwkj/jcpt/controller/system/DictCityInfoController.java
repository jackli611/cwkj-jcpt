/**
 * Project Name:jcpt-web <br>
 * File Name:DictCityInfoController.java <br>
 * Package Name:com.hehenian.jcpt.controller.system <br>
 * @author anxymf
 * Date:2017年1月9日上午9:27:17 <br>
 * Copyright (c) 2017, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcpt.controller.system;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cwkj.jcptsystem.model.system.DictCityInfoDo;
import com.cwkj.jcptsystem.service.system.DictCityInfoService;

/**
 * ClassName: DictCityInfoController <br>
 * Description: 城市信息字典
 * @author anxymf
 * Date:2017年1月9日上午9:27:17 <br>
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping("/system/cityInfo")
public class DictCityInfoController {
	
	/** 城市信息字典 */
	@Resource
	private DictCityInfoService dictCityInfoService;
	
	/**
	 * 
	 * queryProvince:查询省份. <br>
	 *
	 * @author anxymf
	 * Date:2017年2月12日下午4:34:51 <br>
	 * @return
	 */
	@RequestMapping(value = "/queryProvince.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String queryProvince(){
		List<DictCityInfoDo> cityInfos = dictCityInfoService.queryProvince();
		return JSON.toJSONString(cityInfos);
	}

	/**
	 * 
	 * queryCityByProvince:根据省份ID查询城市. <br>
	 *
	 * @author anxymf
	 * Date:2017年2月12日下午4:34:59 <br>
	 * @param cityCode 省份ID
	 * @return
	 */
	@RequestMapping(value = "/queryCityByProvince.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String queryCityByProvince(String cityCode){
		List<DictCityInfoDo> cityInfos = dictCityInfoService.queryCityByProvince(cityCode);
		return JSON.toJSONString(cityInfos);
	}
	
}

	