/**
 * Project Name:jcpt-web <br>
 * File Name:ParamController.java <br>
 * Package Name:com.hehenian.jcpt.controller.system <br>
 * @author anxymf
 * Date:2016年12月7日上午10:33:34 <br>
 * Copyright (c) 2016, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcpt.controller.system;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.ParamDo;
import com.cwkj.jcptsystem.service.system.ParamService;

/**
 * 系统参数
 * @version
 * @since JDK 1.6
 */
@Controller
@RequestMapping("/system/param/")
public class ParamController extends BaseAction {
	
	private static Logger log=LoggerFactory.getLogger(ParamController.class);
	
	/** 系统参数服务 */
	@Resource
	private ParamService paramService;
	
	/**
	 * 
	 * list:系统参数查询页面. <br>
	 * @return
	 */
	@RequestMapping(value = "/list.do", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("/system/param/list");
	}
	
	/**
	 * 
	 * listData:系统参数查询数据. <br>
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/listData.do")
	public ModelAndView listData(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("/system/param/listData");
		try {
			checkPermission(PermissionCode.SYSTEM_PARAM_LIST);
			Map<String,Object> selectItem = getRequestToParamMap(request);
			selectItem.put("isValid",ParamDo.ISVALID_T);
			model.addObject("pagedata",paramService.queryListPage(selectItem));
		} catch (BusinessException e) {
			setPromptException(model, e);
		} catch (Exception e) {
			log.error("获取系统参数列表异常", e);
			setPromptException(model, e);
		}
		return model;
	}
	
	/**
	 * 
	 * preShow:新增修改显示页面. <br>
	 * @param paramId
	 * @return
	 */
	@RequestMapping(value = "/preShow.do")
	public ModelAndView preShow(String paramId){
		ModelAndView model = new ModelAndView("/system/param/preShow");
		try{
			if(StringUtil.isNotEmpty(paramId)){
				checkPermission(PermissionCode.SYSTEM_PARAM_EDIT);
				model.addObject("model",paramService.queryById(paramId));
			}else{
				checkPermission(PermissionCode.SYSTEM_PARAM_ADD);
			}
		} catch (BusinessException e) {
			setPromptException(model, e);
		} catch (Exception e) {
			log.error("获取系统信息字典记录异常", e);
			setPromptException(model, e);
		}
		return model;
		
	}
	
	/**
	 * 
	 * add:新增系统参数. <br>
	 * @param param 系统参数对象
	 * @return
	 */
	@RequestMapping(value = "/add.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<ParamDo> add(ParamDo param){
		JsonView<ParamDo> view = new JsonView<ParamDo>();
		try {
			checkPermission(PermissionCode.SYSTEM_DICT_ADD);
			param.setIsValid(ParamDo.ISVALID_T);
			Integer model = paramService.insert(param);			
			if(model>0)
			{
				setPromptMessage(view,view.CODE_SUCCESS, "操作成功");
			}else {
				setPromptMessage(view,view.CODE_FAILE, "操作失败");
			}
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			log.error("新增系统参数提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "新增系统参数提交异常");
		}
		return view;
	}
	
	/**
	 * 	
	 * update:修改系统参数. <br>
	 * @param param 系统参数对象
	 * @return
	 */
	@RequestMapping(value = "/update.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<ParamDo> update(ParamDo param){
		JsonView<ParamDo> view = new JsonView<ParamDo>();
		try {
			checkPermission(PermissionCode.SYSTEM_DICT_EDIT);
			param.setIsValid(ParamDo.ISVALID_T);
			Integer model = paramService.update(param);			
			if(model>0)
			{
				setPromptMessage(view,view.CODE_SUCCESS, "操作成功");
			}else {
				setPromptMessage(view,view.CODE_FAILE, "操作失败");
			}
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			log.error("修改系统参数提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "修改系统参数提交异常");
		}
		return view;
	}
	
	/**
	 * 
	 * delete:删除系统参数. <br>
	 * @param paramId 系统参数ID
	 * @return
	 */
	@RequestMapping(value = "/delete.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JsonView<ParamDo> delete(String paramId){
		JsonView<ParamDo> view = new JsonView<ParamDo>();
		try {
			checkPermission(PermissionCode.SYSTEM_DICT_DELETE);
			Integer model = paramService.delete(paramId);			
			if(model>0)
			{
				setPromptMessage(view,view.CODE_SUCCESS, "操作成功");
			}else {
				setPromptMessage(view,view.CODE_FAILE, "操作失败");
			}
		} catch (BusinessException e) {
			setPromptException(view, e);
		} catch (Exception e) {
			log.error("删除系统参数提交异常", e);
			setPromptMessage(view, JsonView.CODE_FAILE, "删除系统参数提交异常");
		}
		return view;
	}
	
}

	