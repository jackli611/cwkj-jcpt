package com.cwkj.jcpt.controller.shares;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.controller.property.CompanyController;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.model.property.CompanyDo;
import com.cwkj.scf.model.shares.SharePersonalLoanOrder;
import com.cwkj.scf.service.shares.SharePersonalLoanOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/***
 * 分享订单
 */
@Controller
@RequestMapping(value="/shares/")
public class ShareLoanOrderController extends BaseAction {
    private static Logger logger = LoggerFactory.getLogger(ShareLoanOrderController.class);

    @Autowired
    private SharePersonalLoanOrderService sharePersonalLoanOrderService;

    /**
     * 增加佣金结算模块
     *
     * @param request
     * @param pageIndex
     * @param pageSize
     * @param startDate
     * @param endDate
     * @return
     */
    @RequestMapping(value = "/personalLoanOrderList.do", method = RequestMethod.GET)
    public ModelAndView personalLoanOrderList_methodGet(HttpServletRequest request, Long pageIndex, Integer pageSize, String startDate, String endDate) {
        ModelAndView view = new ModelAndView("shares/personalLoanOrderList");
        try {


        } catch (Exception ex) {
            logger.error("个贷分享订单", ex);
            setPromptException(view, ex);
        }
        return view;
    }

    @RequestMapping(value = "/personalLoanOrderList.do", method = RequestMethod.POST)
    public ModelAndView personalLoanOrderList_methodPost(HttpServletRequest request, Long pageIndex, Integer pageSize, String startDate, String endDate) {
        ModelAndView view = new ModelAndView("shares/personalLoanOrderList_table");
        try {
            checkPermission(PermissionCode.SHARE_PERSONALLOAN_LIST_QUERY);
            Map<String, Object> selectItem = getRequestToParamMap(request);
            setDateBetweemToMap(selectItem, startDate, endDate);
            pageIndex = pageIndex == null ? 1L : pageIndex;
            pageSize = pageSize == null ? 20 : pageSize;
            PageDo<SharePersonalLoanOrder> pagedata = sharePersonalLoanOrderService.querySharePersonalLoanOrderListPage(pageIndex, pageSize, selectItem);
            view.addObject("pagedata", pagedata);
        }catch (BusinessException e)
        {
            logger.info("查询个贷分享订单失败:{}",e.getMessage());
            setPromptException(view,e);
        } catch (Exception ex) {
            logger.error("个贷分享订单", ex);
            setPromptException(view, ex);
        }
        return view;
    }
}
