package com.cwkj.jcpt.controller.property;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.scf.model.property.CompanyDo;
import com.cwkj.scf.service.property.CompanyService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;

/**
 * 企业.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class CompanyController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(CompanyController.class);
	
	private CompanyService companyService;
	
	@Autowired
	public void setCompanyService(CompanyService companyService)
	{
		this.companyService=companyService;
	}
	
	/**
	 * 企业数据分页
	 * @return
	 */
	@RequestMapping(value="/companyList.do",method=RequestMethod.GET)
	public ModelAndView companyList_methodGet()
	{
		ModelAndView view=new ModelAndView("property/companyList");
		return view;
	}
	
	/**
	 * 企业数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/companyList.do",method=RequestMethod.POST)
	public ModelAndView companyList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/companyList_table");
		try{ 
			checkPermission(PermissionCode.COMPANY_LIST_QUERY);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 String names=request.getParameter("names");
			 String mobile=request.getParameter("mobile");
			 String organizationCode=request.getParameter("organizationCode");
			 String levelsStr=request.getParameter("levelsStr");
			 String approvalStatusStr=request.getParameter("approvalStatusStr");
			 if(StringUtil.isNotBlank(names))
			 {
				 selectItem.put("names", names);
			 }
			 if(StringUtil.isNotBlank(mobile))
			 {
				 selectItem.put("mobile",mobile);
			 }
			 if(StringUtil.isNotBlank(organizationCode))
			 {
				 selectItem.put("organizationCode",organizationCode);
			 }
			 if(StringUtil.isNotBlank(levelsStr))
			 {
				 selectItem.put("levels",Integer.valueOf(levelsStr));
			 }
			 if(StringUtil.isNotBlank(approvalStatusStr))
			 {
				 selectItem.put("approvalStatus",Integer.valueOf(approvalStatusStr));
			 }
			PageDo<CompanyDo> pagedata= companyService.queryCompanyListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取企业数据分页异常", e);
		}
		return view;
	}
	
	
	
	
	/**
	 * 新增企业
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addCompany.do",method=RequestMethod.GET)
	public ModelAndView addCompany_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_ADD);
			view.addObject("company", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加企业异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增企业
	 * @param request
	 * @param company
	 * @return
	 */
	@RequestMapping(value="/addCompany.do",method=RequestMethod.POST)
	public ModelAndView addCompany_methodPost(HttpServletRequest request,CompanyDo company)
	{
		ModelAndView view=new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_ADD);
			AssertUtils.isNotBlank(company.getNames(), "企业名称不能为空");
			AssertUtils.isNotBlank(company.getMobile(), "手机号不能为空");
			AssertUtils.isNotBlank(company.getLegal(), "企业法人不能为空");
			AssertUtils.isNotBlank(company.getLegalIdNo(), "企业法人身份证号不能为空");
			AssertUtils.isNotBlank(company.getOrganizationCode(), "组织机构代码不能为空");
			company.setRecordUserId(currentUser().getId());
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				company.setLevels(Integer.valueOf(levelsStr));
			}
			company.setApprovalStatus(CompanyDo.APPROVALSTATUS_INIT);
			Integer result= companyService.insertCompany(company);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加企业异常（POST）",e);
		}finally {
			view.addObject("company", company);
		}
		return view;
	}
	
	
	/**
	 *	 新增企业
     * @param request
	 * @param company
	 * @return
	 */
	@RequestMapping(value="/addCompanyByAjax.do",method=RequestMethod.POST)
	public JsonView<CompanyDo> addCompanyByAjax(HttpServletRequest request,CompanyDo company)
	{
		JsonView<CompanyDo> view = JsonView.successJsonView("操作成功");
		try{
			checkPermission(PermissionCode.COMPANY_ADD);
			AssertUtils.isNotBlank(company.getNames(), "企业名称不能为空");
			AssertUtils.isNotBlank(company.getMobile(), "手机号不能为空");
			AssertUtils.isNotBlank(company.getLegal(), "企业法人不能为空");
			AssertUtils.isNotBlank(company.getLegalIdNo(), "企业法人身份证号不能为空");
			AssertUtils.isNotBlank(company.getOrganizationCode(), "组织机构代码不能为空");
			company.setRecordUserId(currentUser().getId());
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				company.setLevels(Integer.valueOf(levelsStr));
			}
			company.setApprovalStatus(CompanyDo.APPROVALSTATUS_INIT);
			Integer result= companyService.insertCompany(company);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加企业异常（POST）",e);
		}
		return view;
	}

	
	 /**
	 * 浏览企业
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewCompany.do",method=RequestMethod.GET)
	public ModelAndView viewCompany_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/viewCompany");
		try{
			checkPermission(PermissionCode.COMPANY_VIEW);
			CompanyDo company= companyService.findCompanyById(id);
			view.addObject("company", company);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览企业异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑企业
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editCompany.do",method=RequestMethod.GET)
	public ModelAndView editCompany_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_EDIT);
			CompanyDo company= companyService.findCompanyById(id);
			view.addObject("company", company);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑企业异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑企业
	 * @param request
	 * @param company
	 * @return
	 */
	@RequestMapping(value="/editCompany.do",method=RequestMethod.POST)
	public ModelAndView editCompany_methodPost(HttpServletRequest request,CompanyDo  company)
	{
		ModelAndView view =new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_EDIT);
			AssertUtils.isNotBlank(company.getId(), "参数无效");
			AssertUtils.isNotBlank(company.getNames(), "企业名称不能为空");
			AssertUtils.isNotBlank(company.getMobile(), "手机号不能为空");
			AssertUtils.isNotBlank(company.getLegal(), "企业法人不能为空");
			AssertUtils.isNotBlank(company.getLegalIdNo(), "企业法人身份证号不能为空");
			AssertUtils.isNotBlank(company.getOrganizationCode(), "组织机构代码不能为空");
			company.setRecordUserId(currentUser().getId());
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				company.setLevels(Integer.valueOf(levelsStr));
			}
			company.setApprovalStatus(CompanyDo.APPROVALSTATUS_INIT);
			Integer result= companyService.updateCompanyById(company);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
			
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑企业异常（GET）",e);
		}finally {
			view.addObject("company", company);
		}
		return view;
	}
	
	/**
	 * 编辑企业
	 * @param request
	 * @param company
	 * @return
	 */
	@RequestMapping(value="/editCompanyByAjax.do",method=RequestMethod.POST)
	public JsonView<CompanyDo> editCompanyByAjax(HttpServletRequest request,CompanyDo  company)
	{
		JsonView<CompanyDo> view = JsonView.successJsonView("操作成功");
		try{
			checkPermission(PermissionCode.COMPANY_EDIT);
			AssertUtils.isNotBlank(company.getId(), "参数无效");
			AssertUtils.isNotBlank(company.getNames(), "企业名称不能为空");
			AssertUtils.isNotBlank(company.getMobile(), "手机号不能为空");
			AssertUtils.isNotBlank(company.getLegal(), "企业法人不能为空");
			AssertUtils.isNotBlank(company.getLegalIdNo(), "企业法人身份证号不能为空");
			AssertUtils.isNotBlank(company.getOrganizationCode(), "组织机构代码不能为空");
			company.setRecordUserId(currentUser().getId());
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				company.setLevels(Integer.valueOf(levelsStr));
			}
			company.setApprovalStatus(CompanyDo.APPROVALSTATUS_INIT);
			Integer result= companyService.updateCompanyById(company);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑企业异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 删除企业
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deleteCompany.do")
	public JsonView<String> deleteCompany_methodPost(HttpServletRequest request,String id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.COMPANY_DELETE);
			AssertUtils.isNotBlank(id, "参数无效");
			Integer result=companyService.deleteCompanyById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除企业异常",e);
		}
		return view;
	}
	
	
}
