package com.cwkj.jcpt.taglib;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cwkj.jcptsystem.model.system.DictSystemInfoDo;
import com.cwkj.jcptsystem.service.system.DictSystemInfoService;
import com.cwkj.redis.RedisKeyConst;
import com.cwkj.redis.system.RedisSystemService;

public class DictCheckBoxTag extends TagSupport{


	
	/**
	 * springContext
	 */
	private WebApplicationContext springContext;

	/**
	 * 查询type类型
	 */
	private String type;

	/**
	 * 选中的值
	 */
	private String value = "";

	/**
	 * 排除不显示
	 */
	private String exclude = "";

	/**
	 * 是否为选择框
	 */
	private String checkBoxName;
	private String checkBoxId;
	
	@SuppressWarnings("unchecked")
	@Override
	public int doEndTag() throws JspException {
		//获取spring上下文环境
		springContext = WebApplicationContextUtils
				.getWebApplicationContext(pageContext.getServletContext());
		//获取字典服务
		DictSystemInfoService dictSystemInfoService = (DictSystemInfoService) springContext.getBean(DictSystemInfoService.class);
		//获取缓存服务
		RedisSystemService redisSystemService = (RedisSystemService) springContext.getBean(RedisSystemService.class);
		redisSystemService.setKeyPrefix(RedisKeyConst.JCPT_DICT_SYS);
		List<DictSystemInfoDo> list = null;
		//从page域获取值
		list = (List<DictSystemInfoDo>) pageContext.getAttribute(type,PageContext.PAGE_SCOPE);
		if(list == null || list.size() == 0){
			//从缓存域获取值
			list = (List<DictSystemInfoDo>)redisSystemService.query(type);
			if(list == null || list.size() == 0){
				//查询数据库
				list = dictSystemInfoService.queryCodeList(type);
				//设置到缓存
				redisSystemService.save(type, list);
			}
			//设置page域
			pageContext.setAttribute(type, list,PageContext.PAGE_SCOPE);
		}
		
		JspWriter out = pageContext.getOut();
		try {
			StringBuffer buffer = new StringBuffer("");
			String[] excludeValues = exclude.split(",");
			for (DictSystemInfoDo code : list) {
				for (String excludeValue : excludeValues) {
					if (!excludeValue.equals(code.getCodeValue())) {
						buffer.append("<input type='checkbox' ")
							  .append(" id = '").append(this.checkBoxId).append("'")
							  .append(" name= '").append(this.checkBoxName).append("'")
							  .append(" value='").append(code.getCodeValue()).append("'");
						
						if (code.getCodeValue().equals(value)) {
							buffer.append(" checked='checked' ");
						}
						buffer.append(">");
						buffer.append(code.getCodeName());
					}
				}
			}
			out.print(buffer.toString());
		
		} catch (Exception e) {
			e.printStackTrace();
		}

		return super.doEndTag();
	}

	/**
	 * 获取springContext
	 * 
	 * @return springContext springContext
	 */
	public WebApplicationContext getSpringContext() {
		return springContext;
	}

	/**
	 * 设置springContext
	 * 
	 * @param springContext
	 *            springContext
	 */
	public void setSpringContext(WebApplicationContext springContext) {
		this.springContext = springContext;
	}

	/**
	 * 获取查询type类型
	 * 
	 * @return type 查询type类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 设置查询type类型
	 * 
	 * @param type
	 *            查询type类型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 获取选中的值
	 * 
	 * @return value 选中的值
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 设置选中的值
	 * 
	 * @param value
	 *            选中的值
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 获取排除不显示
	 * 
	 * @return exclude 排除不显示
	 */
	public String getExclude() {
		return exclude;
	}

	/**
	 * 设置排除不显示
	 * 
	 * @param exclude
	 *            排除不显示
	 */
	public void setExclude(String exclude) {
		this.exclude = exclude;
	}

}
