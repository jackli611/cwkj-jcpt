package com.cwkj.jcpt.util;

import org.apache.poi.ss.usermodel.Sheet;

public interface ImportExcelSheetHandler {
	
	/**
	 * 数据处理.
	 * @param sheet.
	 * @return
	 */
	public boolean fetchSheet(Sheet sheet);
	
	/**
	 * @param size
	 */
	public void setSheetSize(Integer size);
	
	
	

}
