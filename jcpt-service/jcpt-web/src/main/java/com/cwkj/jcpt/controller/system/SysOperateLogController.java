package com.cwkj.jcpt.controller.system;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcptsystem.model.system.SysOperateLogDo;
import com.cwkj.jcptsystem.service.system.SysOperateLogService;

/** 
 * 用户操作日志管理
 * @author: harry
 * @date: 2017年7月14日 上午7:53:09  
 */
@Controller
@RequestMapping(value="/system/operateLog/")
public class SysOperateLogController extends BaseAction{
	private static Logger logger=LoggerFactory.getLogger(SysOperateLogController.class);
	@Autowired
	private SysOperateLogService sysOperateLogService;
	
	@RequestMapping(value="/list.do",method=RequestMethod.GET)
	public ModelAndView queryUserList_methodGet()
	{
		ModelAndView view=new ModelAndView("system/queryOperateLogList");
		return view;
	}
	
	@RequestMapping(value="/queryOperateLogList.do",method=RequestMethod.POST)
	public ModelAndView queryUserList_methodPost(HttpServletRequest request,String startDate,String endDate, Long pageIndex,Integer pageSize)
	{
		ModelAndView view=new ModelAndView("system/queryOperateLogList_table");
		try{
			//checkPermission(PermissionCode.system_user_list);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 selectItem.put("userName", request.getParameter("userName"));
			 selectItem.put("realName", request.getParameter("realName"));
			 selectItem.put("parentUrlName", request.getParameter("parentUrlName"));
			 setDateBetweemToMap(selectItem,startDate,endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 PageDo<SysOperateLogDo> result= sysOperateLogService.querySysOperateLogListPage(pageIndex, pageSize, selectItem);
			 view.addObject("pagedata",result);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询用户操作日志数据分页异常", e);
		}
		return view;
	}
	
}
