package com.cwkj.jcpt.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.controller.base.BaseAction;
import com.cwkj.jcpt.controller.base.JsonView;
import com.cwkj.jcpt.controller.base.PermissionCode;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysPermissionDo;
import com.cwkj.jcptsystem.service.system.SysPermissionService;

/**
 * 权限资源
 * @author ljc
 *
 */
@Controller
@RequestMapping("/system")
public class SysPermissionController extends BaseAction{

	private static Logger log=LoggerFactory.getLogger(SysPermissionController.class);
	@Autowired
	private SysPermissionService sysPermissionService;
	
	@RequestMapping(value="/listPermission.do",method=RequestMethod.GET)
	public ModelAndView listPermission(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("/system/listPermission");
		return view;
	}
	
	@RequestMapping(value="/listPermissionData.do")
	public ModelAndView listPermissionData(HttpServletRequest request,Long pageIndex,Integer pageSize,Integer dataType,Long parentId)
	{
		ModelAndView view=new ModelAndView("/system/listPermission_table");
		try{
			//权限检验
			checkPermission(PermissionCode.system_permission_list);
			//查询
			pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 selectItem.put("name", request.getParameter("name"));
			 selectItem.put("dataType", dataType);
			 selectItem.put("perCode", request.getParameter("perCode"));
//			 selectItem.put("parentId", parentId==null?0:parentId);
			PageDo<SysPermissionDo> result= sysPermissionService.queryPermissionListPage(pageIndex, pageSize, selectItem);
			view.addObject("pagedata",result);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			log.error("获取权限资源列表异常", e);
			setPromptException(view, e);
		}
		return view;
	}
	
	@RequestMapping(value="/editPermission.do",method=RequestMethod.GET)
	public ModelAndView editPermission_page(Long id)
	{
		ModelAndView view=new ModelAndView("/system/editPermission");
		try{
			view.addObject("id", id);
			SysPermissionDo sysPermission= sysPermissionService.findSysPermissionById(id);
			view.addObject("sysPermission",sysPermission);
		}catch (Exception e) {
			log.error("编辑权限资源异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/editPermission.do",method=RequestMethod.POST)
	public ModelAndView editPermission(SysPermissionDo sysPermission)
	{
		ModelAndView view=new ModelAndView("/system/editPermission");
		try{
			//权限验证
			checkPermission(PermissionCode.system_permission_edit);
			//返回页面表单信息
			view.addObject("sysPermission", sysPermission);
		
			//表单信息过滤
			checkEmpty(sysPermission.getName(),"名称不能为空");
			checkEmpty(sysPermission.getId(),"无效参数");
			Integer dataType= sysPermission.getDataType();
			if(dataType!=null)
			{
				if(dataType.equals(1))
				{
					checkEmpty(sysPermission.getUrl(),"菜单链接地址不能为空");
				}else if(dataType.equals(2))
				{
					checkEmpty(sysPermission.getPerCode(),"权限代码不能为空");
				}
			} 
			//保存表单信息
			sysPermission.setAvailable(1);//默认生效
			
			sysPermissionService.updateSysPermissionBaseInfoById(sysPermission);
			//业务执行成功，页面提示
			setPromptMessage(view, "修改成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			log.error("编辑权限异常", e);
			setPromptException(view, e);
		}
		return view;
	}

	@RequestMapping(value="/addPermission.do",method=RequestMethod.GET)
	public ModelAndView addPerimission_methodGet(Long parentId)
	{
		ModelAndView view=new ModelAndView("/system/addPermission");
		try{
			view.addObject("parentId", parentId);
		}catch (Exception e) {
			log.error("新增权限资源异常",e);
			setPromptException(view, e);
		}
		return view;
	}
	
	@RequestMapping(value="/addPermission.do",method=RequestMethod.POST)
	public ModelAndView addPerimission_methodPost(SysPermissionDo sysPermission)
	{
		ModelAndView view=new ModelAndView("/system/addPermission");
		try{
			//权限验证
			checkPermission(PermissionCode.system_permission_add);
			//返回页面表单信息
			view.addObject("sysPermission", sysPermission);
		
			//表单信息过滤
			checkEmpty(sysPermission.getName(),"名称不能为空");
			Integer dataType= sysPermission.getDataType();
			if(dataType!=null)
			{
				if(dataType.equals(1))
				{
					checkEmpty(sysPermission.getUrl(),"菜单链接地址不能为空");
				}else if(dataType.equals(2))
				{
					checkEmpty(sysPermission.getPerCode(),"权限代码不能为空");
				}
			} 
			//保存表单信息
			sysPermission.setAvailable(1);//默认生效
			if(sysPermission.getParentId()==null)
			{
				sysPermission.setParentId(0L);
			}
			
			sysPermissionService.insertSysPermission(sysPermission);
			//业务执行成功，页面提示
			setPromptMessage(view, "保存成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			log.error("新增权限资源异常", e);
			setPromptException(view, e);
		}
		return view;
	}
	
	
	/** 重新设计新增／修改权限**/
	@RequestMapping(value="/editPermissionTree.do",method=RequestMethod.GET)
	public ModelAndView editPermissionTree_page(Long parentId)
	{
		ModelAndView view=new ModelAndView("/system/editPermissionTree");
		try{
//			view.addObject("id", id);
			view.addObject("parentId", parentId);
			checkAtLeastOnePermission(PermissionCode.system_permission_edit,PermissionCode.system_permission_add,PermissionCode.system_permission_delete);
			SysPermissionDo sysPermission= sysPermissionService.findSysPermissionById(parentId);
			view.addObject("sysPermission",sysPermission);
			
		}catch (Exception e) {
			log.error("编辑权限资源异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/editPermissionTree.do",method=RequestMethod.POST)
	public ModelAndView editPermissionTree_methodPost(SysPermissionDo sysPermission,Integer parentId)
	{
		ModelAndView view=new ModelAndView("/system/editPermissionTree");
		try{
			//权限验证
			checkPermission(PermissionCode.system_permission_edit);
			//返回页面表单信息
			//view.addObject("sysPermission", sysPermission);
		
			//表单信息过滤
			checkEmpty(sysPermission.getName(),"名称不能为空");
			checkEmpty(sysPermission.getId(),"无效参数");
			Integer dataType= sysPermission.getDataType();
			if(dataType!=null)
			{
				if(dataType.equals(1))
				{
					checkEmpty(sysPermission.getUrl(),"菜单链接地址不能为空");
				}else if(dataType.equals(2))
				{
					checkEmpty(sysPermission.getPerCode(),"权限代码不能为空");
				}
			} 
			//保存表单信息
			sysPermission.setAvailable(1);//默认生效
			
			sysPermissionService.updateSysPermissionBaseInfoById(sysPermission);
			//业务执行成功，页面提示
			setPromptMessage(view, "修改成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			log.error("编辑权限异常", e);
			setPromptException(view, e);
		}finally {
			//返回页面表单信息
			view.addObject("sysPermission", sysPermission);
			view.addObject("parentId", parentId);
		}
		return view;
	}
	
	@RequestMapping(value="/addPermissionTree.do",method=RequestMethod.POST)
	public ModelAndView addPerimissionTree_methodPost(SysPermissionDo sysPermission,Integer parentId)
	{
		ModelAndView view=new ModelAndView("/system/editPermissionTree");
		try{
			//权限验证
			checkPermission(PermissionCode.system_permission_add);
			//返回页面表单信息
			view.addObject("sysPermission", sysPermission);
		
			//表单信息过滤
			checkEmpty(sysPermission.getName(),"名称不能为空");
			Integer dataType= sysPermission.getDataType();
			if(dataType!=null)
			{
				if(dataType.equals(1))
				{
					checkEmpty(sysPermission.getUrl(),"菜单链接地址不能为空");
				}else if(dataType.equals(2))
				{
					checkEmpty(sysPermission.getPerCode(),"权限代码不能为空");
				}
			} 
			//保存表单信息
			sysPermission.setAvailable(1);//默认生效
			if(sysPermission.getParentId()==null)
			{
				sysPermission.setParentId(0L);
			}
			sysPermission.setId(null);
			sysPermissionService.insertSysPermissionByUser(sysPermission,currentUser());
			//业务执行成功，页面提示
			setPromptMessage(view, "保存成功");
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			log.error("新增权限资源异常", e);
			setPromptException(view, e);
		}finally {
			//返回页面表单信息
			view.addObject("sysPermission", sysPermission);
			view.addObject("parentId", parentId);
		}
		return view;
	}
	/*** end **/
	
	
	@RequestMapping("/menuView.do")
	public ModelAndView menuView(HttpServletRequest request){
		ModelAndView model = new ModelAndView("system/menuView");
		return model;
	}
	
	@RequestMapping(value = "/menuData.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String menuJsonData()
	{
		try{
			JSONArray result=new JSONArray();
			List<JSONObject> list=sysPermissionService.jsonObjectChildMenuListByUser(null,currentUser());
			if(list!=null)
			{
				log.info("获取总数据记录数有："+list.size());
				for(JSONObject item:list)
				{
					result.add(item);
				}
			}
			JSONObject root=new JSONObject();
			root.put("title", "系统菜单");
			root.put("children", result);
			root.put("dataType", 3);
			root.put("open", true);
			root.put("url", "javascript:menuUrl(0)");
			String jsonData=root.toJSONString();
			log.info("菜单数据："+jsonData);
			return jsonData;
		}catch (Exception e) {
			log.error("获取JSON菜单数据异常", e);
		}
		return null;
		
	}
	
	@RequestMapping(value="/deletePermission.do")
	public JsonView<String> deletePerimission(Long id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.system_permission_delete);
			checkEmpty(id, "参数无效");
			sysPermissionService.deleteSysPermissionById(id);
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			log.error("新增权限资源异常",e);
			setPromptException(view, e);
		}
		return view;
	}
	
	
}
