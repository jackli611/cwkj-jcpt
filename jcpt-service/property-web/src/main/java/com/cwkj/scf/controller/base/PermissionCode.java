package com.cwkj.scf.controller.base;

/**
 * 权限编号
 * @author ljc
 * @version 1.0
 */
public  class PermissionCode {

	/**
	 * 系统新增角色
	 */
	public static final String system_role_add="system_role_add";
	/**
	 * 系统角色编辑
	 */
	public static final String system_role_edit="system_role_edit";
	/**
	 * 系统角色删除
	 */
	public static final String system_role_delete="system_role_delete";
	
	/** 重置密码 */
	public static final String system_user_password_reset = "system_user_password_reset";
	
	/**
	 * 系统角色列表
	 */
	public static final String system_role_list="system_role_list";
	
	/**
	 * 系统权限资源编辑
	 */
	public static final String system_permission_edit="system_permission_edit";
	/**
	 * 系统权限资源新增
	 */
	public static final String system_permission_add="system_permission_add";
	/**
	 * 系统权限资源删除
	 */
	public static final String system_permission_delete="system_permission_delete";
	/**
	 * 系统权限资源列表
	 */
	public static final String system_permission_list="system_permission_list";
	/**
	 * 系统添加用户
	 */
	public static final String system_user_add="system_user_add";
	/**
	 * 系统修改用户
	 */
	public static final String system_user_edit="system_user_edit";
	/**
	 * 系统查询用户列表
	 */
	public static final String system_user_list="system_user_list";
	/** 系统用户锁定 */
	public static final String system_user_lock="system_user_lock";
	/** 系统用户注销 */
	public static final String system_user_delete="system_user_delete";
	/**
	 * 系统用户角色配置
	 */
	public static final String system_user_role_setting="system_user_role_setting";
 
	/** 
	 * 系统角色权限配置
	 */
	public static final String system_role_permission_edit="system_role_permission_edit";
	
	/** 调度系统定时任务触发操作  */
	public static final String system_schedule_trigger="system_schedule_trigger";
	
	
	/**
	 * 系统数据字典列表
	 */
	public static final String SYSTEM_DICT_LIST="system_dict_list";
	
	/**
	 * 系统数据字典新增
	 */
	public static final String SYSTEM_DICT_ADD="system_dict_add";
	
	/**
	 * 系统数据字典修改
	 */
	public static final String SYSTEM_DICT_EDIT="system_dict_edit";
	
	/**
	 * 系统数据字典删除
	 */
	public static final String SYSTEM_DICT_DELETE="system_dict_delete";
	
	/**
	 * 系统数据字典更新缓存
	 */
	public static final String SYSTEM_DICT_CACHE = "system_dict_cache";
	/**
	 * 系统参数列表
	 */
	public static final String SYSTEM_PARAM_LIST="system_param_list";
	
	/**
	 * 系统参数新增
	 */
	public static final String SYSTEM_PARAM_ADD="system_param_add";
	/**
	 * 系统参数修改
	 */
	public static final String SYSTEM_PARAM_EDIT="system_param_edit";
	/**
	 * 系统参数删除
	 */
	public static final String SYSTEM_PARAM_DELETE="system_param_delete";
	/**
	 * 回还款方案列表
	 */
	public static final String SYSTEM_SCHEME_LIST="system_scheme_list";
	
	/** 业务锁列表 */
	public static final String SYSTEM_BIZLOCK_LIST="system_bizlock_list";
	/** 业务锁删除 */
	public static final String SYSTEM_BIZLOCK_DEL="system_bizlock_del";
	
	/** 查询供应商列表 */
	public static final String SUPPLIER_LIST_QUERY="supplier_list_query";
	
	public static final String SUPPLIER_ADD="supplier_add";
	public static final String SUPPLIER_EDIT="supplier_edit";
	public static final String SUPPLIER_DELETE="supplier_delete";
	public static final String SUPPLIER_VIEW="supplier_view";
	
	public static final String COMPANY_LIST_QUERY="company_list_query";
	public static final String COMPANY_ADD="company_add";
	public static final String COMPANY_EDIT="company_edit";	                   
	public static final String COMPANY_DELETE="company_delete";	                   
	public static final String COMPANY_VIEW="company_view";	      
	
	public static final String PAYINFO_LIST_QUERY="payinfo_list_query";
	public static final String PAYINFO_ADD="payinfo_add";
	public static final String PAYINFO_EDIT="payinfo_edit";
	public static final String PAYINFO_VIEW="payinfo_view";
	public static final String PAYINFO_DELETE="payinfo_delete";
	
	public static final String PROPERTYORG_LIST_QUERY="propertyorg_list_query";
	public static final String PROPERTYORG_ADD="propertyorg_add";
	public static final String PROPERTYORG_EDIT="propertyorg_edit";
	public static final String PROPERTYORG_VIEW="propertyorg_view";
	public static final String PROPERTYORG_DELETE="propertyorg_delete";
	
	public static final String PROPERTYUSER_LIST_QUERY="propertyuser_list_query";
	public static final String PROPERTYUSER_ADD="propertyuser_add";
	public static final String PROPERTYUSER_EDIT="propertyuser_edit";
	public static final String PROPERTYUSER_VIEW="propertyuser_view";
	public static final String PROPERTYUSER_DELETE="propertyuser_delete";
	
	public static final String FINANCEORG_LIST_QUERY="financeorg_list_query";
	public static final String FINANCEORG_ADD="financeorg_add";
	public static final String FINANCEORG_EDIT="financeorg_edit";
	public static final String FINANCEORG_VIEW="financeorg_view";
	public static final String FINANCEORG_DELETE="financeorg_delete";
}
