package com.cwkj.scf.controller.loanorder;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.scf.controller.base.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.scf.controller.base.JsonView;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.Orderaudit;
import com.cwkj.scf.service.loan.ILoanOrderService;

/**
 * 借款订单审批
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/audit/")
public class LoanOrderAuditController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(LoanOrderAuditController.class);
	
	@Autowired
	private ILoanOrderService loanOrderSercice;
	
	
	
	
	/**
	 * 订单审批
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/audit.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView audit(HttpServletRequest request,Long loanId,String auditStep)
	{
		ModelAndView view=new ModelAndView("loanorder/audit");
		try{ 
			logger.info("audit order id :"+loanId);
			view.addObject("loanid", loanId);
			view.addObject("step", auditStep);
			
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("订单审批异常", e);
		}
		return view;
	}
	
	/**
	 * 保存订单申请第一步
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/saveAudit.do")
	public ModelAndView  saveAudit(HttpServletRequest request,
								   Orderaudit audit){
		
		ModelAndView view=new ModelAndView("loadmodel/loan_order_list");
		try{ 

			SysUserDo currentUser  = this.currentUser();
			audit.setAuditusername(currentUser.getUserName());
			audit.setUserid(Long.valueOf(currentUser.getId()));
			Integer result= loanOrderSercice.auditOrder(audit);
			AssertUtils.isTrue(result.equals(1),"审批出错");
			setPromptMessage(view,  "审批成功");
		
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("订单审批异常", e);
		}
		return view;
		
	}
	
	
	/**
	 * 	审批 json返回
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/saveAuditByAjx.do")
	public JsonView<Loanorder> saveAuditByAjx(HttpServletRequest request,Orderaudit audit)
	{
		JsonView<Loanorder> view=  JsonView.successJsonView();
		try{
			SysUserDo currentUser  = this.currentUser();
			audit.setUserid(Long.valueOf(currentUser.getId()));
			Integer result= loanOrderSercice.auditOrder(audit);
			AssertUtils.isTrue(result.equals(1),"审批出错");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	 
}
