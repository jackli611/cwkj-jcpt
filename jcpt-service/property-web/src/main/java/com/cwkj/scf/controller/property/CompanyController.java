package com.cwkj.scf.controller.property;

import java.io.File;
import java.util.Map;

import com.cwkj.scf.controller.base.BaseAction;
import com.cwkj.scf.controller.base.HttpServletResponseUtil;
import com.cwkj.scf.controller.base.JsonView;
import com.cwkj.scf.controller.base.PermissionCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.CompanyDo;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.service.config.CommonConfigService;
import com.cwkj.scf.service.property.CompanyService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;

/**
 * 企业.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class CompanyController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(CompanyController.class);
	
	private CompanyService companyService;
	@Autowired
	private CommonConfigService commonConfigService;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	
	@Autowired
	public void setCompanyService(CompanyService companyService)
	{
		this.companyService=companyService;
	}
	
	private String storeFileRootPath()
	{
		return commonConfigService.getUploadFileConfig().getStoreRootPath();
	}
	
	
	/**
	 * 企业数据分页
	 * @return
	 */
	@RequestMapping(value="/companyList.do",method=RequestMethod.GET)
	public ModelAndView companyList_methodGet()
	{
		ModelAndView view=new ModelAndView("property/companyList");
		return view;
	}
	
	/**
	 * 企业数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/companyList.do",method=RequestMethod.POST)
	public ModelAndView companyList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/companyList_table");
		try{ 
			checkPermission(PermissionCode.COMPANY_LIST_QUERY);
			PropertyUserDo propertyUser= currentPropertyUser(true,null);
			 Map<String, Object> selectItem=getRequestToParamMap(request);
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 String levelsStr=request.getParameter("levelsStr");
			 
			 if(StringUtil.isNotBlank(levelsStr))
			 {
				 selectItem.put("levels",Integer.valueOf(levelsStr));
			 }
			  
			 String[] statusArr=request.getParameterValues("statusArr");
			 if(statusArr!=null && statusArr.length>0)
			 {
				 Integer[] statusIntArr=new Integer[statusArr.length];
				 for(int i=0,k=statusArr.length;i<k;i++)
				 {
					 statusIntArr[i]=Integer.valueOf(statusArr[i]);
				 }
				 selectItem.put("approvalStatusArr", statusIntArr);
			 }else {
				 selectItem.put("approvalStatusArr", null);
			 }
			 selectItem.put("propertyId", propertyUser.getPropertyId());
			PageDo<CompanyDo> pagedata= companyService.queryCompanyListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取企业数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增企业
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addCompany.do",method=RequestMethod.GET)
	public ModelAndView addCompany_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_ADD);
			view.addObject("company", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加企业异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增企业
	 * @param request
	 * @param company
	 * @return
	 */
	@RequestMapping(value="/addCompany.do",method=RequestMethod.POST)
	public ModelAndView addCompany_methodPost(HttpServletRequest request,CompanyDo company)
	{
		ModelAndView view=new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_ADD);
			AssertUtils.isNotBlank(company.getNames(), "企业名称不能为空");
			AssertUtils.isNotBlank(company.getMobile(), "手机号不能为空");
			AssertUtils.isNotBlank(company.getLegal(), "企业法人不能为空");
			AssertUtils.isNotBlank(company.getLegalIdNo(), "企业法人身份证号不能为空");
			AssertUtils.isNotBlank(company.getOrganizationCode(), "组织机构代码不能为空");
			company.setRecordUserId(currentUser().getId());
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				company.setLevels(Integer.valueOf(levelsStr));
			}
			company.setApprovalStatus(CompanyDo.APPROVALSTATUS_INIT);
			company.setStatus(CompanyDo.STATUS_VALID);
			Integer result= companyService.insertCompany(company);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加企业异常（POST）",e);
		}finally {
			view.addObject("company", company);
		}
		return view;
	}
	
	 /**
	 * 浏览企业
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewCompany.do",method=RequestMethod.GET)
	public ModelAndView viewCompany_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/viewCompany");
		try{
			checkPermission(PermissionCode.COMPANY_VIEW);
			CompanyDo company= companyService.findCompanyById(id);
			view.addObject("company", company);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览企业异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑企业
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editCompany.do",method=RequestMethod.GET)
	public ModelAndView editCompany_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_EDIT);
			CompanyDo company= companyService.findCompanyById(id);
			view.addObject("company", company);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑企业异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑企业
	 * @param request
	 * @param company
	 * @return
	 */
	@RequestMapping(value="/editCompany.do",method=RequestMethod.POST)
	public ModelAndView editCompany_methodPost(HttpServletRequest request,CompanyDo  company)
	{
		ModelAndView view =new ModelAndView("property/addCompany");
		try{
			checkPermission(PermissionCode.COMPANY_EDIT);
			AssertUtils.isNotBlank(company.getId(), "参数无效");
			AssertUtils.isNotBlank(company.getNames(), "企业名称不能为空");
			AssertUtils.isNotBlank(company.getMobile(), "手机号不能为空");
			AssertUtils.isNotBlank(company.getLegal(), "企业法人不能为空");
			AssertUtils.isNotBlank(company.getLegalIdNo(), "企业法人身份证号不能为空");
			AssertUtils.isNotBlank(company.getOrganizationCode(), "组织机构代码不能为空");
			company.setRecordUserId(currentUser().getId());
			String levelsStr=request.getParameter("levelsStr");
			if(StringUtil.isNotBlank(levelsStr))
			{
				company.setLevels(Integer.valueOf(levelsStr));
			}
			company.setApprovalStatus(CompanyDo.APPROVALSTATUS_INIT);
			Integer result= companyService.updateCompanyById(company);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑企业异常（GET）",e);
		}finally {
			view.addObject("company", company);
		}
		return view;
	}
	
	/**
	 * 删除企业
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deleteCompany.do")
	public JsonView<String> deleteCompany_methodPost(HttpServletRequest request, String id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.COMPANY_DELETE);
			AssertUtils.isNotBlank(id, "参数无效");
			Integer result=companyService.deleteCompanyById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除企业异常",e);
		}
		return view;
	}
	
	
	/**
	 * 浏览物业机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/uploadCompanyBLFile.do",method=RequestMethod.GET)
	public ModelAndView uploadCompanyBLFile_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/uploadCompanyBLFile");
		try{
			CompanyDo company= companyService.findCompanyById(id);
			view.addObject("company", company);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览物业机构异常（GET）",e);
		}
		return view;
	}
	
	@RequestMapping(value="/uploadCompanyBLFile.do",method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String uploadCompanyBLFile_methodPost(@RequestParam(value = "file", required = false) MultipartFile file,HttpServletRequest request,String id)
	{
//		JsonView<String> view=null;
		JSONObject result=new JSONObject();
		try{
			AssertUtils.notNull(file,"上传图片文件不能为空");
		 
			String imageDirName="companyBLFile/";
				String path=storeFileRootPath()+imageDirName;
				 String saveFileName=sysSeqNumberService.daySeqnumberString("BL");
				 int pointIndex=file.getOriginalFilename().lastIndexOf(".");
			        if(pointIndex>0)
			        {
			        	saveFileName+=file.getOriginalFilename().substring( pointIndex);
			        }
				 File targetFile = new File(path, saveFileName);  
			        if(!targetFile.exists()){  
			            targetFile.mkdirs();  
			        }   
			        //保存  
	      file.transferTo(targetFile);  
	      Integer dbRes=companyService.updateBusinessLicense(id, imageDirName+saveFileName);
	      AssertUtils.isTrue(dbRes!=null && dbRes>0,"更新失败");
	      result.put("result", "ok");
	      result.put("message", "操作成功");
		}catch (BusinessException e) {
			logger.info("上传营业执照失败:{}",e.getMessage());
			 result.put("result", "failed");
		      result.put("message", e.getMessage());
		}catch (Exception e) {
			logger.error("上传营业执照异常",e);
			 result.put("result", "failed");
		      result.put("message", e.getMessage());
		}
		return result.toString();
	}
	
	/**
	 * 获取图片（营业执照）
	 * @param request
	 * @param response
	 * @param id
	 */
	@RequestMapping("/companyBLFile.do")
	public void businessLicenseFile(HttpServletRequest request,HttpServletResponse response,String id)  
	{
		try {
			File file=null;
			CompanyDo company= companyService.findCompanyById(id);
			String contentType="JPEG";
			if(company!=null && StringUtil.isNotBlank(company.getBusinessLicense()))
			{
				file=new File(storeFileRootPath()+company.getBusinessLicense());
				String[] imgtype=company.getBusinessLicense().split(".");
				if(imgtype.length>1)
				{
				contentType=imgtype[imgtype.length-1];
				}
			}else {
				file=null;
			}
		
		if(file.exists()){
			//设置MIME类型
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setContentType("image/"+contentType);
			HttpServletResponseUtil.responseFile(response,file);
		}
		}catch (Exception e) {
			logger.error("获取营业执照异常",e);
		}
	} 
	 
}
