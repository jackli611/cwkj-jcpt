package com.cwkj.scf.controller.platform;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwkj.scf.controller.base.JsonView;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.*;
import com.cwkj.scf.controller.base.BaseAction;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.property.PropertyOrgService;
import com.cwkj.scf.service.property.SupplierInvitationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.constant.SystemConst;

/**
 * 平台
 * 
 * @author ljc
 * 
 */
@Controller
public class MainController extends BaseAction {

  private static Logger log = LoggerFactory.getLogger(MainController.class);
  @Autowired
  private SysPermissionService sysPermissionService;
  private SupplierInvitationService supplierInvitationService;
  @Autowired
  private SysSeqNumberService sysSeqNumberService;
  @Autowired
  private ParamService paramService;
  @Autowired
  private PropertyOrgService propertyOrgService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysAttachmentService sysAttachmentService;

    private static final  String userPhotoPath="/res/images/logo/default_user.png";




 private ModelAndView propertyModelAndView(HttpServletRequest request,   HttpServletResponse response)
 {
	 ModelAndView view= new ModelAndView("platform/main");
	 PropertyUserDo propertyUser=currentPropertyUser(true, null);
	 PropertyOrgDo propertyOrg=propertyOrgService.findPropertyOrgById(propertyUser.getPropertyId());
		view.addObject("propertyUser", propertyUser);
		view.addObject("propertyOrg", propertyOrg);
	return view;
 }
 
 private ModelAndView supplierModelAndView(HttpServletRequest request,   HttpServletResponse response)
 {
	 ModelAndView view= new ModelAndView("platform/main_supplier");
	 SupplierDo supplier=currentSupplier(true, null);
		view.addObject("supplier", supplier);
	return view;
 }

  @RequestMapping("/main.do")
  public ModelAndView index(HttpServletRequest request,
                            HttpServletResponse response) {
    ModelAndView view = new ModelAndView("platform/main_refuse");

    try {
        String photoPath = userPhotoPath;
        String tag = request.getParameter("tag");
        if (hasRole(SystemConst.SYS_ROLE_PROPERTYS_NAME) && hasRole(SystemConst.SYS_ROLE_SUPPLIER_NAME))//同时拥有供应商和核心企业的角色，不允许登录，因为业务中判断当前角色会出问题
        {
            return view;
        } else {
            if (hasRole(SystemConst.SYS_ROLE_PROPERTYS_NAME)) {
                view = propertyModelAndView(request, response);
            } else if (hasRole(SystemConst.SYS_ROLE_SUPPLIER_NAME)) {
                view = supplierModelAndView(request, response);
            } else {

                view = new ModelAndView("platform/main_refuse");
                throw new BusinessException("无权限问题");
            }
        }
        SysUserDo sysUserDo = sysUserService.findSysUserById(currentUser().getId());
        String userPhoto = sysUserDo.getImg();

        if (StringUtil.isNotBlank(userPhoto)) {
            photoPath = "/attachment.do?id=" + userPhoto;

        }
        view.addObject("sysUserPhoto", photoPath);
    }catch (BusinessException ex)
    {
        setPromptException(view, ex);
    } catch (Exception e) {
        logger.error("使用主界面异常：",e);
      setPromptException(view, e);

    }
    
    return view;
  }
  


  @RequestMapping(value="/loadmodel.do", method= RequestMethod.GET)
  public ModelAndView resetPasswordPage(String id)
  {
    ModelAndView modelView = new ModelAndView("loadmodel/"+id);
    return modelView;
  }

    @RequestMapping(value="/userPhotoChange.do",method= RequestMethod.POST)
    public JsonView<String> userPhotoChange_methodPost(HttpServletRequest request, HttpServletResponse response, String photo)
    {
        JsonView<String> view=new JsonView<String>();
        try{
            String img=request.getParameter("photo");
            AssertUtils.isNotBlank(img,"参数无效");
            SysUserDo sysUserDo=new SysUserDo();
            sysUserDo.setImg(img);
            sysUserDo.setId(currentUser().getId());
            Integer dbRes= sysUserService.updateUserPhoto(sysUserDo);
            AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存失败");
            //更新附件使用状态。
            sysAttachmentService.updateCheckedStatusPass(Long.valueOf(img),currentUser().getId());
            setPromptMessage(view, JsonView.CODE_SUCCESS,"保存成功");
            view.setData(img);
        }catch(BusinessException e)
        {
            setPromptException(view, e);
        }catch (Exception e) {
            logger.error("修改用户头像异常", e);
            setPromptException(view, e);
        }
        return view;
    }

}
