package com.cwkj.scf.controller.agreement;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.scf.controller.base.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BusinessException;

/**
 * 协议处理类
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/agreement/")
public class AgreementController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(AgreementController.class);
	
	/**
	 * 	查看借款协议模板
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/showProtocolTemplate.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView showProtocolTemplate(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("agreement/protocol_template");
		try{ 
			
		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取查看借款协议模板异常", e);
		}
		return view;
	}
	 
}
