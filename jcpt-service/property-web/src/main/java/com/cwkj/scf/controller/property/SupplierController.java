package com.cwkj.scf.controller.property;

import java.util.List;
import java.util.Map;

import com.cwkj.scf.controller.base.BaseAction;
import com.cwkj.scf.controller.base.JsonView;
import com.cwkj.scf.controller.base.PermissionCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
 
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.model.property.SupplierAuditDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.property.SupplierInvitationService;
import com.cwkj.scf.service.property.SupplierService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysUserDo;

/**
 * 供应商.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class SupplierController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(SupplierController.class);
	
	private SupplierService supplierService;
	@Autowired
	private SupplierInvitationService supplierInvitationService;
	
	@Autowired
	public void setSupplierService(SupplierService supplierService)
	{
		this.supplierService=supplierService;
	}
	
	/**
	 * 供应商数据分页
	 * @return
	 */
	@RequestMapping(value="/supplierList.do",method=RequestMethod.GET)
	public ModelAndView supplierList_methodGet()
	{
		ModelAndView view=new ModelAndView("property/supplierList");
		return view;
	}
	
	/**
	 * 供应商数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/supplierList.do",method=RequestMethod.POST)
	public ModelAndView supplierList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/supplierList_table");
		try{ 
			checkPermission(PermissionCode.SUPPLIER_LIST_QUERY);
			PropertyUserDo propertyUser= currentPropertyUser(true,null);
			AssertUtils.notNull(propertyUser,"权限错误");
			 Map<String, Object> selectItem=getRequestToParamMap(request);
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 String[] statusArr=request.getParameterValues("statusArr");
			 if(statusArr!=null && statusArr.length>0)
			 {
				 Integer[] statusIntArr=new Integer[statusArr.length];
				 for(int i=0,k=statusArr.length;i<k;i++)
				 {
					 statusIntArr[i]=Integer.valueOf(statusArr[i]);
				 }
				 selectItem.put("statusArr", statusIntArr);
			 }else {
				 selectItem.put("statusArr", null);
			 }
			 selectItem.put("propertyId", propertyUser.getPropertyId());
			 selectItem.put("platformStatus",SupplierDo.PLATFORMSTATUS_APPROVED);
			PageDo<SupplierDo> pagedata= supplierService.querySupplierListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取供应商数据分页异常", e);
		}
		return view;
	}
	 
	
	 /**
	 * 浏览供应商
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewSupplier.do",method=RequestMethod.GET)
	public ModelAndView viewSupplier_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/viewSupplier");
		try{
			checkPermission(PermissionCode.SUPPLIER_VIEW);
			SupplierDo supplier= supplierService.findSupplierById(id);
			List<SupplierAuditDo> supplierAuditList=supplierService.findSupplierAuditList(id);
			view.addObject("supplier", supplier);
			view.addObject("supplierAuditList", supplierAuditList);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览供应商异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑供应商
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editSupplier.do",method=RequestMethod.GET)
	public ModelAndView editSupplier_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/addSupplier");
		try{
			checkPermission(PermissionCode.SUPPLIER_EDIT);
			SupplierDo supplier= supplierService.findSupplierById(id);
			view.addObject("supplier", supplier);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑供应商异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑供应商
	 * @param request
	 * @param supplier
	 * @return
	 */
	@RequestMapping(value="/editSupplier.do",method=RequestMethod.POST)
	public ModelAndView editSupplier_methodPost(HttpServletRequest request,SupplierDo  supplier)
	{
		ModelAndView view =new ModelAndView("property/addSupplier");
		try{
			checkPermission(PermissionCode.SUPPLIER_EDIT);
			AssertUtils.isNotBlank(supplier.getId(), "参数无效");
			Integer result= supplierService.updateSupplierById(supplier);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑供应商异常（GET）",e);
		}finally {
			view.addObject("supplier", supplier);
		}
		return view;
	}
	

	
	/**
	 * 审核供应商
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/auditSupplier.do",method=RequestMethod.GET)
	public ModelAndView auditSupplier_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("property/auditSupplier");
		try{
			String supplierId= request.getParameter("supplierId");
			view.addObject("supplierId", supplierId);
			SupplierDo supplier= supplierService.findSupplierById(supplierId);
			List<SupplierAuditDo> supplierAuditList=supplierService.findSupplierAuditList(supplierId);
			view.addObject("supplier", supplier);
			view.addObject("supplierAuditList", supplierAuditList);
			view.addObject("currentOropertyUser", currentPropertyUser(true,null));
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加供应商异常（GET）",e);
		}
		return view;
	}
	 
	/**
	 * 修改供应商合作状态
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/auditSupplier.do",method=RequestMethod.POST)
	public JsonView<String> supplierStatus_methodPost(HttpServletRequest request,
                                                      String supplierId,
                                                      Integer status,
                                                      String remark,
                                                      String level)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.isNotBlank(supplierId, "参数无效[error:001]");
			AssertUtils.notNull(status, "参数无效[error:002]");
			AssertUtils.notNull(level, "供应商等级不能为空");
			SupplierAuditDo supplierAudit=new SupplierAuditDo();
			SupplierDo supplier=supplierService.findSupplierById(supplierId);
			supplierAudit.setPropertyId(supplier.getPropertyId());
			supplierAudit.setSupplierId(supplierId);
			supplierAudit.setRecordUserId(currentUser().getId());
			supplierAudit.setStatus(status);
			supplierAudit.setRemark(remark);
			supplierAudit.setLevel(level);
			Integer result=supplierService.auditSupplierStatus(supplierAudit);
			AssertUtils.isTrue(result.equals(1),"修改失败:[error:003]");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("修改供应商合作状态异常",e);
		}
		return view;
	}
	
	

	/**
	 * 获取当前登录用户所属供应商
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getSupplierByCurrentUser.do")
	public JsonView<SupplierDo> getSupplierByCurrentUser(HttpServletRequest request)
	{
		JsonView<SupplierDo> view= JsonView.successJsonView();
		
		try{
			SysUserDo currentUser  = this.currentUser();
			SupplierDo supplier = supplierService.findSupplierByUserId(currentUser.getId());
			
			if(null != supplier) {
				view.setData(supplier);
			}else {
				view = JsonView.failureJsonView("没有找到供应商联系人");
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取供应商联系人",e);
		}
		return view;
	}
	
	
}
