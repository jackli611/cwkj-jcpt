package com.cwkj.scf.controller.platform;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.scf.controller.base.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 平台
 * 
 * @author ljc
 * 
 */
@Controller
@RequestMapping("/platform/")
public class DashboardController extends BaseAction {

  private static Logger log = LoggerFactory.getLogger(DashboardController.class);
  
	@RequestMapping(value="/dashboard.do",method=RequestMethod.GET)
	public ModelAndView dashboard_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("platform/dashboard");
		try{
			view.addObject("propertyOrg", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业机构异常（GET）",e);
		}
		return view;
	}
}
