package com.cwkj.scf.controller.property;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwkj.scf.controller.base.BaseAction;
import com.cwkj.scf.controller.base.HttpServletResponseUtil;
import com.cwkj.scf.controller.base.JsonView;
import com.cwkj.scf.controller.base.PermissionCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.model.property.PropertyFinancesDo;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.config.CommonConfigService;
import com.cwkj.scf.service.products.ProductService;
import com.cwkj.scf.service.property.PropertyOrgService;
import com.cwkj.scf.service.property.SupplierService;

/**
 * 物业机构.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class PropertyOrgController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(PropertyOrgController.class);
	
	private PropertyOrgService propertyOrgService;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	@Autowired
	private CommonConfigService commonConfigService;
	@Autowired
	private ProductService productService;
	
	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	public void setPropertyOrgService(PropertyOrgService propertyOrgService)
	{
		this.propertyOrgService=propertyOrgService;
	}
	
	private String storeFileRootPath()
	{
		return commonConfigService.getUploadFileConfig().getStoreRootPath();
	}
	
	/**
	 * 物业机构数据分页
	 * @return
	 */
	@RequestMapping(value="/propertyOrgList.do",method=RequestMethod.GET)
	public ModelAndView propertyOrgList_methodGet()
	{
		ModelAndView view=new ModelAndView("property/propertyOrgList");
		return view;
	}
	
	/**
	 * 物业机构数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/propertyOrgList.do",method=RequestMethod.POST)
	public ModelAndView propertyOrgList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/propertyOrgList_table");
		try{ 
			checkPermission(PermissionCode.PROPERTYORG_LIST_QUERY);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<PropertyOrgDo> pagedata= propertyOrgService.queryPropertyOrgListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取物业机构数据分页异常", e);
		}
		return view;
	}
	

	
	 /**
	 * 浏览物业机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewPropertyOrg.do",method=RequestMethod.GET)
	public ModelAndView viewPropertyOrg_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/viewPropertyOrg");
		try{
			PropertyOrgDo propertyOrg= propertyOrgService.findFullPropertyOrgById(currentPropertyUser(true, null).getPropertyId());
			view.addObject("propertyOrg", propertyOrg);
			view.addObject("currentPropertyUser",currentPropertyUser(false,null));
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览物业机构异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑物业机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editPropertyOrg.do",method=RequestMethod.GET)
	public ModelAndView editPropertyOrg_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/addPropertyOrg");
		try{
			checkPermission(PermissionCode.PROPERTYORG_EDIT);
			PropertyOrgDo propertyOrg= propertyOrgService.findPropertyOrgById(id);
			view.addObject("propertyOrg", propertyOrg);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业机构异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑物业机构
	 * @param request
	 * @param propertyOrg
	 * @return
	 */
	@RequestMapping(value="/editPropertyOrg.do",method=RequestMethod.POST)
	public ModelAndView editPropertyOrg_methodPost(HttpServletRequest request,PropertyOrgDo  propertyOrg)
	{
		ModelAndView view =new ModelAndView("property/addPropertyOrg");
		try{
			checkPermission(PermissionCode.PROPERTYORG_EDIT);
			AssertUtils.isNotBlank(propertyOrg.getId(), "参数无效");
			Integer result= propertyOrgService.updatePropertyNameInfo(propertyOrg);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业机构异常（GET）",e);
		}finally {
			view.addObject("propertyOrg", propertyOrg);
		}
		return view;
	}
	
	/**
	 * 删除物业机构
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/propertyOrgStatus.do")
	public JsonView<String> propertyOrgStatus_methodPost(HttpServletRequest request, String id, Integer status)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkPermission(PermissionCode.PROPERTYORG_DELETE);
			AssertUtils.isNotBlank(id, "参数无效[error:001]");
			AssertUtils.notNull(status, "参数无效[error:002]");
			Integer result=propertyOrgService.updateStatus(id, status);
			AssertUtils.isTrue(result.equals(1),"修改失败:[error:003]");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除物业机构异常",e);
		}
		return view;
	}
	
	 /**
		 * 浏览物业机构
		 * @param request
		 * @param id
		 * @return
		 */
		@RequestMapping(value="/uploadBusinessLicense.do",method=RequestMethod.GET)
		public ModelAndView uploadBusinessLicense_methodGet(HttpServletRequest request,String id)
		{
			ModelAndView view =new ModelAndView("property/uploadBusinessLicense");
			try{
				checkPermission(PermissionCode.PROPERTYORG_VIEW);
				PropertyOrgDo propertyOrg= propertyOrgService.findPropertyOrgById(id);
				view.addObject("propertyOrg", propertyOrg);
			}catch (BusinessException e) {
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("浏览物业机构异常（GET）",e);
			}
			return view;
		}
		
		@RequestMapping(value="/uploadBusinessLicense.do",method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		@ResponseBody
		public String uploadExcelFile_methodPost(@RequestParam(value = "file", required = false) MultipartFile file,HttpServletRequest request,String id)
		{
//			JsonView<String> view=null;
			JSONObject result=new JSONObject();
			try{
				AssertUtils.notNull(file,"上传图片文件不能为空");
			 
				String imageDirName="businessLicense/";
					String path=storeFileRootPath()+imageDirName;
					 String saveFileName=sysSeqNumberService.daySeqnumberString("BL");
					 int pointIndex=file.getOriginalFilename().lastIndexOf(".");
				        if(pointIndex>0)
				        {
				        	saveFileName+=file.getOriginalFilename().substring( pointIndex);
				        }
					 File targetFile = new File(path, saveFileName);  
				        if(!targetFile.exists()){  
				            targetFile.mkdirs();  
				        }   
				        //保存  
		      file.transferTo(targetFile);  
		      Integer dbRes=propertyOrgService.updateBusinessLicense(id, imageDirName+saveFileName);
		      AssertUtils.isTrue(dbRes!=null && dbRes>0,"更新失败");
		      result.put("result", "ok");
		      result.put("message", "操作成功");
			}catch (BusinessException e) {
				logger.info("上传营业执照失败:{}",e.getMessage());
				 result.put("result", "failed");
			      result.put("message", e.getMessage());
			}catch (Exception e) {
				logger.error("上传营业执照异常",e);
				 result.put("result", "failed");
			      result.put("message", e.getMessage());
			}
			return result.toString();
		}
		
		/**
		 * 获取图片（营业执照）
		 * @param request
		 * @param response
		 * @param id
		 */
		@RequestMapping("/businessLicenseFile.do")
		public void businessLicenseFile(HttpServletRequest request,HttpServletResponse response,String id)  
		{
			try {
				File file=null;
				PropertyOrgDo propertyOrg=propertyOrgService.findPropertyOrgById(id);
				String contentType="JPEG";
				if(propertyOrg!=null && StringUtil.isNotBlank(propertyOrg.getBusinessLicense()))
				{
					file=new File(storeFileRootPath()+propertyOrg.getBusinessLicense());
					String[] imgtype=propertyOrg.getBusinessLicense().split(".");
					if(imgtype.length>1)
					{
					contentType=imgtype[imgtype.length-1];
					}
				}else {
					file=null;
				}
			
			if(file.exists()){
				//设置MIME类型
				response.setHeader("Pragma", "no-cache");
				response.setHeader("Cache-Control", "no-cache");
				response.setDateHeader("Expires", 0);
				response.setContentType("image/"+contentType);
				HttpServletResponseUtil.responseFile(response,file);
			}
			}catch (Exception e) {
				logger.error("获取营业执照异常",e);
			}
		} 
		
	 
		@RequestMapping(value="/linkProduct.do",method=RequestMethod.GET)
		public ModelAndView linkProduct_methodGet(HttpServletRequest request,String id)
		{
			ModelAndView view =new ModelAndView("property/linkProduct");
			try{
				checkPermission(PermissionCode.PROPERTYORG_EDIT);
				PropertyOrgDo propertyOrg= propertyOrgService.findFullPropertyOrgById(id);
				view.addObject("propertyOrg", propertyOrg);
				List<ProductDo> productList= productService.queryProductList(null);
				view.addObject("productList", productList);
			}catch (BusinessException e) {
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("编辑物业机构异常（GET）",e);
			}
			return view;
		}
		
		@RequestMapping(value="/propertyFinanceList.do",method=RequestMethod.GET)
		public ModelAndView propertyFinanceList_methodGet(HttpServletRequest request,String id)
		{
			ModelAndView view =new ModelAndView("property/propertyFinanceList");
			try{
				List<PropertyFinancesDo> propertyFinanceList= propertyOrgService.findPropertyFinanceList(currentPropertyUser(true, null).getPropertyId());
				view.addObject("propertyFinanceList", propertyFinanceList);
			}catch (BusinessException e) {
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("编辑物业机构异常（GET）",e);
			}
			return view;
		}
		

		/**
		 * 根据供应商找核心企业，查看核心企业已分配的产品
		 * @param request
		 * @param id
		 * @return
		 */
		@RequestMapping(value="/getProduct.do")
		public JsonView<PropertyorgProduct> getProduct(HttpServletRequest request)
		{
			JsonView<PropertyorgProduct> view= JsonView.successJsonView();
			
			try{
				SysUserDo currentUser  = this.currentUser();
				SupplierDo supplier = supplierService.findSupplierByUserId(currentUser.getId());
				
				PropertyorgProduct product = null;				
				if(supplier != null) {
					
					Map<String,Object> selectItem = new HashMap<String,Object>();
					selectItem.put("propertyOrgId", supplier.getPropertyId());
					selectItem.put("productCode", ProductEnum.PROD_SCF.getProductCode());
					selectItem.put("level", supplier.getLevel());
					
					List<PropertyorgProduct> productLst =  propertyOrgService.findProduct(selectItem);
					if(productLst.size() != 1) {
						JsonView.failureJsonView("核心企业产品不正确");
					}else {
						product = productLst.get(0);
					}
				}
				
				
				if(null != product) {
					view.setData(product);
				}else {
					view = JsonView.failureJsonView("核心企业没有配置产品");
				}
			}catch (BusinessException e) {
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("获取核心企业产品异常",e);
			}
			return view;
		}
		
		
		/**
		 * 根据核心企业，查看核心企业已配置的产品
		 * @param request
		 * @param id
		 * @return
		 */
		@RequestMapping(value="/getPropertyProduct.do")
		public JsonView<List<PropertyorgProduct>> getPropertyProduct(HttpServletRequest request)
		{
			JsonView<List<PropertyorgProduct>> view= JsonView.successJsonView();
			
			try{
				PropertyOrgDo propertyOrg= propertyOrgService.findFullPropertyOrgById(currentPropertyUser(true, null).getPropertyId());
				List<PropertyorgProduct> prodLst = propertyOrg.getPropertyorgProductList();
				if(prodLst != null) {
					view.setData(prodLst);
				}else {
					view = JsonView.failureJsonView("核心企业没有配置产品");
				}
			}catch (BusinessException e) {
				setPromptException(view, e);
			}catch (Exception e) {
				setPromptException(view, e);
				logger.error("获取核心企业产品异常",e);
			}
			return view;
		}
		
		
}
