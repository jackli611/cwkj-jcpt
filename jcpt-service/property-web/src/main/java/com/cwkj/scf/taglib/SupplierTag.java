package com.cwkj.scf.taglib;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.property.SupplierService;
 
/**
 * 供应商标签
 * @author ljc
 *
 */
public class SupplierTag extends TagSupport {

	/**
	 * serialVersionUID:序列化.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * springContext
	 */
	private WebApplicationContext springContext;

	/**
	 * 查询type类型
	 */
	private String type;

	/**
	 * 选中的值
	 */
	private String value = "";

	/**
	 * 排除不显示
	 */
	private String exclude = "";

	/**
	 * 是否为选择框
	 */
	private boolean selection;
	
	@SuppressWarnings("unchecked")
	@Override
	public int doEndTag() throws JspException {
		//获取spring上下文环境
		springContext = WebApplicationContextUtils
				.getWebApplicationContext(pageContext.getServletContext());
		//获取字典服务
		SupplierService supplierService = (SupplierService) springContext.getBean(SupplierService.class);
		List<SupplierDo> list =null;
		//从page域获取值
		list = (List<SupplierDo>) pageContext.getAttribute(type,PageContext.PAGE_SCOPE);
		if(list == null || list.size() == 0){
			list = supplierService.querySupplierList(null);
			//设置page域
			pageContext.setAttribute(type, list,PageContext.PAGE_SCOPE);
		}
		
		JspWriter out = pageContext.getOut();
		try {
			if (selection) {
				StringBuffer buffer = new StringBuffer("");
				String[] excludeValues = exclude.split(",");
				for (SupplierDo code : list) {
					for (String excludeValue : excludeValues) {
						if (!excludeValue.equals(code.getId())) {
							if (code.getId().equals(value)) {
								buffer.append("<option value='"
										+ code.getId() + "' selected='seleceted'>"
										+ code.getNames() + "</option>");
							} else {
								buffer.append("<option value='"
										+ code.getId() + "'>"
										+ code.getNames() + "</option>");
							}
						}
					}
				}
				out.print(buffer.toString());
			} else {
				for (SupplierDo code : list) {
					if (code.getId().equals(value)) {
						out.print(code.getNames());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return super.doEndTag();
	}

	/**
	 * 获取springContext
	 * 
	 * @return springContext springContext
	 */
	public WebApplicationContext getSpringContext() {
		return springContext;
	}

	/**
	 * 设置springContext
	 * 
	 * @param springContext
	 *            springContext
	 */
	public void setSpringContext(WebApplicationContext springContext) {
		this.springContext = springContext;
	}

	/**
	 * 获取查询type类型
	 * 
	 * @return type 查询type类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 设置查询type类型
	 * 
	 * @param type
	 *            查询type类型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 获取选中的值
	 * 
	 * @return value 选中的值
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 设置选中的值
	 * 
	 * @param value
	 *            选中的值
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 获取排除不显示
	 * 
	 * @return exclude 排除不显示
	 */
	public String getExclude() {
		return exclude;
	}

	/**
	 * 设置排除不显示
	 * 
	 * @param exclude
	 *            排除不显示
	 */
	public void setExclude(String exclude) {
		this.exclude = exclude;
	}

	/**  
	 * 获取是否为选择框  
	 * @return selection 是否为选择框  
	 */
	public boolean isSelection() {
		return selection;
	}

	/**  
	 * 设置是否为选择框  
	 * @param selection 是否为选择框  
	 */
	public void setSelection(boolean selection) {
		this.selection = selection;
	}


	

}
