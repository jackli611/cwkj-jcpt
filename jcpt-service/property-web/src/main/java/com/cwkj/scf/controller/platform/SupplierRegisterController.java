package com.cwkj.scf.controller.platform;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.scf.controller.base.BaseAction;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.model.property.SupplierInvitationDo;
import com.cwkj.scf.service.property.PropertyOrgService;
import com.cwkj.scf.service.property.SupplierInvitationService;
import com.cwkj.scf.service.property.SupplierService;

/**
 * @author ljc
 *
 */
@Controller
public class SupplierRegisterController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(SupplierRegisterController.class);
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private SupplierInvitationService supplierInvitationService;
	@Autowired
	private PropertyOrgService propertyOrgService;
	
	/**
	 * 新增供应商
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/registerSupplier.do",method=RequestMethod.GET)
	public ModelAndView addSupplier_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("platform/registerSupplier");
		try{
			
			String inviteCode= request.getParameter("inviteCode");
			String supplierId= request.getParameter("supplierId");
			SupplierDo supplier = null;
			if(StringUtil.isNotBlank((supplierId))){
				supplier = supplierService.findSupplierById(supplierId);
			}
			
			if(supplier == null) {
				supplier = new SupplierDo();
			}
			view.addObject("supplier", supplier);
			
			view.addObject("inviteCode", inviteCode);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加供应商异常（GET）",e);
		}
		return view;
	}


	
	@RequestMapping(value="/registerSupplierSuccess.do",method=RequestMethod.GET)
	public ModelAndView registerSupplierSuccess_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("platform/registerSupplierSuccess");
		try{
			String inviteCode= request.getParameter("inviteCode");
			String mobile= request.getParameter("mobile");
			String pwd= request.getParameter("pwd");
			view.addObject("inviteCode", inviteCode);
			view.addObject("mobile", mobile);
			view.addObject("pwd", pwd);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("注册供应商成功加载页面异常",e);
		}
		return view;
	}
	
	 /**
	 * 注册供应商
	 * @param request
	 * @param supplier
	 * @return
	 */
	@RequestMapping(value="/registerSupplier.do",method=RequestMethod.POST)
	public ModelAndView registerSupplier_methodPost(HttpServletRequest request,SupplierDo supplier)
	{
		ModelAndView view=new ModelAndView("platform/registerSupplier");
		try{
			String password=request.getParameter("password");
			String inviteCode=request.getParameter("inviteCode");
			AssertUtils.isNotBlank(supplier.getNames(), "供应商名称不能为空");
			AssertUtils.isNotBlank(supplier.getLegal(), "法人不能为空");
			AssertUtils.isNotBlank(supplier.getLegalIdNo(), "法人身份证号不能为空");
			AssertUtils.isNotBlank(supplier.getOrganizationCode(), "组织机构代码不能为空");
			AssertUtils.isNotBlank(supplier.getMobile(), "手机号不能为空");
			if(StringUtil.isBlank(supplier.getId())) {
				AssertUtils.isNotBlank(password, "注册密码不能为空");
			}
			
			if(StringUtil.isBlank(supplier.getId())) {
				SupplierInvitationDo supplierInvitation= supplierInvitationService.findByInviteCode(inviteCode);
				AssertUtils.notNull(supplierInvitation,"邀请码无效[error:001]");
				AssertUtils.isNotBlank(supplierInvitation.getPropertyId(),"邀请码无效[error:002]");
				PropertyOrgDo propertyOrg=propertyOrgService.findPropertyOrgById(supplierInvitation.getPropertyId());
				AssertUtils.notNull(propertyOrg,"邀请码无效[error:003]");
				supplier.setPropertyId(supplierInvitation.getPropertyId());
			}else {
				//重新提交修改
				supplier.setPlatformStatus(101);
				supplier.setStatus(SupplierDo.STATUS_INIT);
				
			}
			
			Integer result = null;
			if(StringUtil.isBlank(supplier.getId())) {
				result= supplierService.registerSupplier(supplier, password);
			}else {
				supplier.setUserId(this.currentUser().getId());
				result = supplierService.updateByPrimaryKeySelective(supplier);
			}
			AssertUtils.isTrue(result!=null && result>0, "注册失败");
			view=new ModelAndView("redirect:registerSupplierSuccess.do?mobile="+supplier.getMobile());
		}catch (BusinessException e) {
			logger.info("注册供应商失败:{}",e.getMessage());
			setPromptException(view, e);
		}catch (Exception e) {
			logger.error("添加供应商异常（POST）",e);
			setPromptMessage(view, "操作失败[error:001]");
		}finally {
			view.addObject("supplier",supplier);
		}
		return view;
	}
}
