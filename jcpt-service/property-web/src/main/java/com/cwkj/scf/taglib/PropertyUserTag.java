package com.cwkj.scf.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.service.property.PropertyUserService;
 
/**
 * 	物业用户
 * @author harry
 *
 */
public class PropertyUserTag extends TagSupport {

	/**
	 * serialVersionUID:序列化.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * springContext
	 */
	private WebApplicationContext springContext;

	/**
	 * 选中的值
	 */
	private String value = "";

	/**
	 * 排除不显示
	 */
	private String exclude = "";
	

	/**
	 * 是否为选择框
	 */
	private boolean selection;
	
	private final static String userKeyPrev = "p_user";
	
	private Integer getUserId() {
		Subject subject = SecurityUtils.getSubject();
		if(subject!=null && subject.getPrincipal()!=null )
		{
			return ((SysUserDo) subject.getPrincipal()).getId();
		}
		return null;
	}
	
	@Override
	public int doStartTag() throws JspException {
     
		Integer userId = getUserId();
		if(null == userId) {
			return TagSupport.SKIP_BODY;
		}
		
		//获取spring上下文环境
		springContext = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext());
		//获取服务
		PropertyUserService pUserService = (PropertyUserService) springContext.getBean(PropertyUserService.class);
		PropertyUserDo pUser =null;
		//从page域获取值
		pUser = (PropertyUserDo) pageContext.getAttribute(userKeyPrev+userId,PageContext.PAGE_SCOPE);
		if(pUser == null){
			pUser = pUserService.findByUserId(userId);
			//设置page域
			pageContext.setAttribute(userKeyPrev+userId, pUser,PageContext.PAGE_SCOPE);
		}
		
		
		try {
			if(StringUtil.isBlank(this.value)) {
				return TagSupport.SKIP_BODY;
			}
			boolean hasUserType = pUser.getUserType().equals(Integer.valueOf(this.value));
			if (hasUserType) {
	            return TagSupport.EVAL_BODY_INCLUDE;
	        } else {
	            return TagSupport.SKIP_BODY;
	        }
		} catch (Exception e) {
			e.printStackTrace();
			return TagSupport.SKIP_BODY;
		}
	}

	/**
	 * 获取springContext
	 * 
	 * @return springContext springContext
	 */
	public WebApplicationContext getSpringContext() {
		return springContext;
	}

	/**
	 * 设置springContext
	 * 
	 * @param springContext
	 *            springContext
	 */
	public void setSpringContext(WebApplicationContext springContext) {
		this.springContext = springContext;
	}

	
	

	/**
	 * 获取选中的值
	 * 
	 * @return value 选中的值
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 设置选中的值
	 * 
	 * @param value
	 *            选中的值
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 获取排除不显示
	 * 
	 * @return exclude 排除不显示
	 */
	public String getExclude() {
		return exclude;
	}

	/**
	 * 设置排除不显示
	 * 
	 * @param exclude
	 *            排除不显示
	 */
	public void setExclude(String exclude) {
		this.exclude = exclude;
	}

	/**  
	 * 获取是否为选择框  
	 * @return selection 是否为选择框  
	 */
	public boolean isSelection() {
		return selection;
	}

	/**  
	 * 设置是否为选择框  
	 * @param selection 是否为选择框  
	 */
	public void setSelection(boolean selection) {
		this.selection = selection;
	}


	

}
