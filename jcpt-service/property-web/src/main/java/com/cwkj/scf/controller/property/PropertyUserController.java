package com.cwkj.scf.controller.property;

import java.util.HashMap;
import java.util.Map;

import com.cwkj.scf.controller.base.BaseAction;
import com.cwkj.scf.controller.base.PermissionCode;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.service.property.PropertyUserService;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysRoleService;
import com.cwkj.jcptsystem.service.system.SysUserService;

/**
 * 物业所属用户.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/property/")
public class PropertyUserController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(PropertyUserController.class);
	
	private PropertyUserService propertyUserService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	public void setPropertyUserService(PropertyUserService propertyUserService)
	{
		this.propertyUserService=propertyUserService;
	}
	
	/**
	 * 物业所属用户数据分页
	 * @return
	 */
	@RequestMapping(value="/propertyUserList.do",method=RequestMethod.GET)
	public ModelAndView propertyUserList_methodGet(String propertyId)
	{
		ModelAndView view=new ModelAndView("property/propertyUserList");
		try {
			 PropertyUserDo opUser=propertyUserService.findByUserId(currentUser().getId());
			 view.addObject("currentPropertyUser", opUser);
			 view.addObject("propertyId", propertyId);
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取物业所属用户数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 物业所属用户数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/propertyUserList.do",method=RequestMethod.POST)
	public ModelAndView propertyUserList_methodPost(HttpServletRequest request,String propertyId,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("property/propertyUserList_table");
		try{ 
			checkPermission(PermissionCode.PROPERTYUSER_LIST_QUERY);
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			 selectItem.put("propertyId", propertyId);
			PageDo<PropertyUserDo> pagedata= propertyUserService.queryWithSysUserListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取物业所属用户数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增物业所属用户
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addPropertyUser.do",method=RequestMethod.GET)
	public ModelAndView addPropertyUser_methodGet(HttpServletRequest request,Integer userType)
	{
		ModelAndView view=new ModelAndView("property/addPropertyUser");
		try{
			checkPermission(PermissionCode.PROPERTYUSER_ADD);
			String propertyId=request.getParameter("propertyId");
			view.addObject("propertyUser", null);
			view.addObject("propertyId", propertyId);
			PropertyUserDo propertyUser=new PropertyUserDo();
			propertyUser.setPropertyId(currentPropertyUser(true,null).getPropertyId());
			if(userType==2 || userType==3 || userType==4)
			{
				propertyUser.setUserType(userType);
			}
			view.addObject("propertyUser",propertyUser);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业所属用户异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增物业所属用户
	 * @param request
	 * @param propertyUser
	 * @return
	 */
	@RequestMapping(value="/addPropertyUser.do",method=RequestMethod.POST)
	public ModelAndView addPropertyUser_methodPost(HttpServletRequest request,PropertyUserDo propertyUser,String password1,String password2)
	{
		ModelAndView view=new ModelAndView("property/addPropertyUser");
		try{
			String buildUserTagStr=request.getParameter("buildUserTagStr");
			String sexStr=request.getParameter("sexStr");
			if(StringUtil.isNotBlank(sexStr))
			{
				propertyUser.setSex(Integer.valueOf(sexStr));
			}
			if(StringUtil.isNotBlank(buildUserTagStr))
			{
				propertyUser.setBuildUserTag(Integer.valueOf(buildUserTagStr));
			}
			checkPermission(PermissionCode.PROPERTYUSER_ADD);
			PropertyUserDo currentPropertyUser=currentPropertyUser(true,null);
			AssertUtils.isTrue(currentPropertyUser.getUserType()!=null && currentPropertyUser.getUserType().compareTo(1)==0,"不是主账号，无权限添加用户");//1：主账号，2：借款一审，3：借款二审，4：供应商审批
			AssertUtils.isNotBlank(propertyUser.getRealName(), "输入姓名不能为空");
			AssertUtils.isNotBlank(propertyUser.getMobile(), "输入手机号不能为空");
			AssertUtils.isNotBlank(password1,"密码不能为空");
			AssertUtils.isNotBlank(password2,"确认密码不能为空");
			AssertUtils.isTrue(password1.equals(password2),"二次密码不一致");
			AssertUtils.isNotBlank(currentPropertyUser.getPropertyId(),"找不到核心企业信息");
			propertyUser.setPassword(password1);
			 propertyUser.setPropertyId(currentPropertyUser.getPropertyId());
			 /*
			 PropertyUserDo opUser=propertyUserService.findByUserId(currentUser().getId());
			 AssertUtils.notNull(opUser,"无添加账号权限");
			 AssertUtils.isTrue(opUser.getBuildUserTag()==1,"无添加账号权限");
			 */
			propertyUser.setRecordUserId(currentUser().getId());
			 propertyUser.setParentUserId(propertyUser.getRecordUserId());
			Integer result= propertyUserService.insertPropertyUser(propertyUser);
			AssertUtils.isTrue(result==1,"操作失败");
			setPromptMessage(view, PROMPTCODE_OK, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加物业所属用户异常（POST）",e);
		}finally {
			view.addObject("propertyUser", propertyUser);
			view.addObject("propertyId",propertyUser.getPropertyId());
		}
		return view;
	}
	
	 /**
	 * 浏览物业所属用户
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewPropertyUser.do",method=RequestMethod.GET)
	public ModelAndView viewPropertyUser_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/viewPropertyUser");
		try{
			checkPermission(PermissionCode.PROPERTYUSER_VIEW);
			PropertyUserDo propertyUser= propertyUserService.findPropertyUserById(id);
			view.addObject("propertyUser", propertyUser);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览物业所属用户异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑物业所属用户
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/editPropertyUser.do",method=RequestMethod.GET)
	public ModelAndView editPropertyUser_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("property/addPropertyUser");
		try{
			checkPermission(PermissionCode.PROPERTYUSER_EDIT);
			PropertyUserDo propertyUser= propertyUserService.findWithSysUserById(id);
			view.addObject("propertyUser", propertyUser);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业所属用户异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑物业所属用户
	 * @param request
	 * @param propertyUser
	 * @return
	 */
	@RequestMapping(value="/editPropertyUser.do",method=RequestMethod.POST)
	public ModelAndView editPropertyUser_methodPost(HttpServletRequest request,PropertyUserDo  propertyUser)
	{
		ModelAndView view =new ModelAndView("property/addPropertyUser");
		try{
			checkPermission(PermissionCode.PROPERTYUSER_EDIT);
			AssertUtils.isNotBlank(propertyUser.getId(), "参数无效");
			String buildUserTagStr=request.getParameter("buildUserTagStr");
			 String sexStr=request.getParameter("sexStr");
			 if(StringUtil.isNotBlank(sexStr))
			 {
				 propertyUser.setSex(Integer.valueOf(sexStr));
			 }
			 if(StringUtil.isNotBlank(buildUserTagStr))
			 {
				 propertyUser.setBuildUserTag(Integer.valueOf(buildUserTagStr));
			 }
			propertyUser.setRecordUserId(currentUser().getId());
			PropertyUserDo dbUser=propertyUserService.findPropertyUserById(propertyUser.getId());
			AssertUtils.notNull(dbUser,"参数错误[err:01]");
			if(dbUser.getUserId()!=null)
			{
				propertyUser.setUserId(dbUser.getUserId());
			}else {
				 if(StringUtil.isNotBlank(propertyUser.getUserName()))
				 {//添加系统账号
					 String password=SystemConst.SYS_USER_PASSWORD_DEFAULT;
					 SysUserDo user=sysUserService.findSysUserByUserName(propertyUser.getUserName());
						if(user!=null)
						{
							throw new BusinessException("账户名已被使用");
						}
						user=new SysUserDo();
						user.setLocked(1);//未锁定
						user.setUserName(propertyUser.getUserName());
						user.setRealName(propertyUser.getRealName());
						user.setTelphone(propertyUser.getMobile());
						String psw=DigestUtils.md5Hex(password+ SystemConst.PASS_KEY); 
						user.setPassword(psw);
						user.setPlatformId(currentPlatformId());
					int dbRes=sysUserService.insertSysUser(user);
					if(dbRes>0)
					{
						sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_PROPERTYS}, user.getId());
					}
					propertyUser.setUserId(user.getId());
					
				 }
			}
			
			Integer result= propertyUserService.updatePropertyUserById(propertyUser);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑物业所属用户异常（GET）",e);
		}finally {
			view.addObject("propertyUser", propertyUser);
		}
		return view;
	}
	

}
