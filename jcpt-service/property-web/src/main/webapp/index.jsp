<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="UTF-8">
    <title>小物橙科技</title>
    <meta http-equiv="refresh" content="0.1;url=./login.do">
    <link rel="stylesheet" href="res/css/zui.min.css" />
    <link rel="stylesheet" href="res/css/font-awesome.min.css" />
    <link rel="stylesheet" href="res/css/font-5.5-all.min.css" />
    <link rel="stylesheet" href="res/css/platform.css" />
    <link rel="stylesheet" href="res/css/theme-linear.css" />
    <script src="res/lib/jquery/jquery.js"></script>
    <script src="res/js/zui.min.js"></script>
    <!-- <meta http-equiv="refresh" content="0.2;url=./platform/main.do"> -->

    <style type="text/css">
        body{
            font-family: "PingFang SC", "Microsoft YaHei", "Hiragino Sans GB W3", Helvetica, "Heiti SC", "Droid Sans";
        }
        .page-header{
            min-height: 500px;background-color:#ffaa26; color:white; margin-top: 0px;
        }
        .page-header .navbar-nav>li>a{
            color:white;
        }
        .page-header h1{
            font-family: "PingFang SC", "Microsoft YaHei", "Hiragino Sans GB W3", Helvetica, "Heiti SC", "Droid Sans";

        }
        .mt-md-4, .my-md-4 {
            margin-top: 1.5rem!important;
        }

        .p-md-2 {
            padding: .5rem!important;
        }
        .img-fluid, .img-thumbnail {
            max-width: 100%;
            height: auto;
        }
        img {
            vertical-align: middle;
            border-style: none;
        }

        .pb-5, .py-5 {
            padding-bottom: 3rem!important;
        }
        .pt-5, .py-5 {
            padding-top: 3rem!important;
        }
        .col-md-10 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 83.33333%;
            flex: 0 0 83.33333%;
            max-width: 83.33333%;
        }
        .col {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
        }
        .ml-auto, .mx-auto {
            margin-left: auto!important;
        }
        .mr-auto, .mx-auto {
            margin-right: auto!important;
        }

        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            color: #12263f;
            border-color: transparent transparent #2c7be5;
            background-color: transparent;
        }
        .nav-tabs-sm .nav-link {
            padding-top: 1.15385rem;
            padding-bottom: 1.15385rem;
        }
        .login_box_border{
            border:1px solid #ffffff;
            border-radius:6px;
            padding:6px 25px !important;
        }
        .login_box{
            line-height:20px !important;
        }
        .banner-text1{font-size: 40px; }
        .banner-text2{font-size: 20px; }
        .c-title-1{font-size: 30px; color:#faaf67;}
        .c-title-2{font-size: 18px; color:#868686}
        .c-content{ color:#a3a3a3}
        .footer-nav-title-item{margin-top: 20px; color:#eb9700;}
        .footer-nav-title{color:#8e8e87;}

        .navbar-toggle .icon-bar {
            background-color: #fff;
        }

        .navbar-toggle .icon-bar+.icon-bar {
            margin-top: 4px;
        }
        .navbar-toggle .icon-bar {
            display: block;
            width: 22px;
            height: 2px;
            border-radius: 1px;
        }

        /*滚动条样式*/
        .innerbox::-webkit-scrollbar {/*滚动条整体样式*/
            width: 0px;     /*高宽分别对应横竖滚动条的尺寸*/
            height: 4px;
            background:transparent;
        }
        .innerbox::-webkit-scrollbar-thumb {/*滚动条里面小方块*/
            border-radius: 5px;
            -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.2);
            background: rgba(0,0,0,0.2);
        }
        .innerbox::-webkit-scrollbar-track {/*滚动条里面轨道*/
            -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.2);
            border-radius:0;
            background:transparent;
        }

        .navbar-nav-center
        {
            margin-top:10px;
        }

        @media (min-width: 768px){
            .navbar-brand
            {
                margin-right:300px;
            }
            .navbar-nav-cw li a:hover {
                background-color: transparent !important;
                color:#0d3d88 !important ;
            }

        }
    </style>
</head>
<body class="innerbox"  style="padding: 0px; margin: 0px;" >
<!-- 页头 -->
<div class="page-header inverse"  >
    <div class="section" style="padding:25px 0px;">
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-cw" style="margin:15px 0px;">
                        <span class="sr-only">切换导航</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"><img src="res/images/logo/logo-320.png" style="max-height: 46px;" /></a>
                </div>

                <div class="collapse navbar-collapse navbar-collapse-cw navbar-nav-center">
                    <ul class="nav navbar-nav navbar-nav-cw">
                        <li  ><a href="#">首页</a></li>
                        <li ><a href="#">产品中心</a></li>
                        <li   ><a href="#">解决方案</a></li>
                        <li   ><a href="#">关于我们</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="login_box"  ><a   class="login_box_border"  href="#">登录</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <!-- 页头介绍 -->
    <div class="section" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 align-self-center">
                    <p class="banner-text1 mb-3"  >金融机构和物业公司的连接器</p>
                    <p class="banner-text2 mb-4"  >小物橙是一家聚焦在物业产业链的金融服务科技公司</p>
                </div>
                <div class="col-md-4 ml-auto">
                    <img class="table-img" style="max-width: 300px;" src="res/images/public/section-00.jpg" alt="">
                </div>
            </div>

            <div class="row" style=" ">
                <a class="btn btn-lg btn-warning disabled"  href="#"> 我是金融机构 </a>
                <a class="btn btn-lg  btn-warning"  href="login.do"> 我是物业 </a>
                <a class="btn btn-lg  btn-warning disabled"  href="#"> 我是合伙人 </a>
            </div>
        </div>


    </div>
</div>

<!-- 单行 -->
<div class="section  py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class="c-title-1 mb-3"  >供应链金融-基于房地产及物业的供应链金融</p>
                <p class="c-title-2 mb-4"  >无缝对接MT4/MT5/cTrader,为经纪商量身定制的业务/客户/代理管理系统</p>
                <p class="c-content"><small>业务管理系统Back Office,结合Wder Work实现在线开户；灵活的权限控制；强大的报表与BI仪表盘，帮助经纪商李轻松掌握平台运营状况</small></p>
                <p class="c-content"><small>客户管理系统CRM，跟踪客户从销售线索到开户交易的全过程，并通过IP电话，邮件等多渠道方式触达客户，对分鲜客户进行精细化运营，提升开户、交易的转化率。</small></p>
                <p class="c-content"><small>客户管理系统CRM，跟踪客户从销售线索到开户交易的全过程，并通过IP电话，邮件等多渠道方式触达客户，对分鲜客户进行精细化运营，提升开户、交易的转化率。</small></p>
            </div>
            <div class="col-md-4">
                <img class="table-img" style="max-width: 300px;" src="res/images/public/section-01.jpg" alt="">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <img class="table-img" style="max-width: 300px;" src="res/images/public/section-02.jpg" alt="">
            </div>

            <div class="col-md-8">
                <p class="c-title-1 mb-3"  >白条-物业居民/白领的专属白条</p>
                <p class="c-title-2 mb-4"  >交易者账户管理、交易、投教、运营的一站式终端</p>
                <p  class="c-content"><small>Work,60秒极速开户，简洁高效的在线开户流程，及时的消息通知机制，帮助经纪商简化业务流程；同名多账户的一站式管理 ，帮助交易者轻松管理资金与个人账户信息：直播、点播、资讯、帮助经纪商在线进行投资教育，保持客户粘性，提升客户转化率。</small></p>
                <p  class="c-content"><small>Work App,一款客户管理系统CRM，跟踪客户从销售线索到开户交易的全过程，并通过IP电话，邮件等多渠道方式触达客户，对分鲜客户进行精细化运营，提升开户、交易的转化率。</small></p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <p class="c-title-1 mb-3"  >易借-基于大数据算法的智能化产品</p>
                <p class="c-title-2 mb-4"  >经纪商的图形化风控及桥接系统</p>
                <p  class="c-content"><small>基于云端架构的【桥】，对接全球顶尖流动性，为券商提供稳定，可靠的报价，而且无需安装插件，主标的标都可以使用，风控人员可随时随地联网访问页面</small></p>
                <p class="c-content"><small>客户管理系统CRM，跟踪客户从销售线索到开户交易的全过程，并通过IP电话，邮件等多渠道方式触达客户，对分鲜客户进行精细化运营，提升开户、交易的转化率。</small></p>
                <p class="c-content"><small>客户管理系统CRM，跟踪客户从销售线索到开户交易的全过程，并通过IP电话，邮件等多渠道方式触达客户，对分鲜客户进行精细化运营，提升开户、交易的转化率。</small></p>
            </div>

            <div class="col-md-4">
                <img class="table-img" style="max-width: 300px;" src="res/images/public/section-03.jpg" alt="">
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-4">
                <img class="table-img" style="max-width: 300px;" src="res/images/public/section-04.jpg" alt="">
            </div>
            <div class="col-md-8">
                <p class="c-title-1 mb-3"  >钱包-围绕物业产业链的钱包及账务处理服务</p>
                <p class="c-title-2 mb-4"  >行情聚合引擎</p>
                <p class="c-content"><small>为券商提供稳定，可靠的报价，而且无需安装插件，主标的标都可以使用，风控人员可随时随地联网访问页面</small></p>
                <p class="c-content"><small>客户管理系统CRM，跟踪客户从销售线索到开户交易的全过程，并通过IP电话，邮件等多渠道方式触达客户，对分鲜客户进行精细化运营，提升开户、交易的转化率。</small></p>
                <p class="c-content"><small>客户管理系统CRM，跟踪客户从销售线索到开户交易的全过程，并通过IP电话，邮件等多渠道方式触达客户，对分鲜客户进行精细化运营，提升开户、交易的转化率。</small></p>
            </div>

        </div>
    </div>
</div>



<!-- 合作伙伴 -->
<div class="section  py-5" style="border-top:1px solid #e4e4e4;" >
    <div class="container">
        <div class="row">
            <div clss="column col-full">
                <a style="color:#484c4b; font-size:30px; "><center>合作伙伴</center></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ml-auto">
                <img src="https://demos.creative-tim.com/marketplace/now-ui-kit-pro/assets/img/presentation-page/vodafone.jpg" alt="">
            </div>
            <div class="col-md-3">
                <img src="https://demos.creative-tim.com/marketplace/now-ui-kit-pro/assets/img/presentation-page/microsoft.jpg" alt="">
            </div>
            <div class="col-md-3">
                <img src="https://demos.creative-tim.com/marketplace/now-ui-kit-pro/assets/img/presentation-page/harvard.jpg" alt="">
            </div>
            <div class="col-md-3 mr-auto">
                <img src="https://demos.creative-tim.com/marketplace/now-ui-kit-pro/assets/img/presentation-page/stanford.jpg" alt="">
            </div>
        </div>
    </div>
</div>
<!-- 页尾 -->
<div class="section  py-5" style="background-color: #262e39; color:#484c4b; " >
    <div class="container">
        <div class="row">
            <div class="col-md-3 ml-auto ">
                <b class="footer-nav-title">产品</b>
                <div class="footer-nav-title-item">
                    <p >关于我们</p>
                    <p>公司历程</p>
                    <p>加入我们</p>
                    <p>免责声明</p>
                </div>
            </div>
            <div class="col-md-3">
                <b class="footer-nav-title">博客</b>
                <div class="footer-nav-title-item">
                    <p>最新功能</p>
                    <p>金融资信</p>
                </div>
            </div>
            <div class="col-md-3">
                <b class="footer-nav-title">关于</b>
                <div class="footer-nav-title-item">
                    <p>关于我们</p>
                    <p>公司历程</p>
                    <p>加入我们</p>
                    <p>免责声明</p>
                </div>
            </div>
            <div class="col-md-3 mr-auto-item">
                <b class="footer-nav-title"> 联系我们</b>

            </div>
        </div>
    </div>
</div>
</body>
</html>