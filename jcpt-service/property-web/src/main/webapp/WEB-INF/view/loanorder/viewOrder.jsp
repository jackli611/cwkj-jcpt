<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<div class="container-fluid">
	<div class="page-header">
		 <ol class="breadcrumb">
		  <li class="active">订单信息</li>
		</ol> 
	</div>

	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">核心企业基本信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>企业名称:${propertyDo.names}</td>
				</tr>
				<tr>
					<td>合作状态:<c:if test="${propertyDo.status eq 1 }">合作中</c:if>
							   <c:if test="${propertyDo.status eq 2 }">未合作</c:if>
					</td>
				</tr>
				<tr>
					<td>合作日期:<f:formatDate value="${propertyDo.createTime}" pattern="yyyy-MM-dd"/> </td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">借款供应商基本信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>借款企业:${supplierDo.names}</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">资金方基本信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>金融机构:${financeOrgDo.names}</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">借款信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>当期应收金额:${order.payamount}</td>
				</tr>
				<tr>
					<td>收款日期:<f:formatDate value="${order.paydate}" pattern="yyyy-MM-dd"/></td>
				</tr>
				<tr>
					<td>申请借款日期:<f:formatDate value="${order.applydate}" pattern="yyyy-MM-dd"/></td>
				</tr>
				<tr>
					<td>申请借款金额:<f:formatNumber value="${order.applyamount}" pattern="#,#00.0#"/></td>
				</tr>
				<tr>
					<td>借款期限:${order.loanperiod}期</td>
				</tr>
				<tr>
					<td>综合利息: <f:formatNumber value="${order.applyamount*order.applyrate/100}" pattern="#,#00.0#"/></td>
				</tr>
				<tr>
					<td>供应商还款方式:${repayName}</td>
				</tr>
				<tr>
					<td>订单状态:
						<c:if test="${order.loanstatus eq 'PENDING'}"> 待审批(应付方一审)</c:if>
						<c:if test="${order.loanstatus eq 'PROCESSING'}"> 待审批(应付方二审)</c:if>
						<c:if test="${order.loanstatus eq 'AUDITED'}"> 待放款</c:if>
						<c:if test="${order.loanstatus eq 'REPAYING'}"> 还款中</c:if>
						<c:if test="${order.loanstatus eq 'DRAFT'}">草稿</c:if>
						<c:if test="${order.loanstatus eq 'NOPASS'}">请修改</c:if>
						<c:if test="${order.loanstatus eq 'INVALID'}">审批不通过</c:if>
					</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	<%-- 
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">放款账户信息</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td>账户</td>
				</tr>
				<tr>
					<td>银行名称:</td>
				</tr>
				<tr>
					<td>放款日期:</td>
				</tr>
				
			</table>
		</div>
		</div>
	</div>
	--%>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">协议</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
				<tr>
					<td><img src="" /></td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	
	<div class="panel panel-default k_container_bg_color">
	  <div class="panel-body">
	  	<h4 class="title">合同</h4>
		<div class="pull-left">
		  <table class="table table-none-border k_panel_margin">
		  	<c:forEach items="${attachments}" var="attItem">
				<tr>
					<td>
						<span>附件名：</span><span>${attItem.certificatename}</span>
						<span>文件格式：</span><span>${attItem.filetype}</span>
						<span>文件类别：</span><span>合同</span>
						<span><a onclick="showAttachment('${attItem.id}');">查看附件</a></span><span>${attItem.originalfilename}</span>						
					</td>
				</tr>
		  	</c:forEach>
			</table>
		</div>
		</div>
	</div>
	
	
	
</div>
<script type="text/javascript">

function showAttachment(descFileId){
	  openWindow({url:'<c:url value="/toViewAttachment.do?attachmentId="/>'+descFileId});
}

<!--
$(function() {
    $('[data-toggle="lightbox"]').lightbox();
});
//-->
</script>