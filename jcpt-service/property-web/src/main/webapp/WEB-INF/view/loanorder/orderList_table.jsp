<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
	<table class="table table-hover">
		<thead>
			
			<tr >
				<th>借款订单号</th>
				<th>申请日期</th>
				<secure:hasRole name="核心企业">
				<th>借款企业</th>
				<th>应付金额（万元）</th>
				</secure:hasRole>
				<secure:hasRole name="供应商">
				<th>借款金额（万元）</th>
				</secure:hasRole>
				<th>状态</th>
				<th>操作</th>
			</tr>
			
			</thead>
			
 			<c:if test="${empty pagedata.page}">
 				<tbody  >
	             <tr class=gvItem>
	                 <td align="center" colspan="6">查询数据为空！</td>
	             </tr>
	            </tbody>
             </c:if>
             
                        
             <c:if test="${not empty pagedata.page }">
                    <tbody>
                    <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                     <tr >
							<td ><c:out value="${modelObj.ordercode}"/></td>
							<td ><f:formatDate value="${modelObj.applydate}"  pattern="yyyy-MM-dd"/></td>
							<secure:hasRole name="核心企业">
							<td ><c:out value="${modelObj.suppliername}"/></td>
							<td ><f:formatNumber value="${modelObj.payamount}" pattern="#,###,##0.0#"/></td>
							</secure:hasRole>
							<secure:hasRole name="供应商">
							<td ><f:formatNumber value="${modelObj.applyamount}" pattern="#,###,##0.0#"/></td>
							</secure:hasRole>
							<td >
								<c:if test="${modelObj.loanstatus eq 'PENDING'}"> 待审批(应付方一审)</c:if>
								<c:if test="${modelObj.loanstatus eq 'PROCESSING'}"> 待审批(应付方二审)</c:if>
								<c:if test="${modelObj.loanstatus eq 'AUDITED'}"> 待放款</c:if>
								<c:if test="${modelObj.loanstatus eq 'REPAYING'}"> 还款中</c:if>
								<c:if test="${modelObj.loanstatus eq 'DRAFT'}">草稿</c:if>
								<c:if test="${modelObj.loanstatus eq 'NOPASS'}">请修改</c:if>
								<c:if test="${modelObj.loanstatus eq 'INVALID'}">审批不通过</c:if>
							</td>
							<td>
						          <div class="dropdown">
								  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
								  <ul class="dropdown-menu">
								  	<c:if test="${modelObj.loanstatus eq 'NOPASS' }">
								    	<li><a href="#" onclick="javascript:loadModelUrl({'url':'<c:url value="/loanorder/toEdit.do?loanid=${modelObj.loanid}"/>'});" >编辑</a></li>
								    </c:if>
								    <li><a href="#" onclick="javascript:loadModelUrl({'url':'<c:url value="/loanorder/viewOrder.do?loanid=${modelObj.loanid}"/>'});" >查看详情</a></li>
								    <tool:propertyUser  value="2">
									    <c:if test="${modelObj.loanstatus eq 'PENDING' }">
									    	<li><a href="#" onclick="javascript:auditLoan('step1',${modelObj.loanid});" >审批</a></li>
									    </c:if>
									</tool:propertyUser>
									<tool:propertyUser  value="3">
									    <c:if test="${modelObj.loanstatus eq 'PROCESSING' }">
									    	<li><a href="#" onclick="javascript:auditLoan('step2',${modelObj.loanid});" >审批</a></li>
									    </c:if>
								    </tool:propertyUser>
								  </ul>
								</div>
							</td>
                     </tr>
                    </c:forEach>
                     </tbody>
               </c:if>
	</table>
	
	 <c:if test="${not empty pagedata.page }">
	 <ul class="pager" id="pager"></ul>
	 </c:if>
    