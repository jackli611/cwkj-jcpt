<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp"%>
<link rel="stylesheet" href="<c:url value="/res/css/zui.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/res/css/font-awesome.min.css"/>">
<link rel="stylesheet" href="<c:url value="/res/css/font-5.5-all.min.css"/>">
<link rel="stylesheet" href="<c:url value="/res/css/platform.css"/>" />
<link rel="stylesheet" href="<c:url value="/res/css/theme-linear.css"/>">
<link rel="stylesheet" href="<c:url value="/res/css/cw.css"/>">
<script src="<c:url value="/res/lib/jquery/jquery.js"/>"></script>
<script src="<c:url value="/res/js/zui.min.js"/>"></script>