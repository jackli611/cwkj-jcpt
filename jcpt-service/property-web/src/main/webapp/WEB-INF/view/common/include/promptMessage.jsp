<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<c:if test="${not empty error}">
<div  class="alert alert-block alert-danger alert-dismissable with-icon"> 
<i class="icon-remove-sign"></i><div class="content">${error.message }</div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
</div>
</c:if>
<c:if test="${not empty promptMsg}">
<div  class="alert alert-block alert-info-inverse alert-dismissable with-icon">
<i class="icon-info-sign"></i><div class="content">${promptMsg }</div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
</div>
</c:if>
<c:if test="${not empty promptCode}">
    <script type="text/javascript">
        $(function () {
            if(window.dealLoadCodeFn)
            {
                window.dealLoadCodeFn(<c:out value="${promptCode}"/>);
            }
        });
    </script>
</c:if>