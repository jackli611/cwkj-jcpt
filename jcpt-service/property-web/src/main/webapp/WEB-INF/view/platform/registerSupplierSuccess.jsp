<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <%@ include file="/WEB-INF/view/common/include/header.jsp"%>
    <style type="text/css">
        .page-header {
            padding: 5px 0;
            margin: 0px 0px;
            font-size: 14px;
            color:#666666;
        }

        .page-header-container {
            background-color: #f1f1f1;
            padding: 0px;
        }

        .page-left {
            float: left;
            width: 201px;
            background-color: #ffffff;
            border-right: 1px solid #a2a2a2;
        }

        .page-main {
            float: left;
        }

        .header {
            min-height: 40px;
            z-index: 1000;
            border-radius: 0;
            border: 0px;
            margin: 0px;
        }

        .pagecontainer {
            overflow: hidden;
        }
        .noShadow{
            box-shadow:none !important;

        }

        .nav-primary>li>a {
            border-right: 0px;
        }

        .menu>.nav>li>.nav>li>a {
            border-right: 0px;
        }

        .menu>.nav>li.show:last-child>.nav>li:last-child>a {
            border-bottom-right-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .noShadow label.col-sm-2{
            margin-top: 6px;
        }
    </style>

</head>
<body  style="margin: 0px; padding: 0px;">
<header >
    <div class="container" style="height: 50px;">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html"><img src="res/images/logo/logo-cyb-login.png"/></a>
        </div>

    </div>

</header>
<hr >
<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
<div class="container" id="mainModel">

    <div class="container-fluid" style="margin-top:20px;">

        <div class="panel panel-default panel_transparent">
            <div class="panel-body">
                <div class="alert alert-success with-icon">
                    <i class="icon-ok-sign"></i>
                    <div class="content">
						恭喜！提交成功，请耐心等待确认。 确认成功后我们的工作人员会和您取得联系！
						<br>
						请保存好以下信息等待确认后申请借款。
						<br>
						登录账号：${mobile }
						<br>
						登录网址: www.xwckeji.com
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


</body>
</html>
