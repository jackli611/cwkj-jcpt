<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
<!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
<title><tool:code type="SYSTEM_NAME"  selection="false" value="zh_cn"></tool:code></title>

 <link rel="stylesheet" href="<c:url value="/res/css/theme-keen.css"/>" />
 <script type="text/javascript" src="<c:url value="/res/js/theme-keen.js"/>"></script>
<script type="text/javascript">
    function loadData()
    {
        $.ajax({
            type: 'post',
            url: 'resetPassword.do',
            data: $("#form_password").serialize(),
            success: function(data) {
                $("#datalist").prop('outerHTML', data);
            }
        });

    }

    function loadModelID(model)
    {
        $("#mainModel").load("loadmodel.do?id="+model);
    }
    function loadModelUrl(obj){
        $("#mainModel").load(obj.url);
    }


    function loadNavMenus()
    {

        $(this).siblings().each(function(obj){
            if($(this).hasClass("active"))
            {
                $(this).removeClass("active");
            }
        });
        $(this).addClass("active");
        var model_id=$(this).attr("model_id");
        if(model_id)
        {
            loadModelID(model_id);
        }else{
        	 var model_url=$(this).attr("model_url");
        	 if(model_url)
        		 {
        		 	loadModelUrl({url:model_url});
        		 }else{
        			 $("#mainModel").html("");
        		 }
           
        }
    }
    $(function(){
        $("#navNenuList").children("li").bind("click",loadNavMenus);
        <c:if test="${propertyUser.userType == 4}">
        loadModelID("supplier_list");
        </c:if>
        <c:if test="${propertyUser.userType != 4}">
        loadModelID("loan_order_list");
        </c:if>
    });
</script>
<style type="text/css">
.k-aside-black .navbar-menu li a{
padding: 17px 15px !important;
font-size:14px !important ;
vertical-align: middle;
}
.k-aside-black .navbar-menu li a{
 color:#7d8092 !important;
 border: 0px !important;
 border-radius:0px !important;
 cursor: pointer;
}

.k-aside-black .nav-primary>li.active>a,.k-aside-black .nav-primary>li.active>a:focus,.k-aside-black  .nav-primary>li.active>a:hover{
background-color:#242939 !important;
}
.k-aside-white .navbar-menu li a{
color:#666666 !important;
 border: 0px !important;
 border-right: 0px !important;
 border-radius:0px !important;
 cursor: pointer;
}
.k-aside-white .nav-primary>li.active>a, .nav-primary>li.active>a:focus, .nav-primary>li.active>a:hover{
background-color:#3d95ff !important;
color:#ffffff !important;
}
.k-aside-bottom-logo{
    position: fixed;
    bottom: 10px;
    left:20px;
}
</style>
<%@ include file="leadingModal.jsp" %>
</head>
<body  style="font-weight: normal;">
<!-- 左侧菜单栏 -->
<div class="k-aside k-aside--fixed k-aside-black" id="k_aside">
<div class="container">
<div class="k-aside_brand row" id="k_aside_brand"  style="">
	<div class="k-aside_brand-logo">
		<a href="#">
            <c:if test="${not empty propertyOrg.logo}">
                <img alt="Logo" src="<c:url value="/attachment.do?id=${propertyOrg.logo}" />" style="height: 70px; width: 70px; border: 2px solid rgb(255, 255, 255); border-radius:35px; background: rgb(255, 255, 255); margin-left:20px; margin-top:30px;">
            </c:if>
            <c:if test="${empty propertyOrg.logo}">
                <img alt="Logo" src="<c:url value="/res/images/logo/521547032203_.pic_hd.jpg" />" style="height: 70px; width: 70px; border: 2px solid rgb(255, 255, 255); border-radius:35px; background: rgb(255, 255, 255); margin-left:20px; margin-top:30px;">
            </c:if>
		</a>
	</div>
</div>
<div class="row navbar-menu">
<div class="k-aside--mtop">
<ul class="nav nav-primary nav-stacked navbar-menu">
 <li class="active"><a><img src="<c:url value="/res/images/property/531547033558_.pic_hd.jpg"/>" style="margin-right: 10px; margin-left:6px; vertical-align: middle;" />供应链</a></li>
 <%--
 <li><a><img src="<c:url value="/res/images/property/541547033559_.pic_hd.jpg"/>" style="margin-right: 10px; margin-left:6px; vertical-align: text-bottom;" />钱包</a></li>
 <li><a><img src="<c:url value="/res/images/property/551547033561_.pic_hd.jpg"/>" style="margin-right: 10px; margin-left:6px; vertical-align: middle;" />白条</a></li>
  --%>
</ul>
</div>
</div>
</div>
    <div class="k-aside-bottom-logo">
        <img src="<c:url value="/res/images/logo/logo-cyb-bt.jpg"/>" alt="" />
    </div>
</div>


<!-- 二级菜单 -->
<div class="k-aside k-aside--3fixed k-aside-white" style="border-right: 1px solid #e4e4e4;">
<div class="container">
<div class="row navbar-menu">
 <ul class="nav nav-primary nav-stacked navbar-menu k-aside--mtop k-nav-2levelmenu" id="navNenuList">
 <li ><b style="line-height:30px;">核心企业</b></li>
     <c:if test="${propertyUser.userType != 4}">
 <li model_id="loan_order_list"><a>贴现</a></li>
     </c:if>
 <li model_id="supplier_list"><a >供应商</a></li>
 <c:if test="${propertyUser.userType == 1}">
 <li model_url="<c:url value="/property/propertyFinanceList.do" />"  ><a>资金方</a></li>
 </c:if>
 <li model_url="<c:url value="/property/viewPropertyOrg.do" />" ><a >企业信息</a></li>
</ul>
</div>
</div>
</div>

<!-- 主栏目 -->
<div class="k-wrapper" style="background-color: white;">
<div class="container-fluid" id="mainModel">
<div class="row">
</div>
</div>
</div>
 
 <!-- 顶部栏目 -->
<div class="k-aside--2fixed k-aside-white" id="k_bar_top">
<div class="container-fluid">
<div class="row">
<nav class="navbar navbar-default" role="navigation" style="border-radius:0px !important; min-height:50px; ">
  <div class="container-fluid">
  <ul class="nav navbar-nav navbar-left  k-navbar-topmenu" >
  <li><a style="font-size: 14px; " ><c:out value="${propertyOrg.names }"/></a></li>
  </ul>

  <ul class="nav navbar-nav navbar-right k-navbar-topmenu" id="topmenu" >
   <li model_url="supplierInvitation_list"><a href="property/supplierInvitationCode.do" data-toggle="modal" >邀请注册</a></li>
  <li class="dropdown">
          <a href="##" class="dropdown-toggle" data-toggle="dropdown">
           <i class="fa fa-user fa-1x"></i>&nbsp;&nbsp;<span class="text-username">
         <secure:principal property="realName" emptyProperty="userName"></secure:principal>
         <b class="caret"></b></a>
          <ul class="dropdown-menu" role="menu">
            <li><a  href="resetPassword.do" data-toggle="modal"><i class="fa fa-lock"></i> &nbsp;修改密码</a></li>
          <li><a href="logout.do"><i class="fa fa-sign-out"></i> &nbsp;退出系统</a></li>
          </ul>
        </li>
  </ul>
 </div>
 </nav>
</div>
</div>
</div>

</body>
</html>