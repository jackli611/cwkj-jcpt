<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/mainHeader.jsp"%>
<script type="text/javascript">
function loadModelID(model)
{
	$("#mainModel").load("loadModel.do?model="+model); 
}

$(function(){
	loadModelID("supplier_register_step_1");
});
</script>

<style type="text/css">
.page-header {
	border-bottom: 1px solid #a2a2a2;
	padding: 5px 0;
	background-color: #f1f1f1;
	margin: 0px 0px;
}

.page-header-container {
	background-color: #f1f1f1;
	padding: 0px;
}

.page-left {
	float: left;
	width: 201px;
	background-color: #ffffff;
	border-right: 1px solid #a2a2a2;
}

.page-main {
	float: left;
}

.header {
	min-height: 40px;
	z-index: 1000;
	border-radius: 0;
	border: 0px;
	margin: 0px;
}

.pagecontainer {
	overflow: hidden;
}

.nav-primary>li>a {
	border-right: 0px;
}

.menu>.nav>li>.nav>li>a {
	border-right: 0px;
}

.menu>.nav>li.show:last-child>.nav>li:last-child>a {
	border-bottom-right-radius: 0px;
	border-bottom-left-radius: 0px;
}
</style>

</head>
<body  style="margin: 0px; padding: 0px;">
 <div class="container" id="mainModel">
 
</div>
</body>
</html>