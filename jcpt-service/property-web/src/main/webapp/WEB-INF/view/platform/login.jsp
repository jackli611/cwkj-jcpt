<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/mainHeader.jsp"%>
<style type="text/css">
.page-header {
	border-bottom: 1px solid #a2a2a2;
	padding: 5px 0;
	background-color: #f1f1f1;
	margin: 0px 0px;
}

.page-header-container {
	background-color: #f1f1f1;
	padding: 0px;
}

.page-left {
	float: left;
	width: 201px;
	background-color: #ffffff;
	border-right: 1px solid #a2a2a2;
}

.page-main {
	float: left;
}

.header {
	min-height: 40px;
	z-index: 1000;
	border-radius: 0;
	border: 0px;
	margin: 0px;
}

.pagecontainer {
	overflow: hidden;
}

.nav-primary>li>a {
	border-right: 0px;
}

.menu>.nav>li>.nav>li>a {
	border-right: 0px;
}

.menu>.nav>li.show:last-child>.nav>li:last-child>a {
	border-bottom-right-radius: 0px;
	border-bottom-left-radius: 0px;
}
</style>

</head>
<body style=" background: #f5f7fa url(res/images/property/background.png);">
<header >
<div class="container">
 <div class="navbar-header">
 <a class="navbar-brand" href="index.html"><img src="res/images/logo/logo-cyb-platform.png"/></a>
 </div>
</div>
</header>

<secure:notAuthenticated  >
<div id="loginpanel"  style="width:100%;height:500px; position: static; top:50%">

<div  class="container-fixed-sm"
		style="width:300px; height: 220px; opacity:1;box-shadow:0 1px 1px rgba(0, 0, 0, 0.1); ">
		<div class="panel">
			<div class="panel-heading ">
				<h4>登录</h4>
			</div>
			<div class="panel-body">


					<form action="login.do" method="POST" class="form-horizontal">
						<div class="form-inline">
							<div style="color: red; font-size: 22px;" class="ml20">${message_login}</div>


							<div class="input-group">
								<span class="input-group-addon"><i class="icon-user"></i></span>
								<input type="text" class="form-control" name="username" id="_username" placeholder="用户名">
							</div>
							<div class="input-group mt10">
								<span class="input-group-addon fix-border"><i class="icon-key"></i></span>
								<input type="password" class="form-control" name="password" id="_password"  placeholder="密码">
							</div>
							<div class="input-group mt10">
								<input type="text" name="verifyCode" placeholder="验证码"  id="_verifyCode"
									   class="form-control" />
								<div  class="input-group-addon" style="width:90px; height: 12px; padding-top: 0px; padding-bottom: 0px;">
<img id="verifyCodeImage"
	 onclick="reloadVerifyCode()" src="getVerifyCodeImage.do"
	 style="cursor: pointer; width:90px;" title="点击重新成生验证码"/>
								</div>
							</div>



						<input type="submit" value="登录" class="btn-primary btn btn-block mt10" />
					</form>

			</div>
		</div>
	</div>
	 
	</div>
	<script type="text/javascript">
$(function(){
	var width=$(window).width();
	var height=$(window).height();
	var l_top=(height-350)/2;
	if(l_top>80)
		{
		l_top=l_top-50;
		}
	$("#loginpanel").css("margin-top",l_top+'px');
	if(window.location.href.indexOf("login.do")<0) { 
		　top.location.href="<c:url value="/login.do"/>"
	}
});
function reloadVerifyCode(){  
	 var today=new Date();
    $('#verifyCodeImage').attr('src', 'getVerifyCodeImage.do?12'+today.getSeconds());  
}  

</script>

</secure:notAuthenticated>
<secure:authenticated>
<div class="container mt50">
	<div class="alert  alert-warning-inverse with-icon"><i class="icon-info-sign"></i>
		<div class="content">
	您已经登陆系统，请选择<a href="main.do" class="alert-link" >进入系统</a>,或者<a href="logout.do" class="alert-link" >登出系统来用其它用户登陆</a>
		</div>
	</div>
</div>
</secure:authenticated>
</body>
</html>