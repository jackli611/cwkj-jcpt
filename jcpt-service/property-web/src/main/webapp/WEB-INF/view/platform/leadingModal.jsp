<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<style type="text/css">
#leadingModal .modal-body {
    padding: 0px;
}
.leadingModalBox{
    position: absolute;
    top:0px;
}
#leadingModalMsg{
    position: absolute;
    right:20px;
    top:20px;
    color:#999999;
}

</style>
<script type="text/javascript">
    $(function () {

var modalObj= (new $.zui.ModalTrigger({name:"leadingModal",className:"leadingModal",showHeader: false,width:472,height:322,custom: function() {
return "<div class='leadingModalBox'><img src=\"<c:url value="/res/images/property/leading.png" />\"  ></div><div id='leadingModalMsg'>2秒后自动关闭</div>" ;
}}));
modalObj.show();
window.closeTimeNow=2;
window.leadingModalTimes=setInterval(function(){
window.closeTimeNow-=1;
if(window.closeTimeNow<1) {
if(window.leadingModalTimes)
clearInterval(window.leadingModalTimes);
modalObj.close();
}else{
var msg = "" + window.closeTimeNow + "秒后自动关闭";
$("#leadingModalMsg").html(msg);
}
},1000);

})
</script>