<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
    <link href="<c:url value="/res/lib/uploader/zui.uploader.min.css"/>" rel="stylesheet" />
    <script src="<c:url value="/res/lib/uploader/zui.uploader.min.js"/>" ></script>
<!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
<title><tool:code type="SYSTEM_NAME"  selection="false" value="zh_cn"></tool:code></title>

 <link rel="stylesheet" href="<c:url value="/res/css/theme-keen.css"/>" />
 <script type="text/javascript" src="<c:url value="/res/js/theme-keen.js"/>"></script>
<script type="text/javascript">
    function loadData()
    {
        $.ajax({
            type: 'post',
            url: 'resetPassword.do',
            data: $("#form_password").serialize(),
            success: function(data) {
                $("#datalist").prop('outerHTML', data);
            }
        });

    }

    function loadModelID(model)
    {
        $("#mainModel").load("loadmodel.do?id="+model);
    }
    function loadModelUrl(obj){
        $("#mainModel").load(obj.url);
    }


    function loadNavMenus()
    {

        $(this).siblings().each(function(obj){
            if($(this).hasClass("active"))
            {
                $(this).removeClass("active");
            }
        });
        $(this).addClass("active");
        var model_id=$(this).attr("model_id");
        if(model_id)
        {
            loadModelID(model_id);
        }else{
        	 var model_url=$(this).attr("model_url");
        	 if(model_url)
        		 {
        		 	loadModelUrl({url:model_url});
        		 }else{
        			 $("#mainModel").html("");
        		 }
           
        }
    }

    $(function(){
        $("#navNenuList").children("li").bind("click",loadNavMenus);
       // loadModelID("dashboard");

        $('#uploaderMyUserPhoto').uploader({
            autoUpload: true,            // 当选择文件后立即自动进行上传操作
            file_data_name:"file",
            filters:{mime_types:[{title: 'Image File', extensions: 'jpg,jepg,png,gif'}]},
            url: '<c:url value="/uploadAttachment.do?groupType=sysuserlogo" />',
            onFileUploaded:function(file,responseObject){
                var backRes=JSON.parse(responseObject.response);
                if(backRes.code == 1)
                {


                    loadJsonData({url:"<c:url value="/userPhotoChange.do"/>",data:{photo:backRes.data},complete:function(bcObj){
                            if(bcObj.code ==1)
                            {
                                $("#sysuserlogo").attr("src","<c:url value="/attachment.do?id="/>"+bcObj.data);
                            }

                        }});
                }

            }

        });
    });
</script>
<style type="text/css">
.k-aside-black .navbar-menu li a{
padding: 17px 15px !important;
font-size:14px !important ;
vertical-align: middle;
}
.k-aside-black .navbar-menu li a{
 color:#7d8092 !important;
 border: 0px !important;
 border-radius:0px !important;
 cursor: pointer;
}

.k-aside-black .nav-primary>li.active>a,.k-aside-black .nav-primary>li.active>a:focus,.k-aside-black  .nav-primary>li.active>a:hover{
background-color:#242939 !important;
}
.k-aside-white .navbar-menu li a{
color:#666666 !important;
 border: 0px !important;
 border-right: 0px !important;
 border-radius:0px !important;
 cursor: pointer;
}
.k-aside-white .nav-primary>li.active>a, .nav-primary>li.active>a:focus, .nav-primary>li.active>a:hover{
background-color:#3d95ff !important;
color:#ffffff !important;
}
.k-aside-bottom-logo{
position: fixed;
bottom: 10px;
    left:20px;
}
#k_aside_brand .file-rename-by-click{
    position: absolute;
    top: -1px;
    left: -1px;
    width: 1px;
    height: 1px;
    opacity: 0;
}
</style>
    <%@ include file="leadingModal.jsp" %>
</head>
<body  style="font-weight: normal;">
<!-- 左侧菜单栏 -->
<div class="k-aside k-aside--fixed k-aside-black" id="k_aside">
<div class="container">
<div class="k-aside_brand row" id="k_aside_brand"  style="">
	<div class="k-aside_brand-logo" id="uploaderMyUserPhoto">
		<a href="#">
			<img class="uploader-btn-browse" id="sysuserlogo" alt="Logo" src="<c:url value="${sysUserPhoto}" />" style="height: 70px; width: 70px; border: 2px solid rgb(255, 255, 255); border-radius:35px; background: rgb(255, 255, 255); margin-left:20px; margin-top:30px;">
		</a>
	</div>
</div>
<div class="row navbar-menu">
<div class="k-aside--mtop">
<ul class="nav nav-primary nav-stacked navbar-menu">
 <li class="active"><a><img src="<c:url value="/res/images/property/531547033558_.pic_hd.jpg"/>" style="margin-right: 10px; margin-left:6px; vertical-align: middle;" />供应链</a></li>
 <%--
 <li><a><img src="<c:url value="/res/images/property/541547033559_.pic_hd.jpg"/>" style="margin-right: 10px; margin-left:6px; vertical-align: text-bottom;" />钱包</a></li>
 <li><a><img src="<c:url value="/res/images/property/551547033561_.pic_hd.jpg"/>" style="margin-right: 10px; margin-left:6px; vertical-align: middle;" />白条</a></li>
  --%>
</ul>
</div>
</div>
</div>
    <div class="k-aside-bottom-logo">
        <img src="<c:url value="/res/images/logo/logo-cyb-bt.jpg"/>" alt="" />
    </div>
</div>


<!-- 二级菜单 -->
<div class="k-aside k-aside--3fixed k-aside-white" style="border-right: 1px solid #e4e4e4;">
<div class="container">
<div class="row navbar-menu">
 <ul class="nav nav-primary nav-stacked navbar-menu k-aside--mtop k-nav-2levelmenu" id="navNenuList">
 <li ><b style="line-height:30px;">贴现</b></li>
 <c:if test="${supplier.status eq 3 }">
 <li model_id="loan_order_list"><a>申请</a></li>
 </c:if>
 <li model_url="<c:url value="/property/viewSupplier.do?id=${supplier.id}"/>"><a >企业信息</a></li>
 <c:if test="${supplier.platformStatus eq 104 }">
	 <li onclick="window.location.href='<c:url value='/registerSupplier.do?supplierId=${supplier.id}'/>';"><a>重填入网申请</a></li>
 </c:if>
</ul>
</div>
</div>
</div>

<!-- 主栏目 -->
<div class="k-wrapper" style="background-color: white;">
<div class="container-fluid" id="mainModel">
<div class="">
    <c:if test="${supplier.status != 3 }">

        
        <div style=" width:500px;height: 100px; position: absolute; top:40%; left: 50%; z-index:1; ">
            <div style="margin-left:-250px;">
        <div class="alert alert-danger with-icon" style="">
            <i class="icon-info-sign"></i>
            <div class="content">
               <c:if test="${supplier.platformStatus ne 104 }">
                <h4>等待审核!</h4>
                <p>您新注册商户我们正在加快审核信息，审核通过后就可以申请借款！</p>
               </c:if>
               <c:if test="${supplier.platformStatus eq 104 }">
                <h4>不通过!</h4>
                <p>请修改您的信息，重新提交申请！</p>
               </c:if>
                
            </div>
        </div>
            </div>
        </div>

    </c:if>
</div>
</div>
</div>
 
 <!-- 顶部栏目 -->
<div class="k-aside--2fixed k-aside-white" id="k_bar_top">
<div class="container-fluid">
<div class="row">
<nav class="navbar navbar-default" role="navigation" style="border-radius:0px !important; min-height:50px; ">
  <div class="container-fluid">
  <ul class="nav navbar-nav navbar-left  k-navbar-topmenu" >
  <li><a style="font-size: 14px; " ><c:out value="${supplier.names }"/></a></li>
  </ul>

  <ul class="nav navbar-nav navbar-right k-navbar-topmenu" id="topmenu" >
  <li class="dropdown">
          <a href="##" class="dropdown-toggle" data-toggle="dropdown">
           <i class="fa fa-user fa-1x"></i>&nbsp;&nbsp;<span class="text-username">
         <secure:principal property="realName" emptyProperty="userName"></secure:principal>
         <b class="caret"></b></a>
          <ul class="dropdown-menu" role="menu">
            <li><a  href="resetPassword.do" data-toggle="modal"><i class="fa fa-lock"></i> &nbsp;修改密码</a></li>
          <li><a href="logout.do"><i class="fa fa-sign-out"></i> &nbsp;退出系统</a></li>
          </ul>
        </li>
  </ul>
 </div>
 </nav>
</div>
</div>
</div>

</body>
</html>