<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp"%>
    <link href="<c:url value="/res/lib/uploader/zui.uploader.min.css"/>" rel="stylesheet" />
    <script src="<c:url value="/res/lib/uploader/zui.uploader.min.js"/>" ></script>
<style type="text/css">
.page-header {
	padding: 5px 0;
	margin: 0px 0px;
	font-size: 14px;
	color:#666666;
}

.page-header-container {
	background-color: #f1f1f1;
	padding: 0px;
}

.page-left {
	float: left;
	width: 201px;
	background-color: #ffffff;
	border-right: 1px solid #a2a2a2;
}

.page-main {
	float: left;
}

.header {
	min-height: 40px;
	z-index: 1000;
	border-radius: 0;
	border: 0px;
	margin: 0px;
}

.pagecontainer {
	overflow: hidden;
}
.noShadow{
     box-shadow:none !important;

}

.nav-primary>li>a {
	border-right: 0px;
}

.menu>.nav>li>.nav>li>a {
	border-right: 0px;
}

.menu>.nav>li.show:last-child>.nav>li:last-child>a {
	border-bottom-right-radius: 0px;
	border-bottom-left-radius: 0px;
}

.noShadow label.col-sm-2{
    margin-top: 6px;
}
.file-rename-by-click{
    position: absolute;
    top: -1px;
    left: -1px;
    width: 1px;
    height: 1px;
    opacity: 0;
}
</style>

</head>
<body  style="margin: 0px; padding: 0px;">
<header >
    <div class="container" style="height: 50px;">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html"><img src="res/images/logo/logo-cyb-login.png"/></a>
        </div>

    </div>

</header>
<hr >
<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
 <div class="container" id="mainModel">
 
<div class="container-fluid" style="margin-top:20px;">
    <form method="post" class="form-horizontal" onsubmit="return registerSupplier()">
    <div class="k_container k_container_bg_color">
<div class="panel panel-default noShadow" >
   
  <div class="panel-body">
      <h4 class="title">基本信息</h4>

  <div class="form-group">
    <label class="col-sm-2 required">供应商名称</label>
    <div class="col-sm-10">
      <p class="form-control-static">
      <input type="text" class="form-control requiredvalue"  maxlength="50" name="names" value="<c:out value="${supplier.names }" />"/></p>
    </div>
  </div>
   <div class="form-group">
    <label class="col-sm-2 required">组织机构代码</label>
     <div class="col-sm-10">
      <p class="form-control-static"><input type="text" class="form-control requiredvalue"  maxlength="50"  name="organizationCode" value="<c:out value="${supplier.organizationCode }" />"/></p>
    </div>
  </div>
   <div class="form-group">
    <label class="col-sm-2 required">法人</label>
     <div class="col-sm-10">
      <p class="form-control-static"><input type="text" class="form-control requiredvalue"  maxlength="20"  name="legal" value="<c:out value="${supplier.legal }" />"/></p>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 required">法人身份证号码</label>
     <div class="col-sm-10">
      <p class="form-control-static"><input type="text" class="form-control requiredvalue"  maxlength="35"  name="legalIdNo" value="<c:out value="${supplier.legalIdNo }" />"/></p>
    </div>
  </div>
      <div class="form-group">
          <label class="col-sm-2 required">上传营业执照</label>
          <div class="col-sm-10">
<input type="hidden" id="businessLicense" name="businessLicense" value="<c:out value="${supplier.businessLicense}"/>" />
              <div  id="businessLicensePhoto">
                      <c:if test="${not empty  supplier.businessLicense}">
                          <img id="businessLicenseImg"  src='<c:url value="/attachmentPublic.do?id=${supplier.businessLicense}" />'
                               data-image='<c:url value="/attachmentPublic.do?id=${supplier.businessLicense}" />'  class="uploader-btn-browse img-rounded" alt="" style="max-width: 130px; max-height: 190px; box-shadow:  0px 3px 2px #e4e4e4;">
                      </c:if>
                      <c:if test="${ empty  supplier.businessLicense}">
                      <img    id="businessLicenseImg" src="res/images/public/uploadImg.png"    class="uploader-btn-browse img-rounded" alt="" style="max-width: 130px; max-height: 190px; box-shadow:  0px 3px 2px #e4e4e4;">
                      </c:if>

              </div>
          </div>
      </div>

      <div class="form-group">
          <label class="col-sm-2 required">地址</label>
          <div class="col-sm-10">
              <p class="form-control-static"><input type="text"  maxlength="50"  class="form-control requiredvalue" name="address" value="<c:out value="${supplier.address }" />"/></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-2 required">联系人</label>
          <div class="col-sm-10">
              <p class="form-control-static"><input type="text"  maxlength="50"  class="form-control requiredvalue" name="contactPerson" value="<c:out value="${supplier.contactPerson }" />"/></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-2 required">邮箱</label>
          <div class="col-sm-10">
              <p class="form-control-static"><input type="text"  maxlength="35" class="form-control requiredvalue" name="email" value="<c:out value="${supplier.email }" />"/></p>
          </div>
      </div>
  </div>
</div>
		<%--
        <div class="panel panel-default noShadow" >

            <div class="panel-body">
                <h4 class="title">收款信息</h4>
		      <div class="form-group">
		          <label class="col-sm-2">银行名称</label>
		          <div class="col-sm-10">
		              <p class="form-control-static"><input type="text"  maxlength="20" class="form-control requiredvalue" name="bankName" value="<c:out value="${supplier.bankName }" />"/></p>
		          </div>
		      </div>
		      <div class="form-group">
		          <label class="col-sm-2">银行卡账户名</label>
		          <div class="col-sm-10">
		              <p class="form-control-static"><input type="text"  maxlength="20" class="form-control requiredvalue" name="bankAccount" value="<c:out value="${supplier.bankAccount }" />"/></p>
		          </div>
		      </div>
		      <div class="form-group">
		          <label class="col-sm-2">银行卡号</label>
		          <div class="col-sm-10">
		              <p class="form-control-static"><input type="text"  maxlength="50" class="form-control requiredvalue" name="bankCardno" value="<c:out value="${supplier.bankCardno }" />"/></p>
		          </div>
		      </div>

            </div>
        </div>
     --%>
        <div class="panel panel-default noShadow" >

            <div class="panel-body">
                <h4 class="title">登录账号</h4>
                <div class="form-group">
                    <label class="col-sm-2 required">手机号</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                        	<input type="text" maxlength="15" class="form-control requiredvalue" 
                        	name="mobile" value="<c:out value="${supplier.mobile }" />"  
                        	<c:if test="${supplier.mobile != null }"> readonly="readonly" </c:if> />
                        </p>
                    </div>
                </div>
                <c:if test="${supplier.id == null }">
                <div class="form-group">
                    <label class="col-sm-2 required">登录密码</label>
                    <div class="col-sm-10">
                        <p class="form-control-static"><input type="password"  maxlength="50"  class="form-control requiredvalue" name="password" /></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 required">确认密码</label>
                    <div class="col-sm-10">
                        <p class="form-control-static"><input type="password"  maxlength="50"  class="form-control requiredvalue" name="password2" /></p>
                    </div>
                </div>
                </c:if>
            </div>
        </div>
        <div class="panel panel-default noShadow" >

            <div class="panel-body">
                <h4 class="title">简介</h4>
      <div class="form-group">
          <label class="col-sm-2">企业简介</label>
          <div class="col-sm-10">
              <textarea rows="2" cols="3" name="companyProfile"  maxlength="500"   class="form-control"></textarea>
          </div>
      </div>


</div>

 
</div>
    </div>
        <div class="form-group mt20">
            <label class="col-sm-2"> <input type="hidden" name="inviteCode"  value='<c:out value="${inviteCode }" />' /></label>
            <div class="col-sm-10">
                <p class="form-control-static">
                <input type="submit"   class="btn btn-primary" value="提交申请" /></p>
            </div>
        </div>
        <input type="hidden" id="id" name="id"  value="<c:out value="${supplier.id }"/>"  />
        <input type="hidden" id="propertyId" name="propertyId"  value="<c:out value="${supplier.propertyId }"/>"  />
        <input type="hidden" id="level" name="level"  value="<c:out value="${supplier.level}"/>"  />
    </form>
</div>
</div>

<script type="text/javascript" >

    function registerSupplier(){

        var requiredvalue=true;

        $("input.requiredvalue,select.requiredvalue").each(function(){
            if($(this).val()=="")
            {
                var msgObj=$(this);
                TipsLayer({msg: msgObj.attr("placeholder")+"必填", obj: msgObj});
                requiredvalue=false;
                return false;
            }
        });

        if(requiredvalue) {
           return true
        }
        return false;
    }

    $(function () {
        $('#businessLicensePhoto').uploader({
            autoUpload: true,            // 当选择文件后立即自动进行上传操作
            file_data_name:"file",
            filters:{mime_types:[{title: 'Image File', extensions: 'jpg,jepg,png,gif'}]},
            url: '<c:url value="/uploadAttachmentPublic.do" />',
            onFileUploaded:function(file,responseObject){
                var backRes=JSON.parse(responseObject.response);
                if(backRes.code == 1)
                {
                    $("#businessLicense").val(backRes.data);
                    $("#businessLicenseImg").attr("src","<c:url value="/attachmentPublic.do?id="/>"+backRes.data);
                }else{
                    alertLayer(backRes.msg);
                }


            }

        });
        $('[data-toggle="lightbox"]').lightbox();
    })

</script>
</body>
</html>