<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="refresh" content="3.0;url=logout.do"> 
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
</head>
<body>
	 <div class="alert alert-danger with-icon">
    <i class="icon-info-sign"></i>
    <div class="content">
      <h4>无权限访问!</h4>
      <p>3秒后将退出。</p>
    </div>
  </div>
</body>
</html>