<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
		 
<div class="form-group  form-inline">
<label for="productName" class="monospaced_lg">产品名称:</label>
<input type="text" id="productName" name="productName" class="form-control w400" value="<c:out value="${product.productName}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="levels" class="monospaced_lg">级别:</label>
<label  class="radio-inline"><input type="radio"  <c:if test="${product.levels==1 }">checked="checked"</c:if>  name="levels" value="1" />一级</label>
<label  class="radio-inline"><input type="radio" <c:if test="${product.levels==2 }">checked="checked"</c:if>   name="levels" value="2" />二级</label>
</div> 
<div class="form-group  form-inline">
<label for="userType" class="monospaced_lg">用户类型:</label>
<label  class="checkbox-inline"><input type="checkbox"  <c:if test="${product.enterpriseProduct==1}">checked="checked"</c:if>  name="enterpriseProduct" value="1" />企业</label>
<label  class="checkbox-inline"><input type="checkbox" <c:if test="${product.personalProduct==1}">checked="checked"</c:if>   name="personalProduct" value="1" />个人</label>
</div> 
 
<div class="col-sm-offset-2 col-sm-10">
<c:if test="${empty product.id }">
<input type="button" onclick='submitFn("addProduct.do")' class="btn" id="addBtn" value="新增" />
</c:if>
<c:if test="${not empty product.id }">
<input type="hidden" id="id" name="id" value="<c:out value="${product.id}"/>" />
<input type="button" onclick='submitFn("editProduct.do")' class="btn" id="modifyBtn" value="修改" />
</c:if>
</div>
			</form>
		</div>
	</div>
</body>
</html>