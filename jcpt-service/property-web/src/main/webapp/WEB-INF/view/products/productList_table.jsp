<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>产品ID</th>
<th>产品名称</th>
<th>级别</th>
<th>企业用户</th>
<th>个人用户</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="8">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
                                    <td ><c:out value="${modelObj.id}"/></td>
<td ><c:out value="${modelObj.productName}"/></td>
<td ><c:out value="${modelObj.levels}"/></td>
<td ><c:if test="${modelObj.enterpriseProduct==1}"><i class="icon icon-check"></i></c:if></td>
<td ><c:if test="${modelObj.personalProduct==1}"><i class="icon icon-check"></i></c:if></td>
                                    <td>
	                                    <div class="dropdown">
										  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
										  <ul class="dropdown-menu">
										    <li><a href="viewProduct.do?id=<c:out value="${modelObj.id}"/>"  data-toggle="modal"   >浏览</a></li>
										    <li><a href="javascript:openWindow({url:'editProduct.do?id=<c:out value="${modelObj.id}"/>'});">编辑</a></li>
										    <li><a href="javascript:deleteProduct('<c:out value="${modelObj.id}"/>');">删除</a></li>
										  </ul>
										</div>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   