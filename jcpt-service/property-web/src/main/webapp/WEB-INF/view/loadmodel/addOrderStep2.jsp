<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="container-fluid mt50" id="orderStep2appId">
 <div class="panel panel-default panel_transparent">
  <div class="panel-body">

	  <form class="form-horizontal">
		  <div class="form-group">
			  <label for="payamount" class="col-sm-2">当前应收金额:</label>
			  <div class="col-sm-8" > <div class="help-block" id="payamount"></div>

			  </div>
		  </div>
		  <div class="form-group">
			  <label for="payamount" class="col-sm-2">收款日期:</label>
			  <div class="col-sm-8">
				  <span class="help-block" id="paydate" ></span>
			  </div>
		  </div>
		  <div class="form-group">
			  <label for="payamount" class="col-sm-2 required">借款金额:</label>
			  <div class="col-sm-5">
				  <input type="text" class="form-control" name="applyamount"  id = "applyamount" value=""/>
			  </div>
			  <div class="help-block">
				  元（不超过提交合同金额）
			  </div>
		  </div>
		  <div class="form-group">
			  <label for="loanperiod" class="col-sm-2 required">借款期限:</label>
			  <div class="col-sm-5">
				  <select class="form-control" name="loanperiod" id="loanperiod">

				  </select>
			  </div>
		  </div>
		  <div class="form-group">
			  <label for="applyrate" class="col-sm-2">综合利率:</label>
			  <div class="col-sm-8">
				  <input type="hidden" class="form-control" name="applyrate"  id = "applyrate" value="" />
				  <span class="help-block" id="form-applyrate"></span>
			  </div>
		  </div>
		  <div class="form-group">
			  <label for="repaytype" class="col-sm-2">还款方式:</label>
			  <div class="col-sm-8">
				  <input type="hidden" class="form-control" name="repaytype"  id = "repaytype" value=""/>
				  <span id="repayname"></span>
			  </div>
		  </div>
		  <div class="form-group">

			  <div class="col-sm-offset-2 col-sm-10">
				  <div class="checkbox">
					  <label>
						  <input type="checkbox" checked="checked" name="protocol"  id = "protocol" value=""/> <a onclick="showProtocol();" href="##">同意“借款协议”组合</a>
					  </label>
				  </div>
			  </div>
		  </div>
		  <div class="form-group">
			  <div class="col-sm-offset-2 col-sm-10">
			  <input type="button" onclick="prevStep()" class="btn btn-primary" value="返回上一步">
			  <input type="button" onclick="nextStep()" class="btn btn-primary" value="下一步">
			  </div>
		  </div>
	  </form>

</div>

</div>
</div>
<script type="text/javascript">

  function checkNull(){
	  var amt = $("#applyamount").val();
	  
	  if(amt==""){
		  alertLayer("借款金额不能小于1万");
		  return false;
	  }
	  
	  
	  if(amt<10000){
		  alertLayer("借款金额不能小于1万");
		  return false;
	  }
	  
	  if(amt>10000000){
		  alertLayer("借款金额不能大于1000万");
		  return false;
	  }
	  
	  var payAmt = $.trim($("#payamount").text())
	  if(Number(amt)>Number(payAmt)){
		  alertLayer("借款金额不能大于当前应收金额");
		  return false;
	  }
	  if(!$("#loanperiod").val() || $("#loanperiod").val()==""){
		  alertLayer("借款期限不能为空");
		  return false;
	  }
  }
  function showProtocol(){
	  openWindow({url:'<c:url value="/agreement/showProtocolTemplate.do"/>'});
  }
  
  function nextStep(){
	  
	  if(checkNull()==false){
		  return false;
	  }
	  
	  var cachOrder = sessionStorage.getItem('supper_apply_order');
	  var loanOrder = {};
	  if(cachOrder){
		  loanOrder = JSON.parse(cachOrder);
		}
	  loanOrder.repaytype= $("#repaytype").val();
	  loanOrder.applyrate= $("#applyrate").val();
	  loanOrder.loanperiod= $("#loanperiod").val();
	  loanOrder.applyamount= $("#applyamount").val();
	  loanOrder.repayname= $("#repayname").text();
	  console.log(JSON.stringify(loanOrder));
	  sessionStorage.setItem('supper_apply_order', JSON.stringify(loanOrder));
	  loadModelID('addOrderStep3');	  
  }
  function prevStep(){
	  loadModelID('addOrderStep1');	  
  }

  function showProductPlan(data){
	  $("#applyrate").val( data.data.rate);
	  $("#form-applyrate").text( "年率"+data.data.rate+"%");
	  $("#repaytype").val(data.data.repaycode);
	  $("#repayname").text(data.data.repayname);	  
	  
	  var minPeroid =  data.data.minperoid;
	  var maxPeroid =  data.data.maxperoid;
	  var loanperiodSelect = $("#loanperiod");
	  loanperiodSelect.empty();
	  
	  for(var i = minPeroid; i <= maxPeroid; i++){
	  	loanperiodSelect.append("<option value='"+i+"'>"+i+"期</option>");//添加option		  
	  }
	  
  }
  
  function getPlan(){
	  var cachOrder = sessionStorage.getItem('supper_apply_order');
	  var order = JSON.parse(cachOrder);
	  $.ajax({
          type:"POST",
          dataType:"json",
          url:"<c:url value='/property/getProduct.do'/>",
          data: order,
          async: false,
          success:function(data){
              if(data.code == "1"){             	
            	  showProductPlan(data);	
              }else{
              	alert(data.msg);
				  //alertLayer(data.msg);
              }
          }
      });
	  
  }
  
</script>
<script type="text/javascript">
$(function(){
	
	getPlan();
	
	var cachOrder = sessionStorage.getItem('supper_apply_order');
	console.log(JSON.stringify(cachOrder));
	if(cachOrder){
		var order = JSON.parse(cachOrder);
		$("#payamount").text(order.payamount);
		$("#paydate").text(order.paydate);
		$("#applydate").text(new Date());
		$("#applyamount").val(order.applyamount);
		$("#loanperiod").val(order.loanperiod);
		//$("#applyrate").val(order.applyrate);
		//$("#repaytype").val(order.repaytype);
		//$("#repayname").text(order.repayname);
	}
});
</script>