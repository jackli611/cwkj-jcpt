<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<link href="<c:url value="/res/lib/uploader/zui.uploader.min.css"/>" rel="stylesheet" />
<script src="<c:url value="/res/lib/uploader/zui.uploader.min.js"/>" ></script>

<div class="container-fluid">
  <div class="panel panel-default panel_transparent mt30">
  <div class="panel-body">

	  <form class="form-horizontal">
		  <div class="form-group">
			  <label for="payamount" class="col-sm-2 required">当前应收金额:</label>
			  <div class="col-md-6 col-sm-10">
				  <input type="text" class="form-control numDecText" name="payamount" id="payamount" value="" />
			  </div>
			  <div class="help-block">
				  元（ 1万以上，500万以下）
			  </div>
		  </div>
		  <div class="form-group">
			  <label for="paydate" class="col-sm-2 required">应收款日期:</label>
			  <div class="col-md-6 col-sm-10">
				  <input type="text" class="form-control form-control" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" name="paydate"  id = "paydate" value=""/>
			  </div>

		  </div>

		  <div class="form-group">
			  <input type="hidden" name="attachments" id="attachments" >
			  <label   class="col-sm-2">上传合同：</label>
			  <div class="col-md-6 col-sm-10">
				  <div id='uploadattachments' class="uploader" >
						  <div class="file-list file-list-grid" data-drag-placeholder="请拖拽文件到此处">
						  </div>
						  	<button type="button" class="btn uploader-btn-browse"><i class="icon icon-cloud-upload"></i> 选择合同 （WORD或者PDF文档）</button>
						  <br/>
				  </div>
			  </div>
		  </div>
		  
		  <div class="form-group">
			  <input type="hidden" name="invoices" id="invoices" >
			  <label   class="col-sm-2">上传发票：</label>
			  <div class="col-md-6 col-sm-10">
				  <div id='uploadinvoices' class="uploader" >
						  <div class="file-list file-list-grid" data-drag-placeholder="请拖拽文件到此处">
						  </div>
						  	<button type="button" class="btn uploader-btn-browse"><i class="icon icon-cloud-upload"></i> 选择合同 （图片或者PDF）</button>
						  <br/>
				  </div>

			  </div>
		  </div>
		 
		  <div class="form-group">
			  <label for="exampleInputPassword4" class="col-sm-2">&nbsp;</label>
			  <div class="col-md-6 col-sm-10">
			  <input type="hidden" value="${loanid}" id="loanid" name="loanid"/>
			  <input type="button" onclick="nextStep()" class="btn btn-primary" value="下一步">
			  </div>
		  </div>
	  </form>

</div>

</div>
</div>
<script type="text/javascript">

	function checkNull(){
		  var amt = $("#payamount").val();
		  if(amt == ""){
			  alertLayer("当前应收金额不能为空");
			  return false;
		  }
		  if($("#paydate").val()==""){
			  alertLayer("收款日期不能为空");
			  return false;
		  }
	}


  function nextStep(){
	  
	  if(checkNull()==false){
		  return false;
	  }
	 	  
	  //检查是否上传合同
	  if(("#attachments").val == ""){
		  alertLayer("请上传合同");
		  return false;
	  }
	  if(("#invoices").val == ""){
		  alertLayer("请上传发票");
		  return false;
	  }

	  var cachOrder = sessionStorage.getItem('supper_apply_order');
	  var loanOrder = {};
	  if(cachOrder){
		  loanOrder = JSON.parse(cachOrder);
		}
	  loanOrder.payamount= $("#payamount").val();
	  loanOrder.paydate= $("#paydate").val();
	  //loanOrder.attachments= $("#attachments").val();
	  console.log(JSON.stringify(loanOrder));
	  sessionStorage.setItem('supper_apply_order', JSON.stringify(loanOrder));
	  loadModelID('addOrderStep2');
  }

	
</script>
<script src="<c:url value='/res/lib/datetimepicker/datetimepicker.min.js'/>"></script>
<script type="text/javascript">

var fileArr=[];
function getUploadFileData(oneFile){
	fileArr.push(oneFile);
	sessionStorage.setItem('supper_apply_order.attachments', JSON.stringify(fileArr));
}

$(function(){
	
	var loanId = $("#loanid").val();
	if(loanId){
		$.ajax({
	          type:"POST",
	          dataType:"json",
	          url:"<c:url value='/loanorder/getOrder.do?loanid='/>"+loanId,
	          data: {},
	          async:false,
	          success:function(data){
	              if(data.code == "1"){
	            	  sessionStorage.setItem('supper_apply_order',JSON.stringify(data.data));	
	              }
	          }
	      });
	}
	
	//显示缓存的订单信息
	var cachOrder = sessionStorage.getItem('supper_apply_order');
	console.log(JSON.stringify(cachOrder));
	
	var uploadOptions={autoUpload:true,
					   url:"<c:url value="/fileupload.do"/>",
					   deleteActionOnDone: function(file, doRemoveFile) {
												doRemoveFile();
											},
					  filters:{ mime_types: [
						  						{title: 'Word文件', extensions: 'doc,docx'},	
						  						{title: 'PDF文件', extensions: 'pdf'},
						  						{title: '图片', extensions: 'jpg,png,jpeg'},
											],
							    prevent_duplicates:true
							   },
					  staticFiles:staticFiles,
					  responseHandler: function(responseObject, file) {
						  var uploadDivId = this.plOptions.container.id;
						  var uploadFileInput = uploadDivId.substr(uploadDivId.indexOf("upload"),-1);
						  console.info(JSON.stringify(file));
						  console.info(JSON.stringify(responseObject));
						  var retObj = JSON.parse(responseObject.response);
						  if(retObj.code == 1){
							  //this.plOptions.container.id: uploadinvoices  uploadattachments
							  var newAttObj ={"destfilepath":retObj.data,
				                       		  "filepath":retObj.data,
				                        	  "originalfilename":file.name,
				                              "certificatename":"供应商申请资料",
				                              "filetype":file.ext,
				                              "certificateType":uploadFileInput};
							  $("#"+uploadFileInput).val(JSON.stringify(newAttObj));
							  getUploadFileData(newAttObj);							  
						  }else{
							  return responseObject.response.msg;
						  }						  
					    }
					};
	if(cachOrder){
		var order = JSON.parse(cachOrder);
		$("#payamount").val(order.payamount);
		$("#paydate").val(order.paydate);
		//$("#attachments").val(order.attachments);

		var staticFiles=[];
	}
	$('#uploadattachments').uploader(uploadOptions);
	$('#uploadinvoices').uploader(uploadOptions);
	numInputFn();
});



</script>
