<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<div class="container-fluid">
<div class="page-header">
 <ol class="breadcrumb">
  <li class="active">借款管理</li>
</ol> 

<div class="panel panel-default">
  <div class="panel-body">
  
<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<div class="form-group"  data-toggle="buttons">
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox" name="statusArr"   value="1"> 待审批(应付方一审)
  </label>
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox" name="statusArr"   value="2"> 待审批(应付方二审)
  </label>
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox" name="statusArr"   value="3"> 待审批(资金方)
  </label>
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox"  name="statusArr" value="4"> 审核不通过
  </label>
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox" name="statusArr" value="5"> 请修改
  </label>
   <label class="btn k-btn-pd statusArr" >
    <input type="checkbox"  name="statusArr"  value="6" > 待放款
  </label>
   <label class="btn k-btn-pd statusArr" >
    <input type="checkbox"  name="statusArr"  value="7" > 已放款
  </label>
</div>
			</form>
  </div>
</div>
</div>
<div id="datalist" class="datalist"></div>
</div>
		
 <script type="text/javascript">
 $(function(){
	queryStart();
	 $("label.statusArr").mouseup(function(){
		 if(window.suppliermouseupTime)
			 {
				 clearTimeout(window.suppliermouseupTime);
			 }
			 
		 window.suppliermouseupTime=setTimeout(function() {
			 queryStart();
			}, 500);
		 
		});
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:'<c:url value="/property/payInfoList.do"/>',data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 </script>