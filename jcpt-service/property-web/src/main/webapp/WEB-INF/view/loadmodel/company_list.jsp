<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<div class="container-fluid">
<div class="page-header">
 <ol class="breadcrumb">
 <li >供应链</li>
  <li class="active">企业信息</li>
</ol> 

<div class="panel panel-default">
  <div class="panel-body">
  
<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<div class="form-group"  data-toggle="buttons">
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox" name="statusArr"   value="1"> 待审批
  </label>
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox"  name="statusArr" value="4"> 驳回待修改
  </label>
  <label class="btn k-btn-pd statusArr" >
    <input type="checkbox" name="statusArr" value="3"> 合作中
  </label>
   <label class="btn k-btn-pd statusArr" >
    <input type="checkbox"  name="statusArr"  value="2" > 不在合作
  </label>
</div>

			 <div class="form-group">
			 	<label for="names">企业名称：</label>
			  	<input id="names" class="form-control" type="text" name="names"    />
			</div>
			 <div class="form-group"><label for="mobile">手机号：</label>
			 <input type="text" class="form-control" name="mobile"  id="mobile">
			</div>
			 <div class="form-group"><label for="organizationCode">组织机构代码：</label>
			 <input type="text" class="form-control" name="organizationCode"  id="organizationCode">
			</div>
			 <div class="form-group"><label for="levelsStr">等级：</label>
			<select class="form-control" name="levelsStr">
<option value="" >请选择</option>
<tool:code type="levels" selection="true" value=""></tool:code>
</select>
			</div>
			 <div class="form-group">
			 <secure:hasPermission name="company_list_query">
			<input type="submit" class="btn" id="queryBtn" value="查询" />
			</secure:hasPermission>
			</div>
			</form>
  </div>
</div>
</div>
<div id="datalist" class="datalist"></div>
</div>
		
 <script type="text/javascript">
 $(function(){
	queryStart();
	 $("label.statusArr").mouseup(function(){
		 if(window.suppliermouseupTime)
			 {
				 clearTimeout(window.suppliermouseupTime);
			 }
			 
		 window.suppliermouseupTime=setTimeout(function() {
			 queryStart();
			}, 500);
		 
		});
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:'<c:url value="/property/companyList.do"/>',data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 </script>