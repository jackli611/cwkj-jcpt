<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<div class="container-fluid mt50">
 
<div class="panel panel-default k_container_bg_color">
  <div class="panel-body">
	  <div class="col-sm-offset-1 col-sm-11">
	  <table class="table table-none-border">
		  <tr ><td><span>当前应收金额:</span><span id="payamount"></span></td></tr>
		  <tr><td ><span>收款日期:</span><span id="paydate"></span></td></tr>
		  <tr><td><span>申请借款日期:</span><span id="applydate"></span></td></tr>
		  <tr><td ><span>申请借款金额:</span><span id="applyamount"></span></td></tr>
		  <tr><td><span>联系人信息:</span><span id="contactman"></span></td></tr>
		  <tr><td><span>联系电话:</span><span id="contacttel"></span></td></tr>
		  <tr><td><span>借款期限:</span><span id="loanperiodDisp"></span></td></tr>
		  <tr><td ><span>综合利率:</span><span id="applyrate"></span></td></tr>
		  <tr><td ><span>供应商还款方式:</span><span id="repaytype"></span></td>	</tr>
		  <tr><td ><span>同意“借款协议”组合</span></td></tr>
	  </table>
	  </div>

</div>
</div>
    <div class="panel panel-default panel_transparent">
		<div class="panel-body">
		<div class="col-sm-offset-1 col-sm-11 mt20">
		<input type="button" onclick="prevStep()" class="btn btn-primary" value="返回上一步">
		<input type="button" onclick="nextStep()" class="btn btn-primary" value="确认">
		</div>
		</div>
  </div>
</div>
</div>
<script src="<c:url value='/res/js/dateFormat.js'/>"></script>
<script type="text/javascript">
  function nextStep(){
	  submitOrder();
  }
  function prevStep(){
	  loadModelID('addOrderStep2');	  
  }
  
  function submitOrder(){
	  var cachOrder = sessionStorage.getItem('supper_apply_order');
	  var attachments = sessionStorage.getItem('supper_apply_order.attachments');
	  var order = JSON.parse(cachOrder);
	  order.attachments = attachments;
	  $.ajax({
          type:"POST",
          dataType:"json",
          url:"<c:url value='/loanorder/saveOrder.do'/>",
          data: order,
          success:function(data){
              if(data.code == "1"){             	
            	  loadModelID('addOrderStep4');	
              }else{
				  alertLayer(data.msg);            	  
              }
          }
      });
	  
  }
  
  
  function getSupplierContactMan(){
	  $.ajax({
          type:"POST",
          dataType:"json",
          url:"<c:url value='/property/getSupplierByCurrentUser.do'/>",
          data:{},
          success:function(data){
        	  
              if(data.code == "1"){ 
            	  console.log(data);
            	  $("#contacttel").text(data.data.mobile);
            	  $("#contactman").text(data.data.legal);
              }else{
				  alertLayer(data.msg);            	  
              }
          }
      });
  }
  
  
</script>
<script type="text/javascript">
$(function(){
	var cachOrder = sessionStorage.getItem('supper_apply_order');
	console.log(JSON.stringify(cachOrder));
	if(cachOrder){
		var order = JSON.parse(cachOrder);
		$("#payamount").text(order.payamount);
		$("#paydate").text(order.paydate);
		$("#applydate").text( (new Date()).Format("yyyy-MM-dd"));
		$("#applyamount").text(order.applyamount);
		$("#loanperiodDisp").text(order.loanperiod+'期');
		$("#applyrate").text(order.applyrate+'%(利息手续费及资金方利息构成)');
		$("#repaytype").text(order.repayname);
	}
	
	//获取供应商联系人
	getSupplierContactMan();
	
});
</script>