<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<div class="container-fluid">
<div class="page-header">
 <ol class="breadcrumb">
  <li class="active">供应商邀请码管理</li>
</ol> 

<div class="panel panel-default">
  <div class="panel-body">
  
<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<div class="form-group">
			<input type="button" class="btn" id="addBtn" onclick="openWindow({url:'<c:url value="/property/addSupplierInvitation.do"/>'})" value="邀请注册" />
			</div>
			</form>

  </div>
</div>
</div>
 
		<div id="datalist" class="datalist">
		</div>
</div>
  <script type="text/javascript">
 $(function(){
	queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"property/supplierInvitationList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 </script>
