<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<div class="container-fluid">
	<div class="page-header">

		<div class="panel panel-default panel_transparent">
			<div class="panel-body">
				<div class="row">
					<span class="pull-left">
						<ol class="breadcrumb">
						<li class="active">借款管理</li>
					</ol>

					</span>
					<secure:hasRole name="供应商">
						<span class="pull-right">
							<input type="button" class="btn btn-primary k-btn-pd"  onclick="javascript:sessionStorage.removeItem('supper_apply_order'); loadModelID('addOrderStep1');"  value="申请借款" />
						</span>
					</secure:hasRole>
				</div>
			</div>
		</div>
	</div>
	
	 <div class="k_container k_container_bg_color">
		<div class="panel panel-default">
		  <div class="panel-body">
		  
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
					<input type="hidden" name="pageIndex" value="1" id="pageIndex">
					<input type="hidden" name="pageSize" value="20" >
					<input type="hidden" name="queryLoanstatus"  id="queryLoanstatus" value="" >
				<div class="form-group"  data-toggle="buttons">
					<secure:hasRole name="核心企业">
					<label class="btn k-btn-pd statusArr k_btn" >
						<input type="checkbox" name="statusArr"   value="PENDING"> 待审批(应付方一审)
					</label>
					<label class="btn k-btn-pd statusArr k_btn" >
						<input type="checkbox" name="statusArr"   value="PROCESSING"> 待审批(应付方二审)
					</label>
					</secure:hasRole>
					<secure:hasRole name="供应商">
						<label class="btn k-btn-pd statusArr k_btn" >
							<input type="checkbox" name="statusArr"   value="PENDING,PROCESSING"> 待审批(应付方)
						</label>
					</secure:hasRole>
					<label class="btn k-btn-pd statusArr k_btn" >
						<input type="checkbox"  name="statusArr" value="TREATY"> 待审批(资金方)
					</label>
					<label class="btn k-btn-pd statusArr k_btn" >
						<input type="checkbox" name="statusArr" value="INVALID"> 审批不通过
					</label>
					<label class="btn k-btn-pd statusArr k_btn" >
						<input type="checkbox" name="statusArr" value="NOPASS"> 请修改
					</label>
					<label class="btn k-btn-pd statusArr k_btn" >
						<input type="checkbox" name="statusArr" value="AUDITED"> 待放款
					</label>
					<label class="btn k-btn-pd statusArr k_btn" >
						<input type="checkbox" name="statusArr" value="REPAYING,REPAYED"> 已放款
					</label>
				</div>
				<div class="form-group"  data-toggle="buttons" style="padding-left: 18px; padding-top: 10px;">
					<secure:hasRole name="核心企业">
					<input type="text" name="searchSupplierName" 
						  id="searchSupplierName" value="" placeholder="输入借款企业名称查询"
						  style="width:300px;" 
						  />
					<label class="btn k-btn-pd statusArr k_btn" >
						<span onclick="javascript:queryStart();">搜索</span> 
					</label>
					
					</secure:hasRole>
					
				</div>
			</form>
		
		  </div>
		</div>
		<div id="datalist" class="datalist">
		</div>
			
	</div>
</div>
<script type="text/javascript">
 $(function(){
	 queryStart();
	 $(".k_btn").on("click",function(){
		 loanstatus = $(this).find("input").val();
		 console.log(loanstatus);
		 $("#queryLoanstatus").val(loanstatus);
		 queryStart();
	 });
 });
 function loadData()
 {
 	loadTableData({id:'datalist',url:'<c:url value="loanorder/orderList.do"/>',data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 function getTabListChk(){
	 var chkObj = $(".tabck");
	 if(!chkObj){
		 return ;
	 }
	 for(var onechk = 0 ; onechk < chkObj.size(); onechk++){
		 var ckFlag = $(chkObj[onechk]).is(':checked');
		 if(ckFlag){
			 return  $(chkObj[onechk]).val();
		 }
	 }
 }
 function auditLoan(step,loanId){
	 
	 //var loanId = getTabListChk();
	 console.log(loanId);
	 if(!loanId){
		 alertLayer("请选择你要操作的记录");
		 return;
	 }
	 var auditWinIndex = openWindow({url:'<c:url value="/audit/audit.do"/>?loanId='+loanId+"&auditStep="+step});
	 
 }

 </script>
