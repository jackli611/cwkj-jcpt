<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<div class="container-fluid">

<div class="page-header">
 <ol class="breadcrumb">
  <li class="active">供应商管理</li>
</ol> 
</div>

 <div class="k_container k_container_bg_color">

<div class="panel panel-default">
  <div class="panel-body">
  
<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			<div class="form-group"  data-toggle="buttons">
  <label class="btn k-btn-pd statusArr k_btn" >
    <input type="checkbox" name="statusArr"   value="1"> 待审批
  </label>
  <label class="btn k-btn-pd statusArr k_btn" >
    <input type="checkbox"  name="statusArr" value="4"> 驳回待修改
  </label>
  <label class="btn k-btn-pd statusArr k_btn" >
    <input type="checkbox" name="statusArr" value="3"> 合作中
  </label>
   <label class="btn k-btn-pd statusArr k_btn" >
    <input type="checkbox"  name="statusArr"  value="2" > 不在合作
  </label>
</div>
			</form>

  </div>

</div>


	 
		<div id="datalist" class="datalist">
		</div>
		
		</div>
</div>
		<script type="text/javascript">
 $(function(){
	 queryStart();
	 $("label.statusArr").mouseup(function(){
		 if(window.suppliermouseupTime)
			 {
				 clearTimeout(window.suppliermouseupTime);
			 }
			 
		 window.suppliermouseupTime=setTimeout(function() {
			 queryStart();
			}, 500);
		 
		});
 });
 function loadData()
 {
 	loadTableData({id:'datalist',url:'<c:url value="property/supplierList.do"/>',data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
  
 
 function supplierStatus(id,status)
 {
	 confirmLayer({msg:"确定要修改状态？",yesFn:function(){
		 loadJsonData({url:'<c:url value="/property/supplierStatus.do?id=" />'+id+"&status="+status,complete:function(resultData){
			 if(resultData.code)
				 {
				 queryStart();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
 }
 </script>
