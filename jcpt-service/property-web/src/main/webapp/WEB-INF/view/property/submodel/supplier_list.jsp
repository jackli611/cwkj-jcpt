<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>
<div class="container-fluid">
<div class="page-header">
 <ol class="breadcrumb">
  <li class="active">供应商管理</li>
</ol> 

<div class="panel panel-default">
  <div class="panel-body">
  
<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
			 <div class="form-group"><label for="feeSource">供应商名称：</label>
			 <input type="text" class="form-control" name="names"  id="names">
			</div>
			 <div class="form-group"><label for="feeSource">手机号：</label>
			 <input type="text" class="form-control" name="mobile"  id="mobile">
			</div>
			 <div class="form-group"><label for="feeSource">组织机构代码：</label>
			 <input type="text" class="form-control" name="organizationCode"  id="organizationCode">
			</div>
			<div class="form-group">
			<secure:hasPermission name="supplier_list_query">
			<input type="submit" class="btn" id="queryBtn" value="查询" />
			</secure:hasPermission>
			<secure:hasPermission name="supplier_add">
			<input type="button" class="btn" id="addBtn" onclick="openWindow({url:'addSupplier.do'})" value="添加供应商" />
			</secure:hasPermission>
			</div>
			</form>

  </div>
</div>
</div>
</div>

	 
		<div id="datalist" class="datalist">
		</div>
		<script type="text/javascript">
 $(function(){
	 queryStart()
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"supplierList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 function deleteSupplier(id)
 {
	 confirmLayer({msg:"确定要删除？",yesFn:function(){
		 loadJsonData({url:"deleteSupplier.do?id="+id,complete:function(resultData){
			 if(resultData.code)
				 {
				 queryStart();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
 }
 
 function supplierStatus(id,status)
 {
	 alert(status);
	 confirmLayer({msg:"确定要修改状态？",yesFn:function(){
		 loadJsonData({url:"supplierStatus.do?id="+id+"&status="+status,complete:function(resultData){
			 if(resultData.code)
				 {
				 queryStart();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
 }
 </script>