<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table  table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>供应商编号</th>
<th>供应商名称</th>
<th>手机号</th>
<th>法人主体</th>
<th>法人身份证号码</th>
<th>组织机构代码</th>
<th>合作状态</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="9">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.supplierNo}"/></td>
<td ><c:out value="${modelObj.names}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:out value="${modelObj.legal}"/></td>
<td ><c:out value="${modelObj.legalIdNo}"/></td>
<td ><c:out value="${modelObj.organizationCode}"/></td>
<td><tool:code type="supplierStatus" selection="false" value="${modelObj.status}"></tool:code></td>
                                    <td>
                                        <a href="#" onclick="loadModelUrl({'title':'供应商审批','url':'<c:url value="/property/auditSupplier.do?supplierId=${modelObj.id}"/>'})"   >查看</a>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   