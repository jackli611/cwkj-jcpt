<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>核心企业</th>
<th>邀请码</th>
<th>访问地址</th>
<th>登记时间</th>
<th>有效到期时间</th>
<th>手机号</th>
<th>备注</th>
<th>创建时间</th>
<th>操作</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
<tr class=gvItem>
<td align="center" colspan="10">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty pagedata.page }">
<c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.propertyOrg}"/></td>
<td ><c:out value="${modelObj.inviteCode}"/></td>
<td ><c:out value="${modelObj.links}"/></td>
<td ><f:formatDate value="${modelObj.signUpTime}"  pattern="yyyy-MM-dd"/></td>
<td ><f:formatDate value="${modelObj.expiryDate}"  pattern="yyyy-MM-dd"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:out value="${modelObj.remark}"/></td>
<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
        <td>
            <div class="dropdown">
			  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
			  <ul class="dropdown-menu pull-left">
			   <li><a href="##" onclick="openWindow({'title':'供应商邀请注册浏览','url':'<c:url value="/property/viewSupplierInvitation.do?id=${modelObj.id}" />'})"   >浏览</a></li>
			   <li><a href="##" onclick="openWindow({'title':'供应商邀请注册编辑','url':'<c:url value="/property/editSupplierInvitation.do?id=${modelObj.id}" />'})"   >编辑</a></li>
			   <li><a href="##" onclick="deleteSupplierInvitation('<c:out value="${modelObj.id}"/>')">删除</a></li>
			  </ul>
			</div>
		</td>
 </tr>
</c:forEach>
</tbody>
</table>
	
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>

	
   