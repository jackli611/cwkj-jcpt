<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>

	<div id="promptMsg"></div>
    <div class="container-fluid">

        <div class="page-header">
            <ol class="breadcrumb">
                <li class="active">供应商</li>
            </ol>
        </div>

        <div class="panel panel-default k_container_bg_color">
            <div class="panel-body">
                <h4 class="title">基本信息</h4>
                <div class="pull-left">
                    <table class="table table-none-border k_panel_margin">
                        <tr ><td>供应商编号: <c:out value="${supplier.supplierNo }"/></td></tr>
                        <tr><td >供应商名称:<c:out value="${supplier.names }"/></td></tr>
                        <tr><td>手机号:<c:out value="${supplier.mobile }"/></td></tr>
                        <tr><td >法人:<c:out value="${supplier.legal }"/></td></tr>
                        <tr><td>法人身份证号码:<c:out value="${supplier.legalIdNo }"/></td></tr>
                        <tr><td>组织机构代码:<c:out value="${supplier.organizationCode }"/></td></tr>
<%--                        <tr><td>银行卡账户名:<c:out value="${supplier.bankAccount }"/>	</td></tr>--%>
<%--                        <tr><td >银行卡号:<c:out value="${supplier.bankCardno }"/></td></tr>--%>
<%--                        <tr><td >银行名称:<c:out value="${supplier.bankName }"/></td>	</tr>--%>
                        <tr><td >地址:<c:out value="${supplier.address }"/></td></tr>
                        <tr><td >联系人:<c:out value="${supplier.contactPerson }"/></td></tr>
                        <tr><td >邮箱:<c:out value="${supplier.email }"/></td></tr>
                        <tr><td >企业简介:<c:out value="${supplier.companyProfile }"/></td></tr>
                    </table>
                </div>
                <div class="pull-right">
                    <img class="img-rounded" data-toggle="lightbox" data-image="../attachment.do?id=<c:out value="${supplier.businessLicense}"/>" alt="" style="max-width: 130px; max-height: 190px; box-shadow:  0px 3px 2px #e4e4e4;" src="../attachment.do?id=<c:out value="${supplier.businessLicense}"/>" alt="">
                </div>
            </div>
        </div>

        <c:if test="${not empty supplierAuditList }">
            <div class="panel panel-default k_container_bg_color">
                <div class="panel-body">
                    <h4 class="title">注册审批信息</h4>
                    <div class="pull-left">
                        <table class="table table-bordered k_panel_margin k_table_bg_color">
                            <thead>
                            <tr >
                                <th style="width:60px; min-width: 60px;">序号</th>
                                <th>有效状态</th>
                                <th>备注</th>
                                <th>创建时间</th>
                            </tr>
                            </thead>
                            <c:forEach items="${supplierAuditList}" var="modelObj" varStatus="status">
                            <tr >
                                <td ><c:out value="${status.index+1}"/></td>
                                <td ><tool:code type="supplierStatus" selection="false" value="${modelObj.status}"></tool:code></td>
                                <td ><c:out value="${modelObj.remark}"/></td>
                                <td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
                            </tr>
                            </c:forEach>
                        </table>

                    </div>
                </div></div>
        </c:if>



<c:if test="${supplier.status == '1' && (not empty currentOropertyUser) && (currentOropertyUser.userType  == 4)}">
<div class="panel panel-default panel_transparent" id="auditPanel">
<div class="form-panel ">
    <form id="form_auditSupplier" name="form"    method="post"  class="form-horizontal">

        <div class="form-group" >
            <label for="level" class="monospaced_lg col-sm-2 required">审批状态:</label>
            <div class="col-md-6 col-sm-10">
            <label class="statusArr radio-inline" >
                <input type="radio"  name="status" value="4"> 不通过，打回修改
            </label>
            <label class="k-btn-pd statusArr radio-inline" >
                <input type="radio" name="status" value="3" id="radio_status" placeholder="审批状态"> 审批通过
            </label>
            </div>
        </div>
        <div class="form-group" >
        	<label for="level" class="monospaced_lg col-sm-2 required">供应商等级:</label>
            <div class="col-md-6 col-sm-10">
            <select class="form-control requiredvalue" name="level" id="level" placeholder="供应商等级">
            	<option value="">请选择</option>
			</select>
            </div>
        </div>
        <div class="form-group">
            <label for="level" class="monospaced_lg col-sm-2">审批说明:</label>
            <div class="col-md-6 col-sm-10">
            <textarea id="remark" name="remark" class="form-control new-comment-text" placeholder="审批说明"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-sm-10 col-sm-offset-2">
            <input type="hidden" id="supplierId" name="supplierId" value="<c:out value="${supplierId}"/>" />
            <input type="button" onclick='submitFn("property/auditSupplier.do")' class="btn btn-primary" id="modifyBtn" value="提交" />
            </div>
        </div>
    </form>
</div>
</div>
</div>


<script type="text/javascript">
//初始化已分配产品的等级
$(function(){
	loadJsonData({url:"<c:url value='/property/getPropertyProduct.do' />",data:{},complete:function(obj){
        if(obj.code==1){
        	for(var i=0; i<obj.data.length;i++){
            	$("#level").append("<option value='"+obj.data[i].levels+"' >A"+obj.data[i].levels+"</option>");
        	}
        }
    }});
    $('[data-toggle="lightbox"]').lightbox();
});

    function submitFn(url)
    {
        var requiredvalue=true;

        var maritalStatus= $('input[name="status"]:checked').val();
        if(!maritalStatus || maritalStatus=="")
        {
            TipsLayer({msg: $("#radio_status").attr("placeholder")+"必选", obj: $("#radio_status")});
            return false;
        }

        $("input.requiredvalue,select.requiredvalue").each(function(){
            if($(this).val()=="")
            {
                var msgObj=$(this);
                TipsLayer({msg: msgObj.attr("placeholder")+"必选", obj: msgObj});
                requiredvalue=false;
                return false;
            }
        });

        if(requiredvalue) {
            loadJsonData({
                url: url, data: $("#form_auditSupplier").serialize(), complete: function (obj) {
                    alertLayer(obj.msg);
                    if (obj.code == 1) {
                        $("#auditPanel").remove();
                    }
                }
            });
        }

        return false;
    }

</script>

</c:if>

</div>