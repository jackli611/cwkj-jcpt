<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
<input type="hidden" id="id" name="id" class="form-control w400" value="<c:out value="${supplier.id}"/>" />
<div class="form-group  form-inline">
<label for="supplierNo" class="monospaced_lg">供应商编号:</label>
<input type="text" id="supplierNo" name="supplierNo" class="form-control w400" value="<c:out value="${supplier.supplierNo}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="names" class="monospaced_lg">公司名称:</label>
<input type="text" id="names" name="names" class="form-control w400" value="<c:out value="${supplier.names}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="mobile" class="monospaced_lg">手机号:</label>
<input type="text" id="mobile" name="mobile" class="form-control w400" value="<c:out value="${supplier.mobile}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="legal" class="monospaced_lg">法人主体:</label>
<input type="text" id="legal" name="legal" class="form-control w400" value="<c:out value="${supplier.legal}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="legalIdNo" class="monospaced_lg">法人身份证号码:</label>
<input type="text" id="legalIdNo" name="legalIdNo" class="form-control w400" value="<c:out value="${supplier.legalIdNo}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="organizationCode" class="monospaced_lg">组织机构代码:</label>
<input type="text" id="organizationCode" name="organizationCode" class="form-control w400" value="<c:out value="${supplier.organizationCode}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="address" class="monospaced_lg">地址:</label>
<input type="text" id="address" name="address" class="form-control w400" value="<c:out value="${supplier.address}"/>" />
</div> 
<%--<div class="form-group  form-inline">--%>
<%--<label for="bankCardno" class="monospaced_lg">银行卡号:</label>--%>
<%--<input type="text" id="bankCardno" name="bankCardno" class="form-control w400" value="<c:out value="${supplier.bankCardno}"/>" />--%>
<%--</div> --%>
<%--<div class="form-group  form-inline">--%>
<%--<label for="bankName" class="monospaced_lg">银行名称:</label>--%>
<%--<input type="text" id="bankName" name="bankName" class="form-control w400" value="<c:out value="${supplier.bankName}"/>" />--%>
<%--</div> --%>
<%--<div class="form-group  form-inline">--%>
<%--<label for="bankAccount" class="monospaced_lg">银行卡账户名:</label>--%>
<%--<input type="text" id="bankAccount" name="bankAccount" class="form-control w400" value="<c:out value="${supplier.bankAccount}"/>" />--%>
<%--</div> --%>
 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<secure:hasPermission name="supplier_add">
<c:if test="${empty supplier.id }">
<input type="button" onclick='submitFn("addSupplier.do")' class="btn" id="addBtn" value="保存" />
</c:if>
</secure:hasPermission>
<secure:hasPermission name="supplier_edit">
<c:if test="${not empty supplier.id }">
<input type="button" onclick='submitFn("editSupplier.do")' class="btn" id="modifyBtn" value="保存" />
</c:if>
</secure:hasPermission>
</div>
			</form>
		</div>
	</div>
</body>
</html>