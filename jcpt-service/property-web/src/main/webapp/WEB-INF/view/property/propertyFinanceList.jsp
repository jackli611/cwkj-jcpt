<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<div class="container-fluid">

<div class="page-header">
 <ol class="breadcrumb">
  <li class="active">资金方管理</li>
</ol> 
</div>

 <div class="k_container k_container_bg_color">
<div class="panel panel-default">
  <div class="panel-body">
  
<table class="table table-bordered table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>资金方</th>
<th>授信额度</th>
<th>匹配日期</th>
<th>还款方式</th>
<th>担保方式</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty propertyFinanceList}">
<tr class=gvItem>
<td align="center" colspan="6">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty propertyFinanceList }">
<c:forEach items="${propertyFinanceList}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${status.index+1}"/></td>
<td ><c:out value="${modelObj.financeName}"/></td>
<td ><c:out value="${modelObj.creditAmount}"/></td>
<td ><f:formatDate value="${modelObj.metaDate}"  pattern="yyyy-MM-dd"/></td>
<td ><tool:code type="paymentType" selection="false" value="${modelObj.paymentType}"/></td>
<td ><tool:code type="guaranteeType" selection="false"  value="${modelObj.guaranteeType}"/></td>
 </tr>
</c:forEach>
</tbody>
</table>
</c:if>

</div>
</div>
</div>
</div>
   