<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>企业名称</th>
<th>企业简称</th>
<th>组织机构代码</th>
<th>法人主体</th>
<th>法人身份证号码</th>
<th>手机号</th>
<th>状态</th>
<th>创建时间</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="6">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.names}"/></td>
<td ><c:out value="${modelObj.shortName }"/></td>
<td ><c:out value="${modelObj.organizationCode}"/></td>
<td ><c:out value="${modelObj.legal}"/></td>
<td ><c:out value="${modelObj.legalIdNo}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:if test="${modelObj.status==1}">合作中</c:if><c:if test="${modelObj.status==2}">取消合作</c:if></td>
<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
                                    <td>
	                                    <div class="dropdown">
										  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
										  <ul class="dropdown-menu">
										   <secure:hasPermission name="propertyorg_view">
										     <li><a href="##" onclick="openWindow({'title':'物业机构浏览','url':'viewPropertyOrg.do?id=<c:out value="${modelObj.id}"/>'})"   >浏览</a></li>
										    </secure:hasPermission>
										  <secure:hasPermission name="propertyorg_edit">
										  <li><a href="##" onclick="openWindow({'title':'物业机构编辑','url':'editPropertyOrg.do?id=<c:out value="${modelObj.id}"/>'})"   >编辑</a></li>
										    </secure:hasPermission>
										    <secure:hasPermission name="propertyorg_delete">
										    <c:if test="${modelObj.status==1}">
										    <li><a href="##" onclick="propertyOrgStatus('<c:out value="${modelObj.id}"/>',2)">取消合作</a></li>
										    </c:if>
										    <c:if test="${modelObj.status==2}">
										    <li><a href="##" onclick="propertyOrgStatus('<c:out value="${modelObj.id}"/>',1)">合作</a></li>
										  </c:if>
										     
										    </secure:hasPermission>
										    <li><a href="##" onclick="openWindow({'title':'上传营业执照','url':'uploadBusinessLicense.do?id=<c:out value="${modelObj.id}"/>'})"   >上传营业执照</a></li>
										    <li><a href="##" onclick="openWindow({'title':'设置产品','url':'linkProduct.do?id=<c:out value="${modelObj.id}"/>'})"   >设置产品</a></li>
										    <li><a href="##" onclick="openWindow({'title':'用户列表','url':'propertyUserList.do?propertyId=<c:out value="${modelObj.id}"/>'})"   >用户信息</a></li>
										  </ul>
										</div>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   