<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/taglib.jsp" %>

<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
				 <div class="form-group">
			 	<label for="startDate">录入时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			 <div class="form-group">
			 	<label for="names">企业名称：</label>
			  	<input id="names" class="form-control" type="text" name="names"    />
			</div>
			 <div class="form-group"><label for="mobile">手机号：</label>
			 <input type="text" class="form-control" name="mobile"  id="mobile">
			</div>
			 <div class="form-group"><label for="organizationCode">组织机构代码：</label>
			 <input type="text" class="form-control" name="organizationCode"  id="organizationCode">
			</div>
			 <div class="form-group"><label for="levelsStr">等级：</label>
			<select class="form-control" name="levelsStr">
<option value="" >请选择</option>
<tool:code type="levels" selection="true" value=""></tool:code>
</select>
			</div>
			 <div class="form-group"><label for="approvalStatusStr">审批状态：</label>
			<select class="form-control" name="approvalStatusStr">
<option value="" >请选择</option>
<tool:code type="approvalStatus" selection="true" value=""></tool:code>
</select>
			</div>
			 <div class="form-group">
			 <secure:hasPermission name="company_list_query">
			<input type="submit" class="btn" id="queryBtn" value="查询" />
			</secure:hasPermission>
			<secure:hasPermission name="company_add">
			<input type="button" class="btn" id="addBtn" onclick="openWindow({url:'addCompany.do'})" value="添加企业" />
			</secure:hasPermission>
			</div>
			</form>
			<div id="datalist" class="datalist"></div>
			
 <script type="text/javascript">
 $(function(){
	// $("#queryBtn").click(queryStart);
	queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"companyList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 function deleteCompany(id)
 {
	 confirmLayer({msg:"确定要删除？",yesFn:function(){
		 loadJsonData({url:"deleteCompany.do?id="+id,complete:function(resultData){
			 if(resultData.code)
				 {
				 queryStart();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
 }
 
 </script>