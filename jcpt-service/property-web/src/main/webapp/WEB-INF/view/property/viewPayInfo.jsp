<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<link href="<c:url value="/res/lib/uploader/zui.uploader.min.css"/>" rel="stylesheet">
<script src="<c:url value="/res/lib/uploader/zui.uploader.min.js"/>" ></script>
<div class="container-fluid">
<div class="page-header">
 <ol class="breadcrumb">
  <li ><a href="#" onclick="loadModelID('company_list')">企业列表</a></li>
  <li class="active">企业信息</li>
</ol> 
</div>

<div class="panel panel-default">
  <div class="panel-body">
  <div class="pull-left">
 <table class="table table-none-border">
 <tr>
				<td >企业</th>
				<th><c:out value="${payInfo.companyName }"/></th>
				</tr>
						<tr >
 <td>当期应收金额:</td>
				<th> 
				<c:out value="${payInfo.amount }"/>
				</th>
				</tr>
				<tr>
				<td>收款日期:</th>
				<th><f:formatDate value="${payInfo.payDate}"  pattern="yyyy-MM-dd"/></th>
						</tr>
						<tr>
				<td>申请借款日期:</th>
				<th><f:formatDate value="${payInfo.createTime}"  pattern="yyyy-MM-dd"/></th>
				</tr>
 
			<tr>
				<td>支付状态</td>
				<th colspan="5">
				<tool:code type="payStatus" selection="false" value="${payInfo.payStatus }"></tool:code>
				</th>
			</tr>
			 
				</table>
 
</div>
</div></div>
 
 

 