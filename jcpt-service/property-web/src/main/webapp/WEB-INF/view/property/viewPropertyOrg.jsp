<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<div class="container-fluid">
<div class="page-header">
 <ol class="breadcrumb">
  <li class="active">核心企业信息</li>
</ol> 
</div>

<div class="panel panel-default k_container_bg_color">
  <div class="panel-body">
  <h4 class="title">基本信息</h4>
<div class="pull-left">
  <table class="table table-none-border k_panel_margin">
<tr>
<td>公司名称:<c:out value="${propertyOrg.names}"/></td>
</tr>
<tr>
<td>公司简称:<c:out value="${propertyOrg.shortName }"/></td>
</tr>
<tr>
<td>组织机构代码:<c:out value="${propertyOrg.organizationCode}"/></td>
</tr>
<tr>
<td>法人主体:<c:out value="${propertyOrg.legal}"/></td>
</tr>
<tr>
<td>法人身份证号码:<c:out value="${propertyOrg.legalIdNo}"/></td>
</tr>
<tr>
<td>手机号:<c:out value="${propertyOrg.mobile}"/></td>
</tr>
<tr>
<td>有效状态:<c:if test="${propertyOrg.status==1}">合作中</c:if><c:if test="${propertyOrg.status==2}">取消合作</c:if></td>
</tr>
<tr>
<td>创建时间:<f:formatDate value="${propertyOrg.createTime}"  pattern="yyyy-MM-dd"/></td>
</tr>
<tr>
<td>修改时间:<f:formatDate value="${propertyOrg.updateTime}"  pattern="yyyy-MM-dd"/></td>
</tr> 
<tr>
<td>备注:<c:out value="${propertyOrg.remark}"/></td>
</tr>
</table>
</div>

<div class="pull-left" style="margin-left: 200px;">
<c:if test="${not empty  propertyOrg.businessLicense}">
<img data-toggle="lightbox" src='<c:url value="property/businessLicenseFile.do?id=${propertyOrg.id}" />'
 data-image='<c:url value="property/businessLicenseFile.do?id=${propertyOrg.id}" />' data-caption="营业执照" class="img-rounded" alt="" style="max-width: 130px; max-height: 190px; box-shadow:  0px 3px 2px #e4e4e4;">
 <div style="position:relative; z-index:999; bottom: 10px; right:-10px; text-align: right;">
 <img src="<c:url value="/res/images/icon/zoomIn.png"/>"/>
 </div>
 <p>（营业执照）</p>
</c:if>


</div>
</div></div>

<div class="panel panel-default k_container_bg_color"><div class="panel-body">
<div class="pull-left">
<h4 class="title">合作产品</h4>

<table class="table k_table_bg_color k_panel_margin k_last_not_border">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>产品</th>
<th>还款方式</th>
<th>利率等级</th>
<th>利率</th>
			</tr>
			</thead>
			<tbody style="k_table_last_none_border" >
 <c:if test="${empty propertyOrg.propertyorgProductList}">
                            <tr class=gvItem>
                                <td align="center" colspan="5">查询数据为空！</td>
                            </tr>
                          
                        </c:if>
                        <c:if test="${not empty propertyOrg.propertyorgProductList }">
                            <c:forEach items="${propertyOrg.propertyorgProductList}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${status.index+1}"/></td>
<td ><c:out value="${modelObj.productName}"/></td>
<td ><c:out value="${modelObj.repaycode }"/></td>
<td ><c:out value="${modelObj.levels}"/></td>
<td ><c:out value="${modelObj.rate}"/></td>
                             </tr>
                            </c:forEach>
                            </c:if>
                             </tbody>
	</table>

</div>
</div></div>

<div class="panel panel-default k_container_bg_color"><div class="panel-body">
<div class="pull-left">
<h4 class="title">主账号登录</h4>
<p></p>

<c:if test="${not empty propertyOrg.propertyUserList }">
<c:forEach items="${propertyOrg.propertyUserList}" var="modelObj" varStatus="status">
<c:if test="${modelObj.userType == 1 }">
<p class="k_panel_margin"><c:out value="${modelObj.mobile}"/></p>
 </c:if>
</c:forEach>
</c:if>
                    
</div>
</div></div>

<div class="panel panel-default k_container_bg_color"><div class="panel-body">
<div class="pull-left">
<h4 class="title">核心企业协议</h4>
<p class="k_panel_margin"><span> <a href="#">代扣协议<i class="icon icon-double-angle-right"></i></a></span>
<span class="k_panel_margin"><a href="#">手续费协议<i class="icon icon-double-angle-right"></i></a></span></p>
</div>
</div></div>


<div class="panel panel-default k_container_bg_color"><div class="panel-body">
<h4 class="title">审批账号</h4>



<c:if test="${ empty propertyOrg.propertyUserList }">
    <c:if test="${(not empty currentPropertyUser) && currentPropertyUser.userType==1 }">
    <p class="k_panel_margin"><i class="fas fa-exclamation-circle"></i>还未建立审批账号</p>
<div class="panel pull-left k_panel_margin k_panel_padding panel_transparent">
<input type="button" class="btn k_btn_line k_btn_blue" value="建立借款审批账号" onclick="openWindow({'title':'创建借款审批账号','url':'<c:url value="/property/addPropertyUser.do?userType=2"/>','width':660,'height':450,complete:reloadModel})" />
</div>
    </c:if>
</c:if>

<c:if test="${not empty propertyOrg.propertyUserList }">
<div class="panel pull-left k_panel_margin k_panel_padding" style="width:400px;">
<div>
<span><b>借款审批账号</b></span>
    <c:if test="${(not empty currentPropertyUser) && currentPropertyUser.userType==1 }">
<div class="pull-right">
<input type="button" class="btn k_btn_line k_btn_blue" value="添加账号" onclick="openWindow({'title':'创建借款审批账号','url':'<c:url value="/property/addPropertyUser.do?userType=2"/>','width':660,'height':450,complete:reloadModel})" />
</div>
    </c:if>
</div>
<table class="table">
<c:forEach items="${propertyOrg.propertyUserList}" var="modelObj" varStatus="status">
<c:if test="${modelObj.userType == 2 }">
  <tr>
  <td>一审(初审):</td>
<td ><c:out value="${modelObj.realName}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:out value="${modelObj.position }"/></td>
 </tr>
 </c:if>
</c:forEach>
</table>


<p>
<table class="table">
<c:forEach items="${propertyOrg.propertyUserList}" var="modelObj" varStatus="status">
<c:if test="${modelObj.userType == 3 }">
  <tr>
  <td>二审(复审):</td>
<td ><c:out value="${modelObj.realName}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:out value="${modelObj.position }"/></td>
 </tr>
 </c:if>
</c:forEach>
</table>
</p>
</div>
</c:if>

<c:if test="${ empty propertyOrg.propertyUserList }">
    <c:if test="${(not empty currentPropertyUser) && currentPropertyUser.userType==1 }">
<div class="panel pull-left k_panel_margin k_panel_padding panel_transparent">
<input type="button" class="btn k_btn_line k_btn_blue" value="建上游供应商审批账号" onclick="openWindow({'title':'创建借款审批账号','url':'<c:url value="/property/addPropertyUser.do?userType=4"/>','width':660,'height':450,complete:reloadModel})" />
</div>
    </c:if>
</c:if>

<c:if test="${ not empty propertyOrg.propertyUserList }">
<div class=" panel pull-left k_panel_padding"  style="width:400px; margin-left: 30px;">
<div>
<span><b>上游供应商入网审批账号</b></span>
    <c:if test="${(not empty currentPropertyUser) && currentPropertyUser.userType==1 }">
<div class="pull-right">
<input type="button" class="btn k_btn_line k_btn_blue" value="添加账号" onclick="openWindow({'title':'创建借款审批账号','url':'<c:url value="/property/addPropertyUser.do?userType=4"/>','width':660,'height':450,complete:reloadModel})" />
</div>
    </c:if>
</div>
<table class="table">
<c:forEach items="${propertyOrg.propertyUserList}" var="modelObj" varStatus="status">
<c:if test="${modelObj.userType == 4 }">
  <tr>
  <td ><c:out value="${modelObj.realName}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td >
<c:out value="${modelObj.position }"/></td>
 </tr>
 </c:if>
</c:forEach>
</table>

</div>

</div></div>
</c:if>

</div>
<script type="text/javascript">
<!--
$(function() {
    $('[data-toggle="lightbox"]').lightbox();
});

function reloadModel()
{
    loadModelUrl({url:"<c:url value="/property/viewPropertyOrg.do" />"});
}
//-->
</script>
