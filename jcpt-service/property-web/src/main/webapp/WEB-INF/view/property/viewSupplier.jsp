<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<div class="container-fluid">

<div class="page-header">
 <ol class="breadcrumb">
  <li class="active">供应商</li>
</ol> 
</div>

<div class="panel panel-default k_container_bg_color">
  <div class="panel-body">
   <h4 class="title">基本信息</h4>
  <div class="pull-left">
 <table class="table table-none-border k_panel_margin">
						<tr ><td>供应商编号: <c:out value="${supplier.supplierNo }"/></td></tr>
							<tr><td >供应商名称:<c:out value="${supplier.names }"/></td></tr>
							<tr><td>手机号:<c:out value="${supplier.mobile }"/></td></tr>
			<tr><td >法人:<c:out value="${supplier.legal }"/></td></tr>
							<tr><td>法人身份证号码:<c:out value="${supplier.legalIdNo }"/></td></tr>
			<tr><td>组织机构代码:<c:out value="${supplier.organizationCode }"/></td></tr>
<%--			<tr><td>银行卡账户名:<c:out value="${supplier.bankAccount }"/>	</td></tr>--%>
<%--				<tr><td >银行卡号:<c:out value="${supplier.bankCardno }"/></td></tr>--%>
<%--				<tr><td >银行名称:<c:out value="${supplier.bankName }"/></td>	</tr>--%>
			<tr><td >地址:<c:out value="${supplier.address }"/></td></tr>
			<tr><td >联系人:<c:out value="${supplier.contactPerson }"/></td></tr>
			<tr><td >邮箱:<c:out value="${supplier.email }"/></td></tr>
			<tr><td style="padding-right: 80px;">企业简介:<c:out value="${supplier.companyProfile }"/></td></tr>
				</table>
				</div>


      <div class="pull-right">
          <c:if test="${not empty  supplier.businessLicense}">
              <img data-toggle="lightbox" src='<c:url value="/attachmentPublic.do?id=${supplier.businessLicense}" />'
                   data-image='<c:url value="/attachmentPublic.do?id=${supplier.businessLicense}" />' data-caption="营业执照" class="img-rounded" alt="" style="max-width: 130px; max-height: 190px; box-shadow:  0px 3px 2px #e4e4e4;">
              <div style="position:relative; z-index:999; bottom: 10px; right:-10px; text-align: right;">
                  <img src="<c:url value="/res/images/icon/zoomIn.png"/>"/>
              </div>
              <p>（营业执照）</p>
          </c:if>


      </div>
 
 </div>
 </div>
 
 <c:if test="${not empty supplierAuditList }">
<div class="panel panel-default k_container_bg_color">
  <div class="panel-body">
   <h4 class="title">注册审批信息</h4>
  <div class="pull-left">
  <table class="table table-bordered k_panel_margin k_table_bg_color">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>有效状态</th>
<th>备注</th>
<th>创建时间</th>
</tr>
</thead>
   <c:forEach items="${supplierAuditList}" var="modelObj" varStatus="status">
   <tr >
        <td ><c:out value="${status.index+1}"/></td>
<td ><tool:code type="supplierStatus" selection="false" value="${modelObj.status}"></tool:code></td>
<td ><c:out value="${modelObj.remark}"/></td>
<td ><f:formatDate value="${modelObj.createTime}"  pattern="yyyy-MM-dd"/></td>
 </tr>
   </c:forEach>
   
  </div>
  </div></div>
</c:if>
 
 
</div>

<script type="text/javascript">
    <!--
    $(function() {
        $('[data-toggle="lightbox"]').lightbox();
    });


    //-->
</script>