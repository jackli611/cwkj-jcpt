<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<link href="<c:url value="/res/lib/uploader/zui.uploader.min.css"/>" rel="stylesheet">
<script src="<c:url value="/res/lib/uploader/zui.uploader.min.js"/>" ></script>
<div class="container-fluid">
<div class="page-header">
 <ol class="breadcrumb">
  <li ><a href="#" onclick="loadModelID('company_list')">企业列表</a></li>
  <li class="active">企业信息</li>
</ol> 
</div>

<div class="panel panel-default">
  <div class="panel-body">
  <div class="pull-left">
 <table class="table table-none-border">
						<tr >
							<td>等级</td>
							<th  ><tool:code type="levels" selection="false" value="${company.levels}"></tool:code></th>
							</tr>
			<tr>
							<td >企业名称</th>
				<th><c:out value="${company.names }"/></th>
				</tr>
			<tr>
							<td >企业简称</th>
				<th><c:out value="${company.shortName }"/></th>
				</tr>
			<tr>
				<td>手机号</th>
				<th><c:out value="${company.mobile }"/></th>
						</tr>
			<tr>
				<td >法人</th>
				<th><c:out value="${company.legal }"/></th>
				</tr>
			<tr>
				<td>法人身份证号码</th>
				<th><c:out value="${company.legalIdNo }"/></th>
				</tr>
			<tr>
				<td>组织机构代码</td>
				<th> 
				<c:out value="${company.organizationCode }"/>
				</th>
			</tr>
			<tr>
				
				<td>邮箱</td>
				<th >
				<c:out value="${company.email }"/>
				</th>
				</tr>
			<tr>
				 <td>审批状态</td>
				<th >
				<tool:code type="approvalStatus" selection="false" value="${company.approvalStatus}"></tool:code>
				</th>
			</tr>
			 
				</table>
 </div>
 
 <div class="pull-right">
<c:if test="${empty  company.businessLicense}">
<div id="uploaderExample" class="uploader">
 <div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
  <div class="btn-group ml20">
  <button type="button" class="btn   uploader-btn-browse"><i class="icon icon-cloud-upload"></i> 选择文件	</button>
</div>
</div>
</c:if>
<c:if test="${not empty  company.businessLicense}">
<img data-toggle="lightbox" src="property/companyBLFile.do?id=<c:out value="${company.id}"/>"
 data-image="property/companyBLFile.do?id=<c:out value="${company.id}"/>" data-caption="营业执照" class="img-rounded" alt="" width="200">
</c:if>
</div>

 </div>
</div>
</div>

<c:if test="${empty  company.businessLicense}">
<script type="text/javascript">
$(function(){
	$('#uploaderExample').uploader({
		file_data_name:"file",
		filters:{mime_types:[{title: '图片', extensions: 'jpg,png,jpeg,gif'}]},
		fileList:'grid',
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: 'property/uploadCompanyBLFile.do?id=<c:out value="${company.id}"/>',  // 文件上传提交地址
	    onUploadFile:function(file){
	    	loading_start();
	    },
	    onUploadComplete: function(file,responseObject) {
	       loading_close();
	       if(responseObject.response)
	    	   {
	    	   alert(responseObject.response);
	    	   }
	     }
	});
	$('[data-toggle="lightbox"]').lightbox();
});
</script>
</c:if>