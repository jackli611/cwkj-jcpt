<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
<input type="hidden" id="propertyId" name="propertyId" class="form-control w400" value="<c:out value="${supplierInvitation.propertyId}"/>" />
<div class="form-group  form-inline">
<label for="inviteCode" class="monospaced_lg">邀请码:</label>
<input type="hidden" id="inviteCode" name="inviteCode"  value="<c:out value="${supplierInvitation.inviteCode}"/>" />
<input type="text" id="inviteCodeStr" name="inviteCodeStr" class="form-control w400" disabled="disabled" value="<c:out value="${supplierInvitation.inviteCode}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="links" class="monospaced_lg">访问地址:</label>
<input type="hidden" id="links" name="links"  value="<c:out value="${supplierInvitation.links}"/>" />
<input type="text" id="linksStr" name="linksStr" class="form-control w400" disabled="disabled" value="<c:out value="${supplierInvitation.links}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="expiryDateStr" class="monospaced_lg">有效到期时间:</label>
<input type="text" id="expiryDateStr" name="expiryDateStr" class="form-control w400" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${supplierInvitation.expiryDate}"  pattern="yyyy-MM-dd"/>"  />
</div> 
<div class="form-group  form-inline">
<label for="mobile" class="monospaced_lg">手机号:</label>
<input type="text" id="mobile" name="mobile" class="form-control w400" value="<c:out value="${supplierInvitation.mobile}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="remark" class="monospaced_lg">备注:</label>
<input type="text" id="remark" name="remark" class="form-control w400" value="<c:out value="${supplierInvitation.remark}"/>" />
</div> 
 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<c:if test="${empty supplierInvitation.id}" >
<input type="button" onclick='submitFn("addSupplierInvitation.do")' class="btn btn-primary" id="addBtn" value="保存" />
</c:if>
<c:if test="${not empty supplierInvitation.id}" >
<input type="hidden" id="id" name="id" value="<c:out value="${supplierInvitation.id}"/>" />
<input type="button" onclick='submitFn("editSupplierInvitation.do")' class="btn btn-primary" id="modifyBtn" value="保存" />
</c:if>
</div>
			</form>
		</div>
	</div>
</body>
</html>