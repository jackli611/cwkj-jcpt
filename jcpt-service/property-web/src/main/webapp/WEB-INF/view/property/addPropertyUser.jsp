<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">
     function dealLoadCodeFn(code) {
         if(code==1)
         {
             if(parent.closeWindow)
             {
                 parent.reloadModel();
                 window.setTimeout(parent.closeWindow,1000);
             }

         }
     }

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
    <div class="container-fluid">
        <div class="panel panel-default panel_transparent">
            <div class="panel-body">
			<form id="form_1" name="form"   method="post"  class="form-horizontal">
                <div class="form-group">
                    <label for="userType" class="col-xs-2 text-right">账号类型：</label>
                    <div class="col-xs-10">
                    <select name="userType" id="userType" class="form-control">
                        <c:if test="${propertyUser.userType== 2 || propertyUser.userType== 3}" >
                         <option value="2">一审（初审)</option>
                            <option value="3">二审（复审)</option>
                        </c:if>
                        <c:if test="${propertyUser.userType==4}">
                            <option value="4">供应商入网审批</option>
                        </c:if>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="realName" class="col-xs-2 text-right">姓名：</label>
                    <div class="col-xs-10">
                    <input type="text" id="realName" name="realName" placeholder="输入姓名" class="form-control" value="<c:out value="${propertyUser.realName}"/>" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile" class="col-xs-2 text-right">手机号：</label>
                    <div class="col-xs-10">
                    <input type="tel" id="mobile" name="mobile" placeholder="输入手机号" class="form-control" value="<c:out value="${propertyUser.mobile}"/>" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="position" class="col-xs-2 text-right">职务：</label>
                    <div class="col-xs-10">
                    <select name="position" id="position" class="form-control">
                        <option value="">请选择</option>
                        <tool:code type="propertyUserPosition" selection="true" value="${propertyUser.position}"></tool:code>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password1" class="col-xs-2 text-right">密码：</label>
                    <div class="col-xs-10">
                    <input type="password"  id="password1" name="password1" class="form-control"  placeholder="账号登录密码"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password2" class="col-xs-2 text-right">确认密码：</label>
                    <div class="col-xs-10">
                    <input type="password" name="password2" id="password2" class="form-control" placeholder="确认账号登录密码" />
                    </div>
                </div>

<secure:hasPermission name="propertyuser_add">
                <div class="form-group">
                    <label for="password2" class="col-xs-2 text-right">&nbsp;</label>
                    <div class="col-sm-offset-2 col-xs-10">
<input type="button" onclick='submitFn("addPropertyUser.do")' class="btn btn-primary" id="addBtn" value="保存" />

</div>
</div>
</secure:hasPermission>
			</form>
		</div>
	</div>
    </div>
</body>
</html>