<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <script type="text/javascript">
 $(function(){
	// $("#queryBtn").click(queryStart);
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"supplierList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }

 
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
				 <div class="form-group">
			 	<label for="startDate">时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			 <div class="form-group"><label for="feeSource">供应商名称：</label>
			 <input type="text" class="form-control" name="names"  id="names">
			</div>
			 <div class="form-group"><label for="feeSource">手机号：</label>
			 <input type="text" class="form-control" name="mobile"  id="mobile">
			</div>
			 <div class="form-group"><label for="feeSource">组织机构代码：</label>
			 <input type="text" class="form-control" name="organizationCode"  id="organizationCode">
			</div>
			<div class="form-group">
			<secure:hasPermission name="supplier_list_query">
			<input type="submit" class="btn" id="queryBtn" value="查询" />
			</secure:hasPermission>
			<secure:hasPermission name="supplier_add">
			<input type="button" class="btn" id="addBtn" onclick="openWindow({url:'addSupplier.do'})" value="添加供应商" />
			</secure:hasPermission>
			</div>
			</form>
		</div>
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
		<table class="table table-bordered">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>供应商编号</th>
<th>供应商名称</th>
<th>手机号</th>
<th>法人主体</th>
<th>法人身份证号码</th>
<th>组织机构代码</th>
<th>地址</th>
<th>银行卡号</th>
<th>银行名称</th>
<th>银行卡账户名</th>
			</thead>
			<tbody  >
			    
	</tbody>
	</table>
		</div>
	</div>
</body>
</html>