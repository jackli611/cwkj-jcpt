<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
			 
<div class="form-group  form-inline">
<label for="names" class="monospaced_lg">机构名称:</label>
<input type="text" id="names" name="names" class="form-control w400" value="<c:out value="${financeOrg.names}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="shortName" class="monospaced_lg">机构简称:</label>
<input type="text" id="shortName" name="shortName" class="form-control w400" value="<c:out value="${financeOrg.shortName}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="organizationCode" class="monospaced_lg">组织机构代码:</label>
<input type="text" id="organizationCode" name="organizationCode" class="form-control w400" value="<c:out value="${financeOrg.organizationCode}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="legal" class="monospaced_lg">法人主体:</label>
<input type="text" id="legal" name="legal" class="form-control w400" value="<c:out value="${financeOrg.legal}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="legalIdNo" class="monospaced_lg">法人身份证号码:</label>
<input type="text" id="legalIdNo" name="legalIdNo" class="form-control w400" value="<c:out value="${financeOrg.legalIdNo}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="mobile" class="monospaced_lg">手机号:</label>
<input type="text" id="mobile" name="mobile" class="form-control w400" value="<c:out value="${financeOrg.mobile}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="remark" class="monospaced_lg">备注:</label>
<input type="text" id="remark" name="remark" class="form-control w400" value="<c:out value="${financeOrg.remark}"/>" />
</div> 
 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<c:if test="${empty financeOrg.id}" >
<input type="button" onclick='submitFn("addFinanceOrg.do")' class="btn btn-primary" id="addBtn" value="保存" />
</c:if>
<c:if test="${not empty financeOrg.id}" >
<input type="hidden" id="id" name="id" value="<c:out value="${financeOrg.id}"/>" />
<input type="button" onclick='submitFn("editFinanceOrg.do")' class="btn btn-primary" id="modifyBtn" value="保存" />
</c:if>
</div>
			</form>
		</div>
	</div>
</body>
</html>