<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %> 
</head>
<body>
<div id="activity" >
  <table class="table table-bordered "> 
<tr>
<td>机构名称</td>
<th><c:out value="${financeOrg.names}"/></th>
</tr>
<tr>
<td>机构简称</td>
<th><c:out value="${financeOrg.shortName}"/></th>
</tr>
<tr>
<td>组织机构代码</td>
<th><c:out value="${financeOrg.organizationCode}"/></th>
</tr>
<tr>
<td>法人主体</td>
<th><c:out value="${financeOrg.legal}"/></th>
</tr>
<tr>
<td>法人身份证号码</td>
<th><c:out value="${financeOrg.legalIdNo}"/></th>
</tr>
<tr>
<td>手机号</td>
<th><c:out value="${financeOrg.mobile}"/></th>
</tr>
<tr>
<td>有效状态</td>
<th ><c:if test="${financeOrg.status==1}">合作中</c:if><c:if test="${financeOrg.status==2}">取消合作</c:if></th>
</tr>
<tr>
<td>备注</td>
<th><c:out value="${financeOrg.remark}"/></th>
</tr>
</table>

</div>
</body>
</html>