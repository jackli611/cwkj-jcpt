<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>

 <script type="text/javascript">

 function submitFn(url)
 {
	 $("#form_1").attr("action",url);
	 $("#form_1").submit();
 }

 </script>
</head>
<body>
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form"    method="post"  class="form-horizontal">
			<input type="hidden" id="financeId" name="financeId" class="form-control" value="<c:out value="${financeId}"/>" />
			<div class="form-group  form-inline">
<c:if test="${not empty financeUser.userId}">
<label for="userIdStr" class="monospaced_lg">系统账户:</label>
<input type="text" id="userName" name="userName" disabled="disabled" class="form-control w400" value="<c:out value="${financeUser.userName}"/>" />
</c:if>
<c:if test="${empty financeUser.userId}">
<label for="userIdStr" class="monospaced_lg">系统账户:</label>
<input type="text" id="userName" name="userName" class="form-control w400" value="<c:out value="${financeUser.userName}"/>" />
<small>默认密码：12345678</small>
</c:if>
</div> 
<div class="form-group  form-inline">
<label for="realName" class="monospaced_lg">用户姓名:</label>
<input type="text" id="realName" name="realName" class="form-control w400" value="<c:out value="${financeUser.realName}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="mobile" class="monospaced_lg">手机号:</label>
<input type="text" id="mobile" name="mobile" class="form-control w400" value="<c:out value="${financeUser.mobile}"/>" />
</div> 
<div class="form-group  form-inline">
<label for="email" class="monospaced_lg">邮箱:</label>
<input type="text" id="email" name="email" class="form-control w400" value="<c:out value="${financeUser.email}"/>" />
</div> 
 <div class="form-group  form-inline">
<label for="sexStr" class="monospaced_lg">性别:</label>
<select name="sexStr" class="form-control">
<option value="">请选择</option>
<tool:code type="sex" selection="true" value="${financeUser.sex}"></tool:code>
</select>
</div> 
<div class="form-group  form-inline">
<label for="buildUserTag" class="monospaced_lg">能否创建账户:</label>
<select name="buildUserTagStr" class="form-control">
<option value="">请选择</option>
<tool:code type="buildUserTag" selection="true" value="${financeUser.buildUserTag}"></tool:code>
</select>
</div> 
 <div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<c:if test="${empty financeUser.id}" >
<input type="button" onclick='submitFn("addFinanceUser.do")' class="btn btn-primary" id="addBtn" value="保存" />
</c:if>
<c:if test="${not empty financeUser.id}" >
<input type="hidden" id="id" name="id" value="<c:out value="${financeUser.id}"/>" />
<input type="button" onclick='submitFn("editFinanceUser.do")' class="btn btn-primary" id="modifyBtn" value="保存" />
</c:if>
</div>
			</form>
		</div>
	</div>
</body>
</html>