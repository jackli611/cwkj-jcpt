<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
<thead>
<tr >
<th style="width:60px; min-width: 60px;">序号</th>
<th>用户姓名</th>
<th>手机号</th>
<th>邮箱</th>
<th>性别</th>
<th>能否创建账户</th>
<th>操作</th>
</tr>
</thead>
<tbody  >
 <c:if test="${empty pagedata.page}">
<tr class=gvItem>
<td align="center" colspan="7">查询数据为空！</td>
</tr>
</tbody>
</table>
</c:if>
<c:if test="${not empty pagedata.page }">
<c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
 <tr >
        <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.realName}"/></td>
<td ><c:out value="${modelObj.mobile}"/></td>
<td ><c:out value="${modelObj.email}"/></td>
<td ><tool:code type="sex" selection="false" value="${modelObj.sex}"></tool:code></td>
<td > 
<tool:code type="buildUserTag" selection="false" value="${modelObj.buildUserTag}"></tool:code>
</td>
        <td>
            <div class="dropdown">
			  <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
			  <ul class="dropdown-menu pull-left">
			   <li><a href="##" onclick="openWindow({'title':'金融机构用户浏览','url':'viewFinanceUser.do?id=<c:out value="${modelObj.id}"/>'})"   >浏览</a></li>
			   <li><a href="##" onclick="openWindow({'title':'金融机构用户编辑','url':'editFinanceUser.do?id=<c:out value="${modelObj.id}"/>'})"   >编辑</a></li>
			   <li><a href="##" onclick="deleteFinanceUser('<c:out value="${modelObj.id}"/>')">删除</a></li>
			  </ul>
			</div>
		</td>
 </tr>
</c:forEach>
</tbody>
</table>
	
<ul class="pager" id="pager"></ul>
<script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
</c:if>

	
   