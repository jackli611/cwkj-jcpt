<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
</body>
</html>
<script src="<c:url value='/res/lib/pdf/pdfobject.min.js'/>"></script>
<script type="text/javascript">
	if(PDFObject.supportsPDFs){
	console.log("Yay, this browser supports inline PDFs.");
	} else {
		console.log("Boo, inline PDFs are not supported by this browser");
	}
	PDFObject.embed("<c:url value='/viewattachment.do?attachmentId=${attachmentId}'/>");
</script>