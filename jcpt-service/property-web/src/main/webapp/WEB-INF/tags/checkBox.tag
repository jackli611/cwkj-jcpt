<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="value"  %>
<%@ attribute name="name"  %>
<%@ attribute name="id"  %>
<%@ attribute name="text"  %>
<%@ attribute name="checked"  %>

<input type="checkbox" 
<c:if test="${checked==true}">
checked="checked"
</c:if>
name="<c:out value="${name}" />" id="<c:out value="${id }" />" value="<c:out value="${value}" />" />
<label for="<c:out value="${id }" />" >
<c:out value="${text}" />
</label> 