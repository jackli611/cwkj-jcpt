package com.cwkj.agreement.base;


import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.notify.MailNotify;
import com.cwkj.jcptsystem.service.notify.NotifyService;
import com.cwkj.jcptsystem.service.system.ExcelService;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.property.PropertyUserService;
import com.cwkj.scf.service.property.SupplierService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.beans.PropertyEditorSupport;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *控制层基类
 *
 * @author ljc
 *
 */
public class BaseAction {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());


	protected int PROMPTCODE_OK=1;
	protected int PROMPTCODE_FAIL=0;

	/**
	 * springContext
	 */
	private WebApplicationContext springContext;

	/**
	 * 发送异常邮件
	 * @param throwable 异常内容
	 * @param title 异常邮件标题
	 * @return
	 */
	public void sendExceptionMail(Throwable throwable,String title) {

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		try {
			throwable.printStackTrace(pw);

			ServletContext servletContext = ContextLoader.getCurrentWebApplicationContext().getServletContext();
			springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			NotifyService notifyService = (NotifyService) springContext.getBean("mailNotifyService");
			Map<String, String> param = new HashMap<String, String>();
			String greeting = String.format("系统异常(来自彩云倍服务)。", DateUtils.format(new Date(), "yyyy年MM月dd日hh时mm分ss秒"));
			param.put("greeting", greeting);
			param.put("content", sw.toString());
			param.put("company", "<div class='line w400'>彩云倍系统</div><br>此为系统邮件请勿回复");
			MailNotify notify = new MailNotify();
			notify.setAsync(true);
			notify.setMessageTemplate("mail_template_jcpt.ftl");
			notify.setRecievers("notification@xwckeji.com");
			notify.setSubject("系统异常：" + title);
			notify.setMessage(param);
			notifyService.send(notify);
		} catch (Exception ex) {
			logger.error("发送系统日志邮件异常", ex);
		}finally{
			try {
				pw.close();
			}catch (Exception e)
			{

			}
		}
	}
	 
	/**
	 * 设置提示业务异常信息
	 * @param view
	 * @param e
	 */
	protected void setPromptException(ModelAndView view,BusinessException e)
	{
		if(view!=null)
		view.addObject("error", e);
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected  <T> void setPromptException(JsonView<T> view,BusinessException e)
	{
		if(view!=null) {
			view.setCode(JsonView.CODE_FAILE);
			view.setMsg(e.getMessage());
		}
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected void setPromptException(ModelAndView view,Exception e)
	{
		if(view!=null) {
			if (e instanceof DataAccessException) {
				view.addObject("error", new BusinessException(BusinessException.code_other, "Fail [Error:1001]"));
			} else {
				view.addObject("error", new BusinessException(BusinessException.code_other, e.getMessage()));
			}
		}
		sendExceptionMail(e,e!=null?e.getMessage():"");
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected  <T> void setPromptException(JsonView<T> view,Exception e)
	{

		if(view!=null) {
			if (e instanceof DataAccessException) {
				view.setCode(JsonView.CODE_FAILE);
				view.setMsg("Fail [Error:1001]");
			} else {
				view.setCode(JsonView.CODE_FAILE);
				view.setMsg(e.getMessage());
			}
		}
		sendExceptionMail(e,e!=null?e.getMessage():"");
	}
	
	/**
	 * 设置提示信息
	 * @param view
	 * @param msg
	 */
	protected void setPromptMessage(ModelAndView view,String msg)
	{
		if(view!=null)
		view.addObject("promptMsg", msg);
	}

	/**
	 * 设置提示信息
	 * @param view
	 * @param msg 信息
	 * @param code 结果编号
	 */
	protected void setPromptMessage(ModelAndView view,Integer code,String msg)
	{
		if(view!=null) {
			view.addObject("promptMsg", msg);
			view.addObject("promptCode", code);
		}
	}

	/**
	 * 设置提示信息
	 * @param view
	 * @param code
	 * @param msg
	 */
	protected <T> void setPromptMessage(JsonView<T> view,Integer code, String msg)
	{
		if(view!=null) {
			view.setCode(code);
			view.setMsg(msg);
		}
	}
	

	//=========== 提供权限验证方法 ===========

	/**
	 * 
	 * getRequestToParamMap:将reuqest参数转换为map. <br>
	 *
	 * @author anxymf
	 * Date:2016年11月21日下午2:50:49 <br>
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, Object> getRequestToParamMap(
			HttpServletRequest request) {
	
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> keyNames = request.getParameterNames();
		if(StringUtil.isEmpty(request.getParameter("pageIndex"))){
			paramMap.put("pageIndex", "1");
		}
		if(StringUtil.isEmpty(request.getParameter("pageSize"))){
			paramMap.put("pageSize","20");
		}
		while (keyNames.hasMoreElements()) {
			String attrName = keyNames.nextElement();
			String attrValue = request.getParameter(attrName);
			if (StringUtils.isNotEmpty(attrValue)) {
				paramMap.put(attrName, attrValue.trim());
			}	
		}
		return paramMap;
	}	

	
	  @InitBinder
	  public void initBinder(WebDataBinder binder) {
		  
//	    /**
//	     * 注册一个自定义的编辑器，编辑器是日期类型
//	     * 使用自定义的日期编辑器，日期格式：yyyy-MM-dd,第二个参数为是否为空  true代表可以为空
//	     */
//	    binder.registerCustomEditor(Date.class, new CustomDateEditor(
//	        new SimpleDateFormat("yyyy-MM-dd"), true));
		  

		    /**
		     * 方式二：使用WebDataBinder注册一个自定义的编辑器，编辑器是日期类型
		     * 使用属性编辑器实现：重载setAsText,getAsText
		     */
		    binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
		 
		      @Override
		      public String getAsText() {
		        return new SimpleDateFormat("yyyy-MM-dd")
		            .format((Date) getValue());
		      }
		 
		      @Override
		      public void setAsText(String text) {
		        try {
		        	setValue(new SimpleDateFormat("yyyy-MM-dd").parse(text));
		        } catch (Exception e) {
		        	
		          e.printStackTrace();
		          try {
		        	  setValue(new Date(text));
		          }catch(Exception e1) {
		        	  e1.printStackTrace();
		        	  setValue(null);
		          }
		        }
		      }
		 
		    });
		    
	  }
	
}
