package com.cwkj.agreement.util;


import java.io.*;

import com.cwkj.jcpt.util.BusinessException;
import com.zqsign.core.Base64Utils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.resource.XMLResource;
import org.xml.sax.InputSource;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

/**
 * Pdf生成
 * @author ljc
 * @version 1.0
 */
public class PdfUtils {

    //	private static ITextRenderer renderer = null;
    private static Logger logger=LoggerFactory.getLogger(PdfUtils.class);

    private static ITextRenderer initITextRenderer()
    {
        ITextRenderer	renderer = new ITextRenderer();
        ITextFontResolver fontResolver = renderer.getFontResolver();
        //该文件夹下放所需字体文件
//			File fonts = new File(Thread.currentThread().getContextClassLoader().getResource("/fonts").getPath());
        File fonts = new File(PdfUtils.class.getResource("/fonts").getPath());
//		File fonts = new File("/workspace/idea/xwckeji/20190218-dev-personal-loan/scf/jcpt-service/agreement-web/src/main/resources/fonts");
        File[] fileList = fonts.listFiles();
        for(int i=0; i < fileList.length; i++){
            try {
                fontResolver.addFont(fileList[i].getAbsolutePath(),	BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return renderer;
    }

    /**
     * 保存HTML转换PDF文件
     * @param html
     * HTML字符串
     * @param savePath
     * 保存文件路径
     * @return
     */
    public static void saveHtml2Pdf(String html,String savePath)throws FileNotFoundException,DocumentException
    {
        OutputStream os = null;
        BufferedReader br = null;
        StringReader sr = null;
        ITextRenderer renderer=null;
        try{
            renderer=initITextRenderer();
            //logger.debug("html生成pdf:savePath={},html={}",savePath,html);
            sr = new StringReader(html);
            br = new BufferedReader(sr);
            InputSource is = new InputSource(br);
            Document dom = XMLResource.load(is).getDocument();
            dom.normalizeDocument();
            renderer.setDocument(dom, null);
            renderer.layout();
            os = new FileOutputStream(savePath);
            renderer.createPDF(os);


        }finally {
            IOUtils.closeQuietly(sr);
            IOUtils.closeQuietly(br);
            IOUtils.closeQuietly(os);
            renderer.finishPDF();
        }
    }

    public static String encodeStringPdf(String html)throws FileNotFoundException,DocumentException
    {
        OutputStream os = null;
        BufferedReader br = null;
        StringReader sr = null;
        ITextRenderer renderer=null;
        try {
            renderer = initITextRenderer();
            //logger.debug("html生成pdf:savePath={},html={}",savePath,html);
            sr = new StringReader(html);
            br = new BufferedReader(sr);
            InputSource is = new InputSource(br);
            Document dom = XMLResource.load(is).getDocument();
            dom.normalizeDocument();
            renderer.setDocument(dom, null);
            renderer.layout();
            os = new ByteArrayOutputStream();
            renderer.createPDF(os);
            String encodePDFStr = Base64Utils.encode(((ByteArrayOutputStream) os).toByteArray());
            return encodePDFStr;
        }catch (Exception ex)
        {
            logger.error("生成PDF文件Base64字符串异常：",ex);
            throw new BusinessException(ex);
        }finally {
            IOUtils.closeQuietly(sr);
            IOUtils.closeQuietly(br);
            IOUtils.closeQuietly(os);
            renderer.finishPDF();
        }
    }

    /**
     * 添加水印
     * @param inputFile
     * PDF文件路径
     * @param imageFile
     * 水印图片路径
     * @param outputFile
     * 输出PDF路径
     */
    public static void waterMark(String inputFile, String imageFile,String outputFile)throws FileNotFoundException,IOException,DocumentException {
        PdfReader reader = null;
        PdfStamper stamper = null;
        try {
            reader = new PdfReader(inputFile);
            stamper = new PdfStamper(reader, new FileOutputStream(outputFile));

            BaseFont base = BaseFont.createFont(Thread.currentThread().getContextClassLoader().getResource("/fonts").getPath() + "/SIMSUN.TTC,1", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// 使用系统字体
            int total = reader.getNumberOfPages() + 1;
            //水印
            Image image = Image.getInstance(imageFile);
            image.setAbsolutePosition(400, 50);
            PdfContentByte under;
            for (int i = 1; i < total; i++) {
                under = stamper.getUnderContent(i);
                under.beginText();
                under.setFontAndSize(base, 30);
                under.addImage(image);
                under.stroke();
            }
            stamper.close();


        } finally {
            reader.close();
        }


    }

}
