package com.cwkj.agreement.controller;

import com.cwkj.agreement.common.AgreementCommon;
import com.cwkj.agreement.util.FreeMarkerUtils;
import com.cwkj.agreement.util.PdfUtils;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.scf.model.agreement.AgreementEnterpriseRegisterDo;
import com.cwkj.scf.model.agreement.AgreementPersonRegisterDo;
import com.zqsign.core.ZQResult;
import com.zqsign.core.ZQSignUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 签章默认请求
 */
public class AgreementDefaultController {

    private static Logger logger= LoggerFactory.getLogger(AgreementDefaultController.class);

    @Autowired
    private ZQSignUtil zQSignUtil;

    /**
     * 小橙橙企业签章注册
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value="/xwcEnterperiseResgister.do")
    @ResponseBody
    public  String xwcEnterperiseResgister(HttpServletRequest request, HttpServletResponse response)
    {
        String resultHtml=null;
        try {

            ZQResult<String> zqResult = zQSignUtil.regEnterprise(AgreementCommon.XWCKEJI_REGID,
                    AgreementCommon.XWCKEJI_ENTERPRISENAME,
                    AgreementCommon.XWCKEJI_CERTIFICATE,
                    AgreementCommon.XWCKEJI_ADDRESS,
                    AgreementCommon.XWCKEJI_CONTACT,
                    AgreementCommon.XWCKEJI_MOBILE);

            String zqCode=zqResult.getCode();
            if(ZQResult.CODE_SUCCESS.equals( zqCode) || "120000".equals(zqCode))
            {
                resultHtml="注册成功("+zqResult.getMsg()+")";
            }else{
                resultHtml=zqResult.getMsg();
            }



        }catch (IOException ex)
        {
            logger.error("签章异常",ex);
            resultHtml=ex.getMessage();
        }catch (BusinessException e)
        {
            logger.info("获取个贷个人贷款居间服务合同HTML失败:{}",e.getMessage());
            resultHtml="获取个贷个人贷款居间服务合同HTML失败:"+e.getMessage();
        }catch (Exception ex)
        {
            logger.error("获取个贷个人贷款居间服务合同HTML异常",ex);
            resultHtml="获取个贷个人贷款居间服务合同HTML异常";
        }
        return resultHtml;

    }
}
