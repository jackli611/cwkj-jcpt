package com.cwkj.agreement.controller;

import com.cwkj.agreement.base.BaseAction;
import com.cwkj.agreement.common.AgreementCommon;
import com.cwkj.agreement.util.FreeMarkerUtils;
import com.cwkj.agreement.util.PdfUtils;
import com.cwkj.jcpt.util.*;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.model.agreement.AgreementEnterpriseRegisterDo;
import com.cwkj.scf.model.agreement.AgreementPersonRegisterDo;
import com.cwkj.scf.model.agreement.AgreementSignDo;
import com.cwkj.scf.service.agreement.AgreementEnterpriseRegisterService;
import com.cwkj.scf.service.agreement.AgreementPersonRegisterService;
import com.cwkj.scf.service.agreement.AgreementSignService;
import com.cwkj.scf.service.agreement.PersonalLoanAgreementService;
import com.zqsign.core.ZQResult;
import com.zqsign.core.ZQSignConfig;
import com.zqsign.core.ZQSignUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 个贷居间服务合同
 */
@Controller
public class PersonLoanServerAgrtController extends BaseAction {

    private static Logger logger= LoggerFactory.getLogger(PersonLoanServerAgrtController.class);

    @Autowired
    private AgreementPersonRegisterService agreementPersonRegisterService;
    @Autowired
    private AgreementSignService agreementSignService;
    @Autowired
    private PersonalLoanAgreementService personalLoanAgreementService;
    @Autowired
    private ZQSignUtil zQSignUtil;


    @RequestMapping(value="/createPersonServerAgrt",produces="text/html;charset=UTF-8")
    @ResponseBody
    public  String personHtmlAgrt(HttpServletRequest request, HttpServletResponse response, String idcardNo, String userName, String contractNo, String mobile)
    {
        String resultHtml=null;
        try {
            AssertUtils.isNotBlank(idcardNo, "个人身份证号码不能为空");
            AssertUtils.isNotBlank(userName, "个人姓名不能为空");
            AssertUtils.isNotBlank(contractNo, "合同号不能为空");
            AssertUtils.isNotBlank(mobile, "手机号不能为空");

            AgreementSignDo signContract= agreementSignService.findAgreementSignByContractNo(contractNo);
            String regId = AgreementCommon.createPersonRegId( idcardNo , mobile);
            if(signContract==null) {
                AgreementPersonRegisterDo personRegisterDo = agreementPersonRegisterService.findAgreementPersonRegisterById(regId);
                if (personRegisterDo == null) {
                    ZQResult<String> zqResult = zQSignUtil.personReg(regId, userName, idcardNo, mobile);
                    String zqCode = zqResult.getCode();
                    if (ZQResult.CODE_SUCCESS.equals(zqCode) || "120000".equals(zqCode)) {
                        personRegisterDo = new AgreementPersonRegisterDo();
                        personRegisterDo.setIdcardNo(idcardNo);
                        personRegisterDo.setMobile(mobile);
                        personRegisterDo.setRealName(userName);
                        personRegisterDo.setId(regId);
                        Integer dbRes = agreementPersonRegisterService.insertAgreementPersonRegister(personRegisterDo);
                        AssertUtils.isTrue(dbRes != null && dbRes.intValue() > 0, "保存用户签章注册信息失败");
                    }
                }

                signContract=new AgreementSignDo();
                signContract.setContractNo(contractNo);
                signContract.setUploadPdfStatus(AgreementSignDo.STATUS_NOT);
                signContract.setSignStatus(AgreementSignDo.STATUS_NOT);
                signContract.setCompletionStatus(AgreementSignDo.STATUS_NOT);
                signContract.setTemplateFile("peronalLoanServerAgrt.ftl");
                signContract.setSignData("idcardNo="+idcardNo+",userName="+userName+",contractNo="+contractNo+",mobile="+mobile);
                Integer dbRes=agreementSignService.insertAgreementSign(signContract);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存合同信息失败");
            }

            AssertUtils.isNotNull(signContract,"合同签单失败");
            //检查要签章合同是否已上传成功过
            if(!AgreementSignDo.STATUS_YES.equals( signContract.getUploadPdfStatus()))
            {
                Map<String, String> argtData = new HashMap<String, String>();
                argtData.put("contractNo", contractNo);
                argtData.put("userNameJF", userName);
                argtData.put("idCardNoJF", idcardNo);
                AgreementCommon.putXWCEnterpriseAgreementInfo(argtData);
                argtData.put("signedDate", DateUtils.format(DateUtils.getCurrentDate(), "yyyy年MM月dd日"));
                BaseResult<String> result = FreeMarkerUtils.getHtml(argtData, "peronalLoanServerAgrt.ftl");
                AssertUtils.isTrue(result != null && result.getCode().equals(result.CODE_SUCCESS), "生成合同本档失败");
                String base64Pdf = PdfUtils.encodeStringPdf(result.getData());
                ZQResult<String> uploadPdfResult = zQSignUtil.uploadPdfBase64Str(base64Pdf, contractNo, "个贷居间服务合同");
                AssertUtils.isTrue((ZQResult.CODE_SUCCESS.equals(uploadPdfResult.getCode()) || "120001".equals(uploadPdfResult.getCode())), "上传签章合同失败:" + (uploadPdfResult == null ? "" : uploadPdfResult.getMsg()));

                Integer dbRes= agreementSignService.updateUploadPdfStatus(contractNo,AgreementSignDo.STATUS_YES);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存上传合同标识失败");
            }
            //检查要签章合同是否已签名过
            if(!AgreementSignDo.STATUS_YES.equals(signContract.getSignStatus()))
            {
                ZQResult<String> signAutoPdfResult = zQSignUtil.signAutoCustom(contractNo, "{'" + regId + "':'" + userName + "（盖章）','" + AgreementCommon.XWCKEJI_REGID + "':'" + AgreementCommon.XWCKEJI_ENTERPRISENAME + "（盖章）'}");
                AssertUtils.isTrue(ZQResult.CODE_SUCCESS.equals(signAutoPdfResult.getCode()), "合同签章失败:" + (signAutoPdfResult == null ? "" : signAutoPdfResult.getMsg()));
                Integer dbRes= agreementSignService.updateSignStatus(contractNo,AgreementSignDo.STATUS_YES);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存合同已签章标识失败");
            }
            //检查要签章合同是否已生效
            if(!AgreementSignDo.STATUS_YES.equals(signContract.getCompletionStatus()))
            {
                ZQResult<String> signAutoPdfResult = zQSignUtil.completionContract(contractNo);
                AssertUtils.isTrue(ZQResult.CODE_SUCCESS.equals(signAutoPdfResult.getCode()), "合同签章生效请求失败:" + (signAutoPdfResult == null ? "" : signAutoPdfResult.getMsg()));
                Integer dbRes= agreementSignService.updateCompletionStatus(contractNo,AgreementSignDo.STATUS_YES);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存合同已签章生效标识失败");
            }

            OutputStream outputStream = response.getOutputStream();
            zQSignUtil.downloadPdf2OutputStream(contractNo, outputStream);
            outputStream.close();

        }catch (BusinessException e)
        {
            logger.info("获取个贷个人贷款居间服务合同HTML失败:{}",e.getMessage());
            resultHtml="获取个贷个人贷款居间服务合同HTML失败:"+e.getMessage();
            sendExceptionMail(e,"请求个贷居间服务异常,contractNo="+contractNo);
        }catch (Exception ex)
        {
            logger.error("获取个贷个人贷款居间服务合同HTML异常",ex);
            resultHtml="获取个贷个人贷款居间服务合同HTML异常";
            sendExceptionMail(ex,"请求个贷居间服务异常,contractNo="+contractNo);
        }
        return resultHtml;

    }

    /**
     * 根据loadId获取合同
     * @param request
     * @param response
     * @param loanId
     * @return
     */
    @RequestMapping(value="/serverPersonAgrt.do",produces="text/html;charset=UTF-8")
    @ResponseBody
    public  String serverPersonAgrt(HttpServletRequest request, HttpServletResponse response, Long loanId) {
        String resultHtml = null;
        try {
                Map<String,String> contractData=personalLoanAgreementService.serverContractDataByLoanId(loanId);
            String idcardNo=contractData.get("idcardNo");
            String userName=contractData.get("realName");
            String contractNo=contractData.get("contractNo");
            String mobile=contractData.get("mobile");
                resultHtml=personHtmlAgrt(request,response,idcardNo,userName,contractNo,mobile);

        }catch (Exception ex)
        {
            logger.error("获取个贷个人贷款居间服务合同HTML异常",ex);
            resultHtml="获取个贷个人贷款居间服务合同HTML异常";
            sendExceptionMail(ex,"请求个贷居间服务异常,loanId="+loanId);
        }
        return resultHtml;
    }


    @RequestMapping(value="/registerPersonHtmlAgrt",produces="text/html;charset=UTF-8")
    @ResponseBody
    public  String registerPersonHtmlAgrt(HttpServletRequest request, HttpServletResponse response, String idcardNo, String userName, String contractNo, String mobile)
    {
        String resultHtml=null;
        try {
            AssertUtils.isNotBlank(idcardNo, "个人身份证号码不能为空");
            AssertUtils.isNotBlank(userName, "个人姓名不能为空");
            AssertUtils.isNotBlank(contractNo, "合同号不能为空");
            AssertUtils.isNotBlank(mobile, "手机号不能为空");

            AgreementSignDo signContract= agreementSignService.findAgreementSignByContractNo(contractNo);
            String regId = AgreementCommon.createPersonRegId( idcardNo , mobile);
            if(signContract==null) {
                AgreementPersonRegisterDo personRegisterDo = agreementPersonRegisterService.findAgreementPersonRegisterById(regId);
                if (personRegisterDo == null) {
                    ZQResult<String> zqResult = zQSignUtil.personReg(regId, userName, idcardNo, mobile);
                    String zqCode = zqResult.getCode();
                    if (ZQResult.CODE_SUCCESS.equals(zqCode) || "120000".equals(zqCode)) {
                        personRegisterDo = new AgreementPersonRegisterDo();
                        personRegisterDo.setIdcardNo(idcardNo);
                        personRegisterDo.setMobile(mobile);
                        personRegisterDo.setRealName(userName);
                        personRegisterDo.setId(regId);
                        Integer dbRes = agreementPersonRegisterService.insertAgreementPersonRegister(personRegisterDo);
                        AssertUtils.isTrue(dbRes != null && dbRes.intValue() > 0, "保存用户签章注册信息失败");
                    }
                }

                signContract=new AgreementSignDo();
                signContract.setContractNo(contractNo);
                signContract.setUploadPdfStatus(AgreementSignDo.STATUS_NOT);
                signContract.setSignStatus(AgreementSignDo.STATUS_NOT);
                signContract.setCompletionStatus(AgreementSignDo.STATUS_NOT);
                signContract.setTemplateFile("peronalLoanRegisterAgrt.ftl");
                signContract.setSignData("idcardNo="+idcardNo+",userName="+userName+",contractNo="+contractNo+",mobile="+mobile);

                Integer dbRes=agreementSignService.insertAgreementSign(signContract);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存合同信息失败");
            }

            AssertUtils.isNotNull(signContract,"合同签单失败");
            //检查要签章合同是否已上传成功过
            if(!AgreementSignDo.STATUS_YES.equals( signContract.getUploadPdfStatus()))
            {
                Map<String, String> argtData = new HashMap<String, String>();
                argtData.put("contractNo", contractNo);
                argtData.put("userNameJF", userName);
                argtData.put("idCardNoJF", idcardNo);
                AgreementCommon.putXWCEnterpriseAgreementInfo(argtData);
                argtData.put("signedDate", DateUtils.format(DateUtils.getCurrentDate(), "yyyy年MM月dd日"));
                BaseResult<String> result = FreeMarkerUtils.getHtml(argtData, "peronalLoanRegisterAgrt.ftl");
                AssertUtils.isTrue(result != null && result.getCode().equals(result.CODE_SUCCESS), "生成合同本档失败");
                String base64Pdf = PdfUtils.encodeStringPdf(result.getData());
                ZQResult<String> uploadPdfResult = zQSignUtil.uploadPdfBase64Str(base64Pdf, contractNo, "会员注册协议");
                AssertUtils.isTrue((ZQResult.CODE_SUCCESS.equals(uploadPdfResult.getCode()) || "120001".equals(uploadPdfResult.getCode())), "上传签章合同失败:" + (uploadPdfResult == null ? "" : uploadPdfResult.getMsg()));

                Integer dbRes= agreementSignService.updateUploadPdfStatus(contractNo,AgreementSignDo.STATUS_YES);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存上传合同标识失败");
            }
            //检查要签章合同是否已签名过
            if(!AgreementSignDo.STATUS_YES.equals(signContract.getSignStatus()))
            {
                ZQResult<String> signAutoPdfResult = zQSignUtil.signAutoCustom(contractNo, "{'" + regId + "':'" + userName + "（盖章）','" + AgreementCommon.XWCKEJI_REGID + "':'" + AgreementCommon.XWCKEJI_ENTERPRISENAME + "（盖章）'}");
                AssertUtils.isTrue(ZQResult.CODE_SUCCESS.equals(signAutoPdfResult.getCode()), "合同签章失败:" + (signAutoPdfResult == null ? "" : signAutoPdfResult.getMsg()));
                Integer dbRes= agreementSignService.updateSignStatus(contractNo,AgreementSignDo.STATUS_YES);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存合同已签章标识失败");
            }
            //检查要签章合同是否已生效
            if(!AgreementSignDo.STATUS_YES.equals(signContract.getCompletionStatus()))
            {
                ZQResult<String> signAutoPdfResult = zQSignUtil.completionContract(contractNo);
                AssertUtils.isTrue(ZQResult.CODE_SUCCESS.equals(signAutoPdfResult.getCode()), "合同签章生效请求失败:" + (signAutoPdfResult == null ? "" : signAutoPdfResult.getMsg()));
                Integer dbRes= agreementSignService.updateCompletionStatus(contractNo,AgreementSignDo.STATUS_YES);
                AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存合同已签章生效标识失败");
            }

            OutputStream outputStream = response.getOutputStream();
            zQSignUtil.downloadPdf2OutputStream(contractNo, outputStream);
            outputStream.close();

        }catch (BusinessException e)
        {
            logger.info("获取个贷个人贷款居间服务合同HTML失败:{}",e.getMessage());
            resultHtml="获取个贷个人贷款居间服务合同HTML失败:"+e.getMessage();
            sendExceptionMail(e,"请求个贷注册协议失败，idcardNo="+idcardNo);
        }catch (Exception ex)
        {
            logger.error("获取个贷个人贷款居间服务合同HTML异常",ex);
            resultHtml="获取个贷个人贷款居间服务合同HTML异常";
            sendExceptionMail(ex,"请求个贷注册协议异常,idcardNo="+idcardNo);
        }
        return resultHtml;

    }
    /**
     * 根据loadId获取合同
     * @param request
     * @param response
     * @param loanId
     * @return
     */
    @RequestMapping(value="/registerPersonAgrt.do",produces="text/html;charset=UTF-8")
    @ResponseBody
    public  String registerPersonAgrt(HttpServletRequest request, HttpServletResponse response, Long loanId) {
        String resultHtml = null;
        try {
            Map<String,String> contractData=personalLoanAgreementService.serverContractDataByLoanId(loanId);
            String idcardNo=contractData.get("idcardNo");
            String userName=contractData.get("realName");
            //String contractNo=contractData.get("contractNo");
            String mobile=contractData.get("mobile");
            String contractNo="USERREGPLOAN"+contractData.get("userId");//注册用户就使用用户ID吧
            resultHtml=registerPersonHtmlAgrt(request,response,idcardNo,userName,contractNo,mobile);

        }catch (Exception ex)
        {
            logger.error("获取个贷个人贷款居间服务合同HTML异常",ex);
            resultHtml="获取个贷个人贷款居间服务合同HTML异常";
            sendExceptionMail(ex,"请求个贷注册协议异常,loanId="+loanId);
        }
        return resultHtml;
    }

}
