package com.cwkj.agreement.util;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.DateUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TemplateTest {

    public static  void wordHtmlToPDF()
    {
        try {
            //========== 数据 ====================
            Map<String, Object> argtData=new HashMap<String,Object>();
argtData.put("contractNo","SCF20190315001201");
argtData.put("userNameJF","鲍道城");
argtData.put("idCardNoJF","350125198411154151");
            argtData.put("userNameYF","深圳市小物橙科技有限公司");
            argtData.put("signedDate", DateUtils.format(DateUtils.getCurrentDate(),"yyyy年MM月dd日"));
//            BaseResult<String> result = FreeMarkerUtils.getHtml(argtData, "peronalLoanServerAgrt.ftl");
            BaseResult<String> result = FreeMarkerUtils.getHtml(argtData, "peronalLoanRegisterAgrt.ftl");
            if(result!=null && result.getCode().equals(result.CODE_SUCCESS))
            {
                System.out.println(result.getData());
                PdfUtils.saveHtml2Pdf(result.getData(), "/workspace/temp/test-peronalLoanRegisterAgrt.pdf");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args)
    {
        wordHtmlToPDF();
    }
}
