package com.cwkj.agreement.common;

import java.util.HashMap;
import java.util.Map;

public class AgreementCommon {

    public static String XWCKEJI_REGID="91440300MA5FEXJD32";

    /**
     * 小物橙企业名称
     */
    public static String XWCKEJI_ENTERPRISENAME="深圳市小物橙科技有限公司";

    /**
     * 小物橙统一社会信用代码
     */
    public static String XWCKEJI_CERTIFICATE="91440300MA5FEXJD32";
    /**
     * 小物橙注册地址
     */
    public static String XWCKEJI_ADDRESS="深圳市宝安区新安街道海富社区46区翻身路147号卡罗社区2B606";
    /**
     * 小物橙联系人
     */
    public static String XWCKEJI_CONTACT="陈芝瑛";
    /**
     * 小物橙联系方式
     */
    public static String XWCKEJI_MOBILE="15019238715";

    public static String  createPersonRegId(String idcardNo,String mobile)
    {
        return idcardNo+"-"+mobile;
    }

    public static  Map<String,String> putXWCEnterpriseAgreementInfo(Map<String, String> argtData)
    {
        argtData.put("XWCKEJI_ENTERPRISENAME",XWCKEJI_ENTERPRISENAME);
        argtData.put("XWCKEJI_CERTIFICATE",XWCKEJI_CERTIFICATE);
        argtData.put("XWCKEJI_ADDRESS",XWCKEJI_ADDRESS);
        argtData.put("XWCKEJI_CONTACT",XWCKEJI_CONTACT);
        argtData.put("XWCKEJI_MOBILE",XWCKEJI_MOBILE);
        return argtData;
    }
}
