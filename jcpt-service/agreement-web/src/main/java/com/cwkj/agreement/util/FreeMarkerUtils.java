package com.cwkj.agreement.util;


import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cwkj.jcpt.util.BaseResult;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * HTML模板生成
 * @author ljc
 * @version 1.0
 */
public class FreeMarkerUtils {
    private static Logger logger=LoggerFactory.getLogger(FreeMarkerUtils.class);

    private static Configuration cfg;

    private static Configuration getCfg() {
        if (cfg == null) {
            cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
            cfg.setDefaultEncoding("UTF-8");
//			cfg.setClassLoaderForTemplateLoading(FreeMarkerUtils.class, "/templates");
            cfg.setClassForTemplateLoading(FreeMarkerUtils.class, "/templates");
            cfg.setNumberFormat("0.######");
            cfg.setTemplateUpdateDelay(0);
        }
        return cfg;
    }

    /**
     * 获取HTML
     * @param paramMap
     * 模板参数
     * @param filename
     * 模板文件名
     * @return
     */
    public static BaseResult<String> getHtml(Map paramMap, String filename) {
        return getHtml(paramMap, filename, "utf-8");
    }

    /**
     * 获取HTML
     * @param paramMap
     * 模板参数
     * @param filename
     * 模板文件名
     * @param charset
     * 字符串编码
     * @return
     */
    public static BaseResult<String> getHtml(Map paramMap, String filename, String charset) {
        BaseResult<String> result=new BaseResult<String>();
        Configuration cfg =null;
        Template t;
        try {
            cfg= getCfg();
            t = cfg.getTemplate(filename, charset);
            Writer out = new StringWriter();
            t.process(paramMap, out);
            result.setCode(result.CODE_SUCCESS);
            result.setData(out.toString());
            result.setMsg("success");
            return result;
        } catch (Exception e) {
            logger.error("获取HTML模板生成异常", e);
            result.setCode(result.CODE_FAILE);
            result.setMsg(e.getMessage());
        }
        return result;
    }
}
