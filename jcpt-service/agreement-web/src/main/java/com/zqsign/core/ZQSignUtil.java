package com.zqsign.core;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class ZQSignUtil {

    private static Logger logger= LoggerFactory.getLogger(ZQSignUtil.class);

    private ZQSignConfig config;


    public void setConfig(ZQSignConfig config)
    {
        this.config=config;
    }

    public ZQSignConfig getConfig()
    {
        return this.config;
    }

    private String httpRequestZQ(String url,Map<String, String> map)throws IOException
    {
        AssertUtils.notNull(config,"配置不能为空");
        EncryptData ed = new EncryptData();
        map.put("zqid", config.getZqId());//商户的zqid,该值需要与private_key对应
        String sign_val = ed.encrptData(map, config.getPrivateKey());
        map.put("sign_val", sign_val); //请求参数的签名值
        String singUrl=config.getRequestUrl()+ url;
        String response_str = HttpRequest.sendPost(singUrl, map);//向服务端发送请求，并接收请求结果
        logger.info("请求结果[{}]：{}" , singUrl,response_str);//输出服务器响应结果
        return response_str;
    }

    private byte[] httpSendPostReturnByte(String url,Map<String, String> map)throws IOException
    {
        EncryptData ed = new EncryptData();
        map.put("zqid", config.getZqId());//商户的zqid,该值需要与private_key对应
        String sign_val = ed.encrptData(map, config.getPrivateKey());
        map.put("sign_val", sign_val); //请求参数的签名值
        String singUrl=config.getRequestUrl()+ url;
        byte[] response_str =  HttpRequest.sendPostReturnByte(singUrl, map);//向服务端发送请求，并接收请求结果
        logger.info("请求结果[{}]" , singUrl);//输出服务器响应结果
        return response_str;
    }

    /**
     * 个人注册数字证书
     * @param userId 用户唯一标示，该值不能重复
     * @param userName 平用户姓名
     * @param idCardNo 身份证号
     * @param mobile 联系手机号码
     * @return
     */
    public  ZQResult<String> personReg(String userId,String userName, String idCardNo, String mobile) {

        ZQResult<String> result = new ZQResult<String>();
        try {
            EncryptData ed = new EncryptData();
            Map<String, String> map = new HashMap<String, String>();
            map.put("user_code", userId);//用户唯一标示，该值不能重复
            map.put("name", userName);//平台方用户姓名
            map.put("id_card_no", idCardNo);//身份证号
            map.put("mobile",mobile);//联系电话（手机号码）
            String response_str = httpRequestZQ("personReg", map);//向服务端发送请求，并接收请求结果
            logger.info("请求结果：{}" , response_str);//输出服务器响应结果
            ZQResult<String> jsonObj = JSONObject.parseObject(response_str, ZQResult.class);
            return jsonObj;
        } catch (Exception ex) {
            result = new ZQResult<String>();
            result.setCode(ZQResult.CODE_FAILE);
            result.setMsg(ex.getMessage());
        }
        return result;
    }

    /**
     * 企业注册数字证书
     * @param enterpriseId 用户唯一标示，该值不能重复
     * @param enterprise 企业名称
     * @param certificate 营业执照号或社会统一代码
     * @param address 企业注册地址
     * @param contact 联系人
     * @param mobile 联系电话（手机号码）
     * @return
     * @throws IOException
     */
    public  ZQResult<String> regEnterprise(String enterpriseId,String enterprise,String certificate,String address,String contact,String mobile) throws IOException {


        EncryptData ed = new EncryptData();

        Map<String,String> map = new HashMap<String,String>();

        map.put("user_code",enterpriseId);//用户唯一标示，该值不能重复
        map.put("name", enterprise);//企业名称
        map.put("certificate", certificate);//营业执照号或社会统一代码
        map.put("address", address); //企业注册地址
        map.put("contact", contact);//联系人
        map.put("mobile", mobile);//联系电话（手机号码）


        String response_str = httpRequestZQ("entpReg", map);//向服务端发送请求，并接收请求结果
        logger.info("请求结果：{}" , response_str);//输出服务器响应结果
        ZQResult<String> jsonObj = JSONObject.parseObject(response_str, ZQResult.class);
        return jsonObj;
    }


    /**
     * 上传PDF合同模板
     * @param pdfFile PDF文件路径
     * @param contractNo 合同编号
     * @param merName 商户平台合同名称
     * @return
     * @throws Exception
     */
    public  ZQResult<String> uploadPdf(String pdfFile,String contractNo,String merName) throws Exception{

        EncryptData ed = new EncryptData();

        Map<String, String> map = new HashMap<String, String>();
        byte[] fileToByte = Base64Utils.fileToByte(pdfFile);//转义字符
        String encode = Base64Utils.encode(fileToByte);
        map.put("no",contractNo);//自行创建合同编号，该值不可重复使用
        map.put("name", merName);//商户平台合同名称
        map.put("contract", encode);//合同文件的base64/平台方合同文件的网络地址

        String response_str = httpRequestZQ( "uploadPdf", map);//向服务端发送请求，并接收请求结果
        ZQResult<String> result=JSONObject.parseObject(response_str,ZQResult.class);
        return result;
    }

    /**
     * 上传PDF base64编码合同文件模板
     * @param pdfBase64Str PDF文件的Base64编码
     * @param contractNo 合同编号
     * @param merName 商户平台合同名称
     * @return
     * @throws Exception
     */
    public   ZQResult<String> uploadPdfBase64Str(String pdfBase64Str,String contractNo,String merName) throws Exception{

        EncryptData ed = new EncryptData();

        Map<String, String> map = new HashMap<String, String>();
        String encode =pdfBase64Str;
        map.put("no",contractNo);//自行创建合同编号，该值不可重复使用
        map.put("name", merName);//商户平台合同名称
        map.put("contract", encode);//合同文件的base64/平台方合同文件的网络地址


        String response_str =httpRequestZQ( "uploadPdf", map);//向服务端发送请求，并接收请求结果
        ZQResult<String> result=JSONObject.parseObject(response_str,ZQResult.class);
        return result;
    }

    /**
     * 下载合同PDF
     * @param contractNo 合同编号
     * @param saveFileName 下载保存地址
     * @throws Exception
     */
    public void downloadPdf(String contractNo,String saveFileName) throws Exception {

        EncryptData ed = new EncryptData();
        Map<String, String> map = new HashMap<String, String>();
        map.put("no", contractNo);//已存在的合同编号
        byte[] response_str = httpSendPostReturnByte( "getPdf", map);
        FileOutputStream fileOutputStream = new FileOutputStream(saveFileName);//存储路径，例：F://test//1.pdf
        IOUtils.write(response_str, fileOutputStream);
        logger.info("当前文件流已转成PDF文件放在"+saveFileName);

    }

    /**
     * 下载合同PDF到输出流
     * @param contractNo 合同编号
     * @param outputStream 输出流
     */
    public void downloadPdf2OutputStream(String contractNo, OutputStream outputStream) {

        try {
            EncryptData ed = new EncryptData();
            Map<String, String> map = new HashMap<String, String>();
            map.put("no", contractNo);//已存在的合同编号
            byte[] response_str = httpSendPostReturnByte("getPdf", map);
            IOUtils.write(response_str, outputStream);
        }catch (Exception ex)
        {
            throw new BusinessException(ex);
        }

    }

    /**
     * 自动签署
     * @param contractNo 合同编号
     * @param signers 签署人user_code，如（zquser_001,zquser_002）
     * @throws Exception
     */
    public ZQResult<String> signAuto(String contractNo,String signers) throws Exception{
        EncryptData ed = new EncryptData();
        Map<String, String> map = new HashMap<String, String>();
        map.put("no", contractNo);//已存在的合同编号
        map.put("signers", signers);//签署人user_code
        String response_str = httpRequestZQ("signAuto", map);//向服务端发送请求，并接收请求结果
        logger.info("请求结果：{}" , response_str);//输出服务器响应结果
        ZQResult<String> result=JSONObject.parseObject(response_str,ZQResult.class);
        return result;
    }

    /**
     * 关键字签署
     * @param contractNo 合同编号
     * @param signers 签署人user_code，如(zquser_001：张三,zquser_002：李四)
     * @return
     * @throws Exception
     */
    public ZQResult<String> signAutoCustom(String contractNo,String signers) throws Exception{
        EncryptData ed = new EncryptData();
        Map<String, String> map = new HashMap<String, String>();
        map.put("no", contractNo);//已存在的合同编号
        map.put("signers", signers);//签署人user_code

        String response_str = httpRequestZQ("signAutoCustom", map);//向服务端发送请求，并接收请求结果
        logger.info("请求结果：" ,response_str);//输出服务器响应结果
        ZQResult<String> result=JSONObject.parseObject(response_str,ZQResult.class);
        return result;
    }

    /**
     *骑缝章签署
     * @param contractNo 合同编号
     * @param signers 签署人user_code，如（zquser_001,zquser_002）
     * @throws Exception
     */
    public ZQResult<String> signCheckMark(String contractNo,String signers) throws Exception{
        EncryptData ed = new EncryptData();
        Map<String, String> map = new HashMap<String, String>();
        map.put("no", contractNo);//已存在的合同编号
        map.put("signers", signers);//签署人user_code

        String response_str = httpRequestZQ("signCheckMark", map);//向服务端发送请求，并接收请求结果
        logger.info("请求结果：{}" ,response_str);//输出服务器响应结果
        ZQResult<String> result=JSONObject.parseObject(response_str,ZQResult.class);
        return result;
    }

    /**
     * 合同生效标记
     * @param contractNo
     * @throws Exception
     */
    public  ZQResult<String> completionContract(String contractNo) throws Exception{
        EncryptData ed = new EncryptData();
        Map<String, String> map = new HashMap<String, String>();
        map.put("no", contractNo);//已存在的合同编号

        String response_str = httpRequestZQ("completionContract", map);//向服务端发送请求，并接收请求结果
        logger.info("请求结果：{}" ,response_str);//输出服务器响应结果
        ZQResult<String> result=JSONObject.parseObject(response_str,ZQResult.class);
        return result;
    }


    public static void main(String[] args) throws IOException {

        try {
            String contractNo="TEST00005";
            String userId="350125198411154151";
            String enterpriseId="91440300MA5FEXJD32";
            String enterpriseName="深圳市小物橙科技有限公司";
            String certificate="91440300MA5FEXJD32";//统一社会信用代码
            String address="深圳市宝安区新安街道海富社区46区翻身路147号卡罗社区2B606";
            String contact="陈芝瑛";
            String mobile="15019238715";
            String pdfFile="/workspace/temp/test-peronalLoanServerAgrt.pdf";
            ZQSignUtil zqSignUtil=new ZQSignUtil();
            ZQSignConfig config=new ZQSignConfig();
            config.setZqId("ZQE5D4B5CF62104307B6F005B1AB5F3CAF");
            config.setPrivateKey("MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJvxRzAASyFy9R/ML3 Pp4fggDLIyf2I0XekDxDn3vyZ2OtKVL0teA3OL2yS2plb8cVHlpyhMnfxQWcnYH8DA/SEc wq0n91Grj/fTjHqzCLfa5/t9LYVzWBcHMGZ5+OKbBFOTKyVOegzOQnLFxYhh+GHfydXF bLgrWt3zkFR1qSvZAgMBAAECgYBnYy3pOyiUgpzVehAz72orkUQZmLUvrLxeUmr0a7bF jzksiFAUzb3IC3889gOOREv1Gica5sEPmU4OLkXPC6RGGMpuUHeIJ0mfq+MaxWIiI2qm 26o9IBQZ8sJSGjqdKDFHuKKOJ0K5aP2qptg6vsHvbQTUPSNpsla8WwI+G/JdIQJBAMtzX eTI98R53TPwlFRe35NEIp20lVN74QT97iR0mVrBNzps5dY8I4hggfeMuFRxvOVV4ppzFY/ 3wytSvbmXe40CQQDEOI+GdHLR/rItDjKPXyyzXJDxkM6ZG1kAQFKTcosm+87tsuJDFKE Nd8WgkRwuZfqj0WYPCcMYkv1EADaCFzh9AkAzkQs0aOe76fNyLcE63U0nw3ZOqK3M mjJ/lGke3lrcUOrRqTZcJaQP9f4bI5S4+pcZPbqQ0Sie8/qI6ZDjEI0lAkEAgMxHmZASVjkCp 4RsN6PXaZxXH9lXYzSKRRnzmglTwbED8Q68ah++X2LA/FaD0wjpbmJixXg48YZ6bApfvS CU2QJBAK8pbvA4cnGkb8qbwekTMC9HIeCMlq1++TynorhcLSgeIdF45HlUGOnpOXC6p Bs1n8ZIpAfpcVQNKarA5hqMTiQ=");
            config.setPublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYBCeqme8yGq3ok0svBE0XRPoH uOMhu/JFze/szSiJ9z4QW1k8XoCHiRO/pAmZ+jUPrLzo+owf1SN2STGBrWNl2C3smTHp VvSZYxcOzglam94OAJ/ZSWJ3yFc7d/QYBFFHzcCjX3Jpes/laPD9rO8C1yi1OIXUTIgzjBVYY EAT6wIDAQAB");
            config.setRequestUrl("http://signtest.zqsign.com/");
            zqSignUtil.setConfig(config);
           // ZQResult<String> result = zqSignUtil.personReg("350125198411154151", "鲍道城", "350125198411154151", "13655003303");
            //System.out.println("code=" + result.getCode() + ",msg=" + result.getMsg());//{"code":"120000","msg":"user_code已存在，无法推送"}

          //  zqSignUtil.regEnterprise(enterpriseId,enterpriseName,certificate,address,contact,mobile);
            //zqSignUtil.uploadPdf(pdfFile, contractNo, "服务协议");
//            zqSignUtil.downloadPdf(contractNo,"/workspace/temp/zq-"+contractNo+"-2.pdf");
//            zqSignUtil.signAuto(contractNo, userId+","+enterpriseId);
//            zqSignUtil.signAutoCustom(contractNo,"{'"+userId+"':'鲍道城','"+enterpriseId+"':'"+enterpriseName+"'}");
            zqSignUtil.completionContract(contractNo);
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
