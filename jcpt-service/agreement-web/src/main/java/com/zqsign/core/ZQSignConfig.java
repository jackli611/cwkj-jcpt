package com.zqsign.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 配置文件
 */
public class ZQSignConfig {

//    public static final String ZQID = "ZQE5D4B5CF62104307B6F005B1AB5F3CAF";
//
//    public static final String PRIVATEKEY="MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJvxRzAASyFy9R/ML3 Pp4fggDLIyf2I0XekDxDn3vyZ2OtKVL0teA3OL2yS2plb8cVHlpyhMnfxQWcnYH8DA/SEc wq0n91Grj/fTjHqzCLfa5/t9LYVzWBcHMGZ5+OKbBFOTKyVOegzOQnLFxYhh+GHfydXF bLgrWt3zkFR1qSvZAgMBAAECgYBnYy3pOyiUgpzVehAz72orkUQZmLUvrLxeUmr0a7bF jzksiFAUzb3IC3889gOOREv1Gica5sEPmU4OLkXPC6RGGMpuUHeIJ0mfq+MaxWIiI2qm 26o9IBQZ8sJSGjqdKDFHuKKOJ0K5aP2qptg6vsHvbQTUPSNpsla8WwI+G/JdIQJBAMtzX eTI98R53TPwlFRe35NEIp20lVN74QT97iR0mVrBNzps5dY8I4hggfeMuFRxvOVV4ppzFY/ 3wytSvbmXe40CQQDEOI+GdHLR/rItDjKPXyyzXJDxkM6ZG1kAQFKTcosm+87tsuJDFKE Nd8WgkRwuZfqj0WYPCcMYkv1EADaCFzh9AkAzkQs0aOe76fNyLcE63U0nw3ZOqK3M mjJ/lGke3lrcUOrRqTZcJaQP9f4bI5S4+pcZPbqQ0Sie8/qI6ZDjEI0lAkEAgMxHmZASVjkCp 4RsN6PXaZxXH9lXYzSKRRnzmglTwbED8Q68ah++X2LA/FaD0wjpbmJixXg48YZ6bApfvS CU2QJBAK8pbvA4cnGkb8qbwekTMC9HIeCMlq1++TynorhcLSgeIdF45HlUGOnpOXC6p Bs1n8ZIpAfpcVQNKarA5hqMTiQ=";
//    public static final String PUBLICKEY="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYBCeqme8yGq3ok0svBE0XRPoH uOMhu/JFze/szSiJ9z4QW1k8XoCHiRO/pAmZ+jUPrLzo+owf1SN2STGBrWNl2C3smTHp VvSZYxcOzglam94OAJ/ZSWJ3yFc7d/QYBFFHzcCjX3Jpes/laPD9rO8C1yi1OIXUTIgzjBVYY EAT6wIDAQAB";
//    public static final String REQUEST_URL="http://signtest.zqsign.com/";

    private String zqId;
    private String privateKey;
    private String publicKey;
    private String requestUrl;

    public String getZqId() {
        return zqId;
    }

    public void setZqId(String zqId) {
        this.zqId = zqId;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }
}
