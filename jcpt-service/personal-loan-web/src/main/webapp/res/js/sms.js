//获取验证码----2计时
var interval;
function countdown(btnId) {
	   
	   if(!$("#"+btnId).hasClass('disabled')){
	   $("#"+btnId).attr('disabled', 'disabled').html('60秒').addClass('disabled');
       var step = 60;
       interval = setInterval(function() {
               if (!--step) {
                   clearInterval(interval);
                   $("#"+btnId).html('重新获取').attr('disabled', null).removeClass('disabled');
                   return;
               }
               $("#"+btnId).html(step + '秒');
	           }, 1000);
	   }
};
function countup(btnId) {
	   	clearInterval(interval);
	   	$("#"+btnId).html('重新获取').attr('disabled', null).removeClass('disabled');
	   	return;
};