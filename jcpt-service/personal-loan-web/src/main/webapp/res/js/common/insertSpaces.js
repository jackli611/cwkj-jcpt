var HHN = HHN || {};
HHN.changePhone= function(obj){	
	obj.on('change input propertychange',function(){
		var phone = obj.val().replace(/\s/g, "");
		if(phone.length > 3 && phone.length < 8){
        	length3(obj);
        }	
        if(phone.length >= 8){
        	length8(obj);
        }
        
	    $(document).keydown(function(event){
	    	var phone = obj.val().replace(/\s/g, "");
	        if(event.keyCode==8){                       
	            if(phone.length == 4 || phone.length == 9){	            	
	            	phone=phone.substring(0,phone.length)	                        
	                obj.val(phone);
	            }
	            return;
	        }        	        
	    })
	});
}
function length3(obj){	
	var phone = obj.val().replace(/\s/g, "");
	newStr = phone.substring(0,3) + " ";
	str = phone.substring(3,phone.length);
    phone = newStr + str;
    obj.val(phone);
}
function length8(obj){	
	length3(obj);
	var phone = obj.val();	
	newStr = phone.substring(0,8) + " ";
	str = phone.substring(8,phone.length);
    phone = newStr + str;
    obj.val(phone);
}

HHN.changeIdNo= function(obj){	
	obj.on('change input propertychange',function(){
		var IdNo = obj.val().replace(/\s/g, "");
		if(IdNo.length > 6 && IdNo.length < 15){
			length6(obj);			     	
        }	
        if(IdNo.substring(6,7) != 1){
			if(IdNo.length >= 13){
	        	length13(obj);
	       }			
		}else{
			if(IdNo.length >= 15){
	        	length15(obj);
	        }
		}
	    $(document).keydown(function(event){
	    	var IdNo = obj.val().replace(/\s/g, "");
	        if(event.keyCode==8){                       
	            if(IdNo.length == 7 || IdNo.length == 16){	            	
	            	IdNo=IdNo.substring(0,IdNo.length)	                        
	                obj.val(IdNo);
	            }
	            return;
	        }               
	    })
	});
}
function length6(obj){	
	var IdNo = obj.val().replace(/\s/g, "");
	newStr = IdNo.substring(0,6) + " ";
	str = IdNo.substring(6,IdNo.length);
    IdNo = newStr + str;
    obj.val(IdNo);
}
function length13(obj){	
	length6(obj);
	var IdNo = obj.val();	
	newStr = IdNo.substring(0,13) + " ";
	str = IdNo.substring(13,IdNo.length);
    IdNo = newStr + str;
    obj.val(IdNo);
}
function length15(obj){	
	length6(obj);
	var IdNo = obj.val();	
	newStr = IdNo.substring(0,15) + " ";
	str = IdNo.substring(15,IdNo.length);
    IdNo = newStr + str;
    obj.val(IdNo);
}