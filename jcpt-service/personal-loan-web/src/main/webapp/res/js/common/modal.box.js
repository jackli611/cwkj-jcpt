var listLen=0 , lists;
function Modalbox(options) {
    $.extend(this, {
        model:$("#boxPanel").clone() ,
        listPanel: '',
        tag: '',
        prevBtn:'',
        nextBtn:'',
        open: function(){},
        init:function(){}
    }, options || {});

    this.initialize();
};
Modalbox.prototype = {
    initialize:function(){
        
        var that = this , dialog;

        dialog = '<div id="boxPanel"><div class="boxPanel">'+ 
                  '<div  class="boxTle">'+
                  '<label>关闭</label>'+
                  '</div>'+
                  '<div id="prevBtn" onclick="prevClick(this)" class="prevImg">'+
                  '<label></label>'+
                  '</div>'+
                  '<div class="boxImg " cutIndex="0">'+
                  '<img src="../images/uimg1.png">'+
                  '</div>'+
                  '<div id="nextBtn" onclick="nextClick(this)" class="nextImg">'+
                  '<label>'+
                  '</label>'+
                  '</div>'+
                  '<div class="seeYimg"><a href="javascript:void(0);">查看原图</a></div>'+
                  '</div></div>';

        listLen = this.listPanel.length;
        lists = this.listPanel;

        this.dialog = $(dialog);

        this.cutClick();

    },
    cutClick:function(){
        /*this.model.find("#prevBtn").attr("#prevBtn_m");
        this.model.find("#nextBtn").attr("#nextBtn_m");*/
        var content = this.dialog.html();           
        var that = this;
        this.listPanel.on("click" , function(){
            $(content).find(".boxImg img").attr("src",$(this).find("img").attr("src"));
            var options={title:"",
            content:content,
            cancel:false,        
            title : '',
            ok:false
            }
            HHN.webPopup("",options); 
        });
    }
};
function getImgByIndex(cutIndex){
    return $(lists).eq(cutIndex).find("img").attr("src");
}
function prevClick(that){    
    var index = $(that).siblings(".boxImg").attr("cutIndex");
    var mbVlid = listLen > (listLen-index) ? true : false;
    var tag = $(that).siblings(".boxImg");
    
    if(mbVlid){       
        cutIndex = index*1-1;
        tag.attr("cutIndex" , cutIndex);          
    }else{
        cutIndex = listLen*1-1;
        tag.attr("cutIndex" , cutIndex);   
    }
    tag.find("img").attr("src" ,  getImgByIndex(cutIndex));          
}
function nextClick(that){
    var index = $(that).siblings(".boxImg").attr("cutIndex");
    /*var mbVlid = listLen >= (1+index) ? true : false;*/
    var mbVlid = (index*1+1) < listLen ? true : false;
    var tag = $(that).siblings(".boxImg");
    
    if(mbVlid){      
        cutIndex = index*1+1;       
        tag.attr("cutIndex" , index*1+1);                
    }else{
        cutIndex = 0;
        tag.attr("cutIndex" , 0);   
    }
    tag.find("img").attr("src" ,  getImgByIndex(cutIndex));    
}