function PageNavigation(options) {
	this.target   = options.target;
	this.curIndex = parseInt(options.curIndex) || 0;
	this.total    = parseInt(options.total) || 0;
	this.subSize  = parseInt(options.subSize) || 5;
	this.data     = options.data;
	this.callback = options.callback;
	this.ajax     = options.ajax===undefined ? false : options.ajax;

	this.input;
	this.btn;
	this.pageNum;

	this.init();
};

PageNavigation.prototype = {
	init: function() {
		this.pageNum = document.createElement('span');
		this.target.appendChild(this.pageNum);
		this.createHTML();
		
	},
	createHTML: function() {
		if (this.curIndex < 1 || this.curIndex > this.total) return;
		if (this.total ==1){
			this.pageNum.innerHTML = '<i>1</i>';
			return;
		}
		var pageHTML = [],
		start        = this.curIndex - Math.floor(this.subSize / 2),
		end,
		i;

		start = start > 1 ? start : 1;
		end = start + this.subSize - 1;
		end = end > this.total ? this.total : end;
		start = (end - start < (this.subSize - 1)) ? this.total - this.subSize + 1 : start;
		if(start<=0) start=1;

		pageHTML.push(this.curIndex == 1 ? '<i class="pageprv z-dis">上一页</i>' : '<a class="pageprv" href="###">上一页</a>');

		pageHTML.push(start > 2 ? '<a href="###">1</a><i>...</i>' : '');

		for (i = start; i <= end; i++) {
			pageHTML.push(i != this.curIndex ? '<a href="###">' + i + '</a>' : '<i class="z-crt">' + i + '</i>');
		}
		pageHTML.push(end == this.total ? '' : '<i>...</i>');
		pageHTML.push(end < this.total ? '<a href="###">' + this.total + '</a>' : '');
		pageHTML.push(this.curIndex != this.total ? '<a class="pagenxt" href="###">下一页</a>' : '<i class="pagenxt">下一页</i>');
		this.pageNum.innerHTML = pageHTML.join('');
		this.total>1&&this.createInput();
		this.gotopage();
	},
	gotopage: function(){
		var page, that = this,
			btn = this.target.getElementsByTagName('a'),
			len = btn.length;
			i = 0;
		for( ; i < len ; i++){
		btn[i].onclick = function(event){
				event = event || window.event;
			    if (event.preventDefault) {
			        event.preventDefault();
			    } else {
			        event.returnValue = false;
			    }			
				page = this.innerHTML;
				if(page == '上一页'){
					that.curIndex = --that.curIndex > 1 ? that.curIndex : 1;
				}else if(page == '下一页'){
					that.curIndex = ++that.curIndex < that.total ? that.curIndex : that.total;
				}else if(page == '前往'){
					var value = parseInt(that.input.value);
					if(value>that.total || value<1){
						alert('输入的页数不存在！');
						return;
					}
					if(value == that.curIndex)return;
					that.curIndex = value;
				}else{
					that.curIndex = parseInt(page);
				}
				that.input.value = that.curIndex;
				
				if(!that.ajax)that.createHTML();
				that.ajaxData();
				// $('html,body').animate({
				// 	scrollTop:$('#commentBox').offset().top-50
				// },600);
			};
		}
	},
	createInput: function(){
		//表单
		if(this.input)return;
		this.input = document.createElement('input');
		this.input.type = 'text';
		this.input.className = 'pageInput';
		this.input.value = this.curIndex;
		this.target.appendChild(this.input);

		this.btn = document.createElement('a');
		this.btn.className = 'pageBtn';
		this.btn.setAttribute('href','###');
		this.btn.innerHTML = '前往';
		this.target.appendChild(this.btn);
		
		this.inputEvents();
	},
	inputEvents: function(){
		var that = this;
		this.input.onkeyup = function() {
			this.value = this.value.replace(/[^\d]/g, '');
		};
		this.input.onkeydown = function(e) {
			e = e || window.event;
			if (13 !== e.keyCode) return;
			that.curIndex = this.value.replace(/[^\d]/g, '');
			that.createHTML();
			that.ajaxData();
			
			// $('html,body').animate({
			// 	scrollTop:$('#commentBox').offset().top-50
			// },600);			
		};
		this.input.onmouseover = function(){
			this.select();
		};
		this.btn.onclick = function() {
			var value = parseInt(that.input.value.replace(/[^\d]/g, ''));
			if(value == that.curIndex){
				return;
			}else if(value > that.total){
				input.value = that.total;
				return;
			}else{
                var value = parseInt(that.input.value);
                that.curIndex = value;
				that.input.value = that.curIndex;
				that.createHTML();
				that.ajaxData();
				
			}
		};
	},
	ajaxData: function(){
		this.callback&&this.callback.call(this,this.curIndex);
	}
};