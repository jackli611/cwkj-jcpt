var input = $("li").find("input"),
userId = 'pageId' + $("input[name='surveyId']").val();
function userlocalStorage(checkVal){
	if(localStorage[userId]){
		getStorage(checkVal);										
	}
	setStorage();
}
		
//存储
function setStorage(){
	$.each(input,function(i){
    	var obj = $("li").find("input").eq(i),id = obj.attr("id");
    	obj.on("change input propertychange",function(){
			setStorageCheck();
		})
    })
}
function setStorageCheck(){
	var jsonObj={};
	$.each(input,function(i){
    	var obj = $("li").find("input").eq(i),id = obj.attr("id");
    	if(obj.val().replace(/\s/g, "") != ""){
    		if(obj.next()[0]){
				jsonObj[obj.attr("id")] = obj.val().replace(/\s/g, "") + "&" + obj.next().text();
	    	}else{
	    		jsonObj[obj.attr("id")] = obj.val().replace(/\s/g, "");
	    	} 
    	} 	  	
    	var userStr = JSON.stringify(jsonObj);
		localStorage.setItem(userId,userStr);
	})
}
//获取
function getStorage(fn){
    $.each(input,function(i){
    	var obj = $("li").find("input").eq(i),id = obj.attr("id"),json = $.parseJSON(localStorage[userId]);
    	if(json[id]){
    		if(json[id].indexOf("&")>=0){
				obj.val(json[id].split("&")[0]);
				obj.next().html(json[id].split("&")[1]);
				obj.next().css({"color":"#333B46","font-size":"12px"});
				fn(id);
			}else{
				obj.val(json[id]);
				obj.next().html(json[id]);
				fn(id);
			}
    	}	        	
	})
}
//删除
function removeStorage(){
	localStorage.removeItem(userId); 
}
//清除
function clearStorage(){
	localStorage.clear(); 
}