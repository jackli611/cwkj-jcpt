function animateCss(element, animationName, callback) {
    var node = document.querySelector(element);
    node.classList.add('animated', animationName);

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName);
        node.removeEventListener('animationend', handleAnimationEnd);

        if (typeof callback === 'function') callback();
    }

    node.addEventListener('animationend', handleAnimationEnd);
}


function MBModal(options) {
    $.extend(this, {
        title: '',
        model: 'select',
        time: 3000,
        type: 'info',
        content: '努力加载中/Loading ...',
        selectOption:[],
        cancelText: '取消',
        okText: '确定',
        cancel: false,
        ok: false,
        auto: true,
        src: '',
        close: false,
        autoClose:false,
        selected:function(item){return true;},
        open: function(){},
        init:function(){}
    }, options || {});

    this.initialize();
};

MBModal.prototype = {
    initialize: function() {
        var that = this,
            dialog;
        if (this.model == 'dialog' || this.model=='loading') {
            dialog = '<div class="modal">' +
                '  <div class="modal-dialog">' +
                '    {title}' +
                '    <div class="modal-bd">' +
                '      {content}' +
                '    </div>' +
                '    <div class="modal-footer">{cancel}{ok}</div>' +
                '  </div>' +
                '</div>';
            if(this.model=='loading'){
                this.ok = false;
                this.cancel=false;
                this.content = '<div class="modal-loading"></div><p>'+this.content+'</p>';
            }
            dialog = dialog.replace('{title}', this.title ? '<div class="modal-hd">' + this.title + '</div>' : '')
                .replace('{content}', this.content)
                .replace('{cancel}', this.cancel ? '<span class="modal-btn" data-btn="cancel">' + this.cancelText + '</span>' : '')
                .replace('{ok}', this.ok ? '<span class="modal-btn" data-btn="ok">' + this.okText + '</span>' : '');
        } else if (this.model == 'popup') {
            dialog = '<div style="top:'+(window.innerHeight-60)+'px" class="modal modal-popup">' +
                '  <div class="popup popup-' + this.type + '">' + this.content + '</div>' +
                '</div>';
        } else if (this.model == 'img') {
            dialog = '<div class="modal modal-img modal-active"><img src="'+this.src+'" /></div>';
        } else if (this.model == 'mapselect') {

            var layer_h= $(window).height()-100;
            dialog = '<div class="modal model-bottom">'+
                '<div class="modal-dialog model-select">'+
                '<div class="modal-bd faster">'+
                '    {title}' +
                '<div style="overflow-y:scroll;max-height:'+layer_h+'px;"><ul style=\"margin: 0;padding: 0;\"  >' ;
            for(var i=0,k=this.selectOption.length;i<k;i++)
            {
                var optionObj=this.selectOption[i];
                dialog+='<li class=\"selectoption\" data-value=\"'+optionObj.value+'\">'+optionObj.text+'</li>';
            }
            dialog+='</ul></div>'+
                '</div>'+
                '</div>'+
                '</div>';
            dialog = dialog.replace('{title}', this.title ? '<div class="modal-header">' + this.title + '{cancel}</div>' : '')
                .replace('{content}', this.content)
                .replace('{cancel}', this.cancel ? '<span  class="closeBtn" style="float: right;" data-btn="cancel"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>' : '')

        
        }else if (this.model == 'select') {
            var layer_h= $(window).height()-100;
            dialog = '<div class="modal model-bottom">'+
                '<div class="modal-dialog model-select">'+
                '<div class="modal-bd faster">'+
                '    {title}' +
                '<div style="overflow-y:scroll;max-height:'+layer_h+'px;"><ul style=\"margin: 0;padding: 0;\"  >' ;
            for(var i=0,k=this.selectOption.length;i<k;i++)
            {
                var optionObj=this.selectOption[i];
                dialog+='<li class=\"selectoption\" data-value=\"'+optionObj.value+'\">'+optionObj.text+'</li>';
            }
            dialog+='</ul></div>'+
                '</div>'+
                '</div>'+
                '</div>';
            dialog = dialog.replace('{title}', this.title ? '<div class="modal-header">' + this.title + '{cancel}</div>' : '')
                .replace('{content}', this.content)
                .replace('{cancel}', this.cancel ? '<span  class="closeBtn" style="float: right;" data-btn="cancel"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>' : '')

        } else if (this.model == 'bottomLayer') {
            var layer_h= $(window).height()-100;
            dialog = '<div class="modal model-bottom">'+
                '<div class="modal-dialog model-select">'+
                '<div class="modal-bd faster">'+
                '<div style="overflow-y:scroll;max-height:'+layer_h+'px;"><ul style=\"margin: 0;padding: 0;\"  >' +
                '    {title}' +
                '    {content}' +
                '</div></div>'+
                '</div>'+
                '</div>';
            dialog = dialog.replace('{title}', this.title ? '<div class="modal-header">' + this.title + '{cancel}</div>' : '')
                .replace('{content}', this.content)
                .replace('{cancel}', this.cancel ? '<span  class="closeBtn" style="float: right;" data-btn="cancel"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>' : '')

        } else {
            return;
        }
        this.dialog = $(dialog);
        this.auto && this.show();
        this.init();
        this.events();
    },
    show: function() {
        var that = this;
        $(document.body).append(this.dialog);
        // if (!!(window.history && history.pushState)) {
        // 	history.pushState({
        // 		dialog: 1
        // 	}, "title", "?dialog");
        // 	window.onpopstate = function(e) {
        // 		that.remove();
        // 	}
        // }
        setTimeout(function() {
            that.dialog.addClass('modal-active');
        }, 10);
        if (this.model == 'popup' || this.autoClose) {
            setTimeout(function() {
                that.remove()
            }, this.time);
        }
        this.open.call(this);
    },
    setContent: function(val){
        this.dialog.find('.modal-bd').html(val);
    },
    remove: function() {
        var that = this;
        this.dialog.removeClass('modal-active');
        setTimeout(function() {
            try {
                that.close && that.close();
                that.dialog.remove();
            } catch (e) {}
        }, 310);
        // window.onpopstate = null;
    },
    events: function() {
        var that = this;
        this.dialog.delegate('.modal-btn,.closeBtn', 'click', function() {
            switch ($(this).data('btn')) {
                case 'cancel':
                    // if (typeof that.cancel == 'function' && that.cancel() !== false) that.remove();

                    /*
                        修改非回调函数才能关闭
                    */
                    if (typeof that.cancel == 'function' && that.cancel() !== false) {
                        that.remove()
                    }else{
                        that.remove()
                    }
                    break;
                case 'ok':
                    // if (typeof that.ok == 'function' && that.ok() !== false) that.remove();
                    /*
                        修改非回调函数才能关闭
                    */
                    if (typeof that.ok == 'function' && that.ok() !== false) {
                        that.remove();
                    }else{
                        that.remove();
                    }
                    break;
            }
        });
        this.dialog.delegate(".selectoption", 'click', function() {
            var selectValue=$(this).data('value')
            var selectText=$(this).text();
            var item={value:selectValue,text:selectText};
            if(!that.selected(item)){
                return false;
            }
            if (typeof that.cancel == 'function' && that.cancel() !== false) {
                that.remove()
            }else{
                that.remove()
            }

        });
        if(this.model == 'img'){
            this.dialog.on('click', function(){
                that.remove();
            });
        }
    }
}


window.alertLayer = function (msg, opt) {
    var opts = $.extend({end: null, icon: -1, id: null,autoClose:false,cancelText:"关闭",cancel:true}, opt);
    new MBModal({model:"dialog",cancel:opts.cancel,cancelText:opts.cancelText,autoClose:opts.autoClose,content:msg});
}


function loadJson(opt) {
    var opts = $.extend({url: null, complete: null,type:"POST",cache:false, dataType:"json", showLoading: true, data: null,async:true}, opt);
    if (opts.url != null) {
        $.ajax({
            type: opts.type,
            url: opts.url,
            data: opts.data,
            cache: opts.cache,
            dataType: opts.dataType,
            async: opts.async,
            context: opts,
            beforeSend: function (XHR) {
                if (opts.showLoading) {
                }
            },
            success: function (msg) {

                if (this.complete != null) {
                    this.complete(msg, this);
                }
            }
        }).error(function (XMLHttpRequest, textStatus, errorThrown) {
            alertLayer("请求失败");
        });
    }
}

function loadPostData(opt) {
    var opts = $.extend({id: null, url: null, complete: null, showLoading: true, data: null,type:"POST",cache:false, dataType:"html",async:true}, opt);
    if (opts.id != null) {
        $.ajax({
            type: opts.type,
            url: opts.url,
            data:opts.data,
            cache: opts.cache,
            dataType:opts.dataType,
            async:opts.async,
            context:opts,
            beforeSend:function(XHR){ if (opts.showLoading) {

            }},
            success: function(htmldata){

                if (this.complete != null) {
                    this.complete(htmldata);
                }
                $("#" + this.id).html(htmldata);
            }
        }).error(function(XMLHttpRequest, textStatus, errorThrown) { alertLayer("请求失败："+textStatus,{icon: 5}); })
            .complete(function() { });
    }
}


function getFullUrl(url){
	//获取当前网址
    var curWwwPath=window.document.location.href;
    //获取主机地址之后的目录
    var pathName=window.document.location.pathname;
    var pos=curWwwPath.indexOf(pathName);
    //获取主机地址
    var localhostPaht=curWwwPath.substring(0,pos);    
    if(url.substring(0,1) == '/'){
    	if(projectPath==""|| projectPath=="/"){
    		return(localhostPaht+url);
    	}else{
    		return(localhostPaht+projectPath+url);
    	}
    }else{
    	if(projectPath==""|| projectPath=="/"){
    		return(localhostPaht+"/"+url);
    	}else{
    		return(localhostPaht+projectPath+"/"+url);
    	}
    }
}