//判断是否是微信浏览器的函数
function isWeiXin(){
  //window.navigator.userAgent属性包含了浏览器类型、版本、操作系统类型、浏览器引擎类型等信息，这个属性可以用来判断浏览器类型
  var ua = window.navigator.userAgent.toLowerCase();
  //通过正则表达式匹配ua中是否含有MicroMessenger字符串
  if(ua.match(/MicroMessenger/i) == 'micromessenger'){
	  return true;
  }else{
	  return false;
  }
}

function getWXCode(redirect_url){
	if(isWeiXin()==false){
		return redirect_url;
	}else{
		redirect_url = getFullUrl(redirect_url); 
		redirect_url = encodeURIComponent(redirect_url);
		return "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8c1ca2bd6dbd7382&redirect_uri="+redirect_url+"&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
	}
	
}

function doWxpay(btn){
	$(btn).attr("disabled","disabled");
	//加载支付页面
	var currentLoanId = sessionStorage.getItem('loanid');
	if(!currentLoanId){
		console.error("无效订单");
		alert("无效订单");
		return false;
	}
	var signUrl = encodeURIComponent(location.href.split('#')[0]);
	var payParam = {"loanid":currentLoanId,
					"payMethod":"wx",
					"wxcode":"${param.code}",
					"signUrl":signUrl,
					"token": $("#token").val()};
	 $("#paydiv").load("<c:url value='/pay/prePay.do'/>",payParam);
	//end 加载支付页面
	
}

function loadWxShareJsconfig(){
	if(isWeiXin()==true){
		var signUrl=window.location.href;
	    loadPostData({id:"wxpage",url:"wxJSConfig.do",data:{signUrl:signUrl},complete:function(html){}});
	}
}