<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="PAY002div"></div>
<script src="https://res.wx.qq.com/open/js/jweixin-1.4.0.js" type="text/javascript"></script>
<script src="<c:url value="/res/js/wx.js"/>"></script>
<script type="text/javascript">
		var currentLoanId = sessionStorage.getItem('loanid');
		if(currentLoanId){
			var signUrl = encodeURIComponent(location.href.split('#')[0]);
			var payParam = {"loanid":currentLoanId,
					"payMethod":"wx",
					"wxcode":"${param.code}",
					"signUrl":signUrl,
					"payAmt":"${param.payAmt}",
					"token": $("#token").val()};
			 $("#PAY002div").load("<c:url value='/wxpay/prePayOrder.do'/>",payParam);
			//end 加载支付页面
		}else{
			console.error("无效订单");
			alert("无效订单");
		}
		
		

</script>