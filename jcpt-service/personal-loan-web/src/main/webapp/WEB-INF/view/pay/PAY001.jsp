<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">
#payInfo{
    position: absolute;
    left: 0px;
    buttom:600px;
    width:100%;
    height: 400px;
    background: #ffffff;
    border-top: 1px solid #e4e4e4;
    border-radius: 4px;
    -webkit-box-shadow: 0 -2px 2px rgba(0,0,0,.05);
    box-shadow: 0 -2px 2px rgba(0,0,0,.05);
    z-index: 9999;
}

        .xwc_btn_block_lg{
            min-height: 56px;
        }

        .xwc_panel_lg{
            /*min-height: 86px;*/
            font-size: 15px;
        }
.xwc_panel_lg .fa{
    font-size: 20px;
    padding:10px;

}
        .xwc_bg_success{
            background-color: #74e6b2;
            color: #ffffff;
            border-color: #70e7b1 !important;
        }
.xwc_bg_success .fa{
    color: #ffffff;
    margin-right: 20px;
}

.xwc_bank_list{
    min-height:50px;
    min-width: 250px;
    padding-top:0px !important;
    padding-bottom:0px !important;
    border-color: #eee;
    box-shadow: 0 1px 4px rgba(47, 47, 47, 0.13);
}
.xwc_bank_list img{
    float: left;
    margin-right: 20px;
    width: 50px;
    height: 50px;
    margin-bottom: 12px;
}
.xwc_bank_list dl{
    margin-top:16px;
}
.xwc_bank_list dt{
    font-weight: 360;
}
.xwc_bank_list dl dd{
   color: gray;
    font-weight: 200;
}
.xwc_num_lg{
    font-size: 18px;
}
hr {
    margin-top: 10px;
    margin-bottom: 10px;
    border: 0;
    border-top: 1px solid #eee;
}
        a.xwc_btn_block_lg{
            padding-top: 16px;
        }
    </style>
</head>
<body class="default_body_bg">
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<div class="container-fluid">
    <div class="row p-20">

        <div class="panel panel-default">
            <div class="panel-body bg-success xwc_panel_lg xwc_bank_list xwc_bg_success" style="padding-left: 20px; line-height: 46px; " >
                <div class="pull-left" style="width:50px; height: 50px; margin-top:20px;" >
                <i class="fa fa-check" aria-hidden="true"></i>
                </div>
                <div style="margin-top:15px;">
                    <span>资金方匹配成功</span>
                </div>


            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body xwc_panel_lg xwc_bank_list">
                <div  >
                    <img src="../res/images/demo/bank_ny.png">
                <dl style="">
                    <dt>农业银行</dt>
                    <dd>最低利息：4.8%</dd>
                </dl>
                    </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body xwc_panel_lg xwc_bank_list">
                <div >
                    <img src="../res/images/demo/bank_gs.png">
                    <dl>
                <dt>工商银行</dt>
                <dd>最低利息：4.8%</dd>
            </dl>
                    </div>
            </div>
        </div>
    </div>


</div>

<div class="container-fluid">
    <div class="row">
        <div class="m-20">
    <div class="text-left xwc_text_gray">
         <small>小橙会根据您的信息，从100多家金额机构中挑出最符合您心意来推荐。最终匹配结果以您的人行征信报告为准</small>
    </div>
    </div>
    </div>

</div>

<div id="paydiv" style="display: none"></div>

<div class="container-fluid" id="toLoanBtnDiv">
    <div class="row ">
        <div class="p-20">
        <input type="button" class="btn btn-primary btn-block xwc_btn_block_lg" value="下一步" onclick="openChooseLayer()">
        </div>
    </div>
</div>


<div id="payInfo" class="hide">

    <div class="container-fluid">

<c:choose>
    <c:when test="${order.loantype eq 3 }">

        <div class="row text-left">
            <div class="p-20">
                <div>
                    <b class="text-left"><img src="<c:url value="/res/images/cw-gd/exclamation.png"/>" style="width: 18px;height: 18px; margin-bottom: 4px;"> 获得授信，无需支付尾款</b>
                    <a style="position: absolute;right: 20px; top:20px;" onclick="closeChooseLayer()"><img src="<c:url value="/res/images/cw-gd/close-gray.png"/>" style="width: 18px;height: 18px; margin-bottom: 4px;"></a>
                </div>
                <hr>
                <div class="m-t20">
                    <b><img src="<c:url value="/res/images/cw-gd/exclamation.png"/>" style="width: 18px;height: 18px; margin-bottom: 4px;"> 未获得授信，则退款599元，说明：</b>

                    <small>退599元，余下100元为信息服务费</small>
                </div>
            </div>

        </div>
        <div class="row" id="paybtnDiv">
            <div class="p-20">
                <a  class="btn btn-primary btn-block xwc_btn_block_lg"   onclick="dopay(this);">
                    立即支付 <i class="fa fa-jpy" aria-hidden="true" ></i>699
                </a>
            </div>
        </div>

    </c:when>
    <c:otherwise>

        <div class="row text-left">
            <div class="p-20">
                <div>
                    <b class="text-left"><img src="<c:url value="/res/images/cw-gd/exclamation.png"/>" style="width: 18px;height: 18px; margin-bottom: 4px;"> 成功获得授信或者收款，需支付尾款，说明：</b>
                    <a style="position: absolute;right: 20px; top:20px;" onclick="closeChooseLayer()"><img src="<c:url value="/res/images/cw-gd/close-gray.png"/>" style="width: 18px;height: 18px; margin-bottom: 4px;"></a>
                </div>

            	<hr>
				    <small>30万以下（不含30万元），尾款为1000元</small><br>
				    <small>30—100万元（不含100万元），尾款为5000元</small><br>
				    <small>100-500万元（不含500万元），尾款为10000元</small><br>
				    <small>500-1000万元（不含1000万元），尾款为30000元</small><br>
				    <small>1000万以上，按融资金额2%定制方案</small><br>
                <br>
                <div class="m-t20">
                <b><img src="<c:url value="/res/images/cw-gd/exclamation.png"/>" style="width: 18px;height: 18px; margin-bottom: 4px;"> 未获得授信，则退款，说明：</b>
                <hr>
                <small>仅退199元，余下100元为信息服务费</small>
                </div>
            </div>

        </div>
        <div class="row" id="paybtnDiv">
            <div class="p-20">
                <a  class="btn btn-primary btn-block xwc_btn_block_lg"   onclick="dopay(this);">
                    立即支付 <i class="fa fa-jpy" aria-hidden="true" ></i>299
                </a>
            </div>
        </div>
    </c:otherwise>
</c:choose>
    </div>
</div>
<input type="hidden" name="token" id="token" value="${token}"/>
<input type="hidden" name="payAmt" id="payAmt" value="${payAmt}"/>
<script type="text/javascript">
$(function(){
	//加载支付按钮
	 var payBtnParam = {'payAmt':'${payAmt}','loanid':'<c:out value="${loanid}"/>','payStepCode':'PAY001'};
	 $("#paybtnDiv").load("<c:url value='/pay/getPayBtnByChannel.do'/>",payBtnParam,function(obj){});
	//end 加载订单
});

</script>

<script type="text/javascript">
    function openPayInfo() {
        $("#payInfo").removeClass("hide")
        $("#payInfo").addClass("show");
        $("#payBtnDiv").removeClass("show")
        $("#payBtnDiv").addClass("hide");
        animateCss('#payInfo', 'slideInUp',function(){

        });

    }
    function closePayInfo() {
        $("#payBtnDiv").removeClass("hide")
        $("#payBtnDiv").addClass("show");

        animateCss('#payInfo', 'slideOutDown',function(){
            $("#payInfo").removeClass("show");
            $("#payInfo").addClass("hide");
        });
    }
    
</script>
<script type="text/javascript">
    var myMBModal=null;
    function openChooseLayer(opt)
    {
        var opts = $.extend({selectOption: [], id: null}, opt);
        var inputId=opts.id;
        myMBModal=new MBModal({model:"bottomLayer", content:$("#payInfo").html()});
        myMBModal.show();
    }

    function closeChooseLayer()
    {
        myMBModal.remove();
    }
</script>
</body>
</html>
