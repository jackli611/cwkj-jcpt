<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">
.text-success{
    color: #74e6b2;
}
        .remarkText{
            color: #999999;
        }
    </style>
    <script type="text/javascript">
        function stepNext(id) {
            window.location='<c:url value="/index.do"/>';
        }
    </script>
</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<div class="container-fluid">
    <div class="row m-t20">

            <div class="box m-20 ">
                <div class="text-center text-success" style="font-size: 80px;">
                    <img src="<c:url value="/res/images/cw-gd/paySuccess.png"/>" style="max-width: 50px; max-height: 50px;"/>
                </div>
                <div class="text-center  m-t10">
                    <p>已支付成功</p>
                </div>
            </div>

    </div>

    <div class="row remarkText">
    <div class="box m-20">
        <p class="text-left">
            <span>请用"微信"添加客户人员：</span>
        </p>
        <p class="text-left">
            <span>扫一扫二维码：</span>

        </p>
        <div class="text-center">
            <img style="max-width: 140px; max-height: 140px;" src="<c:url value='/res/images/kefu/xwckeji-kefu001.jpg'/>" />
        </div>
    </div>
    </div>
        <div class="row remarkText">
            <div class="box p-20 ">
        <div class="text-left">
            <span>或者输入微信号添加：</span>
        </div>
        <div class="text-left">
            <span>XWCKEJI2018</span>

        </div>
            </div>
    </div>
    </div>


    <div class="container-fluid" id="payBtnDiv">
        <div class="row ">
            <div class="p-20">
                <input type="button" class="btn btn-primary btn-block btn-lg" value="完成" onclick="stepNext('orderDetail')" >
            </div>
        </div>
    </div>



</div>

</body>
</html>
