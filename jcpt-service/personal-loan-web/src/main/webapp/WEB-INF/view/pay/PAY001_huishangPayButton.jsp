<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="p-20">
    <a  class="btn btn-primary btn-block xwc_btn_block_lg"   onclick="dopay(this);">
        	立即支付 <i class="fa fa-jpy" aria-hidden="true" ></i><c:out value="${payInfo.payAmt}"/>
    </a>
</div>
<script type="text/javascript">
	function dopay(btn){
		var payAmt = $("#payAmt").val();
		window.location.href="<c:url value='/hspay/toHuishangPayPage.do'/>?payAmt="+payAmt;
	}
</script>