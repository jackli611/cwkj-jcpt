<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="p-20">
    <a  class="btn btn-primary btn-block xwc_btn_block_lg"   onclick="dopay(this);">
        	立即支付 <i class="fa fa-jpy" aria-hidden="true" ></i>${payInfo.payAmt}
    </a>
</div>
<script src="https://res.wx.qq.com/open/js/jweixin-1.4.0.js" type="text/javascript"></script>
<script src="<c:url value="/res/js/wx.js"/>"></script>
<script type="text/javascript">
	function dopay(btn){
		$(btn).attr("disabled","disabled");
		//加载支付页面
		var currentLoanId = sessionStorage.getItem('loanid');
		if(!currentLoanId){
			console.error("无效订单");
			alert("无效订单");
			return false;
		}
		var signUrl = encodeURIComponent(location.href.split('#')[0]);
		
    	var payParam = {"loanid":currentLoanId,
    					"payMethod":"wx",
    					"wxcode":"${param.code}",
    					"signUrl":signUrl,
    					"payAmt":"${payInfo.payAmt}",
    					"token": $("#token").val()};
    	 $("#paydiv").load("<c:url value='/wxpay/prePayOrder.do'/>",payParam);
	    	 
		//end 加载支付页面
		
	}
</script>