<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script src="<c:url value="/res/js/common/global.js"/>"></script>
    <script src="<c:url value="/res/js/idNoTools.js"/>"></script>
    <script src="<c:url value="/res/js/sms.js"/>"></script>

    <style type="text/css">
        .info_list a.sel_txt {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
        }
        .info_list a:hover {
            text-underline: none;
            text-decoration: none;
        }
        .info_list a input.txtIpt {
            display: block;
            -webkit-box-flex: 1;
            line-height: 30px;
            height: 30px;
            -webkit-flex: 1;
            flex: 1;
            font-size: 13px;
            padding-left: 15px;
            border:0px;
        }
        input.pull-right{
            text-align: right;
        }
        .info_list a input:focus,.info_list a input:hover {
            outline: 0;
            box-shadow: none;
        }

        .textLabel{
            font-weight: 200;
        }
        .textLabel_active{
            color:#000000;
        }
        input::-webkit-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input::-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-ms-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
    </style>

    <script type="text/javascript">

        function openChooseLayer(opt)
        {
            var opts = $.extend({selectOption: [], id: null}, opt);
            var inputId=opts.id;
            new MBModal({model:"select",cancel:true,title:"请选择",selected:function(item){$("#"+inputId+" input[type='hidden']").val(item.value);
                    $("#"+inputId+" .textLabel").text(item.text);
                    if(!$("#"+inputId+" .textLabel").hasClass("textLabel_active"))
                    {
                        $("#"+inputId+" .textLabel").addClass("textLabel_active")
                    }
                   // checkInputValueStatus();
                    return true;},selectOption:opts.selectOption}).show();
        }

        // function openChooseOccupationList

    </script>

</head>
<body class="default_body_bg">
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<form method="post" id="shform"  action="<c:url value='/hspay/submitPay.do'/>" >
    <div class="container-fluid">

        <div class="row">
<div class="text-center m-t20 m-b20 ">
            <img src="<c:url value="/res/images/cw-gd/bankcardIcon.png"/>" style="width: 90px;">
</div>
        </div>
        <div class="row">
            <div class="card">
                <ul class="info_list">
                    <li >
                        <a  id="banks" onclick='openChooseLayer({id:"banks",selectOption:[<tool:code type="banks"  selection="true"  ></tool:code>]});' >
                            <span class="pull-left">银行</span>
                            <input type="hidden" name="bankName"   id="bankName"  />
                            <span class="pull-right info_state"><label class="textLabel">请选择银行</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                     <li>
                         <a class="sel_txt">
						<span class="pull-left">
	                      姓名
						</span>
                             <input  type="text" name="realname"  id="realname" value="${sysUserAuth.realname}"  <c:if test="${sysUserAuth.ischeck eq 'T' }">readonly="readonly"</c:if>    class="pull-right txtIpt"  placeholder="请输入您的姓名">
                         </a>
                     </li>
                    <li>
                        <a class="sel_txt">
						<span class="pull-left">
	                      身份证号
						</span>
                            <input name="idNo"  id="idNo"  value="${sysUserAuth.idno}" <c:if test="${sysUserAuth.ischeck eq 'T' }">readonly="readonly"</c:if>  type="text" class="pull-right txtIpt"  placeholder="请输入身份证号">
                        </a>
                    </li>
                    <li>
                        <a class="sel_txt">
						<span class="pull-left">
	                      银行卡号
						</span>
                            <input name="bankcardNo"  id="bankcardNo"  value="${sysUserAuth.cardnum}" type="text" class="pull-right txtIpt"  placeholder="请输入银行卡号">
                        </a>
                    </li>
                    <li>
                        <a class="sel_txt">
						<span class="pull-left">手机号</span>
                            <input name="mobile"  id="mobile" type="text" value="${sysUserAuth.mobile}"  class="pull-right txtIpt"  placeholder="请输入银行预留手机号">
                        </a>
                    </li>
                    <li>
                        <a class="sel_txt">
                            <span class="pull-left">验证码</span>
                            <input name="smscode" id="smscode"  type="text" class="pull-right txtIpt"  placeholder="请输入验证码">
                            <span class="txtIpt pull-right m-l10 btn-link" id="smsbtn" onclick="getsms();">获取短信验证码</span>
                        </a>
                    </li>

                </ul>
            </div>


        </div>

        <div class="row ">
            <div class="p-10">
                <!--
                 <input type="button" id="nextBtn" disabled="disabled" class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive disabled" value="下一步" onclick="stepNext();">
                 -->
                <a   class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive" onclick="stepNext();"><img src="<c:url value="/res/images/cw-gd/dun.png" />" style="height: 18px;" class="m-r10"> 确认</a>
            </div>
        </div>

        <div id="address">
        </div>
    </div>

	<input type="hidden"  id="needSign" name="needSign">
	<input type="hidden"  id="signToken" name="signToken">
	<input type="hidden"  id="loanid" name="loanid">
	<input type="hidden"  id="payAmt" name="payAmt" value="${payAmt}">

</form>

</body>
</html>
<script type="text/javascript">
	
	function getsms(){
		if($("#smsbtn").text() != "获取短信验证码" && $("#smsbtn").text() != "重新获取" ){
			return false;
		}
		
		if($("#realname").val() == ""){
			alertLayer('请输入您的姓名 ');
			return false;
		}
		if($("#idNo").val() == ""){
			alertLayer('请输入身份证号 ');
			return false;
		}
		if($("#bankcardNo").val() == ""){
			alertLayer('请输入银行卡号 ');
			return false;
		}
		if($("#mobile").val() == ""){
			alertLayer('请输入银行预留手机号 ');
			return false;
		}
		
		var currentLoanId = sessionStorage.getItem('loanid');
    	if(!currentLoanId){
    		console.error("无效订单");
    		alert("无效订单");
    		return false;
    	}
    	
    	
    	
		$("#smsbtn").text("60s");
		countdown("smsbtn");
		$.ajax({
			type:"POST",
			dataType:"json",
			url:"<c:url value='/hspay/gethspaySmscode.do'/>",
			data: {"mobile":$("#mobile").val(),
				   "realname":$("#realname").val(),
				   "bankcardNo":$("#bankcardNo").val(),
				   "idNo":$("#idNo").val(),
				   "loanid":currentLoanId},
			async: false,
			success:function(data){
				if(data.code == "1"){
					//发送成功
					$("#signToken").val(data.data.SIGN_TOKEN);
					$("#needSign").val(data.data.SIGN_NEEDED);
					//不可再修改
					$("#realname").attr('readonly', 'readonly');
					$("#idNo").attr('readonly', 'readonly');
					$("#bankcardNo").attr('readonly', 'readonly');
					$("#mobile").attr('readonly', 'readonly');
					$(".btn-primary").attr('disabled', '').removeClass('disabled');
				}else{
					alertLayer(data.msg);
				}
			}
		});
	}
	
	
    function stepNext(id) {
    	
    	var currentLoanId = sessionStorage.getItem('loanid');
    	if(!currentLoanId){
    		console.error("无效订单");
    		alert("无效订单");
    		return false;
    	}
    	$("#loanid").val(currentLoanId);
    	
    	$(".btn-primary").attr('disabled', 'disabled').addClass('disabled');
    	
    	$.ajax({
			type:"POST",
			dataType:"json",
			url:"<c:url value='/hspay/submitPay.do'/>",
			data: $("#shform").serialize(),
			async: false,
			success:function(data){
				if(data.code == "1"){
					$(".btn-primary").attr('disabled', '');
					window.location='<c:url value='/hspay/payResult.do'/>';
				}else{
					alertLayer(data.msg);
				}
			}
		});
    }

</script>

