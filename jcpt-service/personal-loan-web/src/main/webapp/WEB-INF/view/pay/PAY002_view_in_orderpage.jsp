<%@ page language="java" contentType="text/html;charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/taglib/tool.tld" prefix="tool" %>
<script type="text/javascript" src="<c:url value='/res/js/loan.js'/>"></script>
<c:if test="${ orderpay != null && orderpay.payName != null }">
<div class="row  m-t20">
       <div class="card ">

               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                   <label>

                       <input name="payChk" type="checkbox" value="1000"  <c:if test="${order.applyamount le 300000 }"> checked="checked" </c:if>  class="chkbx pull-left p-t20"  />
                       <span class=" pull-right">
                       <font style="font-weight: 700;font-size: large;"> ￥1000</font>
                       <p>申请金额30万以下</p>
                       </span>
                   </label>
               </div>
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                   <label  >
                       <input name="payChk" type="checkbox" value="2000"  <c:if test="${order.applyamount le 1000000 &&  order.applyamount gt 300000}"> checked="checked" </c:if>  class="chkbx pull-left p-t20" />
                       <span class=" pull-right"> <font style=" font-weight: 700;font-size: large;">￥2000</font>
                       <p>申请金额30-100万</p>
                       </span>
                   </label>
               </div>

           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <label  >

                   <input name="payChk" type="checkbox" value="5000"  <c:if test="${order.applyamount le 5000000 &&  order.applyamount gt 1000000}"> checked="checked" </c:if>  class="chkbx pull-left p-t20" />
                   <span class=" pull-right"> <font style=" font-weight: 700;font-size: large;">￥5000</font>
                   <p>申请金额100-500万</p>
                   </span>
               </label>
           </div>

           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <label>

                   <input name="payChk" type="checkbox" value="10000"  <c:if test="${order.applyamount gt 5000000}"> checked="checked" </c:if> class="chkbx pull-left p-t20" />
                   <span class=" pull-right"> <font style=" font-weight: 700;font-size: large;">￥10000</font>
                   <p>申请金额500万以上</p>
                   </span>
               </label>
           </div>
           
       </div>

</div>
<div class="row m-t20">
    <div class="p-20">
    	<input onclick="clkPay();" type="button" class="btn btn-block btn-lg active" value="去付${orderpay.payName}" />
    </div>
</div>
<script type="text/javascript">	
$(function() {
	  $('input[name=payChk]').bind('click', function(){  
	        var that = this;
	        //当前的checkbox是否选中
	        if(that.checked){
	            //除当前的checkbox其他的都不选中
	            $('input[name=payChk]').not(this).attr("checked", false); 
	        }
	    }); 
});
function clkPay(){
	sessionStorage.setItem('loanid','${orderpay.loanid}');
	
	var payAmt = 0 ;
	var arr = $('input[name=payChk]');
	for(var i = 0 ; i < arr.length; i++){
		if($(arr[i]).is(":checked")==true){
			payAmt = $(arr[i]).val();
			break;
		}
	}
	if(payAmt == 0 ){
		new MBModal({model:"dialog",cancel:true,cancelText:"关闭",autoClose:true,content:"请根据你的借款金额选择尾款"});
		return false;
	}
	gotoPayPageByFeeType('${orderpay.loanid}','${orderpay.payStepCode}',payAmt);
}
</script>
</c:if>