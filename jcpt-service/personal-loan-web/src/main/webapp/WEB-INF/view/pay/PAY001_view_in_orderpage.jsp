<%@ page language="java" contentType="text/html;charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/taglib/tool.tld" prefix="tool" %>
<script type="text/javascript" src="<c:url value='/res/js/loan.js'/>?v=99"></script>
<c:if test="${ orderpay != null && orderpay.payName != null }">
<div class="row  m-t20">
     <div class="card ">
          <div class="text-center">
              <label>
                  <span>
                  <font style="font-weight: 900;font-size: large;"> 你需要支付 ￥${orderpay.payAmt}${orderpay.payName}</font>
                  </span>
              </label>
          </div>
     </div>
</div>
<div class="row m-t20">
    <div class="p-20">
    	<input onclick="clkPay();" type="button" class="btn btn-block btn-lg active" value="去付${orderpay.payName}" />
    </div>
</div>
<script type="text/javascript">
function clkPay(){
	sessionStorage.setItem('loanid','${orderpay.loanid}');
	gotoPayPageByFeeType('${orderpay.loanid}','${orderpay.payStepCode}','${orderpay.payAmt}');
}
</script>

</c:if>