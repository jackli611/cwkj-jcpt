<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
</head>
   
<body class="default_body_bg">
	<%@ include file="/WEB-INF/common/navbar.jsp" %>
	<div id="paybtnDiv"></div>

<script type="text/javascript">
$(function(){
	//加载支付按钮
	 var payBtnParam = {'payAmt':'${payAmt}','loanid':'<c:out value="${loanid}"/>','payStepCode':'PAY002'};
	 $("#paybtnDiv").load("<c:url value='/pay/getPayBtnByChannel.do'/>",payBtnParam,function(obj){});
	
	 //end 加载订单
});

</script>
</body>
</html>
