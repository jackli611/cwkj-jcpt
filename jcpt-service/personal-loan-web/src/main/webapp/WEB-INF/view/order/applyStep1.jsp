<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
	<script src="<c:url value="/res/js/common/global.js"/>"></script>
    <style type="text/css">
.xwc_input_font_lg{
    font-size: 28px;
}
input.xwc_input_font_lg::-webkit-input-placeholder {
    letter-spacing: 0;
    font-size: 14px;
    padding-top: 10px;
    color: #999999;
}
input.xwc_input_font_lg:-moz-placeholder {
    letter-spacing: 0;
    font-size: 14px;
    padding-top: 10px;
    color: #999999;
}
input.xwc_input_font_lg::-moz-placeholder {
    letter-spacing: 0;
    font-size: 14px;
    padding-top: 10px;
    color: #999999;
}
input.xwc_input_font_lg:-ms-input-placeholder {
    letter-spacing: 0;
    font-size: 14px;
    padding-top: 10px;
    color: #999999;
}

.info_list a input.txtIpt {
    display: block;
    -webkit-box-flex: 1;
    line-height: 30px;
    height: 30px;
    -webkit-flex: 1;
    flex: 1;
    font-size: 13px;
    padding-left: 15px;
    border:0px;
}
input.pull-right{
    text-align: right;
}
.info_list a input:focus,.info_list a input:hover {
    outline: 0;
    box-shadow: none;
}

.textLabel{
    font-weight: 200;
}
.textLabel_active{
    color:#000000;
}

    </style>
</head>
<body class="default_body_bg">
<%@ include file="/WEB-INF/common/navbar.jsp" %>

 <form class="form-horizontal" action="<c:url value='/loanorder/saveOrder.do'/>" method="post">
<div class="container-fluid">
    <!-- 信用贷-->
    <c:if test="${empty loanType || loanType == '1' || loanType == '4' }">
    <div class="row">

        <div class="card">
		<div class="card-body">
		<p class=" p-t10" style="margin-left:4px;"> 借多少</p>
            <div class="form-group">
                <label class="col-xs-2 control-label col-compact">&nbsp;<img src="<c:url value="/res/images/cw-gd/rmb-01.png"/>" style="width:12px;"></label>
                <div class="col-xs-9 col-compact">
                    <input type="number" value="${order.applyamount}" name="applyamount" class="form-control xwc_input_font_lg" placeholder="最高可借50000000.00" maxlength="9" style="padding-left: 10px; padding-top:0px; padding-bottom: 0px; letter-spacing: 5px">
                </div>
            </div>
            <input type="hidden" value="PERSONAL_XY" name="prodcode" id="prodcode">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="card">
        <ul class="info_list">

            <li>
                <a  id="loanperiod" onclick='openChooseLayer({id:"loanperiod",selectOption:[<tool:code type="loanPeriod"  selection="true"  ></tool:code>]});'>
					<span  class="pull-left">借多久</span>
                    <input type="hidden" name="loanperiod" class="" value="${order.loanperiod }" nullmsg="借多久" errormsg="借多久">
                    <span  class="pull-right info_state"><label class="textLabel">借多久</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                </a>
            </li>
            <li>
                <a id="repaytype" onclick='openChooseLayer({id:"repaytype",selectOption:[<tool:code type="repayType"  selection="true"  ></tool:code>]});' >
                    <span  class="pull-left">怎么还</span>
                    <input type="hidden" name="repaytype" value="${order.repaytype }" nullmsg="怎么还" errormsg="怎么还">
                    <span  class="pull-right info_state"><label class="textLabel">怎么还</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                </a>
            </li>
            <li>
                <a  id="deductHouse" onclick='openChooseLayer({id:"deductHouse",selectOption:[<tool:code type="deductHouse"  selection="true"  ></tool:code>]});'>
					<span  class="pull-left">能否抵押房子</span>
                    <input type="hidden" name="deducthouse" value="${order.person.deducthouse}" nullmsg="能否抵押房子" errormsg="能否抵押房子">
                    <span  class="pull-right info_state"><label class="textLabel">能否抵押房子</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                </a>
            </li>
            <li>
                <a    onclick='openAgreementLayer()'>
                    <span  class="pull-left">居间服务协议</span>
                    <input type="hidden" name="whereRedBook" value="${order.whereRedBook}" nullmsg="居间服务协议" errormsg="居间服务协议">
                    <span  class="pull-right info_state"><label class="textLabel agrtValueLabel">是否同意居间服务协议</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                </a>
            </li>



        </ul>
        </div>
        </c:if>
        <!-- 房屋抵押贷-->
        <c:if test="${ loanType == '2' || loanType == '5' }">
            <div class="row">

                <div class="card">
                    <div class="card-body">
                        <p class=" p-t10" style="margin-left:4px;"> 通过抵押红本商品房借款，借多少</p>
                        <div class="form-group">
                            <label class="col-xs-2 control-label col-compact">&nbsp;<img src="<c:url value="/res/images/cw-gd/rmb-01.png"/>" style="width:12px;"></label>
                            <div class="col-xs-9 col-compact">
                                <input type="number" value="${order.applyamount}" name="applyamount" class="form-control xwc_input_font_lg" placeholder="填写借款金额" maxlength="9" style="padding-left: 10px; padding-top:0px; padding-bottom: 0px; letter-spacing: 5px">
                            </div>
                        </div>
                        <input type="hidden" value="PERSONAL_XY" name="prodcode" id="prodcode">
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="card">
                <ul class="info_list">
                <li>
                <a  id="whereHouse" onclick='openChooseLayer({id:"whereHouse",selectOption:[<tool:code type="whereHouse"  selection="true"  ></tool:code>]});'>
                    <span  class="pull-left">被抵押红本房在哪里</span>
                    <input type="hidden" name="wherehouse" value="${order.person.wherehouse}" nullmsg="被抵押红本房在哪里" errormsg="被抵押红本房在哪里">
                    <span  class="pull-right info_state"><label class="textLabel">被抵押红本房在哪里</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                </a>
                </li>
                    <li>
                        <a  id="redBookType" onclick='openChooseLayer({id:"redBookType",selectOption:[<tool:code type="redBookType"  selection="true"  ></tool:code>]});'>
                            <span  class="pull-left">红本房类型</span>
                            <input type="hidden" name="redbooktype" value="${order.person.redbooktype}" nullmsg="红本房类型" errormsg="红本房类型">
                            <span  class="pull-right info_state"><label class="textLabel">红本房类型</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                    <li >
                        <a class="sel_txt">
						<span class="pull-left">
	                      被抵押房子价格
						</span>
                            <input name="houseprice" value="${order.person.houseprice}" type="number" class="pull-right txtIpt" maxlength="10"  placeholder=" 被抵押房子价格">
                        </a>
                    </li>
            <li>
                <a  id="loanperiod" onclick='openChooseLayer({id:"loanperiod",selectOption:[<tool:code type="loanPeriod"  selection="true"  ></tool:code>]});'>
                    <span  class="pull-left">借多久</span>
                    <input type="hidden" name="loanperiod" class="" value="${order.loanperiod }" nullmsg="借多久" errormsg="借多久">
                    <span  class="pull-right info_state"><label class="textLabel">借多久</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                </a>
            </li>
            <li>
                <a id="repaytype" onclick='openChooseLayer({id:"repaytype",selectOption:[<tool:code type="repayType"  selection="true"  ></tool:code>]});' >
                    <span  class="pull-left">怎么还</span>
                    <input type="hidden" name="repaytype" value="${order.repaytype }" nullmsg="怎么还" errormsg="怎么还">
                    <span  class="pull-right info_state"><label class="textLabel">怎么还</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                </a>
            </li>
                    <li>
                        <a    onclick='openAgreementLayer()'>
                            <span  class="pull-left">居间服务协议</span>
                            <input type="hidden" name="whereRedBook" value="${order.whereRedBook}" nullmsg="居间服务协议" errormsg="居间服务协议">
                            <span  class="pull-right info_state"><label class="textLabel agrtValueLabel">是否同意居间服务协议</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                </ul>
            </div>
        </c:if>
            <!-- 赎回红本-->
            <c:if test="${ loanType == '3' }">
            <div class="row">

                <div class="card">
                    <div class="card-body">
                        <p class=" p-t10" style="margin-left:4px;"> 需要用多少金额赎楼</p>
                        <div class="form-group">
                            <label class="col-xs-2 control-label col-compact">&nbsp;<img src="<c:url value="/res/images/cw-gd/rmb-01.png"/>" style="width:12px;"></label>
                            <div class="col-xs-9 col-compact">
                                <input type="number" value="${order.applyamount}" name="applyamount" class="form-control xwc_input_font_lg" placeholder="填写借款金额" maxlength="9" style="padding-left: 10px; padding-top:0px; padding-bottom: 0px; letter-spacing: 5px">
                            </div>
                        </div>
                        <input type="hidden" value="PERSONAL_XY" name="prodcode" id="prodcode">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="card">
                    <ul class="info_list">
                        <li>
                            <a  id="loanperiod" onclick='openChooseLayer({id:"loanperiod",selectOption:[<tool:code type="howLongForLoan"  selection="true"  ></tool:code>]});'>
                                <span  class="pull-left">需要用多久</span>
                                <input type="hidden" name="loanperiod" value="${order.loanperiod}" nullmsg="需要用多久" errormsg="需要用多久">
                                <span  class="pull-right info_state"><label class="textLabel">需要用多久</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  id="whereRedBook" onclick='openChooseLayer({id:"whereRedBook",selectOption:[<tool:code type="whereRedBook"  selection="true"  ></tool:code>]});'>
                                <span  class="pull-left">红本被抵押在哪里</span>
                                <input type="hidden" name="whereRedBook" value="${order.whereRedBook}" nullmsg="红本被抵押在哪里" errormsg="红本被抵押在哪里">
                                <span  class="pull-right info_state"><label class="textLabel">红本被抵押在哪里</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                            </a>
                        </li>
                        <li>
                            <a    onclick='openAgreementLayer()'>
                                <span  class="pull-left">居间服务协议</span>
                                <input type="hidden" name="whereRedBook" value="${order.whereRedBook}" nullmsg="居间服务协议" errormsg="居间服务协议">
                                <span  class="pull-right info_state"><label class="textLabel agrtValueLabel">是否同意居间服务协议</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
                </c:if>
                <input type="checkbox" id="inlineCheckbox3" style="display: none">
<%--        <div class="checkbox m-l20">--%>
<%--            <label style="display: none">--%>
<%--&lt;%&ndash;                <input type="checkbox" id="inlineCheckbox3" >&ndash;%&gt;--%>
<%--            </label>--%>
<%--            <div id="checkboxImg" style="cursor: pointer">--%>
<%--                <div style="height: 50px; width:28px; float: left; margin-right: 0px; margin-left: -8px;">--%>
<%--                    <img src="<c:url value="/res/images/btn/checkbox-checked.png"/>" style="width: 16px; height: 16px;">--%>
<%--                </div>--%>
<%--                <small style="font-size: 13px;">同意<a href="<c:url value="/protocol/showProtocolTemplate.do?template=default-service-v1"/>">居间服务协议</a></small>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>

    <div class="row"  >
        <div class="p-10">
        	<input type="hidden" name="loantype" value="${loanType}"  />
            <input type="button" disabled="disabled" id="nextBtn" class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive" value="下一步"
            onclick="stepNext('applyStep2');">
        </div>
    </div>
</div>
</div>
</form>

<script type="text/javascript">

    var myMBModal=null;

    function openAgreementLayer(opt)
    {
        // alert(0);
        var opts = $.extend({selectOption: [], id: null}, opt);
        var inputId=opts.id;
        var agrtTitle="<div style='text-align: left'><h3 >阅读并同意协议</h3>";
        var agrt=$("#agreementFrame").contents().find("#agrtContent").html();
        agrt+="<div style=\"clear: both;\"><div class=\"p-20 m-20\">\n" +
            "                <a  class=\"btn btn-primary btn-block xwc_btn_block_lg\"   onclick=\"closeAgreementLayer(this);\">\n" +
            "                    我已阅读并同意本协议" +
            "                </a>\n" +
            "            </div></div>";
        myMBModal=new MBModal({model:"bottomLayer", content:(agrtTitle+agrt)});
        myMBModal.show();
    }

    function closeAgreementLayer()
    {
        $("label.agrtValueLabel").text("同意");
        if(!$("label.agrtValueLabel").hasClass("textLabel_active"))
        {
            $("label.agrtValueLabel").addClass("textLabel_active")
        }
        $('#inlineCheckbox3').attr("checked","checked");
        myMBModal.remove();
        checkInputValueStatus();
    }
</script>
<script type="text/javascript">
    function stepNext(id) {
        document.forms[0].submit();
    }

    function checkInputValueStatus(){
        var checkHasValue=false;
        <c:if test="${empty loanType || loanType == '1' }">
        checkHasValue=$("input[name='applyamount']").val()!="" && $("input[name='loanperiod']").val()!="" && $("input[name='repaytype']").val()!="" && $("input[name='deducthouse']").val()!="" && $('#inlineCheckbox3').attr("checked") =="checked";
        </c:if>
        <c:if test="${not empty loanType || loanType == '2' }">
        checkHasValue=$("input[name='applyamount']").val()!="" && $("input[name='loanperiod']").val()!="" && $("input[name='repaytype']").val()!="" && $("input[name='houseprice']").val()!="" && $("input[name='wherehouse']").val()!="" && $("input[name='redbooktype']").val()!="" && $('#inlineCheckbox3').attr("checked") =="checked";
        </c:if>
        <c:if test="${not empty loanType || loanType == '3' }">
        checkHasValue=$("input[name='applyamount']").val()!="" && $("input[name='loanperiod']").val()!="" && $("input[name='whereredbook']").val()!="" && $('#inlineCheckbox3').attr("checked") =="checked";
        </c:if>
        if(checkHasValue)
        {
            var submitBtn= $("#nextBtn");
            if(submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.removeClass("xwc_btn_inactive")
                submitBtn.removeClass("disabled");
                submitBtn.removeAttr("disabled");
            }

        }else{

            var submitBtn= $("#nextBtn");
            if(!submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.addClass("xwc_btn_inactive")
                submitBtn.addClass("disabled");
                submitBtn.attr("disabled","disabled");
            }
        }
    }
    $("#inlineCheckbox3").change(function() {
        checkInputValueStatus();
    });
    $("input[name='applyamount']").bind("keyup",function () {

        checkInputValueStatus();
    });
    $("#checkboxImg").bind("click",function () {

        if($('#inlineCheckbox3').attr("checked") =="checked")
        {
            $("#checkboxImg").find("img").attr("src",'<c:url value="/res/images/btn/checkbox-unchecked.png"/>');
            // $('#inlineCheckbox3').checked=false;
            // $('#inlineCheckbox3').attr("checked",false);
            $('#inlineCheckbox3').removeAttr("checked");

        }else{
            $("#checkboxImg").find("img").attr("src",'<c:url value="/res/images/btn/checkbox-checked.png"/>');
            // $('#inlineCheckbox3').checked=true;
            $('#inlineCheckbox3').attr("checked","checked");
        }

        checkInputValueStatus();
    });
    function openChooseLayer(opt)
    {
        var opts = $.extend({selectOption: [], id: null}, opt);
        var inputId=opts.id;
        new MBModal({model:"select",
            cancel:true,
            title:"请选择",
            selected:function(item){
                $("#"+inputId+" input[type='hidden']").val(item.value);
                $("#"+inputId+" .textLabel").text(item.text);
                if(!$("#"+inputId+" .textLabel").hasClass("textLabel_active"))
                {
                    $("#"+inputId+" .textLabel").addClass("textLabel_active")
                }
                checkInputValueStatus();
                return true;},
            selectOption:opts.selectOption}).show();
    }



</script>
<iframe id="agreementFrame" src="../protocol/showProtocolTemplate.do?template=default-service-v1" style="display: none" />
</body>
</html>


