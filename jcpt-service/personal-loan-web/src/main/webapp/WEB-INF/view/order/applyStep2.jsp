<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
	<script src="<c:url value="/res/js/common/global.js"/>"></script>
	<script src="<c:url value="/res/js/idNoTools.js"/>"></script>
	<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.13&key=912118f499f5a67524046e6443d92e97"></script> 
	
    <style type="text/css">
        .info_list a.sel_txt {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
        }
        .info_list a:hover {
            text-underline: none;
            text-decoration: none;
        }
        .info_list a input.txtIpt {
            display: block;
            -webkit-box-flex: 1;
            line-height: 30px;
            height: 30px;
            -webkit-flex: 1;
            flex: 1;
            font-size: 13px;
            padding-left: 15px;
            border:0px;
        }
        input.pull-right{
            text-align: right;
        }
        .info_list a input:focus,.info_list a input:hover {
            outline: 0;
            box-shadow: none;
        }

        .textLabel{
            font-weight: 200;
        }
        .textLabel_active{
            color:#000000;
        }
        input::-webkit-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input::-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-ms-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
    </style>

    <script type="text/javascript">
        var occupationList=[<tool:code type="job"  selection="true"  ></tool:code>];
        //职场白领
       //个体工商
        //中小企业主
        // 自由职业
        var communityList=[<tool:code type="community"  selection="true"  ></tool:code>];
        
        function openChooseLayer(opt)
        {
            var opts = $.extend({selectOption: [], id: null}, opt);
            var inputId=opts.id;
            new MBModal({model:"select",cancel:true,title:"请选择",selected:function(item){$("#"+inputId+" input[type='hidden']").val(item.value);
                    $("#"+inputId+" .textLabel").text(item.text);
                    if(!$("#"+inputId+" .textLabel").hasClass("textLabel_active"))
                    {
                        $("#"+inputId+" .textLabel").addClass("textLabel_active")
                    }
                    checkInputValueStatus();
                    return true;},selectOption:opts.selectOption}).show();
        }

       // function openChooseOccupationList

    </script>

</head>
<body class="default_body_bg">
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<form method="post" action="<c:url value='/loanorder/savePerson.do'/>">
<div class="container-fluid">
    <div class="row">
        <div class="card">
                <p class="m-l15 p-t10 xwc_normal_title">资产信息</p>
                <ul class="info_list">
                    <li >
                        <a  id="occupation" onclick='openChooseLayer({id:"occupation",selectOption:occupationList});' >
                            <span class="pull-left">职业</span>
                            <input type="hidden" name="job" id="job" value="${person.job }" />
                            <span class="pull-right info_state"><label class="textLabel">请职业身份</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                    <li >
                        <a  id="insurance" onclick='openChooseLayer({id:"insurance",selectOption:[<tool:code type="insurance"  selection="true"  ></tool:code>]});'>
                            <span class="pull-left">是否有社保</span>
                            <input type="hidden" name="insurance"   value="${person.insurance }" />
                            <span class="pull-right info_state"><label class="textLabel">请选择是否有社保</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                    <li >
                        <a   id="ownHouse" onclick='openChooseLayer({id:"ownHouse",selectOption:[<tool:code type="hashouse"  selection="true"  ></tool:code>]});'>
                            <span class="pull-left">是否有房</span>
                            <input type="hidden" name="hashouse"   value="${person.hashouse }" />
                            <span class="pull-right info_state"><label class="textLabel">请选择是否有房</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                    <li >
                        <a id="providentfund" onclick='openChooseLayer({id:"providentfund",selectOption:[<tool:code type="providentfund"  selection="true"  ></tool:code>]});'>
                            <span class="pull-left">是否有公积金</span>
                            <input type="hidden" name="providentfund"   value="${person.providentfund }" />
                            <span class="pull-right info_state"><label class="textLabel">请选择是否有公积金</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                    <li >
                        <a id="creditreport" onclick='openChooseLayer({id:"creditreport",selectOption:[<tool:code type="creditreport"  selection="true"  ></tool:code>]});'>
                            <span class="pull-left">人行征信报告</span>
                            <input type="hidden" name="creditreport"   value="${person.creditreport }" />
                            <span class="pull-right info_state"><label class="textLabel">请选择人行征信逾期情况</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>

                </ul>
        </div>
        <div class="card">
                <ul class="info_list">
                    <li >
						<%--
						
                        <a  id="community" onclick='openChooseLayer({id:"community",selectOption:communityList});'>
                            <span class="pull-left">所住小区</span>
                            <input type="hidden" name="community"   value="${person.community }" />
                            <span class="pull-right info_state"><label class="textLabel">请选择所住小区</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
						 --%>
                         <a  id="community" onclick='showMap();'>
                            <span class="pull-left">所住小区</span>
                            <input type="hidden" name="community"   value="${person.community }" />
                            <span class="pull-right info_state"><label class="textLabel">请选择所住小区</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                </ul>
        </div>

    </div>

    <div class="row ">
        <div class="p-10">
           <!-- 
            <input type="button" id="nextBtn" disabled="disabled" class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive disabled" value="下一步" onclick="stepNext();">
            -->
            <input type="button" id="nextBtn"  class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive disabled" value="下一步" onclick="stepNext();">
        </div>
    </div>
    
    <div id="address">
    </div>
</div>

<input type="hidden" name="loanid" value="${person.loanid }" />
<input type="hidden" name="personid" value="${person.personid }" />

<div id="mapselect" style="display:none">
<div class="modal model-bottom modal-active">
	<div class="modal-dialog model-select">
		<div class="modal-bd faster">
			
			所在城市:	<select id="currentCity" style="line-height: 30px;height:30px;">
						
				    </select>
		         小区名称：<input type="text" name="searchCommunity"  id="searchCommunity" style="line-height: 10px;" />
				<input type="button" onclick="aMapSearch();" value="查询小区" style="line-height: 10px;">
				    
			<div class="modal-header">
				
				<span class="closeBtn" style="float: right;" data-btn="cancel">
					<i class="fa fa-chevron-down" aria-hidden="true"></i>
				</span>
			</div>
			<div style="overflow-y:scroll;max-height:557px;">
				<ul style="margin: 0;padding: 0;" id="communityItem">
				</ul>
			</div>
		</div>
	</div>
</div>
</div>

</form>

</body>
</html>
<script type="text/javascript">
    function stepNext(id) {
    	var currentLoanId = sessionStorage.setItem('loanid',"${person.loanid }");
        document.forms[0].submit();
    }
    function checkInputValueStatus(){
    	
        if( $("input[name='job']").val()!="" && $("input[name='insurance']").val()!=""
            && $("input[name='hashouse']").val()!="" && $("input[name='providentfund']").val()!=""
            && $("input[name='creditreport']").val()!=""){
        	
            var submitBtn= $("#nextBtn");
            if(submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.removeClass("xwc_btn_inactive")
                submitBtn.removeClass("disabled");
                submitBtn.removeAttr("disabled");
            }

        }else{

            var submitBtn= $("#nextBtn");
            if(!submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.addClass("xwc_btn_inactive")
                submitBtn.addClass("disabled");
                submitBtn.attr("disabled","disabled");
            }
        }
    }

    $("input[type='text']").bind("keyup",function () {

        checkInputValueStatus();
    });

    
    $("input[name='idno']").blur(function () {
    	var isIdno = isIdCardNo($("input[name='idno']").val());
    	if(false == isIdno){
    		new MBModal({model:"dialog",cancel:true,cancelText:"关闭",autoClose:true,content:"请输入正确格式的身份证号"});
    		return false;
    	}
    });
    
</script>

<script type="text/javascript">

function showMap(){
	var cityList=[<tool:code type="city"  selection="true"  ></tool:code>];
	var idx = 0;
	var opt = "";
	for(; idx < cityList.length; idx ++){
		if(idx == 0 ){
			opt += "<option selected='selected' value='"+ cityList[idx].value+"'>"+cityList[idx].text+"</option>";
		}else{
			opt += "<option value='"+ cityList[idx].value+"'>"+cityList[idx].text+"</option>";			
		}
	}
	$("#currentCity").html(opt);
	$("#mapselect").show();	
	
	getCurrentPosition();
	
}

function getCurrentPosition() {
   
	AMap.service('AMap.Geolocation', function() {
        geolocation = new AMap.Geolocation({
            enableHighAccuracy : true, //是否使用高精度定位，默认:true
            timeout : 10000, //超过10秒后停止定位，默认：无穷大
            buttonOffset : new AMap.Pixel(10, 20), //定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
            zoomToAccuracy : true, //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
            buttonPosition : 'RB'
        });
        geolocation.getCurrentPosition();
        AMap.event.addListener(geolocation, 'complete', onComplete);
    });
};

function onComplete(data){
	var centerPoint =[data.position.getLng(), data.position.getLat()];
	var city = $("#currentCity").val();
	aMapSearchNearBy(centerPoint,city);
}

//高德地图查询周边
function aMapSearchNearBy(centerPoint, city) {
    AMap.service(["AMap.PlaceSearch"], function() {
        var placeSearch = new AMap.PlaceSearch({
            pageSize: 10,    // 每页10条
            pageIndex: 1,    // 获取第一页
            type:"商务住宅|住宅区|住宅小区",
            city: city       // 指定城市名(如果你获取不到城市名称，这个参数也可以不传，注释掉)
        });

        // 第一个参数是关键字，这里传入的空表示不需要根据关键字过滤
        // 第二个参数是经纬度，数组类型
        // 第三个参数是半径，周边的范围
        // 第四个参数为回调函数
        placeSearch.searchNearBy('', centerPoint, 1000, function(status, result) {
            if(result.info === 'OK') {
                var locationList = result.poiList.pois; // 周边地标建筑列表
                createLocationHtml(locationList);
				console.log(locationList);
            } else {
                 console.log('获取位置信息失败!');
            }
        });
   });
}

function aMapSearch() {
	
	var city = $("#currentCity").val();
	var searchKey = $("#searchCommunity").val();
	
    AMap.service(["AMap.PlaceSearch"], function() {
        var placeSearch = new AMap.PlaceSearch({
            pageSize: 10,    // 每页10条
            pageIndex: 1,    // 获取第一页
            type:"商务住宅|住宅区|住宅小区",
            city: city       // 指定城市名(如果你获取不到城市名称，这个参数也可以不传，注释掉)
        });

        // 第一个参数是关键字，这里传入的空表示不需要根据关键字过滤
        // 第二个参数是经纬度，数组类型
        // 第三个参数是半径，周边的范围
        // 第四个参数为回调函数
        placeSearch.search(searchKey, function(status, result) {
            if(result.info === 'OK') {
                var locationList = result.poiList.pois; // 周边地标建筑列表
　　　　　　　　　 // 生成地址列表html
　　　　　　　　　createLocationHtml(locationList);
			console.log(locationList);
            } else {
                 console.log('获取位置信息失败!');
            }
        });
   });
}
function createLocationHtml(locationList){
	var i=0,len=locationList.length;
	var itemHtml ="";
	for ( ; i<len; i++){
		if(locationList[i].address && locationList[i].address != ''){
			itemHtml +='<li class=\"selectoption\" data-value=\"'+locationList[i].name+'\">'+locationList[i].name+'('+locationList[i].address+')'+'</li>';
		}else{
			itemHtml +='<li class=\"selectoption\" data-value=\"'+locationList[i].name+'\">'+locationList[i].name+'</li>';
		}
	}
	$("#communityItem").html(itemHtml);
	$("#communityItem li").on("click",function(){
		var selectValue=$(this).data('value')
		var selectText=$(this).text();
		selectValue = $("#currentCity").val()+"."+selectValue;
		selectText = $("#currentCity").val()+"."+selectText;
		$("#community input[type='hidden']").val(selectText);
        $("#community .textLabel").text(selectText);
        if(!$("#community .textLabel").hasClass("textLabel_active"))
        {
            $("#community .textLabel").addClass("textLabel_active")
        }
        checkInputValueStatus();
        $("#mapselect").hide();
        return true;
	});
	
}
</script>