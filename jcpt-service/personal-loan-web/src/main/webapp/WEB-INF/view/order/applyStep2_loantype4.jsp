<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script src="<c:url value="/res/js/common/global.js"/>"></script>
    <script src="<c:url value="/res/js/idNoTools.js"/>"></script>
    <script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.13&key=912118f499f5a67524046e6443d92e97"></script>

    <style type="text/css">
        .info_list a.sel_txt {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
        }
        .info_list a:hover {
            text-underline: none;
            text-decoration: none;
        }
        .info_list a input.txtIpt {
            display: block;
            -webkit-box-flex: 1;
            line-height: 30px;
            height: 30px;
            -webkit-flex: 1;
            flex: 1;
            font-size: 13px;
            padding-left: 15px;
            border:0px;
        }
        input.pull-right{
            text-align: right;
        }
        .info_list a input:focus,.info_list a input:hover {
            outline: 0;
            box-shadow: none;
        }

        .textLabel{
            font-weight: 200;
        }
        .textLabel_active{
            color:#000000;
        }
        input::-webkit-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input::-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-ms-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
    </style>

    <script type="text/javascript">

        function openChooseLayer(opt)
        {
            var opts = $.extend({selectOption: [], id: null}, opt);
            var inputId=opts.id;
            new MBModal({model:"select",cancel:true,title:"请选择",selected:function(item){$("#"+inputId+" input[type='hidden']").val(item.value);
                    $("#"+inputId+" .textLabel").text(item.text);
                    if(!$("#"+inputId+" .textLabel").hasClass("textLabel_active"))
                    {
                        $("#"+inputId+" .textLabel").addClass("textLabel_active")
                    }
                    checkInputValueStatus();
                    return true;},selectOption:opts.selectOption}).show();
        }

    </script>

</head>
<body class="default_body_bg">
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<form method="post" action="<c:url value='/loanorder/savePerson.do'/>">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <p class="m-l15 p-t10 xwc_normal_title">企业经营状况</p>
                <ul class="info_list">
                    <li >
                        <a class="sel_txt">
						<span class="pull-left">
	                      最近一年收入
						</span>
                            <input name="incomeAmt" value="" type="number" class="pull-right txtIpt required" maxlength="10"  placeholder="万元">
                        </a>
                    </li>
                    <li >
                        <a class="sel_txt">
						<span class="pull-left">
	                      当前负债总额
						</span>
                            <input name="debtAmt" value="" type="number" class="pull-right txtIpt required" maxlength="10"  placeholder="万元">
                        </a>
                    </li>
                    <li >
                        <a class="sel_txt">
						<span class="pull-left">
	                      最近一年纳税
						</span>
                            <input name="taxesAmt" value="" type="number" class="pull-right txtIpt required" maxlength="10"  placeholder="万元">
                        </a>

                    </li>
                    <li >
                        <a class="sel_txt">
						<span class="pull-left">
	                      最近一年企业所得税
						</span>
                            <input name="incomeTaxAmt" value="" type="number" class="pull-right txtIpt required" maxlength="10"  placeholder="万元">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card">
                <p class="m-l15 p-t10 xwc_normal_title">逾期状况</p>
                <ul class="info_list">
                    <li >
                        <a id="incOverdueTime" onclick='openChooseLayer({id:"incOverdueTime",selectOption:[<tool:code type="overdueTime"  selection="true"  ></tool:code>]});'>
                            <span class="pull-left">企业在人民银行的逾期次数</span>
                            <input type="hidden" name="overdueTime"  class="required" value="" />
                            <span class="pull-right info_state"><label class="textLabel">请选择</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                    <li >
                        <a id="shareholderOverdueTime" onclick='openChooseLayer({id:"shareholderOverdueTime",selectOption:[<tool:code type="overdueTime"  selection="true"  ></tool:code>]});'>
                            <span class="pull-left">大股东在人民银行的逾期次数</span>
                            <input type="hidden" name="shareholderOverdueTime"  class="required" value="" />
                            <span class="pull-right info_state"><label class="textLabel">请选择</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <div class="row ">
            <div class="p-10">
                <input type="button" id="nextBtn"  class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive disabled" value="下一步" onclick="stepNext();">
            </div>
        </div>

        <div id="address">
        </div>
    </div>

    <input type="hidden" name="loanid" class="required" value="${person.loanid }" />
    <input type="hidden" name="personid" class="required" value="${person.personid }" />
    <input type="hidden" name="loanType" class="required" value="${loanType}" />

</form>

</body>
</html>
<script type="text/javascript">
    function stepNext(id) {
        var currentLoanId = sessionStorage.setItem('loanid',"${person.loanid}");
        document.forms[0].submit();
    }
    function checkInputValueStatus(){

        var requiredValue=true;
        $("input.required").each(function (item) {
            if($(this).val()=="")
            {
                requiredValue=false;
            }
        });
        if(requiredValue){

            var submitBtn= $("#nextBtn");
            if(submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.removeClass("xwc_btn_inactive")
                submitBtn.removeClass("disabled");
                submitBtn.removeAttr("disabled");
            }

        }else{

            var submitBtn= $("#nextBtn");
            if(!submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.addClass("xwc_btn_inactive")
                submitBtn.addClass("disabled");
                submitBtn.attr("disabled","disabled");
            }
        }
    }

    $("input.required").bind("keyup",function () {

        checkInputValueStatus();
    });
</script>
