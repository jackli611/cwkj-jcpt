<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/taglib/tool.tld" prefix="tool" %>
<c:forEach items="${pagedata.page}" var="order">
<div class="panel panel-transparent  m-t20" onclick="window.location.href='<c:url value="/loanorder/viewOrder.do?loanid=${order.loanid}"/>'">
    <div class="panel-heading">
        <div class="container-fluid">
        <div class="row">
        <span class="pull-left" >订单号：${order.ordercode}</span>
        <span class="pull-right">类型：
        	<c:choose>
        		<c:when test="${order.loantype eq 1 }">个人信用贷</c:when>
        		<c:when test="${order.loantype eq 2 }">红本抵押</c:when>
        		<c:when test="${order.loantype eq 3 }">红本赎楼过桥</c:when>
        		<c:when test="${order.loantype eq 4 }">企业信用贷</c:when>
        		<c:when test="${order.loantype eq 5 }">置换红本抵押</c:when>
        		<c:otherwise>智能借款</c:otherwise>
        	</c:choose> 
        </span>
        </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="container-fluid">
            <div class="row">
		        <span class="pull-left" >申请日期：<f:formatDate value="${order.createtime}" pattern="yyyy-MM-dd" /></span>
		        <span class="pull-right">状态：<tool:code type="personloanstatus" selection="false" value="${order.loanstatus}"></tool:code></span>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
		        <span class="pull-left" >借款金额：￥<f:formatNumber value="${order.applyamount}" pattern="##,###,##0.0#" />元</span>
		                <span class="pull-right">
		            <a  href="##" onclick="window.location.href='<c:url value="/loanorder/viewOrder.do?loanid=${order.loanid}"/>'">查看订单详情<i class="fa fa-angle-right m-l10" aria-hidden="true"></i></a>
		        </span>
            </div>
        </div>
    </div>
</div>
</c:forEach>