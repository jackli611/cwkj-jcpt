<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="/WEB-INF/taglib/tool.tld" prefix="tool" %>
<div class="row">
        <hr>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <strong>姓名：${person.realname}</strong>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            	身份证：${person.idno}
        </div>        
        <br>

    </div>
	
<div class="row">
    <ul  class="list-unstyled p-20">
<c:choose>
    <c:when test="${order.loantype eq 4 }">
        <li>
            <span>最近一年收入：<c:out value="${person.incomeAmt}"/></span>
        </li>
        <li>
            <span>当前负债总额：<c:out value="${person.debtAmt}"/></span>
        </li>
        <li>
            <span>最近一年纳税：<c:out value="${person.taxesAmt}"/></span>
        </li>
        <li>
            <span>最近一年企业所得税：<c:out value="${person.incomeTaxAmt}"/></span>
        </li>
        <li>
            <span>最近一年收入：<c:out value="${person.incomeAmt}"/></span>
        </li>
        <li>
            <span>最近一年收入：<c:out value="${person.incomeAmt}"/></span>
        </li>
        <li>
            <span>企业在人民银行的逾期次数：<c:out value="${person.overdueTime}"/></span>
        </li>
        <li>
            <span>大股东在人民银行的逾期次数：<c:out value="${person.shareholderOverdueTime}"/></span>
        </li>
    </c:when>
    <c:when test="${order.loantype eq 2 || order.loantype eq 5}">
        <li>
            <span>被抵押红本房在哪里：<tool:code type="whereHouse" selection="false" value="${person.wherehouse}"></tool:code></span>
        </li>
        <li>
            <span>红本房类型：<tool:code type="redBookType" selection="false" value="${person.redbooktype}"></tool:code></span>
        </li>
        <li>
            <span>被抵押房子价格：<c:out  value="${person.houseprice}"/></span>
        </li>

        <li>
            <span>职业：<tool:code type="job" selection="false" value="${person.job}"></tool:code></span>
        </li>
        <li>
            <span>是否有社保：<tool:code type="insurance" selection="false" value="${person.insurance}"></tool:code></span>
        </li>
        <li>
            <span>是否有房：<tool:code type="hashouse" selection="false" value="${person.hashouse}"></tool:code>(红本)</span>
        </li>
        <li>
            <span>是否有公积金：<tool:code type="providentfund" selection="false" value="${person.providentfund}"></tool:code></span>
        </li>
        <li>
            <span>人行逾期次数：<tool:code type="creditreport" selection="false" value="${person.creditreport}"></tool:code></span>
        </li>
        <li>
            <span>所住小区/楼：${person.community}</span>
        </li>

    </c:when>
    <c:when test="${order.loantype eq 3 }">
        <li>
            <span>职业：<tool:code type="job" selection="false" value="${person.job}"></tool:code></span>
        </li>
        <li>
            <span>是否有社保：<tool:code type="insurance" selection="false" value="${person.insurance}"></tool:code></span>
        </li>
        <li>
            <span>是否有房：<tool:code type="hashouse" selection="false" value="${person.hashouse}"></tool:code>(红本)</span>
        </li>
        <li>
            <span>是否有公积金：<tool:code type="providentfund" selection="false" value="${person.providentfund}"></tool:code></span>
        </li>
        <li>
            <span>人行逾期次数：<tool:code type="creditreport" selection="false" value="${person.creditreport}"></tool:code></span>
        </li>
        <li>
            <span>所住小区/楼：${person.community}</span>
        </li>
    </c:when>
    <c:otherwise>
        <li>
            <span>职业：<tool:code type="job" selection="false" value="${person.job}"></tool:code></span>
        </li>
        <li>
            <span>是否有社保：<tool:code type="insurance" selection="false" value="${person.insurance}"></tool:code></span>
        </li>
        <li>
            <span>是否有房：<tool:code type="hashouse" selection="false" value="${person.hashouse}"></tool:code>(红本)</span>
        </li>
        <li>
            <span>是否有公积金：<tool:code type="providentfund" selection="false" value="${person.providentfund}"></tool:code></span>
        </li>
        <li>
            <span>人行逾期次数：<tool:code type="creditreport" selection="false" value="${person.creditreport}"></tool:code></span>
        </li>
        <li>
            <span>所住小区/楼：${person.community}</span>
        </li>
    </c:otherwise>
</c:choose>
    </ul>
</div>
