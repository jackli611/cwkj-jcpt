<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">
        .card label {
            font-weight: 300;
        }
        .nobord{
            border: 0px !important;
        }
    </style>
    
</head>
<body class="bg-white">
<%--<header class="top-nav">--%>
    <%--<a class="link" href="javascript:history.go(-1)"  ><i class="fa fa-chevron-left" aria-hidden="true"></i></a>--%>
    <%--<h2>订单详情</h2>--%>
<%--</header>--%>

<div class="container-fluid">

	<div id="payInfo">
	</div>

    <div class="row m-t20">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <strong>订单号：${order.ordercode}</strong>
        </div>
        <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 m-t10">
            	状态：<tool:code type="personloanstatus" selection="false" value="${order.loanstatus}"></tool:code>
        </div>
        <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 m-t10">
            	借多少：￥<f:formatNumber value="${order.applyamount}" pattern="##,###,##0.0#" />元
        </div>
		<div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 m-t10">
            	申请日期：<f:formatDate value="${order.createtime}" pattern="yyyy-MM-dd" />
        </div>
        <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 m-t10">
                <c:choose>
                	<c:when test="${order.loantype eq 3 }">
            			借多久：<tool:code type="howLongForLoan" selection="false" value="${order.loanperiod}"></tool:code> 
                	</c:when>
                	<c:when test="${order.loantype eq 4 }">
            			借多久：<tool:code type="howLongForLoan" selection="false" value="${order.loanperiod}"></tool:code> 
                	</c:when>
                	<c:otherwise>
            			借多久：<tool:code type="loanPeriod" selection="false" value="${order.loanperiod}"></tool:code> 
                	</c:otherwise>
                </c:choose>
        </div>
        <br>

    </div>

	<div id="person"></div>
	
	<div class="row">
		<div class="p-l20">

			<i class="fa fa-check text-success" aria-hidden="true"></i><a href="<c:url value="/protocol/showProtocolTemplate.do?template=default-service-v1"/>"><span class="m-l10">居间服务协议</span><i class="fa fa-angle-right m-l10" aria-hidden="true"></i></a>
		</div>
	</div>

</div>
</body>
</html>
<script type="text/javascript">
	$(function(){
		//加载订单个人信息
		var orderHeaderParam = {"loanId":"${order.loanid}"};
		$("#person").load("<c:url value='/loanorder/getLoanPerson.do'/>",orderHeaderParam);
		//end 加载订单个人信息
		
		//加载订单支付按钮
		orderHeaderParam = {"loanid":"${order.loanid}"};
		$("#payInfo").load("<c:url value='/pay/getOrderPayStepBtn.do'/>",orderHeaderParam);
		//end 加载订单支付按钮
		
	});
	
</script>
