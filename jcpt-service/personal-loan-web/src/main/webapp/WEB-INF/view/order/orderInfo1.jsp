<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">

    </style>
</head>
<body>
<header class="top-nav">
    <a class="link"   ><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    <h2>订单详情</h2>
</header>

<div class="container-fluid">
    <div class="row p-20">

        <div class="container-fluid">
            <div class="row">
                <div class="p-20">
                    <div class="col-xs-6">姓名： 陈 * 瑛</div>
                    <div class="col-xs-6">状态： 未支付订金</div>
                    <div class="col-xs-12">身份证：460980883******2874</div>

                    <hr>
                    <div class="col-xs-12">订单：</div>
                    <div class="col-xs-12">申请日期：</div>
                    <div class="col-xs-12">借多少：</div>
                    <div class="col-xs-12">借多久：</div>
                    <div class="col-xs-12">怎么还：按月还本金和利息</div>
                    <div class="col-xs-12">能否抵押房子：能</div>
                    <div class="col-xs-12">职业：白领</div>
                    <div class="col-xs-12">是否有房：有房（红本）</div>
                    <div class="col-xs-12">是否社保：有</div>
                    <div class="col-xs-12">是否有公积金：有</div>
                    <div class="col-xs-12">人民银行征信：有房（红本）</div>
                    <div class="col-xs-12">所住的小区与楼：深圳金域帝豪</div>
                    <div class="col-xs-12">个人信息查询授权书></div>
                </div>
            </div>
        </div>

    </div>


</div>

<div class="container-fluid" id="payBtnDiv">
    <div class="row ">
        <div class="p-20">
            <input type="button" class="btn btn-primary btn-block btn-lg" value="定金" onclick="openPayInfo()">
        </div>
    </div>
</div>

</body>
</html>
