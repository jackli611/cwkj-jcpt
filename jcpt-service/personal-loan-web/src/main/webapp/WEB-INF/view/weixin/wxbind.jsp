<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script type="text/javascript" src="<c:url value='/res/js/wx.js'/>" ></script>
    <style type="text/css">
        input[type="password"]::-webkit-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
        input[type="password"]:-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
        input[type="password"]::-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
        input[type="password"]:-ms-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
    </style>

</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<form id="regForm" action="<c:url value='/wx/submitBind.do'/>" method="post">
    <input type="hidden" name="checkStatus" value="1">
    <div class="container-fluid">
        <div class="row p-20">
            <span class="xwc_normal_title">注册/登录</span>
            <div class="box m-t10">
                <label class=" xwc_inactive" style="position:relative; z-index: 99; right:0px; top:40px; float: right; cursor: pointer;">
                    <span  id="smsbtn" onclick="getsms();" class="btn-link">获取短信验证码</span>
                </label>
                <input id="userName" name="userName" type="text" class="form-control input-lg input-line" placeholder="请输入手机号">
                <input name="smscode" id="smscode" type="text" class="form-control input-lg input-line" placeholder="请输入短信验证码">
                <label class="checkbox-inline input-lg xwc_inactive" style="padding-right: 0px;">
                    <div style="display: none">
                        <input type="checkbox" id="inlineCheckbox3" value="option3" checked="checked" >
                    </div>


                </label>
            </div>

        </div>

        <div class="container-fluid"   >
            <div class="row ">
                <input type="button" id="submitBtn" disabled="disabled" onclick="btnRegister()"  class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive" value="我要绑定，以后微信免登陆"  >
            </div>
        </div>

		<!-- 
        <div class="container-fluid"  >
            <div class="row ">
                <div class="p-20 text-center xwc_inactive">
                    <a href="login.do?showNav=<c:out value="${param.showNav}"/>">登录</a> | <a href="<c:url value='/index.do'/>">以后再说，我先逛逛</a>
                </div>
            </div>
        </div>
         -->

    </div>
</form>
</body>
</html>
<script type="text/javascript" src="<c:url value='/res/js/sms.js'/>"></script>
<script type="text/javascript">
    function btnRegister(){
		var userName = $("#userName").val();
        if(userName  == ""){
            alertLayer('请输入手机号 ');
            return false;
        }
        var smscode = $("#smscode").val();
        if(smscode == ""){
            alertLayer('请输入短信验证码 ');
            return false;
        }
        var redirectUrl = "<c:url value='/wx/submitBind.do'/>?userName="+userName+"&smscode="+smscode;
        var newurl = getWXCode(redirectUrl);
        window.location = newurl;
    }
    
    function getsms(){
        if($("#smsbtn").text() != "获取短信验证码"){
            return false;
        }
        if($("#userName").val() == ""){
            alertLayer('请输入手机号 ');
            return false;
        }

       $("#smsbtn").text("60s");
       countdown("smsbtn");
       $.ajax({
           type:"POST",
           dataType:"json",
           url:"<c:url value='/sms/sendResetPswSms.do'/>",
           data: {"recievers":$("#userName").val()},
           async: false,
           success:function(data){
               if(data.code != "1"){
                   alertLayer(data.msg);
               }
           }
       });
    }

    function checkInputValueStatus() {
        var submitBtn=$("#submitBtn");
        var userName=$("#userName").val() ;
        var smscode=$("#smscode").val();
        if( userName!=null && userName!="" && smscode!="" && $('#inlineCheckbox3').is(':checked')){
            if(submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.removeClass("xwc_btn_inactive")
                submitBtn.removeClass("disabled");
                submitBtn.removeAttr("disabled");
            }
        }else{
            if(!submitBtn.hasClass("xwc_btn_inactive"))
            {
                submitBtn.addClass("xwc_btn_inactive")
                submitBtn.addClass("disabled");
                submitBtn.attr("disabled","disabled");
            }
        }
    }
    $(function(){

        $("input.input-line").bind("keyup",function () {

            checkInputValueStatus();
        });
        $("#checkboxImg").bind("click",function () {

            if($('#inlineCheckbox3').is(':checked'))
            {
                $("#checkboxImg").find("img").attr("src",'<c:url value="/res/images/btn/checkbox-unchecked.png"/>');
                $('#inlineCheckbox3').checked=false;

            }else{
                $("#checkboxImg").find("img").attr("src",'<c:url value="/res/images/btn/checkbox-checked.png"/>');
                $('#inlineCheckbox3').checked=true;
            }

            checkInputValueStatus();
        });
    });
</script>