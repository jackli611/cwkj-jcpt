<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script src="<c:url value="/res/js/common/global.js"/>"></script>
    <script src="<c:url value="/res/js/idNoTools.js"/>"></script>

    <style type="text/css">
        .info_list a.sel_txt {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
        }
        .info_list a:hover {
            text-underline: none;
            text-decoration: none;
        }
        .info_list a input.txtIpt {
            display: block;
            -webkit-box-flex: 1;
            line-height: 30px;
            height: 30px;
            -webkit-flex: 1;
            flex: 1;
            font-size: 13px;
            padding-left: 15px;
            border:0px;
        }
        input.pull-right{
            text-align: right;
        }
        .info_list a input:focus,.info_list a input:hover {
            outline: 0;
            box-shadow: none;
        }

        .textLabel{
            font-weight: 200;
        }
        .textLabel_active{
            color:#000000;
        }
        input::-webkit-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input::-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-ms-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
    </style>

    <script type="text/javascript">

        function openChooseLayer(opt)
        {
            var opts = $.extend({selectOption: [], id: null}, opt);
            var inputId=opts.id;
            new MBModal({model:"select",cancel:true,title:"请选择",selected:function(item){$("#"+inputId+" input[type='hidden']").val(item.value);
                    $("#"+inputId+" .textLabel").text(item.text);
                    if(!$("#"+inputId+" .textLabel").hasClass("textLabel_active"))
                    {
                        $("#"+inputId+" .textLabel").addClass("textLabel_active")
                    }
                    checkInputValueStatus();
                    return true;},selectOption:opts.selectOption}).show();
        }

        // function openChooseOccupationList

    </script>

</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<form method="post" action="profileSubmit.do" >
    <input type="hidden" name="id" value="<c:out value="${etcProfile.id}"/>" />
    <div class="container-fluid">

        <div class="row">
            <div class="card m-10" style="border: 1px solid #e4e4e4;">
                <ul class="info_list">
                    <li >
                        <a  id="etcCardType" onclick='openChooseLayer({id:"etcCardType",selectOption:[<tool:code type="etcCardType"  selection="true"  ></tool:code>]});' >
                            <span class="pull-left">申请卡类型</span>
                            <input type="hidden" class="required" name="cardType"   />
                            <span class="pull-right info_state"><label class="textLabel">请选择申请卡类型</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                    <li >
                        <a  id="hasEtcCard" onclick='openChooseLayer({id:"hasEtcCard",selectOption:[<tool:code type="hasEtcCard"  selection="true"  ></tool:code>]});' >
                            <span class="pull-left">是否有ETC卡</span>
                            <input type="hidden" class="required" name="usingCard"   />
                            <span class="pull-right info_state"><label class="textLabel">请选择是否有ETC卡</label><i class="fa fa-angle-right pull-right fa-2x" aria-hidden="true"></i></span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <div class="row ">
            <div class="p-10">
                <input type="button" id="nextBtn" disabled="disabled" class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive disabled" value="确认提交" onclick="stepNext();">
            </div>
        </div>



    </div>



</form>
<div style="position:absolute; bottom: 10px; width:100%;">
    <div class="row" >
        <div class="text-center" >
            <small style="color: #999999;">本服务由<strong>中国农业银行</strong>提供</small>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
    function stepNext(id) {
        document.forms[0].submit();
    }
    function checkInputValueStatus()
    {
        var checkHasValue=true;
        $("input.required").each(function () {
            if( $(this).val() == "")
            {
                checkHasValue=false;
            }
        });
        if(checkHasValue)
        {
            var submitBtn= $("#nextBtn");
            if(submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.removeClass("xwc_btn_inactive")
                submitBtn.removeClass("disabled");
                submitBtn.removeAttr("disabled");
            }

        }else{

            var submitBtn= $("#nextBtn");
            if(!submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.addClass("xwc_btn_inactive")
                submitBtn.addClass("disabled");
                submitBtn.attr("disabled","disabled");
            }
        }

    }
    function openChooseLayer(opt)
    {
        var opts = $.extend({selectOption: [], id: null}, opt);
        var inputId=opts.id;
        new MBModal({model:"select",cancel:true,title:"请选择",selected:function(item){$("#"+inputId+" input[type='hidden']").val(item.value);
                $("#"+inputId+" .textLabel").text(item.text);
                if(!$("#"+inputId+" .textLabel").hasClass("textLabel_active"))
                {
                    $("#"+inputId+" .textLabel").addClass("textLabel_active")
                }
                checkInputValueStatus();
                return true;},selectOption:opts.selectOption}).show();
    }

</script>

