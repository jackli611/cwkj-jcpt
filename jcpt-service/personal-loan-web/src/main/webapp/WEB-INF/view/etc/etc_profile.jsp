<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script src="<c:url value="/res/js/common/global.js"/>"></script>
    <script src="<c:url value="/res/js/idNoTools.js"/>"></script>
    <script type="text/javascript" src="<c:url value='/res/js/sms.js'/>"></script>

    <style type="text/css">
        .info_list a.sel_txt {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
        }
        .info_list a:hover {
            text-underline: none;
            text-decoration: none;
        }
        .info_list a input.txtIpt {
            display: block;
            -webkit-box-flex: 1;
            line-height: 30px;
            height: 30px;
            -webkit-flex: 1;
            flex: 1;
            font-size: 13px;
            padding-left: 15px;
            border:0px;
        }
        input.pull-right{
            text-align: right;
        }
        .info_list a input:focus,.info_list a input:hover {
            outline: 0;
            box-shadow: none;
        }

        .textLabel{
            font-weight: 200;
        }
        .textLabel_active{
            color:#000000;
        }
        input::-webkit-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input::-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
        input:-ms-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            color: #999999;
        }
    </style>

    <script type="text/javascript">

        function openChooseLayer(opt)
        {
            var opts = $.extend({selectOption: [], id: null}, opt);
            var inputId=opts.id;
            new MBModal({model:"select",cancel:true,title:"请选择",selected:function(item){$("#"+inputId+" input[type='hidden']").val(item.value);
                    $("#"+inputId+" .textLabel").text(item.text);
                    if(!$("#"+inputId+" .textLabel").hasClass("textLabel_active"))
                    {
                        $("#"+inputId+" .textLabel").addClass("textLabel_active")
                    }
                    checkInputValueStatus();
                    return true;},selectOption:opts.selectOption}).show();
        }

        // function openChooseOccupationList

    </script>

</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<form method="post"  >
    <div class="container-fluid">

        <div class="row">
            <div class="text-center m-10">
                <img src="<c:url value="/res/images/cw-gd/etc-banner.png"/>" style="width: 100%">
            </div>
        </div>
        <div class="row">
            <div class="card m-10" style="border: 1px solid #e4e4e4;">
                <ul class="info_list">
                    <li>
                        <a class="sel_txt">
						<span class="pull-left">
	                      姓名
						</span>
                            <input name="realName"  type="text" class="pull-right txtIpt required" value="<c:out value="${etcProfile.realName}"/>"  placeholder="请输入您的姓名">
                        </a>
                    </li>

                    <li>
                        <a class="sel_txt">
                            <span class="pull-left">手机号</span>
                            <input name="telphone" id="telphone" type="text" class="pull-right txtIpt required" value="<c:out value="${etcProfile.telphone}"/>" placeholder="请输入手机号" />
                        </a>
                    </li>
                    <li>
                        <a class="sel_txt">
                            <span class="pull-left">验证码</span>
                            <input name="smscode"  type="text" class="pull-right txtIpt required"  placeholder="请输入验证码" />
                            <span class="txtIpt pull-right m-l10 btn-link" id="smsbtn" onclick="getsms()">发送验证码</span>
                        </a>
                    </li>

                    <li>
                        <a class="sel_txt">
						<span class="pull-left">
	                      车牌号
						</span>
                            <input name="carno"  type="text" class="pull-right txtIpt required" value="<c:out value="${etcProfile.carno}"/>" placeholder="请输入车牌号" />
                        </a>
                    </li>
                    <li>
                        <a class="sel_txt">
						<span class="pull-left">
	                      行驶证持有人姓名
						</span>
                            <input name="driverUserName"  type="text" class="pull-right txtIpt required" value="<c:out value="${etcProfile.driverUserName}"/>" placeholder="请输入行驶证持有人姓名" />
                        </a>
                    </li>
                    <li>
                        <a class="sel_txt">
						<span class="pull-left">
	                      住址
						</span>
                            <input name="address"  type="text" class="pull-right txtIpt required" value="<c:out value="${etcProfile.address}"/>" placeholder="请输入住址" />
                        </a>
                    </li>

                </ul>
            </div>


        </div>

        <div class="row ">
            <div class="p-10">
                 <input type="button" id="nextBtn" disabled="disabled" class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive disabled" value="下一步" onclick="stepNext();" />
            </div>
        </div>

        <div class="row ">
            <div  class="text-center m-t20" >
                <small style="color: #999999;">本服务由<strong>中国农业银行</strong>提供</small>
            </div>
        </div>


    </div>



</form>

</body>
</html>
<script type="text/javascript">
    function stepNext(id) {

        if(checkInputValueStatus())
        {
            document.forms[0].submit();
        }

    }

    function checkInputValueStatus(){
        var checkHasValue=true;
        $("input.required").each(function () {
            if( $(this).val() == "")
            {
                checkHasValue=false;
            }
        });
        if(checkHasValue)
        {
            var submitBtn= $("#nextBtn");
            if(submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.removeClass("xwc_btn_inactive")
                submitBtn.removeClass("disabled");
                submitBtn.removeAttr("disabled");
            }

        }else{

            var submitBtn= $("#nextBtn");
            if(!submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.addClass("xwc_btn_inactive")
                submitBtn.addClass("disabled");
                submitBtn.attr("disabled","disabled");
            }
        }
        return checkHasValue;
    }

    $("input.required").bind("keyup",function () {

        checkInputValueStatus();
    });

    function getsms(){

        if($("#smsbtn").text() != "发送验证码" && $("#smsbtn").text() != "重新获取"){
            return false;
        }
        if($("#telphone").val() == ""){
            alertLayer('请输入手机号 ');
            return false;
        }

        loadJson({url:"../checkRegTelphone.do",data:{userName:$("#telphone").val(),checkStatus:2},complete:function (callbackObj) {

                if(callbackObj.code==1)
                {
                    $("#smsbtn").text("60s");
                    countdown("smsbtn");
                    $.ajax({
                        type:"POST",
                        dataType:"json",
                        url:"<c:url value='/sms/sendResetPswSms.do'/>",
                        data: {"recievers":$("#telphone").val()},
                        async: false,
                        success:function(data){
                            if(data.code == "1"){

                            }else{
                                alertLayer(data.msg);
                            }
                        }
                    });

                }else{
                    alertLayer(callbackObj.msg);
                    return false;
                }

            }});


    }
</script>

