<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">
        .text-success{
            color: #74e6b2;
        }
        .remarkText{
            color: #999999;
        }
    </style>
    <script type="text/javascript">
        function stepNext(id) {
            window.location='<c:url value="/index.do"/>';
        }
    </script>
</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<div class="container-fluid">
    <div class="row m-t20">

        <div class="box m-20 ">
            <div class="text-center text-success" style="font-size: 80px;">
                <img src="<c:url value="/res/images/cw-gd/paySuccess.png"/>" style="max-width: 50px; max-height: 50px;"/>
            </div>
            <div class="text-center  m-t10">
                <p>申请成功</p>
            </div>
        </div>

    </div>

    <div class="row remarkText">
        <div class="box m-20">
            <p class="text-center">
                <span>请耐心等待农业银行工作人员电话联系您。</span>
            </p>
            <p  class="text-center">
                <small>欢迎您，关注客服小橙微信号，有问题可以随时咨询哦
                </small>
            </p>

            <div class="text-center">
                <img style="max-width: 140px; max-height: 140px;" src="<c:url value='/res/images/kefu/xwckeji-kefu001.jpg'/>" />
            </div>
            <p  class="text-center">
                <small>长按上图，选择"识别图中二维码"</small>
            </p>
        </div>
    </div>


</div>


<div class="container-fluid" id="payBtnDiv">
    <div class="row ">
        <div class="p-20">
            <input type="button" class="btn btn-primary btn-block btn-lg" value="完成" onclick="stepNext()" >
        </div>
    </div>
</div>


</div>

<div style="position:absolute; bottom: 10px; width:100%;">
    <div class="row" >
        <div class="text-center" >
            <small style="color: #999999;">本服务由<strong>中国农业银行</strong>提供</small>
        </div>
    </div>
</div>
</body>
</html>
