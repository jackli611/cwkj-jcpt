<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd ">     
<html xmlns="http://www.w3.org/1999/xhtml ">     
<head>     
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>居间服务合同</title>
	<style type="text/css">         
		* {
			margin:0;
		}
		body {
			padding:0 80px;
			font-family:"Microsoft YaHei";
			font-size:12px;
			line-height: 22px;
		}
		.title {
			text-align: center;
			margin-top:40px;
			margin-bottom:10px;
		}
		.user-data {
			font-size:13px;
			color:#000;
			padding:5px 0;
			font-weight: 400;
		}
		.table-data {
			font-size:14px;
			color:#000;
			border-bottom:1px solid #000;
			border-right:1px solid #000;
		}
		.ps {
			margin-top:10px;
			color:#000;
			font-size:14px;
			font-weight: bold;
			margin-bottom:5px;
		}
		.paragraph {
			font-size:12px;
			color:#000;
			line-height: 25px;
			text-indent: 28px;
		}
		table {  
            border: 1px solid #000;  
            padding:0;   
            margin:0 auto; 
			margin-left: 100px;
			
            border-collapse: collapse;  
        }  
          
        td {  
            border: 1px solid #000;  
            font-size:12px;  
            padding: 3px 3px 3px 8px;  
            color: #4f6b72;  
        } 
		.investTable{
			width:400px;
		}
	</style>
</head>
<body>

	<h3 class="title">居间服务合同</h3>
	<div id="agrtContent" style=" text-align: left">
    	<h5 style="float:right; margin-right: 20px"> 编号：<span></span> </h5>
	<br />
	<p class="user-data">甲方(委托人，资金需求方): </p>
	<p class="user-data">甲方身份证号： </p>
	<p class="user-data">乙方(居间人)： <span> </span></p>
	<p class="paragraph">鉴于：甲方为乙方“小物橙”平台会员，甲方因个人资金需求，特委托乙方为甲方制定融资方案、
	订立合适的借款合同的机会。现甲乙双方(以下简称“双方”)根据《中华人民共和国合同法》及平等自
    愿的原则，经协商一致，订立本合同，以兹信守：</p>
	<p class="user-data">一、居间事项</p>
	<p class="paragraph">乙方根据甲方融资需求(包括但不限于借款目标金额及自身征信条件)进行信息分析，为甲方推荐适合的金融机构或贷款平台的融资产品。</p>
	<p class="user-data">二、居间服务期限</p>
	<p class="paragraph">本居间服务的服务有效期为90天，从甲方支付定金之日起算。未构成贷款方案沟通的天数不算入服务有效期内。由于受理甲方融资申请的金融机构或贷款平台审批时长原因导致90天内未放款的，在甲方未取消向金融机构或贷款平台融资申请的前提下，服务有效期延长至成功放款或融资申请被拒。</p>
	<p class="user-data">三、双方权利义务</p>
	<p class="paragraph">1、甲方保证其提供的所有个人资料及信息真实、准确且合法。如违反前述保证，甲方自行向融资资金提供方承担法律责任，乙方不承担任何保证及连带责任。</p>
	<p class="paragraph">2、在居间服务期限内，甲方应根据乙方要求积极配合乙方的工作，乙方将根据甲方自身征信条件，为甲方推荐若干资金提供方(金融机构或贷款平台)。</p>
	<p class="paragraph">3、本合同签订后，如乙方发现甲方征信及还款行为存在不良记录或被列入黑名单等情形，乙方有权暂时中止居间事项的办理，并通知甲方。甲方应在收到通知的7日内回复解决方案，否则，乙方有权单方面终止本合同，且不退还甲方已支付的定金。</p>
	<p class="paragraph">4、甲方应积极配合乙方完成居间事项，根据乙方要求提供资料和信息，如因甲方不配合导致居间服务不能顺利进行（包含但不限于乙方以甲方在“小物橙“平台(以下简称“乙方平台”填写的联系方式联系甲方后2日内未获得甲方回复、甲方在乙方通知后2日内未按照乙方要求提供资料和信息等)，乙方有权终止本合同，且不退还已支付的定金。</p>
	<p class="paragraph">5、甲方理解并同意，为达到甲方的目标金额，可能需要尝试多个方案。在乙方为甲方输出方案信息后，甲方应积极配合申请，并及时按乙方要求反馈申请结果。若甲方不配合申请，或未按乙方要求反馈申请结果。乙方有理由认为甲方已从乙方输出的方案信息中获益，乙方有权单方面终止合同，且不退还甲方已支付的定金。</p>
	<p class="paragraph">6、甲方理解并同意，乙方作为居间服务人，不保证用户的融资需求能够实际得到满足。甲方应尊重乙方的劳动，不得强迫、辱骂乙方工作人员。否则乙方有权单方面终止本合同，且不退还甲方已支付的定金。</p>
    <p class="paragraph">7、融资金额：双方约定目标金额，最终的融资金额以融资资金提供方实际审批结果为准。融资资金由融资资金提供方与甲方根据双方协议的约定直接向甲方支付，乙方不提供上述资金的转账及代收代付服务，亦不对上述资金的发放承担保证或连带责任。</p>
	<p class="paragraph"> 8、资金利息：以甲方与融资资金提供方的约定为准，由甲方根据其与融资资金提供方约定的方式支付。乙方不提供上述利息的代收代付服务，亦不对甲方可能的延迟支付利息的行为承担保证或连带清偿责任。</p>
	<p class="paragraph">9、借款本金的归还：甲方应按照其与融资资金提供方约定的期限和方式归还借款本金。乙方不对甲方本金的归还承担保证或连帯清偿责任。</p>
    <p class="paragraph">10、甲方保证遵守履约承诺，本次委托为其本人真实意思表示。</p>


    <p class="user-data">四、居间报酬</p>
	<p class="paragraph">1、本协议项下的居间报酬包括定金和信息咨询费。</p>
	<p class="paragraph">2、甲方在提交融资申请时，需支付一定金额的定金，定金的具体金额以平台页面实时展示的标</p>
	<p>准为准，定金需于本合同签订当日支付。</p>
	<p class="paragraph">3、若居间服务期限届满乙方未成功协助甲方获得融资资金，则乙方于居间服务期限届满之日或</p>
	<p>双方达成融资失败共识(以在前之日为准)起3个工作日内退还定金，但是由于甲方自身原因如甲方未如</p>
	<p>实提供个人信息、资料等原因导致未成功获得融资资金的，定金不予退还。</p>
	<p class="paragraph">4、甲方通过乙方服务获得借款成功（包括部分实现目标金额的借款），甲方需要在收到借款的24个小时内向乙方支付信息咨询费，支付方式为进入服务订单进行在线支付，费用标准如下：实际放款金额在30万元以下（不含30万元）的支付1299元；实际放款金额在30-100万元（不含100万元） 的支付5299元；实际放款金额在100-500万元（不含500万元）的支付10299元；实际放款金额在500万元以上的支付30299元；实际放款金额在1000万元以上的支付放款金额的2%。上述信息咨询费的计算不包含之前已经支付的定金。</p>

	<p class="user-data"><b>五、违约责任甲方应按本合同约定，及时支付信息咨询费，否则，甲方除应按本合同第四条的</b>
	<b>约定支付信息咨询费外还应支付违约金。违约金按每日10元计算，自逾期之日起至甲方付清之日止，</b>
	<b>违约金上限不超过本次信息咨询费总额的两倍。</b></p>

    <p class="user-data">六、争议解决条款</p>
	<p class="paragraph">1、未尽事宜，双方应友好协商解决，协商不成的，任一方均应提请深圳仲裁委员会按简易程序
	仲裁解决。</p>
	<p class="paragraph">2、如发生诉讼或仲裁等情况的，甲方自愿承担乙方因甲方违约所产生的一切损失(包括但不限
	于催收费、诉讼费、仲裁费、公证费、司法鉴定费、财产保全费、律师费等相关费用)。</p>
	<p class="paragraph">3、双方收件地址以其营业执照或身份证上记载的为准。如甲方拒收或因其他原因不能获得甲方
	签收的，则在乙方投递通知之日起5日后即视为甲方已经签收。</p>

	<p class="user-data">七、合同的终止本合同自发生以下任一情形时终止：</p>
	<p class="paragraph">(1)本合同约定的委托事项完成，且甲方付清居间报酬等相关费用；</p>
	<p class="paragraph">(2)经双方协商一致提前终止本合同；</p>
	<p class="paragraph">(3)一方根据本合同的约定解除本合同。</p>

	<p class="user-data" >八、其他</p>
	<p class="paragraph">1、甲方在乙方及其关联平台（包括但不限于网页、微信公众号、APP客户端）勾选“同意小物橙《会员注册协议》、《居间服务合同》”并完成定金支付，即视为甲方已经充分阅读并了解本合同的内容。甲方以支付定金的方式确认本协议生效后，本协议的条款对甲乙双方均具有约束力。</p>
	<p class="paragraph">2、本合同为电子合同，乙方平台为甲方提供本合同的保管服务。甲方若对本合同的真伪或内容有任何疑问，可以要求乙方平台进行核对。如甲方经查询仍对本合同的真伪、内容有任何争议，应以乙方平台或第三方合作机构记录为准。</p>
	<p class="paragraph">3、本合同独立于甲方在乙方平台签署的《会员注册协议》。本合同履行完毕或以任何形式终止或解除均不影响甲方与乙方签署的平台《会员注册协议》的有效性及法律效力。</p>
	<p class="paragraph" style="float:left;">
		<b>甲方(签名)</b>:
	</p>
	<p class="paragraph" style="float:right; margin-right:20px;"><b>乙方(签名)</b>:</p>
	<p class="paragraph" style="clear:both; float:left;">
		日期：
	</p>
	<p class="paragraph" style="float:right; margin-right: 50px;"> 日期： </p>
</div>
</body>
</html>