<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://res.wx.qq.com/open/js/jweixin-1.4.0.js" type="text/javascript"></script>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script type="text/javascript" src="<c:url value='/res/js/loan.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/res/js/wx.js'/>"></script>
    <style type="text/css">
        .panel-transparent{
            border-color: #ddd;
        }

        .panel-transparent>.panel-heading {
            color: #adadad;
            background-color: transparent;
            border-color: #ddd;
        }

        button:active{
            background-color: #549fff !important;
            background-image: none;
            outline: 0;
        }
    </style>
    <script type="text/javascript">
        
        $(function () {
            var h=$(window).height();
            if(h<600)
            {
                $("#companyFooter").removeClass("bottom-nav");
            }
        })
    </script>
</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<div class="container-fluid">
        <div class="row">

            <div class="text-center m-t50">
                <div class="btn-group xwc_btn_group_m" role="group" aria-label="...">
                    <button type="button" class="btn active">智能借款</button>
                    <button type="button" class="btn" onclick="javascript:window.location.href='usageMain.do?shareid=<c:out value="${shareid}"/>'">按用途借款</button>
                </div>
            </div>
        </div>

        <div class="row m-t60  text-center xwc_font_m xwc_text_gray">
            <span>最高可借额度</span>
        </div>
        <div class="row m-20  text-center xwc_font_lg xwc_bg_linear_1">
            <span ><i class="fa fa-jpy xwc_font_rmb_lg" aria-hidden="true"></i>50,000,000</span>
        </div>
        <div class="row m-t20  text-center xwc_font_m xwc_text_gray">
            <span>最低年利息 4.3%</span>
        </div>
        <div class="row m-t80  text-center">
                <button type="button"  class="btn btn-circle btn-xl xwc_btn_shadow active" onclick="toLoan('applyStep1','1')">去借款</button>
        </div>
</div>
<div class="container-fluid">

	<div id="orderLst" style="padding-bottom: 30px;">
	</div>
    <div class="row">
        <%@ include file="/WEB-INF/common/footer.jsp" %>
    </div>
</div>
<div id="wxpage" style="display:none;width:0px;height: 0px;">

</div>
<div id="nativeShare" ></div>

</body>
</html>
<script type="text/javascript">
$(function(){
	//加载订单
	<c:if test="${currentUser != null }">
	 var orderQueryParam = {};
	 $("#orderLst").load("<c:url value='/loanorder/orderList.do'/>",orderQueryParam,function(obj){ checkMyFooter();});
	</c:if>
	//end 加载订单

    loadWxShareJsconfig();

});

</script>
