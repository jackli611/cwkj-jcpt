<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/res/js/wx.js'/>" ></script>
	<style type="text/css">
		input[type="password"]::-webkit-input-placeholder {
			letter-spacing: 0;
			font-size: 14px;
			padding-top: 15px;
		}
		input[type="password"]:-moz-placeholder {
			letter-spacing: 0;
			font-size: 14px;
			padding-top: 15px;
		}
		input[type="password"]::-moz-placeholder {
			letter-spacing: 0;
			font-size: 14px;
			padding-top: 15px;
		}
		input[type="password"]:-ms-input-placeholder {
			letter-spacing: 0;
			font-size: 14px;
			padding-top: 15px;
		}
	</style>
    <script type="text/javascript">
		$(function(){
			if(isWeiXin())
			{
				window.location.href ="wxbindlogin.do";
				return ;
			}
			$("input.input-line").bind("keyup",function () {
				var submitBtn=$(":submit");
				var passwd=$(":password").val();
				var userName=$(":text").val();
				if(passwd!=null && passwd !="" && userName!=null && userName!="")
				{
					if(submitBtn.hasClass("xwc_btn_inactive")) {
						submitBtn.removeClass("xwc_btn_inactive")
						submitBtn.removeClass("disabled");
						submitBtn.removeAttr("disabled");
					}
				}else{
					if(!submitBtn.hasClass("xwc_btn_inactive"))
					{
						submitBtn.addClass("xwc_btn_inactive")
						submitBtn.addClass("disabled");
						submitBtn.attr("disabled","disabled");
					}
				}

			});
		});

		function submitLogin() {
			var passwd=$(":password").val();
			var userName=$(":text").val();
			if(passwd!=null && passwd !="" && userName!=null && userName!="")
			{
				return true;
			}
			return false;
		}

	</script>
</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>
<form action="<c:url value='/login.do'/>" method="post" onsubmit="return submitLogin()">
	
	<div class="container-fluid">
	    <div class="row p-20 m-t30">
	<span class="xwc_normal_title">账号密码登录</span>
			
	        <div class="box m-t20">
	            <input name="username" type="text" class="form-control input-lg input-line" placeholder="请输入手机号">
	            <input name="password" type="password" class="form-control input-lg input-line" placeholder="请输入密码" style="font-size:38px;">
	        </div>
	
	    </div>

	    <div class="container-fluid" >
	        <div class="row ">
	                <input type="submit" disabled="disabled" class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive" value="立即登录"  >
	        </div>
	    </div>
	
	    <div class="container-fluid" >
	        <div class="row ">
	            <div class="p-20 text-center xwc_inactive">
					<a href="register.do?showNav=<c:out value="${param.showNav}"/>">注册账号</a>  | <a href="resetPassword.do">忘记密码</a>
	            </div>
	        </div>
	    </div>
	
	</div>
	</form>
</body>
</html>
