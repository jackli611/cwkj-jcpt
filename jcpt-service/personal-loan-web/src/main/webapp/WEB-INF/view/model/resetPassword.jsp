<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">
        input[type="password"]::-webkit-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
        input[type="password"]:-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
        input[type="password"]::-moz-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
        input[type="password"]:-ms-input-placeholder {
            letter-spacing: 0;
            font-size: 14px;
            padding-top: 15px;
        }
    </style>

</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<form id="regForm" action="<c:url value='resetPassword.do'/>" method="post">
    <input type="hidden" name="checkStatus" value="1">
    <div class="container-fluid">
        <div class="row p-20">
            <span class="xwc_normal_title">重置密码</span>
            <div class="box m-t10">
                <label class=" xwc_inactive" style="position:relative; z-index: 99; right:0px; top:40px; float: right; cursor: pointer;">
                    <span  id="smsbtn" onclick="getsms();" class="btn-link">获取短信验证码</span>
                </label>
                <input id="userName" name="userName" type="text" class="form-control input-lg input-line" placeholder="请输入手机号">
                <input name="smscode" id="smscode" type="text" class="form-control input-lg input-line" placeholder="请输入短信验证码">
                <input id="password" name="password" type="password" class="form-control input-lg input-line" placeholder="请输入密码(不少于6位)" style="font-size:38px;">
                <input id ="password2" name="password2" type="password" class="form-control input-lg input-line" placeholder="请输入确认密码(不少于6位)" style="font-size:38px;">

                <label class="checkbox-inline input-lg xwc_inactive" style="padding-right: 0px;">
                    <div style="display: none">
                        <input type="checkbox" id="inlineCheckbox3" value="option3" checked="checked" >
                    </div>


                </label>
            </div>

        </div>



        <div class="container-fluid" id="payBtnDiv" >
            <div class="row ">
                <input type="button" id="submitBtn" disabled="disabled" onclick="btnRegister()"  class="btn btn-primary btn-block btn-lg xwc_btn_lg xwc_btn_inactive" value="重置密码"  >
            </div>
        </div>

        <div class="container-fluid" id="payBtnDiv">
            <div class="row ">
                <div class="p-20 text-center xwc_inactive">
                    <a href="login.do?showNav=<c:out value="${param.showNav}"/>">登录</a> | <a href="register.do?showNav=<c:out value="${param.showNav}"/>">注册账号</a>
                </div>
            </div>
        </div>

    </div>
</form>
</body>
</html>
<script type="text/javascript" src="<c:url value='/res/js/sms.js'/>"></script>
<script type="text/javascript">
    function btnRegister(){

        if($("#userName").val() == ""){
            alertLayer('请输入手机号 ');
            return false;
        }

        if($("#password").val() == ""){
            alertLayer('请输入密码 ');
            return false;
        }

        if($("#smscode").val() == ""){
            alertLayer('请输入短信验证码 ');
            return false;
        }

        if($('#password').val() != $('#password2').val() ) {
            alertLayer('您两次输入的密码不同 ');
            return false;
        }


        loadJson({url:"<c:url value='/checkPassword.do'/>",data:$("#regForm").serialize(),complete:function (callbackObj) {

                if(callbackObj.code==1)
                {
                    $("#regForm").submit();
                }else{
                    alertLayer(callbackObj.msg);
                    return false;
                }

            }});

        return false;
    }
    function getsms(){
        if($("#smsbtn").text() != "获取短信验证码"){
            return false;
        }
        if($("#userName").val() == ""){
            alertLayer('请输入手机号 ');
            return false;
        }




        loadJson({url:"checkRegTelphone.do",data:{userName:$("#userName").val(),checkStatus:1},complete:function (callbackObj) {

                if(callbackObj.code==1)
                {
                    $("#smsbtn").text("60s");
                    countdown("smsbtn");
                    $.ajax({
                        type:"POST",
                        dataType:"json",
                        url:"<c:url value='/sms/sendResetPswSms.do'/>",
                        data: {"recievers":$("#userName").val()},
                        async: false,
                        success:function(data){
                            if(data.code == "1"){

                            }else{
                                alertLayer(data.msg);
                            }
                        }
                    });

                }else{
                    alertLayer(callbackObj.msg);
                    return false;
                }

            }});


    }

    function checkInputValueStatus() {
        var submitBtn=$("#submitBtn");
        var passwd=$("#password").val();
        var passwd2=$('#password2').val();
        var userName=$("#userName").val() ;
        var smscode=$("#smscode").val();
        if(passwd!=null && passwd !="" && userName!=null && userName!="" && passwd2 !="" && smscode!="" && $('#inlineCheckbox3').is(':checked'))
        {
            if(submitBtn.hasClass("xwc_btn_inactive")) {
                submitBtn.removeClass("xwc_btn_inactive")
                submitBtn.removeClass("disabled");
                submitBtn.removeAttr("disabled");
            }
        }else{
            if(!submitBtn.hasClass("xwc_btn_inactive"))
            {
                submitBtn.addClass("xwc_btn_inactive")
                submitBtn.addClass("disabled");
                submitBtn.attr("disabled","disabled");
            }
        }
    }
    $(function(){

        $("input.input-line").bind("keyup",function () {

            checkInputValueStatus();
        });
        $("#checkboxImg").bind("click",function () {

            if($('#inlineCheckbox3').is(':checked'))
            {
                $("#checkboxImg").find("img").attr("src",'<c:url value="/res/images/btn/checkbox-unchecked.png"/>');
                $('#inlineCheckbox3').checked=false;

            }else{
                $("#checkboxImg").find("img").attr("src",'<c:url value="/res/images/btn/checkbox-checked.png"/>');
                $('#inlineCheckbox3').checked=true;
                //$('#inlineCheckbox3').attr("checked",true);
            }

            checkInputValueStatus();
        });
    });
</script>