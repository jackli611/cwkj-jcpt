<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">

    </style>
</head>
<body>
<header class="top-nav">
    <a class="link"   ><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    <h2>注册成功</h2>
</header>

<div class="container-fluid">
    <div class="row p-20">

        <div class="box p-20 ">
            <div class="text-center text-success">
                <i class="fas fa-check-circle fa-8x"></i>
            </div>
            <div class="text-center">
                <h3>注册成功</h3>
            </div>
        </div>

    </div>

    <div class="row p-20">
        <div class="box p-20 ">
            <div class="text-center">
                <h4>恭喜您，您已经成功注册小物橙账号！</h4>
            </div>

        </div>
    </div>


    <div class="container-fluid" id="payBtnDiv">
        <div class="row ">
            <div class="p-20">
                <input type="button" class="btn btn-primary btn-block btn-lg" value="立即借款"  >
            </div>
        </div>
    </div>



</div>

</body>
</html>
