<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">
        .panel-transparent{
            border-color: #ddd;
        }

        .panel-transparent>.panel-heading {
            color: #adadad;
            background-color: transparent;
            border-color: #ddd;
        }
    </style>
</head>
<body>
<%@ include file="/WEB-INF/common/navbar.jsp" %>
</body>
</html>

