<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script src="<c:url value="/res/js/loan.js"/>"></script>
    <style type="text/css">
        body{
        }
        .animatePerson {
            position: absolute;
            z-index: 9999;
            animation-duration: 2s;
            animation-delay: 4s;
            animation-iteration-count: 100;
            position: absolute;
            top:-20px;
            left: -75px;
            max-width: 150px;
        }
        .animatePersonBounce {
            animation-iteration-count:10000;
        }
        .loadWrapper{
            position: absolute;
            left:50%;
            top:50%;
            width:1px;
            height:1px;
        }
        #engine-light{
            position: absolute;
            top: -150px;
            left: -75px;
            z-index: 1;
            width: 150px;
        }
        .loader{
            margin-left: -162px;
            margin-top: -228px;
            width:304px;
            height:307px;
            background-repeat: no-repeat;
        }
        .loaderDesc {
            width: 200px;
            height: 20px;
            margin-left: -65px;
            color: #1a9ada;
            margin-top: 20px;
        }

    </style>
    <script type="text/javascript" >
        function animateCss_fadeInUp()
        {
            animateCss('.animatePerson', 'jackInTheBox',animateCss_filp);
        }

        function animateCss_filp()
        {
            if(!$(".animatePerson").hasClass("animatePersonBounce"))
            {
                $(".animatePerson").addClass("animatePersonBounce");
            }
            animateCss('.animatePerson','bounce');
            // animateCss('.animatePerson','pulse',function(){
            //    // $(".animatePerson").removeClass("animatePersonBounce");
            //     animateCss_filp();
            //    // animateCss('.animatePerson','fadeOutDown',animateCss_fadeInUp);
            //     // animateCss('.animatePerson','fadeOutDown',animateCss_fadeInUp);
            //     //$(".animatePerson").addClass("animatePersonBounce");
            // });
        }

        $(function(){
          //  animateCss('.animatePerson', 'bounce',animateCss_filp);
			window.setTimeout(function(){
				gotoPayPageByFeeType(sessionStorage.getItem('loanid'),'PAY001','299');
				},5000);
        });

    </script>
</head>
<body>
<div class="container bg-white">
    <div class="row">
        <div class="loadWrapper">
            <div id="engine-light">
                <img src="<c:url value="/res/images/welcome/engine2.png"/>" style="max-width:150px">
            </div>
            <div class="loader">
                <img id="enginePerson" src="<c:url value="/res/images/welcome/engline-light2.png"/>" class="animatePerson animated infinite fadeOutUpBig delay-1s">
            </div>
            <div class="loaderDesc">正在智能匹配资金方</div>
        </div>
    </div>
</div>
</body>
</html>
