<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <script type="text/javascript" src="<c:url value='/res/js/loan.js'/>"></script>
    <style type="text/css">
.swap_white{
    margin: 5px 20px;
    padding-top: 4px;
    background-color: white;
    border-radius: 4px;
}

.grid-border a{
    padding: 20px 10px;
}
.company{
color: #999999;
}

        .grid-border img{
            width:25px;
            height: 25px;
        }
    </style>
    <script type="text/javascript">
        function stepNext(id,loanType) {
            window.location="model.do?mod="+id+"&loanType="+loanType;
        }
    </script>
</head>
<body class="default_body_bg">
<%@ include file="/WEB-INF/common/navbar.jsp" %>

<div class="container-fluid">
    <div class="row">

        <div class="swap_white  m-t50 p-20">

            <div class="text-center" style="margin-top: 16px;">
                <div class="btn-group xwc_btn_group_m" role="group" aria-label="...">
                    <button type="button" class="btn" onclick="javascript:window.location.href='index.do?shareid=<c:out value="${shareid}"/>'">智能借款</button>
                    <button type="button" class="btn active">按用途借款</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="swap_white">
            <div class="row" style="margin-left: 0px ;margin-right: 0px;">
        <div class="card">
        <div class="card-heading">
            <span class="m-l10">借款服务</span>
        </div>
            <div class="grid-border">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item ">
                    <a href="##" onclick="toLoan('applyStep1','1')">
                    <img class="index-icon icon-newbie" src="res/images/cw-gd/creditLoan.png"></img>
                    <p>个人信用贷</p>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item">
                    <a href="##" onclick="toLoan('applyStep1','4')">
                        <img class="index-icon icon-aboutus" src="res/images/cw-gd/redbook2.png"></img>
                        <p>企业信用贷</p>
                    </a>

                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item grid-item-row-end">
                    <a href="##" onclick="toLoan('applyStep1','2')">
                        <img class="index-icon icon-safe" src="res/images/cw-gd/housingMortgage.png"></img>
                        <p>红本抵押</p>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item ">
                    <a  href="##" onclick="toLoan('applyStep1','3')" >
                        <img class="index-icon icon-invest" src="res/images/cw-gd/yhgq.png"></img>
                        <p>红本赎楼过桥</p></a>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item ">
                    <a  href="##" onclick="toLoan('applyStep1','5')">
                        <img class="index-icon icon-invest" src="res/images/cw-gd/dyzh.png"></img>
                        <p>置换红本抵押</p></a>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item grid-item-row-end">
                    <a href="https://cardloan.xiaoying.com/kadai/index?source=100020820" >
                        <img class="index-icon icon-invest" src="res/images/cw-gd/creditCard.png"></img>
                        <p>帮还信用卡</p></a>
                </div>


            </div>

        </div></div>
        </div>
    </div>


    <div class="row">
        <div class="swap_white">
            <div class="row" style="margin-left: 0px ;margin-right: 0px;">
        <div class="card">
            <div class="card-heading">
                <span class="m-l10">银行VIP服务</span>
            </div>
            <div class="grid-border">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item ">
                    <a href="https://credit.cardniu.com/activity/2018/creditcard_zhitou/?channel=caishenghuo">
                        <img class="index-icon icon-newbie" src="res/images/cw-gd/creditCard.png"></img>
                        <p>申请信用卡</p>
                    </a>
                </div>

            </div>
            <div class="grid-border">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-item ">
                    <a href="etc/profile.do">
                        <img class="index-icon icon-newbie" src="res/images/cw-gd/bankpng/bank_ny.png"></img>
                        <p>ETC</p>
                    </a>
                </div>

            </div>
        </div>

            </div>
        </div>
    </div>

    <div class="row">
        <%@ include file="/WEB-INF/common/footer.jsp" %>
    </div>

</div>
</body>
</html>
