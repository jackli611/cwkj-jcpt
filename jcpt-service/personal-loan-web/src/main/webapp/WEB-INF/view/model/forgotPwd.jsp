<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/common/header.jsp" %>
    <style type="text/css">
        .input-line{
            border-top:0px;
            border-left:0px;
            border-right: 0px;
            border-radius: 0px;
            box-shadow:none;
            -webkit-box-shadow:none;
        }
    </style>
</head>
<body>
<header class="top-nav">
    <a class="link"   ><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    <h2>找回密码</h2>
</header>

<div class="container-fluid">
    <div class="row p-20">

        <div class="box ">
            <input type="text" class="form-control input-lg input-line" placeholder="请输入手机号">
            <input type="text" class="form-control input-lg input-line" placeholder="请输入短信验证码">
            <input type="password" class="form-control input-lg input-line" placeholder="请输入密码">
            <input type="password" class="form-control input-lg input-line" placeholder="请输入确认密码">
        </div>

    </div>



    <div class="container-fluid" id="payBtnDiv">
        <div class="row ">
            <div class="p-20">
                <input type="button" class="btn btn-primary btn-block btn-lg" value="提交"  >
            </div>
        </div>
    </div>



</div>

</body>
</html>
