<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${not empty error}">
<script type="text/javascript">
        $(function () {
            new MBModal({model:"dialog",cancel:true,cancelText:"关闭",autoClose:true,content:"${error.message }"});
        });
    </script>
</c:if>
<c:if test="${not empty promptMsg}">
<script type="text/javascript">
        $(function () {
            new MBModal({model:"dialog",cancel:true,cancelText:"关闭",autoClose:true,content:"${promptMsg}"});
        });
    </script>
</c:if>
<c:if test="${not empty nextPage}">
    <script type="text/javascript">
        $(function () {
            window.location.href='<c:out value="${nextPage}"/>';
        });
    </script>
</c:if>