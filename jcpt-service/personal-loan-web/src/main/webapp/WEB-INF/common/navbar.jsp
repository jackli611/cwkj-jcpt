<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${not empty param.showNav && param.showNav=='true'}">
    <header class="top-nav">
        <a  class="link"  class="link" href="javascript:history.go(-1)"  ><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    </header>
</c:if>