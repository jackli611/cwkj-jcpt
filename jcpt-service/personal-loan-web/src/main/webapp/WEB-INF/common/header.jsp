<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<title>去找钱</title>
<meta name="keywords" content="替您省事，省钱，省时，抵挡高利贷风险" />
<meta name="description" content="替您省事，省钱，省时，抵挡高利贷风险" />
<meta property="og:description" content="替您省事，省钱，省时，抵挡高利贷风险" />
<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link rel="apple-touch-icon" href="/apple-touch-icon-iphone.png"/>
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-ipad.png"/>
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-iphone4.png"/>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="full-screen" content="yes">
<meta name="browsermode" content="application">
<meta name="x5-orientation" content="portrait">
<meta name="x5-fullscreen" content="true">
<meta name="x5-page-mode" content="app">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href='<c:url value="/res/css/bootstrap.min.css"/>' />
<link rel="stylesheet" type="text/css" href='<c:url value="/res/css/animate.min.css"/>' />
<link rel="stylesheet" type="text/css" href='<c:url value="/res/css/font-awesome.min.css"/>' />
<link rel="stylesheet" type="text/css" href='<c:url value="/res/js/common/plugins/modal/modal.css"/>' />
<link rel="stylesheet" type="text/css" href='<c:url value="/res/css/xwckeji.css"/>' />
<script type="text/javascript">
	if(typeof projectPath =="undefined"){
    	projectPath = "<%=request.getContextPath()%>";
    }
</script>
<script src="<c:url value="/res/js/jquery-1.12.4.min.js"/>"></script>
<script src="<c:url value="/res/js/bootstrap-3.3.4.js"/>"></script>
<script src="<c:url value="/res/js/xwckeji.js"/>?v=5566"></script>
<%@ include file="promptMessage.jsp"%>