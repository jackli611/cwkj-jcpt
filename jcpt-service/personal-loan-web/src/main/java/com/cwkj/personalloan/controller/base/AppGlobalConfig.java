package com.cwkj.personalloan.controller.base;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("appGlobalConfig")
public class AppGlobalConfig {
	
	private static String isTest;

	public static String getIsTest() {
		return isTest;
	}

	@Value("${global.isTest}")
	public void setIsTest(String isTest) {
		AppGlobalConfig.isTest = isTest;
	}

}
