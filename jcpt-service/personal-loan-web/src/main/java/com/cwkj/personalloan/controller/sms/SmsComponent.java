package com.cwkj.personalloan.controller.sms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.scf.model.sms.TMessage;
import com.cwkj.scf.service.sms.ISmsService;

/**
 * 	短信工具
 * @author harry
 * @version  v1.0
 */
@Component
public class SmsComponent extends BaseAction {

	@Autowired
	private ISmsService smsService;
	
	public JsonView<String> checkSms(HttpServletRequest request,String phone,String businessType){
		JsonView<String> view= new JsonView<String>();
		try{
			
			String smsCode = request.getParameter("smscode");
			if(StringUtil.isBlank(smsCode)) {
				this.setPromptMessage(view,JsonView.CODE_FAILE, "短信验证码不能为空");
				return view;
			}
			if(StringUtil.isBlank(phone)) {
				phone = request.getParameter("recievers");
			}
			
			if(StringUtil.isBlank(businessType)) {
				businessType = request.getParameter("businessType");
			}
			
			boolean result = smsService.checkSms(phone, businessType, smsCode);
			if(!result) {
				setPromptMessage(view, JsonView.CODE_FAILE, "无效的短信验证码");
			}else {
				view = JsonView.successJsonView();
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
			logger.error("验证短信异常",e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("验证短信异常",e);
		}
		return view;
	}
	

	
}
