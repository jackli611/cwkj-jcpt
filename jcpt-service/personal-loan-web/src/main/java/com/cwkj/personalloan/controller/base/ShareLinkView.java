package com.cwkj.personalloan.controller.base;

import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.scf.model.shares.ShareUserDo;
import com.cwkj.scf.service.shares.ShareUserService;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

/**
 * 链接分享
 */
public class ShareLinkView {

    private static Logger logger= LoggerFactory.getLogger(ShareLinkView.class);
    private static final String SHARELINKKEY="shareid";

    /**
     * 分享链接KEY
     * @return
     */
    public static String getShareLinkKey()
    {
        return SHARELINKKEY;
    }

    /**
     * 保存分享信息到当前用户Session
     * @param request
     * @return
     */
    public static boolean saveShareIdToSession(HttpServletRequest request)
    {
        String fromShareId=request.getParameter(getShareLinkKey());
        //保存上级分享者参数
        if (StringUtil.isNotBlank(fromShareId)) {
            try {
                //校验分享ID是数值类型
                String sessonShareId = Long.valueOf(fromShareId).toString();
                //保存到当前用户Session里
                SecurityUtils.getSubject().getSession().setAttribute(getShareLinkKey(), sessonShareId);
                return true;
            } catch (Exception ex) {
                logger.error("保存分享参数到Sesson异常", ex);
            }
        }
        return false;
    }

    /**
     * 获取Session中分享ID
     * @return
     */
    public static Long getShareIdFromSession()
    {
        Object shareIdObj= SecurityUtils.getSubject().getSession().getAttribute(getShareLinkKey());
        if(shareIdObj!=null)
        {
            try {
                return Long.valueOf(shareIdObj.toString());
            }catch (Exception ex)
            {
                logger.error("获取Session分享ID异常",ex);
            }

        }
        return null;

    }

    public static Long shareIdByUserId(Integer userId)
    {
        ServletContext servletContext = ContextLoader.getCurrentWebApplicationContext().getServletContext();
        WebApplicationContext springContext= WebApplicationContextUtils.getWebApplicationContext(servletContext);
        ShareUserService shareUserService = (ShareUserService) springContext.getBean("shareUserService");
        ShareUserDo shareUserDo= shareUserService.myShareUser(userId);
        if(shareUserDo!=null)
        {
            return shareUserDo.getId();
        }
        return null;
    }

    /**
     * 设置分享ID到视图
     * @param view 要分享的当前视图
     * @param userId 要分享的用户ID
     * @return
     */
    public static boolean setViewShareId(ModelAndView view,  Integer userId)
    {
        if(userId!=null) {
            Long shareId=shareIdByUserId(userId);
            view.addObject(getShareLinkKey(), shareId);
            return true;
        }
        return false;
    }

    /**
     * 创建有分享信息的地址
     * @param url
     * @param userId
     * @return
     */
    public static String builderUrlShareId(String url,  Integer userId)
    {
        String shareKey=getShareLinkKey();

        Long shareId=shareIdByUserId(userId);
        if(shareId!=null) {
            if (url.indexOf("?") > 0) {
                int index=url.indexOf(shareKey+"=");
                if (index > 0) {
                    int len = url.length();
                    int lastIndex = len;
                    for (int i = index; i < len; i++) {
                        char curChar = url.charAt(i);
                        if (curChar == '&') {
                            lastIndex = i;
                            break;
                        }
                    }
                    StringBuffer sb = new StringBuffer();
                    sb.append(url.substring(0, index));
                    sb.append(shareKey).append("=").append(shareId);
                    if(lastIndex!=len) {
                        sb.append(url.substring(lastIndex));
                    }
                    return sb.toString();
                }else{
                    return url+"&"+shareKey + "=" + shareId;
                }

            } else {
                    return url + "?" + shareKey + "=" + shareId;
            }
        }else{
            return url;
        }

    }
}
