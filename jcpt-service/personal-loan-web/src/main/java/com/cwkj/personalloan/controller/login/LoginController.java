package com.cwkj.personalloan.controller.login;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.util.Date;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwkj.personalloan.controller.base.ShareLinkView;
import com.cwkj.scf.model.shares.ShareUserDo;
import com.cwkj.scf.service.shares.ShareUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.IPUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcpt.util.VerifyCodeUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.personalloan.controller.base.ModelAndViewUtil;
import com.cwkj.personalloan.controller.sms.SmsComponent;
import com.cwkj.scf.model.sms.SMSBusinessType;


/**
 * 
 * @author ljc
 *
 */
@Controller
public class LoginController extends BaseAction {

	private static Logger logger=LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private SysUserService sysUserService;

	@Autowired
    private CommonLoginHandler commonLoginHandler;
	
	@Autowired
	private SmsComponent smsComponent;
	@Autowired
	private ShareUserService shareUserService;


	/**
	 * 获取验证码图片和文本(验证码文本会保存在HttpSession中)
	 */
	@RequestMapping("/getVerifyCodeImage.do")
	public void getVerifyCodeImage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//设置页面不缓存
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		String verifyCode = VerifyCodeUtil.generateTextCode(VerifyCodeUtil.TYPE_NUM_ONLY, 4, null);
		//将验证码放到HttpSession里面
//			request.getSession().setAttribute("verifyCode", verifyCode);
		SecurityUtils.getSubject().getSession().setAttribute("verifyCode", verifyCode);
		logger.info("本次生成的验证码为[" + verifyCode + "],已存放到HttpSession中");
		//设置输出的内容的类型为JPEG图像
		response.setContentType("image/jpeg");
		BufferedImage bufferedImage = VerifyCodeUtil.generateImageCode(verifyCode, 90, 30, 3, true, Color.getHSBColor(180f,0.01f,0.91f), Color.BLACK, null);
		//写给浏览器
		ImageIO.write(bufferedImage, "JPEG", response.getOutputStream());
	}

	@RequestMapping(value="/login.do", method=RequestMethod.GET)
	public ModelAndView loginInit(HttpServletRequest request){
		ModelAndView model = new ModelAndView("model/login");
		try {
			Subject currentUser = SecurityUtils.getSubject();

			if (currentUser !=null && currentUser.isAuthenticated()) {

				model = new ModelAndView("redirect:index.do");
				return model;
			}
		}catch (Exception ex)
		{
			logger.error("登录异常",ex);
		}
		return model;
	}

	@RequestMapping(value="/wxbindlogin.do", method=RequestMethod.GET)
	public ModelAndView wxlogin(HttpServletRequest request){
		ModelAndView model = new ModelAndView("weixin/wxbind");
		try {
			Subject currentUser = SecurityUtils.getSubject();

			if (currentUser !=null && currentUser.isAuthenticated()) {

				model = new ModelAndView("redirect:index.do");
				return model;
			}
		}catch (Exception ex)
		{
			logger.error("登录异常",ex);
		}
		return model;
	}



	/**
	 * 用户登录
	 */
	@RequestMapping(value="/login.do", method=RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request ,String username, String password){
		ModelAndView view = new ModelAndView("model/login");
		try {
			PlatformLoginProxy lp = new PlatformLoginProxy(commonLoginHandler);
			LoginForm loginForm = new LoginForm();
			loginForm.setReqData(request.getParameterMap());
			loginForm.setLoginIp(IPUtils.getIP(request));
			loginForm.setUsername(username);
			loginForm.setPwd(password);
			view= lp.login(request,loginForm );

			//设置我的分享地址参数
			SysUserDo currentUser = currentUser();
			if (currentUser !=null ) {
 				ShareLinkView.setViewShareId(view,currentUser.getId());
			}

		}catch (BusinessException e){
	            logger.error("登录校验失败",e.getMessage());
	            ModelAndViewUtil.setPromptException(view,e);
        }catch (Exception ex){
            logger.error("登录异常",ex);
            ModelAndViewUtil.setPromptMessage(view,PROMPTCODE_FAIL,"error:10000,请求异常");
        }
		return view;
	
	}


	/**
	 * 用户登出
	 */
	@RequestMapping("/logout.do")
	public String logout(HttpServletRequest request){
		SecurityUtils.getSubject().logout();
		return InternalResourceViewResolver.REDIRECT_URL_PREFIX + "login.do";
	}



	@RequestMapping(value="/checkRegTelphone.do", method=RequestMethod.POST)
	public JsonView<String> checkTelephone_MethodGET(HttpServletRequest request,String userName,Integer checkStatus)
	{
		JsonView<String>  view = new JsonView<String>();
		try {

			AssertUtils.isNotBlank(userName, "手机号不能为空");
			if (!Pattern.compile(SystemConst.REGEX_TELEPHONE).matcher(userName).matches()) {
				throw new BusinessException("手机号码无效");
			}
			if(checkStatus==null || checkStatus.intValue() != 2) {
				SysUserDo dbUser = sysUserService.findSysUserByUserName(userName);
				if (checkStatus != null && checkStatus.intValue() == 1) {
					AssertUtils.notNull(dbUser, "手机号未注册");
				} else {
					AssertUtils.isNull(dbUser, "手机号已注册");
				}
			}

			setPromptMessage(view, PROMPTCODE_OK, "手机号可以注册");

		}catch (BusinessException ex)
		{
			logger.error("注册手机号校验失败:{}",ex);
			setPromptException(view,ex);
			return  view;
		}catch (Exception ex)
		{
			logger.error("校验手机号异常",ex);
			setPromptException(view,ex);
		}
		return view;
	}

	@RequestMapping(value="/checkPassword.do", method=RequestMethod.POST)
	public JsonView<String> checkPassword_MethodGET(HttpServletRequest request,String userName,String password,Integer checkStatus)
	{
		JsonView<String>  view = new JsonView<String>();
		try {

			AssertUtils.isNotBlank(userName, "手机号不能为空");
			AssertUtils.isNotBlank(password, "密码不能为空");
			if (!Pattern.compile(SystemConst.REGEX_TELEPHONE).matcher(userName).matches()) {
				throw new BusinessException("手机号码无效");
			}

			if (!Pattern.compile(SystemConst.REGEX_PWD2).matcher(password).matches()) {
				throw new BusinessException("密码不少于6位");
			}

			SysUserDo dbUser = sysUserService.findSysUserByUserName(userName);
			if(checkStatus!=null && checkStatus.intValue()==1)
			{
				AssertUtils.notNull(dbUser,"手机号未注册");
			}else {
				AssertUtils.isNull(dbUser, "手机号已注册");
			}

			setPromptMessage(view, PROMPTCODE_OK, "手机号可以注册");

		}catch (BusinessException ex)
		{
			logger.error("注册手机号校验失败:{}",ex);
			setPromptException(view,ex);
			return  view;
		}catch (Exception ex)
		{
			logger.error("校验手机号异常",ex);
			setPromptException(view,ex);
		}
		return view;
	}

	@RequestMapping(value="/resetPassword.do", method=RequestMethod.GET)
	public ModelAndView resetPasswordPage()
	{
		ModelAndView model = new ModelAndView("model/resetPassword");
		return model;
	}

	@RequestMapping(value="/resetPassword.do", method=RequestMethod.POST)
	public ModelAndView resetPassword(HttpServletRequest request, String userName,String password,String password2)
	{
		ModelAndView model = new ModelAndView("model/resetPassword");
		try{
			AssertUtils.isNotBlank(userName, "手机号不能为空");
			AssertUtils.isNotBlank(password, "新密码不能为空");
			if(!password.equals(password2))
			{
				throw new BusinessException("密码不一致");
			}

			JsonView v = smsComponent.checkSms(request,userName, SMSBusinessType.resetPsw.name());
			if (JsonView.CODE_SUCCESS.intValue() != v.getCode().intValue()) {
				this.setPromptMessage(model, "无效的短信验证码");
				return model;
			}
			SysUserDo dbUser=sysUserService.findSysUserByUserName(userName);
			if(dbUser!=null)
			{
					String newPsw=DigestUtils.md5Hex(password+SystemConst.PASS_KEY);
				Integer dbRes=	sysUserService.resetPassword(dbUser.getId(), newPsw, new Date());
				AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"密码重置失败");
				setPromptMessage(model,PROMPTCODE_OK,"密码重置成功");

			}else{
				throw new BusinessException("手机号未注册");
			}

		}catch (BusinessException e) {
			setPromptException(model, e);
		}catch (Exception e) {
			logger.error("修改密码异常", e);
			setPromptException(model, e);
		}
		return model;
	}




	@RequestMapping(value="/register.do", method=RequestMethod.GET)
	public ModelAndView register_methodGet()
	{
		ModelAndView model = new ModelAndView("model/register");
		return model;
	}

	@RequestMapping(value="/register.do", method=RequestMethod.POST)
	public ModelAndView register_methodPost(HttpServletRequest request,SysUserDo user)
	{
		ModelAndView view = new ModelAndView("model/register");
		try {
			user.setLocked(1);//未锁定
			if (StringUtil.isBlank(user.getUserName())) {
				this.setPromptMessage(view, "用户名不能为空");
				return view;
			}
			if (StringUtil.isBlank(user.getPassword())) {
				this.setPromptMessage(view, "密码不能为空");
				return view;
			}

			if (!Pattern.compile(SystemConst.REGEX_TELEPHONE).matcher(user.getUserName()).matches()) {
				throw new BusinessException("手机号码无效");
			}
			if (!Pattern.compile(SystemConst.REGEX_PWD2).matcher(user.getPassword()).matches()) {
				throw new BusinessException("密码不少于6位");
			}

			SysUserDo dbUser = sysUserService.findSysUserByUserName(user.getUserName());
			if (dbUser != null) {
				this.setPromptMessage(view, "手机号已注册");
				return view;
			}

			JsonView v = smsComponent.checkSms(request, user.getUserName(), SMSBusinessType.register.name());
			if (JsonView.CODE_SUCCESS.intValue() != v.getCode().intValue()) {
				this.setPromptMessage(view, "无效的短信验证码");
				return view;
			}
			user.setTelphone(user.getUserName());
			String myPassword=user.getPassword();
			String psw = DigestUtils.md5Hex( myPassword+ SystemConst.PASS_KEY);
			user.setPassword(psw);
			user.setRegisterSource(getCurrentChannel(request).getChannelCode());

			Integer dbRes = sysUserService.insertSysUser(user);
			if (dbRes.intValue() > 0) {
				dbRes=sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_PERSONALLOAN}, user.getId());
				//设置推荐分享信息
				Long shareId= ShareLinkView.getShareIdFromSession();
				if(shareId!=null) {
					ShareUserDo shareUserDo =shareUserService.findShareUserById(shareId);
					if(shareUserDo!=null) {
						ShareUserDo newShareUser=new ShareUserDo();
						newShareUser.setUserId(user.getId());
						newShareUser.setTelphone(user.getUserName());
						newShareUser.setFromUserId(shareUserDo.getUserId());
						newShareUser.setFromShareId(shareUserDo.getId());
						newShareUser.setFromTelphone(shareUserDo.getTelphone());
						newShareUser.setTopShareUserId(shareUserDo.getFromUserId());
						newShareUser.setTopShareTelphone(shareUserDo.getFromTelphone());
						newShareUser.setPartnerStatus(ShareUserDo.PARTNERSTATUS_DEFAULT);
						newShareUser.setOpenPlatform(getCurrentChannel(request).getChannelCode());

						shareUserService.insertShareUser(newShareUser);
					}
				}

				 //注册成功跳到登录页面
				return login(request,user.getUserName(),myPassword);
			} else {
				this.setPromptMessage(view, "注册失败");
			}

		}catch (BusinessException ex)
		{
			logger.info("注册个贷账号失败：{}",ex);
			setPromptException(view,ex);
			return view;
		}catch (Exception ex)
		{
			logger.error("注册个贷账号异常",ex);
			setPromptException(view,ex);
		}
		return view;
	}

	public  String getIP(HttpServletRequest request) {
		String ip =request.getHeader("x-forwarded-for");
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip =request.getHeader("Proxy-Client-IP");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Real-IP");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		// 多个路由时，取第一个非unknown的ip
		final String[] arr = ip.split(",");
		for (final String str : arr) {
			if (!"unknown".equalsIgnoreCase(str))
			{
				ip = str;
				break;
			}
		}
		return ip;
	}
	
	

}
