package com.cwkj.personalloan.interceptor;


import com.cwkj.jcpt.util.IPUtils;
import com.cwkj.jcptsystem.model.system.SysOperateLogDo;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysOperateLogService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * 日志拦截器
 */
public class LogInterceptor extends HandlerInterceptorAdapter{  
	
    private final Logger logger = LoggerFactory.getLogger(LogInterceptor.class);

    @Autowired
    private SysOperateLogService sysOperateLogService;
    
    //以下url不需要记录
    private String [] urls = {"/platform/main.do","/system/operateLog/list.do",
            "/system/operateLog/queryOperateLogList.do","/platform/getVerifyCodeImage.do",
            "/platform/checkLoginedUser.do","/platform/resetPwd.do",
            "/platform/getSmsCode.do","/trade/dealHistoryLog.do"};
    
	/**  
     * 在业务处理器处理请求之前被调用  
     * 如果返回false  
     *     从当前的拦截器往回执行所有拦截器的afterCompletion(),再退出拦截器链 
     * 如果返回true  
     *    执行下一个拦截器,直到所有的拦截器都执行完毕  
     *    再执行被拦截的Controller  
     *    然后进入拦截器链,  
     *    从最后一个拦截器往回执行所有的postHandle()  
     *    接着再从最后一个拦截器往回执行所有的afterCompletion()  
     */    
	@Override    
    public boolean preHandle(HttpServletRequest request,    
            HttpServletResponse response, Object handler) throws Exception {    
		logger.debug("==============LogInterceptor.preHandle================");  
		return true;
		
    }    
    
    /** 
     * 在业务处理器处理请求执行完成后,生成视图之前执行的动作    
     * 可在modelAndView中加入数据，比如当前时间 
     */  
    @Override    
    public void postHandle(HttpServletRequest request,    
            HttpServletResponse response, Object handler,    
            ModelAndView modelAndView) throws Exception {     
        logger.debug("==============LogInterceptor.postHandle================");
    }    
    
    /**  
     * 在DispatcherServlet完全处理完请求后被调用,可用于清理资源等   
     * 当有拦截器抛出异常时,会从当前拦截器往回执行所有的拦截器的afterCompletion()  
     */    
    @Override    
    public void afterCompletion(HttpServletRequest request,    
            HttpServletResponse response, Object handler, Exception ex)    
            throws Exception {    
        logger.debug("==============LogInterceptor.afterCompletion================");    
        String requestUrl = request.getRequestURI();  //请求路径
        String url = requestUrl.substring(request.getContextPath().length(),requestUrl.length());
        try {
			
			//以下url不需要记录
			List<String> list = Arrays.asList(urls);
			if(list.contains(url)){
				logger.info("The url["+url+"] is doesn't interceptor...");
				return;
			}
			
			Subject currentUser = SecurityUtils.getSubject();
			if(null != currentUser){
				SysUserDo user = currentUser.getPrincipal() == null ? null : (SysUserDo) currentUser.getPrincipal();
				logger.debug("user:"+user);
				if(null != user){
					SysOperateLogDo sysOperateLogDo = new SysOperateLogDo();
					sysOperateLogDo.setUserId(user.getId());
					sysOperateLogDo.setUrl(url);
					sysOperateLogDo.setIp(IPUtils.getIP(request));
					sysOperateLogDo.setUserAgent(request.getHeader("User-Agent"));
					sysOperateLogDo.setStatus(1);
					sysOperateLogService.insertSysOperateLog(sysOperateLogDo);
				}else{
					logger.warn("用户没登录..."+requestUrl);
				}
			}
        	
		} catch (Exception e) {
			logger.error("日志拦截["+requestUrl+"]error...", e);
		}
    }    
}    