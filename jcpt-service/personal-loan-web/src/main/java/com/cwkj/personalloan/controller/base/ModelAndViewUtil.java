package com.cwkj.personalloan.controller.base;

import org.springframework.dao.DataAccessException;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BusinessException;

public class ModelAndViewUtil {
	
	/**
	 * 设置提示业务异常信息
	 * @param view
	 * @param e
	 */
	public  static void  setPromptException(ModelAndView view,BusinessException e){
		view.addObject("error", e);
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	public  static  <T>  void  setPromptException(JsonView<T> view,BusinessException e){
		view.setCode(JsonView.CODE_FAILE);
		view.setMsg(e.getMessage());
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	public  static void  setPromptException(ModelAndView view,Exception e)
	{
		if(e instanceof DataAccessException)
		{
			view.addObject("error", new BusinessException(BusinessException.code_other, "Fail [Error:1001]"));
		}else {
			view.addObject("error", new BusinessException(BusinessException.code_other, e.getMessage()));
		}
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	public static  <T>  void  setPromptException(JsonView<T> view,Exception e)
	{

		if(e instanceof DataAccessException)
		{
			view.setCode(JsonView.CODE_FAILE);
			view.setMsg("Fail [Error:1001]");
		}else {
			view.setCode(JsonView.CODE_FAILE);
			view.setMsg(e.getMessage());
		}
	}
	
	/**
	 * 设置提示信息
	 * @param view
	 * @param msg
	 */
	public  static void  setPromptMessage(ModelAndView view,String msg)
	{
		view.addObject("promptMsg", msg);
	}

	/**
	 * 设置提示信息
	 * @param view
	 * @param msg 信息
	 * @param code 结果编号
	 */
	public  static void  setPromptMessage(ModelAndView view,Integer code,String msg)
	{
		view.addObject("promptMsg", msg);
		view.addObject("promptCode", code);
	}

	/**
	 * 设置提示信息
	 * @param view
	 * @param code
	 * @param msg
	 */
	public static  <T>  void  setPromptMessage(JsonView<T> view,Integer code, String msg)
	{
		view.setCode(code);
		view.setMsg(msg);
	}
	

}
