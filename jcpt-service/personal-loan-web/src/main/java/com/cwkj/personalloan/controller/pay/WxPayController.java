package com.cwkj.personalloan.controller.pay;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BigDecimalUtil;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.IPUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcpt.util.TokenUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.personalloan.controller.base.AppGlobalConfig;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.PayInfoBean;
import com.cwkj.scf.model.merchant.MerchantAccountDo;
import com.cwkj.scf.service.loan.ILoanOrderService;
import com.cwkj.scf.service.merchant.MerchantAccountService;
import com.cwkj.scf.service.pay.wx.WxDeductForm;

@RestController
@RequestMapping("/wxpay")
public class WxPayController extends BaseAction {
	
    private final static Logger logger = Logger.getLogger(WxPayController.class);

    @Autowired
    private ILoanOrderService orderService;
    
    @Autowired
    private PayCommponent payCommponent;
    
    @Autowired
    private MerchantAccountService merchantAccountService;

    

    

    /**
     *	支付之前准备支付数据，微信支付采用h5，先获取 js签名
     * @return
     */
    @RequestMapping(value = "/prePayOrder", method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView prePay(HttpServletRequest request) {
    	ModelAndView mv = new ModelAndView("/pay/wxpay");
    	try {
    		
    		//查询订单和当前用户
    		SysUserDo currentUser = this.currentUser();
    		String loanId = request.getParameter("loanid");
    		Loanorder order = orderService.selectByPrimaryKey(Long.valueOf(loanId));
    		PayInfoBean payInfoBean = order.calPayAmt();
    		
    		
    		
    		WxDeductForm deductForm = new WxDeductForm();
    		deductForm.setAMOUNT(payInfoBean.getWxPayAmt());
    		deductForm.setUserId(currentUser.getId().toString());
    		if("T".equalsIgnoreCase(AppGlobalConfig.getIsTest())) {
    			deductForm.setAMOUNT("1");
    		}
    		if("PAY002".equals(payInfoBean.getPayStepCode())) {
    			String payAmt = request.getParameter("payAmt");
        		if(StringUtil.isNotBlank(payAmt)) {
        			deductForm.setAMOUNT(payAmt);
        		}
    		}
    		
    		
    		deductForm.setPayFeeCode(payInfoBean.getPayStepCode());
    		deductForm.setPayFeeName(payInfoBean.getPayName());
    		deductForm.setOrderNo(order.getOrdercode());
    		//登录时的openId
    		String openId = (String)request.getSession().getAttribute("MySessionOpenId");
    		if(StringUtil.isBlank(openId)) {
    			logger.error("wx pay not get openId from session");
    			MerchantAccountDo  wxAccount = merchantAccountService.findWxAccountByUserId(currentUser.getId());
    			if(wxAccount != null) {
	    			openId = wxAccount.getAcctid();
    			}
    		}
    		if(StringUtil.isBlank(openId)) {
    			throw new BusinessException("不能获取openId");
    		}
    		
    		deductForm.setOpenId(openId);
    		
    		//支付页面url
    		String signUrl = request.getParameter("signUrl");
    		signUrl=URLDecoder.decode(signUrl, "utf8");
    		deductForm.setSignUrl(signUrl);
    		//获取微信授权code
    		String code = request.getParameter("code");
    		deductForm.setWxAuthCode(code);
    		deductForm.setUSR_IP(IPUtils.getIP(request));
    		
    		String channel = this.getCurrentChannel(request).getChannelCode();
    		deductForm.setChannelCode(channel);
    		
			BaseResult<Map> ret = payCommponent.prePay(deductForm );    		
    		logger.info("paycommponent wx prePay return:"+ret.getData());
			mv.addAllObjects(ret.getData());
			if(!ret.resultSuccess()) {
				return mv.addObject("error",ret.getMsg());
			}
			
    	} catch (Exception e) {
            logger.error("微信预支付订单生成异常：", e);
            return mv.addObject("error",e.getMessage());
        }
        return mv;
	}

    private boolean checkToken(HttpServletRequest request, ModelAndView mv) {
		boolean isValid = TokenUtil.checkToken(request, "pay");
		if(isValid == false) {
			this.setPromptMessage(mv, "你已提交支付请求，在处理中");
			return false;
		}
		String newToken = TokenUtil.putToken(request, "pay");
		mv.addObject("token", newToken);
		return true;
	}
    
   
}
