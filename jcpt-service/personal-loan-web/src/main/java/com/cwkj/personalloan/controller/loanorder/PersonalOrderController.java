package com.cwkj.personalloan.controller.loanorder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.scf.model.loan.LoanOrderStatus;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.LoanorderExample;
import com.cwkj.scf.model.loan.PayInfoBean;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.service.loan.ILoanOrderService;

/**
 * 借款订单申请
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/loanorder/")
public class PersonalOrderController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(PersonalOrderController.class);

	@Autowired
	private ILoanOrderService loanOrderSercice;
	
	/**
	 * 提交保存订单数据验证
	 * @param order
	 */
	private void valid(Loanorder order) {
		if( 1 == order.getLoantype().intValue()) {
			AssertUtils.isTrue(order.getApplyamount() != null, "请填写借款金额");
			AssertUtils.isTrue(order.getLoanperiod() != null, "请填写借多久");
			AssertUtils.isTrue(order.getRepaytype() != null, "请填写怎么还");
		}if( 2 == order.getLoantype().intValue() || 5 == order.getLoantype().intValue() )  {
			AssertUtils.isTrue(order.getApplyamount() != null, "请填写借款金额");
			AssertUtils.isTrue(order.getLoanperiod() != null, "请填写借多久");
			AssertUtils.isTrue(order.getRepaytype() != null, "请填写怎么还");
			AssertUtils.isTrue(order.getPerson().getWherehouse() != null, "请填写被抵押红本房在哪里");
			AssertUtils.isTrue(order.getPerson().getRedbooktype() != null, "请填写红本房类型");
			AssertUtils.isTrue(order.getPerson().getHouseprice() != null, "请填写被抵押房子价格");
		}if( 3 == order.getLoantype().intValue() || 4  == order.getLoantype().intValue() ) {
			AssertUtils.isTrue(order.getApplyamount() != null, "请填写借款金额");
			AssertUtils.isTrue(order.getLoanperiod() != null, "请填写借多久");
		}
	}
	

	/**
	 * 	查询订单用来编辑
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getApplyStepPage.do")
	public ModelAndView getApplyStepPage(HttpServletRequest request,Long loanid){
		
		String viewName = request.getParameter("step");
		String loanType = request.getParameter("loanType");
		if(StringUtil.isBlank(loanType)) {
			loanType = "1";
		}
		if(StringUtil.isBlank(viewName)) {
			viewName = "applyStep1";
		}
		ModelAndView view=  new ModelAndView("order/"+viewName);

		try{
			view.addObject("loanType",loanType);
			view.addObject("loanid",loanid);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	

	/**
	 * 	订单详情
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/viewOrder.do")
	public ModelAndView viewOrder(HttpServletRequest request,Long loanid)
	{
		ModelAndView view=  new ModelAndView("order/orderDetail");
		try{
			SysUserDo currentUser  = this.currentUser();
			if(currentUser == null ) {
				return view;
			}
			long userId = currentUser.getId(); 
			if(loanid != null) {
				LoanorderExample example  = new LoanorderExample();
				example.createCriteria().andLoanidEqualTo(loanid).andUseridEqualTo(userId ).andProdcodeEqualTo(ProductEnum.PROD_PERSONAL_XY.getProductCode());
				List<Loanorder> orderLst = loanOrderSercice.selectByExample(example);
				if(orderLst.size()>0) {
					view.addObject("order", orderLst.get(0));
				}
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}

	/**
	 * 	查询订单用来编辑
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getOrder.do")
	public JsonView<Loanorder> getOrder(HttpServletRequest request,Long loanid)
	{
		JsonView<Loanorder> view=  JsonView.successJsonView();
		try{

			SysUserDo currentUser  = this.currentUser();

			if(loanid != null) {
				Loanorder order = loanOrderSercice.selectOrderAndAttachmentByPrimaryKey(loanid );
				if(null != order) {
					//判断是否是他的数据
					if(order.getUserid().intValue() == currentUser.getId().intValue()) {
						throw new RuntimeException("没有权限查看此订单");
					 }
					view.setData(order);
				}
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}



	/**
	 * 	保存订单
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/saveOrder.do")
	public ModelAndView saveOrder(HttpServletRequest request,Loanorder order)
	{
		int loanType=order.getLoantype()==null?1:order.getLoantype().intValue();
		ModelAndView view=new ModelAndView("order/applyStep1");
		view.addObject("order", order);
		view.addObject("loanType", loanType);
		try{
			SysUserDo currentUser  = this.currentUser();
			AssertUtils.isTrue(currentUser != null, "请登录再操作");
			LoanPerson person = buildPersonInfo(request);
			order.setPerson(person);
			valid(order);
			
			order.setUserid(Long.valueOf(currentUser.getId()));
			if(StringUtil.isBlank(order.getProdcode())){
				order.setProdcode(ProductEnum.PROD_PERSONAL_XY.getProductCode());
			}

			//对公借款
			order.setOrdertype(Loanorder.privateOrder);
			//根據登錄入口設置核心企業
			order.setPropertyid(this.getCurrentChannel(request).getChannelCode());
			
			//修改后重新编辑
			if(LoanOrderStatus.NOPASS.getStatusCode().equals(order.getLoanstatus())) {
				order.setLoanstatus(LoanOrderStatus.PENDING.getStatusCode());
			}

			
			Integer result= loanOrderSercice.updateOrder(order);
			
			view.addObject("person", person);

			//根据个贷类型分离视图
			switch (loanType)
			{
				case 4:{//企业信用贷
					view.setViewName("order/applyStep2_loantype4");
					break;
				}
				default:{
					view.setViewName("order/applyStep2");
					break;
				}
			}
			
			AssertUtils.isTrue(result.equals(1),"保存出错");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("保存订单申请第一步异常",e);
		}
		return view;
	}
	



	private LoanPerson buildPersonInfo(HttpServletRequest request) {
		LoanPerson person = new LoanPerson();
		String whereHouse = request.getParameter("wherehouse");
		if(StringUtil.isNotBlank(whereHouse)) {
			person.setWherehouse(whereHouse);
		}
		String redbooktype = request.getParameter("redbooktype");
		if(StringUtil.isNotBlank(redbooktype)) {
			person.setRedbooktype(redbooktype);
		}
		String houseprice = request.getParameter("houseprice");
		if(StringUtil.isNotBlank(houseprice)) {
			person.setHouseprice(new BigDecimal(houseprice));
		}
		String whereredbook = request.getParameter("whereredbook");
		if(StringUtil.isNotBlank(whereredbook)) {
			person.setWhereredbook(whereredbook);
		}
		String deducthouse = request.getParameter("deducthouse");
		if(StringUtil.isNotBlank(deducthouse)) {
			person.setDeducthouse(deducthouse);
		}
		return person;
	    
	}


	/**
	 * 	查看借款协议模板
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/showProtocolTemplate.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView showProtocolTemplate(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("order/protocol_template");
		try{

		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取查看借款协议模板异常", e);
		}
		return view;
	}

	/**
	 * 借款订单数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/orderList.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView orderList(HttpServletRequest request,Long pageIndex,Integer pageSize)
	{
		ModelAndView view=new ModelAndView("order/orderList_table");
		try{
			
			Map<String, Object> selectItem=new HashMap<String, Object>();

			 SysUserDo currentUser  = this.currentUser();
			 if(currentUser == null) {
				 return view;
			 }
			 
			 selectItem.put("userid", currentUser.getId());
			 String loanStatus = request.getParameter("queryLoanstatus");
			 if(StringUtil.isNotBlank(loanStatus)) {
				 selectItem.put("loanstatus", loanStatus);
			 }
			 selectItem.put("prodcode", ProductEnum.PROD_PERSONAL_XY.getProductCode());

			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?200:pageSize;
			 PageDo<Loanorder> pagedata= loanOrderSercice.queryOrderListPage(pageIndex,pageSize, selectItem);
			 view.addObject("pagedata",pagedata);

		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取借款订单数据分页异常", e);
		}
		return view;
	}


}
