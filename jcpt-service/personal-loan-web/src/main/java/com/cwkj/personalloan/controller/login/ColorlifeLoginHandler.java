package com.cwkj.personalloan.controller.login;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.security.SignatureException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.personalloan.controller.base.ShareLinkView;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.base.Channel;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.RSASignatureUtils;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.personalloan.util.SortMapUtil;
import com.cwkj.scf.model.merchant.MerchantAccountDo;
import com.cwkj.scf.model.merchant.MerchantApiRequestDo;
import com.cwkj.scf.model.merchant.MerchantProfileDo;
import com.cwkj.scf.service.merchant.MerchantAccountService;
import com.cwkj.scf.service.merchant.MerchantApiRequestService;
import com.cwkj.scf.service.merchant.MerchantProfileService;

@Component("colorLoingHandler")
public class ColorlifeLoginHandler implements InvocationHandler {

	protected int PROMPTCODE_OK=1;
	protected int PROMPTCODE_FAIL=0;
	
	private static Logger logger= LoggerFactory.getLogger(ColorlifeLoginHandler.class);
	private IPlatformLogin  loginImpl = new CommonLogin();
	
	

    @Autowired
    private MerchantProfileService merchantProfileService;
    @Autowired
    private MerchantApiRequestService merchantApiRequestService;
    @Autowired
    private MerchantAccountService merchantAccountService;

    @Autowired
    private SysUserService sysUserService;
    
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		//System.out.println("color life login");
		LoginForm loginForm  = (LoginForm) args[1];
		HttpServletRequest request  = (HttpServletRequest) args[0];
		colorLogin(request,loginForm);
		Object result =  method.invoke(loginImpl, args);
		return result;
	}
	
	private void buildChannel(HttpServletRequest request,MerchantProfileDo merchant) {
		Channel cnl = new Channel();
		cnl.setChannelCode(merchant.getPropertyId());
		cnl.setLoginSource("彩之云app");
		request.getSession().setAttribute("CurrentChannel", cnl);
	}
	
    private void colorLogin(HttpServletRequest request,LoginForm loginForm){
    	
//    	Map<String,Object> reqData = loginForm.getReqData();
    	logger.info("彩生活请求参数："+ JSON.toJSONString(request.getParameterMap()));
        Enumeration<String> keyNames = request.getParameterNames();
        Map<String, String> mapList = new HashMap<String, String>();
//        while (keyNames.hasMoreElements()) {
//            String attrName = keyNames.nextElement();
//            String attrValue = request.getParameter(attrName);
//            if (StringUtils.isNotEmpty(attrValue)) {
//                mapList.put(attrName, attrValue);
//            }
//        }
        String merId =request.getParameter("merId");// mapList.get("merId");
        String secretKey =request.getParameter("secretKey");// mapList.get("secretKey");
        String signedMsg =request.getParameter("signedMsg");// mapList.get("signedMsg");
        String trxId=request.getParameter("trxId");// mapList.get("trxId");
        String acctid=request.getParameter("acctid");//mapList.get("acctid");
        String mobile=request.getParameter("mobile");//mapList.get("mobile");
        String community=request.getParameter("community");//mapList.get("community");
        String community_name=request.getParameter("community_name");//mapList.get("community_name");
        mapList.put("merId",merId);
        mapList.put("secretKey",secretKey);
        mapList.put("trxId",trxId);
        mapList.put("acctid",acctid);
        mapList.put("mobile",mobile);
        mapList.put("community",community);
        mapList.put("community_name",community_name);

        AssertUtils.notEmptyStr(trxId,"error:10001,请求校验不通过");
        AssertUtils.isNotBlank(merId,"error:10002,请求校验不通过");
        AssertUtils.isNotBlank(secretKey,"error:10003,请求校验不通过");
        AssertUtils.notEmptyStr(signedMsg,"error:10004,请求校验不通过");
        AssertUtils.notEmptyStr(acctid,"error:10005,请求校验不通过");
        AssertUtils.notEmptyStr(mobile,"error:10006,请求校验不通过");
        MerchantProfileDo merProfile= merchantProfileService.enableMerchantProfileByMerId(merId,secretKey);
        AssertUtils.notNull(merProfile,"error:20001,请求校验不通过");


        Map<String, String> resultMap = SortMapUtil.sortMapByKey(mapList);
        StringBuffer sb = new StringBuffer();
        for (Map.Entry<String, String> entry : resultMap.entrySet()) {
            if(!"signedMsg".equals(entry.getKey())) {
                sb.append(entry.getKey());
                if (entry.getValue() != null) {
                    sb.append(entry.getValue());
                }
            }
        }
        String bodyStr = sb.toString();
        logger.info("签名信息:signedMsg={},body={}", signedMsg, bodyStr);
        boolean verifyResult =false;
        try {
             verifyResult = RSASignatureUtils.verify(bodyStr, signedMsg, RSASignatureUtils.getPublicKeyFromPemStr(merProfile.getPemKey()), RSASignatureUtils.ALGORITHM_SHA1);
        }catch (Exception ex)
        {
            if(ex instanceof SignatureException ) {
                AssertUtils.isTrue(false, "error:20003,签名值无效");
            }
            else
            {
                AssertUtils.isTrue(false, "error:20004,签名校验失败");
            }
        }
        AssertUtils.isTrue(verifyResult, "error:20002,请求校验不通过");
        Date now=new Date();
        MerchantApiRequestDo apiRequestDo=new MerchantApiRequestDo();
        apiRequestDo.setMerId(merId);
        apiRequestDo.setSecretKey(secretKey);
        apiRequestDo.setRequestBody(JSONObject.toJSONString(mapList));
        apiRequestDo.setTrxId(trxId);
        apiRequestDo.setCreateTime(now);
        apiRequestDo.setUpdateTime(now);
        MerchantApiRequestDo dbApiReq=merchantApiRequestService.findMerchantApiRequestByTrxId(trxId);
        AssertUtils.isNull(dbApiReq,"error:10007,请求流水号无效");
        Integer dbRes= merchantApiRequestService.insertMerchantApiRequest(apiRequestDo);
        AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"error:10007,请求记录失败");

        //校验用户是否存在，创建用户并保存为登录状态
        MerchantAccountDo merchantAccountDo=new MerchantAccountDo();
        merchantAccountDo.setAcctid(acctid);
        merchantAccountDo.setCommunity(community);
        merchantAccountDo.setMerId(merId);
        merchantAccountDo.setMobile(mobile);
        Long shareId= ShareLinkView.getShareIdFromSession();//分享ID
        MerchantAccountDo accountDo= merchantAccountService.checkMerchantAccount(merchantAccountDo,merProfile,shareId);
        AssertUtils.notNull(accountDo,"error:10008,账号校验不通过");
        SysUserDo sysUserDo= sysUserService.findSysUserById(accountDo.getUserId());
        AssertUtils.notNull(sysUserDo,"error:10009,账号校验不通过");

        //設置當前登錄渠道
        buildChannel(request, merProfile);
        
        //保存用户登录状态
        Subject currentUser = SecurityUtils.getSubject();
        if(currentUser.isAuthenticated())
        {
            SecurityUtils.getSubject().logout();
        }

        loginForm.setPwd(accountDo.getId());
        loginForm.setUsername(sysUserDo.getUserName());

    }
	

}
