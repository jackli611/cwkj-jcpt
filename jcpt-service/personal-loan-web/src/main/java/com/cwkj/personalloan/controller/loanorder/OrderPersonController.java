package com.cwkj.personalloan.controller.loanorder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.LoanPersonExample;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.LoanorderExample;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.service.loan.ILoanOrderService;

/**
 * 借款订单申请： 订单个人信息维护
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/loanorder/")
public class OrderPersonController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(OrderPersonController.class);

	@Autowired
	private ILoanOrderService loanOrderSercice;
	
	/**
	 * 验证数据
	 * @param loanPerson
	 * @return
	 */
	private boolean valid(LoanPerson loanPerson) {
		//AssertUtils.isNotBlank(loanPerson.getRealname(),"请填写姓名");
		//AssertUtils.isNotBlank(loanPerson.getIdno(),"请填写身份证");
		AssertUtils.isNotBlank(loanPerson.getCommunity(),"请填写小区");
		AssertUtils.isNotBlank(loanPerson.getInsurance(),"请填写是否有社保");
		AssertUtils.isNotBlank(loanPerson.getProvidentfund(),"请填写是否有公积金");
		AssertUtils.isNotBlank(loanPerson.getJob(),"请填写职业");
		return false;
	}
	
	
	/**
	 * 	保存订单个人信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/savePerson.do")
	public ModelAndView savePerson(HttpServletRequest request,LoanPerson loanPerson,Integer loanType)
	{
		ModelAndView view=new ModelAndView("order/applyStep2");
		view.addObject("person", loanPerson);
		try{
			if(loanType!=null && loanType.intValue()==4)//企业信用贷
			{
				view=new ModelAndView("order/applyStep2_loantype4");
				AssertUtils.isNotBlank(loanPerson.getOverdueTime(),"请选择企业在人民银行的逾期次数");
				AssertUtils.isNotBlank(loanPerson.getShareholderOverdueTime(),"请选择企业在人民银行的逾期次数");
				AssertUtils.isNotNull(loanPerson.getIncomeAmt(),"请填写最近一年收入");
				AssertUtils.isNotNull(loanPerson.getDebtAmt(),"请填写当前负债总额");
				AssertUtils.isNotNull(loanPerson.getTaxesAmt(),"请填写最近一年纳税");
				AssertUtils.isNotNull(loanPerson.getIncomeTaxAmt(),"请填写最近一年企业所得税");
			}else{
				valid(loanPerson);
			}
			
			SysUserDo currentUser  = this.currentUser();
			loanPerson.setUserid(Long.valueOf(currentUser.getId()));
			if(loanPerson.getLoanid() != null) {
				loanOrderSercice.updateOrderPersonByPrimaryKeySelective(loanPerson);
			}else {
				loanOrderSercice.insertOrderPersonSelective(loanPerson);
			}
			view.setViewName("model/matchingFunds");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("保存订单个人信息异常",e);
		}
		return view;
	}
	

	/**
	 * 	订单详情-个人信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getLoanPerson.do")
	public ModelAndView getLoanPerson(HttpServletRequest request,Long  loanId)
	{
		ModelAndView view=  new ModelAndView("order/orderPerson");
		try{
			SysUserDo currentUser  = this.currentUser();
			if(currentUser == null ) {
				return view;
			}
			
			
			LoanPersonExample example = new LoanPersonExample();
			example.createCriteria().andLoanidEqualTo(loanId).andUseridEqualTo(Long.valueOf(currentUser.getId()));
			List<LoanPerson> personLst = loanOrderSercice.selectOrderPersonByExample(example );
			Loanorder order=loanOrderSercice.selectByPrimaryKey(loanId);
			view.addObject("order",order);
			//没有找到数据
			if(null == personLst||personLst.size()<1) {
				return view;
			}
			
			view.addObject("person", personLst.get(0));
			
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单个人信息出错",e);
		}
		return view;
	}
	
}
