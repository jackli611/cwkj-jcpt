package com.cwkj.personalloan.controller.login;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 第三方登录： 微信或商户
 * @author harry
 *
 */
class ThirdLogin  extends  CommonLogin implements IPlatformLogin{
	
	protected UsernamePasswordToken getToken(LoginForm loginForm) {
		UsernamePasswordToken token = loginForm.getUsernamePasswordToken();
		//第三方登录密码的特别处理，用数据库的密码，不加盐值
		token.setPassword(loginForm.getPwd().toCharArray());
		token.setHost(loginForm.getLoginIp());
		return token;
	}
}
