package com.cwkj.personalloan.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cwkj.jcpt.util.TokenUtil;

/**
 * 为每次设置前端token到session
 * 设置到session 需要验证的的时候在各自的方法里去验证，加注解
 */
public class TokenInterceptor extends HandlerInterceptorAdapter{  
	
    private final Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);

	@Override    
    public boolean preHandle(HttpServletRequest request,    
            HttpServletResponse response, Object handler) throws Exception {    
		return super.preHandle(request, response, handler);
		
    }    
    
    /** 
     * 在业务处理器处理请求执行完成后,生成视图之前执行的动作    
     * 可在modelAndView中加入数据，比如当前时间 
     */  
    @Override    
    public void postHandle(HttpServletRequest request,    
            HttpServletResponse response, Object handler,    
            ModelAndView modelAndView) throws Exception {     
        super.postHandle(request, response, handler, modelAndView);
    }    
    
    /**  
     * 
     * 为每个请求的uri设置token  
     */    
    @Override    
    public void afterCompletion(HttpServletRequest request,    
            HttpServletResponse response, Object handler, Exception ex)    
            throws Exception {
    	String url = request.getRequestURL().toString();
    	if(url.contains(".do")) {
    		TokenUtil.putToken(request, request.getRequestURI());
    	}
    	super.afterCompletion(request, response, handler, ex);
    }    
}    