package com.cwkj.personalloan.controller.colourlife;

import javax.servlet.http.HttpServletRequest;

import com.cwkj.personalloan.controller.base.ShareLinkView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.IPUtils;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.ModelAndViewUtil;
import com.cwkj.personalloan.controller.login.ColorlifeLoginHandler;
import com.cwkj.personalloan.controller.login.LoginForm;
import com.cwkj.personalloan.controller.login.PlatformLoginProxy;

@Controller
public class ColourlifeController extends BaseAction {

    private static Logger logger= LoggerFactory.getLogger(ColourlifeController.class);
    @Autowired
    private ColorlifeLoginHandler colorLoginHandler;

	/**
	 * 用户登录
	 */
	@RequestMapping(value="/merLogin.do", method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView login(HttpServletRequest request){
		ModelAndView view = new ModelAndView("model/merLogin-error");
		try {
			//保存分享ID
			ShareLinkView.saveShareIdToSession(request);
			PlatformLoginProxy lp = new PlatformLoginProxy(colorLoginHandler);
			LoginForm loginForm = new LoginForm();
			loginForm.setReqData(request.getParameterMap());
			loginForm.setLoginIp(IPUtils.getIP(request));
			view= lp.login(request,loginForm );
		}catch (BusinessException e){
	            logger.error("登录校验失败",e.getMessage());
	            ModelAndViewUtil.setPromptException(view,e);
        }catch (Exception ex){
            //ex.printStackTrace();
            view = new ModelAndView("model/merLogin-error");
            logger.error("彩生活商户登录异常",ex);
            ModelAndViewUtil.setPromptMessage(view,PROMPTCODE_FAIL,"error:10000,请求异常");
        }
		return view;
	}
	
}
