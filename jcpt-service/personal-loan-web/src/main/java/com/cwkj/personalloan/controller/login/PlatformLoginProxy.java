package com.cwkj.personalloan.controller.login;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

public class PlatformLoginProxy  implements IPlatformLogin{
	
	private InvocationHandler loginHandler ;
	
	public PlatformLoginProxy(InvocationHandler loginHandler) {
		this.loginHandler = loginHandler;
	}
	

	@Override
	public ModelAndView login(HttpServletRequest request,LoginForm loginForm) {
		Class<?>[] interfaces = {IPlatformLogin.class};
		ClassLoader loader = PlatformLoginProxy.class.getClassLoader();
		IPlatformLogin loginProxy = (IPlatformLogin)Proxy.newProxyInstance(loader, interfaces, loginHandler);
		ModelAndView view= loginProxy.login(request,loginForm);
		return view;
	}

}
