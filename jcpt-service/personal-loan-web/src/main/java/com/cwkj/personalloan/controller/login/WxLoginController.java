package com.cwkj.personalloan.controller.login;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.HttpClientUtils;
import com.cwkj.jcpt.util.IPUtils;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.personalloan.controller.base.ModelAndViewUtil;
import com.cwkj.personalloan.controller.pay.WeiXinOauth2TokenDo;
import com.cwkj.personalloan.controller.sms.SmsComponent;
import com.cwkj.scf.config.WxConfig;
import com.cwkj.scf.model.sms.SMSBusinessType;


/**
 *	 微信公众号入口:
 *  1. 第一步 微信公众号配置菜单重定向到 /wx/loginByOpenId.do
 *  2. 第二步获取微信的openid， 根据openId 登录，没有就跳转到微信绑定页面toBind.jsp 
 *	3.	第三步 实现注册、登录、绑定（已注册就绑定openId然后自动登录, 未注册则注册并绑定然后自动登录) submitBind.do
 *  4. 第四步 登录成功后根据菜单code 跳转到对应页面
 * 
 * @author harry
 */
@RestController
@RequestMapping(value = "/wx")
public class WxLoginController   {
	
    private final Logger logger = LoggerFactory.getLogger(WxLoginController.class);

    private static final String openIdUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";//获取openId接口url地址
    
    private static  String WXCONTENT="感谢关注\n\r";
    @Autowired
    private WXLoginHandler wxLoginHandler;
    @Autowired
	private SmsComponent smsComponent;


    private static final Map<String,String> menuMap=new HashMap<String,String>();
    static{
    	//定义菜单
        //menuMap.put("D002","");//菜单
    }

   

    /**
     * 	提交绑定，实现微信登陆
     *  实现注册、登录、绑定（已注册就绑定openId然后自动登录, 未注册则注册并绑定然后自动登录
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = "/submitBind.do", method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ModelAndView submitBind(HttpServletRequest request, HttpServletResponse response, Model model) {
    	ModelAndView view = new ModelAndView("weixin/wxbind");
    	try {
        	
    		String mobile = request.getParameter( "userName");// 手机号帐号
    		 
    		JsonView v = smsComponent.checkSms(request,mobile, SMSBusinessType.resetPsw.name());
			if (JsonView.CODE_SUCCESS.intValue() != v.getCode().intValue()) {
				ModelAndViewUtil.setPromptMessage(view,"无效的短信验证码");
            	return view;
			}
			
            if (StringUtils.isBlank(mobile)) {
            	logger.error("微信绑定失败手机号码为空");
            	ModelAndViewUtil.setPromptMessage(view,"手机号码为空");
            	return view;
            }
            return login(request,response);
            
        } catch (Exception e) {
            logger.error("微信登录失败", e);
            ModelAndViewUtil.setPromptMessage(view,"微信绑定失败系统异常");
        	return view;
        }
    }


    /**
     * 	获取微信用户的openId
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/loginByOpenId.do")
    public ModelAndView loginByOpenId(HttpServletRequest request, 
    								  HttpServletResponse response) {
        
    	
    	ModelAndView modelAndView = new ModelAndView("redirect:/index.do");
        logger.info("微信请求对应的url地址======》："+ "menuCode:" + request.getParameter("menuCode") );
        
        try {
            request.setCharacterEncoding("UTF-8");

            //根据openId登录， 如果没有绑定会跳转到绑定视图，如果出错跳绑定视图
            modelAndView = login(request,response);
            

        } catch (Exception e) {
            logger.info("获取网页授权凭证openId时异常,"+e);
        }
        return modelAndView;
    }

  

    /**
     * 用openId 登录， 如果openId 为空 直接去首页不登录
     * 如果openId 非空登录失败去绑定， 绑定后以后微信直接免登陆
     * @param request
     * @param response
     * @return
     */
    private ModelAndView login(HttpServletRequest request, 
    								  HttpServletResponse response) {
    	
		ModelAndView view = null;
		
		try {
			
			//为了安全，防止客户端恶意获取，必须从微信获取openId
            String openId = pickOpenId(request);
            logger.info("openId======》："+openId);
            if(StringUtils.isBlank(openId)){
                logger.info("获取网页授权凭证openId时为空！直接返回首页");
                view = new ModelAndView("redirect:/index.do");
                return view;
            }
            
            
			PlatformLoginProxy lp = new PlatformLoginProxy(wxLoginHandler);
			LoginForm loginForm = new LoginForm();
			loginForm.setReqData(request.getParameterMap());
			loginForm.setLoginIp(IPUtils.getIP(request));
			loginForm.setUsername(request.getParameter("userName"));
			loginForm.setThirdId(openId);
			view= lp.login(request,loginForm );
			
			Subject currentUser = SecurityUtils.getSubject();
			//登录成功
            if(currentUser.isAuthenticated()){
            	//通过菜单key获取对应的url地址
                String targetUrl = menuMap.get(request.getParameter("menuCode"));
                logger.info("targetUrl:"+targetUrl);
                if(null == targetUrl) {
                	return view;
                }else {
                	view =  new ModelAndView("redirect:"+targetUrl);
                	return view;
                }
            }else {
            	if(null != view) {//登录失败去绑定
            		view.setViewName("weixin/wxbind");
            	}
            	return view;
            }
            
		}catch (BusinessException e){
            logger.error("微信登录失败",e);
            ModelAndViewUtil.setPromptException(view,e);
        }catch (Exception ex){
            logger.error("微信登录异常",ex);
            ModelAndViewUtil.setPromptException(view,ex);
        }finally {
        	
        	if(null == view) {
				view = new ModelAndView("redirect:/index.do");
			}
    		return view;
        }
		
	
	}

    private String pickOpenId(HttpServletRequest request ){
        String openId = "";
        String code = request.getParameter("code");
        logger.info("获取网页授权凭证code:"+code);
        String requestUrl = openIdUrl.replaceAll("APPID",WxConfig.getAppId()).replaceAll("SECRET",WxConfig.getSecret()).replaceAll("CODE",code);

        if (!"authdeny".equals(code)) {
            WeiXinOauth2TokenDo weiXinOauth2TokenDo = this.getOauth2AccessTokenDo(requestUrl);
            if(weiXinOauth2TokenDo!=null){
                openId = weiXinOauth2TokenDo.getOpenId();
            }
        }
        request.getSession().setAttribute("MySessionOpenId", openId);
        logger.info("获取网页授权凭证openId:"+openId);
        return openId;
    }

    private  WeiXinOauth2TokenDo getOauth2AccessTokenDo(String requestUrl) {
        WeiXinOauth2TokenDo wat = new WeiXinOauth2TokenDo();
        try{
            JSONObject jsonObject =  HttpClientUtils.httpsGetRequest(requestUrl);
            if (null != jsonObject) {
                wat.setAccessToken(jsonObject.getString("access_token"));
                wat.setExpiresIn(jsonObject.getInteger("expires_in"));
                wat.setRefeshToken(jsonObject.getString("refresh_token"));
                wat.setOpenId(jsonObject.getString("openid"));
                wat.setScope(jsonObject.getString("scope"));
            }
        }catch (Exception e) {
            logger.info("获取网页授权凭证失败 :",e);
        }
        return wat;
    }
    
	/**
     * 	响应微信事件
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping(value = "/event")
    public void  wxEvent(HttpServletRequest request,
    					 HttpServletResponse response,
    					 Model model) {
	    logger.info("wxEvent....");
		boolean isGet = request.getMethod().toLowerCase().equals("get");
		if (isGet) {
			String echostr = request.getParameter("echostr");
			logger.info("echostr" + request.getParameter("echostr")); 
			if(StringUtils.isNotBlank(echostr)){
				try {
					response.getWriter().write(echostr);
				} catch (IOException e) {
					logger.info("response write异常："+e);
					e.printStackTrace();
				}
			}
			//access(request, response);
		} else {//事件响应
			logger.info("enter post");
			try {
				acceptMessage(request, response);//接收消息并返回消息
			} catch (Exception e) {
				logger.info("acceptMessage异常："+e);
				e.printStackTrace();
			}
		}
	}
    
    private void acceptMessage(HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException {
		// 处理接收消息
		ServletInputStream in = request.getInputStream();
		SAXReader sax = new SAXReader();//创建SAXReader对象 
		Document doc = sax.read(in);
		Element root = doc.getRootElement();
		String toUserName = root.element("ToUserName").getText();//开发者微信号
		String custermname = root.element("FromUserName").getText();//发送方帐号
		long createTime = Long.parseLong(root.element("CreateTime").getText());// 接收时间
		String msgType = root.element("MsgType").getText();//消息类型
		Long returnTime = Calendar.getInstance().getTimeInMillis() / 1000;// 返回时间
		logger.info("开发者微信号：" +toUserName+";发送方帐号：" + custermname+";消息创建时间：" + createTime+";消息类型：" + msgType);
		//组装回复微信用户的信息内容
		StringBuffer str = new StringBuffer();
		str.append("<xml>");
		str.append("<ToUserName><![CDATA[" + custermname + "]]></ToUserName>");
		str.append("<FromUserName><![CDATA[" + toUserName + "]]></FromUserName>");
		str.append("<CreateTime>" + returnTime + "</CreateTime>");
		str.append("<MsgType><![CDATA[text]]></MsgType>");
		// 根据消息类型获取对应的消息内容
		if("event".equals(msgType)){//消息类型为事件类型
			String event = root.element("Event").getText();//事件类型
			logger.info("事件类型：" + event);
			if("subscribe".equals(event)){//用户关注公众号事件
				Element eventKeyElem = root.element("EventKey");
				if(eventKeyElem!=null){
					String eventKey = eventKeyElem.getText();//qrscene_为前缀，后面为二维码的参数值
					logger.info("二维码scene_id：" + eventKey);
					if(StringUtils.isNotBlank(eventKey)){
						eventKey=eventKey.replaceAll("qrscene_","");//获取渠道ID
						if("117".equals(eventKey)){//由于要区分彩生活以及彩生活电梯活动广告，需要将已存在的117转成118，作为区别对待
							eventKey="118";
						}
						//cacheService.setString("WX_OPENID"+custermname,eventKey);//将渠道ID放入对应用户微信号里
					}
				}
				Element ticketElem = root.element("Ticket");
				if(ticketElem!=null){
					String ticket = ticketElem.getText();//二维码的ticket，可用来换取二维码图片
					logger.info("ticket：" + ticket);
				}
				str.append("<Content><![CDATA["+WXCONTENT+ "]]></Content>");
			}else if("unsubscribe".equals(event)){//用户取消关注公众号事件
				Element eventKeyElem = root.element("EventKey");
				if(eventKeyElem!=null){
					String eventKey = eventKeyElem.getText();//qrscene_为前缀，后面为二维码的参数值
					logger.info("事件key值：" + eventKey);
					if(StringUtils.isNotBlank(eventKey)){
						eventKey=eventKey.replaceAll("qrscene_","");//获取渠道ID
						//cacheService.delete("WX_OPENID"+custermname);//将渠道ID从对应用户微信号里清除
					}
				}
				Element ticketElem = root.element("Ticket");
				if(ticketElem!=null){
					String ticket = ticketElem.getText();//二维码的ticket，可用来换取二维码图片
					logger.info("ticket：" + ticket);
				}
				str.append("<Content><![CDATA[感谢一直以来对小合的关注，期待您再次回来！]]></Content>");
			}else if("SCAN".equals(event)){//用户已关注公众号事件
				Element eventKeyElem = root.element("EventKey");
				if(eventKeyElem!=null){
					String eventKey = eventKeyElem.getText();//二维码scene_id
					logger.info("二维码scene_id：" + eventKey);
//					if(StringUtils.isNotBlank(eventKey)){
//						cacheService.setString("WX_OPENID"+custermname,eventKey);//将渠道ID放入对应用户微信号里,已关注的是不是不用再考虑了
//					}
				}
				Element ticketElem = root.element("Ticket");
				if(ticketElem!=null){
					String ticket = ticketElem.getText();//二维码的ticket，可用来换取二维码图片
					logger.info("ticket：" + ticket);
				}
				
				str.append("<Content><![CDATA["+WXCONTENT+ "]]></Content>");
			}else if("LOCATION".equals(event)){//上报地理位置事件
				String latitude = root.element("Latitude").getText();//地理位置纬度
				String longitude = root.element("Longitude").getText();//地理位置经度
				String precision = root.element("Precision").getText();//地理位置精度
				logger.info("地理位置纬度：" + latitude+";经度：" + longitude+";精度：" + precision);
				str.append("<Content><![CDATA[目前您的地理位置纬度：" + latitude+";经度：" + longitude+";精度：" + precision+"]]></Content>");
			}else if("CLICK".equals(event)){//点击菜单拉取消息时的事件
				String eventKey = root.element("EventKey").getText();//事件KEY值，与自定义菜单接口中KEY值对应
				logger.info("事件key值：" + eventKey);
			}else if("VIEW".equals(event)){//点击菜单跳转链接时的事件
				String eventKey = root.element("EventKey").getText();//事件KEY值，设置的跳转URL
				logger.info("事件key值即跳转URL：" + eventKey);
			}
			str.append("</xml>");
			logger.info(str.toString());
			response.getWriter().write(str.toString());
		}else if("text".equals(msgType)){
			
			str.append("<Content><![CDATA["+WXCONTENT+ "]]></Content>");
			str.append("</xml>");
			logger.info(str.toString());
			response.getWriter().write(str.toString());
		}else{//其它消息类型，暂不做处理，直接返回空
			response.getWriter().write("");
		}
		
//		// 获取并返回多图片消息
//		if (msgType.equals(MsgType.Image.toString())) {
//			System.out.println("获取多媒体信息");
//			System.out.println("多媒体文件id：" + inputMsg.getMediaId());
//			System.out.println("图片链接：" + inputMsg.getPicUrl());
//			System.out.println("消息id，64位整型：" + inputMsg.getMsgId());
//
//			OutputMessage outputMsg = new OutputMessage();
//			outputMsg.setFromUserName(servername);
//			outputMsg.setToUserName(custermname);
//			outputMsg.setCreateTime(returnTime);
//			outputMsg.setMsgType(msgType);
//			ImageMessage images = new ImageMessage();
//			images.setMediaId(inputMsg.getMediaId());
//			outputMsg.setImage(images);
//			System.out.println("xml转换：/n" + xs.toXML(outputMsg));
//			response.getWriter().write(xs.toXML(outputMsg));
//
//		}
	}
}
