package com.cwkj.personalloan.controller.platform;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysAttachmentService;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.personalloan.controller.base.ShareLinkView;
import com.cwkj.scf.config.WxConfig;
import com.cwkj.scf.wxpay.util.JSSDKSignUtil;

/**
 * 平台
 * 
 * @author ljc
 * 
 */
@Controller
public class MainController extends BaseAction {

  private static Logger log = LoggerFactory.getLogger(MainController.class);
  @Autowired
  private SysUserService sysUserService;
  @Autowired
  private SysAttachmentService sysAttachmentService;

  private static final  String userPhotoPath="/res/images/logo/default_user.png";

 
  @RequestMapping("/index.do")
  public ModelAndView index(HttpServletRequest request,
                            HttpServletResponse response) {
	  
	ModelAndView view= new ModelAndView("model/index");

    try {
    	SysUserDo currentUser = currentUser();
        if(null != currentUser) {
        	view.addObject("currentUser", currentUser);
        	//设置自己的分享信息
            String shareKey=ShareLinkView.getShareLinkKey();
            String shareId=request.getParameter(shareKey);
            Long myShareId=ShareLinkView.shareIdByUserId(currentUser.getId());
            if(myShareId!=null && (StringUtil.isEmpty(shareId) || !myShareId.toString().equals(shareId)))//验证是否有分享ID或分享ID是否本人分享ID
            {
                String url=ShareLinkView.builderUrlShareId("redirect:index.do",currentUser.getId());
                view=new ModelAndView(url);
            }
        	ShareLinkView.setViewShareId(view,currentUser.getId());

        }else{
            //保存上级分享者参数
            ShareLinkView.saveShareIdToSession(request);
        }

       }catch (BusinessException ex)
    {
        setPromptException(view, ex);
    } catch (Exception e) {
        logger.error("使用主界面异常：",e);
      setPromptException(view, e);

    }
    
    return view;
  }

    @RequestMapping("/wxJSConfig.do")
  public ModelAndView wxJSConfig(HttpServletRequest request,String signUrl)
  {
      ModelAndView view = new ModelAndView("weixin/wx-jsconfig");
      try {
            AssertUtils.isNotBlank(signUrl,"请求参数无效");
            logger.info("[wxJSConfig]请求微信使用访问地址：signUrl="+signUrl);
          //微信JS配置
          Map<String, String> jssignInfo = JSSDKSignUtil.getSignInfo(JSSDKSignUtil.getWxAccessToken(WxConfig.getAppId(), WxConfig.getSecret()), signUrl);
          jssignInfo.put("appid",WxConfig.getAppId());
          logger.info("jssignInfo="+JSON.toJSONString(jssignInfo));
          view.addAllObjects(jssignInfo);
      }catch (Exception ex)
      {
          logger.error("获取微信JS SDK配置异常",ex);
          setPromptException(view,ex);
      }
      return view;

  }
  
  @RequestMapping("/usageMain.do")
  public ModelAndView usageMain(HttpServletRequest request,
		  HttpServletResponse response) {
	  
	  ModelAndView view= new ModelAndView("model/usageMain");
	  
	  try {
		  SysUserDo currentUser = currentUser();
          if(null != currentUser) {
              view.addObject("currentUser", currentUser);
              //设置当前用户的分享ID
               ShareLinkView.setViewShareId(view,currentUser.getId());
          }else {
              //保存上级分享者参数
              ShareLinkView.saveShareIdToSession(request);

          }
		  
	  }catch (BusinessException ex)
	  {
		  setPromptException(view, ex);
	  } catch (Exception e) {
		  logger.error("使用主界面异常：",e);
		  setPromptException(view, e);
		  
	  }
	  return view;
  }
  


  @RequestMapping(value="/loadmodel.do", method= RequestMethod.GET)
  public ModelAndView resetPasswordPage(String id)
  {
    ModelAndView modelView = new ModelAndView("loadmodel/"+id);
    return modelView;
  }

    @RequestMapping(value="/userPhotoChange.do",method= RequestMethod.POST)
    public JsonView<String> userPhotoChange_methodPost(HttpServletRequest request, HttpServletResponse response, String photo)
    {
        JsonView<String> view=new JsonView<String>();
        try{
            String img=request.getParameter("photo");
            AssertUtils.isNotBlank(img,"参数无效");
            SysUserDo sysUserDo=new SysUserDo();
            sysUserDo.setImg(img);
            sysUserDo.setId(currentUser().getId());
            Integer dbRes= sysUserService.updateUserPhoto(sysUserDo);
            AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"保存失败");
            //更新附件使用状态。
            sysAttachmentService.updateCheckedStatusPass(Long.valueOf(img),currentUser().getId());
            setPromptMessage(view, JsonView.CODE_SUCCESS,"保存成功");
            view.setData(img);
        }catch(BusinessException e)
        {
            setPromptException(view, e);
        }catch (Exception e) {
            logger.error("修改用户头像异常", e);
            setPromptException(view, e);
        }
        return view;
    }

}
