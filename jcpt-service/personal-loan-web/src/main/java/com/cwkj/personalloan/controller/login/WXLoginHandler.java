package com.cwkj.personalloan.controller.login;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.cwkj.personalloan.controller.base.ShareLinkView;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.base.Channel;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.scf.model.merchant.MerchantAccountDo;
import com.cwkj.scf.model.merchant.MerchantProfileDo;
import com.cwkj.scf.service.merchant.MerchantAccountService;
import com.cwkj.scf.service.merchant.MerchantProfileService;

@Component("wxLoginHandler")
public class WXLoginHandler implements InvocationHandler {

	protected int PROMPTCODE_OK=1;
	protected int PROMPTCODE_FAIL=0;
	
	private static Logger logger= LoggerFactory.getLogger(WXLoginHandler.class);
	private IPlatformLogin  loginImpl = new ThirdLogin();

    @Autowired
    private MerchantProfileService merchantProfileService;
    @Autowired
    private MerchantAccountService merchantAccountService;

    @Autowired
    private SysUserService sysUserService;
    
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		logger.info("wx login");
		LoginForm loginForm  = (LoginForm) args[1];
		HttpServletRequest request  = (HttpServletRequest) args[0];
		boolean isBind = wxLogin(request,loginForm);
		//未绑定
		if(isBind == false) {
			return new ModelAndView("weixin/wxbind");
		}
		Object result =  method.invoke(loginImpl, args);
		return result;
	}
	
	private void buildChannel(HttpServletRequest request,MerchantProfileDo merchant) {
		Channel cnl = new Channel();
		cnl.setChannelCode(merchant.getPropertyId());
		cnl.setLoginSource("小物橙公众号");
		request.getSession().setAttribute("CurrentChannel", cnl);
	}
	
    private boolean wxLogin(HttpServletRequest request,LoginForm loginForm){
    	logger.info("微信公众号登录：loginForm={}", JSON.toJSONString(loginForm));
        Map<String, Object> selectItem = new HashMap<String,Object>();
        selectItem.put("merId", "wx");
        List<MerchantProfileDo> merProfileLst = merchantProfileService.queryMerchantProfileList(selectItem);
        if(merProfileLst ==null || merProfileLst.size()<1) {
        	throw new BusinessException("没有配置微信作为第三方登录");
        }
        MerchantProfileDo merchant = merProfileLst.get(0);
        
        if(StringUtil.isBlank(loginForm.getThirdId())) {
        	throw new  BusinessException("微信公众号openId不能为空");
        }
        

        //校验用户是否存在，创建用户并保存为登录状态
        MerchantAccountDo merchantAccountDo=new MerchantAccountDo();
        merchantAccountDo.setAcctid(loginForm.getThirdId());
        merchantAccountDo.setMerId(merchant.getMerId());
        merchantAccountDo.setMobile(loginForm.getUsername());
        Long shareId= ShareLinkView.getShareIdFromSession();//分享ID
        MerchantAccountDo accountDo= merchantAccountService.bindWxOpenId(merchantAccountDo,merchant,shareId);
        if(accountDo == null) {
        	return false;
        }
        SysUserDo sysUserDo= sysUserService.findSysUserById(accountDo.getUserId());
        AssertUtils.notNull(sysUserDo,"error:10009,账号校验不通过");
        
        //設置當前登錄渠道
        buildChannel(request, merchant);
        
        //保存用户登录状态
        Subject currentUser = SecurityUtils.getSubject();
        if(currentUser.isAuthenticated()){
            SecurityUtils.getSubject().logout();
        }
        loginForm.setPwd(sysUserDo.getPassword());
        loginForm.setUsername(sysUserDo.getUserName());
        return true;
    }
	

}
