package com.cwkj.personalloan.controller.pay;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cwkj.personalloan.controller.base.BaseAction;
import com.github.wxpay.sdk.WXPayUtil;

@RestController
public class WxPayNotifyController extends BaseAction {
	
    private final static Logger logger = Logger.getLogger(WxPayNotifyController.class);

    @Autowired
    private PayCommponent payCommponent;
   
    /**
     * 	返回成功xml
     */
    private String resSuccessXml = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";

    /**
     * 	返回失败xml
     */
    private String resFailXml = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[报文为空]]></return_msg></xml>";

    /**
     *	 该链接是通过【统一下单API】中提交的参数notify_url设置，如果链接无法访问，商户将无法接收到微信通知。
     * 	通知url必须为直接可访问的url，不能携带参数。示例：notify_url：“https://pay.weixin.qq.com/wxpay/pay.action”
     * <p>
     * 	支付完成后，微信会把相关支付结果和用户信息发送给商户，商户需要接收处理，并返回应答。
     * 	对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，微信会通过一定的策略定期重新发起通知，尽可能提高通知的成功率，但微信不保证通知最终能成功。
     * 	（通知频率为15/15/30/180/1800/1800/1800/1800/3600，单位：秒）
     * 	注意：同样的通知可能会多次发送给商户系统。商户系统必须能够正确处理重复的通知。
     * 	推荐的做法是，当收到通知进行处理时，首先检查对应业务数据的状态，判断该通知是否已经处理过，如果没有处理过再进行处理，如果处理过直接返回结果成功。在对业务数据进行状态检查和处理之前，要采用数据锁进行并发控制，以避免函数重入造成的数据混乱。
     * 	特别提醒：商户系统对于支付结果通知的内容一定要做签名验证，防止数据泄漏导致出现“假通知”，造成资金损失。
     *
     */
    @RequestMapping("/wxpaybycodenotify")
    public void wxnotify(HttpServletRequest request, HttpServletResponse response) {

        String resXml = resFailXml;
        InputStream inStream = null;
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        try {

            inStream = request.getInputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = inStream.read(buffer)) != -1) {
                outSteam.write(buffer, 0, len);
            }

            // 获取微信调用我们notify_url的返回信息
            String result = new String(outSteam.toByteArray(), "utf-8");
            WXPayUtil.getLogger().info("wxnotify: response content:" + result);
            Map<String, String> resultMap = WXPayUtil.xmlToMap(result);
            
            payCommponent.wxNotify(result);
            resXml = resSuccessXml;
        } catch (Exception e) {
        	logger.error("wxnotify: process error", e);
        	
        } finally {
            try {
                // 处理业务完毕
                BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
                out.write(resXml.getBytes());
                out.flush();
                out.close();
                // 关闭流
                if(outSteam != null) {
                	outSteam.close();
                }
                if(inStream != null) {
                	inStream.close();
                }
            } catch (IOException e) {
            	logger.error("wxnotify:close out error", e);
            }
        }

    }
}
