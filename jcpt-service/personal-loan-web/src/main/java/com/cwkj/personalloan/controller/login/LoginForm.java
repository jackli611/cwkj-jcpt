package com.cwkj.personalloan.controller.login;

import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.authc.UsernamePasswordToken;

import com.cwkj.jcpt.common.constant.SystemConst;

public class LoginForm {
	
	private Map<String,Object> reqData;
	
	private String pwd;
	private String username;
	private String loginIp;
	private String thirdId;
	
	
	public Map<String, Object> getReqData() {
		return reqData;
	}
	public void setReqData(Map<String, Object> reqData) {
		this.reqData = reqData;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	
	public String getThirdId() {
		return thirdId;
	}
	public void setThirdId(String thirdId) {
		this.thirdId = thirdId;
	}
	public UsernamePasswordToken getUsernamePasswordToken() {
		String psw=DigestUtils.md5Hex(pwd+ SystemConst.PASS_KEY);
		UsernamePasswordToken token = new UsernamePasswordToken(username, psw);
		return token;
	}
	

}
