package com.cwkj.personalloan.controller.platform;


import com.cwkj.jcpt.util.TokenUtil;
import com.cwkj.personalloan.controller.base.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ljc
 *
 */
@Controller
public class DefaultController extends BaseAction {

    private static Logger logger= LoggerFactory.getLogger(DefaultController.class);


    @RequestMapping(value="/model.do", method= RequestMethod.GET)
    public ModelAndView test_matchingFunds(HttpServletRequest request,String mod,String loanType){
        ModelAndView model = new ModelAndView("model/"+mod);
        try {
        	if(mod !=null && mod.startsWith("zhifu")) {
        		String token = TokenUtil.putToken(request,  "pay");
        		model.addObject("token", token);
        	}
            model.addObject("loanType",loanType);
        }catch (Exception ex)
        {
            logger.error("首页异常",ex);
        }
        return model;
    }




}
