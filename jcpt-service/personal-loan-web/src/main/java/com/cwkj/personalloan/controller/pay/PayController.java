package com.cwkj.personalloan.controller.pay;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.LoanorderExample;
import com.cwkj.scf.model.loan.PayInfoBean;
import com.cwkj.scf.model.loan.SysUserAuth;
import com.cwkj.scf.model.loan.SysUserAuthExample;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.service.loan.ILoanOrderService;


/**
 *        对接支付视图的接口
 * 	@author harry
 *
 */
@RestController
@RequestMapping("/pay")
public class PayController extends BaseAction {
	
	@Autowired
	private PayCommponent  payCommponent;
	
	@Autowired
    private ILoanOrderService loanOrderSercice;
	
	
	/**
	 * 	根据支付通道不同，封装页面支付按钮响应事件：目前支持 微信，徽商
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getPayBtnByChannel.do")
	public ModelAndView getPayBtnByChannel(HttpServletRequest request,Long loanid){
		String channel = payCommponent.getPayChannel();
		String payStepCode = request.getParameter("payStepCode");
		ModelAndView view=  new ModelAndView("pay/"+payStepCode+"_"+channel+"PayButton");
		if(loanid != null) {
			Loanorder order = loanOrderSercice.selectByPrimaryKey(loanid);
			 //order.calPayAmt();
			view.addObject("order",order);
			view.addObject("payInfo",order.calPayAmt());
		}

		return view;
	}
	
	
	/**
	 * 	订单支付按钮，根据订单执行状态封装按钮名称： 订单，尾款
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getOrderPayStepBtn.do")
	public ModelAndView getOrderPayStepBtn(HttpServletRequest request,Long loanid){
		ModelAndView view=  new ModelAndView();
		try{
			SysUserDo currentUser  = this.currentUser();
			if(currentUser == null ) {
				return view;
			}
			long userId = currentUser.getId(); 
			if(loanid != null) {
				LoanorderExample example  = new LoanorderExample();
				example.createCriteria().andLoanidEqualTo(loanid).andUseridEqualTo(userId ).andProdcodeEqualTo(ProductEnum.PROD_PERSONAL_XY.getProductCode());
				List<Loanorder> orderLst = loanOrderSercice.selectByExample(example);
				if(orderLst.size()>0) {
					Loanorder order = orderLst.get(0);
					PayInfoBean payInfo = order.calPayAmt();
					view.addObject("orderpay", payInfo);
					view.setViewName("pay/"+payInfo.getPayStepCode()+"_view_in_orderpage");
				}
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("查询订单出错",e);
		}
		return view;
	}
	
	
	
	/**
     *	跳转到订单定金和尾款支付页面
     * @return
     */
    @RequestMapping(value = "/gotoPayPageByFeeType", method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView gotoPayPageByFeeType(HttpServletRequest request,Long loanId) {
    	String payStepCode = request.getParameter("payStepCode");
//    	String loanId = request.getParameter("loanId");
    	String payAmt = request.getParameter("payAmt");
    	
    	ModelAndView view =  new ModelAndView("pay/"+payStepCode);
    	view.addObject("payStepCode", payStepCode);
    	view.addObject("loanid", loanId);
    	view.addObject("payAmt", payAmt);

		if(loanId != null) {
			Loanorder order = loanOrderSercice.selectByPrimaryKey(loanId);
			view.addObject("order",order);
		}

    	return view;
    }

}
