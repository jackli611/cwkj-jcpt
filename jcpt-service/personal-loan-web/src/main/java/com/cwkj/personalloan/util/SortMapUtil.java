package com.cwkj.personalloan.util;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Map排序
 */
public class SortMapUtil {


    /**
     * 按KEY排序
     * @param map
     * @return
     */
public static Map<String, String> sortMapByKey(Map<String, String> map) {
    if (map == null || map.isEmpty()) {
        return null;
    }

    Map<String, String> sortMap = new TreeMap<>(
            new MapKeyComparator());

    sortMap.putAll(map);

    return sortMap;
}

}

class MapKeyComparator implements Comparator<String> {

    @Override
    public int compare(String str1, String str2) {

        return str1.compareTo(str2);
    }
}