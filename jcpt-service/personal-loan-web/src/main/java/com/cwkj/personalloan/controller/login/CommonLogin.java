package com.cwkj.personalloan.controller.login;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysLoginHistoryDo;
import com.cwkj.jcptsystem.service.system.SysLoginHistoryService;
import com.cwkj.personalloan.controller.base.ModelAndViewUtil;

class CommonLogin implements IPlatformLogin {
	private String lock ="0";
	protected static Logger logger=LoggerFactory.getLogger(LoginController.class);
	
	protected SysLoginHistoryService sysLoginHistoryService;

	@Override
	public ModelAndView login(HttpServletRequest request,LoginForm loginForm) {
		//登記登錄日誌
		SysLoginHistoryDo loginHistory = logLogin(request,loginForm);
		
		ModelAndView model = new ModelAndView("model/login");
		UsernamePasswordToken token = getToken(loginForm);
		//获取当前的Subject
		Subject currentUser = SecurityUtils.getSubject();
		try {
			if(currentUser.isAuthenticated()){
				model = new ModelAndView("redirect:/index.do");
				return model;
			}
			logger.info("对用户[" + loginForm.getUsername() + "]进行登录验证..验证开始");
			currentUser.login(token);
			logger.info("对用户[" + loginForm.getUsername() + "]进行登录验证..验证通过");
			model = new ModelAndView("redirect:/index.do");

		}catch (BusinessException ex){
			ModelAndViewUtil.setPromptMessage(model, ex.getMessage());
		}catch(UnknownAccountException uae){
			logger.info("对用户[" + loginForm.getUsername() + "]进行登录验证..验证未通过,未知账户");
			ModelAndViewUtil.setPromptMessage(model, "未知账户");
		}catch(IncorrectCredentialsException ice){
			logger.info("对用户[" + loginForm.getUsername() + "]进行登录验证..验证未通过,错误的凭证");
			ModelAndViewUtil.setPromptMessage(model, "密码不正确");
		}catch(LockedAccountException lae){
			logger.info("对用户[" + loginForm.getUsername() + "]进行登录验证..验证未通过,账户已锁定");
			ModelAndViewUtil.setPromptMessage(model, "账户已锁定");
		}catch(ExcessiveAttemptsException eae){
			logger.info("对用户[" + loginForm.getUsername() + "]进行登录验证..验证未通过,错误次数过多");
			ModelAndViewUtil.setPromptMessage(model, "用户名或密码错误次数过多");
		}catch(AuthenticationException ae){
			logger.info("对用户[" + loginForm.getUsername() + "]进行登录验证..验证未通过,堆栈轨迹如下");
			logger.error("登录验证失败",ae);
			ModelAndViewUtil.setPromptMessage(model, "用户名或密码不正确");
		}
		
		//验证是否登录成功
		if(currentUser.isAuthenticated()){
			logger.info("用户[" + loginForm.getUsername() + "]登录认证通过(这里可以进行一些认证通过后的一些系统参数初始化操作)");
			loginHistory.setStatus(2);
		}else{
			token.clear();
			loginHistory.setStatus(3);
		}
		updateLoginStatus(request,loginHistory);
		return model;
	
	}

	protected UsernamePasswordToken getToken(LoginForm loginForm) {
		UsernamePasswordToken token = loginForm.getUsernamePasswordToken();
		token.setHost(loginForm.getLoginIp());
		return token;
	}
	
	private SysLoginHistoryService getSysLoginHistoryService(HttpServletRequest request) {
		synchronized(lock) {
			if(sysLoginHistoryService == null) {
				//spring上下文环境
				WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
				//日志service
				sysLoginHistoryService = (SysLoginHistoryService) springContext.getBean(SysLoginHistoryService.class);
			}
		}
		return this.sysLoginHistoryService;
	}
	
	private SysLoginHistoryDo logLogin(HttpServletRequest request,LoginForm loginForm) {
		//记录登记日志
		SysLoginHistoryDo loginHistory=new SysLoginHistoryDo();
		loginHistory.setIp(loginForm.getLoginIp());
		loginHistory.setCreateTime(new Date());
		loginHistory.setUserName(loginForm.getUsername());
		loginHistory.setUserAgent(request.getHeader("User-Agent"));
		loginHistory.setStatus(1);
		sysLoginHistoryService = this.getSysLoginHistoryService(request);
		sysLoginHistoryService.insertSysLoginHistory(loginHistory);
		return loginHistory;
				
	}
	//更新登錄狀態
	private void updateLoginStatus(HttpServletRequest request,SysLoginHistoryDo loginHistory) {
		//记录登记日志
		sysLoginHistoryService = this.getSysLoginHistoryService(request);
		sysLoginHistoryService.updateStatusById(loginHistory);
		
	}

}
