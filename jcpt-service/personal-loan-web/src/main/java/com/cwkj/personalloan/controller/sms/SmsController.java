package com.cwkj.personalloan.controller.sms;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.scf.model.sms.MessageFormatType;
import com.cwkj.scf.model.sms.MessageTemplate;
import com.cwkj.scf.model.sms.SMSBusinessType;
import com.cwkj.scf.model.sms.TMessage;
import com.cwkj.scf.service.sms.ISmsService;

/**
 * 	短信控制器
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/sms/")
public class SmsController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(SmsController.class);

	@Autowired
	private ISmsService smsService;
	
	/**
	 * 	注册验证
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/sendLoginSms.do")
	public JsonView<String> sendLoginSms(HttpServletRequest request,TMessage message)
	{
		JsonView<String> view= new JsonView<String>();
		try{
			message.setMessageFormatType(MessageFormatType.SIMPLE_ONLY_CODE.name());
			message.setMessageTemplate(MessageTemplate.SMS_159621481.name());
			message.setBusinessType(SMSBusinessType.login.name());
			BaseResult<String> result = smsService.send(message);
			if(result.resultFail()) {
				setPromptMessage(view, JsonView.CODE_FAILE, result.getMsg());
			}else {
				view = JsonView.successJsonView();
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("发送短信异常",e);
		}
		return view;
	}
	
	
	
	/**
	 * 	注册验证
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/sendRegisterSms.do")
	public JsonView<String> sendRegisterSms(HttpServletRequest request,TMessage message)
	{
		JsonView<String> view= new JsonView<String>();
		try{
			message.setMessageFormatType(MessageFormatType.SIMPLE_ONLY_CODE.name());
			message.setMessageTemplate(MessageTemplate.SMS_159621481.name());
			message.setBusinessType(SMSBusinessType.register.name());
			BaseResult<String> result = smsService.send(message);
			if(result.resultFail()) {
				setPromptMessage(view, JsonView.CODE_FAILE, result.getMsg());
			}else {
				view = JsonView.successJsonView();
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("发送短信异常",e);
		}
		return view;
	}



	/**
	 * 	重置密码验证
	 * @param request
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/sendResetPswSms.do")
	public JsonView<String> sendResetPswSms(HttpServletRequest request,TMessage message)
	{
		JsonView<String> view= new JsonView<String>();
		try{
			message.setMessageFormatType(MessageFormatType.SIMPLE_ONLY_CODE.name());
			message.setMessageTemplate(MessageTemplate.SMS_161685134.name());
			message.setBusinessType(SMSBusinessType.resetPsw.name());
			BaseResult<String> result = smsService.send(message);
			if(result.resultFail()) {
				setPromptMessage(view, JsonView.CODE_FAILE, result.getMsg());
			}else {
				view = JsonView.successJsonView();
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("发送短信异常",e);
		}
		return view;
	}
	
	/**
	 * 	发送短信
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/sendSms.do")
	public JsonView<String> sendSms(HttpServletRequest request,TMessage message)
	{
		JsonView<String> view= new JsonView<String>();
		try{
			BaseResult<String> result = smsService.send(message);
			if(result.resultFail()) {
				setPromptMessage(view, JsonView.CODE_FAILE, result.getMsg());
			}else {
				view = JsonView.successJsonView();
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("发送短信异常",e);
		}
		return view;
	}
	
	/**
	 * 	验证短信
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/checkSms.do")
	public JsonView<String> checkSms(HttpServletRequest request,TMessage message)
	{
		JsonView<String> view= new JsonView<String>();
		try{
			boolean result = smsService.checkSms(message.getRecievers(), message.getBusinessType(), message.getMessage());
			if(!result) {
				setPromptMessage(view, JsonView.CODE_FAILE, "无效的短信验证码");
			}else {
				view = JsonView.successJsonView();
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("验证短信异常",e);
		}
		return view;
	}
	

	
}
