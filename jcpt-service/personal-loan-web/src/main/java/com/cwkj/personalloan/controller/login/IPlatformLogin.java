package com.cwkj.personalloan.controller.login;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

public interface IPlatformLogin {
	
	public ModelAndView login(HttpServletRequest request,LoginForm loginForm);

}
