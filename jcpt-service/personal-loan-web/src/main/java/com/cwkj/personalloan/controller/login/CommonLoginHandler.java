package com.cwkj.personalloan.controller.login;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.base.Channel;

@Component("commonLoginHandler")
public class CommonLoginHandler implements InvocationHandler {

	private IPlatformLogin  loginImpl = new CommonLogin();
	
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = method.invoke(loginImpl, args);
		buildChannel(args);
		return result;
	}


	private void buildChannel(Object[] args) {
		HttpServletRequest request  = (HttpServletRequest) args[0];
		Channel cnl = new Channel();
		cnl.setChannelCode("platform");
		if(request.getHeader("User-Agent").contains("micromessenger")) {
			cnl.setLoginSource("wx");
		}else {
			cnl.setLoginSource("H5");
		}
		request.getSession().setAttribute("CurrentChannel", cnl);
	}

}
