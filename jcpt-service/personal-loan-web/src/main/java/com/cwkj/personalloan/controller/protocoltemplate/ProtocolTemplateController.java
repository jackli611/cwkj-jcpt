package com.cwkj.personalloan.controller.protocoltemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.DictSystemInfoService;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.scf.model.loan.LoanOrderStatus;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.LoanorderExample;
import com.cwkj.scf.model.products.ProductEnum;
import com.cwkj.scf.service.loan.ILoanOrderService;

/**
 * 	协议模版控制器
 * @author harry
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/protocol/")
public class ProtocolTemplateController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(ProtocolTemplateController.class);

	@Autowired
	private ILoanOrderService loanOrderSercice;

	/**
	 * 	查看借款协议模板
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/showProtocolTemplate.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView showProtocolTemplate(HttpServletRequest request,
											 String template){
		
		
		ModelAndView view=new ModelAndView("protocoltemplate/"+template);
		return view;
	}

	/**
	 * 借款订单数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/orderList.do",method= {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView orderList(HttpServletRequest request,Long pageIndex,Integer pageSize)
	{
		ModelAndView view=new ModelAndView("model/orderList_table");
		try{
			
			Map<String, Object> selectItem=new HashMap<String, Object>();

			 SysUserDo currentUser  = this.currentUser();
			 if(currentUser == null) {
				 return view;
			 }
			 
			 selectItem.put("userid", currentUser.getId());
			 String loanStatus = request.getParameter("queryLoanstatus");
			 if(StringUtil.isNotBlank(loanStatus)) {
				 selectItem.put("loanstatus", loanStatus);
			 }
			 selectItem.put("prodcode", ProductEnum.PROD_PERSONAL_XY.getProductCode());

			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?200:pageSize;
			 PageDo<Loanorder> pagedata= loanOrderSercice.queryOrderListPage(pageIndex,pageSize, selectItem);
			 view.addObject("pagedata",pagedata);

		}catch(BusinessException e){
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取借款订单数据分页异常", e);
		}
		return view;
	}


}
