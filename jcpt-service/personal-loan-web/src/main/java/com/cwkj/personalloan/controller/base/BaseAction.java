package com.cwkj.personalloan.controller.base;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.cwkj.scf.model.shares.ShareUserDo;
import com.cwkj.scf.service.shares.ShareUserService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.base.Channel;
import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.notify.MailNotify;
import com.cwkj.jcptsystem.service.notify.NotifyService;

/**
 *控制层基类
 *
 * @author ljc
 *
 */
public class BaseAction {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());


	protected int PROMPTCODE_OK=1;
	protected int PROMPTCODE_FAIL=0;

	/**
	 * springContext
	 */
	private WebApplicationContext springContext;

	/**
	 * 发送异常邮件
	 * @param throwable 异常内容
	 * @param title 异常邮件标题
	 * @return
	 */
	private void sendExceptionMail(Throwable throwable,String title) {

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		try {
			throwable.printStackTrace(pw);

			ServletContext servletContext = ContextLoader.getCurrentWebApplicationContext().getServletContext();
			springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			NotifyService notifyService = (NotifyService) springContext.getBean("mailNotifyService");
			Map<String, String> param = new HashMap<String, String>();
			String greeting = String.format("系统异常(来自彩云倍服务)。", DateUtils.format(new Date(), "yyyy年MM月dd日hh时mm分ss秒"));
			param.put("greeting", greeting);
			param.put("content", sw.toString());
			param.put("company", "<div class='line w400'>彩云倍系统</div><br>此为系统邮件请勿回复");
			MailNotify notify = new MailNotify();
			notify.setAsync(true);
			notify.setMessageTemplate("mail_template_jcpt.ftl");
			notify.setRecievers("notification@xwckeji.com");
			notify.setSubject("系统异常：" + title);
			notify.setMessage(param);
			notifyService.send(notify);
		} catch (Exception ex) {
			logger.error("发送系统日志邮件异常", ex);
		}finally{
			try {
				pw.close();
			}catch (Exception e)
			{

			}
		}
	}

	/**
	 * 设置提示业务异常信息
	 * @param view
	 * @param e
	 */
	protected void setPromptException(ModelAndView view,BusinessException e){
		view.addObject("error", e);
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected  <T> void setPromptException(JsonView<T> view,BusinessException e)
	{
		view.setCode(JsonView.CODE_FAILE);
		view.setMsg(e.getMessage());
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected void setPromptException(ModelAndView view,Exception e)
	{
		if(e instanceof DataAccessException)
		{
			view.addObject("error", new BusinessException(BusinessException.code_other, "Fail [Error:1001]"));
		}else {
			view.addObject("error", new BusinessException(BusinessException.code_other, e.getMessage()));
		}
		sendExceptionMail(e,e!=null?e.getMessage():"");
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected  <T> void setPromptException(JsonView<T> view,Exception e)
	{

		if(e instanceof DataAccessException)
		{
			view.setCode(JsonView.CODE_FAILE);
			view.setMsg("Fail [Error:1001]");
		}else {
			view.setCode(JsonView.CODE_FAILE);
			view.setMsg(e.getMessage());
		}
		sendExceptionMail(e,e!=null?e.getMessage():"");
	}
	
	/**
	 * 设置提示信息
	 * @param view
	 * @param msg
	 */
	protected void setPromptMessage(ModelAndView view,String msg)
	{
		view.addObject("promptMsg", msg);
	}

	/**
	 * 设置提示信息
	 * @param view
	 * @param msg 信息
	 * @param code 结果编号
	 */
	protected void setPromptMessage(ModelAndView view,Integer code,String msg)
	{
		view.addObject("promptMsg", msg);
		view.addObject("promptCode", code);
	}

	/**
	 * 设置提示信息
	 * @param view
	 * @param code
	 * @param msg
	 */
	protected <T> void setPromptMessage(JsonView<T> view,Integer code, String msg)
	{
		view.setCode(code);
		view.setMsg(msg);
	}
	
	/**
	 * 检查是否空值，
	 * @param obj
	 * @param msg
	 */
	protected void checkEmpty(Object obj,String msg)
	{
		if(obj==null)
		{
			throw new BusinessException(BusinessException.code_argument,msg);
		}
		if(obj instanceof String && StringUtil.isEmpty((String) obj))
		{
			throw new BusinessException(BusinessException.code_argument,msg);
		}
	}
	
	//=========== 提供权限验证方法 ===========

	protected SysUserDo currentUser()
	{
		Subject subject = SecurityUtils.getSubject();
		if(subject!=null && subject.getPrincipal()!=null )
		{
			return (SysUserDo) subject.getPrincipal();
		}
		return null;
		
	}
	
	/**
	 * 当前平台ID
	 * @return
	 */
	protected int currentPlatformId()
	{
		SysUserDo sysUser=currentUser();
		if(sysUser==null || sysUser.getPlatformId()==null)
		{
			return SystemConst.PLATFORM_DEFAULT;
		}else {
			return sysUser.getPlatformId().intValue();
		}
	}
	
	/**
	 * 验证是否有权限操作，如果没有抛出BusinessException权限类型异常。
	 * @param code
	 * 权限代码。
	 */
	protected void checkPermission(String code)
	{
		try{
//			if(!isSuperSysUser())//不是超级管理员就下一步验证。
//			{
				SecurityUtils.getSubject().checkPermission(code);	
//			}
		}catch (AuthorizationException e) {
			throw new BusinessException(BusinessException.code_auth,"您无操作权限。");
		}
	}
	
	protected void checkAtLeastOnePermission(String ... codes)
	{
			if(codes!=null)
			{
				for(String code:codes)
				{
					if(hasPermission(code))
					{
						return;
					}
				}
			}
			throw new BusinessException(BusinessException.code_auth,"您无操作权限。");
	}
	
	/**
	 * 验证是否拥有角色，如果没有抛出BusinessException权限类型异常。
	 * @param roleIdentifier
	 * 角色名称。
	 */
	protected void checkRole(String roleIdentifier)
	{
		try{
//			if(!isSuperSysUser())//不是超级管理员就下一步验证。
//			{
				SecurityUtils.getSubject().checkRole(roleIdentifier);
//			}
		}catch (AuthorizationException e) {
			throw new BusinessException(BusinessException.code_auth,"您无操作权限。");
		}
	}
	
	
	
	/**
	 * 判断是否拥有角色。
	 * @param roleIdentifier
	 * 角色名称
	 * @return
	 */
	protected Boolean hasRole(String roleIdentifier)
	{
//		if(isSuperSysUser())//不是超级管理员就下一步验证。
//		{
//			return true;
//		}
		return SecurityUtils.getSubject().hasRole(roleIdentifier);
	}
	
	/**
	 * 判断是否拥有权限代码。
	 * @param code
	 * 权限代码。
	 * @return
	 */
	protected Boolean hasPermission(String code)
	{
//		if(isSuperSysUser())//不是超级管理员就下一步验证。
//		{
//			return true;
//		}
		return SecurityUtils.getSubject().isPermitted(code);
	}

	/**
	 * 判断是否是系统超级用户。
	 * @return
	 */
	protected Boolean isSuperSysUser()
	{
		return SecurityUtils.getSubject().hasRole(SystemConst.SYSTEM_SUPERROLE);
	}
	
	

	/**
	 * 设置查询时间范围
	 * @param selectItem
	 * @param startDate
	 * @param endDate
	 */
	protected void setDateBetweemToMap(Map<String, Object> selectItem,String startDate,String endDate)
	{
		try{
		 selectItem.put("startDate", StringUtils.isNotBlank(startDate)?(DateUtils.parse(startDate+" 00:00:00")):null);
		 selectItem.put("endDate", StringUtils.isNotBlank(endDate)?(DateUtils.parse(endDate+" 23:59:59","yyyy-MM-dd hh:mm:ss")):null);
		}catch (ParseException e) {
			e.printStackTrace();
		}
	}
	//=========== 提供权限验证方法 end===========


	/**
	 * 将reuqest参数转换为map.
	 * @param request
	 * @return
	 */
	protected Map<String, Object> getRequestToParamMap(
			HttpServletRequest request) {
	
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> keyNames = request.getParameterNames();
		if(StringUtil.isEmpty(request.getParameter("pageIndex"))){
			paramMap.put("pageIndex", "1");
		}
		if(StringUtil.isEmpty(request.getParameter("pageSize"))){
			paramMap.put("pageSize","20");
		}
		while (keyNames.hasMoreElements()) {
			String attrName = keyNames.nextElement();
			String attrValue = request.getParameter(attrName);
			if (StringUtils.isNotEmpty(attrValue)) {
				paramMap.put(attrName, attrValue.trim());
			}	
		}
		return paramMap;
	}

	protected Map<String, String> getRequestToMap(
			HttpServletRequest request) {

		Map<String, String> paramMap = new HashMap<String, String>();
		Enumeration<String> keyNames = request.getParameterNames();
		if(StringUtil.isEmpty(request.getParameter("pageIndex"))){
			paramMap.put("pageIndex", "1");
		}
		if(StringUtil.isEmpty(request.getParameter("pageSize"))){
			paramMap.put("pageSize","20");
		}
		while (keyNames.hasMoreElements()) {
			String attrName = keyNames.nextElement();
			String attrValue = request.getParameter(attrName);
			if (StringUtils.isNotEmpty(attrValue)) {
				paramMap.put(attrName, attrValue.trim());
			}
		}
		return paramMap;
	}
	
	protected Channel getCurrentChannel(HttpServletRequest request) {
		Object ch  =  request.getSession().getAttribute("CurrentChannel");
		if(ch != null) {
			return (Channel)ch;
		}else {
			Channel cnl = new Channel();
			cnl.setChannelCode("platform");
			cnl.setLoginSource("wx");
			return cnl;
		}
		
	}


}
