package com.cwkj.personalloan.controller.colourlife;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.HttpClientUtil;
import com.cwkj.jcpt.util.Md5Utils;
import com.cwkj.personalloan.controller.base.BaseAction;

@Controller
public class ColourlifeClientTestController extends BaseAction {

    private static Logger logger= LoggerFactory.getLogger(ColourlifeController.class);

    @RequestMapping(value="/testLogin.do")
    public ModelAndView testLogin(HttpServletRequest request)
    {
        ModelAndView view = new ModelAndView("model/testLogin");
        try {
            view.addObject("merId","");
            view.addObject("secretKey","");
            view.addObject("signedMsg","");
            view.addObject("trxId","");
            view.addObject("acctid","");
            view.addObject("mobile","");
            view.addObject("community","");

        }catch (Exception ex)
        {
            logger.error("模拟登录失败",ex);
        }
        return view;
    }




    private static String unicodeToString(String str) {

        Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(str);
        char ch;
        while (matcher.find()) {
            ch = (char) Integer.parseInt(matcher.group(2), 16);
            str = str.replace(matcher.group(1), ch + "");
        }
        return str;
    }

    private static String decodeUnicode2(String dataStr) {
        int start = 0;
        int end = 0;
        final StringBuffer buffer = new StringBuffer();
        while (start > -1) {
            end = dataStr.indexOf("\\u", start + 2);
            String charStr = null;
            if (end == -1) {
                charStr = dataStr.substring(start + 2, dataStr.length());
            } else {
                charStr = dataStr.substring(start + 2, end);
            }
            char letter = (char) Integer.parseInt(charStr, 16);
            buffer.append(new Character(letter).toString());
            start = end;
        }
        return buffer.toString();
    }

    public static void  main2(String[] args)
    {
        try {
            String sn = "201903140000003";
            String md5_key = "xkxk&^%2132334";
            Map<String, String> params = new HashMap<String, String>();
            params.put("sn", sn);
            params.put("czy_id", "1002848704");
            params.put("product_code", "1001");
            params.put("amount", "10000.00");
            params.put("time_limit", "3");
            params.put("purpose", "装修");
            params.put("status", "2");
            params.put("source_code", "203");
            params.put("community_uuid", "1231-34234-12312-123123");

            String sign = Md5Utils.MD5(md5_key + sn);
            params.put("sign", sign);
            String html = HttpClientUtil.post("https://loan-czytest.colourlife.com/loan/xwc/create", params);
            System.out.println("html="+html);
            JSONObject jsonObject=JSONObject.parseObject(html);
            System.out.println("message="+ColourlifeClientTestController.unicodeToString(jsonObject.getString("message")));
//            System.out.println("message="+ColourlifeController.unicodeToString("\u8bf7\u6c42\u6210\u529f"));
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void  main(String[] args)
    {
        try {
            Date now=new Date();
            long times=now.getTime()/1000;
            String sn = "201903140000003";
            String md5_key = "xkxk&^%2132334";
            Map<String, String> params = new HashMap<String, String>();
            params.put("sn", sn);
            params.put("real_amount", "10000.00");
            params.put("real_type", "1");
            params.put("real_limit", "3");
            params.put("real_time", String.valueOf(times));
            params.put("status", "5");

            String sign = Md5Utils.MD5(md5_key + sn);
            params.put("sign", sign);
            String html = HttpClientUtil.post("https://loan-czytest.colourlife.com/loan/xwc/notify", params);
            System.out.println("html="+html);
            JSONObject jsonObject=JSONObject.parseObject(html);
            System.out.println("message="+ColourlifeClientTestController.unicodeToString(jsonObject.getString("message")));
//            System.out.println("message="+ColourlifeController.unicodeToString("\u8bf7\u6c42\u6210\u529f"));
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private  String getIP(HttpServletRequest request) {
        String ip =request.getHeader("x-forwarded-for");
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip =request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        // 多个路由时，取第一个非unknown的ip
        final String[] arr = ip.split(",");
        for (final String str : arr) {
            if (!"unknown".equalsIgnoreCase(str))
            {
                ip = str;
                break;
            }
        }
        return ip;
    }
}
