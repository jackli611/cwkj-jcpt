package com.cwkj.personalloan.controller.etc;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.personalloan.controller.base.ShareLinkView;
import com.cwkj.personalloan.controller.colourlife.ColourlifeController;
import com.cwkj.personalloan.controller.sms.SmsComponent;
import com.cwkj.scf.model.etc.EtcProfileDo;
import com.cwkj.scf.model.shares.ShareUserDo;
import com.cwkj.scf.model.sms.SMSBusinessType;
import com.cwkj.scf.service.etc.EtcProfileService;
import com.cwkj.scf.service.shares.ShareUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * ETC数据采集
 */
@Controller
public class EtcController extends BaseAction {


    private static Logger logger= LoggerFactory.getLogger(EtcController.class);

    @Autowired
    private EtcProfileService etcProfileService;
    @Autowired
    private SmsComponent smsComponent;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private ShareUserService shareUserService;


    /**
     * 基本信息采集
     * @param request
     * @return
     */
    @RequestMapping(value="/etc/profile.do", method={RequestMethod.GET})
    public ModelAndView profile_methodGet(HttpServletRequest request)
    {
        ModelAndView view=new ModelAndView("etc/etc_profile");
        try{


        }catch (Exception ex)
        {
            logger.error("ETC基本信息采集异常",ex);
            setPromptException(view,ex);
        }
        return view;
    }

    /**
     * 基本信息采集
     * @param request
     * @param etcProfile
     * @return
     */
    @RequestMapping(value="/etc/profile.do", method={RequestMethod.POST})
    public ModelAndView profile_methodPost(HttpServletRequest request, EtcProfileDo etcProfile)
    {
        ModelAndView view=new ModelAndView("etc/etc_profile");
        try{
            view.addObject("etcProfile",etcProfile);
            AssertUtils.isNotBlank(etcProfile.getRealName(),"请输入姓名");
            AssertUtils.isNotBlank(etcProfile.getTelphone(),"请输入手机号");
            AssertUtils.isNotBlank(etcProfile.getCarno(),"请输入车牌号");
            AssertUtils.isNotBlank(etcProfile.getDriverUserName(),"请输入行驶证持有人姓名");
            AssertUtils.isNotBlank(etcProfile.getAddress(),"请输入住址");

            JsonView v = smsComponent.checkSms(request,etcProfile.getTelphone(), SMSBusinessType.resetPsw.name());
            AssertUtils.isTrue(JsonView.CODE_SUCCESS.intValue() == v.getCode().intValue(),"无效的短信验证码");
            etcProfile.setApplyStatus(EtcProfileDo.APPLYSTATUS_EDIT);
            Integer dbRes = etcProfileService.insertEtcProfile(etcProfile);
            AssertUtils.isTrue(dbRes != null && dbRes.intValue() > 0, "提交失败");

            //============以下部分业务：如果当前访问没有登录，那就自动按手机号登录，如果手机号没有注册过账号，自动生成一个账号=========
            SysUserDo sysUser= currentUser();
            if(sysUser==null)
            {
              SysUserDo dbSysUser= sysUserService.findSysUserByUserName(etcProfile.getTelphone());
              if(dbSysUser==null)//手机号没有注册过账号，自动帮其注册一个账号
              {
                  SysUserDo newSysUser=new SysUserDo();
                  newSysUser.setUserName(etcProfile.getTelphone());
                  newSysUser.setRealName(etcProfile.getRealName());
                  newSysUser.setTelphone(etcProfile.getTelphone());
                  String psw = DigestUtils.md5Hex( etcProfile.getTelphone()+"abc123"+ SystemConst.PASS_KEY);
                  newSysUser.setPassword(psw);
                  newSysUser.setRegisterSource(getCurrentChannel(request).getChannelCode());
                  dbRes = sysUserService.insertSysUser(newSysUser);
                  if (dbRes.intValue() > 0) {
                      dbRes=sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_PERSONALLOAN}, newSysUser.getId());
                      //设置推荐分享信息
                      Long shareId= ShareLinkView.getShareIdFromSession();
                      if(shareId!=null) {
                          ShareUserDo shareUserDo =shareUserService.findShareUserById(shareId);
                          if(shareUserDo!=null) {
                              ShareUserDo newShareUser=new ShareUserDo();
                              newShareUser.setUserId(newSysUser.getId());
                              newShareUser.setTelphone(newSysUser.getUserName());
                              newShareUser.setFromUserId(shareUserDo.getUserId());
                              newShareUser.setFromShareId(shareUserDo.getId());
                              newShareUser.setFromTelphone(shareUserDo.getTelphone());
                              newShareUser.setTopShareUserId(shareUserDo.getFromUserId());
                              newShareUser.setTopShareTelphone(shareUserDo.getFromTelphone());
                              newShareUser.setPartnerStatus(ShareUserDo.PARTNERSTATUS_DEFAULT);
                              newShareUser.setOpenPlatform(getCurrentChannel(request).getChannelCode());

                              shareUserService.insertShareUser(newShareUser);
                          }
                      }

                      //注册成功修改登录状态
                      UsernamePasswordToken token = new UsernamePasswordToken(newSysUser.getUserName(), psw);
                      SecurityUtils.getSubject().login(token);
                  }
              }else{//没有登录就设置为已登录
                  UsernamePasswordToken token = new UsernamePasswordToken(dbSysUser.getUserName(), dbSysUser.getPassword());
                  SecurityUtils.getSubject().login(token);
              }

            }
            //================以上业务到止结束================

            view=new ModelAndView("etc/etc_submit");
            view.addObject("etcProfile",etcProfile);
        }catch (Exception ex)
        {
            logger.error("ETC基本信息采集异常",ex);
            setPromptException(view,ex);
        }
        return view;
    }

    @RequestMapping(value="/etc/profileSubmit.do", method={RequestMethod.POST})
    public ModelAndView profileSubmit_methodPost(HttpServletRequest request, Long id,String cardType,String usingCard)
    {
        ModelAndView view=new ModelAndView("etc/etc_submit");
        try {
            view.addObject("id", id);
            view.addObject("cardType", cardType);
            view.addObject("usingCard", usingCard);
            AssertUtils.isNotBlank(cardType, "请选择申请卡类型");
            AssertUtils.isNotBlank(usingCard, "请选择是否有ETC卡");
            AssertUtils.notNull(id, "提交失败，请重新操作");
            EtcProfileDo etcProfile =etcProfileService.findEtcProfileById(id);
            etcProfile.setCardType(cardType);
            etcProfile.setUsingCard(usingCard);
            AssertUtils.isNotBlank(etcProfile.getRealName(), "请输入姓名");
            AssertUtils.isNotBlank(etcProfile.getTelphone(), "请输入手机号");
            AssertUtils.isNotBlank(etcProfile.getCarno(), "请输入车牌号");
            AssertUtils.isNotBlank(etcProfile.getDriverUserName(), "请输入行驶证持有人姓名");
            AssertUtils.isNotBlank(etcProfile.getAddress(), "请输入住址");
            etcProfile.setApplyStatus(EtcProfileDo.APPLYSTATUS_SUBMIT);
            Integer dbRes = etcProfileService.updateCardTypeById(etcProfile);
            AssertUtils.isTrue(dbRes != null && dbRes.intValue() > 0, "提交失败");
            view=new ModelAndView("etc/etc_submit_success");
        }catch (BusinessException ex)
        {
            logger.info("ETC基本信息采集保存失败",ex.getMessage());
            setPromptException(view,ex);
        }catch (Exception ex)
        {
            logger.error("ETC基本信息采集异常",ex);
            setPromptException(view,ex);
        }
        return view;
    }

}
