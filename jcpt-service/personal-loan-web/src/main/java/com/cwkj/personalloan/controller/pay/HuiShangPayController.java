package com.cwkj.personalloan.controller.pay;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.BaseResult;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.personalloan.controller.base.BaseAction;
import com.cwkj.personalloan.controller.base.JsonView;
import com.cwkj.scf.model.loan.SysUserAuth;
import com.cwkj.scf.model.loan.SysUserAuthExample;
import com.cwkj.scf.service.loan.ILoanOrderService;

@RestController
@RequestMapping("/hspay")
public class HuiShangPayController extends BaseAction {
	
    private final static Logger logger = Logger.getLogger(HuiShangPayController.class);

    @Autowired
    private PayCommponent payCommponent;
    
	@Autowired
    private ILoanOrderService loanOrderSercice;

    
	/**
     *	获取支付短信：如果不需要签约需要平台自己发送短信,发送短信之前需要实名
     * @return
     */
    @RequestMapping(value = "/gethspaySmscode.do", method = {RequestMethod.GET,RequestMethod.POST})
    public JsonView<Map> gethspaySmscode(HttpServletRequest request) {
    	SysUserDo currentUser = this.currentUser();
    	JsonView<Map> view = JsonView.successJsonView();
    	try {
    		BaseResult<Map> ret = payCommponent.gethspaySmscode(request,currentUser);
    		logger.info("gethspaySmsCode ret:"+ret.getData());
    		view.setData(ret.getData());
    		if(!ret.resultSuccess()) {
    			view.setCode(JsonView.CODE_FAILE);
    			view.setMsg(ret.getMsg());
	    	}
    	}catch(BusinessException e) {
    		logger.error("徽商渠道支付提交异常:",e);
    		this.setPromptException(view, e);
    	}catch(Exception e) {
    		logger.error("徽商渠道支付提交异常:",e);
    		this.setPromptException(view, e);
    	}
    	return view;
    	
    }
    
    
	/**
     *	支付页面提交:验证短信，发起支付
     * @return
     */
    @RequestMapping(value = "/submitPay.do", method = {RequestMethod.GET,RequestMethod.POST})
    public JsonView<Map> submitPay(HttpServletRequest request) {
    	JsonView<Map> view = JsonView.successJsonView(); 
    	try {
	    	SysUserDo currentUser = this.currentUser();
	    	BaseResult<Map> ret = payCommponent.submitPay(request,currentUser);
	    	if(!ret.resultSuccess()) {
	    		view.setCode(JsonView.CODE_FAILE);
    			view.setMsg(ret.getMsg());
	    	}
    	}catch(BusinessException e) {
    		logger.error("徽商渠道支付提交异常:",e);
    		this.setPromptException(view,e);
    	}catch(Exception e) {
    		logger.error("徽商渠道支付提交异常:",e);
    		this.setPromptException(view, e);
    	}
    	return view;
    }
    
    /**
     *	支付完成页面
     * @return
     */
    @RequestMapping(value = "/payResult.do", method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView payResult(HttpServletRequest request) {
    	ModelAndView view = new ModelAndView("pay/payResult");
    	try {
    		
    	}catch(BusinessException e) {
    		logger.error("徽商渠道支付提交异常:",e);
    		this.setPromptException(view,e);
    	}catch(Exception e) {
    		logger.error("徽商渠道支付提交异常:",e);
    		this.setPromptException(view, e);
    	}
    	return view;
    }
    
    
    /**
     *	根据不同支付通道跳转到通道对应的支付页面
     * @return
     */
    @RequestMapping(value = "/toHuishangPayPage", method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView toHuishangPayPage(HttpServletRequest request) {
    	String channel = payCommponent.getPayChannel();
    	ModelAndView view = new ModelAndView("pay/hsPayPage");
    	String payAmt = request.getParameter("payAmt");
    	view.addObject("payAmt", payAmt);
    	
    	SysUserAuthExample example = new SysUserAuthExample();
    	example.createCriteria().andUseridEqualTo(this.currentUser().getId());
    	SysUserAuth  sysUserAuth = loanOrderSercice.findRealAuth(example);
    	
    	if(null == sysUserAuth) {
    		sysUserAuth = new SysUserAuth();
    	}
    	view.addObject("sysUserAuth", sysUserAuth);
    	return view;
    }
    
    
}
