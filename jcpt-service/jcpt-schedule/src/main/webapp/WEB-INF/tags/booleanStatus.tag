<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="value"  %>
<%@ attribute name="type"  %>

<c:if test="${not empty type }">
<c:if test="${type eq 'option' }">
<option value="">全部</option>
<option value="0" <c:if test="${value == 0}">selected="selected"</c:if> >否</option>
<option value="1" <c:if test="${value == 1 }">selected="selected"</c:if> >是</option>
</c:if>
<c:if test="${type eq 'text' }">
<c:choose>
<c:when test="${value == 0 }">否</c:when>
<c:when test="${value == 1 }">是</c:when>
<c:otherwise>
<c:out value="${value}" />
</c:otherwise>
</c:choose>
</c:if>
</c:if>