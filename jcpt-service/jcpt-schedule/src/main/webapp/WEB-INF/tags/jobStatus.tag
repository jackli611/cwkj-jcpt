<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="value"  %>
<%@ attribute name="type"  %>

<c:if test="${not empty type }">
<c:if test="${type eq 'option' }">
<option value="">全部</option>
<option value="0" <c:if test="${'PAUSED' eq value}">selected="selected"</c:if> >暂停</option>
<option value="1" <c:if test="${'NORMAL' eq value }">selected="selected"</c:if> >正常</option>
<option value="2" <c:if test="${'BLOCKED' eq value }">selected="selected"</c:if> >阻塞</option>
<option value="2" <c:if test="${'ERROR' eq value }">selected="selected"</c:if> >出错</option>
<option value="2" <c:if test="${'COMPLETE' eq value }">selected="selected"</c:if> >完成</option>
</c:if>
<c:if test="${type eq 'text' }">
<c:choose>
<c:when test="${'PAUSED' eq value}">暂停</c:when>
<c:when test="${'NORMAL' eq value }">正常</c:when>
<c:when test="${'BLOCKED' eq value }">阻塞</c:when>
<c:when test="${'ERROR' eq value }">出错</c:when>
<c:when test="${'COMPLETE' eq value }">完成</c:when>
<c:otherwise>
<c:out value="${value}" />
</c:otherwise>
</c:choose>
</c:if>
</c:if>