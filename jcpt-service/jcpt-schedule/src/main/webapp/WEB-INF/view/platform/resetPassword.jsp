<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>

	<div id="datalist">
	<div class="form-panel">
			<form id="form_password" name="form"  action="resetPassword.do"  method="post"  class="form-horizontal">
			<input type="hidden" name="parentId" value="<c:out value="${parentId}" />"> 
			 <div class="form-group form-inline">
				<label for="oldPassword" class="monospaced_lg">原密码:</label>
				 <input type="password" id="oldPassword" name="oldPassword" class="form-control w400" />
				</div>
			  <div class="form-group form-inline">
				<label for="password" class="monospaced_lg">新密码:</label>
				 <input type="password" id="password" name="password" class="form-control w400" />
				</div>
				<div class="form-group form-inline">
				<label for="password2" class="monospaced_lg">确认密码:</label>
				 <input type="password" id="password2" name="password2" class="form-control w400" />
				</div>
				<div class="form-group">
			 <div class="col-sm-offset-3 col-sm-10">
			<input type="button" class="btn btn-primary" onclick="loadData()" id="submitBtn" value="确定" />
			</div></div>
			</form>
		</div>
	 
		 
	</div>
