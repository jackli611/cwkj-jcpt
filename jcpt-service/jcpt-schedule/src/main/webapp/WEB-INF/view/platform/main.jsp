<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>调度管理</title>
<%@ include file="/WEB-INF/view/common/include/mainHeader.jsp" %>
<style type="text/css">
.main-header .navbar-nav>li>a {
    padding-top: 15px;
    padding-bottom: 15px;
}
.navbar-nav>.user-menu>.dropdown-menu {
   
     width: 140px;
}

.navbar-nav>.user-menu>.dropdown-menu li a{
   padding-top:10px;
   padding-bottom:10px;
}
</style>

<script type="text/javascript">
function loadData()
{
	  $.ajax({
		    type: 'post',
		    url: 'resetPassword.do',
		    data: $("#form_password").serialize(),
		    success: function(data) {
		    	$("#datalist").html(data);
		    }
		});
				//$("#datalist").load("resetPassword.do",$("#form_password").serialize());
			 
}
</script>
</head>
<body class="hold-transition skin-yellow-light sidebar-mini" style="font-weight: normal;">
<!-- Site wrapper -->
<div class="wrapper" id="rrapp" v-cloak>
  <header class="main-header">
    <a href="javascript:void(0);" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>调度管理系统</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>调度管理系统</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><i class="fa fa-user"></i>&nbsp;&nbsp; <secure:principal property="realName" emptyProperty="userName"></secure:principal> </span>
            </a>
            <ul class="dropdown-menu">
		 <!--  <li><a  href="../platform/resetPassword.do" data-toggle="modal"><i class="fa fa-lock"></i> &nbsp;修改密码</a></li> -->
          <li><a href="../platform/logout.do"><i class="fa fa-sign-out"></i> &nbsp;退出系统</a></li>
          </ul>
		</ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
<section class="sidebar">
   <ul class="sidebar-menu menu">
    <li  class="treeview" ><a href="javascript:;"><i class="fa fa-sitemap"></i><span>任务管理</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
    <ul  class="treeview-menu" style="display: block;">
    <li><a href="javascript:;" eventdata="../schedule/scheduleGroupList.do"><i class="fa fa-gears"></i><span>任务配置</span></a></li>
    <li><a href="javascript:;" eventdata="../schedule/runningJob.do"><i class="fa fa-play"></i><span>运行中任务</span></a></li>
    <li><a href="javascript:;" eventdata="../schedule/allJob.do"><i class="fa fa-database"></i><span>计划中的任务</span></a></li>
    <li><a href="javascript:;" eventdata="../schedule/testcron.do"><i class="fa fa-question-circle"></i><span>测试Cron</span></a></li>
    </ul>
    </li>
    </ul>
   </section>
  </aside>
  
  <!-- =============================================== -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <iframe id="mainFrame" src="../schedule/runningJob.do" style="border: 0px; padding: 0px; margin: 0px; width:100%;" ></iframe>
  </div>
 
  <div class="control-sidebar-bg"></div>
  
  <!-- 修改密码 -->
<div id="passwordLayer" style="display: none;">
	<form class="form-horizontal">
	<div class="form-group">
		<div class="form-group">
		   	<div class="col-sm-2 control-label">账号</div>
		    <span class="label label-success" style="vertical-align: bottom;">{{user.username}}</span>
		</div>
		<div class="form-group">
		   	<div class="col-sm-2 control-label">原密码</div>
		   	<div class="col-sm-10">
		      <input type="password" class="form-control" v-model="password" placeholder="原密码"/>
		    </div>
		</div>
		<div class="form-group">
		   	<div class="col-sm-2 control-label">新密码</div>
		   	<div class="col-sm-10">
		      <input type="text" class="form-control" v-model="newPassword" placeholder="新密码"/>
		    </div>
		</div>
	</div>
	</form>
</div>

</div>
<!-- ./wrapper -->


 
</body>
</html>