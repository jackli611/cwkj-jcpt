<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp"%>
<link rel="stylesheet" href="../res/css/zui.min.css">
<link rel="stylesheet" href="../res/css/platform.css">
<link rel="stylesheet" href="../res/css/font-awesome.min.css">
<link rel="stylesheet" href="../res/css/AdminLTE.min.css">
<link rel="stylesheet" href="../res/css/all-skins.min.css">
<script src="../res/lib/jquery/jquery.js"></script>
<script src="../res/js/zui.min.js"></script>
<script type="text/javascript" src="../res/js/app.js"></script>
<script type="text/javascript" src="../res/js/menu.js"></script>