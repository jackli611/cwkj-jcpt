<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<c:if test="${not empty error}">
<div  class="alert alert-danger with-icon">
<i class="icon-remove-sign"></i><div class="content">${error.message }</div>
</div>
</c:if>
<c:if test="${not empty promptMsg}">
<div  class="alert alert-info-inverse with-icon">
<i class="icon-info-sign"></i><div class="content">${promptMsg }</div>
</div>
</c:if>