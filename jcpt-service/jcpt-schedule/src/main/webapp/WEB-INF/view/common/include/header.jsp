<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp"%>
<link rel="stylesheet" href="../res/css/zui.min.css">
<link rel="stylesheet" href="../res/css/doc.min.css" />
<link rel="stylesheet" href="../res/css/platform.css">
<script type="text/javascript" src="../res/lib/jquery/jquery.js"></script>
<script type="text/javascript" src="../res/js/zui.min.js"></script>
<script type="text/javascript" src="../res/lib/layer/layer.js"></script>
<script type="text/javascript" src="../res/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="../res/js/highcharts.js"></script>
<script type="text/javascript" src="../res/js/jqPaginator.min.js"></script>
<script type="text/javascript" src="../res/js/jcptTools.js"></script>
 