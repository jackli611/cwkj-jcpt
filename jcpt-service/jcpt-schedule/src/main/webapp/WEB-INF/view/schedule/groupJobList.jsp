<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
<script type="text/javascript">
function deleteScheduleJob(jobId)
{
	 confirmLayer({msg:"确定要删除？",yesFn:function(){
		 loadJsonData({url:"deleteScheduleJob.do?id="+jobId,complete:function(resultData){
			 if(resultData.code)
				 {
				window.location.reload();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
}
$(function(){
	// $("#queryBtn").click(queryStart);
	queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"groupJobHistoryList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 function jobCmd(cmd,jobName,jobGroup)
 {
 	loadJsonData({url:"jobCmd.do?cmd="+cmd+"&jobName="+jobName+"&jobGroup="+jobGroup,complete:function(resultData){
 		if(resultData.code)
		{
			alertLayer(resultData.msg,{end:function(){
				queryStart();
			}});
		}else{
				alertLayer(resultData.msg);
			}
 	}});
 }
</script>
</head>
<body class="page-show"　style="overflow:scroll;">
	<div id="pageContent" >
	<section>
	<header>
	<h3>功能</h3>
	</header>
	<article>
	<div id="activity">
	<button type="button" class="btn btn-primary"  data-iframe="addScheduleJob.do?jobGroup=<c:out value="${jobGroup }" />" data-title="新增任务"  data-toggle="modal" data-size="fullscreen">新增</button>
	</div>
	</article>
	</section>
	<section>
	<header>
	<h3>执行中任务</h3>
	</header>
	<article>
	
	<div  >
		<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>描述</th>
<th>crom表达式</th>
<th>参数</th>
<th>是否并发</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty datalist}">
                            <tr class=gvItem>
                                <td align="center" colspan="5">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty datalist }">
                            <c:forEach items="${datalist}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${status.index+1}"/></td>
<td ><c:out value="${modelObj.description}"/></td>
<td ><c:out value="${modelObj.cronExpression}"/></td>
<td ><c:out value="${modelObj.jobData}"/></td>
<td ><bebe:booleanStatus value="${modelObj.isConcurrent}" type="text"/></td>
 <td>
  <div class="dropdown">
 <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
 <ul class="dropdown-menu">
<li><a href="javascript:deleteScheduleJob('<c:out value="${modelObj.id}"/>');" class="btn-link">删除</a></li>
<li><a onclick="jobCmd('runNow','<c:out value="${modelObj.jobName}"/>','<c:out value="${modelObj.jobGroup}"/>');" class="btn-link">立即执行</a></li>
</ul>
</div>
										</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
                            </c:if>
		</div>
		
	</article>
	</section>
	
	<section>
	<header>
	<h3>运行日志</h3>
	</header>
	<article>
	
	<form id="form_1">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex" />
			<input type="hidden" name="pageSize" value="5" />
			<input type="hidden" name="jobGroup" value="<c:out value="${jobGroup}"/>" />
		</form>
		<div id="datalist">
		
		</div>
		
	</article>
	</section>
	
</body>
</html>