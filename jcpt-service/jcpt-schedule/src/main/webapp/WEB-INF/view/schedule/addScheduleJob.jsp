<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
			<form id="form_1" name="form"  action="addScheduleJob.do"  method="post"  class="form-horizontal">
			<input type="hidden" name="id" value="<c:out value="${scheduleJob.id}" />"> 
			<input type="hidden" id="jobGroup" name="jobGroup" value="<c:out value="${scheduleJob.jobGroup}" />" />		  	
			 
			  <div class="form-group form-inline">
			 <label for="cronExpression"  class="monospaced_lg col-sm-2">crom表达式:</label>
			 <div class="col-sm-10">
			 <input type="text" class="form-control w400" id="cronExpression" name="cronExpression"   />
			 </div>
			 </div>
			 
			 <div class="form-group form-inline">
			 <label for="description"  class="monospaced_lg col-sm-2">描述:</label>
			 <div class="col-sm-10">
			 <input id="description" name="description" class="form-control" />
			 </div>
			 </div>
			 <div class="form-group form-inline">
			 <label for="description"  class="monospaced_lg col-sm-2">参数:</label>
			 <div class="col-sm-10">
			 <input id="jobData" name="jobData" class="form-control" />
			 </div>
			 </div>
			 
			 
			  <div class="form-group form-inline">
			 <label for="isConcurrent"  class="monospaced_lg col-sm-2">是否并发:</label>
			 <div class="col-sm-10">
			 <select id="" name="isConcurrent">
			 <option value="0">否</option>
			 <option value="1">是</option>
			 </select>
			 </div>
			 </div>
			  
			 <div class="form-group">
			 <div class="col-sm-offset-2 col-sm-10">
			<input type="submit" class="btn btn-primary"   id="submitBtn" value="保存" />
			</div></div>
			</form>
		</div>
		<div id="statictable">
		</div>
		 
	</div>

</body>
</html>