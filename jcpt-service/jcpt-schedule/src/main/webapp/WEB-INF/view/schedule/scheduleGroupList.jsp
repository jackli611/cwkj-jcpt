<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
 <style type="text/css">
 .repaytype22{
 background-color: #F33;
 color:#fff;
 }
 </style>
 <script type="text/javascript">
 $(function(){
	 queryStart();
 });
 function loadData()
 {
 	loadTableData({id:"datalist",url:"scheduleGroupList.do",data:$("#form_1").serialize()});
 }
 function queryStart()
 {
		 	toPage(1);
		 	return false;
 } 
 
 function toPage(pageIndex)
 {
	 $("#pageIndex").val(pageIndex);
	 loadData();
 }
 
 function deleteScheduleGroup(Id)
 {
	 confirmLayer({msg:"确定要删除？",yesFn:function(){
		 loadJsonData({url:"deleteScheduleGroup.do?Id="+Id,complete:function(resultData){
			 if(resultData.code)
				 {
				 queryStart();
				 }
			 alertLayer(resultData.msg);
		 }});
	 }});
	return false;
 }
 
 </script>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
			<form id="form_1" name="form" onsubmit="return queryStart()"  method="post"  class="form-inline">
			<input type="hidden" name="pageIndex" value="1" id="pageIndex">
			<input type="hidden" name="pageSize" value="20" >
				 <div class="form-group">
			 	<label for="startDate">时间：</label>
			  	<input id="startDate" class="form-control" type="text" name="startDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})" value="<f:formatDate value="${startDate}"  pattern="yyyy-MM-dd"/>"  />-<input type="text" name="endDate" class="form-control"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:'readOnly'})"  value="<f:formatDate value="${endDate}"  pattern="yyyy-MM-dd"/>"  />
			</div>
			
			<input type="submit" class="btn" id="queryBtn" value="查询" />
			<button type="button" class="btn btn-primary"  data-iframe="addScheduleGroup.do" data-title="新增任务"  data-toggle="modal" data-size="fullscreen">新增</button>
			
			</form>
		</div>
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
		<table class="table table-bordered">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
				<th>50</th>
<th>创建时间</th>
<th>更新时间</th>
<th>description</th>
<th>类全称</th>
<th>spring ID</th>
<th>方法名称</th>
<th>是否有效</th>

			</tr>
			</thead>
			<tbody  >
			    
	</tbody>
	</table>
		</div>
	</div>
</body>
</html>