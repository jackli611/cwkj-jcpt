<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
<script type="text/javascript">
function jobCmd(cmd,jobName,jobGroup)
{
	loadJsonData({url:"jobCmd.do?cmd="+cmd+"&jobName="+jobName+"&jobGroup="+jobGroup,complete:function(resultData){
		if(resultData.code)
			{
				alertLayer(resultData.msg,{end:function(){
					window.location.reload();
				}});
			}else{
					alertLayer(resultData.msg);
				}
	}});
}
</script>
</head>
<body>
	<div id="activity">
		<div id="statictable">
		</div>
		<div id="datalist" class="datalist">
		<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>描述</th>
<th>sprint ID</th>
<th>crom表达式</th>
<th>是否并发</th>
　<th>任务状态</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty datalist}">
                            <tr class=gvItem>
                                <td align="center" colspan="7">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty datalist }">
                            <c:forEach items="${datalist}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${status.index+1}"/></td>
<td ><c:out value="${modelObj.description}"/></td>
<td ><c:out value="${modelObj.springId}"/></td>
<td ><c:out value="${modelObj.cronExpression}"/></td>
<td ><xwckj:booleanStatus value="${modelObj.isConcurrent}" type="text"/></td>
<td ><xwckj:jobStatus value="${modelObj.triggerState}" type="text"/></td>
 <td>
 <div class="dropdown">
 <a class="btn-link"   data-toggle="dropdown">操作 <span class="caret"></span></a>
 <ul class="dropdown-menu">
   <c:if test="${'PAUSED' eq  modelObj.triggerState}">
 <li><a onclick="jobCmd('resume','${modelObj.jobName}','${modelObj.jobGroup}')" class="btn-link">恢复</a></li>
</c:if>
<c:if test="${'NORMAL' eq  modelObj.triggerState}">
<li><a onclick="jobCmd('pause','${modelObj.jobName}','${modelObj.jobGroup}')" class="btn-link">暂停</a></li>
<li><a onclick="jobCmd('runNow','${modelObj.jobName}','${modelObj.jobGroup}')" class="btn-link">立即执行</a></li>
</c:if>
  </ul>
</div>
 </td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
                            </c:if>
		</div>
	</div>
</body>
</html>