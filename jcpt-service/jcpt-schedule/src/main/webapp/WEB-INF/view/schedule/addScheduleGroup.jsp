<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@ include file="/WEB-INF/view/common/include/header.jsp" %>
</head>
<body>
	<div id="activity">
	<div class="form-panel">
	<%@ include file="/WEB-INF/view/common/include/promptMessage.jsp" %>
			<form id="form_1" name="form"  action="addScheduleGroup.do"  method="post"  class="form-horizontal">
			<input type="hidden" name="id" value="<c:out value="${id}" />"> 
			 <input type="hidden"  name="methodName" value="run" />
			 
			 <div class="form-group form-inline">
			 <label for="description"  class="monospaced_lg col-sm-2">描述:</label>
			 <div class="col-sm-10">
			 <input id="description" name="description" class="form-control" />
			 </div>
			 </div>
			 
<!-- 			 <div class="form-group form-inline"> -->
<!-- 			 <label for="beanClass"  class="monospaced_lg col-sm-2">类路径:</label> -->
<!-- 			 <div class="col-sm-10"> -->
<!-- 			 <input type="text" class="form-control w400" id="beanClass" name="beanClass" value=""   /> -->
<!-- 			 </div> -->
<!-- 			 </div> -->
			 
			  <div class="form-group form-inline">
			 <label for="springId"  class="monospaced_lg col-sm-2">spring id:</label>
			 <div class="col-sm-10">
			 <input type="text" class="form-control w400" id="springId" name="springId" value="" />
			 </div>
			 </div>
			  
			 <div class="form-group">
			 <div class="col-sm-offset-2 col-sm-10">
			<input type="submit" class="btn btn-primary"   id="submitBtn" value="保存" />
			</div></div>
			</form>
		</div>
		<div id="statictable">
		</div>
		 
	</div>

</body>
</html>