<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/include/loadDataTable.jsp" %>
<table class="table table-bordered table-hover">
		<thead>
			<tr >
				<th style="width:60px; min-width: 60px;">序号</th>
<th>crom表达式</th>
<th>description</th>
<th>类全称</th>
<th>是否同步</th>
<th>spring id</th>
<th>方法名</th>
<th>操作</th>
			</tr>
			</thead>
			<tbody  >
 <c:if test="${empty pagedata.page}">
                            <tr class=gvItem>
                                <td align="center" colspan="8">查询数据为空！</td>
                            </tr>
                            </tbody>
	</table>
                        </c:if>
                        <c:if test="${not empty pagedata.page }">
                            <c:forEach items="${pagedata.page}" var="modelObj" varStatus="status">
                             <tr >
                                    <td ><c:out value="${pagedata.startOfPage+status.index+1}"/></td>
<td ><c:out value="${modelObj.cronExpression}"/></td>
<td ><c:out value="${modelObj.description}"/></td>
<td ><c:out value="${modelObj.beanClass}"/></td>
<td >
<xwckj:booleanStatus value="${modelObj.isConcurrent }" type="text"></xwckj:booleanStatus>
</td>
<td ><c:out value="${modelObj.springId}"/></td>
<td ><c:out value="${modelObj.methodName}"/></td>

                                    <td>
	                                   <a class="btn-link" href="javascript:deleteScheduleJob('<c:out value="${modelObj.id}"/>');">删除</a>
	                                   <a onclick="jobCmd('runNow','<c:out value="${modelObj.jobName}"/>','<c:out value="${modelObj.jobGroup}"/>');" class="btn-link">立即执行</a>
									</td>
                             </tr>
                            </c:forEach>
                             </tbody>
	</table>
	
	 <ul class="pager" id="pager">
    </ul>
       <script type="text/javascript">
<!--
$(function(){
jqPager({id:"pager",totalPages:${pagedata.totalPage},pageIndex:${pagedata.pageIndex},change:toPage});
});
//-->
</script>
                            </c:if>

	
   