
window.alertLayer = function(msg,opt) {
	var opts=$.extend({end:null,icon:-1},opt);
//	layer.alert({dialog: { msg:msg,type:opts.type},border:[0],end:opts.end});
	layer.alert(msg, opts);
}

window.confirmLayer=function(opt)
{
	var opts=$.extend({msg:"",yesTitle:'确定',yesFn:null,noTitle:'取消',noFn:null},opt);
	window.confirmLayerLastIndex=layer.confirm(opts.msg, {
		  btn: [opts.yesTitle,opts.noTitle] //按钮
		}, function(){
			layer.close(window.confirmLayerLastIndex);
			if(opts.yesFn)
				{
				opts.yesFn();
				}
			 },opts.noFn);	
}
 

window.openWindow=function(opt)
{
	var w=$(window).width()-140;
	var h=$(window).height()-100;
	var opts=$.extend({title:"",url:null,width:w,height:h,close:function(index){layer.close(index);}},opt);
	var layerindex=layer.open({
	    type: 2,
	    title:opt.title,
	    area: [opts.width+'px', opts.height+'px'],
	    fix: true,
	    offset: [($(window).height() - opts.height)/2+'px', ''],
	    content: opts.url,
	    end: opts.close
	});
	window.openwindowLastIndex=layerindex;
	return layerindex;
}

window.closeWindow=function(index)
{
	var winIndex=index||window.openwindowLastIndex;
	layer.close(winIndex);
}
window.loading_Layer_Index=null;
function loading_start()
{
	window.loading_Layer_Index=layer.load(0, {shade:  [0.3, '#393D49']});
}
function loading_close()
{
	if(window.loading_Layer_Index!=null)
		{
			window.setTimeout(function(){
//				layer.loadClose();
				layer.close(window.loading_Layer_Index);
			window.loading_Layer_Index=null;
			}, 200);
		}
}
function loadTableData(opt)
{
	var opts=$.extend({id:null,url:null,complete:null,showLoading:true,data:null},opt);
	if(opts.id!=null)
		{
			if(opts.showLoading)
				{
				loading_start();
				}
			  $.ajax({
				    type: 'post',
				    url: opts.url,
				    data: opts.data,
				    success: function(htmldata) {
				    	if(opts.showLoading)
						{
						loading_close();
						}
					if(opts.complete!=null)
						{
						opts.complete(htmldata);
						}
						$("#"+opts.id).html(htmldata);
				    }
				    	
				    });
//			$("#"+opts.id).load(opts.url,opts.data,function (v){
//				if(opts.showLoading)
//					{
//					loading_close();
//					}
//				if(opts.complete!=null)
//					{
//					opts.complete(v);
//					}
//			});
		}
}
function loadJsonData(opt)
{
	var opts=$.extend({url:null,complete:null,showLoading:true,data:null},opt);
	if(opts.url!=null)
		{
			if(opts.showLoading)
			{
				loading_start();
			}
//			$.getJSON(opts.url,opts.data,function(v){
//				if(opts.showLoading)
//				{
//					loading_close();
//				}
//				if(opts.complete!=null)
//				{
//					opts.complete(v);
//				}
//			});
			
			jQuery.ajax({
				type: "POST",
				url: opts.url,
				data: opts.data,
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (v){
					if(opts.showLoading)
					{
						loading_close();
					}
					if(opts.complete!=null)
					{
						opts.complete(v);
					}
				}
				});
		}
}

// 分页

function jqPager(opt)
{
	var opts=$.extend({id:null,totalPages:0,pageSize:10,pageIndex:1,change:null},opt);
	if(opts.totalPages>0)
		{
			$('#'+opts.id).jqPaginator({
			    totalPages:opts.totalPages,
			    visiblePages:opts.pageSize,
			    currentPage: opts.pageIndex,
			    first: '<li class="first"><a href="javascript:void(0);">首页<\/a><\/li>',
			    prev: '<li class="previous"><a href="javascript:void(0);"><i class="arrow arrow2"><\/i>上一页<\/a><\/li>',
			    next: '<li class="next"><a href="javascript:void(0);">下一页<i class="arrow arrow3"><\/i><\/a><\/li>',
			    last: '<li class="last"><a href="javascript:void(0);">末页<\/a><\/li>',
			    page: '<li class="page"><a href="javascript:void(0);">{{page}}<\/a><\/li>',
			    onPageChange: function (num, type) {
			    	if(type=="change")
			    		{
			    			if(opts.change!=null)
			    			{
			    				opts.change(num);
			    			}
			    		}
			       
			    }
			});
		}
}