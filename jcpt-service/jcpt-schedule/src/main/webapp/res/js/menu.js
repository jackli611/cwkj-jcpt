function openmenu(url)
{
	if(url)
		{
		$("#mainFrame").attr("src",url);
		}
}
 
function  setMainFrameSize(index)
{
	  var window_height = $(window).height();
      var window_width = $(window).width();
      var sidebar_width = $(".sidebar").width();
      var content_height = $(".content-wrapper").height();
      var frame_w=window_width;
      if(index==1)
    	  {
	    		frame_w= window_width-sidebar_width;
    	  }else{
    		  if(sidebar_width<228)
        	  {
        	  	frame_w= window_width-228;
        	  }else{
        		  frame_w= window_width-50;
        	  }
    	  }
      
       	$("#mainFrame").css("width",frame_w+"px");
	  	$("#mainFrame").css("min-width",frame_w+"px");
  	$("#mainFrame").css("height",content_height+"px");	
  	$("#mainFrame").css("min-height",content_height+"px");	
}

function afterPageLoad() {
//  $('.main-sidebar .menu').menu();
  $('.main-sidebar .sidebar .sidebar-menu li:not(".treeview") a').click(function() {
	  var $this = $(this);
      openmenu($this.attr("eventdata"));
      $('.main-sidebar .sidebar .sidebar-menu .active').removeClass('active');
      $this.closest('li').addClass('active');
      var parent = $this.closest('.treeview');
      if(parent.length)
      {
          parent.addClass('active');
      }
  });
}
$(function (){afterPageLoad();});