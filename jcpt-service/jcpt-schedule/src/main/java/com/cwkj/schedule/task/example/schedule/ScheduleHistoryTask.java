package com.cwkj.schedule.task.example.schedule;

import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.schedule.service.schedule.ScheduleHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 清理历史记录
 */
@Service("scheduleHistoryTask")
public class ScheduleHistoryTask {

    private static Logger logger= LoggerFactory.getLogger(ScheduleHistoryTask.class);
    @Autowired
    private  ScheduleHistoryService scheduleHistoryService;

    public void run(String args)
    {
        try{
            Date lastTime= DateUtils.addMonth(new Date(),-2);
            scheduleHistoryService.deleteHistory(lastTime);
        }catch (Exception e) {
            logger.error("清理历史记录任务异常",e);
        }
    }
}
