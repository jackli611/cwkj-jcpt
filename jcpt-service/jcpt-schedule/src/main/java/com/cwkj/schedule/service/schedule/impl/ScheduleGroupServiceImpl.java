package com.cwkj.schedule.service.schedule.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.schedule.component.schedule.ScheduleGroupComponent;
import com.cwkj.schedule.model.schedule.ScheduleGroupDo;
import com.cwkj.schedule.service.schedule.ScheduleGroupService;

/**
 * 调度作业组.
 * @author ljc
 * @version  v1.0
 */
@Component("scheduleGroupService")
public class ScheduleGroupServiceImpl  implements ScheduleGroupService{
	
	@Autowired
	private ScheduleGroupComponent scheduleGroupComponent;
	
	/**
	 * 添加调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer insertScheduleGroup(ScheduleGroupDo scheduleGroup)
	{
		return scheduleGroupComponent.insertScheduleGroup(scheduleGroup);
	}
	
	/**
	 * 获取调度作业组数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleGroupDo> queryScheduleGroupList(Map<String,Object> selectItem)
	{
		return scheduleGroupComponent.queryScheduleGroupList(selectItem);
	}

	/**
	 * 获取调度作业组数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleGroupDo> queryScheduleGroupListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return scheduleGroupComponent.queryScheduleGroupListPage(pageIndex,pageSize,selectItem);
	}
	

	/**
	 * 根据Id修改调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer updateScheduleGroupById(ScheduleGroupDo scheduleGroup)
	{
		return scheduleGroupComponent.updateScheduleGroupById(scheduleGroup);
	}

	/**
	 * 根据Id删除调度作业组.
	 * @param Id
	 * @return
	 */
	public Integer deleteScheduleGroupById(String Id)
	{
		return scheduleGroupComponent.deleteScheduleGroupById(Id);
	}

	/**
	 * 根据Id获取调度作业组.
	 * @param Id
	 * @return
	 */
	public ScheduleGroupDo findScheduleGroupById(String Id)
	{
		return scheduleGroupComponent.findScheduleGroupById(Id);
	}	
}
