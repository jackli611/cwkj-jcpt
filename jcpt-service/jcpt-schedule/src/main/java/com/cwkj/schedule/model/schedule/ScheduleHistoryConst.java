package com.cwkj.schedule.model.schedule;


/**
 * ScheduleHistory字典常量
 * @author ljc
 * @version 1.0
 */
public interface ScheduleHistoryConst {
	
	/** 运行结果：成功 */
	public final int RUNRESULT_SUCCESS = 1;
	/** 运行结果：失败 */
	public final int RUNRESULT_FAILED = 2;
	/** 运行结果：没触发 */
	public final int RUNRESULT_MISFIRED = 3;
	/** 运行结果：被拒绝 */
	public final int RUNRESULT_VETOED = 4;

}
