package com.cwkj.schedule.service.schedule.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.schedule.controller.base.BusinessException;
import com.cwkj.schedule.model.schedule.ScheduleJobConst;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;
import com.cwkj.schedule.service.schedule.QuartzJobFactory;
import com.cwkj.schedule.service.schedule.QuartzJobFactoryDisallowConcurrentExecution;
import com.cwkj.schedule.service.schedule.ScheduleService;

/**
 * @author ljc
 * @version 1.0
 */
@Service("scheduleService")
public class ScheduleServiceImpl implements ScheduleService{
	
	private static Logger logger=LoggerFactory.getLogger(ScheduleServiceImpl.class);
	
	@Autowired
	private SchedulerFactoryBean schedulerFactoryBean;

	@Override
	public void addJob(ScheduleJobDo job) {
		if (job == null || !ScheduleJobConst.JOBSTATUS_RUNNING.equals(job.getJobStatus())) {
			return;
		}
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			logger.debug(scheduler + ".......................................................................................add");
			TriggerKey triggerKey = TriggerKey.triggerKey(job.getJobName(), job.getJobGroup());
	
			CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
			AssertUtils.isNull(trigger, "调度任务已存在");
			// 不存在，创建一个
			if (null == trigger) {
				Class clazz = ScheduleJobConst.CONCURRENT_YES.equals(job.getIsConcurrent()) ? QuartzJobFactory.class : QuartzJobFactoryDisallowConcurrentExecution.class;
	
				JobDetail jobDetail = JobBuilder.newJob(clazz).withIdentity(job.getJobName(), job.getJobGroup()).build();
	
				jobDetail.getJobDataMap().put("scheduleJob", job);
	
				CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
	
				trigger = TriggerBuilder.newTrigger().withIdentity(job.getJobName(), job.getJobGroup()).withSchedule(scheduleBuilder).build();
	
				scheduler.scheduleJob(jobDetail, trigger);
			} else {
				
//				// Trigger已存在，那么更新相应的定时设置
//				CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
//	
//				// 按新的cronExpression表达式重新构建trigger
//				trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
//	
//				// 按新的trigger重新设置job执行
//				scheduler.rescheduleJob(triggerKey, trigger);
			}
		}catch (BusinessException e) {
			throw e;
		}catch (SchedulerException e) {
			logger.error("添加调度任务异常",e);
			throw new BusinessException(e);
		}
		
	}
	
	@Override
	public void pauseJob(String jobName,String jobGroup) {
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			JobKey jobKey = JobKey.jobKey(jobName,jobGroup);
		scheduler.pauseJob(jobKey);
		}catch (SchedulerException e) {
			logger.error("暂停任务异常",e);
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void resumeJob(String jobName,String jobGroup) {
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
			scheduler.resumeJob(jobKey);
		}catch (SchedulerException e) {
			logger.error("恢复任务异常",e);
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void runAJobNow(String jobName,String jobGroup) {
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
			scheduler.triggerJob(jobKey);
		}catch (SchedulerException e) {
			logger.error("立即执行任务异常",e);
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public void updateJobCron(String jobName,String jobGroup,String cronExpression) {
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);
			CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
			trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
			scheduler.rescheduleJob(triggerKey, trigger);
		}catch (SchedulerException e) {
			logger.error("更新Cron表达式异常",e);
			throw new BusinessException(e);
		}
	}

 
	@Override
	public void deleteJob(String jobName,String jobGroup) {
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
			scheduler.deleteJob(jobKey);
		}catch (SchedulerException e) {
			logger.error("删除任务异常",e);
			throw new BusinessException(e);
		}
	}
	
	

	@Override
	public List<ScheduleJobDo> getRunningJob() {
		List<ScheduleJobDo> jobList = new ArrayList<ScheduleJobDo>();
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			List<JobExecutionContext> executingJobs = scheduler.getCurrentlyExecutingJobs();
			for (JobExecutionContext executingJob : executingJobs) {
				ScheduleJobDo job = new ScheduleJobDo();
				JobDetail jobDetail = executingJob.getJobDetail();
				JobKey jobKey = jobDetail.getKey();
				Trigger trigger = executingJob.getTrigger();
				job.setJobName(jobKey.getName());
				job.setJobGroup(jobKey.getGroup());
				job.setDescription("触发器:" + trigger.getKey());
				Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
				job.setJobStatus(triggerState.name());
				if (trigger instanceof CronTrigger) {
					CronTrigger cronTrigger = (CronTrigger) trigger;
					String cronExpression = cronTrigger.getCronExpression();
					job.setCronExpression(cronExpression);
				}
				jobList.add(job);
			}
		}catch (SchedulerException e) {
			logger.error("获取调度执行中的任务列表异常",e);
			throw new BusinessException(e);
		}
		return jobList;
	}

	@Override
	public List<ScheduleJobDo> getAllJob() {
		List<ScheduleJobDo> jobList = new ArrayList<ScheduleJobDo>();
		try{
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
			Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
			for (JobKey jobKey : jobKeys) {
				List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
				for (Trigger trigger : triggers) {
					ScheduleJobDo job = new ScheduleJobDo();
					job.setJobName(jobKey.getName());
					job.setJobGroup(jobKey.getGroup());
					job.setDescription("触发器:" + trigger.getKey());
					Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
					job.setJobStatus(triggerState.name());
					if (trigger instanceof CronTrigger) {
						CronTrigger cronTrigger = (CronTrigger) trigger;
						String cronExpression = cronTrigger.getCronExpression();
						job.setCronExpression(cronExpression);
					}
					jobList.add(job);
				}
			}
		}catch (SchedulerException e) {
			logger.error("获取调度计划中的任务列表异常",e);
			throw new BusinessException(e);
		}
		return jobList;
	}

}
