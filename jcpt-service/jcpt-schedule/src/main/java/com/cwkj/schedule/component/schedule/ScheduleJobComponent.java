package com.cwkj.schedule.component.schedule;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;

/**
 * 调度任务.
 * @author ljc
 * @version  v1.0
 */
public interface ScheduleJobComponent {
	
	/**
	 * 添加调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer insertScheduleJob(ScheduleJobDo scheduleJob);
	
	/**
	 * 获取调度任务数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleJobDo> queryScheduleJobList(Map<String,Object> selectItem);

	/**
	 * 获取调度任务数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleJobDo> queryScheduleJobListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer updateScheduleJobById(ScheduleJobDo scheduleJob);

	/**
	 * 根据Id删除调度任务.
	 * @param Id
	 * @return
	 */
	public Integer deleteScheduleJobById(String Id);

	/**
	 * 根据Id获取调度任务.
	 * @param Id
	 * @return
	 */
	public ScheduleJobDo findScheduleJobById(String Id);	
	
	/**
	 * 根据任务名称和组获取任务
	 * @param jobName
	 * @param jobGroup
	 * @return
	 */
	public ScheduleJobDo findScheduleJobByNameAndGroup(String jobName,String jobGroup);
}
