package com.cwkj.schedule.service.schedule;

import java.util.List;
import java.util.Map;

import org.quartz.SchedulerException;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;

/**
 * 调度任务.
 * @author ljc
 * @version  v1.0
 */
public interface ScheduleJobService {
	
	/**
	 * 添加调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer insertScheduleJob(ScheduleJobDo scheduleJob);
	
	/**
	 * 获取调度任务数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleJobDo> queryScheduleJobList(Map<String,Object> selectItem);

	/**
	 * 获取调度任务数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleJobDo> queryScheduleJobListPage(Long pag正在运行的jobeIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据JobId修改调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer updateScheduleJobById(ScheduleJobDo scheduleJob);

	/**
	 * 根据JobId删除调度任务.
	 * @param jobId
	 * @return
	 */
	public Integer deleteScheduleJobById(String jobId);

	/**
	 * 根据JobId获取调度任务.
	 * @param jobId
	 * @return
	 */
	public ScheduleJobDo findScheduleJobById(String jobId);	
	
	/**
	 * 添加任务
	 * @param job
	 * @throws SchedulerException
	 */
	public void addJob(ScheduleJobDo job) throws SchedulerException;
	
	/**
	 * 修改状态
	 * @param jobId
	 * @param cmd
	 * @throws SchedulerException
	 */
	public void changeStatus(String jobId, String cmd) throws SchedulerException;
	
	/**
	 * 删除任务
	 * @param scheduleJob
	 * @throws SchedulerException
	 */
	public void deleteJob(ScheduleJobDo scheduleJob) throws SchedulerException;
	
	/**
	 * 正在执行的任务
	 * @return
	 * @throws SchedulerException
	 */
	public List<ScheduleJobDo> getRunningJob() throws SchedulerException;
	
	/**
	 * 所有任务
	 * @return
	 * @throws SchedulerException
	 */
	public List<ScheduleJobDo> getAllJob() throws SchedulerException;
	
	/**
	 * 任务列表
	 * @param jobGroup
	 * @return
	 */
	public List<ScheduleJobDo> getGroupJob(String jobGroup);
	
	/**
	 * 立即执行一项任务（任务组中的任务）
	 * @param jobGroup
	 */
	public Boolean runOneJobBySpringId(String springId);
}
