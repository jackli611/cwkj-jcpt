package com.cwkj.schedule.controller.base;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.StringUtil;

/**
 * JSON输出
 * @author ljc
 * @version 1.0
 * @param <T>
 * 数据类型
 */
public class JsonView<T> extends AbstractView{

	@Override
	protected void renderMergedOutputModel(Map<String, Object> arg0, HttpServletRequest arg1, HttpServletResponse arg2)
			throws Exception {
		  arg2.setContentType("text/json; charset=UTF-8");  
	        PrintWriter out = arg2.getWriter();  
	        out.print(this.toJsonString());		
	}
	
	private Integer code;
	private String msg;
	private T data;
	public final static Integer CODE_SUCCESS=1;
	public final static Integer CODE_FAILE=0;
	
	public JsonView()
	{
		super();
	}
	
	/**
	 * JSON格式对象
	 * @param code
	 * 结果编号
	 * @param msg
	 * 结果信息
	 */
	public JsonView(Integer code,String msg)
	{
		super();
		this.setCode(code);
		this.setMsg(msg);
	}
	
	 
	
	/**
	 * 结果编号
	 * @param code
	 */
	public void setCode(Integer code)
	{
		this.code=code;
	}
	
	/**
	 * 结果编号
	 * @return
	 */
	public Integer getCode()
	{
		return this.code;
	}
	
	/**
	 * 结果信息
	 * @param msg
	 */
	public void setMsg(String msg)
	{
		this.msg=msg;
	}
	
	/**
	 * 结果信息
	 * @return
	 */
	public String getMsg()
	{
		return this.msg;
	}
	
	/**
	 * 返回值
	 * @param data
	 */
	public void setData(T data)
	{
		this.data=data;
	}
	
	/**
	 * 返回值
	 * @return
	 */
	public T getData()
	{
		return this.data;
	}
	
	/**
	 * 返回JSON格式字符串
	 * 格式如下：
	 * {"code":1,"msg":"消息","data":返回值}
	 * @return
	 */
	public String toJsonString()
	{
		StringBuilder result=new StringBuilder("{\"code\":");
		if(this.code==null)
		{
			result.append(CODE_FAILE);
		}else{
			result.append(this.code);
		}
		result.append(",\"msg\":\"");
		if(StringUtil.isNotEmpty(this.msg))
		{
			result.append(StringUtil.toJsValue(this.msg));
		} 
		result.append("\"");
		if(this.data!=null)
		{
			result.append(",\"data\":");
			if(this.data instanceof String)
			{
				result.append("\"").append(StringUtil.toJsValue(String.valueOf(this.data))).append("\"");
			}else{
				result.append(JSONObject.toJSON(this.data));
			}
		}
		result.append("}");
		return result.toString();
	}

}
