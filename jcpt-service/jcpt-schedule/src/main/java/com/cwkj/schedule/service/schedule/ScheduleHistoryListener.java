package com.cwkj.schedule.service.schedule;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.TriggerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cwkj.schedule.model.schedule.ScheduleHistoryConst;
import com.cwkj.schedule.model.schedule.ScheduleHistoryDo;

/**
 * @author ljc
 * @version 1.0
 */
public class ScheduleHistoryListener implements JobListener, TriggerListener{

	private static Logger logger=LoggerFactory.getLogger(ScheduleHistoryListener.class);
	private String name="scheduleHistoryListener";
	/** 调度历史管理服务	 */
	private ScheduleHistoryService scheduleHistoryService;
	
	@Override
	public void triggerFired(Trigger trigger, JobExecutionContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void triggerMisfired(Trigger trigger) {
		ScheduleHistoryDo history=new ScheduleHistoryDo();
		history.setFireTime(new Date());
		history.setRunTime(0L);
		history.setJobGroup(trigger.getJobKey().getGroup());
		history.setJobName(trigger.getJobKey().getName());
		history.setRunResult(ScheduleHistoryConst.RUNRESULT_MISFIRED);
		try {
			scheduleHistoryService.insertScheduleHistory(history);
		} catch (Exception e) {
			logger.error("############################# insert schedule history failed #############################");
			logger.error("保存调度作业历史记录异常",e);
			logger.error("########################################################################################");
		}
	}

	@Override
	public void triggerComplete(Trigger trigger, JobExecutionContext context,
			CompletedExecutionInstruction triggerInstructionCode) {
		
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		Trigger trigger = context.getTrigger();
		ScheduleHistoryDo history=new ScheduleHistoryDo();
		history.setFireTime(context.getFireTime());
		history.setRunTime(context.getJobRunTime());
		history.setJobGroup(trigger.getJobKey().getGroup());
		history.setJobName(trigger.getJobKey().getName());
		history.setRunResult(ScheduleHistoryConst.RUNRESULT_VETOED);
		try {
			scheduleHistoryService.insertScheduleHistory(history);
		} catch (Exception e) {
			logger.error("############################# insert schedule history failed #############################");
			logger.error("保存调度作业历史记录异常",e);
			logger.error("########################################################################################");
		}
		
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		Trigger trigger = context.getTrigger();
		JobDetail jobDetail = context.getJobDetail();
		
		ScheduleHistoryDo history=new ScheduleHistoryDo();
		history.setFireTime(context.getFireTime());
		history.setRunTime(context.getJobRunTime());
		history.setJobGroup(trigger.getJobKey().getGroup());
		history.setJobName(trigger.getJobKey().getName());
		
		if(jobException !=null)
		{
			history.setRunResult(ScheduleHistoryConst.RUNRESULT_FAILED);
			// 构建报告
			StringBuilder report = new StringBuilder();
			// 显示dataMap
			JobDataMap dataMap = context.getMergedJobDataMap();
			report.append("mergedJobDataMap:\r\n");
			for (Object key : dataMap.keySet()) {
				report.append(key).append(" = ").append(dataMap.get(key));
				report.append("\r\n");
			}
			report.append("\r\n\r\n");
			// 显示堆栈信息
			StringWriter stackTrace = new StringWriter();
			jobException.printStackTrace(new PrintWriter(stackTrace));
			report.append("stackTrace:\r\n").append(stackTrace);
			history.setReport(report.toString());
		}else{
			history.setRunResult(ScheduleHistoryConst.RUNRESULT_SUCCESS);
			StringBuilder report = new StringBuilder();
			// 显示运行结果
			report.append("result:\r\n").append(context.getResult());
			history.setReport(report.toString());
		}
		try {
			scheduleHistoryService.insertScheduleHistory(history);
		} catch (Exception e) {
			logger.error("############################# insert schedule history failed #############################");
			logger.error("保存调度作业历史记录异常",e);
			logger.error("########################################################################################");
		}
		
	}

	/**  
	 * 调度历史管理服务  
	 * @return
	 */
	public ScheduleHistoryService getScheduleHistoryService() {
		return scheduleHistoryService;
	}
	

	/**  
	 * 调度历史管理服务  
	 * @param scheduleHistoryService 调度历史管理服务  
	 */
	public void setScheduleHistoryService(ScheduleHistoryService scheduleHistoryService) {
		this.scheduleHistoryService = scheduleHistoryService;
	}
	

	
}
