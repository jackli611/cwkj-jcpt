package com.cwkj.schedule.model.schedule;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 调度任务.
 * @author ljc
 * @version  v1.0
 */
@Alias("scheduleJobDo")
public class ScheduleJobDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 50. */
	private String id;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;

	/** 任务名称. */
	private String jobName;

	/** 组名称. */
	private String jobGroup;

	/** 作业状态. */
	private String jobStatus;

	/** crom表达式. */
	private String cronExpression;

	/** description. */
	private String description;

	/** 类全称. */
	private String beanClass;

	/** 是否并发. */
	private String isConcurrent;

	/** spring ID. */
	private String springId;

	/** 方法名称. */
	private String methodName;

	/** 是否有效. */
	private Integer status;

	/**
	 * 执行状态
	 */
	private String triggerState;
	
	/** 执行参数 */
	private String jobData;

	/**
	 * 50.
	 * @param id
	 * 50
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 50.
	 * @return 50
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 任务名称.
	 * @param jobName
	 * 任务名称
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * 任务名称.
	 * @return 任务名称
	 */
	public String getJobName() {
		return this.jobName;
	}

	/**
	 * 组名称.
	 * @param jobGroup
	 * 组名称
	 */
	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	/**
	 * 组名称.
	 * @return 组名称
	 */
	public String getJobGroup() {
		return this.jobGroup;
	}

	/**
	 * 作业状态.
	 * @param jobStatus
	 * 作业状态
	 */
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	/**
	 * 作业状态.
	 * @return 作业状态
	 */
	public String getJobStatus() {
		return this.jobStatus;
	}

	/**
	 * crom表达式.
	 * @param cronExpression
	 * crom表达式
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	/**
	 * crom表达式.
	 * @return crom表达式
	 */
	public String getCronExpression() {
		return this.cronExpression;
	}

	/**
	 * description.
	 * @param description
	 * description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * description.
	 * @return description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * 类全称.
	 * @param beanClass
	 * 类全称
	 */
	public void setBeanClass(String beanClass) {
		this.beanClass = beanClass;
	}

	/**
	 * 类全称.
	 * @return 类全称
	 */
	public String getBeanClass() {
		return this.beanClass;
	}

	/**
	 * 是否并发.
	 * @param isConcurrent
	 * 是否并发
	 */
	public void setIsConcurrent(String isConcurrent) {
		this.isConcurrent = isConcurrent;
	}

	/**
	 * 是否并发.
	 * @return 是否并发
	 */
	public String getIsConcurrent() {
		return this.isConcurrent;
	}

	/**
	 * spring ID.
	 * @param springId
	 * spring ID
	 */
	public void setSpringId(String springId) {
		this.springId = springId;
	}

	/**
	 * spring ID.
	 * @return spring ID
	 */
	public String getSpringId() {
		return this.springId;
	}

	/**
	 * 方法名称.
	 * @param methodName
	 * 方法名称
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * 方法名称.
	 * @return 方法名称
	 */
	public String getMethodName() {
		return this.methodName;
	}

	/**
	 * 是否有效.
	 * @param status
	 * 是否有效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 是否有效.
	 * @return 是否有效
	 */
	public Integer getStatus() {
		return this.status;
	}
	
	


	/**
	 * 执行状态
	 * @return triggerState
	 */
	public String getTriggerState() {
		return triggerState;
	}

	/**
	 * 执行状态
	 * @param triggerState triggerState
	 */
	public void setTriggerState(String triggerState) {
		this.triggerState = triggerState;
	}
	
	/**
	 * 执行参数
	 * @return 
	 */
	public String getJobData() {
		return jobData;
	}

	/**
	 * 执行参数
	 * @param jobData 
	 */
	public void setJobData(String jobData) {
		this.jobData = jobData;
	}
	

	@Override
	public String toString()
	{
		return "ScheduleJobDo ["+",id="+id
+",createTime="+createTime
+",updateTime="+updateTime
+",jobName="+jobName
+",jobGroup="+jobGroup
+",jobStatus="+jobStatus
+",cronExpression="+cronExpression
+",description="+description
+",beanClass="+beanClass
+",isConcurrent="+isConcurrent
+",springId="+springId
+",methodName="+methodName
+",status="+status
+"]";
	}

}
