package com.cwkj.schedule.model.schedule;

/**
 * @author ljc
 * @version 1.0
 */
public interface ScheduleJobConst {
	
	
	/**
	 * 作业状态：运行中
	 */
	public static final String JOBSTATUS_RUNNING = "1";
	/**
	 * 作业状态：停止运行
	 */
	public static final String JOBSTATUS_NOT_RUNNING = "0";
	/**
	 * 并发状态：是
	 */
	public static final String CONCURRENT_YES = "1";
	/**
	 * 并发状态：否
	 */
	public static final String CONCURRENT_NOT = "0";
	
	/**
	 * 有效状态：有效
	 */
	public final Integer STATUS_YES=1;
	/** 
	 * 有效状态：无效 
	 */
	public final Integer STATUS_NO=0;

}
