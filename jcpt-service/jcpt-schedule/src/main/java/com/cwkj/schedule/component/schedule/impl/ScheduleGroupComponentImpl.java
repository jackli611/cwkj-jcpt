package com.cwkj.schedule.component.schedule.impl;

import java.util.List;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.schedule.component.schedule.ScheduleGroupComponent;
import com.cwkj.schedule.dao.schedule.ScheduleGroupDao;
import com.cwkj.schedule.model.schedule.ScheduleGroupDo;

/**
 * 调度作业组.
 * @author ljc
 * @version  v1.0
 */
@Component("scheduleGroupComponent")
public class ScheduleGroupComponentImpl  implements ScheduleGroupComponent{
	
	@Autowired
	private ScheduleGroupDao scheduleGroupDao;
	
	/**
	 * 添加调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer insertScheduleGroup(ScheduleGroupDo scheduleGroup)
	{
		if(StringUtil.isEmpty(scheduleGroup.getId()))
		{
			scheduleGroup.setId(StringUtil.getUUID32());
		}
		Date now=new Date();
		scheduleGroup.setCreateTime(now);
		scheduleGroup.setUpdateTime(now);
		return scheduleGroupDao.insert(scheduleGroup);
	}
	
	/**
	 * 获取调度作业组数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleGroupDo> queryScheduleGroupList(Map<String,Object> selectItem)
	{
		return scheduleGroupDao.queryList(selectItem);
	}

	/**
	 * 获取调度作业组数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleGroupDo> queryScheduleGroupListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<ScheduleGroupDo> pageBean=new PageDo<ScheduleGroupDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(scheduleGroupDao.queryListPage(selectItem));
		return pageBean;
	}
	

	/**
	 * 根据Id修改调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer updateScheduleGroupById(ScheduleGroupDo scheduleGroup)
	{
		return scheduleGroupDao.updateById(scheduleGroup);
	}

	/**
	 * 根据Id删除调度作业组.
	 * @param Id
	 * @return
	 */
	public Integer deleteScheduleGroupById(String Id)
	{
		return scheduleGroupDao.deleteById(Id);
	}

	/**
	 * 根据Id获取调度作业组.
	 * @param Id
	 * @return
	 */
	public ScheduleGroupDo findScheduleGroupById(String Id)
	{
		return scheduleGroupDao.findById(Id);
	}

	@Override
	public ScheduleGroupDo findScheduleGroupBySpringId(String springId) {
		return scheduleGroupDao.findBySpringId(springId);
	}	
}
