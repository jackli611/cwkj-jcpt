package com.cwkj.schedule.controller.platform;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcptsystem.model.system.SysLoginHistoryDo;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysLoginHistoryService;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcpt.util.VerifyCodeUtil;
import com.cwkj.schedule.controller.base.BaseAction;
import com.cwkj.schedule.controller.base.BusinessException;


/**
 * 
 * @author ljc
 *
 */
@Controller
@RequestMapping("/platform/")
public class LoginController extends BaseAction{
	
	private static Logger logger=LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysLoginHistoryService sysLoginHistoryService;
	
	
	/**
	 * 获取验证码图片和文本(验证码文本会保存在HttpSession中)
	 */
	@RequestMapping("/getVerifyCodeImage.do")
	public void getVerifyCodeImage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//设置页面不缓存
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		String verifyCode = VerifyCodeUtil.generateTextCode(VerifyCodeUtil.TYPE_NUM_ONLY, 4, null);
		//将验证码放到HttpSession里面
//		request.getSession().setAttribute("verifyCode", verifyCode);
		SecurityUtils.getSubject().getSession().setAttribute("verifyCode", verifyCode);
		logger.info("本次生成的验证码为[" + verifyCode + "],已存放到HttpSession中");
		//设置输出的内容的类型为JPEG图像
		response.setContentType("image/jpeg");
		BufferedImage bufferedImage = VerifyCodeUtil.generateImageCode(verifyCode, 90, 30, 3, true, Color.WHITE, Color.BLACK, null);
		//写给浏览器
		ImageIO.write(bufferedImage, "JPEG", response.getOutputStream());
	}
	
	@RequestMapping(value="/login.do", method=RequestMethod.GET)
	public ModelAndView loginInit(HttpServletRequest request){
			ModelAndView model = new ModelAndView("platform/login");
			return model;
	}
	
	/**
	 * 用户登录
	 */
	@RequestMapping(value="/login.do", method=RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request){
			ModelAndView model = new ModelAndView("platform/login");
			
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		//获取HttpSession中的验证码
		//String verifyCode = (String)request.getSession().getAttribute("verifyCode");
		String verifyCode = (String)SecurityUtils.getSubject().getSession().getAttribute("verifyCode");
		//获取用户请求表单中输入的验证码
		String submitCode = WebUtils.getCleanParam(request, "verifyCode");
		logger.info("用户[" + username + "]登录时输入的验证码为[" + submitCode + "],HttpSession中的验证码为[" + verifyCode + "]");
		if (StringUtil.isEmpty(submitCode) || StringUtil.isEmpty(verifyCode) || !verifyCode.equals(submitCode.toLowerCase())){
			request.setAttribute("message_login", "验证码不正确");
			return model;
		}
		
		//记录登记日志
		SysLoginHistoryDo loginHistory=new SysLoginHistoryDo();
		loginHistory.setIp(getIP(request));
		loginHistory.setCreateTime(new Date());
		loginHistory.setUserName(username);
		loginHistory.setUserAgent(request.getHeader("User-Agent"));
		loginHistory.setStatus(1);
		sysLoginHistoryService.insertSysLoginHistory(loginHistory);
				
		String psw=DigestUtils.md5Hex(password+ SystemConst.PASS_KEY); 
		UsernamePasswordToken token = new UsernamePasswordToken(username, psw);
		token.setHost(loginHistory.getIp());
//		token.setRememberMe(true);
//		System.out.println("为了验证登录用户而封装的token为" + ReflectionToStringBuilder.toString(token, ToStringStyle.MULTI_LINE_STYLE));
		//获取当前的Subject
		Subject currentUser = SecurityUtils.getSubject();
		try {
			 
			logger.info("对用户[" + username + "]进行登录验证..验证开始");
			currentUser.login(token);
			
			logger.info("对用户[" + username + "]进行登录验证..验证通过");
			loginHistory.setStatus(2);
			model=new ModelAndView("redirect:main.do");
		}catch(UnknownAccountException uae){
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,未知账户");
			request.setAttribute("message_login", "未知账户");
			loginHistory.setStatus(3);
		}catch(IncorrectCredentialsException ice){
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,错误的凭证");
			request.setAttribute("message_login", "密码不正确");
			loginHistory.setStatus(3);
		}catch(LockedAccountException lae){
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,账户已锁定");
			request.setAttribute("message_login", "账户已锁定");
			loginHistory.setStatus(3);
		}catch(ExcessiveAttemptsException eae){
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,错误次数过多");
			request.setAttribute("message_login", "用户名或密码错误次数过多");
			loginHistory.setStatus(3);
		}catch(AuthenticationException ae){
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,堆栈轨迹如下");
			logger.error("登录验证失败",ae);
			request.setAttribute("message_login", "用户名或密码不正确");
			loginHistory.setStatus(3);
		}finally {
			sysLoginHistoryService.updateStatusById(loginHistory);
		}
		//验证是否登录成功
		if(currentUser.isAuthenticated()){
			logger.info("用户[" + username + "]登录认证通过(这里可以进行一些认证通过后的一些系统参数初始化操作)");
		}else{
			token.clear();
		}
		return model;
	}
	
	
	/**
	 * 用户登出
	 */
	@RequestMapping("/logout.do")
	public String logout(HttpServletRequest request){
		 SecurityUtils.getSubject().logout();
		 return InternalResourceViewResolver.REDIRECT_URL_PREFIX + "/platform/login.do";
	}
	
	@RequestMapping(value="/resetPassword.do", method=RequestMethod.GET)
	public ModelAndView resetPasswordPage()
	{
		ModelAndView model = new ModelAndView("platform/resetPassword");
		return model;
	}
	
	@RequestMapping(value="/resetPassword.do", method=RequestMethod.POST)
	public ModelAndView resetPassword(String oldPassword,String password,String password2)
	{
		ModelAndView model = new ModelAndView("platform/resetPassword");
		try{
			checkEmpty(password, "新密码不能为空");
			checkEmpty(oldPassword,"原密码不能为空");
			if(!password.equals(password2))
			{
				throw new BusinessException("密码不一致");
			}
			SysUserDo currentUser=currentUser();
			if(currentUser==null)
			{
				throw new BusinessException("请先登录");
			}
			currentUser=sysUserService.findSysUserById(currentUser.getId());
			if(currentUser!=null)
			{
				String psw=DigestUtils.md5Hex(oldPassword+ SystemConst.PASS_KEY); 
				if(currentUser.getPassword().equals(psw))
				{
					String newPsw=DigestUtils.md5Hex(password+SystemConst.PASS_KEY);
					sysUserService.resetPassword(currentUser.getId(), newPsw, new Date());
					setPromptMessage(model, "修改密码成功");
				}else{
					throw new BusinessException("原密码不正确");
				}
			}else{
				throw new BusinessException("请先登录");
			}
		}catch (BusinessException e) {
			setPromptException(model, e);
		}catch (Exception e) {
			logger.error("修改密码异常", e);
			setPromptException(model, e);
		}
		return model;
	}
	
	 public  String getIP(HttpServletRequest request) {  
	        String ip =request.getHeader("x-forwarded-for");  
	         if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	             ip =request.getHeader("Proxy-Client-IP");  
	         }  
	         if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	              ip = request.getHeader("WL-Proxy-Client-IP");  
	         }  
	         if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	        	 ip = request.getHeader("X-Real-IP"); 
	         }  
	         if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	             ip = request.getRemoteAddr();  
	         }  
	         
	         // 多个路由时，取第一个非unknown的ip
	         final String[] arr = ip.split(",");
	         for (final String str : arr) {
		         if (!"unknown".equalsIgnoreCase(str))
		        	 {
		        	 	ip = str;
		        	 	break;
		        	 }
	         }
	         return ip;  
	}  
}