package com.cwkj.schedule.model.schedule;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 调度作业组.
 * @author ljc
 * @version  v1.0
 */
@Alias("scheduleGroupDo")
public class ScheduleGroupDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 50. */
	private String id;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;

	/** description. */
	private String description;

	/** 类全称. */
	private String beanClass;

	/** spring ID. */
	private String springId;

	/** 方法名称. */
	private String methodName;

	/** 是否有效. */
	private Integer status;


	/**
	 * 50.
	 * @param id
	 * 50
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 50.
	 * @return 50
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * description.
	 * @param description
	 * description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * description.
	 * @return description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * 类全称.
	 * @param beanClass
	 * 类全称
	 */
	public void setBeanClass(String beanClass) {
		this.beanClass = beanClass;
	}

	/**
	 * 类全称.
	 * @return 类全称
	 */
	public String getBeanClass() {
		return this.beanClass;
	}

	/**
	 * spring ID.
	 * @param springId
	 * spring ID
	 */
	public void setSpringId(String springId) {
		this.springId = springId;
	}

	/**
	 * spring ID.
	 * @return spring ID
	 */
	public String getSpringId() {
		return this.springId;
	}

	/**
	 * 方法名称.
	 * @param methodName
	 * 方法名称
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * 方法名称.
	 * @return 方法名称
	 */
	public String getMethodName() {
		return this.methodName;
	}

	/**
	 * 是否有效.
	 * @param status
	 * 是否有效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 是否有效.
	 * @return 是否有效
	 */
	public Integer getStatus() {
		return this.status;
	}


	@Override
	public String toString()
	{
		return "ScheduleGroupDo ["+",id="+id
+",createTime="+createTime
+",updateTime="+updateTime
+",description="+description
+",beanClass="+beanClass
+",springId="+springId
+",methodName="+methodName
+",status="+status
+"]";
	}

}
