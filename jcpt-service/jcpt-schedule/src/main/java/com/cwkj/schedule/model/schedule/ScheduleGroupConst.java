package com.cwkj.schedule.model.schedule;

/**
 * 调度组常量
 * @author ljc
 * @version 1.0
 */
public interface ScheduleGroupConst {

	/**
	 * 有效状态：有效
	 */
	public final Integer STATUS_YES=1;
	/** 
	 * 有效状态：无效 
	 */
	public final Integer STATUS_NO=0;
}
