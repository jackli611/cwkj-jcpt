package com.cwkj.schedule.service.schedule.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.schedule.base.spring.SpringUtils;
import com.cwkj.schedule.component.schedule.ScheduleGroupComponent;
import com.cwkj.schedule.component.schedule.ScheduleJobComponent;
import com.cwkj.schedule.controller.base.BusinessException;
import com.cwkj.schedule.model.schedule.ScheduleGroupDo;
import com.cwkj.schedule.model.schedule.ScheduleJobConst;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;
import com.cwkj.schedule.service.schedule.QuartzJobFactory;
import com.cwkj.schedule.service.schedule.QuartzJobFactoryDisallowConcurrentExecution;
import com.cwkj.schedule.service.schedule.ScheduleJobService;

/**
 * 调度任务.
 * @author ljc
 * @version  v1.0
 */
@Service("scheduleJobService")
public class ScheduleJobServiceImpl  implements ScheduleJobService{
	
	private static Logger logger=LoggerFactory.getLogger(ScheduleJobServiceImpl.class);
	
	@Autowired
	private ScheduleJobComponent scheduleJobComponent;
	
	@Autowired
	private SchedulerFactoryBean schedulerFactoryBean;
	
	@Autowired
	private ScheduleGroupComponent scheduleGroupComponent;
	
	/**
	 * 添加调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer insertScheduleJob(ScheduleJobDo scheduleJob)
	{
		try {
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJob.getCronExpression());
		} catch (Exception e) {
			throw new BusinessException("cron表达式有误，不能被解析！");
		}
		Object obj = null;
		try {
			if (StringUtil.isNotEmpty(scheduleJob.getSpringId())) {
				obj = SpringUtils.getBean(scheduleJob.getSpringId());
			} else {
				Class clazz = Class.forName(scheduleJob.getBeanClass());
				obj = clazz.newInstance();
			}
		} catch (Exception e) {
			// do nothing.........
		}
		AssertUtils.notNull(obj,"未找到目标类！");
		 
		Class clazz = obj.getClass();
		Method method = null;
		try {
			method = clazz.getMethod(scheduleJob.getMethodName(), new Class[] {String.class});
		} catch (Exception e) {
			// do nothing.....
		}
		AssertUtils.notNull(method, "未找到目标方法！");
		Integer result= scheduleJobComponent.insertScheduleJob(scheduleJob);
		try{
			addJob(scheduleJob);
		}catch (SchedulerException e) {
			throw new BusinessException(e);
		}
		return result;
	}
	
	/**
	 * 获取调度任务数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleJobDo> queryScheduleJobList(Map<String,Object> selectItem)
	{
		return scheduleJobComponent.queryScheduleJobList(selectItem);
	}

	/**
	 * 获取调度任务数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleJobDo> queryScheduleJobListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return scheduleJobComponent.queryScheduleJobListPage(pageIndex,pageSize,selectItem);
	}
	

	/**
	 * 根据JobId修改调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer updateScheduleJobById(ScheduleJobDo scheduleJob)
	{
		return scheduleJobComponent.updateScheduleJobById(scheduleJob);
	}

	/**
	 * 根据JobId删除调度任务.
	 * @param jobId
	 * @return
	 */
	public Integer deleteScheduleJobById(String jobId)
	{
		return scheduleJobComponent.deleteScheduleJobById(jobId);
	}

	/**
	 * 根据JobId获取调度任务.
	 * @param jobId
	 * @return
	 */
	public ScheduleJobDo findScheduleJobById(String jobId)
	{
		return scheduleJobComponent.findScheduleJobById(jobId);
	}	
	
	/**
	 * 更改任务状态
	 * 
	 * @throws SchedulerException
	 */
	public void changeStatus(String jobId, String cmd) throws SchedulerException {
		ScheduleJobDo job = findScheduleJobById(jobId);
		if (job == null) {
			return;
		}
		if ("stop".equals(cmd)) {
			deleteJob(job);
			job.setJobStatus(ScheduleJobConst.JOBSTATUS_NOT_RUNNING);
		} else if ("start".equals(cmd)) {
			job.setJobStatus(ScheduleJobConst.JOBSTATUS_RUNNING);
			addJob(job);
		}
		updateScheduleJobById(job);
	}
	
	/**
	 * 添加任务
	 * 
	 * @param scheduleJob
	 * @throws SchedulerException
	 */
	public void addJob(ScheduleJobDo job) throws SchedulerException {
		if (job == null || !ScheduleJobConst.JOBSTATUS_RUNNING.equals(job.getJobStatus())) {
			return;
		}

		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		logger.debug(scheduler + ".......................................................................................add");
		TriggerKey triggerKey = TriggerKey.triggerKey(job.getJobName(), job.getJobGroup());

		CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);

		// 不存在，创建一个
		if (null == trigger) {
			Class clazz = ScheduleJobConst.CONCURRENT_YES.equals(job.getIsConcurrent()) ? QuartzJobFactory.class : QuartzJobFactoryDisallowConcurrentExecution.class;

			JobDetail jobDetail = JobBuilder.newJob(clazz).withIdentity(job.getJobName(), job.getJobGroup()).build();

			jobDetail.getJobDataMap().put("scheduleJob", job);
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());

			trigger = TriggerBuilder.newTrigger().withIdentity(job.getJobName(), job.getJobGroup()).withSchedule(scheduleBuilder).build();

			scheduler.scheduleJob(jobDetail, trigger);
		} else {
			// Trigger已存在，那么更新相应的定时设置
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());

			// 按新的cronExpression表达式重新构建trigger
			trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();

			// 按新的trigger重新设置job执行
			scheduler.rescheduleJob(triggerKey, trigger);
		}
	}
	
	/**
	 * 删除一个job
	 * 
	 * @param scheduleJob
	 * @throws SchedulerException
	 */
	public void deleteJob(ScheduleJobDo scheduleJob) throws SchedulerException {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		JobKey jobKey = JobKey.jobKey(scheduleJob.getJobName(), scheduleJob.getJobGroup());
		scheduler.deleteJob(jobKey);

	}
	
	@PostConstruct
	public void init() throws Exception {

//		Scheduler scheduler = schedulerFactoryBean.getScheduler();

		// 这里获取任务信息数据
		List<ScheduleJobDo> jobList =queryScheduleJobList(null);
	
		for (ScheduleJobDo job : jobList) {
			addJob(job);
		}
	}
	
	/**
	 * 获取所有计划中的任务列表
	 * 
	 * @return
	 * @throws SchedulerException
	 */
	public List<ScheduleJobDo> getAllJob() throws SchedulerException {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
		Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
		List<ScheduleJobDo> jobList = new ArrayList<ScheduleJobDo>();
		for (JobKey jobKey : jobKeys) {
			List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
			for (Trigger trigger : triggers) {
				ScheduleJobDo job=scheduleJobComponent.findScheduleJobByNameAndGroup(jobKey.getName(), jobKey.getGroup());
				if(job!=null)
				{
					Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
					job.setTriggerState(triggerState.name());
				}
				jobList.add(job);
			}
		}
		return jobList;
	}
	
	/**
	 * 所有正在运行的job
	 * 
	 * @return
	 * @throws SchedulerException
	 */
	public List<ScheduleJobDo> getRunningJob() throws SchedulerException {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		List<JobExecutionContext> executingJobs = scheduler.getCurrentlyExecutingJobs();
		List<ScheduleJobDo> jobList = new ArrayList<ScheduleJobDo>(executingJobs.size());
		for (JobExecutionContext executingJob : executingJobs) {
			JobDetail jobDetail = executingJob.getJobDetail();
			JobKey jobKey = jobDetail.getKey();
			ScheduleJobDo job=scheduleJobComponent.findScheduleJobByNameAndGroup(jobKey.getName(), jobKey.getGroup());
			if(job!=null)
			{
				Trigger trigger = executingJob.getTrigger();
				Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
				job.setTriggerState(triggerState.name());
			}
			jobList.add(job);
		}
		return jobList;
	}
	
	public List<ScheduleJobDo> getGroupJob(String jobGroup) {
		List<ScheduleJobDo> jobList = new ArrayList<ScheduleJobDo>();
		try{
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		GroupMatcher<JobKey> matcher = GroupMatcher.jobGroupEquals(jobGroup);
		Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
		for (JobKey jobKey : jobKeys) {
			List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
			for (Trigger trigger : triggers) {
				ScheduleJobDo job=scheduleJobComponent.findScheduleJobByNameAndGroup(jobKey.getName(), jobGroup);
				if(job!=null)
				{
					Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
					job.setTriggerState(triggerState.name());
				}
				jobList.add(job);
				
			}
		}
		}catch (SchedulerException e) {
			logger.error("获取计划中任务组异常",e);
			throw new BusinessException(e);
		}
		return jobList;
	}
	
	@Override
	public Boolean runOneJobBySpringId(String springId)
	{
		Boolean result=false;
		try{
			ScheduleGroupDo scheduleGroupDo=scheduleGroupComponent.findScheduleGroupBySpringId(springId);
			AssertUtils.notNull(scheduleGroupDo,"任务不存在");
			String jobGroup=scheduleGroupDo.getId();
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			List<JobExecutionContext> executingJobs = scheduler.getCurrentlyExecutingJobs();
			Trigger.TriggerState triggerState=null;
			for (JobExecutionContext executingJob : executingJobs) {
				JobDetail jobDetail = executingJob.getJobDetail();
				JobKey jobKey = jobDetail.getKey();
				if(jobKey.getGroup().equals(jobGroup))
				{
					Trigger trigger = executingJob.getTrigger();
					triggerState = scheduler.getTriggerState(trigger.getKey());
					result=true;
				}
			}
			if(triggerState==null)
			{
				GroupMatcher<JobKey> matcher = GroupMatcher.jobGroupEquals(jobGroup);
				Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
				JobKey newJobKey=null;
				for (JobKey jobKey : jobKeys) {
					newJobKey=jobKey;
				}
				scheduler.triggerJob(newJobKey);
				result=true;
			}
			
		}catch (SchedulerException e) {
			logger.error("立即执行任务异常",e);
			result=false;
			throw new BusinessException(e);
		}
		return result;
	}
 
}
