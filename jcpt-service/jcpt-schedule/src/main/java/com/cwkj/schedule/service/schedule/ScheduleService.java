package com.cwkj.schedule.service.schedule;

import java.util.List;

import com.cwkj.schedule.model.schedule.ScheduleJobDo;

/**
 * 调度管理
 * @author ljc
 * @version 1.0
 */
public interface ScheduleService {
	
	/**
	 * 添加任务
	 * @param job 
	 */
	public void addJob(ScheduleJobDo job);
	
	
	/**
	 * 暂停任务
	 * @param jobName 任务名称
	 * @param jobGroup 任务组
	 */
	public void pauseJob(String jobName,String jobGroup);
	
	/**
	 * 恢复任务
	 * @param jobName
	 * @param jobGroup
	 */
	public void resumeJob(String jobName,String jobGroup);
	
	/**
	 * 立即执行任务
	 * @param jobName
	 * @param jobGroup
	 */
	public void runAJobNow(String jobName,String jobGroup);
	
	/**
	 * 更新任务Cron表达式
	 * @param jobName
	 * @param jobGroup
	 * @param cronExpression
	 */
	public void updateJobCron(String jobName,String jobGroup,String cronExpression);
	
	/**
	 * 删除任务
	 * @param jobName
	 * @param jobGroup
	 */
	public void deleteJob(String jobName,String jobGroup);
	
	/**
	 * 获取执行中的任务
	 * @return
	 */
	public List<ScheduleJobDo> getRunningJob();
	
	/**
	 * 获取所有任务
	 * @return
	 */
	public List<ScheduleJobDo> getAllJob();

}
