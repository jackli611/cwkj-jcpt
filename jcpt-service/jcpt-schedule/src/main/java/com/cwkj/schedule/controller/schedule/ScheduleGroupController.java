package com.cwkj.schedule.controller.schedule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.schedule.controller.base.BaseAction;
import com.cwkj.schedule.controller.base.BusinessException;
import com.cwkj.schedule.controller.base.JsonView;
import com.cwkj.schedule.model.schedule.ScheduleGroupConst;
import com.cwkj.schedule.model.schedule.ScheduleGroupDo;
import com.cwkj.schedule.service.schedule.ScheduleGroupService;

import javax.servlet.http.HttpServletRequest;

/**
 * 调度作业组.
 * @author ljc
 * @version  v1.0
 */
@Controller
@RequestMapping(value="/schedule/")
public class ScheduleGroupController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(ScheduleGroupController.class);
	
	@Autowired
	private ScheduleGroupService scheduleGroupService;
	
	/**
	 * 调度作业组数据分页
	 * @return
	 */
	@RequestMapping(value="/scheduleGroupList.do",method=RequestMethod.GET)
	public ModelAndView scheduleGroupList_methodGet()
	{
		ModelAndView view=new ModelAndView("schedule/scheduleGroupList");
		return view;
	}
	
	/**
	 * 调度作业组数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/scheduleGroupList.do",method=RequestMethod.POST)
	public ModelAndView scheduleGroupList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("schedule/scheduleGroupList_table");
		try{ 
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<ScheduleGroupDo> pagedata= scheduleGroupService.queryScheduleGroupListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取调度作业组数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增调度作业组
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addScheduleGroup.do",method=RequestMethod.GET)
	public ModelAndView addScheduleGroup_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("schedule/addScheduleGroup");
		try{
			view.addObject("scheduleGroup", null);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加调度作业组异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增调度作业组
	 * @param request
	 * @param scheduleGroup
	 * @return
	 */
	@RequestMapping(value="/addScheduleGroup.do",method=RequestMethod.POST)
	public ModelAndView addScheduleGroup_methodPost(HttpServletRequest request,ScheduleGroupDo scheduleGroup)
	{
		ModelAndView view=new ModelAndView("schedule/addScheduleGroup");
		try{
			scheduleGroup.setStatus(ScheduleGroupConst.STATUS_YES); 
			Integer result= scheduleGroupService.insertScheduleGroup(scheduleGroup);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加调度作业组异常（POST）",e);
		}
		return view;
	}
	
	 /**
	 * 浏览调度作业组
	 * @param request
	 * @param Id
	 * @return
	 */
	@RequestMapping(value="/viewScheduleGroup.do",method=RequestMethod.GET)
	public ModelAndView viewScheduleGroup_methodGet(HttpServletRequest request,String Id)
	{
		ModelAndView view =new ModelAndView("schedule/viewScheduleGroup");
		try{
			ScheduleGroupDo scheduleGroup= scheduleGroupService.findScheduleGroupById(Id);
			view.addObject("scheduleGroup", scheduleGroup);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览调度作业组异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑调度作业组
	 * @param request
	 * @param Id
	 * @return
	 */
	@RequestMapping(value="/editScheduleGroup.do",method=RequestMethod.GET)
	public ModelAndView editScheduleGroup_methodGet(HttpServletRequest request,String Id)
	{
		ModelAndView view =new ModelAndView("schedule/editScheduleGroup");
		try{
			ScheduleGroupDo scheduleGroup= scheduleGroupService.findScheduleGroupById(Id);
			view.addObject("scheduleGroup", scheduleGroup);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑调度作业组异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑调度作业组
	 * @param request
	 * @param scheduleGroup
	 * @return
	 */
	@RequestMapping(value="/editScheduleGroup.do",method=RequestMethod.POST)
	public ModelAndView editScheduleGroup_methodPost(HttpServletRequest request,ScheduleGroupDo  scheduleGroup)
	{
		ModelAndView view =new ModelAndView("schedule/editScheduleGroup");
		try{
			AssertUtils.notEmptyStr(scheduleGroup.getId(), "参数无效");
			Integer result= scheduleGroupService.updateScheduleGroupById(scheduleGroup);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
			view.addObject("scheduleGroup", scheduleGroup);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑调度作业组异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 删除调度作业组
	 * @param request
	 * @param Id
	 * @return
	 */
	@RequestMapping(value="/deleteScheduleGroup.do")
	public JsonView<String> deleteScheduleGroup_methodPost(HttpServletRequest request,String Id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.notEmptyStr(Id, "参数无效");
			Integer result=scheduleGroupService.deleteScheduleGroupById(Id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除调度作业组异常",e);
		}
		return view;
	}
	 
}
