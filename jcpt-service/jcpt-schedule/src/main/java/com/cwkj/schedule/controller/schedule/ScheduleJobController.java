package com.cwkj.schedule.controller.schedule;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.quartz.CronScheduleBuilder;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.Md5Utils;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.schedule.base.spring.SpringUtils;
import com.cwkj.schedule.controller.base.BaseAction;
import com.cwkj.schedule.controller.base.BusinessException;
import com.cwkj.schedule.controller.base.JsonView;
import com.cwkj.schedule.model.schedule.ScheduleGroupDo;
import com.cwkj.schedule.model.schedule.ScheduleHistoryDo;
import com.cwkj.schedule.model.schedule.ScheduleJobConst;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;
import com.cwkj.schedule.service.schedule.ScheduleGroupService;
import com.cwkj.schedule.service.schedule.ScheduleHistoryService;
import com.cwkj.schedule.service.schedule.ScheduleJobService;
import com.cwkj.schedule.service.schedule.ScheduleService;

/**
 * @author ljc
 * @version 1.0
 */
@Controller
@RequestMapping("/schedule/")
public class ScheduleJobController extends BaseAction{

private static Logger logger=LoggerFactory.getLogger(ScheduleJobController.class);
	
	@Autowired
	private ScheduleJobService scheduleJobService;
	@Autowired
	private ScheduleGroupService scheduleGroupService;
	@Autowired
	private ScheduleHistoryService scheduleHistoryService;
	@Autowired
	private ScheduleService scheduleService;
	
	
	/**
	 * 调度任务数据分页
	 * @return
	 */
	@RequestMapping(value="/scheduleJobList.do",method=RequestMethod.GET)
	public ModelAndView scheduleJobList_methodGet()
	{
		ModelAndView view=new ModelAndView("schedule/scheduleJobList");
		return view;
	}
	
	/**
	 * 调度任务数据分页
	 * @param request
	 * @param pageIndex
	 * @param pageSize
	 * @param startDate  
	 * @param endDate  
	 * @return
	 */
	@RequestMapping(value="/scheduleJobList.do",method=RequestMethod.POST)
	public ModelAndView scheduleJobList_methodPost(HttpServletRequest request,Long pageIndex,Integer pageSize,String startDate,String endDate)
	{
		ModelAndView view=new ModelAndView("schedule/scheduleJobList_table");
		try{ 
			 Map<String, Object> selectItem=new HashMap<String, Object>();
			 setDateBetweemToMap(selectItem, startDate, endDate);
			 pageIndex=pageIndex==null?1L:pageIndex;
			 pageSize=pageSize==null?20:pageSize;
			PageDo<ScheduleJobDo> pagedata= scheduleJobService.queryScheduleJobListPage(pageIndex,pageSize, selectItem);
			view.addObject("pagedata",pagedata);
			
		}catch(BusinessException e)
		{
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("获取调度任务数据分页异常", e);
		}
		return view;
	}
	
	/**
	 * 新增调度任务
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/addScheduleJob.do",method=RequestMethod.GET)
	public ModelAndView addScheduleJob_methodGet(HttpServletRequest request,String jobGroup)
	{
		ModelAndView view=new ModelAndView("schedule/addScheduleJob");
		try{
			ScheduleJobDo scheduleJob=new ScheduleJobDo();
			scheduleJob.setJobGroup(jobGroup);
			view.addObject("scheduleJob", scheduleJob);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加调度任务异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 新增调度任务
	 * @param request
	 * @param scheduleJob
	 * @return
	 */
	@RequestMapping(value="/addScheduleJob.do",method=RequestMethod.POST)
	public ModelAndView addScheduleJob_methodPost(HttpServletRequest request,ScheduleJobDo scheduleJob,String jobData)
	{
		ModelAndView view=new ModelAndView("schedule/addScheduleJob");
		try{
			String jobGroup=scheduleJob.getJobGroup();
			AssertUtils.notEmptyStr(jobGroup, "组参数值不能为空");
			ScheduleGroupDo scheduleGroup= scheduleGroupService.findScheduleGroupById(jobGroup);
			AssertUtils.notNull(scheduleGroup, "无效组参数值");
			scheduleJob.setBeanClass(scheduleGroup.getBeanClass());
			scheduleJob.setSpringId(scheduleGroup.getSpringId());
			String jobName= StringUtil.getUUID32();
			scheduleJob.setId(jobName);
			scheduleJob.setJobName(jobName);
			scheduleJob.setMethodName(scheduleGroup.getMethodName());
			scheduleJob.setJobStatus(ScheduleJobConst.JOBSTATUS_RUNNING);
			scheduleJob.setJobData(jobData);
			scheduleJob.setStatus(ScheduleJobConst.STATUS_YES);
			Integer result = scheduleJobService.insertScheduleJob(scheduleJob);
			AssertUtils.isTrue(result==1, "数据库新增记录失败");
			setPromptMessage(view, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("添加调度任务异常（POST）",e);
		}
		return view;
	}
	
	 /**
	 * 浏览调度任务
	 * @param request
	 * @param jobId
	 * @return
	 */
	@RequestMapping(value="/viewScheduleJob.do",method=RequestMethod.GET)
	public ModelAndView viewScheduleJob_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("schedule/viewScheduleJob");
		try{
			ScheduleJobDo scheduleJob= scheduleJobService.findScheduleJobById(id);
			view.addObject("scheduleJob", scheduleJob);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("浏览调度任务异常（GET）",e);
		}
		return view;
	}
	
	 /**
	 * 编辑调度任务
	 * @param request
	 * @param jobId
	 * @return
	 */
	@RequestMapping(value="/editScheduleJob.do",method=RequestMethod.GET)
	public ModelAndView editScheduleJob_methodGet(HttpServletRequest request,String id)
	{
		ModelAndView view =new ModelAndView("schedule/editScheduleJob");
		try{
			ScheduleJobDo scheduleJob= scheduleJobService.findScheduleJobById(id);
			view.addObject("scheduleJob", scheduleJob);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑调度任务异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 编辑调度任务
	 * @param request
	 * @param scheduleJob
	 * @return
	 */
	@RequestMapping(value="/editScheduleJob.do",method=RequestMethod.POST)
	public ModelAndView editScheduleJob_methodPost(HttpServletRequest request,ScheduleJobDo  scheduleJob)
	{
		ModelAndView view =new ModelAndView("schedule/editScheduleJob");
		try{
			AssertUtils.notNull(scheduleJob.getId(), "参数无效");
			Integer result= scheduleJobService.updateScheduleJobById(scheduleJob);
			AssertUtils.isTrue(result.equals(1), "数据库修改失败");
			setPromptMessage(view, "操作成功");
			view.addObject("scheduleJob", scheduleJob);
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("编辑调度任务异常（GET）",e);
		}
		return view;
	}
	
	/**
	 * 删除调度任务
	 * @param request
	 * @param jobId
	 * @return
	 */
	@RequestMapping(value="/deleteScheduleJob.do")
	public JsonView<String> deleteScheduleJob_methodPost(HttpServletRequest request,String id)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			AssertUtils.notNull(id, "参数无效");
			ScheduleJobDo scheduleJob=scheduleJobService.findScheduleJobById(id);
			Integer result=scheduleJobService.deleteScheduleJobById(id);
			AssertUtils.isTrue(result.equals(1),"数据库操作失败");
			scheduleJobService.deleteJob(scheduleJob);
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("删除调度任务异常",e);
		}
		return view;
	}
	
	@RequestMapping("changeJobStatus")
	@ResponseBody
	public JsonView<String> changeJobStatus(HttpServletRequest request, String id, String cmd) {
		JsonView<String>  view = new JsonView<String>();
		try {
			scheduleJobService.changeStatus(id, cmd);
		} catch (Exception e) {
			logger.error("修改启动状态", e);
			setPromptException(view, e);
		}
		return view;
	}
	
	@RequestMapping(value="/runningJob.do",method=RequestMethod.GET)
	public ModelAndView getRunningJob_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("schedule/runningJob");
		try{
			List<ScheduleJobDo> scheduleJobList= scheduleJobService.getRunningJob();
			view.addObject("datalist", scheduleJobList);
		}catch (Exception e) {
			setPromptException(view, e);
		}
		return view;
	}
	
	@RequestMapping(value="/allJob.do",method=RequestMethod.GET)
	public ModelAndView getAllJob_methodGet(HttpServletRequest request)
	{
		ModelAndView view=new ModelAndView("schedule/runningJob");
		try{
			List<ScheduleJobDo> scheduleJobList= scheduleJobService.getAllJob();
			view.addObject("datalist", scheduleJobList);
		}catch (Exception e) {
			setPromptException(view, e);
		}
		return view;
	}
	
	
	@RequestMapping(value="/groupJobList.do",method=RequestMethod.GET)
	public ModelAndView groupJobList_methodGet(HttpServletRequest request,String jobGroup)
	{
		ModelAndView view=new ModelAndView("schedule/groupJobList");
		try{
			view.addObject("jobGroup", jobGroup);
			Map<String, Object> selectItem=new HashMap<String,Object>();
			selectItem.put("jobGroup", jobGroup);
			selectItem.put("status", ScheduleJobConst.STATUS_YES);
//			List<ScheduleJobDo> scheduleJobList= scheduleJobService.queryScheduleJobList(selectItem);
			List<ScheduleJobDo> scheduleJobList= scheduleJobService.getGroupJob(jobGroup);
			view.addObject("datalist", scheduleJobList);
			
		}catch (Exception e) {
			setPromptException(view, e);
		}
		return view;
	}
	 
	/**
	 * @param request
	 * @param jobGroup
	 * @param startDate
	 * @param endDate
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value="/groupJobHistoryList.do")
	public ModelAndView groupJobHistoryList_methodPost(HttpServletRequest request,String jobGroup,String startDate,String endDate,Long pageIndex,Integer pageSize)
	{
		ModelAndView view=new ModelAndView("schedule/scheduleHistoryList_table");
		try{
			view.addObject("jobGroup", jobGroup);
			Map<String, Object> selectItem=new HashMap<String,Object>();
			selectItem.put("jobGroup", jobGroup);
			setDateBetweemToMap(selectItem, startDate, endDate);
			PageDo<ScheduleHistoryDo> pagedata= scheduleHistoryService.queryScheduleHistoryListPage(pageIndex, pageSize, selectItem);
			view.addObject("pagedata", pagedata);
		}catch (Exception e) {
			setPromptException(view, e);
		}
		return view;
	}
	
	@RequestMapping(value="/jobCmd.do")
	public JsonView<String> jobCmd_methodGet(HttpServletRequest request,String cmd,String jobName,String jobGroup)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			logger.info("操作任务：cmd="+cmd);
			AssertUtils.notEmptyStr(cmd, "命令参数错误");
			AssertUtils.notEmptyStr(jobName, "任务名称错误");
			AssertUtils.notEmptyStr(jobGroup, "任务组错误");
			if("runNow".equals(cmd))
			{
				scheduleService.runAJobNow(jobName, jobGroup);
			}else if("resume".equals(cmd))
			{
				scheduleService.resumeJob(jobName, jobGroup);
			}else if("pause".equals(cmd))
			{
				scheduleService.pauseJob(jobName, jobGroup);
			}else {
				AssertUtils.isTrue(false,"操作类型不支持");
			}
			setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("操作任务异常",e);
		}
		return view;
	}
	
	
 
}
