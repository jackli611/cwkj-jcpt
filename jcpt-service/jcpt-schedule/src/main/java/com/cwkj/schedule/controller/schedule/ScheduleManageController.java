package com.cwkj.schedule.controller.schedule;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cwkj.schedule.controller.base.BaseAction;
import com.cwkj.schedule.controller.base.JsonView;

/**
 * 调度管理
 * @author ljc
 * @version 1.0
 */
public class ScheduleManageController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(ScheduleManageController.class);
	
	
	public JsonView<String> scheduleOperate(HttpServletRequest request,String op)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("执行调度操作请求异常",e);
		}
		return view;
	}
}
