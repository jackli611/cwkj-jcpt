package com.cwkj.schedule.dao.schedule;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import com.cwkj.schedule.model.schedule.ScheduleJobDo;

/**
 * 调度任务.
 * @author ljc
 * @version  v1.0
 */
@Repository("scheduleJobDao")
public interface ScheduleJobDao{
	
	/**
	 * 添加调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer insert(ScheduleJobDo scheduleJob);
	
	/**
	 * 获取调度任务数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleJobDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取调度任务数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleJobDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer updateById(ScheduleJobDo scheduleJob);

	/**
	 * 根据Id删除调度任务.
	 * @param Id
	 * @return
	 */
	public Integer deleteById(String Id);

	/**
	 * 根据Id获取调度任务.
	 * @param Id
	 * @return
	 */
	public ScheduleJobDo findById(String Id);
 	
	 
}
