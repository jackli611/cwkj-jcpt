package com.cwkj.schedule.service.schedule;


import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.cwkj.schedule.base.quartz.TaskUtils;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;



/**
 * 计划任务执行处 无状态
 * @author ljc
 * @version 1.0
 */
public class QuartzJobFactory implements Job {
	public final Logger log = Logger.getLogger(this.getClass());

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
//		ScheduleJobDo scheduleJob = (ScheduleJobDo) context.getMergedJobDataMap().get("scheduleJob");
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		ScheduleJobDo scheduleJob = (ScheduleJobDo) dataMap.get("scheduleJob");
		String jobData=scheduleJob.getJobData();
		TaskUtils.invokMethod(scheduleJob,jobData);
	}
}
