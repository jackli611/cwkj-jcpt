package com.cwkj.schedule.service.schedule;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.schedule.model.schedule.ScheduleHistoryDo;

/**
 * 调度作业历史.
 * @author ljc
 * @version  v1.0
 */
public interface ScheduleHistoryService {
	
	/**
	 * 添加调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer insertScheduleHistory(ScheduleHistoryDo scheduleHistory);
	
	/**
	 * 获取调度作业历史数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleHistoryDo> queryScheduleHistoryList(Map<String,Object> selectItem);

	/**
	 * 获取调度作业历史数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleHistoryDo> queryScheduleHistoryListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer updateScheduleHistoryById(ScheduleHistoryDo scheduleHistory);

	/**
	 * 根据Id删除调度作业历史.
	 * @param id
	 * @return
	 */
	public Integer deleteScheduleHistoryById(String id);

	/**
	 * 根据Id获取调度作业历史.
	 * @param id
	 * @return
	 */
	public ScheduleHistoryDo findScheduleHistoryById(String id);

	/**
	 * 消除历史
	 * @param lastTime 清理时间点（截至时间之前的数据删除掉）
	 * @return
			 */
	public Integer deleteHistory(Date lastTime);
}
