package com.cwkj.schedule.controller.base;


import java.text.ParseException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;

/**
 *控制层基类
 *
 * @author ljc
 *
 */
public class BaseAction {
	
	 
	/**
	 * 设置提示业务异常信息
	 * @param view
	 * @param e
	 */
	protected void setPromptException(ModelAndView view,BusinessException e)
	{
		view.addObject("error", e);
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected  <T> void setPromptException(JsonView<T> view,BusinessException e)
	{
		view.setCode(view.CODE_FAILE);
		view.setMsg(e.getMessage());
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected void setPromptException(ModelAndView view,Exception e)
	{
		view.addObject("error",new BusinessException(BusinessException.code_other, e.getMessage()) );
	}
	
	/**
	 * 设置异常信息
	 * @param view
	 * @param e
	 */
	protected  <T> void setPromptException(JsonView<T> view,Exception e)
	{
		view.setCode(view.CODE_FAILE);
		view.setMsg(e.getMessage());
	}
	
	/**
	 * 设置提示信息
	 * @param view
	 * @param msg
	 */
	protected void setPromptMessage(ModelAndView view,String msg)
	{
		view.addObject("promptMsg", msg);
	}
	
	/**
	 * 设置提示信息
	 * @param view
	 * @param code
	 * @param msg
	 */
	protected <T> void setPromptMessage(JsonView<T> view,Integer code, String msg)
	{
		view.setCode(code);
		view.setMsg(msg);
	}
	
	/**
	 * 检查是否空值，
	 * @param obj
	 * @param msg
	 */
	protected void checkEmpty(Object obj,String msg)
	{
		if(obj==null)
		{
			throw new BusinessException(BusinessException.code_argument,msg);
		}
		if(obj instanceof String && StringUtil.isEmpty((String)obj))
		{
			throw new BusinessException(BusinessException.code_argument,msg);
		}
	}
	
	//=========== 提供权限验证方法 ===========
	
	protected SysUserDo currentUser()
	{
		Subject subject = SecurityUtils.getSubject();

		Object user = subject.getSession().getAttribute("currentUser");
		if (user != null) {
			return (SysUserDo) user;
		}
		return null;
		
	}
	/**
	 * 验证是否有权限操作，如果没有抛出BusinessException权限类型异常。
	 * @param code
	 * 权限代码。
	 */
	protected void checkPermission(String code)
	{
		try{
//			if(!isSuperSysUser())//不是超级管理员就下一步验证。
//			{
				SecurityUtils.getSubject().checkPermission(code);	
//			}
		}catch (AuthorizationException e) {
			throw new BusinessException(BusinessException.code_auth,"您无操作权限。");
		}
	}
	
	/**
	 * 验证是否拥有角色，如果没有抛出BusinessException权限类型异常。
	 * @param roleIdentifier
	 * 角色名称。
	 */
	protected void checkRole(String roleIdentifier)
	{
		try{
//			if(!isSuperSysUser())//不是超级管理员就下一步验证。
//			{
				SecurityUtils.getSubject().checkRole(roleIdentifier);
//			}
		}catch (AuthorizationException e) {
			throw new BusinessException(BusinessException.code_auth,"您无操作权限。");
		}
	}
	
	
	
	/**
	 * 判断是否拥有角色。
	 * @param roleIdentifier
	 * 角色名称
	 * @return
	 */
	protected Boolean hasRole(String roleIdentifier)
	{
//		if(isSuperSysUser())//不是超级管理员就下一步验证。
//		{
//			return true;
//		}
		return SecurityUtils.getSubject().hasRole(roleIdentifier);
	}
	
	/**
	 * 判断是否拥有权限代码。
	 * @param code
	 * 权限代码。
	 * @return
	 */
	protected Boolean hasPermission(String code)
	{
//		if(isSuperSysUser())//不是超级管理员就下一步验证。
//		{
//			return true;
//		}
		return SecurityUtils.getSubject().isPermitted(code);
	}

//	/**
//	 * 判断是否是系统超级用户。
//	 * @return
//	 */
//	protected Boolean isSuperSysUser()
//	{
//		return SecurityUtils.getSubject().hasRole(SystemConst.SYSTEM_SUPERROLE);
//	}
	
	

	/**
	 * 设置查询时间范围
	 * @param selectItem
	 * @param startDate
	 * @param endDate
	 */
	protected void setDateBetweemToMap(Map<String, Object> selectItem,String startDate,String endDate)
	{
		try{
		 selectItem.put("startDate", StringUtils.isNotBlank(startDate)?(DateUtils.parse(startDate+" 00:00:00")):null);
		 selectItem.put("endDate", StringUtils.isNotBlank(endDate)?(DateUtils.parse(endDate+" 23:59:59")):null);
		}catch (ParseException e) {
			e.printStackTrace();
		}
	}
	//=========== 提供权限验证方法 end===========
	

	/**
	 * 
	 * getRequestToParamMap:将reuqest参数转换为map. <br>
	 *
	 * @author anxymf
	 * Date:2016年11月21日下午2:50:49 <br>
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, Object> getRequestToParamMap(
			HttpServletRequest request) {
	
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> keyNames = request.getParameterNames();
		if(StringUtil.isEmpty(request.getParameter("pageIndex"))){
			paramMap.put("pageIndex", "1");
		}
		if(StringUtil.isEmpty(request.getParameter("pageSize"))){
			paramMap.put("pageSize","20");
		}
		while (keyNames.hasMoreElements()) {
			String attrName = keyNames.nextElement();
			String attrValue = request.getParameter(attrName);
			if (StringUtils.isNotEmpty(attrValue)) {
				paramMap.put(attrName, attrValue.trim());
			}	
		}
		return paramMap;
	}	
	
}
