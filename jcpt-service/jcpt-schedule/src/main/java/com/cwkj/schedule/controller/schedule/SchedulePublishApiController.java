package com.cwkj.schedule.controller.schedule;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.Md5Utils;
import com.cwkj.schedule.controller.base.BaseAction;
import com.cwkj.schedule.controller.base.BusinessException;
import com.cwkj.schedule.controller.base.JsonView;
import com.cwkj.schedule.service.schedule.ScheduleJobService;

/**
 * 公布的API
 * @author ljc
 * @version 1.0
 */
@Controller
@RequestMapping("/api/")
public class SchedulePublishApiController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(SchedulePublishApiController.class);
	
	@Autowired
	private ScheduleJobService scheduleJobService;
	
	/**
	 * 校验请求参数
	 * @param request
	 * @param signStr
	 */
	private void checkSign(HttpServletRequest request,String signStr)
	{
		String sign=request.getParameter("sign");
		String PASS_KEY=SystemConst.MD5KEY_SCHEDULE;
		String md5Str = Md5Utils.MD5(PASS_KEY + signStr+ PASS_KEY);
		logger.info("请求sign:{},生成md5值：{}",sign,md5Str);
		AssertUtils.isTrue(md5Str.equals(sign),"请求校验不通过");
	}

	@RequestMapping(value="/runNowJobBySpringId.do")
	public JsonView<String> runNowJobBySpringId(HttpServletRequest request,String springId)
	{
		JsonView<String> view=new JsonView<String>();
		try{
			checkSign(request,springId);
			logger.info("立刻执行任务：springId="+springId);
			AssertUtils.notEmptyStr(springId, "参数错误");
 
			Boolean result=scheduleJobService.runOneJobBySpringId(springId);
			if(result)
			{
				setPromptMessage(view, view.CODE_SUCCESS, "操作成功");
			}else{
				setPromptMessage(view, view.CODE_FAILE, "操作失败");
			}
		}catch (BusinessException e) {
			setPromptException(view, e);
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("操作任务异常",e);
		}
		return view;
	}
}
