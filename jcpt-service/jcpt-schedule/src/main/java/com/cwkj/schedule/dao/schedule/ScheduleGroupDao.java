package com.cwkj.schedule.dao.schedule;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import com.cwkj.schedule.model.schedule.ScheduleGroupDo;

/**
 * 调度作业组.
 * @author ljc
 * @version  v1.0
 */
@Repository("scheduleGroupDao")
public interface ScheduleGroupDao{
	
	/**
	 * 添加调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer insert(ScheduleGroupDo scheduleGroup);
	
	/**
	 * 获取调度作业组数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleGroupDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取调度作业组数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleGroupDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer updateById(ScheduleGroupDo scheduleGroup);

	/**
	 * 根据Id删除调度作业组.
	 * @param Id
	 * @return
	 */
	public Integer deleteById(String Id);

	/**
	 * 根据Id获取调度作业组.
	 * @param Id
	 * @return
	 */
	public ScheduleGroupDo findById(String Id);
	
	/**
	 * 根据SpringId获取调度作业组
	 * @param springId
	 * @return
	 */
	public ScheduleGroupDo findBySpringId(String springId);
 	
	 
}
