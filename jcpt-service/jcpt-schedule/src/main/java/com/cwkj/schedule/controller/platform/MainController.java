package com.cwkj.schedule.controller.platform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcptsystem.service.system.SysPermissionService;
import com.cwkj.schedule.controller.base.BaseAction;


/**
 * 平台
 * @author ljc
 *
 */
@Controller
@RequestMapping("/platform/")
public class MainController extends BaseAction{

	private static Logger log=LoggerFactory.getLogger(MainController.class);
	@Autowired
	private SysPermissionService sysPermissionService;
	
	@RequestMapping("/main.do")
	public ModelAndView index(HttpServletRequest request){
		ModelAndView model = new ModelAndView("platform/main");
		return model;
	}
	
	 
	
	 
}
