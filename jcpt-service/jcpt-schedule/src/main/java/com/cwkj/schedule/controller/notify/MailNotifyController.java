package com.cwkj.schedule.controller.notify;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cwkj.jcptsystem.service.notify.NotifyService;
import com.cwkj.jcptsystem.service.notify.MailNotify;
import com.cwkj.jcpt.util.DateUtils;
import com.cwkj.jcpt.util.StringUtil;

/**
 * 邮件通知服务
 * @author ljc
 * @version 1.0
 */
@Controller
@RequestMapping("/mailnotify/")
public class MailNotifyController {

	private static Logger logger=LoggerFactory.getLogger(MailNotifyController.class);
	
	@Autowired
	private NotifyService mailNotifyService;
	
	/**
	 * 测试发送邮件
	 * @param request
	 * @param investDetailId
	 * @return
	 */
	@RequestMapping(value = "/testSendMail.do", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String testSendMail(HttpServletRequest request,Boolean async,String ccList)
	{
		String result=null;
		try{
			Map<String, String> param=new HashMap<String,String>();
			param.put("greeting","Dear All:");
//			param.put("title", "服务器邮件发送测试");
			String content=String.format("恭喜收到这封测试邮件，服务器邮件发送正常(来自jcpt-schedule服务，发送时间:%s)。",DateUtils.format(new Date(),"yyyy年MM月dd日hh时mm分ss秒"));
			param.put("content", content);
			param.put("company", "<div class='line w400'>深圳市彩付宝科技有限公司基础平台部</div><br>此为系统邮件请勿回复");
			MailNotify notify=new MailNotify();
			if(async!=null)
			{
				notify.setAsync(async);
			}else{
				notify.setAsync(true);
			}
			if(StringUtil.isNotBlank(ccList))
			{
				notify.setCcList(ccList);
			}
			notify.setMessageTemplate("mail_template_jcpt.ftl");
			notify.setRecievers("linjincmf@fansfinancial.com");
			notify.setSubject("测试邮件通知");
			notify.setMessage(param);
			mailNotifyService.send(notify);
			result="邮件已发送："+content;
		}catch (Exception e) {
			logger.error("测试发送邮件异常",e);
			result="邮件发送异常";
		}
		return result;
		 
	}
}
