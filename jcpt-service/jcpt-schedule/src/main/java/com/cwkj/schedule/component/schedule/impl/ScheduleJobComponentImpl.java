package com.cwkj.schedule.component.schedule.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.schedule.component.schedule.ScheduleJobComponent;
import com.cwkj.schedule.dao.schedule.ScheduleJobDao;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;

/**
 * 调度任务.
 * @author ljc
 * @version  v1.0
 */
@Component("scheduleJobComponent")
public class ScheduleJobComponentImpl  implements ScheduleJobComponent{
	
	@Autowired
	private ScheduleJobDao scheduleJobDao;
	
	/**
	 * 添加调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer insertScheduleJob(ScheduleJobDo scheduleJob)
	{
		if(StringUtil.isEmpty(scheduleJob.getId()))
		{
			scheduleJob.setId(StringUtil.getUUID32());
		}
		return scheduleJobDao.insert(scheduleJob);
	}
	
	/**
	 * 获取调度任务数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleJobDo> queryScheduleJobList(Map<String,Object> selectItem)
	{
		return scheduleJobDao.queryList(selectItem);
	}

	/**
	 * 获取调度任务数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleJobDo> queryScheduleJobListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<ScheduleJobDo> pageBean=new PageDo<ScheduleJobDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(scheduleJobDao.queryListPage(selectItem));
		return pageBean;
	}
	

	/**
	 * 根据Id修改调度任务.
	 * @param scheduleJob
	 * @return
	 */
	public Integer updateScheduleJobById(ScheduleJobDo scheduleJob)
	{
		return scheduleJobDao.updateById(scheduleJob);
	}

	/**
	 * 根据Id删除调度任务.
	 * @param Id
	 * @return
	 */
	public Integer deleteScheduleJobById(String Id)
	{
		return scheduleJobDao.deleteById(Id);
	}

	/**
	 * 根据Id获取调度任务.
	 * @param Id
	 * @return
	 */
	public ScheduleJobDo findScheduleJobById(String Id)
	{
		return scheduleJobDao.findById(Id);
	}

	@Override
	public ScheduleJobDo findScheduleJobByNameAndGroup(String jobName, String jobGroup) {
		Map<String, Object> selectItem=new HashMap<String,Object>();
		selectItem.put("jobName", jobName);
		selectItem.put("jobGroup", jobGroup);
		List<ScheduleJobDo> list=scheduleJobDao.queryList(selectItem);
		return ( list==null|| list.size()==0?null:list.get(0));
	}	
}
