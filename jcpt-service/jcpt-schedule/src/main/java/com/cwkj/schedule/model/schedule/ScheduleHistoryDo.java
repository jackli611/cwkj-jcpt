package com.cwkj.schedule.model.schedule;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 调度作业历史.
 * @author ljc
 * @version  v1.0
 */
@Alias("scheduleHistoryDo")
public class ScheduleHistoryDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键. */
	private String id;

	/** 触发时间. */
	private Date fireTime;

	/** 运行时间. */
	private Long runTime;

	/** 任务名称. */
	private String jobName;

	/** 任务组. */
	private String jobGroup;

	/** 结果. */
	private Integer runResult;
	
	/** 信息. */
	private String report;


	/**
	 * 主键.
	 * @param id
	 * 主键
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 主键.
	 * @return 主键
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 触发时间.
	 * @param fireTime
	 * 触发时间
	 */
	public void setFireTime(Date fireTime) {
		this.fireTime = fireTime;
	}

	/**
	 * 触发时间.
	 * @return 触发时间
	 */
	public Date getFireTime() {
		return this.fireTime;
	}

	/**
	 * 运行时间.
	 * @param runTime
	 * 运行时间
	 */
	public void setRunTime(Long runTime) {
		this.runTime = runTime;
	}

	/**
	 * 运行时间.
	 * @return 运行时间
	 */
	public Long getRunTime() {
		return this.runTime;
	}

	/**
	 * 任务名称.
	 * @param jobName
	 * 任务名称
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * 任务名称.
	 * @return 任务名称
	 */
	public String getJobName() {
		return this.jobName;
	}

	/**
	 * 任务组.
	 * @param jobGroup
	 * 任务组
	 */
	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	/**
	 * 任务组.
	 * @return 任务组
	 */
	public String getJobGroup() {
		return this.jobGroup;
	}

	/**
	 * 结果.
	 * @param runResult
	 * 结果
	 */
	public void setRunResult(Integer runResult) {
		this.runResult = runResult;
	}

	/**
	 * 结果.
	 * @return 结果
	 */
	public Integer getRunResult() {
		return this.runResult;
	}

	/**  
	 * 信息.  
	 * @return
	 */
	public String getReport() {
		return report;
	}

	/**  
	 * 信息.  
	 * @param report 信息.  
	 */
	public void setReport(String report) {
		this.report = report;
	}
	

	@Override
	public String toString()
	{
		return "ScheduleHistoryDo ["+",id="+id
+",fireTime="+fireTime
+",runTime="+runTime
+",jobName="+jobName
+",jobGroup="+jobGroup
+",runResult="+runResult
+"]";
	}

}
