package com.cwkj.schedule.component.schedule.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.schedule.component.schedule.ScheduleHistoryComponent;
import com.cwkj.schedule.dao.schedule.ScheduleHistoryDao;
import com.cwkj.schedule.model.schedule.ScheduleHistoryDo;

/**
 * 调度作业历史.
 * @author ljc
 * @version  v1.0
 */
@Component("scheduleHistoryComponent")
public class ScheduleHistoryComponentImpl  implements ScheduleHistoryComponent{
	
	@Autowired
	private ScheduleHistoryDao scheduleHistoryDao;
	
	/**
	 * 添加调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer insertScheduleHistory(ScheduleHistoryDo scheduleHistory)
	{
		if(StringUtil.isEmpty(scheduleHistory.getId()))
		{
			scheduleHistory.setId(StringUtil.getUUID32());
		}
		return scheduleHistoryDao.insert(scheduleHistory);
	}
	
	/**
	 * 获取调度作业历史数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleHistoryDo> queryScheduleHistoryList(Map<String,Object> selectItem)
	{
		return scheduleHistoryDao.queryList(selectItem);
	}

	/**
	 * 获取调度作业历史数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleHistoryDo> queryScheduleHistoryListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<ScheduleHistoryDo> pageBean=new PageDo<ScheduleHistoryDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(scheduleHistoryDao.queryListPage(selectItem));
		return pageBean;
	}
	

	/**
	 * 根据Id修改调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer updateScheduleHistoryById(ScheduleHistoryDo scheduleHistory)
	{
		return scheduleHistoryDao.updateById(scheduleHistory);
	}

	/**
	 * 根据Id删除调度作业历史.
	 * @param id
	 * @return
	 */
	public Integer deleteScheduleHistoryById(String id)
	{
		return scheduleHistoryDao.deleteById(id);
	}

	/**
	 * 根据Id获取调度作业历史.
	 * @param id
	 * @return
	 */
	public ScheduleHistoryDo findScheduleHistoryById(String id)
	{
		return scheduleHistoryDao.findById(id);
	}

	@Override
	public Integer deleteHistory(Map<String, Object> selectItem) {
		return scheduleHistoryDao.deleteHistory(selectItem);
	}
}
