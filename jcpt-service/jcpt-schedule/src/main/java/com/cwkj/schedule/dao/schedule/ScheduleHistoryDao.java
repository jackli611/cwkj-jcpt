package com.cwkj.schedule.dao.schedule;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.schedule.model.schedule.ScheduleHistoryDo;

/**
 * 调度作业历史.
 * @author ljc
 * @version  v1.0
 */
@Repository("scheduleHistoryDao")
public interface ScheduleHistoryDao{
	
	/**
	 * 添加调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer insert(ScheduleHistoryDo scheduleHistory);
	
	/**
	 * 获取调度作业历史数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleHistoryDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取调度作业历史数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleHistoryDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer updateById(ScheduleHistoryDo scheduleHistory);

	/**
	 * 根据Id删除调度作业历史.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取调度作业历史.
	 * @param id
	 * @return
	 */
	public ScheduleHistoryDo findById(String id);


	/**
	 * 消除历史
	 * @param selectItem
	 * @return
	 */
	public Integer deleteHistory(Map<String,Object> selectItem);
 	
	 
}
