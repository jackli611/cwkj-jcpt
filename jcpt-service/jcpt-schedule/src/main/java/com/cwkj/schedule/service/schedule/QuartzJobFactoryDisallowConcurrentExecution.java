package com.cwkj.schedule.service.schedule;


import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.cwkj.schedule.base.quartz.TaskUtils;
import com.cwkj.schedule.model.schedule.ScheduleJobDo;



/**
 * 计划任务执行处 有状态,若一个方法一次执行不完下次轮转时则等待改方法执行完后才执行下一次操作
 * @author ljc
 * @version 1.0
 */
@DisallowConcurrentExecution
public class QuartzJobFactoryDisallowConcurrentExecution implements Job {
	public final Logger log = Logger.getLogger(this.getClass());

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
//		ScheduleJobDo scheduleJob = (ScheduleJobDo) context.getMergedJobDataMap().get("scheduleJob");
//		TaskUtils.invokMethod(scheduleJob);
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		ScheduleJobDo scheduleJob = (ScheduleJobDo) dataMap.get("scheduleJob");
		String jobData=scheduleJob.getJobData();
		TaskUtils.invokMethod(scheduleJob,jobData);

	}
}
