package com.cwkj.schedule.task.example;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskExample {
	
	private static Logger logger=LoggerFactory.getLogger(TaskExample.class);
	
	public void run()
	{
		logger.error("========= TaskExample ===start=========:"+(new Date()).getTime());
		try{
		Thread.sleep(100000);
		}catch (Exception e) {
			logger.error("测试任务异常",e);
		}
		logger.error("========= TaskExample ==end==========:"+(new Date()).getTime());
	}

}
