package com.cwkj.schedule.controller.base;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 权限编号
 * @author ljc
 * @version 1.0
 */
public  class PermissionCode {
	private static Logger logger=LoggerFactory.getLogger(PermissionCode.class);
	
	public static String getCode(String code)
	{
		String result=null;
		try{
		Field field= PermissionCode.class.getField(code);
		result=field.toString();
		}catch (Exception e) {
			logger.error("获取权限编号异常",e);
		}
		return result;
	}

	/**
	 * 系统角色编辑
	 */
	public static final String system_role_edit="system_role_edit";
	/**
	 * 系统角色删除
	 */
	public static final String system_role_delete="system_role_delete";
	/**
	 * 系统角色列表
	 */
	public static final String system_role_list="system_role_list";
	
	/**
	 * 系统权限资源新增
	 */
	public static final String system_permission_add="system_permission_add";
	/**
	 * 系统权限资源编辑
	 */
	public static final String system_permission_edit="system_permission_edit";
	/**
	 * 系统权限资源列表
	 */
	public static final String system_permission_list="system_permission_list";
}
