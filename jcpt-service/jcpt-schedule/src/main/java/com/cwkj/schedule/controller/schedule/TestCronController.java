package com.cwkj.schedule.controller.schedule;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.schedule.controller.base.BaseAction;

/**
 * 测试Cron表达式
 * @author ljc
 * @version 1.0
 */
@Controller
@RequestMapping("/schedule/")
public class TestCronController extends BaseAction{

	private static Logger logger=LoggerFactory.getLogger(TestCronController.class);
	
	private boolean satisfiedBy;
	private Date nextValidTime1;
	private Date nextValidTime2;
	private Date nextValidTime3;
	private Date nextValidTime4;
	private Date nextValidTime5;
	private Date nextValidTime6;
	private Date nextValidTime7;
	private Date nextValidTime8;
	private Date nextValidTime9;
	private Date nextValidTime10;
	
	@RequestMapping(value="/testcron.do",method=RequestMethod.GET)
	public ModelAndView testCron_methodGet(HttpServletRequest request,String cronExpression)
	{
		ModelAndView view=new ModelAndView("schedule/testcron");
		try{
			view.addObject("cronExpression", cronExpression);
		}catch (Exception e) {
			logger.error("获取Cron初始化异常", e);
		}
		return view;
	}
	
	@RequestMapping(value="/testcron.do",method=RequestMethod.POST)
	public ModelAndView testCron(HttpServletRequest request,String cronExpression)
	{
		ModelAndView view=new ModelAndView("schedule/testcron");
		try{
			Date nowTime = new Date();
			AssertUtils.notEmptyStr(cronExpression, "cron表达式不能为空");
			view.addObject("cronExpression", cronExpression);
			CronExpression cron = new CronExpression(cronExpression);
			satisfiedBy = cron.isSatisfiedBy(nowTime);
			nextValidTime1 = cron.getNextValidTimeAfter(nowTime);
			if (nextValidTime1 != null) {
				nextValidTime2 = cron.getNextValidTimeAfter(nextValidTime1);
			}
			if (nextValidTime2 != null) {
				nextValidTime3 = cron.getNextValidTimeAfter(nextValidTime2);
			}
			if (nextValidTime3 != null) {
				nextValidTime4 = cron.getNextValidTimeAfter(nextValidTime3);
			}
			if (nextValidTime4 != null) {
				nextValidTime5 = cron.getNextValidTimeAfter(nextValidTime4);
			}
			if (nextValidTime5 != null) {
				nextValidTime6 = cron.getNextValidTimeAfter(nextValidTime5);
			}
			if (nextValidTime6 != null) {
				nextValidTime7 = cron.getNextValidTimeAfter(nextValidTime6);
			}
			if (nextValidTime7 != null) {
				nextValidTime8 = cron.getNextValidTimeAfter(nextValidTime7);
			}
			if (nextValidTime8 != null) {
				nextValidTime9 = cron.getNextValidTimeAfter(nextValidTime8);
			}
			if (nextValidTime9 != null) {
				nextValidTime10 = cron.getNextValidTimeAfter(nextValidTime9);
			}
			view.addObject("nowTime", nowTime);
			view.addObject("satisfiedBy", satisfiedBy);
			view.addObject("nextValidTime1", nextValidTime1);
			view.addObject("nextValidTime2", nextValidTime2);
			view.addObject("nextValidTime3", nextValidTime3);
			view.addObject("nextValidTime4", nextValidTime4);
			view.addObject("nextValidTime5", nextValidTime5);
			view.addObject("nextValidTime6", nextValidTime6);
			view.addObject("nextValidTime7", nextValidTime7);
			view.addObject("nextValidTime8", nextValidTime8);
			view.addObject("nextValidTime9", nextValidTime9);
			view.addObject("nextValidTime10", nextValidTime10);
			
		}catch (Exception e) {
			setPromptException(view, e);
			logger.error("测试cron异常",e);
		}
		return view;
	}
	
}
