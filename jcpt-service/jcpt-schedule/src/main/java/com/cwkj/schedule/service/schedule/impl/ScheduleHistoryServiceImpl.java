package com.cwkj.schedule.service.schedule.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.schedule.component.schedule.ScheduleHistoryComponent;
import com.cwkj.schedule.model.schedule.ScheduleHistoryDo;
import com.cwkj.schedule.service.schedule.ScheduleHistoryService;

/**
 * 调度作业历史.
 * @author ljc
 * @version  v1.0
 */
@Component("scheduleHistoryService")
public class ScheduleHistoryServiceImpl  implements ScheduleHistoryService{
	
	@Autowired
	private ScheduleHistoryComponent scheduleHistoryComponent;
	
	/**
	 * 添加调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer insertScheduleHistory(ScheduleHistoryDo scheduleHistory)
	{
		return scheduleHistoryComponent.insertScheduleHistory(scheduleHistory);
	}
	
	/**
	 * 获取调度作业历史数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleHistoryDo> queryScheduleHistoryList(Map<String,Object> selectItem)
	{
		return scheduleHistoryComponent.queryScheduleHistoryList(selectItem);
	}

	/**
	 * 获取调度作业历史数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleHistoryDo> queryScheduleHistoryListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return scheduleHistoryComponent.queryScheduleHistoryListPage(pageIndex,pageSize,selectItem);
	}
	

	/**
	 * 根据Id修改调度作业历史.
	 * @param scheduleHistory
	 * @return
	 */
	public Integer updateScheduleHistoryById(ScheduleHistoryDo scheduleHistory)
	{
		return scheduleHistoryComponent.updateScheduleHistoryById(scheduleHistory);
	}

	/**
	 * 根据Id删除调度作业历史.
	 * @param id
	 * @return
	 */
	public Integer deleteScheduleHistoryById(String id)
	{
		return scheduleHistoryComponent.deleteScheduleHistoryById(id);
	}

	/**
	 * 根据Id获取调度作业历史.
	 * @param id
	 * @return
	 */
	public ScheduleHistoryDo findScheduleHistoryById(String id)
	{
		return scheduleHistoryComponent.findScheduleHistoryById(id);
	}

	@Override
		public Integer deleteHistory(Date lastTime) {
		Map<String,Object> selectItem=new HashMap<String,Object>();
		selectItem.put("lastTime",lastTime);
		return scheduleHistoryComponent.deleteHistory(selectItem);
	}
}
