package com.cwkj.schedule.component.schedule;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.schedule.model.schedule.ScheduleGroupDo;

/**
 * 调度作业组.
 * @author ljc
 * @version  v1.0
 */
public interface ScheduleGroupComponent {
	
	/**
	 * 添加调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer insertScheduleGroup(ScheduleGroupDo scheduleGroup);
	
	/**
	 * 获取调度作业组数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ScheduleGroupDo> queryScheduleGroupList(Map<String,Object> selectItem);

	/**
	 * 获取调度作业组数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ScheduleGroupDo> queryScheduleGroupListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改调度作业组.
	 * @param scheduleGroup
	 * @return
	 */
	public Integer updateScheduleGroupById(ScheduleGroupDo scheduleGroup);

	/**
	 * 根据Id删除调度作业组.
	 * @param Id
	 * @return
	 */
	public Integer deleteScheduleGroupById(String Id);

	/**
	 * 根据Id获取调度作业组.
	 * @param Id
	 * @return
	 */
	public ScheduleGroupDo findScheduleGroupById(String Id);	
	
	/**
	 * 根据SpringId获取调度作业组
	 * @param springId
	 * @return
	 */
	public ScheduleGroupDo findScheduleGroupBySpringId(String springId);
}
