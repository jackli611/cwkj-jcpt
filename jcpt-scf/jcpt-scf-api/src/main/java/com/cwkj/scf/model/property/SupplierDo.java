package com.cwkj.scf.model.property;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 供应商.
 * @author ljc
 * @version  v1.0
 */
@Alias("supplierDo")
public class SupplierDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 状态：待审批
	 */
	public static final int STATUS_INIT=1;
	/** 状态：无效 */
	public static final int STATUS_INVALID=2;
	/** 状态：合作中 */
	public static final int STATUS_VALID=3;

	/** 平台审核状态：待审批 */
	public  static final  int PLATFORMSTATUS_PENDING=101;
	/** 平台审核状态：审批通过 */
	public  static final  int PLATFORMSTATUS_APPROVED=102;
	/** 平台审核状态：拒绝 */
	public  static final  int PLATFORMSTATUS_DENIED=103;
	/** 平台审核状态：审批不通过,需补充完善 */
	public  static final  int PLATFORMSTATUS_PROCESSING=104;

	/** ID. */
	private String id;

	/** 供应商编号. */
	private String supplierNo;

	/** 公司名称. */
	private String names;

	/** 手机号. */
	private String mobile;

	/** 法人主体. */
	private String legal;

	/** 法人身份证号码. */
	private String legalIdNo;

	/** 组织机构代码. */
	private String organizationCode;

	/** 地址. */
	private String address;

	/** 银行卡号. */
	private String bankCardno;

	/** 银行名称. */
	private String bankName;

	/** 银行卡账户名. */
	private String bankAccount;
	
	/** 营业执照. */
	private String businessLicense;
	
	/** 系统账户ID. */
	private Integer userId;
	
	/** 物业机构ID. */
	private String propertyId;
	
	/** 有效状态. */
	private Integer status;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;

	/** 邮箱 **/
	private String email;
	/** 联系人 */
	private String contactPerson;
	/** 企业信息 */
	private String companyProfile;
	/** 供应商等级. */
	private String level;
	/** 平台审核. */
	private Integer platformStatus;

	/** 核心企业名称 */
	private String propertyOrgName;


	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 供应商编号.
	 * @param supplierNo
	 * 供应商编号
	 */
	public void setSupplierNo(String supplierNo) {
		this.supplierNo = supplierNo;
	}

	/**
	 * 供应商编号.
	 * @return 供应商编号
	 */
	public String getSupplierNo() {
		return this.supplierNo;
	}

	/**
	 * 公司名称.
	 * @param names
	 * 公司名称
	 */
	public void setNames(String names) {
		this.names = names;
	}

	/**
	 * 公司名称.
	 * @return 公司名称
	 */
	public String getNames() {
		return this.names;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 法人主体.
	 * @param legal
	 * 法人主体
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}

	/**
	 * 法人主体.
	 * @return 法人主体
	 */
	public String getLegal() {
		return this.legal;
	}

	/**
	 * 法人身份证号码.
	 * @param legalIdNo
	 * 法人身份证号码
	 */
	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	/**
	 * 法人身份证号码.
	 * @return 法人身份证号码
	 */
	public String getLegalIdNo() {
		return this.legalIdNo;
	}

	/**
	 * 组织机构代码.
	 * @param organizationCode
	 * 组织机构代码
	 */
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	/**
	 * 组织机构代码.
	 * @return 组织机构代码
	 */
	public String getOrganizationCode() {
		return this.organizationCode;
	}

	/**
	 * 地址.
	 * @param address
	 * 地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 地址.
	 * @return 地址
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * 银行卡号.
	 * @param bankCardno
	 * 银行卡号
	 */
	public void setBankCardno(String bankCardno) {
		this.bankCardno = bankCardno;
	}

	/**
	 * 银行卡号.
	 * @return 银行卡号
	 */
	public String getBankCardno() {
		return this.bankCardno;
	}

	/**
	 * 银行名称.
	 * @param bankName
	 * 银行名称
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * 银行名称.
	 * @return 银行名称
	 */
	public String getBankName() {
		return this.bankName;
	}

	/**
	 * 银行卡账户名.
	 * @param bankAccount
	 * 银行卡账户名
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 * 银行卡账户名.
	 * @return 银行卡账户名
	 */
	public String getBankAccount() {
		return this.bankAccount;
	}
	
	/**
	 * 系统账户ID.
	 * @param userId
	 * 系统账户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 系统账户ID.
	 * @return 系统账户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 物业机构ID.
	 * @param propertyId
	 * 物业机构ID
	 */
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * 物业机构ID.
	 * @return 物业机构ID
	 */
	public String getPropertyId() {
		return this.propertyId;
	}
	
	/**
	 * 营业执照.
	 * @param businessLicense
	 * 营业执照
	 */
	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}

	/**
	 * 营业执照.
	 * @return 营业执照
	 */
	public String getBusinessLicense() {
		return this.businessLicense;
	}
	
	/**
	 * 有效状态.
	 * @param status
	 * 有效状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 有效状态.
	 * @return 有效状态
	 */
	public Integer getStatus() {
		return this.status;
	}
	
	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	/**
	 * 邮箱
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 邮箱
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 联系人
	 * @return
	 */
	public String getContactPerson() {
		return contactPerson;
	}


	/**
	 * 联系人
	 * @param contactPerson
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * 企业简介
	 * @return
	 */
	public String getCompanyProfile() {
		return companyProfile;
	}

	/**
	 * 企业简介
	 * @param companyProfile
	 */
	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	/**
	 * 供应商等级.
	 * @param level
	 * 供应商等级
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * 供应商等级.
	 * @return 供应商等级
	 */
	public String getLevel() {
		return this.level;
	}

	/**
	 * 平台审核.
	 * @param platformStatus
	 * 平台审核
	 */
	public void setPlatformStatus(Integer platformStatus) {
		this.platformStatus = platformStatus;
	}

	/**
	 * 平台审核.
	 * @return 平台审核
	 */
	public Integer getPlatformStatus() {
		return this.platformStatus;

	}

	/**
	 * 核心企业名称
	 * @return
	 */
	public String getPropertyOrgName() {
		return propertyOrgName;
	}

	/**
	 * 核心企业名称
	 * @param propertyOrgName
	 */
	public void setPropertyOrgName(String propertyOrgName) {
		this.propertyOrgName = propertyOrgName;
	}

	@Override
	public String toString()
	{
		return "SupplierDo ["+",id="+id
+",supplierNo="+supplierNo
+",names="+names
+",mobile="+mobile
+",legal="+legal
+",legalIdNo="+legalIdNo
+",organizationCode="+organizationCode
+",address="+address
+",bankCardno="+bankCardno
+",bankName="+bankName
+",bankAccount="+bankAccount
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
