package com.cwkj.scf.model.shares;

import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;

/**
 * 个贷分享订单
 */
@Alias("sharePersonalLoanOrder")
public class SharePersonalLoanOrder implements Serializable {

/** serialVersionUID. */
private static final long serialVersionUID = 1L;

    /**
     * 一级分享者手机号
     */
    private String oneLevelShareTelphone;

    /**
     * 二级分享者分机号
     */
    private String twoLeavelShareTelphone;
    /**
     * 订单号
     */
    private String orderCode;
    /**
     * 订单申请人姓名
     */
    private String orderRealName;
    /**
     * 订单申请人手机号
     */
    private String orderUserTelphone;
    /**
     * 订单申请时间
     */
    private Date orderTime;
    /**
     * 订单类型
     */
    private Integer orderType;
    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 订单来源
     */
    private String orderSource;
    /**
     * 订单ID
     */
    private Long loanId;


    public String getOneLevelShareTelphone() {
        return oneLevelShareTelphone;
    }

    public void setOneLevelShareTelphone(String oneLevelShareTelphone) {
        this.oneLevelShareTelphone = oneLevelShareTelphone;
    }

    public String getTwoLeavelShareTelphone() {
        return twoLeavelShareTelphone;
    }

    public void setTwoLeavelShareTelphone(String twoLeavelShareTelphone) {
        this.twoLeavelShareTelphone = twoLeavelShareTelphone;
    }

    public String getOrderRealName() {
        return orderRealName;
    }

    public void setOrderRealName(String orderRealName) {
        this.orderRealName = orderRealName;
    }

    public String getOrderUserTelphone() {
        return orderUserTelphone;
    }

    public void setOrderUserTelphone(String orderUserTelphone) {
        this.orderUserTelphone = orderUserTelphone;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }
}
