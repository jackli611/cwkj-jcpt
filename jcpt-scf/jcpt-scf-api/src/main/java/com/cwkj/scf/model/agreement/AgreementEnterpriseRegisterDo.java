package com.cwkj.scf.model.agreement;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 签章企业注册.
 * @author ljc
 * @version  v1.0
 */
@Alias("agreementEnterpriseRegisterDo")
public class AgreementEnterpriseRegisterDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键ID. */
	private String id;

	/** 企业名称. */
	private String enterpriseName;

	/** 营业执照号或社会统一代码. */
	private String certificate;

	/** 企业注册地址. */
	private String address;

	/** 联系人. */
	private String contact;

	/** 手机号. */
	private String mobile;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;


	/**
	 * 主键ID.
	 * @param id
	 * 主键ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 主键ID.
	 * @return 主键ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 企业名称.
	 * @param enterpriseName
	 * 企业名称
	 */
	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	/**
	 * 企业名称.
	 * @return 企业名称
	 */
	public String getEnterpriseName() {
		return this.enterpriseName;
	}

	/**
	 * 营业执照号或社会统一代码.
	 * @param certificate
	 * 营业执照号或社会统一代码
	 */
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	/**
	 * 营业执照号或社会统一代码.
	 * @return 营业执照号或社会统一代码
	 */
	public String getCertificate() {
		return this.certificate;
	}

	/**
	 * 企业注册地址.
	 * @param address
	 * 企业注册地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 企业注册地址.
	 * @return 企业注册地址
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * 联系人.
	 * @param contact
	 * 联系人
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * 联系人.
	 * @return 联系人
	 */
	public String getContact() {
		return this.contact;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	@Override
	public String toString()
	{
		return "AgreementEnterpriseRegisterDo ["+",id="+id
+",enterpriseName="+enterpriseName
+",certificate="+certificate
+",address="+address
+",contact="+contact
+",mobile="+mobile
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
