package com.cwkj.scf.model.property;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 企业.
 * @author ljc
 * @version  v1.0
 */
@Alias("companyDo")
public class CompanyDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final int APPROVALSTATUS_INIT=0;
	
	/**
	 * 状态：有效
	 */
	public static final int STATUS_VALID=1;
	/** 状态：无效 */
	public static final int STATUS_INVALID=2;

	/** ID. */
	private String id;

	/** 企业LOGO. */
	private String logo;

	/** 公司名称. */
	private String names;

	/** 公司简称. */
	private String shortName ;

	/** 组织机构代码. */
	private String organizationCode;

	/** 法人主体. */
	private String legal;

	/** 法人身份证号码. */
	private String legalIdNo;

	/** 手机号. */
	private String mobile;

	/** 审批状态. */
	private Integer approvalStatus;

	/** 邮箱. */
	private String email;

	/** 等级. */
	private Integer levels;

	/** 营业执照. */
	private String businessLicense;

	/** 附件. */
	private String attachment;

	/** 有效状态. */
	private Integer status;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;


	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 企业LOGO.
	 * @param logo
	 * 企业LOGO
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * 企业LOGO.
	 * @return 企业LOGO
	 */
	public String getLogo() {
		return this.logo;
	}

	/**
	 * 公司名称.
	 * @param names
	 * 公司名称
	 */
	public void setNames(String names) {
		this.names = names;
	}

	/**
	 * 公司名称.
	 * @return 公司名称
	 */
	public String getNames() {
		return this.names;
	}

	/**
	 * 公司简称.
	 * @param shortName 
	 * 公司简称
	 */
	public void setShortName (String shortName ) {
		this.shortName  = shortName ;
	}

	/**
	 * 公司简称.
	 * @return 公司简称
	 */
	public String getShortName () {
		return this.shortName ;
	}

	/**
	 * 组织机构代码.
	 * @param organizationCode
	 * 组织机构代码
	 */
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	/**
	 * 组织机构代码.
	 * @return 组织机构代码
	 */
	public String getOrganizationCode() {
		return this.organizationCode;
	}

	/**
	 * 法人主体.
	 * @param legal
	 * 法人主体
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}

	/**
	 * 法人主体.
	 * @return 法人主体
	 */
	public String getLegal() {
		return this.legal;
	}

	/**
	 * 法人身份证号码.
	 * @param legalIdNo
	 * 法人身份证号码
	 */
	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	/**
	 * 法人身份证号码.
	 * @return 法人身份证号码
	 */
	public String getLegalIdNo() {
		return this.legalIdNo;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 审批状态.
	 * @param approvalStatus
	 * 审批状态
	 */
	public void setApprovalStatus(Integer approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * 审批状态.
	 * @return 审批状态
	 */
	public Integer getApprovalStatus() {
		return this.approvalStatus;
	}

	/**
	 * 邮箱.
	 * @param email
	 * 邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 邮箱.
	 * @return 邮箱
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * 等级.
	 * @param levels
	 * 等级
	 */
	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	/**
	 * 等级.
	 * @return 等级
	 */
	public Integer getLevels() {
		return this.levels;
	}

	/**
	 * 营业执照.
	 * @param businessLicense
	 * 营业执照
	 */
	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}

	/**
	 * 营业执照.
	 * @return 营业执照
	 */
	public String getBusinessLicense() {
		return this.businessLicense;
	}

	/**
	 * 附件.
	 * @param attachment
	 * 附件
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	/**
	 * 附件.
	 * @return 附件
	 */
	public String getAttachment() {
		return this.attachment;
	}

	/**
	 * 有效状态.
	 * @param status
	 * 有效状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 有效状态.
	 * @return 有效状态
	 */
	public Integer getStatus() {
		return this.status;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	@Override
	public String toString()
	{
		return "CompanyDo ["+",id="+id
+",logo="+logo
+",names="+names
+",shortName ="+shortName 
+",organizationCode="+organizationCode
+",legal="+legal
+",legalIdNo="+legalIdNo
+",mobile="+mobile
+",approvalStatus="+approvalStatus
+",email="+email
+",levels="+levels
+",businessLicense="+businessLicense
+",attachment="+attachment
+",status="+status
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
 