package com.cwkj.scf.service.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.CompanyDo;

/**
 * 企业.
 * @author ljc
 * @version  v1.0
 */
public interface CompanyService {
	
	/**
	 * 添加企业.
	 * @param company
	 * @return
	 */
	public Integer insertCompany(CompanyDo company);
	
	/**
	 * 获取企业数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<CompanyDo> queryCompanyList(Map<String,Object> selectItem);

	/**
	 * 获取企业数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<CompanyDo> queryCompanyListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改企业.
	 * @param company
	 * @return
	 */
	public Integer updateCompanyById(CompanyDo company);

	/**
	 * 根据Id删除企业.
	 * @param id
	 * @return
	 */
	public Integer deleteCompanyById(String id);

	/**
	 * 根据Id获取企业.
	 * @param id
	 * @return
	 */
	public CompanyDo findCompanyById(String id);	
	
	/**
	 * 修改营业执照信息
	 * @param id 物业ID 
	 * @param businessLicense 营业执照 
	 * @return
	 */
	public Integer updateBusinessLicense(String id,String businessLicense);
}
