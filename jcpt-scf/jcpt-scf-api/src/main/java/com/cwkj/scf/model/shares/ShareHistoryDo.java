package com.cwkj.scf.model.shares;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 分享历史.
 * @author ljc
 * @version  v1.0
 */
@Alias("shareHistoryDo")
public class ShareHistoryDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键. */
	private Long id;

	/** 用户ID. */
	private Integer userId;

	/** 分享链接. */
	private String shareLink;

	/** 标题. */
	private String shareTitle;

	/** 描述. */
	private String shareDesc;

	/** 分享图片. */
	private String shareImgUrl;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;


	/**
	 * 主键.
	 * @param id
	 * 主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 主键.
	 * @return 主键
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * 用户ID.
	 * @param userId
	 * 用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 用户ID.
	 * @return 用户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 分享链接.
	 * @param shareLink
	 * 分享链接
	 */
	public void setShareLink(String shareLink) {
		this.shareLink = shareLink;
	}

	/**
	 * 分享链接.
	 * @return 分享链接
	 */
	public String getShareLink() {
		return this.shareLink;
	}

	/**
	 * 标题.
	 * @param shareTitle
	 * 标题
	 */
	public void setShareTitle(String shareTitle) {
		this.shareTitle = shareTitle;
	}

	/**
	 * 标题.
	 * @return 标题
	 */
	public String getShareTitle() {
		return this.shareTitle;
	}

	/**
	 * 描述.
	 * @param shareDesc
	 * 描述
	 */
	public void setShareDesc(String shareDesc) {
		this.shareDesc = shareDesc;
	}

	/**
	 * 描述.
	 * @return 描述
	 */
	public String getShareDesc() {
		return this.shareDesc;
	}

	/**
	 * 分享图片.
	 * @param shareImgUrl
	 * 分享图片
	 */
	public void setShareImgUrl(String shareImgUrl) {
		this.shareImgUrl = shareImgUrl;
	}

	/**
	 * 分享图片.
	 * @return 分享图片
	 */
	public String getShareImgUrl() {
		return this.shareImgUrl;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	@Override
	public String toString()
	{
		return "ShareHistoryDo ["+",id="+id
+",userId="+userId
+",shareLink="+shareLink
+",shareTitle="+shareTitle
+",shareDesc="+shareDesc
+",shareImgUrl="+shareImgUrl
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
