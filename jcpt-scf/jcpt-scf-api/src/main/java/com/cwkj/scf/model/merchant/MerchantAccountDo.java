package com.cwkj.scf.model.merchant;

import org.apache.ibatis.type.Alias;

import com.cwkj.jcpt.util.StringUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * 商户账号.
 * @author ljc
 * @version  v1.0
 */
@Alias("merchantAccountDo")
public class MerchantAccountDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键ID. */
	private String id;

	/** 商户用户ID. */
	private String acctid;

	/** 商户ID. */
	private String merId;

	/** 系统用户ID. */
	private Integer userId;

	/** 手机号. */
	private String mobile;

	/** 小区. */
	private String community;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;


	/**
	 * 主键ID.
	 * @param id
	 * 主键ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 主键ID.
	 * @return 主键ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 商户用户ID.
	 * @param acctid
	 * 商户用户ID
	 */
	public void setAcctid(String acctid) {
		this.acctid = acctid;
	}

	/**
	 * 商户用户ID.
	 * @return 商户用户ID
	 */
	public String getAcctid() {
		return this.acctid;
	}

	/**
	 * 商户ID.
	 * @param merId
	 * 商户ID
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}

	/**
	 * 商户ID.
	 * @return 商户ID
	 */
	public String getMerId() {
		return this.merId;
	}

	/**
	 * 系统用户ID.
	 * @param userId
	 * 系统用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 系统用户ID.
	 * @return 系统用户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 小区.
	 * @param community
	 * 小区
	 */
	public void setCommunity(String community) {
		this.community = community;
	}

	/**
	 * 小区.
	 * @return 小区
	 */
	public String getCommunity() {
		return this.community;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	public String getAcctidWithoutMerchant() {
		if(StringUtil.isBlank(this.acctid)) {
			return "";
		}
		return this.acctid.substring(this.merId.length(), this.acctid.length());
	}
	
	
	@Override
	public String toString()
	{
		return "MerchantAccountDo ["+",id="+id
+",acctid="+acctid
+",merId="+merId
+",userId="+userId
+",mobile="+mobile
+",community="+community
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
