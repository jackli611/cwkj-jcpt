package com.cwkj.scf.model.shares;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 分享用户.
 * @author ljc
 * @version  v1.0
 */
@Alias("shareUserDo")
public class ShareUserDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * 取消
	 */
	public static final int PARTNERSTATUS_CANCEL=2;
	/**
	 * 合作中
	 */
	public static final int PARTNERSTATUS_PARTNER=1;
	/**
	 *默认没合作关系
	 */
	public static final int PARTNERSTATUS_DEFAULT=0;



	/** 主键. */
	private Long id;

	/** 用户ID. */
	private Integer userId;

	/** 上级分享ID. */
	private Long fromShareId;

	/** 打开分享链接使用的平台. */
	private String openPlatform;

	/** 合作状态. */
	private Integer partnerStatus;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;

	/** 手机号. */
	private String telphone;

	/** 上级分享用户ID. */
	private Integer fromUserId;

	/** 上级分享手机号. */
	private String fromTelphone;

	/** 上上级分享用户ID. */
	private Integer topShareUserId;

	/** 上上级分享用户手机号. */
	private String topShareTelphone;


	/**
	 * 主键.
	 * @param id
	 * 主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 主键.
	 * @return 主键
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * 用户ID.
	 * @param userId
	 * 用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 用户ID.
	 * @return 用户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 上级分享ID.
	 * @param fromShareId
	 * 上级分享ID
	 */
	public void setFromShareId(Long fromShareId) {
		this.fromShareId = fromShareId;
	}

	/**
	 * 上级分享ID.
	 * @return 上级分享ID
	 */
	public Long getFromShareId() {
		return this.fromShareId;
	}

	/**
	 * 打开分享链接使用的平台.
	 * @param openPlatform
	 * 打开分享链接使用的平台
	 */
	public void setOpenPlatform(String openPlatform) {
		this.openPlatform = openPlatform;
	}

	/**
	 * 打开分享链接使用的平台.
	 * @return 打开分享链接使用的平台
	 */
	public String getOpenPlatform() {
		return this.openPlatform;
	}

	/**
	 * 合作状态.
	 * @param partnerStatus
	 * 合作状态
	 */
	public void setPartnerStatus(Integer partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	/**
	 * 合作状态.
	 * @return 合作状态
	 */
	public Integer getPartnerStatus() {
		return this.partnerStatus;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 手机号.
	 * @param telphone
	 * 手机号
	 */
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getTelphone() {
		return this.telphone;
	}

	/**
	 * 上级分享用户ID.
	 * @param fromUserId
	 * 上级分享用户ID
	 */
	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	/**
	 * 上级分享用户ID.
	 * @return 上级分享用户ID
	 */
	public Integer getFromUserId() {
		return this.fromUserId;
	}

	/**
	 * 上级分享手机号.
	 * @param fromTelphone
	 * 上级分享手机号
	 */
	public void setFromTelphone(String fromTelphone) {
		this.fromTelphone = fromTelphone;
	}

	/**
	 * 上级分享手机号.
	 * @return 上级分享手机号
	 */
	public String getFromTelphone() {
		return this.fromTelphone;
	}

	/**
	 * 上上级分享用户ID.
	 * @param topShareUserId
	 * 上上级分享用户ID
	 */
	public void setTopShareUserId(Integer topShareUserId) {
		this.topShareUserId = topShareUserId;
	}

	/**
	 * 上上级分享用户ID.
	 * @return 上上级分享用户ID
	 */
	public Integer getTopShareUserId() {
		return this.topShareUserId;
	}

	/**
	 * 上上级分享用户手机号.
	 * @param topShareTelphone
	 * 上上级分享用户手机号
	 */
	public void setTopShareTelphone(String topShareTelphone) {
		this.topShareTelphone = topShareTelphone;
	}

	/**
	 * 上上级分享用户手机号.
	 * @return 上上级分享用户手机号
	 */
	public String getTopShareTelphone() {
		return this.topShareTelphone;
	}


	@Override
	public String toString()
	{
		return "ShareUserDo ["+",id="+id
+",userId="+userId
+",fromShareId="+fromShareId
+",openPlatform="+openPlatform
+",partnerStatus="+partnerStatus
+",createTime="+createTime
+",updateTime="+updateTime
+",telphone="+telphone
+",fromUserId="+fromUserId
+",fromTelphone="+fromTelphone
+",topShareUserId="+topShareUserId
+",topShareTelphone="+topShareTelphone
+"]";
	}

}
