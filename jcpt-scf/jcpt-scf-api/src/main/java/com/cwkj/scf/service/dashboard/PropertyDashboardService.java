package com.cwkj.scf.service.dashboard;

import java.util.List;
import java.util.Map;

import com.cwkj.scf.model.dashboard.DashboardInfoBoxDo;

public interface PropertyDashboardService {
	
	 
	/**
	 * InfoBox看板
	 * @param selectItem
	 * @return
	 */
	public List<DashboardInfoBoxDo> InfoBoxDashboard(Map<String, Object> selectItem);

}
