package com.cwkj.scf.model.merchant;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 商户信息.
 * @author ljc
 * @version  v1.0
 */
@Alias("merchantProfileDo")
public class MerchantProfileDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键ID. */
	private Integer id;

	/** 商户ID. */
	private String merId;

	/** 商户名称. */
	private String merName;

	/** 产品密码. */
	private String secretKey;

	/** pem格式的公钥. */
	private String pemKey;

	/** 产品编号. */
	private String productCode;

	/** MD5键值. */
	private String md5Key;

	/** 状态. */
	private Integer status;

	/** 备注. */
	private String remark;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;
	
	/**
	 * 	核心企業id
	 */
	private String propertyId;


	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * 主键ID.
	 * @param id
	 * 主键ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 主键ID.
	 * @return 主键ID
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * 商户ID.
	 * @param merId
	 * 商户ID
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}

	/**
	 * 商户ID.
	 * @return 商户ID
	 */
	public String getMerId() {
		return this.merId;
	}

	/**
	 * 商户名称.
	 * @param merName
	 * 商户名称
	 */
	public void setMerName(String merName) {
		this.merName = merName;
	}

	/**
	 * 商户名称.
	 * @return 商户名称
	 */
	public String getMerName() {
		return this.merName;
	}

	/**
	 * 产品密码.
	 * @param secretKey
	 * 产品密码
	 */
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	/**
	 * 产品密码.
	 * @return 产品密码
	 */
	public String getSecretKey() {
		return this.secretKey;
	}

	/**
	 * pem格式的公钥.
	 * @param pemKey
	 * pem格式的公钥
	 */
	public void setPemKey(String pemKey) {
		this.pemKey = pemKey;
	}

	/**
	 * pem格式的公钥.
	 * @return pem格式的公钥
	 */
	public String getPemKey() {
		return this.pemKey;
	}

	/**
	 * 产品编号.
	 * @param productCode
	 * 产品编号
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * 产品编号.
	 * @return 产品编号
	 */
	public String getProductCode() {
		return this.productCode;
	}

	/**
	 * MD5键值.
	 * @param md5Key
	 * MD5键值
	 */
	public void setMd5Key(String md5Key) {
		this.md5Key = md5Key;
	}

	/**
	 * MD5键值.
	 * @return MD5键值
	 */
	public String getMd5Key() {
		return this.md5Key;
	}

	/**
	 * 状态.
	 * @param status
	 * 状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 状态.
	 * @return 状态
	 */
	public Integer getStatus() {
		return this.status;
	}

	/**
	 * 备注.
	 * @param remark
	 * 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 备注.
	 * @return 备注
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	@Override
	public String toString()
	{
		return "MerchantProfileDo ["+",id="+id
+",merId="+merId
+",merName="+merName
+",secretKey="+secretKey
+",pemKey="+pemKey
+",productCode="+productCode
+",md5Key="+md5Key
+",status="+status
+",remark="+remark
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
