package com.cwkj.scf.model.finances;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 金融机构.
 * @author ljc
 * @version  v1.0
 */
@Alias("financeOrgDo")
public class FinanceOrgDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/**
	 * 状态：有效
	 */
	public static final int STATUS_VALID=1;
	/** 状态：无效 */
	public static final int STATUS_INVALID=2;

	/** ID. */
	private String id;

	/** LOGO. */
	private String logo;

	/** 机构名称. */
	private String names;

	/** 机构简称. */
	private String shortName;

	/** 组织机构代码. */
	private String organizationCode;

	/** 法人主体. */
	private String legal;

	/** 法人身份证号码. */
	private String legalIdNo;

	/** 手机号. */
	private String mobile;

	/** 营业执照. */
	private String businessLicense;

	/** 附件. */
	private String attachment;

	/** 系统账户ID. */
	private Integer userId;

	/** 有效状态. */
	private Integer status;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;

	/** 备注. */
	private String remark;


	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * LOGO.
	 * @param logo
	 * LOGO
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * LOGO.
	 * @return LOGO
	 */
	public String getLogo() {
		return this.logo;
	}

	/**
	 * 机构名称.
	 * @param names
	 * 机构名称
	 */
	public void setNames(String names) {
		this.names = names;
	}

	/**
	 * 机构名称.
	 * @return 机构名称
	 */
	public String getNames() {
		return this.names;
	}

	/**
	 * 机构简称.
	 * @param shortName
	 * 机构简称
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * 机构简称.
	 * @return 机构简称
	 */
	public String getShortName() {
		return this.shortName;
	}

	/**
	 * 组织机构代码.
	 * @param organizationCode
	 * 组织机构代码
	 */
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	/**
	 * 组织机构代码.
	 * @return 组织机构代码
	 */
	public String getOrganizationCode() {
		return this.organizationCode;
	}

	/**
	 * 法人主体.
	 * @param legal
	 * 法人主体
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}

	/**
	 * 法人主体.
	 * @return 法人主体
	 */
	public String getLegal() {
		return this.legal;
	}

	/**
	 * 法人身份证号码.
	 * @param legalIdNo
	 * 法人身份证号码
	 */
	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	/**
	 * 法人身份证号码.
	 * @return 法人身份证号码
	 */
	public String getLegalIdNo() {
		return this.legalIdNo;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 营业执照.
	 * @param businessLicense
	 * 营业执照
	 */
	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}

	/**
	 * 营业执照.
	 * @return 营业执照
	 */
	public String getBusinessLicense() {
		return this.businessLicense;
	}

	/**
	 * 附件.
	 * @param attachment
	 * 附件
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	/**
	 * 附件.
	 * @return 附件
	 */
	public String getAttachment() {
		return this.attachment;
	}

	/**
	 * 系统账户ID.
	 * @param userId
	 * 系统账户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 系统账户ID.
	 * @return 系统账户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 有效状态.
	 * @param status
	 * 有效状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 有效状态.
	 * @return 有效状态
	 */
	public Integer getStatus() {
		return this.status;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}

	/**
	 * 备注.
	 * @param remark
	 * 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 备注.
	 * @return 备注
	 */
	public String getRemark() {
		return this.remark;
	}


	@Override
	public String toString()
	{
		return "FinanceOrgDo ["+",id="+id
+",logo="+logo
+",names="+names
+",shortName="+shortName
+",organizationCode="+organizationCode
+",legal="+legal
+",legalIdNo="+legalIdNo
+",mobile="+mobile
+",businessLicense="+businessLicense
+",attachment="+attachment
+",userId="+userId
+",status="+status
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+",remark="+remark
+"]";
	}

}
