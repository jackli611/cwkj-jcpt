package com.cwkj.scf.model.products;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 个贷产品订单.
 * @author ljc
 * @version  v1.0
 */
@Alias("personalLoanProductOrderDo")
public class PersonalLoanProductOrderDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键. */
	private Long id;

	/** 产品ID. */
	private Long productId;

	/** 借款ID. */
	private String loanId;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;

	/** 资金机构ID. */
	private String financeId;

	/** 产品名称. */
	private String productName;

	/** 产品类型. */
	private Integer productType;

	/** 客户类型. */
	private String customerType;

	/** 费用说明. */
	private String fees;

	/** 征信. */
	private String credit;

	/** 办理说明. */
	private String transaction;

	/** 资金机构名称 */
	private String financeName;


	/**
	 * 主键.
	 * @param id
	 * 主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 主键.
	 * @return 主键
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * 产品ID.
	 * @param productId
	 * 产品ID
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * 产品ID.
	 * @return 产品ID
	 */
	public Long getProductId() {
		return this.productId;
	}

	/**
	 * 借款ID.
	 * @param loanId
	 * 借款ID
	 */
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	/**
	 * 借款ID.
	 * @return 借款ID
	 */
	public String getLoanId() {
		return this.loanId;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	public String getFinanceId() {
		return financeId;
	}

	public void setFinanceId(String financeId) {
		this.financeId = financeId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getFees() {
		return fees;
	}

	public void setFees(String fees) {
		this.fees = fees;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getFinanceName() {
		return financeName;
	}

	public void setFinanceName(String financeName) {
		this.financeName = financeName;
	}

	@Override
	public String toString()
	{
		return "PersonalLoanProductOrderDo ["+",id="+id
+",productId="+productId
+",loanId="+loanId
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
