package com.cwkj.scf.model.dashboard;

import java.math.BigDecimal;

import org.apache.ibatis.type.Alias;

/**
 * InfoBox看板
 * @author ljc
 *
 */
@Alias("dashboardInfoBoxDo")
public class DashboardInfoBoxDo {
	
	/** 标题 */
	private String text;
	/** 值 */
	private BigDecimal showValue;

	/**
	 * 标题
	 * @return  
	 */
	public String getText() {
		return text;
	}

	/**
	 * 标题
	 * @param text 
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 *  值
	 * @return  
	 */
	public BigDecimal getShowValue() {
		return showValue;
	}

	/**
	 *  值
	 * @param number  值
	 */
	public void setShowValue(BigDecimal showValue) {
		this.showValue = showValue;
	}

	
}
