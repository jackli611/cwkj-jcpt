package com.cwkj.scf.model.property;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 供应商邀请注册.
 * @author ljc
 * @version  v1.0
 */
@Alias("supplierInvitationDo")
public class SupplierInvitationDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** ID. */
	private Integer id;

	/** 物业机构ID. */
	private String propertyId;

	/** 邀请码. */
	private String inviteCode;

	/** 访问地址. */
	private String links;

	/** 登记时间. */
	private Date signUpTime;

	/** 有效到期时间. */
	private Date expiryDate;

	/** 手机号. */
	private String mobile;

	/** 备注. */
	private String remark;

	/** 修改时间. */
	private Date updateTime;

	/** 创建时间. */
	private Date createTime;

	/** 操作记录ID. */
	private Integer recordUserId;
	
	/**
	 * 核心企业机构
	 */
	private String propertyOrg;


	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * 物业机构ID.
	 * @param propertyId
	 * 物业机构ID
	 */
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * 物业机构ID.
	 * @return 物业机构ID
	 */
	public String getPropertyId() {
		return this.propertyId;
	}

	/**
	 * 邀请码.
	 * @param inviteCode
	 * 邀请码
	 */
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	/**
	 * 邀请码.
	 * @return 邀请码
	 */
	public String getInviteCode() {
		return this.inviteCode;
	}

	/**
	 * 访问地址.
	 * @param links
	 * 访问地址
	 */
	public void setLinks(String links) {
		this.links = links;
	}

	/**
	 * 访问地址.
	 * @return 访问地址
	 */
	public String getLinks() {
		return this.links;
	}

	/**
	 * 登记时间.
	 * @param signUpTime
	 * 登记时间
	 */
	public void setSignUpTime(Date signUpTime) {
		this.signUpTime = signUpTime;
	}

	/**
	 * 登记时间.
	 * @return 登记时间
	 */
	public Date getSignUpTime() {
		return this.signUpTime;
	}

	/**
	 * 有效到期时间.
	 * @param expiryDate
	 * 有效到期时间
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * 有效到期时间.
	 * @return 有效到期时间
	 */
	public Date getExpiryDate() {
		return this.expiryDate;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 备注.
	 * @param remark
	 * 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 备注.
	 * @return 备注
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	/**
	 * 核心企业机构
	 * @return 
	 */
	public String getPropertyOrg() {
		return propertyOrg;
	}

	/**
	 * 核心企业机构
	 * @param propertyOrg
	 */
	public void setPropertyOrg(String propertyOrg) {
		this.propertyOrg = propertyOrg;
	}

	@Override
	public String toString()
	{
		return "SupplierInvitationDo ["+",id="+id
+",propertyId="+propertyId
+",inviteCode="+inviteCode
+",links="+links
+",signUpTime="+signUpTime
+",expiryDate="+expiryDate
+",mobile="+mobile
+",remark="+remark
+",updateTime="+updateTime
+",createTime="+createTime
+",recordUserId="+recordUserId
+"]";
	}

}
