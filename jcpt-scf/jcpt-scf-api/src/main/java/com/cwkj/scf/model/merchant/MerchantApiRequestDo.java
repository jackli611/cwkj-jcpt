package com.cwkj.scf.model.merchant;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 商户API请求.
 * @author ljc
 * @version  v1.0
 */
@Alias("merchantApiRequestDo")
public class MerchantApiRequestDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 请求流水. */
	private String trxId;

	/** 商户ID. */
	private String merId;

	/** 产品密码. */
	private String secretKey;

	/** 请求内容. */
	private String requestBody;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;


	/**
	 * 请求流水.
	 * @param trxId
	 * 请求流水
	 */
	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	/**
	 * 请求流水.
	 * @return 请求流水
	 */
	public String getTrxId() {
		return this.trxId;
	}

	/**
	 * 商户ID.
	 * @param merId
	 * 商户ID
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}

	/**
	 * 商户ID.
	 * @return 商户ID
	 */
	public String getMerId() {
		return this.merId;
	}

	/**
	 * 产品密码.
	 * @param secretKey
	 * 产品密码
	 */
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	/**
	 * 产品密码.
	 * @return 产品密码
	 */
	public String getSecretKey() {
		return this.secretKey;
	}

	/**
	 * 请求内容.
	 * @param requestBody
	 * 请求内容
	 */
	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	/**
	 * 请求内容.
	 * @return 请求内容
	 */
	public String getRequestBody() {
		return this.requestBody;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	@Override
	public String toString()
	{
		return "MerchantApiRequestDo ["+",trxId="+trxId
+",merId="+merId
+",secretKey="+secretKey
+",requestBody="+requestBody
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
