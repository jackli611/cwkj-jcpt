package com.cwkj.scf.service.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.PayInfoDo;

/**
 * 支付信息.
 * @author ljc
 * @version  v1.0
 */
public interface PayInfoService {
	
	/**
	 * 添加支付信息.
	 * @param payInfo
	 * @return
	 */
	public Integer insertPayInfo(PayInfoDo payInfo);
	
	/**
	 * 获取支付信息数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PayInfoDo> queryPayInfoList(Map<String,Object> selectItem);

	/**
	 * 获取支付信息数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PayInfoDo> queryPayInfoListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改支付信息.
	 * @param payInfo
	 * @return
	 */
	public Integer updatePayInfoById(PayInfoDo payInfo);

	/**
	 * 根据Id删除支付信息.
	 * @param id
	 * @return
	 */
	public Integer deletePayInfoById(String id);

	/**
	 * 根据Id获取支付信息.
	 * @param id
	 * @return
	 */
	public PayInfoDo findPayInfoById(String id);	
}
