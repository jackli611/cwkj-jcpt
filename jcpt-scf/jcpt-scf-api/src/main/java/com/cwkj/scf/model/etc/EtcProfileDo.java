package com.cwkj.scf.model.etc;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

/**
 * ETC基本信息.
 * @author ljc
 * @version  v1.0
 */
@Alias("etcProfileDo")
public class EtcProfileDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * 编辑状态
	 */
	public static int APPLYSTATUS_EDIT=0;
	/**
	 * 已提交状态
	 */
	public static int APPLYSTATUS_SUBMIT=1;


	/** 主键. */
	private Long id;

	/** 姓名. */
	private String realName;

	/** 手机号. */
	private String telphone;

	/** 车牌号. */
	private String carno;

	/** 驾驶证持有人姓名. */
	private String driverUserName;

	/** 住址. */
	private String address;

	/** 是否有ETC卡. */
	private String usingCard;

	/** ETC卡类型. */
	private String cardType;

	/** 用户ID. */
	private Integer userId;

	/** 申请状态. */
	private Integer applyStatus;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;


	/**
	 * 主键.
	 * @param id
	 * 主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 主键.
	 * @return 主键
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * 姓名.
	 * @param realName
	 * 姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * 姓名.
	 * @return 姓名
	 */
	public String getRealName() {
		return this.realName;
	}

	/**
	 * 手机号.
	 * @param telphone
	 * 手机号
	 */
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getTelphone() {
		return this.telphone;
	}

	/**
	 * 车牌号.
	 * @param carno
	 * 车牌号
	 */
	public void setCarno(String carno) {
		this.carno = carno;
	}

	/**
	 * 车牌号.
	 * @return 车牌号
	 */
	public String getCarno() {
		return this.carno;
	}

	/**
	 * 驾驶证持有人姓名.
	 * @param driverUserName
	 * 驾驶证持有人姓名
	 */
	public void setDriverUserName(String driverUserName) {
		this.driverUserName = driverUserName;
	}

	/**
	 * 驾驶证持有人姓名.
	 * @return 驾驶证持有人姓名
	 */
	public String getDriverUserName() {
		return this.driverUserName;
	}

	/**
	 * 住址.
	 * @param address
	 * 住址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 住址.
	 * @return 住址
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * 是否有ETC卡.
	 * @param usingCard
	 * 是否有ETC卡
	 */
	public void setUsingCard(String usingCard) {
		this.usingCard = usingCard;
	}

	/**
	 * 是否有ETC卡.
	 * @return 是否有ETC卡
	 */
	public String getUsingCard() {
		return this.usingCard;
	}

	/**
	 * ETC卡类型.
	 * @param cardType
	 * ETC卡类型
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * ETC卡类型.
	 * @return ETC卡类型
	 */
	public String getCardType() {
		return this.cardType;
	}

	/**
	 * 用户ID.
	 * @param userId
	 * 用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 用户ID.
	 * @return 用户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 申请状态.
	 * @param applyStatus
	 * 申请状态
	 */
	public void setApplyStatus(Integer applyStatus) {
		this.applyStatus = applyStatus;
	}

	/**
	 * 申请状态.
	 * @return 申请状态
	 */
	public Integer getApplyStatus() {
		return this.applyStatus;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	@Override
	public String toString()
	{
		return "EtcProfileDo ["+",id="+id
+",realName="+realName
+",telphone="+telphone
+",carno="+carno
+",driverUserName="+driverUserName
+",address="+address
+",usingCard="+usingCard
+",cardType="+cardType
+",userId="+userId
+",applyStatus="+applyStatus
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
