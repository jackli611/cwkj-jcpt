package com.cwkj.scf.service.agreement;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.agreement.AgreementSignDo;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
public interface AgreementSignService {
	
	/**
	 * 添加.
	 * @param agreementSign
	 * @return
	 */
	public Integer insertAgreementSign(AgreementSignDo agreementSign);
	
	/**
	 * 获取数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementSignDo> queryAgreementSignList(Map<String, Object> selectItem);

	/**
	 * 获取数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<AgreementSignDo> queryAgreementSignListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据ContractNo修改.
	 * @param agreementSign
	 * @return
	 */
	public Integer updateAgreementSignByContractNo(AgreementSignDo agreementSign);

	/**
	 * 根据ContractNo删除.
	 * @param contractNo
	 * @return
	 */
	public Integer deleteAgreementSignByContractNo(String contractNo);

	/**
	 * 根据ContractNo获取.
	 * @param contractNo
	 * @return
	 */
	public AgreementSignDo findAgreementSignByContractNo(String contractNo);


	/**
	 * 更新上传合同PDF状态
	 * @param contractNo 合同编号
	 * @param status 值使用com.cwkj.scf.model.agreement.AgreementSignDo.STATUS_YES/STATUS_NO
	 * @return
	 */
	public Integer updateUploadPdfStatus(String contractNo,String status);

	/**
	 * 更新合同签章状态
	 * @param contractNo 合同编号
	 * @param status  值使用com.cwkj.scf.model.agreement.AgreementSignDo.STATUS_YES/STATUS_NO
	 * @return
	 */
	public Integer updateSignStatus(String contractNo,String status);

	/**
	 * 更新合同生效状态
	 * @param contractNo 合同编号
	 * @param status 值使用com.cwkj.scf.model.agreement.AgreementSignDo.STATUS_YES/STATUS_NO
	 * @return
	 */
	public Integer updateCompletionStatus(String contractNo,String status);
}
