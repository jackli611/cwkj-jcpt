package com.cwkj.scf.model.property;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 供应商利率.
 * @author ljc
 * @version  v1.0
 */
@Alias("supplierRateDo")
public class SupplierRateDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** ID. */
	private Integer id;

	/** 物业机构ID. */
	private String propertyOrgId;

	/** 产品ID. */
	private Integer productId;

	/** 等级. */
	private Integer levels;

	/** 利率. */
	private BigDecimal rate;

	/** 还款方式. */
	private Integer paymentType;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;


	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * 物业机构ID.
	 * @param propertyOrgId
	 * 物业机构ID
	 */
	public void setPropertyOrgId(String propertyOrgId) {
		this.propertyOrgId = propertyOrgId;
	}

	/**
	 * 物业机构ID.
	 * @return 物业机构ID
	 */
	public String getPropertyOrgId() {
		return this.propertyOrgId;
	}

	/**
	 * 产品ID.
	 * @param productId
	 * 产品ID
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * 产品ID.
	 * @return 产品ID
	 */
	public Integer getProductId() {
		return this.productId;
	}

	/**
	 * 等级.
	 * @param levels
	 * 等级
	 */
	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	/**
	 * 等级.
	 * @return 等级
	 */
	public Integer getLevels() {
		return this.levels;
	}

	/**
	 * 利率.
	 * @param rate
	 * 利率
	 */
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	/**
	 * 利率.
	 * @return 利率
	 */
	public BigDecimal getRate() {
		return this.rate;
	}

	/**
	 * 还款方式.
	 * @param paymentType
	 * 还款方式
	 */
	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * 还款方式.
	 * @return 还款方式
	 */
	public Integer getPaymentType() {
		return this.paymentType;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	@Override
	public String toString()
	{
		return "SupplierRateDo ["+",id="+id
+",propertyOrgId="+propertyOrgId
+",productId="+productId
+",levels="+levels
+",rate="+rate
+",paymentType="+paymentType
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
