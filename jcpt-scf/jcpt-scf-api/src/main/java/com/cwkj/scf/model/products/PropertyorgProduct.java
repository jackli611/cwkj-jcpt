package com.cwkj.scf.model.products;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PropertyorgProduct  implements Serializable{
	
	/*  产品名称 */
	private String productName;
	
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.assignId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private Integer assignid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.productCode
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private String productcode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.productName
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private String productname;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.propertyOrgId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private String propertyorgid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.repayName
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private String repayname;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.repayCode
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private String repaycode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.levels
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private String levels;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.rate
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private BigDecimal rate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.minPeroid
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private Short minperoid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.maxPeroid
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private Short maxperoid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.status
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private Short status;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.recordUserId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private Integer recorduserid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.createTime
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private Date createtime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column propertyorg_product.updateTime
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    private Date updatetime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.assignId
     *
     * @return the value of propertyorg_product.assignId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public Integer getAssignid() {
        return assignid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.assignId
     *
     * @param assignid the value for propertyorg_product.assignId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setAssignid(Integer assignid) {
        this.assignid = assignid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.productCode
     *
     * @return the value of propertyorg_product.productCode
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public String getProductcode() {
        return productcode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.productCode
     *
     * @param productcode the value for propertyorg_product.productCode
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.productName
     *
     * @return the value of propertyorg_product.productName
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public String getProductname() {
        return productname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.productName
     *
     * @param productname the value for propertyorg_product.productName
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setProductname(String productname) {
        this.productname = productname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.propertyOrgId
     *
     * @return the value of propertyorg_product.propertyOrgId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public String getPropertyorgid() {
        return propertyorgid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.propertyOrgId
     *
     * @param propertyorgid the value for propertyorg_product.propertyOrgId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setPropertyorgid(String propertyorgid) {
        this.propertyorgid = propertyorgid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.repayName
     *
     * @return the value of propertyorg_product.repayName
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public String getRepayname() {
        return repayname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.repayName
     *
     * @param repayname the value for propertyorg_product.repayName
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setRepayname(String repayname) {
        this.repayname = repayname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.repayCode
     *
     * @return the value of propertyorg_product.repayCode
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public String getRepaycode() {
        return repaycode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.repayCode
     *
     * @param repaycode the value for propertyorg_product.repayCode
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setRepaycode(String repaycode) {
        this.repaycode = repaycode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.levels
     *
     * @return the value of propertyorg_product.levels
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public String getLevels() {
        return levels;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.levels
     *
     * @param levels the value for propertyorg_product.levels
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setLevels(String levels) {
        this.levels = levels;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.rate
     *
     * @return the value of propertyorg_product.rate
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.rate
     *
     * @param rate the value for propertyorg_product.rate
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.minPeroid
     *
     * @return the value of propertyorg_product.minPeroid
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public Short getMinperoid() {
        return minperoid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.minPeroid
     *
     * @param minperoid the value for propertyorg_product.minPeroid
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setMinperoid(Short minperoid) {
        this.minperoid = minperoid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.maxPeroid
     *
     * @return the value of propertyorg_product.maxPeroid
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public Short getMaxperoid() {
        return maxperoid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.maxPeroid
     *
     * @param maxperoid the value for propertyorg_product.maxPeroid
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setMaxperoid(Short maxperoid) {
        this.maxperoid = maxperoid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.status
     *
     * @return the value of propertyorg_product.status
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public Short getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.status
     *
     * @param status the value for propertyorg_product.status
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.recordUserId
     *
     * @return the value of propertyorg_product.recordUserId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public Integer getRecorduserid() {
        return recorduserid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.recordUserId
     *
     * @param recorduserid the value for propertyorg_product.recordUserId
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setRecorduserid(Integer recorduserid) {
        this.recorduserid = recorduserid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.createTime
     *
     * @return the value of propertyorg_product.createTime
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.createTime
     *
     * @param createtime the value for propertyorg_product.createTime
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column propertyorg_product.updateTime
     *
     * @return the value of propertyorg_product.updateTime
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column propertyorg_product.updateTime
     *
     * @param updatetime the value for propertyorg_product.updateTime
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

	/**
	 * 产品名称
	 * @return  
	 */
	public String getProductName() {
		return productName;
	}
	

	/**
	 * 产品名称
	 * @param productName
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
    
    
}