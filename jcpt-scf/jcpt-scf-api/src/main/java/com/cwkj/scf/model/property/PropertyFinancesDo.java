package com.cwkj.scf.model.property;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 物业的资金机构.
 * @author ljc
 * @version  v1.0
 */
@Alias("propertyFinancesDo")
public class PropertyFinancesDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/**
	 * 状态：有效
	 */
	public static final int STATUS_VALID=1;
	/** 状态：无效 */
	public static final int STATUS_INVALID=2;

	/** ID. */
	private Integer id;

	/** 物业机构ID. */
	private String propertyId;

	/** 资金机构ID. */
	private String financeId;

	/** 授信额度. */
	private BigDecimal creditAmount;

	/** 匹配日期. */
	private Date metaDate;

	/** 还款方式. */
	private Integer paymentType;

	/** 担保方式. */
	private Integer guaranteeType;

	/** 有效状态. */
	private Integer status;

	/** 利息. */
	private BigDecimal interestRate;

	/** 附件. */
	private String attachment;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;
	
	
	/**
	 * 产品编码， 按产品授信
	 */
	private String productCode;
	/**
	 * 已使用额度
	 */
	private BigDecimal usedAmout;

	/**
	 * 总的授信额度
	 */
	private BigDecimal totalCreditAmt;
	

	//==========自定义===============
	/** 资金机构名称 */
	private String financeName;
	
	
	
	
	
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public BigDecimal getUsedAmout() {
		return usedAmout;
	}

	public void setUsedAmout(BigDecimal usedAmout) {
		this.usedAmout = usedAmout;
	}

	public BigDecimal getTotalCreditAmt() {
		return totalCreditAmt;
	}

	public void setTotalCreditAmt(BigDecimal totalCreditAmt) {
		this.totalCreditAmt = totalCreditAmt;
	}

	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * 物业机构ID.
	 * @param propertyId
	 * 物业机构ID
	 */
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * 物业机构ID.
	 * @return 物业机构ID
	 */
	public String getPropertyId() {
		return this.propertyId;
	}

	/**
	 * 资金机构ID.
	 * @param financeId
	 * 资金机构ID
	 */
	public void setFinanceId(String financeId) {
		this.financeId = financeId;
	}

	/**
	 * 资金机构ID.
	 * @return 资金机构ID
	 */
	public String getFinanceId() {
		return this.financeId;
	}

	/**
	 * 授信额度.
	 * @param creditAmount
	 * 授信额度
	 */
	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	/**
	 * 授信额度.
	 * @return 授信额度
	 */
	public BigDecimal getCreditAmount() {
		return this.creditAmount;
	}

	/**
	 * 匹配日期.
	 * @param metaDate
	 * 匹配日期
	 */
	public void setMetaDate(Date metaDate) {
		this.metaDate = metaDate;
	}

	/**
	 * 匹配日期.
	 * @return 匹配日期
	 */
	public Date getMetaDate() {
		return this.metaDate;
	}

	/**
	 * 还款方式.
	 * @param paymentType
	 * 还款方式
	 */
	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * 还款方式.
	 * @return 还款方式
	 */
	public Integer getPaymentType() {
		return this.paymentType;
	}

	/**
	 * 担保方式.
	 * @param guaranteeType
	 * 担保方式
	 */
	public void setGuaranteeType(Integer guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	/**
	 * 担保方式.
	 * @return 担保方式
	 */
	public Integer getGuaranteeType() {
		return this.guaranteeType;
	}

	/**
	 * 有效状态.
	 * @param status
	 * 有效状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 有效状态.
	 * @return 有效状态
	 */
	public Integer getStatus() {
		return this.status;
	}

	/**
	 * 利息.
	 * @param interestRate
	 * 利息
	 */
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	/**
	 * 利息.
	 * @return 利息
	 */
	public BigDecimal getInterestRate() {
		return this.interestRate;
	}

	/**
	 * 附件.
	 * @param attachment
	 * 附件
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	/**
	 * 附件.
	 * @return 附件
	 */
	public String getAttachment() {
		return this.attachment;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}
	
	


	/**
	 * 资金机构名称
	 * @return
	 */
	public String getFinanceName() {
		return financeName;
	}

	/**
	 * 资金机构名称
	 * @param financeName 资金机构名称
	 */
	public void setFinanceName(String financeName) {
		this.financeName = financeName;
	}

	@Override
	public String toString()
	{
		return "PropertyFinancesDo ["+",id="+id
+",propertyId="+propertyId
+",financeId="+financeId
+",creditAmount="+creditAmount
+",metaDate="+metaDate
+",paymentType="+paymentType
+",guaranteeType="+guaranteeType
+",status="+status
+",interestRate="+interestRate
+",attachment="+attachment
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
