package com.cwkj.scf.service.config;

/**
 * 上传文件配置
 * @author ljc
 *
 */
public class UploadFileConfig {

	/** 文件保存根目录路径 **/
	private String storeRootPath;

	/**
	 * 文件保存根目录路径
	 * @return 
	 */
	public String getStoreRootPath() {
		return storeRootPath;
	}

	/**
	 * 文件保存根目录路径
	 * @param storeRootPath 
	 */
	public void setStoreRootPath(String storeRootPath) {
		this.storeRootPath = storeRootPath;
	}
 
	
	
	
}
