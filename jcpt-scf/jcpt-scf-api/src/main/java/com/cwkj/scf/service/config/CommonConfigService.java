package com.cwkj.scf.service.config;

/**
 * @author ljc
 *
 */
public interface CommonConfigService {

	UploadFileConfig getUploadFileConfig();
	
}
