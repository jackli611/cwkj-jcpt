package com.cwkj.scf.service.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.SupplierAuditDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.model.property.SupplierInvitationDo;

/**
 * 供应商.
 * @author ljc
 * @version  v1.0
 */
public interface SupplierService {
	
	/**
	 * 添加供应商.
	 * @param supplier
	 * @return
	 */
	public Integer insertSupplier(SupplierDo supplier);
	
	/**
	 * 获取供应商数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierDo> querySupplierList(Map<String,Object> selectItem);
	
	/**
	 * 	根据登录用户id获取供应商对象
	 * @param selectItem
	 * @return
	 */
	public SupplierDo findSupplierByUserId(Integer userId);

	/**
	 * 获取供应商数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<SupplierDo> querySupplierListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改供应商.
	 * @param supplier
	 * @return
	 */
	public Integer updateSupplierById(SupplierDo supplier);

	/**
	 * 根据Id删除供应商.
	 * @param id
	 * @return
	 */
	public Integer deleteSupplierById(String id);

	/**
	 * 根据Id获取供应商.
	 * @param id
	 * @return
	 */
	public SupplierDo findSupplierById(String id);	
	
	/**
	 * 修改状态
	 * @param id 供应商ID
	 * @param status 合作状态
	 * @return
	 */
	public Integer updateStatus(String id, Integer status);

	/**
	 * 修改平台审批状态
	 * @param id 供应商ID
	 * @param platformStatus 平台审批状态
	 * @return
	 */
	public  Integer updatePlatformStatus(String id,Integer platformStatus);
	
 
	/**
	 * 保存营业执照
	 * @param id 供应商ID
	 * @param businessLicense 营业执照
	 * @return
	 */
	public Integer saveBusinessLicense(String id,String businessLicense);
 
	/**
	 * 保存系统账号
	 * @param id 供应商ID
	 * @param userId 用户ID
	 * @return
	 */
	public Integer addSysUserId(String id,Integer userId);
	
	/**
	 * 注册供应商
	 * @param supplier
	 * @return
	 */
	public Integer registerSupplier(SupplierDo supplier,String password);
	
	/**
	 * 审批供应商状态
	 * @param supplierAudit
	 * @return
	 */
	public Integer auditSupplierStatus(SupplierAuditDo supplierAudit);

	/**
	 * 平台审批供应商状态
	 * @param supplierAudit
	 * @return
	 */
	public Integer auditSupplierPlatformStatus(SupplierAuditDo supplierAudit);
	
	/**
	 * 获取供应商注册审批列表
	 * @param supplierId
	 * @return
	 */
	public List<SupplierAuditDo> findSupplierAuditList(String supplierId);
	
	/**
	 * 根据Id修改供应商.
	 * @param supplier
	 * @return
	 */
	public Integer updateByPrimaryKeySelective(SupplierDo supplier);
	
}
