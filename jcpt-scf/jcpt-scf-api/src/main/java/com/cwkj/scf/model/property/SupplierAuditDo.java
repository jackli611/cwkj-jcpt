package com.cwkj.scf.model.property;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 供应商审核.
 * @author ljc
 * @version  v1.0
 */
@Alias("supplierAuditDo")
public class SupplierAuditDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** ID. */
	private Integer id;

	/** 供应商ID. */
	private String supplierId;

	/** 物业机构ID. */
	private String propertyId;

	/** 有效状态. */
	private Integer status;

	/** 备注. */
	private String remark;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;
	/**
	 * 审核时给的供应商等级
	 */
	private String level;


	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * 供应商ID.
	 * @param supplierId
	 * 供应商ID
	 */
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	/**
	 * 供应商ID.
	 * @return 供应商ID
	 */
	public String getSupplierId() {
		return this.supplierId;
	}

	/**
	 * 物业机构ID.
	 * @param propertyId
	 * 物业机构ID
	 */
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * 物业机构ID.
	 * @return 物业机构ID
	 */
	public String getPropertyId() {
		return this.propertyId;
	}

	/**
	 * 有效状态.
	 * @param status
	 * 有效状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 有效状态.
	 * @return 有效状态
	 */
	public Integer getStatus() {
		return this.status;
	}

	/**
	 * 备注.
	 * @param remark
	 * 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 备注.
	 * @return 备注
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public String toString()
	{
		return "SupplierAuditDo ["+",id="+id
+",supplierId="+supplierId
+",propertyId="+propertyId
+",status="+status
+",remark="+remark
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
