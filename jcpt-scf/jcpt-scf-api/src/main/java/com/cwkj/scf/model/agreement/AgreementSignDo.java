package com.cwkj.scf.model.agreement;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
@Alias("agreementSignDo")
public class AgreementSignDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	public static final String STATUS_YES="Y";
	public static final String STATUS_NOT="N";

	/** contractNo. */
	private String contractNo;

	/** 上传PDF状态. */
	private String uploadPdfStatus;

	/** 签章状态. */
	private String signStatus;

	/**
	 * 合同生效状态
	 */
	private String completionStatus;

	/**
	 * 合同模板文件
	 */
	private String templateFile;

	/**
	 * 合同签名数据
	 */
	private String signData;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;


	/**
	 * contractNo.
	 * @param contractNo
	 * contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	/**
	 * contractNo.
	 * @return contractNo
	 */
	public String getContractNo() {
		return this.contractNo;
	}

	/**
	 * 上传PDF状态.
	 * @param uploadPdfStatus
	 * 上传PDF状态
	 */
	public void setUploadPdfStatus(String uploadPdfStatus) {
		this.uploadPdfStatus = uploadPdfStatus;
	}

	/**
	 * 上传PDF状态.
	 * @return 上传PDF状态
	 */
	public String getUploadPdfStatus() {
		return this.uploadPdfStatus;
	}

	/**
	 * 签章状态.
	 * @param signStatus
	 * 签章状态
	 */
	public void setSignStatus(String signStatus) {
		this.signStatus = signStatus;
	}

	/**
	 * 签章状态.
	 * @return 签章状态
	 */
	public String getSignStatus() {
		return this.signStatus;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 合同生效状态
	 * @return
	 */
	public String getCompletionStatus() {
		return completionStatus;
	}

	/**
	 * 合同生效状态
	 * @param completionStatus
	 */
	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}

	/**
	 * 合同模板文件名
	 * @return
	 */
	public String getTemplateFile() {
		return templateFile;
	}

	/**
	 * 合同模板文件名
	 * @param templateFile
	 */
	public void setTemplateFile(String templateFile) {
		this.templateFile = templateFile;
	}

	/**
	 * 签名数据
	 * @return
	 */
	public String getSignData() {
		return signData;
	}

	/**
	 * 签名数据
	 * @param signData
	 */
	public void setSignData(String signData) {
		this.signData = signData;
	}

	@Override
	public String toString()
	{
		return "AgreementSignDo ["+",contractNo="+contractNo
+",uploadPdfStatus="+uploadPdfStatus
+",signStatus="+signStatus
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
