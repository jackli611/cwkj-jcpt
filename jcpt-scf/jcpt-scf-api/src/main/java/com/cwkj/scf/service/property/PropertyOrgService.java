package com.cwkj.scf.service.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.model.property.PropertyFinancesDo;
import com.cwkj.scf.model.property.PropertyOrgDo;

/**
 * 物业机构.
 * @author ljc
 * @version  v1.0
 */
public interface PropertyOrgService {
	
	/**
	 * 添加物业机构.
	 * @param propertyOrg
	 * @return
	 */
	public Integer insertPropertyOrg(PropertyOrgDo propertyOrg);
	
	/**
	 * 获取物业机构数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyOrgDo> queryPropertyOrgList(Map<String,Object> selectItem);

	/**
	 * 获取物业机构数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PropertyOrgDo> queryPropertyOrgListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改物业机构.
	 * @param propertyOrg
	 * @return
	 */
	public Integer updatePropertyOrgById(PropertyOrgDo propertyOrg);

	/**
	 * 根据Id删除物业机构.
	 * @param id
	 * @return
	 */
	public Integer deletePropertyOrgById(String id);

	/**
	 * 根据Id获取物业机构.
	 * @param id
	 * @return
	 */
	public PropertyOrgDo findPropertyOrgById(String id);	
	
	/**
	 * 查找完整的物业机构信息
	 * @param id 物业机构ID
	 * @return
	 */
	public PropertyOrgDo findFullPropertyOrgById(String id);
	
	/**
	 * 修改物业基本信息
	 * @param propertyOrg
	 * @return
	 */
	public Integer updatePropertyNameInfo(PropertyOrgDo propertyOrg);

	/**
	 * 修改营业执照信息
	 * @param id
	 * @param businessLicense
	 * @return
	 */
	public Integer updateBusinessLicense(String id,String businessLicense);
	
	/**
	 * 修改LOGO
	 * @param id
	 * @param logo
	 * @return
	 */
	public Integer updateLogo(String id ,String logo);
	
	/**
	 * 修改记录状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(String id,Integer status);
	
	/**
	 * 添加系统账号信息
	 * @param propertyOrg
	 * @return
	 */
	public Integer addSysUser(PropertyOrgDo propertyOrg);
	
	
	
	/**
	 * 查询产品列表
	 * @param propertyOrgId 物业机构ID
	 * @return
	 */
	public List<ProductDo> queryProductList(String propertyOrgId);
	
	/**
	 * 查找产品
	 * @param propertyOrgId 物业机构ID
	 * @param productId 产品code
	 * 供应商等级
	 * @return
	 */
	public List<PropertyorgProduct> findProduct(Map<String,Object> selectItem);
	
	/**
	 * 物业的资金方列表
	 * @param propertyId 物业ID
	 * @return
	 */
	public List<PropertyFinancesDo> findPropertyFinanceList(String propertyId);
	
	/**
	 * 添加资金方
	 * @param propertyFinances
	 * @return
	 */
	public Integer addPropertyFinances(PropertyFinancesDo propertyFinances);
	
	/**
	 * 修改资金合作方内容
	 * @param propertyFinances
	 * @return
	 */
	public Integer updatePropertyFinances(PropertyFinancesDo propertyFinances);
	
	/**
	 * 删除资金合作方
	 * @param propertyFinanceId
	 * @return
	 */
	public Integer deletePropertyFinances(Integer propertyFinanceId);
	
	/**
	 * 修改资金方合作状态
	 * @param propertyFinanceId
	 * @param status
	 * @return
	 */
	public Integer updatePropertyFinanceStatus(Integer propertyFinanceId,Integer status);
}
