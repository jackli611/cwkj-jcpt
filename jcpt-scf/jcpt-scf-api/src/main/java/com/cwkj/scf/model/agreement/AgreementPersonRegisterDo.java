package com.cwkj.scf.model.agreement;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 签章个人用户注册.
 * @author ljc
 * @version  v1.0
 */
@Alias("agreementPersonRegisterDo")
public class AgreementPersonRegisterDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键ID. */
	private String id;

	/** 身份证号码. */
	private String idcardNo;

	/** 姓名. */
	private String realName;

	/** 手机号. */
	private String mobile;

	/** 用户ID. */
	private Integer userId;

	/** 创建时间. */
	private Date createTime;

	/** 更新时间. */
	private Date updateTime;


	/**
	 * 主键ID.
	 * @param id
	 * 主键ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 主键ID.
	 * @return 主键ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 身份证号码.
	 * @param idcardNo
	 * 身份证号码
	 */
	public void setIdcardNo(String idcardNo) {
		this.idcardNo = idcardNo;
	}

	/**
	 * 身份证号码.
	 * @return 身份证号码
	 */
	public String getIdcardNo() {
		return this.idcardNo;
	}

	/**
	 * 姓名.
	 * @param realName
	 * 姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * 姓名.
	 * @return 姓名
	 */
	public String getRealName() {
		return this.realName;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 用户ID.
	 * @param userId
	 * 用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 用户ID.
	 * @return 用户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 更新时间.
	 * @param updateTime
	 * 更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 更新时间.
	 * @return 更新时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	@Override
	public String toString()
	{
		return "AgreementPersonRegisterDo ["+",id="+id
+",idcardNo="+idcardNo
+",realName="+realName
+",mobile="+mobile
+",userId="+userId
+",createTime="+createTime
+",updateTime="+updateTime
+"]";
	}

}
