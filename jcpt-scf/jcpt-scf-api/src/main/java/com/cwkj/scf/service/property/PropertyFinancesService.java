package com.cwkj.scf.service.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.PropertyFinancesDo;

/**
 * 物业的资金机构.
 * @author ljc
 * @version  v1.0
 */
public interface PropertyFinancesService {
	
	/**
	 * 添加物业的资金机构.
	 * @param propertyFinances
	 * @return
	 */
	public Integer insertPropertyFinances(PropertyFinancesDo propertyFinances);
	
	/**
	 * 获取物业的资金机构数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyFinancesDo> queryPropertyFinancesList(Map<String,Object> selectItem);

	/**
	 * 获取物业的资金机构数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PropertyFinancesDo> queryPropertyFinancesListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改物业的资金机构.
	 * @param propertyFinances
	 * @return
	 */
	public Integer updatePropertyFinancesById(PropertyFinancesDo propertyFinances);

	/**
	 * 根据Id删除物业的资金机构.
	 * @param id
	 * @return
	 */
	public Integer deletePropertyFinancesById(Integer id);

	/**
	 * 根据Id获取物业的资金机构.
	 * @param id
	 * @return
	 */
	public PropertyFinancesDo findPropertyFinancesById(Integer id);

	List<PropertyFinancesDo> queryListByPropertyId(String propertyId);

	/**
	 * 金融机构匹配订单
	 * @param loanId
	 * @param financeId
	 * @param propertyFinanceId
	 * @param optUserId
	 * @return
	 */
	public int saveMatchOrder(Long loanId, String financeId, String propertyFinanceId, Integer optUserId);

	/**
	 * 查询订单匹配记录
	 * @param selectItem
	 * @return
	 */
	public List<Map<String, Object>> selectMatchLog(Map<String, Object> selectItem);

		
}
