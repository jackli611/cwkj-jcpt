package com.cwkj.scf.model.property;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付信息.
 * @author ljc
 * @version  v1.0
 */
@Alias("payInfoDo")
public class PayInfoDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** ID. */
	private String id;

	/** 批次号. */
	private String batchId;

	/** 应付单号. */
	private String payNo;

	/** 到期日. */
	private Date payDate;

	/** 企业ID. */
	private String companyId;

	/** 供应商ID. */
	private String supplierId;

	/** 应付金额. */
	private BigDecimal amount;

	/** 支付状态. */
	private Integer payStatus;

	/** 合同编号. */
	private String contractNo;

	/** 合同日期. */
	private Date contractDate;

	/** 合同金额. */
	private BigDecimal contractAmount;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;
	
	//==============自定义字段=====================
	/** 企业名称 */
	private String companyName;
	/** 供应商名称 */
	private String supplierName;


	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 批次号.
	 * @param batchId
	 * 批次号
	 */
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	/**
	 * 批次号.
	 * @return 批次号
	 */
	public String getBatchId() {
		return this.batchId;
	}

	/**
	 * 应付单号.
	 * @param payNo
	 * 应付单号
	 */
	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	/**
	 * 应付单号.
	 * @return 应付单号
	 */
	public String getPayNo() {
		return this.payNo;
	}

	/**
	 * 到期日.
	 * @param payDate
	 * 到期日
	 */
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	/**
	 * 到期日.
	 * @return 到期日
	 */
	public Date getPayDate() {
		return this.payDate;
	}

	/**
	 * 企业ID.
	 * @param companyId
	 * 企业ID
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/**
	 * 企业ID.
	 * @return 企业ID
	 */
	public String getCompanyId() {
		return this.companyId;
	}

	/**
	 * 供应商ID.
	 * @param supplierId
	 * 供应商ID
	 */
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	/**
	 * 供应商ID.
	 * @return 供应商ID
	 */
	public String getSupplierId() {
		return this.supplierId;
	}

	/**
	 * 应付金额.
	 * @param amount
	 * 应付金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * 应付金额.
	 * @return 应付金额
	 */
	public BigDecimal getAmount() {
		return this.amount;
	}

	/**
	 * 支付状态.
	 * @param payStatus
	 * 支付状态
	 */
	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	/**
	 * 支付状态.
	 * @return 支付状态
	 */
	public Integer getPayStatus() {
		return this.payStatus;
	}

	/**
	 * 合同编号.
	 * @param contractNo
	 * 合同编号
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	/**
	 * 合同编号.
	 * @return 合同编号
	 */
	public String getContractNo() {
		return this.contractNo;
	}

	/**
	 * 合同日期.
	 * @param contractDate
	 * 合同日期
	 */
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	/**
	 * 合同日期.
	 * @return 合同日期
	 */
	public Date getContractDate() {
		return this.contractDate;
	}

	/**
	 * 合同金额.
	 * @param contractAmount
	 * 合同金额
	 */
	public void setContractAmount(BigDecimal contractAmount) {
		this.contractAmount = contractAmount;
	}

	/**
	 * 合同金额.
	 * @return 合同金额
	 */
	public BigDecimal getContractAmount() {
		return this.contractAmount;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}

	

	/**
	 * 企业名称
	 * @return  
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * 企业名称
	 * @param companyName 企业名称
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * 供应商名称
	 * @return  
	 */
	public String getSupplierName() {
		return supplierName;
	}

	/**
	 * 供应商名称
	 * @param supplierName 供应商名称
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	@Override
	public String toString()
	{
		return "PayInfoDo ["+",id="+id
+",batchId="+batchId
+",payNo="+payNo
+",payDate="+payDate
+",companyId="+companyId
+",supplierId="+supplierId
+",amount="+amount
+",payStatus="+payStatus
+",contractNo="+contractNo
+",contractDate="+contractDate
+",contractAmount="+contractAmount
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
