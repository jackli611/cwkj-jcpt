package com.cwkj.scf.model.finances;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 金融机构用户.
 * @author ljc
 * @version  v1.0
 */
@Alias("financeUserDo")
public class FinanceUserDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** ID. */
	private String id;

	/** 系统账户ID. */
	private Integer userId;

	/** 父级账户ID. */
	private Integer parentUserId;

	/** 金融机构ID. */
	private String financeId;

	/** 用户姓名. */
	private String realName;

	/** 手机号. */
	private String mobile;

	/** 邮箱. */
	private String email;

	/** 性别. */
	private Integer sex;

	/** 可创建账户标识. */
	private Integer buildUserTag;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;

	//============自定义==================
	/** 父级用户姓名 **/
	private String parentRealName;
	/** 用户账户名 */
	private String userName;
		
	/**
	 * ID.
	 * @param id
	 * ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * ID.
	 * @return ID
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 系统账户ID.
	 * @param userId
	 * 系统账户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 系统账户ID.
	 * @return 系统账户ID
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * 父级账户ID.
	 * @param parentUserId
	 * 父级账户ID
	 */
	public void setParentUserId(Integer parentUserId) {
		this.parentUserId = parentUserId;
	}

	/**
	 * 父级账户ID.
	 * @return 父级账户ID
	 */
	public Integer getParentUserId() {
		return this.parentUserId;
	}

	/**
	 * 金融机构ID.
	 * @param financeId
	 * 金融机构ID
	 */
	public void setFinanceId(String financeId) {
		this.financeId = financeId;
	}

	/**
	 * 金融机构ID.
	 * @return 金融机构ID
	 */
	public String getFinanceId() {
		return this.financeId;
	}

	/**
	 * 用户姓名.
	 * @param realName
	 * 用户姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * 用户姓名.
	 * @return 用户姓名
	 */
	public String getRealName() {
		return this.realName;
	}

	/**
	 * 手机号.
	 * @param mobile
	 * 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 手机号.
	 * @return 手机号
	 */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * 邮箱.
	 * @param email
	 * 邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 邮箱.
	 * @return 邮箱
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * 性别.
	 * @param sex
	 * 性别
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * 性别.
	 * @return 性别
	 */
	public Integer getSex() {
		return this.sex;
	}

	/**
	 * 可创建账户标识.
	 * @param buildUserTag
	 * 可创建账户标识
	 */
	public void setBuildUserTag(Integer buildUserTag) {
		this.buildUserTag = buildUserTag;
	}

	/**
	 * 可创建账户标识.
	 * @return 可创建账户标识
	 */
	public Integer getBuildUserTag() {
		return this.buildUserTag;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	/**
	 * 上级用户姓名
	 * @return  
	 */
	public String getParentRealName() {
		return parentRealName;
	}

	/**
	 * 上级用户姓名
	 * @param parentRealName 上级用户姓名
	 */
	public void setParentRealName(String parentRealName) {
		this.parentRealName = parentRealName;
	}

	/**
	 * 账户名称
	 * @return  
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 账户名称
	 * @param userName 账户名称
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	@Override
	public String toString()
	{
		return "FinanceUserDo ["+",id="+id
+",userId="+userId
+",parentUserId="+parentUserId
+",financeId="+financeId
+",realName="+realName
+",mobile="+mobile
+",email="+email
+",sex="+sex
+",buildUserTag="+buildUserTag
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
