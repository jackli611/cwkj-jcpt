package com.cwkj.scf.model.products;

		import org.apache.ibatis.type.Alias;
		import java.io.Serializable;
		import java.util.Date;

/**
 * 产品.
 * @author ljc
 * @version  v1.0
 */
@Alias("productDo")
public class ProductDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 产品编码. */
	private String productCode;
	
	/**
	 * 上級产品编码
	 */
	private String 	parentCode;
	

	/** 产品名称. */
	private String productName;

	/** 级别. */
	private Integer levels;

	/** 企业产品. */
	private Integer enterpriseProduct;

	/** 个人产品. */
	private Integer personalProduct;

	

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 创建者ID. */
	private Integer recordId;

	/**
	 * 产品编码.
	 * @param productCode
	 * 产品编码
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * 产品编码.
	 * @return 产品编码
	 */
	public String getProductCode() {
		return this.productCode;
	}

	/**
	 * 产品名称.
	 * @param productName
	 * 产品名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 产品名称.
	 * @return 产品名称
	 */
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 级别.
	 * @param levels
	 * 级别
	 */
	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	/**
	 * 级别.
	 * @return 级别
	 */
	public Integer getLevels() {
		return this.levels;
	}

	/**
	 * 企业产品.
	 * @param enterpriseProduct
	 * 企业产品
	 */
	public void setEnterpriseProduct(Integer enterpriseProduct) {
		this.enterpriseProduct = enterpriseProduct;
	}

	/**
	 * 企业产品.
	 * @return 企业产品
	 */
	public Integer getEnterpriseProduct() {
		return this.enterpriseProduct;
	}

	/**
	 * 个人产品.
	 * @param personalProduct
	 * 个人产品
	 */
	public void setPersonalProduct(Integer personalProduct) {
		this.personalProduct = personalProduct;
	}

	/**
	 * 个人产品.
	 * @return 个人产品
	 */
	public Integer getPersonalProduct() {
		return this.personalProduct;
	}

	

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 创建者ID.
	 * @param recordId
	 * 创建者ID
	 */
	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}

	/**
	 * 创建者ID.
	 * @return 创建者ID
	 */
	public Integer getRecordId() {
		return this.recordId;
	}


	@Override
	public String toString()
	{
		return "ProductDo ["+",productCode="+productCode
				+",productName="+productName
				+",levels="+levels
				+",enterpriseProduct="+enterpriseProduct
				+",personalProduct="+personalProduct
				+",parentCode="+parentCode
				+",createTime="+createTime
				+",updateTime="+updateTime
				+",recordId="+recordId
				+"]";
	}

}
