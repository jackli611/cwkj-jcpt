package com.cwkj.scf.service.merchant;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.merchant.MerchantAccountDo;
import com.cwkj.scf.model.merchant.MerchantProfileDo;

/**
 * 商户账号.
 * @author ljc
 * @version  v1.0
 */
public interface MerchantAccountService {
	
	/**
	 * 	微信登录绑定 openid
	 * 
	 * @param merchantAccountDo
	 * @param merchantProfile
	 * @param shareId 分享ID
	 * @return
	 */
	public MerchantAccountDo bindWxOpenId(MerchantAccountDo merchantAccountDo, 
										  MerchantProfileDo merchantProfile,Long shareId);
	
	/**
	 * 添加商户账号.
	 * @param merchantAccount
	 * @return
	 */
	public Integer insertMerchantAccount(MerchantAccountDo merchantAccount);
	
	/**
	 * 获取商户账号数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantAccountDo> queryMerchantAccountList(Map<String, Object> selectItem);

	/**
	 * 获取商户账号数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<MerchantAccountDo> queryMerchantAccountListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改商户账号.
	 * @param merchantAccount
	 * @return
	 */
	public Integer updateMerchantAccountById(MerchantAccountDo merchantAccount);

	/**
	 * 根据Id删除商户账号.
	 * @param id
	 * @return
	 */
	public Integer deleteMerchantAccountById(String id);

	/**
	 * 根据Id获取商户账号.
	 * @param id
	 * @return
	 */
	public MerchantAccountDo findMerchantAccountById(String id);

	/**
	 * 核对商户账号
	 * @param merchantAccountDo
	 * @param merchantProfile
	 * @param shareId
	 * @return
	 */
	public MerchantAccountDo checkMerchantAccount(MerchantAccountDo merchantAccountDo, MerchantProfileDo merchantProfile,Long shareId);

	/**
	 * 找微信的 merchantAccountDo
	 * @param id
	 * @return
	 */
	public MerchantAccountDo findWxAccountByUserId(Integer id);

}
