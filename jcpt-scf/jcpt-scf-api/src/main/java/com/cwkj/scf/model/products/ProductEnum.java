package com.cwkj.scf.model.products;

public enum ProductEnum {
	
	PROD_SCF("SCF","供应链"),
	PROD_PERSONAL_XY("PERSONAL_XY","个贷-信用贷");
	
	private String productCode;
    private String productName;

    private ProductEnum(String productCode, String productName) {
        this.productCode = productCode;
        this.productName = productName;
    }

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	

}
