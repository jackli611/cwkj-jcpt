package com.cwkj.scf.model.products;

import org.apache.ibatis.type.Alias;
import java.io.Serializable;
import java.util.Date;

/**
 * 资金机构个贷产品.
 * @author ljc
 * @version  v1.0
 */
@Alias("personalLoanProductDo")
public class PersonalLoanProductDo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 主键. */
	private Long id;

	/** 资金机构ID. */
	private String financeId;

	/** 产品名称. */
	private String productName;

	/** 产品类型. */
	private Integer productType;

	/** 客户类型. */
	private String customerType;

	/** 费用说明. */
	private String fees;

	/** 征信. */
	private String credit;

	/** 办理说明. */
	private String transaction;

	/** 创建时间. */
	private Date createTime;

	/** 修改时间. */
	private Date updateTime;

	/** 操作记录ID. */
	private Integer recordUserId;


	/**
	 * 主键.
	 * @param id
	 * 主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 主键.
	 * @return 主键
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * 资金机构ID.
	 * @param financeId
	 * 资金机构ID
	 */
	public void setFinanceId(String financeId) {
		this.financeId = financeId;
	}

	/**
	 * 资金机构ID.
	 * @return 资金机构ID
	 */
	public String getFinanceId() {
		return this.financeId;
	}

	/**
	 * 产品名称.
	 * @param productName
	 * 产品名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 产品名称.
	 * @return 产品名称
	 */
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 产品类型.
	 * @param productType
	 * 产品类型
	 */
	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	/**
	 * 产品类型.
	 * @return 产品类型
	 */
	public Integer getProductType() {
		return this.productType;
	}

	/**
	 * 客户类型.
	 * @param customerType
	 * 客户类型
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * 客户类型.
	 * @return 客户类型
	 */
	public String getCustomerType() {
		return this.customerType;
	}

	/**
	 * 费用说明.
	 * @param fees
	 * 费用说明
	 */
	public void setFees(String fees) {
		this.fees = fees;
	}

	/**
	 * 费用说明.
	 * @return 费用说明
	 */
	public String getFees() {
		return this.fees;
	}

	/**
	 * 征信.
	 * @param credit
	 * 征信
	 */
	public void setCredit(String credit) {
		this.credit = credit;
	}

	/**
	 * 征信.
	 * @return 征信
	 */
	public String getCredit() {
		return this.credit;
	}

	/**
	 * 办理说明.
	 * @param transaction
	 * 办理说明
	 */
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	/**
	 * 办理说明.
	 * @return 办理说明
	 */
	public String getTransaction() {
		return this.transaction;
	}

	/**
	 * 创建时间.
	 * @param createTime
	 * 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 修改时间.
	 * @param updateTime
	 * 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 修改时间.
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * 操作记录ID.
	 * @param recordUserId
	 * 操作记录ID
	 */
	public void setRecordUserId(Integer recordUserId) {
		this.recordUserId = recordUserId;
	}

	/**
	 * 操作记录ID.
	 * @return 操作记录ID
	 */
	public Integer getRecordUserId() {
		return this.recordUserId;
	}


	@Override
	public String toString()
	{
		return "PersonalLoanProductDo ["+",id="+id
+",financeId="+financeId
+",productName="+productName
+",productType="+productType
+",customerType="+customerType
+",fees="+fees
+",credit="+credit
+",transaction="+transaction
+",createTime="+createTime
+",updateTime="+updateTime
+",recordUserId="+recordUserId
+"]";
	}

}
