package com.cwkj.scf.service.agreement;


import java.util.Map;

/**
 * 个贷合同
 */
public interface PersonalLoanAgreementService {

    /**
     * 根据LoanId获取个贷居间合同数据
     * @param loanId
     * @return
     */
    public Map<String,String> serverContractDataByLoanId(Long loanId);


}
