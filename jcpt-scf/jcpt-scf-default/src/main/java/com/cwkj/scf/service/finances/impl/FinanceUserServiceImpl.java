package com.cwkj.scf.service.finances.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.component.finances.FinanceUserComponent;
import com.cwkj.scf.model.finances.FinanceUserDo;
import com.cwkj.scf.service.finances.FinanceUserService;

/**
 * 金融机构用户.
 * @author ljc
 * @version  v1.0
 */
@Component("financeUserService")
public class FinanceUserServiceImpl  implements FinanceUserService{
	
	private FinanceUserComponent financeUserComponent;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	
	@Autowired
	public void setFinanceUserComponent(FinanceUserComponent financeUserComponent)
	{
		this.financeUserComponent=financeUserComponent;
	}
	
	
	@Override
	public Integer insertFinanceUser(FinanceUserDo financeUser)
	{
		Date now=new Date();
		financeUser.setCreateTime(now);
		financeUser.setUpdateTime(now);
		financeUser.setId(sysSeqNumberService.daySeqnumberString("FU"));
		return financeUserComponent.insertFinanceUser(financeUser);
	}
	
	@Override
	public List<FinanceUserDo> queryFinanceUserList(Map<String,Object> selectItem)
	{
		return financeUserComponent.queryFinanceUserList(selectItem);
	}

	@Override
	public PageDo<FinanceUserDo> queryFinanceUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return financeUserComponent.queryFinanceUserListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateFinanceUserById(FinanceUserDo financeUser)
	{
		Date now=new Date();
		financeUser.setUpdateTime(now);
		return financeUserComponent.updateFinanceUserById(financeUser);
	}

	@Override
	public Integer deleteFinanceUserById(String id)
	{
		return financeUserComponent.deleteFinanceUserById(id);
	}

	@Override
	public FinanceUserDo findFinanceUserById(String id)
	{
		return financeUserComponent.findFinanceUserById(id);
	}


	@Override
	public PageDo<FinanceUserDo> queryWithSysUserListPage(Long pageIndex, Integer pageSize,
			Map<String, Object> selectItem) {
		return financeUserComponent.queryWithSysUserListPage(pageIndex, pageSize, selectItem);
	}	
}
