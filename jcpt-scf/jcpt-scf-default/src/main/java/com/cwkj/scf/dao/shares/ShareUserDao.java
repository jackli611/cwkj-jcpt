package com.cwkj.scf.dao.shares;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.shares.ShareUserDo;

/**
 * 分享用户.
 * @author ljc
 * @version  v1.0
 */
@Repository("shareUserDao")
public interface ShareUserDao{
	
	/**
	 * 添加分享用户.
	 * @param shareUser
	 * @return
	 */
	public Integer insert(ShareUserDo shareUser);
	
	/**
	 * 获取分享用户数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ShareUserDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取分享用户数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<ShareUserDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改分享用户.
	 * @param shareUser
	 * @return
	 */
	public Integer updateById(ShareUserDo shareUser);

	/**
	 * 根据Id删除分享用户.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Long id);

	/**
	 * 根据Id获取分享用户.
	 * @param id
	 * @return
	 */
	public ShareUserDo findById(Long id);

	/**
	 * 根据用户ID获取分享用户
	 * @param userId
	 * @return
	 */
	public ShareUserDo findShareUserByUserId(Integer userId);
 	
	 
}
