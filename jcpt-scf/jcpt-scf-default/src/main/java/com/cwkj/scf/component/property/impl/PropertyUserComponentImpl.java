package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.PropertyUserComponent;
import com.cwkj.scf.dao.property.PropertyUserDao;
import com.cwkj.scf.model.property.PropertyUserDo;

/**
 * 物业所属用户.
 * @author ljc
 * @version  v1.0
 */
@Component("propertyUserComponent")
public class PropertyUserComponentImpl  implements PropertyUserComponent{
	
	private PropertyUserDao propertyUserDao;
	
	@Autowired
	public void setPropertyUserDao(PropertyUserDao propertyUserDao)
	{
		this.propertyUserDao=propertyUserDao;
	}
	
	
	@Override
	public Integer insertPropertyUser(PropertyUserDo propertyUser)
	{
		return propertyUserDao.insert(propertyUser);
	}
	
	@Override
	public List<PropertyUserDo> queryPropertyUserList(Map<String,Object> selectItem)
	{
		return propertyUserDao.queryList(selectItem);
	}

	@Override
	public PageDo<PropertyUserDo> queryPropertyUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PropertyUserDo> pageBean=new PageDo<PropertyUserDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(propertyUserDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updatePropertyUserById(PropertyUserDo propertyUser)
	{
		return propertyUserDao.updateById(propertyUser);
	}

	@Override
	public Integer deletePropertyUserById(String id)
	{
		return propertyUserDao.deleteById(id);
	}

	@Override
	public PropertyUserDo findPropertyUserById(String id)
	{
		return propertyUserDao.findById(id);
	}

	@Override
	public PropertyUserDo findByUserId(Integer userId) {
		return propertyUserDao.findByUserId(userId);
	}	
	
	@Override
	public PageDo<PropertyUserDo> queryWithSysUserListPage(Long pageIndex, Integer pageSize,
			Map<String, Object> selectItem) {
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PropertyUserDo> pageBean=new PageDo<PropertyUserDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(propertyUserDao.queryWithSysUserListPage(selectItem));
		return pageBean;
	}


	@Override
	public PropertyUserDo findWithSysUserById(String id) {
		return propertyUserDao.findWithSysUserById(id);
	}


	@Override
	public List<PropertyUserDo> queryWithSysUserList(Map<String, Object> selectItem) {
		return propertyUserDao.queryWithSysUserList(selectItem);
	}

}
