package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.CompanyComponent;
import com.cwkj.scf.dao.property.CompanyDao;
import com.cwkj.scf.model.property.CompanyDo;

/**
 * 企业.
 * @author ljc
 * @version  v1.0
 */
@Component("companyComponent")
public class CompanyComponentImpl  implements CompanyComponent{
	
	private CompanyDao companyDao;
	
	@Autowired
	public void setCompanyDao(CompanyDao companyDao)
	{
		this.companyDao=companyDao;
	}
	
	
	@Override
	public Integer insertCompany(CompanyDo company)
	{
		return companyDao.insert(company);
	}
	
	@Override
	public List<CompanyDo> queryCompanyList(Map<String,Object> selectItem)
	{
		return companyDao.queryList(selectItem);
	}

	@Override
	public PageDo<CompanyDo> queryCompanyListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<CompanyDo> pageBean=new PageDo<CompanyDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(companyDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateCompanyById(CompanyDo company)
	{
		return companyDao.updateById(company);
	}

	@Override
	public Integer deleteCompanyById(String id)
	{
		return companyDao.deleteById(id);
	}

	@Override
	public CompanyDo findCompanyById(String id)
	{
		return companyDao.findById(id);
	}


	@Override
	public Integer updateBusinessLicense(String id, String businessLicense) {
		return companyDao.updateBusinessLicense(id, businessLicense);
	}	
}
