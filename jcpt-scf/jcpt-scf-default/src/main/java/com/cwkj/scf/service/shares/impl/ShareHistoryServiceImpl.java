package com.cwkj.scf.service.shares.impl;

import java.util.List;
import java.util.Map;

import com.cwkj.scf.service.shares.ShareHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.shares.ShareHistoryComponent;
import com.cwkj.scf.model.shares.ShareHistoryDo;

/**
 * 分享历史.
 * @author ljc
 * @version  v1.0
 */
@Service("shareHistoryService")
public class ShareHistoryServiceImpl  implements ShareHistoryService {
	
	private ShareHistoryComponent shareHistoryComponent;
	
	@Autowired
	public void setShareHistoryComponent(ShareHistoryComponent shareHistoryComponent)
	{
		this.shareHistoryComponent=shareHistoryComponent;
	}
	
	
	@Override
	public Integer insertShareHistory(ShareHistoryDo shareHistory)
	{
		return shareHistoryComponent.insertShareHistory(shareHistory);
	}
	
	@Override
	public List<ShareHistoryDo> queryShareHistoryList(Map<String,Object> selectItem)
	{
		return shareHistoryComponent.queryShareHistoryList(selectItem);
	}

	@Override
	public PageDo<ShareHistoryDo> queryShareHistoryListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return shareHistoryComponent.queryShareHistoryListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateShareHistoryById(ShareHistoryDo shareHistory)
	{
		return shareHistoryComponent.updateShareHistoryById(shareHistory);
	}

	@Override
	public Integer deleteShareHistoryById(Long id)
	{
		return shareHistoryComponent.deleteShareHistoryById(id);
	}

	@Override
	public ShareHistoryDo findShareHistoryById(Long id)
	{
		return shareHistoryComponent.findShareHistoryById(id);
	}	
}
