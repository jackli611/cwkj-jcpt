package com.cwkj.scf.component.etc;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.etc.EtcProfileDo;

/**
 * ETC基本信息.
 * @author ljc
 * @version  v1.0
 */
public interface EtcProfileComponent {
	
	/**
	 * 添加ETC基本信息.
	 * @param etcProfile
	 * @return
	 */
	public Integer insertEtcProfile(EtcProfileDo etcProfile);
	
	/**
	 * 获取ETC基本信息数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<EtcProfileDo> queryEtcProfileList(Map<String, Object> selectItem);

	/**
	 * 获取ETC基本信息数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<EtcProfileDo> queryEtcProfileListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改ETC基本信息.
	 * @param etcProfile
	 * @return
	 */
	public Integer updateEtcProfileById(EtcProfileDo etcProfile);

	/**
	 * 根据Id删除ETC基本信息.
	 * @param id
	 * @return
	 */
	public Integer deleteEtcProfileById(Long id);

	/**
	 * 根据Id获取ETC基本信息.
	 * @param id
	 * @return
	 */
	public EtcProfileDo findEtcProfileById(Long id);

	/**
	 * 更新申请卡类型
	 * @param etcProfile
	 * @return
	 */
	public Integer updateCardTypeById(EtcProfileDo etcProfile);
}
