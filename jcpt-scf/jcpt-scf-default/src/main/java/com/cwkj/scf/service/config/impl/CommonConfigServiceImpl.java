package com.cwkj.scf.service.config.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.scf.service.config.CommonConfigService;
import com.cwkj.scf.service.config.UploadFileConfig;

/**
 * @author ljc
 *
 */
@Service("commonConfigService")
public class CommonConfigServiceImpl implements CommonConfigService{
	
	@Autowired
	private UploadFileConfig uploadFileConfig;

	@Override
	public UploadFileConfig getUploadFileConfig() {
		return uploadFileConfig;
	}

}
