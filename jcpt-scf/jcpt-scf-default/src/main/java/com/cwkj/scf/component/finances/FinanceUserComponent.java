package com.cwkj.scf.component.finances;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.finances.FinanceUserDo;

/**
 * 金融机构用户.
 * @author ljc
 * @version  v1.0
 */
public interface FinanceUserComponent {
	
	/**
	 * 添加金融机构用户.
	 * @param financeUser
	 * @return
	 */
	public Integer insertFinanceUser(FinanceUserDo financeUser);
	
	/**
	 * 获取金融机构用户数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<FinanceUserDo> queryFinanceUserList(Map<String,Object> selectItem);

	/**
	 * 获取金融机构用户数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<FinanceUserDo> queryFinanceUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);
	
	/**
	 * 获取带系统账户信息的金融机构用户
	 * @param pageIndex
	 * @param pageSize
	 * @param selectItem
	 * @return
	 */
	public PageDo<FinanceUserDo> queryWithSysUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改金融机构用户.
	 * @param financeUser
	 * @return
	 */
	public Integer updateFinanceUserById(FinanceUserDo financeUser);

	/**
	 * 根据Id删除金融机构用户.
	 * @param id
	 * @return
	 */
	public Integer deleteFinanceUserById(String id);

	/**
	 * 根据Id获取金融机构用户.
	 * @param id
	 * @return
	 */
	public FinanceUserDo findFinanceUserById(String id);	
}
