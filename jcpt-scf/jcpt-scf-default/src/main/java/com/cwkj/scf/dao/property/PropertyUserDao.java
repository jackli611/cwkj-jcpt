package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.PropertyUserDo;

/**
 * 物业所属用户.
 * @author ljc
 * @version  v1.0
 */
@Repository("propertyUserDao")
public interface PropertyUserDao{
	
	/**
	 * 添加物业所属用户.
	 * @param propertyUser
	 * @return
	 */
	public Integer insert(PropertyUserDo propertyUser);
	
	/**
	 * 获取物业所属用户数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyUserDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取物业所属用户数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyUserDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改物业所属用户.
	 * @param propertyUser
	 * @return
	 */
	public Integer updateById(PropertyUserDo propertyUser);

	/**
	 * 根据Id删除物业所属用户.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取物业所属用户.
	 * @param id
	 * @return
	 */
	public PropertyUserDo findById(String id);
	
	/**
	 * 根据账户ID获取
	 * @param userId
	 * @return
	 */
	public PropertyUserDo findByUserId(@Param("userId")Integer userId);
	
	
	/**
	 * 根据ID获取带账户信息的
	 * @param id
	 * @return
	 */
	public PropertyUserDo findWithSysUserById(String id);
 	
	/**
	 * 获取物业所属用户数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyUserDo> queryWithSysUserListPage(Map<String,Object> selectItem);
	
	/**
	 * 获取物业所属用户数据列表.
	 * @param propertyId
	 * @return
	 */
	public List<PropertyUserDo> queryWithSysUserList(Map<String,Object> selectItem);
	 
}
