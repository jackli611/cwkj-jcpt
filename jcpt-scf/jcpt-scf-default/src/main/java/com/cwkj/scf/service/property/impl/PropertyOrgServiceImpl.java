package com.cwkj.scf.service.property.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.component.products.ProductComponent;
import com.cwkj.scf.component.property.PropertyFinancesComponent;
import com.cwkj.scf.component.property.PropertyOrgComponent;
import com.cwkj.scf.component.property.PropertyUserComponent;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.model.property.PropertyFinancesDo;
import com.cwkj.scf.model.property.PropertyOrgDo;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.service.property.PropertyOrgService;

/**
 * 物业机构.
 * @author ljc
 * @version  v1.0
 */
@Component("propertyOrgService")
public class PropertyOrgServiceImpl  implements PropertyOrgService{
	
	private PropertyOrgComponent propertyOrgComponent;
	@Autowired
	private PropertyUserComponent propertyUserComponent;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	@Autowired
	private ProductComponent productComponent;
	
	@Autowired
	private PropertyFinancesComponent propertyFinancesComponent;
	
	@Autowired
	public void setPropertyOrgComponent(PropertyOrgComponent propertyOrgComponent)
	{
		this.propertyOrgComponent=propertyOrgComponent;
	}
	
	
	@Override
	public Integer insertPropertyOrg(PropertyOrgDo propertyOrg)
	{
		Date now=new Date();
		propertyOrg.setId(sysSeqNumberService.daySeqnumberString());
		propertyOrg.setCreateTime(now);
		propertyOrg.setUpdateTime(now);
		return propertyOrgComponent.insertPropertyOrg(propertyOrg);
	}
	
	@Override
	public List<PropertyOrgDo> queryPropertyOrgList(Map<String,Object> selectItem)
	{
		return propertyOrgComponent.queryPropertyOrgList(selectItem);
	}

	@Override
	public PageDo<PropertyOrgDo> queryPropertyOrgListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return propertyOrgComponent.queryPropertyOrgListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updatePropertyOrgById(PropertyOrgDo propertyOrg)
	{
		Date now=new Date();
		propertyOrg.setUpdateTime(now);
		return propertyOrgComponent.updatePropertyOrgById(propertyOrg);
	}

	@Override
	public Integer deletePropertyOrgById(String id)
	{
		return propertyOrgComponent.deletePropertyOrgById(id);
	}

	@Override
	public PropertyOrgDo findPropertyOrgById(String id)
	{
		return propertyOrgComponent.findPropertyOrgById(id);
	}
	
	@Override
	public PropertyOrgDo findFullPropertyOrgById(String id) {
		PropertyOrgDo propertyOrg= propertyOrgComponent.findPropertyOrgById(id);
		if(propertyOrg!=null)
		{
			propertyOrg.setProductList(propertyOrgComponent.queryProductList(id));
			propertyOrg.setPropertyorgProductList(productComponent.queryListByPropertyOrgId(id));
			Map<String, Object> selectItem=new HashMap<String,Object>();
			selectItem.put("propertyId",propertyOrg.getId());
			List<PropertyUserDo> propertyUserList= propertyUserComponent.queryWithSysUserList(selectItem);
			propertyOrg.setPropertyUserList(propertyUserList);
		}
		return propertyOrg;
	}


	@Override
	public Integer updatePropertyNameInfo(PropertyOrgDo propertyOrg) {
		return propertyOrgComponent.updatePropertyNameInfo(propertyOrg);
	}


	@Override
	public Integer updateBusinessLicense(String id, String businessLicense) {
		return propertyOrgComponent.updateBusinessLicense(id, businessLicense);
	}


	@Override
	public Integer updateLogo(String id, String logo) {
		return propertyOrgComponent.updateLogo(id, logo);
	}


	@Override
	public Integer updateStatus(String id, Integer status) {
		return propertyOrgComponent.updateStatus(id, status);
	}


	@Override
	public Integer addSysUser(PropertyOrgDo propertyOrg) {
		return propertyOrgComponent.addSysUser(propertyOrg);
	}


	@Override
	public List<ProductDo> queryProductList(String propertyOrgId) {
		return propertyOrgComponent.queryProductList(propertyOrgId);
	}


	@Override
	public List<PropertyorgProduct> findProduct(Map<String,Object> selectItem) {
		return propertyOrgComponent.findProduct(selectItem);
	}


	@Override
	public List<PropertyFinancesDo> findPropertyFinanceList(String propertyId) {
		return propertyFinancesComponent.queryListByPropertyId(propertyId);
	}


	@Override
	public Integer addPropertyFinances(PropertyFinancesDo propertyFinances) {
		return propertyFinancesComponent.insertPropertyFinances(propertyFinances);
	}


	@Override
	public Integer updatePropertyFinances(PropertyFinancesDo propertyFinances) {
		return propertyFinancesComponent.updatePropertyFinancesById(propertyFinances);
	}


	@Override
	public Integer deletePropertyFinances(Integer propertyFinanceId) {
		return propertyFinancesComponent.deletePropertyFinancesById(propertyFinanceId);
	}


	@Override
	public Integer updatePropertyFinanceStatus(Integer propertyFinanceId, Integer status) {
		return propertyFinancesComponent.updatePropertyFinanceStatus(propertyFinanceId, status);
	}	
}
