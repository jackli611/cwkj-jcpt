package com.cwkj.scf.component.finances.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.finances.FinanceUserComponent;
import com.cwkj.scf.dao.finances.FinanceUserDao;
import com.cwkj.scf.model.finances.FinanceUserDo;

/**
 * 金融机构用户.
 * @author ljc
 * @version  v1.0
 */
@Component("financeUserComponent")
public class FinanceUserComponentImpl  implements FinanceUserComponent{
	
	private FinanceUserDao financeUserDao;
	
	@Autowired
	public void setFinanceUserDao(FinanceUserDao financeUserDao)
	{
		this.financeUserDao=financeUserDao;
	}
	
	
	@Override
	public Integer insertFinanceUser(FinanceUserDo financeUser)
	{
		return financeUserDao.insert(financeUser);
	}
	
	@Override
	public List<FinanceUserDo> queryFinanceUserList(Map<String,Object> selectItem)
	{
		return financeUserDao.queryList(selectItem);
	}

	@Override
	public PageDo<FinanceUserDo> queryFinanceUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<FinanceUserDo> pageBean=new PageDo<FinanceUserDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(financeUserDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateFinanceUserById(FinanceUserDo financeUser)
	{
		return financeUserDao.updateById(financeUser);
	}

	@Override
	public Integer deleteFinanceUserById(String id)
	{
		return financeUserDao.deleteById(id);
	}

	@Override
	public FinanceUserDo findFinanceUserById(String id)
	{
		return financeUserDao.findById(id);
	}


	@Override
	public PageDo<FinanceUserDo> queryWithSysUserListPage(Long pageIndex, Integer pageSize,
			Map<String, Object> selectItem) {
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<FinanceUserDo> pageBean=new PageDo<FinanceUserDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(financeUserDao.queryWithSysUserListPage(selectItem));
		return pageBean;
	}	
}
