package com.cwkj.scf.service.property.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.component.property.PropertyUserComponent;
import com.cwkj.scf.model.property.PropertyUserDo;
import com.cwkj.scf.service.property.PropertyUserService;

/**
 * 物业所属用户.
 * @author ljc
 * @version  v1.0
 */
@Component("propertyUserService")
public class PropertyUserServiceImpl  implements PropertyUserService{
	
	private PropertyUserComponent propertyUserComponent;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	@Autowired
	private SysUserService sysUserService;

	
	@Autowired
	public void setPropertyUserComponent(PropertyUserComponent propertyUserComponent)
	{
		this.propertyUserComponent=propertyUserComponent;
	}
	
	
	@Override
	public Integer insertPropertyUser(PropertyUserDo propertyUser)
	{
		Date now=new Date();
	
		AssertUtils.isNotBlank(propertyUser.getPropertyId(),"无效核心企业");
		AssertUtils.notNull(propertyUser.getUserType(),"用户类型不能为空");
		AssertUtils.isNotBlank(propertyUser.getMobile(),"手机号不能为空");
		AssertUtils.isNotBlank(propertyUser.getPassword(),"密码不能为空");
		AssertUtils.notNull(propertyUser.getRecordUserId(),"操作用户无效");
		SysUserDo user = sysUserService.findSysUserByUserName(propertyUser.getMobile());
		AssertUtils.isNull(user,"手机号已被使用");
		user = new SysUserDo();
		user.setLocked(1);//未锁定
		user.setUserName(propertyUser.getMobile());
		user.setRealName(propertyUser.getRealName());
		user.setTelphone(propertyUser.getMobile());
		user.setEmail(propertyUser.getEmail());
		String psw = DigestUtils.md5Hex(propertyUser.getPassword() + SystemConst.PASS_KEY);
		user.setPassword(psw);
		user.setPlatformId(SystemConst.PLATFORM_DEFAULT);
		int dbRes = sysUserService.insertSysUser(user);
		AssertUtils.isTrue(dbRes>0,"创建系统账号失败");
		sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_PROPERTYS}, user.getId());
		propertyUser.setUserId(user.getId());
		propertyUser.setId(sysSeqNumberService.daySeqnumberString());
		propertyUser.setCreateTime(now);
		propertyUser.setUpdateTime(now);
		return propertyUserComponent.insertPropertyUser(propertyUser);
	}
	
	@Override
	public List<PropertyUserDo> queryPropertyUserList(Map<String,Object> selectItem)
	{
		return propertyUserComponent.queryPropertyUserList(selectItem);
	}

	@Override
	public PageDo<PropertyUserDo> queryPropertyUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return propertyUserComponent.queryPropertyUserListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updatePropertyUserById(PropertyUserDo propertyUser)
	{
		AssertUtils.notNull(propertyUser.getUserType(),"用户类型不能为空");
		Date now=new Date();
		propertyUser.setUpdateTime(now);
		PropertyUserDo dbUser=findPropertyUserById(propertyUser.getId());
		AssertUtils.notNull(dbUser,"参数错误[err:01]");
		dbUser.setUserType(propertyUser.getUserType());
		dbUser.setEmail(propertyUser.getEmail());
		dbUser.setPosition(propertyUser.getPosition());
		dbUser.setRealName(propertyUser.getRealName());
		dbUser.setUserName(propertyUser.getUserName());
		dbUser.setUpdateTime(propertyUser.getUpdateTime());
		dbUser.setPosition(propertyUser.getPosition());
		dbUser.setUserType(propertyUser.getUserType());
		dbUser.setBuildUserTag(propertyUser.getBuildUserTag());
		dbUser.setSex(propertyUser.getSex());
		AssertUtils.notNull(dbUser.getUserId(),"无效的用户");

		//修改系统账号
		SysUserDo sysUserDo=sysUserService.findSysUserById(dbUser.getUserId());
		AssertUtils.isTrue(sysUserDo!=null,"账户信息不存在");
		sysUserDo.setRealName(dbUser.getRealName());
		sysUserDo.setEmail(dbUser.getEmail());
		Integer dbRes= sysUserService.updateSysUserById(sysUserDo);
		AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"账户信息变更失败");


		return propertyUserComponent.updatePropertyUserById(dbUser);
	}

	@Override
	public Integer deletePropertyUserById(String id)
	{
		return propertyUserComponent.deletePropertyUserById(id);
	}

	@Override
	public PropertyUserDo findPropertyUserById(String id)
	{
		return propertyUserComponent.findPropertyUserById(id);
	}

	@Override
	public PropertyUserDo findByUserId(Integer userId) {
		return propertyUserComponent.findByUserId(userId);
	}

	@Override
	public PageDo<PropertyUserDo> queryWithSysUserListPage(Long pageIndex, Integer pageSize,
			Map<String, Object> selectItem) {
		return propertyUserComponent.queryWithSysUserListPage(pageIndex, pageSize, selectItem);
	}


	@Override
	public PropertyUserDo findWithSysUserById(String id) {
		return propertyUserComponent.findWithSysUserById(id);
	}


	@Override
	public List<PropertyUserDo> queryWithSysUserList(Map<String, Object> selectItem) {
		return propertyUserComponent.queryWithSysUserList(selectItem);
	}	
}
