package com.cwkj.scf.component.finances;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.finances.FinanceOrgDo;

/**
 * 金融机构.
 * @author ljc
 * @version  v1.0
 */
public interface FinanceOrgComponent {
	
	/**
	 * 添加金融机构.
	 * @param financeOrg
	 * @return
	 */
	public Integer insertFinanceOrg(FinanceOrgDo financeOrg);
	
	/**
	 * 获取金融机构数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<FinanceOrgDo> queryFinanceOrgList(Map<String,Object> selectItem);

	/**
	 * 获取金融机构数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<FinanceOrgDo> queryFinanceOrgListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改金融机构.
	 * @param financeOrg
	 * @return
	 */
	public Integer updateFinanceOrgById(FinanceOrgDo financeOrg);

	/**
	 * 根据Id删除金融机构.
	 * @param id
	 * @return
	 */
	public Integer deleteFinanceOrgById(String id);

	/**
	 * 根据Id获取金融机构.
	 * @param id
	 * @return
	 */
	public FinanceOrgDo findFinanceOrgById(String id);	

	/**
	 * 修改记录状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(String id,Integer status);
	
	/**
	 * 添加系统账号信息
	 * @param financeOrg
	 * @return
	 */
	public Integer addSysUser(FinanceOrgDo financeOrg);
	
	/**
	 * 修改物业基本信息
	 * @param propertyOrg
	 * @return
	 */
	public Integer updateFinanceNameInfo(FinanceOrgDo financeOrg);
}
