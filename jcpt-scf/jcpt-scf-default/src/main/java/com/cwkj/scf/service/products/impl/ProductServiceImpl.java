package com.cwkj.scf.service.products.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.products.ProductComponent;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.service.products.ProductService;

/**
 * 商品.
 * @author ljc
 * @version  v1.0
 */
@Service("productService")
public class ProductServiceImpl  implements ProductService{
	
	private ProductComponent productComponent;
	
	@Autowired
	public void setProductComponent(ProductComponent productComponent)
	{
		this.productComponent=productComponent;
	}
	
	
	@Override
	public Integer insertProduct(ProductDo product)
	{
		return productComponent.insertProduct(product);
	}
	
	@Override
	public List<ProductDo> queryProductList(Map<String,Object> selectItem)
	{
		return productComponent.queryProductList(selectItem);
	}

	@Override
	public PageDo<ProductDo> queryProductListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return productComponent.queryProductListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateProductById(ProductDo product)
	{
		return productComponent.updateProductById(product);
	}

	@Override
	public Integer deleteProductById(String productCode)
	{
		return productComponent.deleteProductById(productCode);
	}

	@Override
	public ProductDo findProductById(String productCode)
	{
		return productComponent.findProductById(productCode);
	}


	@Override
	public PageDo<PropertyorgProduct> queryCompanyProductListPage(Long pageIndex, Integer pageSize,
			Map<String, Object> selectItem) {
		return productComponent.queryCompanyProductListPage(pageIndex,pageSize,selectItem);
	}


	@Override
	public PropertyorgProduct findAssignProductById(Integer assignId) {
		return productComponent.findAssignProductById(assignId);
	}


	@Override
	public void insertAssignProduct(PropertyorgProduct assignProduct) {
		productComponent.insertAssignProduct(assignProduct);
		
	}


	@Override
	public void updateAssignProduct(PropertyorgProduct assignProduct) {
		productComponent.updateAssignProductById(assignProduct);
		
	}	
}
