package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.SupplierInvitationComponent;
import com.cwkj.scf.dao.property.SupplierInvitationDao;
import com.cwkj.scf.model.property.SupplierInvitationDo;

/**
 * 供应商邀请注册.
 * @author ljc
 * @version  v1.0
 */
@Component("supplierInvitationComponent")
public class SupplierInvitationComponentImpl  implements SupplierInvitationComponent{
	
	private SupplierInvitationDao supplierInvitationDao;
	
	@Autowired
	public void setSupplierInvitationDao(SupplierInvitationDao supplierInvitationDao)
	{
		this.supplierInvitationDao=supplierInvitationDao;
	}
	
	
	@Override
	public Integer insertSupplierInvitation(SupplierInvitationDo supplierInvitation)
	{
		return supplierInvitationDao.insert(supplierInvitation);
	}
	
	@Override
	public List<SupplierInvitationDo> querySupplierInvitationList(Map<String,Object> selectItem)
	{
		return supplierInvitationDao.queryList(selectItem);
	}

	@Override
	public PageDo<SupplierInvitationDo> querySupplierInvitationListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<SupplierInvitationDo> pageBean=new PageDo<SupplierInvitationDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(supplierInvitationDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateSupplierInvitationById(SupplierInvitationDo supplierInvitation)
	{
		return supplierInvitationDao.updateById(supplierInvitation);
	}

	@Override
	public Integer deleteSupplierInvitationById(Integer id)
	{
		return supplierInvitationDao.deleteById(id);
	}

	@Override
	public SupplierInvitationDo findSupplierInvitationById(Integer id)
	{
		return supplierInvitationDao.findById(id);
	}


	@Override
	public SupplierInvitationDo findByPropertyId(String propertyId) {
		return supplierInvitationDao.findByPropertyId(propertyId);
	}

	@Override
	public SupplierInvitationDo findByInviteCode(String inviteCode) {
		return supplierInvitationDao.findByInviteCode(inviteCode);
	}	
}
