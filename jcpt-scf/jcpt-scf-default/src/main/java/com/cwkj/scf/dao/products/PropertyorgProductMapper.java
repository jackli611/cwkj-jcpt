package com.cwkj.scf.dao.products;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.model.products.PropertyorgProductExample;

@Repository("propertyorgProductMapper")
public interface PropertyorgProductMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    long countByExample(PropertyorgProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int deleteByExample(PropertyorgProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int deleteByPrimaryKey(Integer assignid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int insert(PropertyorgProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int insertSelective(PropertyorgProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    List<PropertyorgProduct> selectByExample(PropertyorgProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    PropertyorgProduct selectByPrimaryKey(Integer assignid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int updateByExampleSelective(@Param("record") PropertyorgProduct record, @Param("example") PropertyorgProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int updateByExample(@Param("record") PropertyorgProduct record, @Param("example") PropertyorgProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int updateByPrimaryKeySelective(PropertyorgProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table propertyorg_product
     *
     * @mbg.generated Sat Jan 19 01:09:10 CST 2019
     */
    int updateByPrimaryKey(PropertyorgProduct record);
    
    /**
     * 	分页查询
     * @param selectItem
     * @return
     */
	List<PropertyorgProduct> queryListPage(Map<String, Object> selectItem);
	
	/**
	 * 根据物业机构ID获取产品
	 * @param propertyOrgId
	 * @return
	 * @author ljc
	 */
	List<PropertyorgProduct> queryListByPropertyOrgId(String propertyOrgId);
}