package com.cwkj.scf.service.merchant.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcpt.util.StringUtil;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.scf.component.merchant.MerchantAccountComponent;
import com.cwkj.scf.model.merchant.MerchantAccountDo;
import com.cwkj.scf.model.merchant.MerchantProfileDo;
import com.cwkj.scf.model.shares.ShareUserDo;
import com.cwkj.scf.service.merchant.MerchantAccountService;
import com.cwkj.scf.service.shares.ShareUserService;

/**
 * 商户账号.
 * @author ljc
 * @version  v1.0
 */
@Service("merchantAccountService")
public class MerchantAccountServiceImpl  implements MerchantAccountService {

	private  static Logger logger= LoggerFactory.getLogger(MerchantAccountServiceImpl.class);
	
	private MerchantAccountComponent merchantAccountComponent;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private ShareUserService shareUserService;

	
	@Autowired
	public void setMerchantAccountComponent(MerchantAccountComponent merchantAccountComponent)
	{
		this.merchantAccountComponent=merchantAccountComponent;
	}
	
	private String builderMerchantAccountId(MerchantAccountDo merchantAccountDo)
	{
		AssertUtils.isNotBlank(merchantAccountDo.getMerId(),"商户ID不能为空");
		AssertUtils.isNotBlank(merchantAccountDo.getAcctid(),"商户的用户ID不能为空");
		return merchantAccountDo.getMerId()+merchantAccountDo.getAcctid();
	}

	@Override
	public Integer insertMerchantAccount(MerchantAccountDo merchantAccount)
	{
		Date now=new Date();
		merchantAccount.setCreateTime(now);
		merchantAccount.setUpdateTime(now);
		merchantAccount.setId(builderMerchantAccountId(merchantAccount));
		return merchantAccountComponent.insertMerchantAccount(merchantAccount);
	}
	
	@Override
	public List<MerchantAccountDo> queryMerchantAccountList(Map<String,Object> selectItem)
	{
		return merchantAccountComponent.queryMerchantAccountList(selectItem);
	}

	@Override
	public PageDo<MerchantAccountDo> queryMerchantAccountListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return merchantAccountComponent.queryMerchantAccountListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateMerchantAccountById(MerchantAccountDo merchantAccount)
	{
		return merchantAccountComponent.updateMerchantAccountById(merchantAccount);
	}

	@Override
	public Integer deleteMerchantAccountById(String id)
	{
		return merchantAccountComponent.deleteMerchantAccountById(id);
	}

	@Override
	public MerchantAccountDo findMerchantAccountById(String id)
	{
		return merchantAccountComponent.findMerchantAccountById(id);
	}

	@Override
	@Transactional
	public MerchantAccountDo checkMerchantAccount(MerchantAccountDo merchantAccountDo, MerchantProfileDo merchantProfile,Long shareId) {
		String merchantId=builderMerchantAccountId(merchantAccountDo);
		MerchantAccountDo account=findMerchantAccountById(merchantId);
		if(account==null)
		{
			SysUserDo user=sysUserService.findSysUserByUserName(merchantAccountDo.getMobile());
			if(user!=null)
			{
				throw new BusinessException("手机号已被注册");
			}
			user=new SysUserDo();
			user.setLocked(1);//未锁定
			user.setUserName( merchantAccountDo.getMobile()+"-"+merchantAccountDo.getAcctid());
			user.setRealName(merchantAccountDo.getMobile());
			user.setTelphone(merchantAccountDo.getMobile());
			user.setRegisterSource(merchantProfile.getPropertyId());
			String password= merchantId;
			String psw=DigestUtils.md5Hex(password+ SystemConst.PASS_KEY);
			user.setPassword(psw);
			Integer dbRes=sysUserService.insertSysUser(user);
			if(dbRes>0)
			{
				dbRes=sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_PERSONALLOAN}, user.getId());
			}
			AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"创建用户角色失败");
			merchantAccountDo.setUserId(user.getId());
			dbRes=insertMerchantAccount(merchantAccountDo);
			AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"创建商户用户失败");
			account=merchantAccountDo;

			if(shareId!=null) {
				ShareUserDo shareUserDo = shareUserService.findShareUserById(shareId);
				if (shareUserDo != null) {
					ShareUserDo newShareUser = new ShareUserDo();
					newShareUser.setUserId(user.getId());
					newShareUser.setTelphone(user.getUserName());
					newShareUser.setFromUserId(shareUserDo.getUserId());
					newShareUser.setFromShareId(shareUserDo.getId());
					newShareUser.setFromTelphone(shareUserDo.getTelphone());
					newShareUser.setTopShareUserId(shareUserDo.getFromUserId());
					newShareUser.setTopShareTelphone(shareUserDo.getFromTelphone());
					newShareUser.setPartnerStatus(ShareUserDo.PARTNERSTATUS_DEFAULT);
					newShareUser.setOpenPlatform(merchantProfile.getPropertyId());
					shareUserService.insertShareUser(newShareUser);
				}
			}

		}
		return account;
	}
	
	
	@Override
	@Transactional
	public MerchantAccountDo bindWxOpenId(MerchantAccountDo merchantAccountDo, 
										  MerchantProfileDo merchantProfile,Long shareId) {
		String merchantId=builderMerchantAccountId(merchantAccountDo);
		MerchantAccountDo account=findMerchantAccountById(merchantId);
		if(account==null){ //已绑定直接返回
			logger.info("手机号{}未绑定：",merchantAccountDo.getMobile());
			if(StringUtil.isBlank(merchantAccountDo.getMobile())) {
				return null;
			}
			
			SysUserDo user=sysUserService.findSysUserByUserName(merchantAccountDo.getMobile());
			if(user==null){ //不存在新增并绑定
				user=new SysUserDo();
				user.setLocked(1);//未锁定
				user.setUserName( merchantAccountDo.getMobile());
				user.setRealName(merchantAccountDo.getMobile());
				user.setTelphone(merchantAccountDo.getMobile());
				user.setRegisterSource(merchantProfile.getPropertyId());
				String password= merchantId;
				String psw=DigestUtils.md5Hex(password+ SystemConst.PASS_KEY);
				user.setPassword(psw);
				Integer dbRes=sysUserService.insertSysUser(user);
				if(dbRes>0)
				{
					dbRes=sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_PERSONALLOAN}, user.getId());
				}
				AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"创建用户角色失败");
				merchantAccountDo.setUserId(user.getId());
				dbRes=insertMerchantAccount(merchantAccountDo);
				AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"创建商户用户失败");

				//保存分享推荐信息
				if(shareId!=null) {
					ShareUserDo shareUserDo = shareUserService.findShareUserById(shareId);
					if (shareUserDo != null) {
						ShareUserDo newShareUser = new ShareUserDo();
						newShareUser.setUserId(user.getId());
						newShareUser.setTelphone(user.getUserName());
						newShareUser.setFromUserId(shareUserDo.getUserId());
						newShareUser.setFromShareId(shareUserDo.getId());
						newShareUser.setFromTelphone(shareUserDo.getTelphone());
						newShareUser.setTopShareUserId(shareUserDo.getFromUserId());
						newShareUser.setTopShareTelphone(shareUserDo.getFromTelphone());
						newShareUser.setPartnerStatus(ShareUserDo.PARTNERSTATUS_DEFAULT);
						newShareUser.setOpenPlatform(merchantProfile.getPropertyId());
						shareUserService.insertShareUser(newShareUser);
					}
				}
				account=merchantAccountDo;
			}else {//存在绑定
				logger.info("手机号{}已绑定：",merchantAccountDo.getMobile());
				merchantAccountDo.setUserId(user.getId());
				Integer dbRes=insertMerchantAccount(merchantAccountDo);
				AssertUtils.isTrue(dbRes!=null && dbRes.intValue()>0,"创建商户用户失败");
				account=merchantAccountDo;
			}
		}
		return account;
	}

	@Override
	public MerchantAccountDo findWxAccountByUserId(Integer userId) {
		Map<String, Object> selectItem = new HashMap<String,Object>();
		selectItem.put("userId", userId);
		selectItem.put("merId", "wx");
		List<MerchantAccountDo> accLst = merchantAccountComponent.queryMerchantAccountList(selectItem );
		if(null == accLst || accLst.size()<1) {
			return null;
		}
		return accLst.get(0);
	}
	
}
