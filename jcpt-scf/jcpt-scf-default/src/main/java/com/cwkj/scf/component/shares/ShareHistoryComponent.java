package com.cwkj.scf.component.shares;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.shares.ShareHistoryDo;

/**
 * 分享历史.
 * @author ljc
 * @version  v1.0
 */
public interface ShareHistoryComponent {
	
	/**
	 * 添加分享历史.
	 * @param shareHistory
	 * @return
	 */
	public Integer insertShareHistory(ShareHistoryDo shareHistory);
	
	/**
	 * 获取分享历史数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ShareHistoryDo> queryShareHistoryList(Map<String, Object> selectItem);

	/**
	 * 获取分享历史数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ShareHistoryDo> queryShareHistoryListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改分享历史.
	 * @param shareHistory
	 * @return
	 */
	public Integer updateShareHistoryById(ShareHistoryDo shareHistory);

	/**
	 * 根据Id删除分享历史.
	 * @param id
	 * @return
	 */
	public Integer deleteShareHistoryById(Long id);

	/**
	 * 根据Id获取分享历史.
	 * @param id
	 * @return
	 */
	public ShareHistoryDo findShareHistoryById(Long id);	
}
