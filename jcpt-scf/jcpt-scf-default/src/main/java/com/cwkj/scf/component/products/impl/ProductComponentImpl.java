package com.cwkj.scf.component.products.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.scf.component.products.ProductComponent;
import com.cwkj.scf.dao.products.ProductDao;
import com.cwkj.scf.dao.products.PropertyorgProductMapper;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.model.products.PropertyorgProductExample;
import com.cwkj.scf.model.products.PropertyorgProductExample.Criteria;

/**
 * 商品.
 * @author ljc
 * @version  v1.0
 */
@Component("productComponent")
public class ProductComponentImpl  implements ProductComponent{
	
	private ProductDao productDao;
	private PropertyorgProductMapper propertyorgProductDao;
	
	@Autowired
	public void setProductDao(ProductDao productDao)
	{
		this.productDao=productDao;
	}
	
	
	@Autowired
	public void setPropertyorgProductMapper(PropertyorgProductMapper propertyorgProductDao)
	{
		this.propertyorgProductDao=propertyorgProductDao;
	}
	
	
	@Override
	public Integer insertProduct(ProductDo product)
	{
		return productDao.insert(product);
	}
	
	@Override
	public List<ProductDo> queryProductList(Map<String,Object> selectItem)
	{
		return productDao.queryList(selectItem);
	}

	@Override
	public PageDo<ProductDo> queryProductListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<ProductDo> pageBean=new PageDo<ProductDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(productDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateProductById(ProductDo product)
	{
		return productDao.updateByProductCode(product);
	}

	@Override
	public Integer deleteProductById(String productCode)
	{
		return productDao.deleteByProductCode(productCode);
	}

	@Override
	public ProductDo findProductById(String productCode)
	{
		return productDao.findByProductCode(productCode);
	}


	@Override
	public PageDo<PropertyorgProduct> queryCompanyProductListPage(Long pageIndex, Integer pageSize,
			Map<String, Object> selectItem) {
		
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PropertyorgProduct> pageBean=new PageDo<PropertyorgProduct>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(propertyorgProductDao.queryListPage(selectItem));
		return pageBean;
	}


	@Override
	public PropertyorgProduct findAssignProductById(Integer assignId) {
		return propertyorgProductDao.selectByPrimaryKey(assignId);
	}


	@Override
	public void insertAssignProduct(PropertyorgProduct assignProduct) {
		PropertyorgProductExample example = new PropertyorgProductExample();
		Criteria criteria = example.createCriteria().andProductcodeEqualTo(assignProduct.getProductcode());
		criteria.andLevelsEqualTo(assignProduct.getLevels());
		criteria.andPropertyorgidEqualTo(assignProduct.getPropertyorgid());
		List<PropertyorgProduct> oldProductLst = propertyorgProductDao.selectByExample(example );
		
		if(oldProductLst !=null && oldProductLst.size()>0) {
			throw new BusinessException("A"+assignProduct.getLevels()+"等级产品已存在，请勿重复添加");
		}
		propertyorgProductDao.insertSelective(assignProduct);
	}


	@Override
	public List<PropertyorgProduct> queryListByPropertyOrgId(String propertyOrgId) {
		return propertyorgProductDao.queryListByPropertyOrgId(propertyOrgId);
	}


	@Override
	public void updateAssignProductById(PropertyorgProduct assignProduct) {
		propertyorgProductDao.updateByPrimaryKeySelective(assignProduct);		
	}	
}
