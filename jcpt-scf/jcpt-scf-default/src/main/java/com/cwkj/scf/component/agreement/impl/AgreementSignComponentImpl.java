package com.cwkj.scf.component.agreement.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.agreement.AgreementSignComponent;
import com.cwkj.scf.dao.agreement.AgreementSignDao;
import com.cwkj.scf.model.agreement.AgreementSignDo;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
@Component("agreementSignComponent")
public class AgreementSignComponentImpl  implements AgreementSignComponent{
	
	private AgreementSignDao agreementSignDao;
	
	@Autowired
	public void setAgreementSignDao(AgreementSignDao agreementSignDao)
	{
		this.agreementSignDao=agreementSignDao;
	}
	
	
	@Override
	public Integer insertAgreementSign(AgreementSignDo agreementSign)
	{
		return agreementSignDao.insert(agreementSign);
	}
	
	@Override
	public List<AgreementSignDo> queryAgreementSignList(Map<String,Object> selectItem)
	{
		return agreementSignDao.queryList(selectItem);
	}

	@Override
	public PageDo<AgreementSignDo> queryAgreementSignListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<AgreementSignDo> pageBean=new PageDo<AgreementSignDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(agreementSignDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateAgreementSignByContractNo(AgreementSignDo agreementSign)
	{
		return agreementSignDao.updateByContractNo(agreementSign);
	}

	@Override
	public Integer deleteAgreementSignByContractNo(String contractNo)
	{
		return agreementSignDao.deleteByContractNo(contractNo);
	}

	@Override
	public AgreementSignDo findAgreementSignByContractNo(String contractNo)
	{
		return agreementSignDao.findByContractNo(contractNo);
	}

	@Override
	public Integer updateUploadPdfStatus(String contractNo, String status) {
		return agreementSignDao.updateUploadPdfStatus(contractNo,status);
	}

	@Override
	public Integer updateSignStatus(String contractNo, String status) {
		return agreementSignDao.updateSignStatus(contractNo,status);
	}

	@Override
	public Integer updateCompletionStatus(String contractNo, String status) {
		return agreementSignDao.updateCompletionStatus(contractNo,status);
	}
}
