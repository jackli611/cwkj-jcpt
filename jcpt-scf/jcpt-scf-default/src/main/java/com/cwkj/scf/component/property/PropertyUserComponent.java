package com.cwkj.scf.component.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.PropertyUserDo;

/**
 * 物业所属用户.
 * @author ljc
 * @version  v1.0
 */
public interface PropertyUserComponent {
	
	/**
	 * 添加物业所属用户.
	 * @param propertyUser
	 * @return
	 */
	public Integer insertPropertyUser(PropertyUserDo propertyUser);
	
	/**
	 * 获取物业所属用户数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyUserDo> queryPropertyUserList(Map<String,Object> selectItem);

	/**
	 * 获取物业所属用户数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PropertyUserDo> queryPropertyUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改物业所属用户.
	 * @param propertyUser
	 * @return
	 */
	public Integer updatePropertyUserById(PropertyUserDo propertyUser);

	/**
	 * 根据Id删除物业所属用户.
	 * @param id
	 * @return
	 */
	public Integer deletePropertyUserById(String id);

	/**
	 * 根据Id获取物业所属用户.
	 * @param id
	 * @return
	 */
	public PropertyUserDo findPropertyUserById(String id);	
	
	/**
	 * 根据系统账户ID获取
	 * @param userId
	 * @return
	 */
	public PropertyUserDo findByUserId(Integer userId);
	
	
	/**
	 * 根据ID获取带账户信息的
	 * @param id
	 * @return
	 */
	public PropertyUserDo findWithSysUserById(String id);	
	
	/**
	 * 获取物业所属用户数据分页列表(用户账号).
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PropertyUserDo> queryWithSysUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);
	
	/**
	 * 获取物业所属用户数据列表.
	 * @param propertyId
	 * @return
	 */
	public List<PropertyUserDo> queryWithSysUserList(Map<String,Object> selectItem);
}
