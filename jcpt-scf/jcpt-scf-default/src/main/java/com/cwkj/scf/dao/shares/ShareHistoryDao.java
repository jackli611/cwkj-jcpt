package com.cwkj.scf.dao.shares;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.shares.ShareHistoryDo;

/**
 * 分享历史.
 * @author ljc
 * @version  v1.0
 */
@Repository("shareHistoryDao")
public interface ShareHistoryDao{
	
	/**
	 * 添加分享历史.
	 * @param shareHistory
	 * @return
	 */
	public Integer insert(ShareHistoryDo shareHistory);
	
	/**
	 * 获取分享历史数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ShareHistoryDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取分享历史数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<ShareHistoryDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改分享历史.
	 * @param shareHistory
	 * @return
	 */
	public Integer updateById(ShareHistoryDo shareHistory);

	/**
	 * 根据Id删除分享历史.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Long id);

	/**
	 * 根据Id获取分享历史.
	 * @param id
	 * @return
	 */
	public ShareHistoryDo findById(Long id);
 	
	 
}
