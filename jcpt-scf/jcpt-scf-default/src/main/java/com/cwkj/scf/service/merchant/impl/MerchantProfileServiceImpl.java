package com.cwkj.scf.service.merchant.impl;

import java.util.List;
import java.util.Map;

import com.cwkj.scf.service.merchant.MerchantProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.merchant.MerchantProfileComponent;
import com.cwkj.scf.model.merchant.MerchantProfileDo;

/**
 * 商户信息.
 * @author ljc
 * @version  v1.0
 */
@Service("merchantProfileService")
public class MerchantProfileServiceImpl  implements MerchantProfileService {
	
	private MerchantProfileComponent merchantProfileComponent;
	
	@Autowired
	public void setMerchantProfileComponent(MerchantProfileComponent merchantProfileComponent)
	{
		this.merchantProfileComponent=merchantProfileComponent;
	}
	
	
	@Override
	public Integer insertMerchantProfile(MerchantProfileDo merchantProfile)
	{
		return merchantProfileComponent.insertMerchantProfile(merchantProfile);
	}
	
	@Override
	public List<MerchantProfileDo> queryMerchantProfileList(Map<String,Object> selectItem)
	{
		return merchantProfileComponent.queryMerchantProfileList(selectItem);
	}

	@Override
	public PageDo<MerchantProfileDo> queryMerchantProfileListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return merchantProfileComponent.queryMerchantProfileListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateMerchantProfileById(MerchantProfileDo merchantProfile)
	{
		return merchantProfileComponent.updateMerchantProfileById(merchantProfile);
	}

	@Override
	public Integer deleteMerchantProfileById(Integer id)
	{
		return merchantProfileComponent.deleteMerchantProfileById(id);
	}

	@Override
	public MerchantProfileDo findMerchantProfileById(Integer id)
	{
		return merchantProfileComponent.findMerchantProfileById(id);
	}

	@Override
	public MerchantProfileDo enableMerchantProfileByMerId(String merId, String secretKey) {
		return merchantProfileComponent.enableMerchantProfileByMerId(merId,secretKey);
	}
}
