package com.cwkj.scf.component.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.SupplierInvitationDo;

/**
 * 供应商邀请注册.
 * @author ljc
 * @version  v1.0
 */
public interface SupplierInvitationComponent {
	
	/**
	 * 添加供应商邀请注册.
	 * @param supplierInvitation
	 * @return
	 */
	public Integer insertSupplierInvitation(SupplierInvitationDo supplierInvitation);
	
	/**
	 * 获取供应商邀请注册数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierInvitationDo> querySupplierInvitationList(Map<String,Object> selectItem);

	/**
	 * 获取供应商邀请注册数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<SupplierInvitationDo> querySupplierInvitationListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改供应商邀请注册.
	 * @param supplierInvitation
	 * @return
	 */
	public Integer updateSupplierInvitationById(SupplierInvitationDo supplierInvitation);

	/**
	 * 根据Id删除供应商邀请注册.
	 * @param id
	 * @return
	 */
	public Integer deleteSupplierInvitationById(Integer id);

	/**
	 * 根据Id获取供应商邀请注册.
	 * @param id
	 * @return
	 */
	public SupplierInvitationDo findSupplierInvitationById(Integer id);	
	
	/**
	 * 根据邀请注册码获取供应商邀请注册
	 * @param inviteCode 注册码
	 * @return
	 */
	public SupplierInvitationDo findByInviteCode(String inviteCode);

	/**
	 * 根据核心企业ID获取供应商邀请注册码
	 * @param propertyId 核心企业ID
	 * @return
	 */
	public SupplierInvitationDo findByPropertyId(String propertyId);
}
