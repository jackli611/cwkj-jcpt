package com.cwkj.scf.component.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.PropertyFinancesDo;

/**
 * 物业的资金机构.
 * @author ljc
 * @version  v1.0
 */
public interface PropertyFinancesComponent {
	
	/**
	 * 添加物业的资金机构.
	 * @param propertyFinances
	 * @return
	 */
	public Integer insertPropertyFinances(PropertyFinancesDo propertyFinances);
	
	/**
	 * 获取物业的资金机构数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyFinancesDo> queryPropertyFinancesList(Map<String,Object> selectItem);

	/**
	 * 获取物业的资金机构数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PropertyFinancesDo> queryPropertyFinancesListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改物业的资金机构.
	 * @param propertyFinances
	 * @return
	 */
	public Integer updatePropertyFinancesById(PropertyFinancesDo propertyFinances);

	/**
	 * 根据Id删除物业的资金机构.
	 * @param id
	 * @return
	 */
	public Integer deletePropertyFinancesById(Integer id);

	/**
	 * 根据Id获取物业的资金机构.
	 * @param id
	 * @return
	 */
	public PropertyFinancesDo findPropertyFinancesById(Integer id);	
	
	/**
	 * 物业的资金方列表
	 * @param propertyId 物业ID
	 * @return
	 */
	public List<PropertyFinancesDo> queryListByPropertyId(String propertyId);
	
	
	/**
	 * 修改资金方合作状态
	 * @param propertyFinanceId
	 * @param status
	 * @return
	 */
	public Integer updatePropertyFinanceStatus(Integer propertyFinanceId,Integer status);

	/**
	 * 更新已使用额度
	 * @param tmpPfDo
	 */
	public Integer updateUsedAmt(PropertyFinancesDo tmpPfDo);
}
