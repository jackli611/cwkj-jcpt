package com.cwkj.scf.component.finances.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.finances.FinanceOrgComponent;
import com.cwkj.scf.dao.finances.FinanceOrgDao;
import com.cwkj.scf.model.finances.FinanceOrgDo;

/**
 * 金融机构.
 * @author ljc
 * @version  v1.0
 */
@Component("financeOrgComponent")
public class FinanceOrgComponentImpl  implements FinanceOrgComponent{
	
	private FinanceOrgDao financeOrgDao;
	
	@Autowired
	public void setFinanceOrgDao(FinanceOrgDao financeOrgDao)
	{
		this.financeOrgDao=financeOrgDao;
	}
	
	
	@Override
	public Integer insertFinanceOrg(FinanceOrgDo financeOrg)
	{
		return financeOrgDao.insert(financeOrg);
	}
	
	@Override
	public List<FinanceOrgDo> queryFinanceOrgList(Map<String,Object> selectItem)
	{
		return financeOrgDao.queryList(selectItem);
	}

	@Override
	public PageDo<FinanceOrgDo> queryFinanceOrgListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<FinanceOrgDo> pageBean=new PageDo<FinanceOrgDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(financeOrgDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateFinanceOrgById(FinanceOrgDo financeOrg)
	{
		return financeOrgDao.updateById(financeOrg);
	}

	@Override
	public Integer deleteFinanceOrgById(String id)
	{
		return financeOrgDao.deleteById(id);
	}

	@Override
	public FinanceOrgDo findFinanceOrgById(String id)
	{
		return financeOrgDao.findById(id);
	}


	@Override
	public Integer updateStatus(String id, Integer status) {
		return financeOrgDao.updateStatus(id, status);
	}


	@Override
	public Integer addSysUser(FinanceOrgDo financeOrg) {
		return financeOrgDao.addSysUser(financeOrg);
	}


	@Override
	public Integer updateFinanceNameInfo(FinanceOrgDo financeOrg) {
		return financeOrgDao.updateFinanceNameInfo(financeOrg);
	}	
}
