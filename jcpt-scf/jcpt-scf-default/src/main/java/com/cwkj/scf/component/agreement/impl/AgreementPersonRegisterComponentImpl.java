package com.cwkj.scf.component.agreement.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.agreement.AgreementPersonRegisterComponent;
import com.cwkj.scf.dao.agreement.AgreementPersonRegisterDao;
import com.cwkj.scf.model.agreement.AgreementPersonRegisterDo;

/**
 * 签章个人用户注册.
 * @author ljc
 * @version  v1.0
 */
@Component("agreementPersonRegisterComponent")
public class AgreementPersonRegisterComponentImpl  implements AgreementPersonRegisterComponent{
	
	private AgreementPersonRegisterDao agreementPersonRegisterDao;
	
	@Autowired
	public void setAgreementPersonRegisterDao(AgreementPersonRegisterDao agreementPersonRegisterDao)
	{
		this.agreementPersonRegisterDao=agreementPersonRegisterDao;
	}
	
	
	@Override
	public Integer insertAgreementPersonRegister(AgreementPersonRegisterDo agreementPersonRegister)
	{
		return agreementPersonRegisterDao.insert(agreementPersonRegister);
	}
	
	@Override
	public List<AgreementPersonRegisterDo> queryAgreementPersonRegisterList(Map<String,Object> selectItem)
	{
		return agreementPersonRegisterDao.queryList(selectItem);
	}

	@Override
	public PageDo<AgreementPersonRegisterDo> queryAgreementPersonRegisterListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<AgreementPersonRegisterDo> pageBean=new PageDo<AgreementPersonRegisterDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(agreementPersonRegisterDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateAgreementPersonRegisterById(AgreementPersonRegisterDo agreementPersonRegister)
	{
		return agreementPersonRegisterDao.updateById(agreementPersonRegister);
	}

	@Override
	public Integer deleteAgreementPersonRegisterById(String id)
	{
		return agreementPersonRegisterDao.deleteById(id);
	}

	@Override
	public AgreementPersonRegisterDo findAgreementPersonRegisterById(String id)
	{
		return agreementPersonRegisterDao.findById(id);
	}

	@Override
	public List<AgreementPersonRegisterDo> findByIdcardNo(String idcardNo) {
		return agreementPersonRegisterDao.findByIdcardNo(idcardNo);
	}

	@Override
	public AgreementPersonRegisterDo findByPerson(String idcardNo, String realName, String mobile) {
		return agreementPersonRegisterDao.findByPerson(idcardNo,realName,mobile);
	}
}
