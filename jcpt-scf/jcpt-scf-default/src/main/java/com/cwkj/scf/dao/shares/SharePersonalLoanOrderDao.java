package com.cwkj.scf.dao.shares;

import com.cwkj.scf.model.shares.SharePersonalLoanOrder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 个贷分享订单
 */
@Repository("sharePersonalLoanOrderDao")
public interface SharePersonalLoanOrderDao {

    /**
     * 查询个贷分享订单列表
     * @param selectItem （oneLevelShareTelphone、twoLeavelShareTelphone、startDate、endDate、orderSource）
     * @return
     */
   List<SharePersonalLoanOrder> querySharePersonalLoanOrderListPage(Map<String,Object> selectItem);

}
