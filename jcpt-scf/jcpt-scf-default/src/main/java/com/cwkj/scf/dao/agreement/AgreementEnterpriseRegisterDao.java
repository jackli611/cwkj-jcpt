package com.cwkj.scf.dao.agreement;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.agreement.AgreementEnterpriseRegisterDo;

/**
 * 签章企业注册.
 * @author ljc
 * @version  v1.0
 */
@Repository("agreementEnterpriseRegisterDao")
public interface AgreementEnterpriseRegisterDao{
	
	/**
	 * 添加签章企业注册.
	 * @param agreementEnterpriseRegister
	 * @return
	 */
	public Integer insert(AgreementEnterpriseRegisterDo agreementEnterpriseRegister);
	
	/**
	 * 获取签章企业注册数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementEnterpriseRegisterDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取签章企业注册数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementEnterpriseRegisterDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改签章企业注册.
	 * @param agreementEnterpriseRegister
	 * @return
	 */
	public Integer updateById(AgreementEnterpriseRegisterDo agreementEnterpriseRegister);

	/**
	 * 根据Id删除签章企业注册.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取签章企业注册.
	 * @param id
	 * @return
	 */
	public AgreementEnterpriseRegisterDo findById(String id);
 	
	 
}
