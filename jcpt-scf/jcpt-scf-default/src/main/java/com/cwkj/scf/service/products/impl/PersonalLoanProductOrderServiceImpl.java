package com.cwkj.scf.service.products.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.products.PersonalLoanProductOrderComponent;
import com.cwkj.scf.model.products.PersonalLoanProductOrderDo;
import com.cwkj.scf.service.products.PersonalLoanProductOrderService;

/**
 * 个贷产品订单.
 * @author ljc
 * @version  v1.0
 */
@Service("personalLoanProductOrderService")
public class PersonalLoanProductOrderServiceImpl  implements PersonalLoanProductOrderService{
	
	private PersonalLoanProductOrderComponent personalLoanProductOrderComponent;
	
	@Autowired
	public void setPersonalLoanProductOrderComponent(PersonalLoanProductOrderComponent personalLoanProductOrderComponent)
	{
		this.personalLoanProductOrderComponent=personalLoanProductOrderComponent;
	}
	
	
	@Override
	public Integer insertPersonalLoanProductOrder(PersonalLoanProductOrderDo personalLoanProductOrder)
	{
		Date now=new Date();
		personalLoanProductOrder.setCreateTime(now);
		personalLoanProductOrder.setUpdateTime(now);
		return personalLoanProductOrderComponent.insertPersonalLoanProductOrder(personalLoanProductOrder);
	}
	
	@Override
	public List<PersonalLoanProductOrderDo> queryPersonalLoanProductOrderList(Map<String,Object> selectItem)
	{
		return personalLoanProductOrderComponent.queryPersonalLoanProductOrderList(selectItem);
	}

	@Override
	public PageDo<PersonalLoanProductOrderDo> queryPersonalLoanProductOrderListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return personalLoanProductOrderComponent.queryPersonalLoanProductOrderListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updatePersonalLoanProductOrderById(PersonalLoanProductOrderDo personalLoanProductOrder)
	{
		return personalLoanProductOrderComponent.updatePersonalLoanProductOrderById(personalLoanProductOrder);
	}

	@Override
	public Integer deletePersonalLoanProductOrderById(Long id)
	{
		return personalLoanProductOrderComponent.deletePersonalLoanProductOrderById(id);
	}

	@Override
	public PersonalLoanProductOrderDo findPersonalLoanProductOrderById(Long id)
	{
		return personalLoanProductOrderComponent.findPersonalLoanProductOrderById(id);
	}

	@Override
	public List<PersonalLoanProductOrderDo> queryByLoanId(String loanId) {
		return personalLoanProductOrderComponent.queryByLoanId(loanId);
	}
}
