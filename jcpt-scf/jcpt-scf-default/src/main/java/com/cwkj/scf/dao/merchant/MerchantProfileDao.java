package com.cwkj.scf.dao.merchant;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.merchant.MerchantProfileDo;

/**
 * 商户信息.
 * @author ljc
 * @version  v1.0
 */
@Repository("merchantProfileDao")
public interface MerchantProfileDao{
	
	/**
	 * 添加商户信息.
	 * @param merchantProfile
	 * @return
	 */
	public Integer insert(MerchantProfileDo merchantProfile);
	
	/**
	 * 获取商户信息数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantProfileDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取商户信息数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantProfileDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改商户信息.
	 * @param merchantProfile
	 * @return
	 */
	public Integer updateById(MerchantProfileDo merchantProfile);

	/**
	 * 根据Id删除商户信息.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Integer id);

	/**
	 * 根据Id获取商户信息.
	 * @param id
	 * @return
	 */
	public MerchantProfileDo findById(Integer id);

	/**
	 *获取启用的商户
	 * @param merId 商户ID
	 * @param secretKey 产品密码
	 * @return
	 */
	public MerchantProfileDo enableMerchantProfileByMerId(@Param("merId") String merId, @Param("secretKey")String secretKey);
}
