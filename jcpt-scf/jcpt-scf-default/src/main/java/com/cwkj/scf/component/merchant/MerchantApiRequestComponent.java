package com.cwkj.scf.component.merchant;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.merchant.MerchantApiRequestDo;

/**
 * 商户API请求.
 * @author ljc
 * @version  v1.0
 */
public interface MerchantApiRequestComponent {
	
	/**
	 * 添加商户API请求.
	 * @param merchantApiRequest
	 * @return
	 */
	public Integer insertMerchantApiRequest(MerchantApiRequestDo merchantApiRequest);
	
	/**
	 * 获取商户API请求数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantApiRequestDo> queryMerchantApiRequestList(Map<String, Object> selectItem);

	/**
	 * 获取商户API请求数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<MerchantApiRequestDo> queryMerchantApiRequestListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据TrxId修改商户API请求.
	 * @param merchantApiRequest
	 * @return
	 */
	public Integer updateMerchantApiRequestByTrxId(MerchantApiRequestDo merchantApiRequest);

	/**
	 * 根据TrxId删除商户API请求.
	 * @param trxId
	 * @return
	 */
	public Integer deleteMerchantApiRequestByTrxId(String trxId);

	/**
	 * 根据TrxId获取商户API请求.
	 * @param trxId
	 * @return
	 */
	public MerchantApiRequestDo findMerchantApiRequestByTrxId(String trxId);	
}
