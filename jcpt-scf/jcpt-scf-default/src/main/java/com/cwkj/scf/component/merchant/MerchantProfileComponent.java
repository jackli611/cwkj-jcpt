package com.cwkj.scf.component.merchant;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.merchant.MerchantProfileDo;

/**
 * 商户信息.
 * @author ljc
 * @version  v1.0
 */
public interface MerchantProfileComponent {
	
	/**
	 * 添加商户信息.
	 * @param merchantProfile
	 * @return
	 */
	public Integer insertMerchantProfile(MerchantProfileDo merchantProfile);
	
	/**
	 * 获取商户信息数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantProfileDo> queryMerchantProfileList(Map<String, Object> selectItem);

	/**
	 * 获取商户信息数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<MerchantProfileDo> queryMerchantProfileListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改商户信息.
	 * @param merchantProfile
	 * @return
	 */
	public Integer updateMerchantProfileById(MerchantProfileDo merchantProfile);

	/**
	 * 根据Id删除商户信息.
	 * @param id
	 * @return
	 */
	public Integer deleteMerchantProfileById(Integer id);

	/**
	 * 根据Id获取商户信息.
	 * @param id
	 * @return
	 */
	public MerchantProfileDo findMerchantProfileById(Integer id);

	/**
	 *获取启用的商户
	 * @param merId 商户ID
	 * @param secretKey 产品密码
	 * @return
	 */
	public MerchantProfileDo enableMerchantProfileByMerId(String merId,String secretKey);

}
