package com.cwkj.scf.component.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.SupplierRateDo;

/**
 * 供应商利率.
 * @author ljc
 * @version  v1.0
 */
public interface SupplierRateComponent {
	
	/**
	 * 添加供应商利率.
	 * @param supplierRate
	 * @return
	 */
	public Integer insertSupplierRate(SupplierRateDo supplierRate);
	
	/**
	 * 获取供应商利率数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierRateDo> querySupplierRateList(Map<String,Object> selectItem);

	/**
	 * 获取供应商利率数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<SupplierRateDo> querySupplierRateListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改供应商利率.
	 * @param supplierRate
	 * @return
	 */
	public Integer updateSupplierRateById(SupplierRateDo supplierRate);

	/**
	 * 根据Id删除供应商利率.
	 * @param id
	 * @return
	 */
	public Integer deleteSupplierRateById(Integer id);

	/**
	 * 根据Id获取供应商利率.
	 * @param id
	 * @return
	 */
	public SupplierRateDo findSupplierRateById(Integer id);	
}
