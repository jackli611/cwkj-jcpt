package com.cwkj.scf.component.products;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.finances.FinanceOrgDo;
import com.cwkj.scf.model.products.PersonalLoanProductDo;

/**
 * 资金机构个贷产品.
 * @author ljc
 * @version  v1.0
 */
public interface PersonalLoanProductComponent {
	
	/**
	 * 添加资金机构个贷产品.
	 * @param personalLoanProduct
	 * @return
	 */
	public Integer insertPersonalLoanProduct(PersonalLoanProductDo personalLoanProduct);
	
	/**
	 * 获取资金机构个贷产品数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PersonalLoanProductDo> queryPersonalLoanProductList(Map<String, Object> selectItem);

	/**
	 * 获取资金机构个贷产品数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PersonalLoanProductDo> queryPersonalLoanProductListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改资金机构个贷产品.
	 * @param personalLoanProduct
	 * @return
	 */
	public Integer updatePersonalLoanProductById(PersonalLoanProductDo personalLoanProduct);

	/**
	 * 根据Id删除资金机构个贷产品.
	 * @param id
	 * @return
	 */
	public Integer deletePersonalLoanProductById(Long id);

	/**
	 * 根据Id获取资金机构个贷产品.
	 * @param id
	 * @return
	 */
	public PersonalLoanProductDo findPersonalLoanProductById(Long id);


	/**
	 * 根据资金机构ID获取产品列表
	 * @param financeId 资金机构ID
	 * @return
	 */
	public List<PersonalLoanProductDo> queryByFinanceId(String financeId);

	/**
	 * 根据资金机构及产品类型获取产品记录
	 * @param financeId 资金机构ID
	 * @param productType 产品类型
	 * @return
	 */
	public List<PersonalLoanProductDo> queryByFinanceIdAndProductType(String financeId,Integer productType);


	/**
	 * 个贷产品资金机构列表
	 * @return
	 */
	public List<FinanceOrgDo> personalLoanFinanceOrgList();
}
