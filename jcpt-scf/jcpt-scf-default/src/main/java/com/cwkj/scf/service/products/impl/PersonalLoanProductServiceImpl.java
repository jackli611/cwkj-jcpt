package com.cwkj.scf.service.products.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cwkj.scf.model.finances.FinanceOrgDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.products.PersonalLoanProductComponent;
import com.cwkj.scf.model.products.PersonalLoanProductDo;
import com.cwkj.scf.service.products.PersonalLoanProductService;

/**
 * 资金机构个贷产品.
 * @author ljc
 * @version  v1.0
 */
@Service("personalLoanProductService")
public class PersonalLoanProductServiceImpl  implements PersonalLoanProductService{
	
	private PersonalLoanProductComponent personalLoanProductComponent;
	
	@Autowired
	public void setPersonalLoanProductComponent(PersonalLoanProductComponent personalLoanProductComponent)
	{
		this.personalLoanProductComponent=personalLoanProductComponent;
	}
	
	
	@Override
	public Integer insertPersonalLoanProduct(PersonalLoanProductDo personalLoanProduct)
	{
		Date now=new Date();
		personalLoanProduct.setCreateTime(now);
		personalLoanProduct.setUpdateTime(now);
		return personalLoanProductComponent.insertPersonalLoanProduct(personalLoanProduct);
	}
	
	@Override
	public List<PersonalLoanProductDo> queryPersonalLoanProductList(Map<String,Object> selectItem)
	{
		return personalLoanProductComponent.queryPersonalLoanProductList(selectItem);
	}

	@Override
	public PageDo<PersonalLoanProductDo> queryPersonalLoanProductListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return personalLoanProductComponent.queryPersonalLoanProductListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updatePersonalLoanProductById(PersonalLoanProductDo personalLoanProduct)
	{
		return personalLoanProductComponent.updatePersonalLoanProductById(personalLoanProduct);
	}

	@Override
	public Integer deletePersonalLoanProductById(Long id)
	{
		return personalLoanProductComponent.deletePersonalLoanProductById(id);
	}

	@Override
	public PersonalLoanProductDo findPersonalLoanProductById(Long id)
	{
		return personalLoanProductComponent.findPersonalLoanProductById(id);
	}

	@Override
	public List<PersonalLoanProductDo> queryByFinanceId(String financeId) {
		return personalLoanProductComponent.queryByFinanceId(financeId);
	}

	@Override
	public List<PersonalLoanProductDo> queryByFinanceIdAndProductType(String financeId, Integer productType) {
		return personalLoanProductComponent.queryByFinanceIdAndProductType(financeId, productType);
	}

	@Override
	public List<FinanceOrgDo> personalLoanFinanceOrgList() {
		return personalLoanProductComponent.personalLoanFinanceOrgList();
	}
}

