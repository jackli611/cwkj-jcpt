package com.cwkj.scf.service.shares.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.scf.service.shares.ShareUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.shares.ShareUserComponent;
import com.cwkj.scf.model.shares.ShareUserDo;

/**
 * 分享用户.
 * @author ljc
 * @version  v1.0
 */
@Service("shareUserService")
public class ShareUserServiceImpl  implements ShareUserService {
	
	private ShareUserComponent shareUserComponent;
	@Autowired
	private SysUserService sysUserService;

	
	@Autowired
	public void setShareUserComponent(ShareUserComponent shareUserComponent)
	{
		this.shareUserComponent=shareUserComponent;
	}
	
	
	@Override
	public Integer insertShareUser(ShareUserDo shareUser)
	{
		Date now=new Date();
		shareUser.setCreateTime(now);
		shareUser.setUpdateTime(now);
		return shareUserComponent.insertShareUser(shareUser);
	}
	
	@Override
	public List<ShareUserDo> queryShareUserList(Map<String,Object> selectItem)
	{
		return shareUserComponent.queryShareUserList(selectItem);
	}

	@Override
	public PageDo<ShareUserDo> queryShareUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return shareUserComponent.queryShareUserListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateShareUserById(ShareUserDo shareUser)
	{
		return shareUserComponent.updateShareUserById(shareUser);
	}

	@Override
	public Integer deleteShareUserById(Long id)
	{
		return shareUserComponent.deleteShareUserById(id);
	}

	@Override
	public ShareUserDo findShareUserById(Long id)
	{
		return shareUserComponent.findShareUserById(id);
	}

	@Override
	public ShareUserDo findShareUserByUserId(Integer userId) {
		return shareUserComponent.findShareUserByUserId(userId);
	}

	@Override
	public ShareUserDo myShareUser(Integer userId){

		if(userId!=null) {
			SysUserDo sysUserDo= sysUserService.findSysUserById(userId);
			if(sysUserDo!=null) {
				ShareUserDo shareUserDo = findShareUserByUserId(userId);
				if (shareUserDo == null) {
					shareUserDo = new ShareUserDo();
					shareUserDo.setUserId(sysUserDo.getId());
					shareUserDo.setTelphone(sysUserDo.getUserName());
					shareUserDo.setPartnerStatus(ShareUserDo.PARTNERSTATUS_DEFAULT);
					insertShareUser(shareUserDo);
				}

				return shareUserDo;
			}
		}
		return null;
	}

}
