package com.cwkj.scf.component.property.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.PropertyOrgComponent;
import com.cwkj.scf.dao.property.PropertyOrgDao;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.model.property.PropertyOrgDo;

/**
 * 物业机构.
 * @author ljc
 * @version  v1.0
 */
@Component("propertyOrgComponent")
public class PropertyOrgComponentImpl  implements PropertyOrgComponent{
	
	private PropertyOrgDao propertyOrgDao;
	
	@Autowired
	public void setPropertyOrgDao(PropertyOrgDao propertyOrgDao)
	{
		this.propertyOrgDao=propertyOrgDao;
	}
	
	
	@Override
	public Integer insertPropertyOrg(PropertyOrgDo propertyOrg)
	{
		return propertyOrgDao.insert(propertyOrg);
	}
	
	@Override
	public List<PropertyOrgDo> queryPropertyOrgList(Map<String,Object> selectItem)
	{
		return propertyOrgDao.queryList(selectItem);
	}

	@Override
	public PageDo<PropertyOrgDo> queryPropertyOrgListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PropertyOrgDo> pageBean=new PageDo<PropertyOrgDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(propertyOrgDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updatePropertyOrgById(PropertyOrgDo propertyOrg)
	{
		return propertyOrgDao.updateById(propertyOrg);
	}

	@Override
	public Integer deletePropertyOrgById(String id)
	{
		return propertyOrgDao.deleteById(id);
	}

	@Override
	public PropertyOrgDo findPropertyOrgById(String id)
	{
		return propertyOrgDao.findById(id);
	}


	@Override
	public Integer updatePropertyNameInfo(PropertyOrgDo propertyOrg) {
		return propertyOrgDao.updatePropertyNameInfo(propertyOrg);
	}


	@Override
	public Integer updateBusinessLicense(String id, String businessLicense) {
		return propertyOrgDao.updateBusinessLicense(id, businessLicense);
	}


	@Override
	public Integer updateLogo(String id, String logo) {
		return propertyOrgDao.updateLogo(id, logo);
	}


	@Override
	public Integer updateStatus(String id, Integer status) {
		return propertyOrgDao.updateStatus(id, status);
	}


	@Override
	public Integer addSysUser(PropertyOrgDo propertyOrg) {
		return propertyOrgDao.addSysUser(propertyOrg);
	}


	@Override
	public List<ProductDo> queryProductList(String propertyOrgId) {
		return propertyOrgDao.queryProductList(propertyOrgId);
	}


	@Override
	public List<PropertyorgProduct> findProduct(Map<String,Object> selectItem) {
		return propertyOrgDao.findProduct(selectItem);
	}

	
}
