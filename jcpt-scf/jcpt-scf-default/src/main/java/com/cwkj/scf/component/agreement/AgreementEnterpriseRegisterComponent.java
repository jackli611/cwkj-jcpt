package com.cwkj.scf.component.agreement;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.agreement.AgreementEnterpriseRegisterDo;

/**
 * 签章企业注册.
 * @author ljc
 * @version  v1.0
 */
public interface AgreementEnterpriseRegisterComponent {
	
	/**
	 * 添加签章企业注册.
	 * @param agreementEnterpriseRegister
	 * @return
	 */
	public Integer insertAgreementEnterpriseRegister(AgreementEnterpriseRegisterDo agreementEnterpriseRegister);
	
	/**
	 * 获取签章企业注册数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementEnterpriseRegisterDo> queryAgreementEnterpriseRegisterList(Map<String, Object> selectItem);

	/**
	 * 获取签章企业注册数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<AgreementEnterpriseRegisterDo> queryAgreementEnterpriseRegisterListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改签章企业注册.
	 * @param agreementEnterpriseRegister
	 * @return
	 */
	public Integer updateAgreementEnterpriseRegisterById(AgreementEnterpriseRegisterDo agreementEnterpriseRegister);

	/**
	 * 根据Id删除签章企业注册.
	 * @param id
	 * @return
	 */
	public Integer deleteAgreementEnterpriseRegisterById(String id);

	/**
	 * 根据Id获取签章企业注册.
	 * @param id
	 * @return
	 */
	public AgreementEnterpriseRegisterDo findAgreementEnterpriseRegisterById(String id);	
}
