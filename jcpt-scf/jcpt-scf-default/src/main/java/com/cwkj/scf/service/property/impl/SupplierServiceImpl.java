package com.cwkj.scf.service.property.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.scf.component.property.SupplierAuditComponent;
import com.cwkj.scf.component.property.SupplierComponent;
import com.cwkj.scf.model.property.SupplierAuditDo;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.service.property.SupplierService;

/**
 * 供应商.
 * @author ljc
 * @version  v1.0
 */
@Component("supplierService")
public class SupplierServiceImpl  implements SupplierService{
	
	private SupplierComponent supplierComponent;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SupplierAuditComponent supplierAuditComponent;
	
	@Autowired
	public void setSupplierComponent(SupplierComponent supplierComponent)
	{
		this.supplierComponent=supplierComponent;
	}
	
	
	@Override
	public Integer insertSupplier(SupplierDo supplier)
	{
		supplier.setId(sysSeqNumberService.daySeqnumberString());
		Date now=new Date();
		supplier.setCreateTime(now);
		supplier.setUpdateTime(now);
		return supplierComponent.insertSupplier(supplier);
	}
	
	@Override
	public List<SupplierDo> querySupplierList(Map<String,Object> selectItem)
	{
		return supplierComponent.querySupplierList(selectItem);
	}

	@Override
	public PageDo<SupplierDo> querySupplierListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return supplierComponent.querySupplierListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateSupplierById(SupplierDo supplier)
	{
		Date now=new Date();
		supplier.setUpdateTime(now);
		return supplierComponent.updateSupplierById(supplier);
	}

	@Override
	public Integer deleteSupplierById(String id)
	{
		return supplierComponent.deleteSupplierById(id);
	}

	@Override
	public SupplierDo findSupplierById(String id)
	{
		return supplierComponent.findSupplierById(id);
	}


	@Override
	public Integer updateStatus(String id, Integer status) {
		return supplierComponent.updateStatus(id, status);
	}

	@Override
	public  Integer updatePlatformStatus(String id,Integer platformStatus){
		return supplierComponent.updatePlatformStatus(id,platformStatus);
	}

	@Override
	public Integer saveBusinessLicense(String id, String businessLicense) {
		return supplierComponent.saveBusinessLicense(id, businessLicense);
	}


	@Override
	public Integer addSysUserId(String id, Integer userId) {
		return supplierComponent.addSysUserId(id, userId);
	}


	@Override
	public Integer registerSupplier(SupplierDo supplier,String password) {
		
		AssertUtils.isNotBlank(supplier.getPropertyId(), "所属核心企业信息不能为空");
		AssertUtils.isNotBlank(supplier.getMobile(), "手机号不能为空");
		AssertUtils.isNotBlank(supplier.getNames(), "供应商名称不能为空");
		AssertUtils.isNotBlank(password, "密码不能为空");
		
		 SysUserDo user=sysUserService.findSysUserByUserName(supplier.getMobile());
			if(user!=null)
			{
				throw new BusinessException("手机号已被注册");
			}
			user=new SysUserDo();
			user.setLocked(1);//未锁定
			user.setUserName(supplier.getMobile());
			user.setRealName(supplier.getNames());
			user.setTelphone(supplier.getMobile());
			String psw=DigestUtils.md5Hex(password+ SystemConst.PASS_KEY); 
			user.setPassword(psw);
			Integer dbRes=sysUserService.insertSysUser(user);
			if(dbRes>0)
			{
				sysUserService.modifyUserRoles(new String[]{SystemConst.SYS_ROLE_SUPPLIER}, user.getId());
			}
			supplier.setSupplierNo(sysSeqNumberService.daySeqnumberString("S"));
			supplier.setUserId(user.getId());
			supplier.setRecordUserId(supplier.getUserId());
			supplier.setStatus(SupplierDo.STATUS_INIT);
			supplier.setPlatformStatus(SupplierDo.PLATFORMSTATUS_PENDING);//待审核
			dbRes=insertSupplier(supplier);
			AssertUtils.isTrue(dbRes!=null && dbRes>0,"注册保存失败");
		return dbRes;
	}


	@Override
	public Integer auditSupplierStatus(SupplierAuditDo supplierAudit) {
		Date now=new Date();
		supplierAudit.setCreateTime(now);
		supplierAudit.setUpdateTime(now);
		Integer dbRes= supplierAuditComponent.insertSupplierAudit(supplierAudit);
		AssertUtils.isTrue(dbRes!=null && dbRes>0,"保存审核结果失败");
		
		//更新供应商状态和等级		
		SupplierDo supplier = new SupplierDo();
		supplier.setId(supplierAudit.getSupplierId());
		supplier.setStatus(supplierAudit.getStatus());
		supplier.setLevel(supplierAudit.getLevel());
		supplier.setUpdateTime(now);
		dbRes= supplierComponent.updateByPrimaryKeySelective(supplier );
		return dbRes;
	}

	@Override
	public Integer auditSupplierPlatformStatus(SupplierAuditDo supplierAudit){
		Date now=new Date();
		supplierAudit.setCreateTime(now);
		supplierAudit.setUpdateTime(now);
		Integer dbRes= supplierAuditComponent.insertSupplierAudit(supplierAudit);
		AssertUtils.isTrue(dbRes!=null && dbRes>0,"保存审核结果失败");
		dbRes= supplierComponent.updatePlatformStatus(supplierAudit.getSupplierId(), supplierAudit.getStatus());
		return dbRes;
	}

	@Override
	public List<SupplierAuditDo> findSupplierAuditList(String supplierId) {
		Map<String, Object> selectItem = new HashMap<String, Object>();
		selectItem.put("supplierId", supplierId);
		return supplierAuditComponent.querySupplierAuditList(selectItem);
	}

	@Override
	public SupplierDo findSupplierByUserId(Integer userId) {
		Map<String, Object> selectItem = new HashMap<String, Object>();
		selectItem.put("userId", userId);
		List<SupplierDo> supLst = this.querySupplierList(selectItem);
		if (null == supLst || supLst.size() != 1) {
			return null;
		}
		return supLst.get(0);
	}
	
	/**
	 * 根据Id修改供应商.
	 * @param supplier
	 * @return
	 */
	public Integer updateByPrimaryKeySelective(SupplierDo supplier)
	{
		return supplierComponent.updateByPrimaryKeySelective(supplier);
	}
 
}
