package com.cwkj.scf.service.merchant.impl;

import java.util.List;
import java.util.Map;

import com.cwkj.scf.service.merchant.MerchantApiRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.merchant.MerchantApiRequestComponent;
import com.cwkj.scf.model.merchant.MerchantApiRequestDo;

/**
 * 商户API请求.
 * @author ljc
 * @version  v1.0
 */
@Service("merchantApiRequestService")
public class MerchantApiRequestServiceImpl  implements MerchantApiRequestService {
	
	private MerchantApiRequestComponent merchantApiRequestComponent;
	
	@Autowired
	public void setMerchantApiRequestComponent(MerchantApiRequestComponent merchantApiRequestComponent)
	{
		this.merchantApiRequestComponent=merchantApiRequestComponent;
	}
	
	
	@Override
	public Integer insertMerchantApiRequest(MerchantApiRequestDo merchantApiRequest)
	{
		return merchantApiRequestComponent.insertMerchantApiRequest(merchantApiRequest);
	}
	
	@Override
	public List<MerchantApiRequestDo> queryMerchantApiRequestList(Map<String,Object> selectItem)
	{
		return merchantApiRequestComponent.queryMerchantApiRequestList(selectItem);
	}

	@Override
	public PageDo<MerchantApiRequestDo> queryMerchantApiRequestListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return merchantApiRequestComponent.queryMerchantApiRequestListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateMerchantApiRequestByTrxId(MerchantApiRequestDo merchantApiRequest)
	{
		return merchantApiRequestComponent.updateMerchantApiRequestByTrxId(merchantApiRequest);
	}

	@Override
	public Integer deleteMerchantApiRequestByTrxId(String trxId)
	{
		return merchantApiRequestComponent.deleteMerchantApiRequestByTrxId(trxId);
	}

	@Override
	public MerchantApiRequestDo findMerchantApiRequestByTrxId(String trxId)
	{
		return merchantApiRequestComponent.findMerchantApiRequestByTrxId(trxId);
	}	
}
