package com.cwkj.scf.component.merchant.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.merchant.MerchantApiRequestComponent;
import com.cwkj.scf.dao.merchant.MerchantApiRequestDao;
import com.cwkj.scf.model.merchant.MerchantApiRequestDo;

/**
 * 商户API请求.
 * @author ljc
 * @version  v1.0
 */
@Component("merchantApiRequestComponent")
public class MerchantApiRequestComponentImpl  implements MerchantApiRequestComponent{
	
	private MerchantApiRequestDao merchantApiRequestDao;
	
	@Autowired
	public void setMerchantApiRequestDao(MerchantApiRequestDao merchantApiRequestDao)
	{
		this.merchantApiRequestDao=merchantApiRequestDao;
	}
	
	
	@Override
	public Integer insertMerchantApiRequest(MerchantApiRequestDo merchantApiRequest)
	{
		return merchantApiRequestDao.insert(merchantApiRequest);
	}
	
	@Override
	public List<MerchantApiRequestDo> queryMerchantApiRequestList(Map<String,Object> selectItem)
	{
		return merchantApiRequestDao.queryList(selectItem);
	}

	@Override
	public PageDo<MerchantApiRequestDo> queryMerchantApiRequestListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<MerchantApiRequestDo> pageBean=new PageDo<MerchantApiRequestDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(merchantApiRequestDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateMerchantApiRequestByTrxId(MerchantApiRequestDo merchantApiRequest)
	{
		return merchantApiRequestDao.updateByTrxId(merchantApiRequest);
	}

	@Override
	public Integer deleteMerchantApiRequestByTrxId(String trxId)
	{
		return merchantApiRequestDao.deleteByTrxId(trxId);
	}

	@Override
	public MerchantApiRequestDo findMerchantApiRequestByTrxId(String trxId)
	{
		return merchantApiRequestDao.findByTrxId(trxId);
	}


}
