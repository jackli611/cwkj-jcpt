package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.SupplierInvitationDo;

/**
 * 供应商邀请注册.
 * @author ljc
 * @version  v1.0
 */
@Repository("supplierInvitationDao")
public interface SupplierInvitationDao{
	
	/**
	 * 添加供应商邀请注册.
	 * @param supplierInvitation
	 * @return
	 */
	public Integer insert(SupplierInvitationDo supplierInvitation);
	
	/**
	 * 获取供应商邀请注册数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierInvitationDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取供应商邀请注册数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierInvitationDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改供应商邀请注册.
	 * @param supplierInvitation
	 * @return
	 */
	public Integer updateById(SupplierInvitationDo supplierInvitation);

	/**
	 * 根据Id删除供应商邀请注册.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Integer id);

	/**
	 * 根据Id获取供应商邀请注册.
	 * @param id
	 * @return
	 */
	public SupplierInvitationDo findById(Integer id);
 	
	/**
	 * 根据邀请注册码获取供应商邀请注册
	 * @param inviteCode 注册码
	 * @return
	 */
	public SupplierInvitationDo findByInviteCode(String inviteCode);

	/**
	 * 根据核心企业ID获取供应商邀请注册码
	 * @param propertyId 核心企业ID
	 * @return
	 */
	public SupplierInvitationDo findByPropertyId(String propertyId);

}
