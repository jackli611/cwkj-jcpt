package com.cwkj.scf.component.shares;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.shares.SharePersonalLoanOrder;

import java.util.Map;

/**
 *个贷分享订单
 * @author ljc
 * @version  v1.0
 */
public interface SharePersonalLoanOrderComponent {


    /**
     * 查询分享个贷订单列表
     * @param pageIndex
     * @param pageSize
     * @param selectItem
     * @return
     */
    public PageDo<SharePersonalLoanOrder> querySharePersonalLoanOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);


}
