package com.cwkj.scf.dao.products;

import java.util.List;
import java.util.Map;

import com.cwkj.scf.model.finances.FinanceOrgDo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.products.PersonalLoanProductDo;

/**
 * 资金机构个贷产品.
 * @author ljc
 * @version  v1.0
 */
@Repository("personalLoanProductDao")
public interface PersonalLoanProductDao{
	
	/**
	 * 添加资金机构个贷产品.
	 * @param personalLoanProduct
	 * @return
	 */
	public Integer insert(PersonalLoanProductDo personalLoanProduct);
	
	/**
	 * 获取资金机构个贷产品数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PersonalLoanProductDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取资金机构个贷产品数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<PersonalLoanProductDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改资金机构个贷产品.
	 * @param personalLoanProduct
	 * @return
	 */
	public Integer updateById(PersonalLoanProductDo personalLoanProduct);

	/**
	 * 根据Id删除资金机构个贷产品.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Long id);

	/**
	 * 根据Id获取资金机构个贷产品.
	 * @param id
	 * @return
	 */
	public PersonalLoanProductDo findById(Long id);


	/**
	 * 根据资金机构ID获取产品列表
	 * @param financeId 资金机构ID
	 * @return
	 */
	public List<PersonalLoanProductDo> queryByFinanceId(String financeId);

	/**
	 * 根据资金机构及产品类型获取产品记录
	 * @param financeId 资金机构ID
	 * @param productType 产品类型
	 * @return
	 */
	public List<PersonalLoanProductDo> queryByFinanceIdAndProductType(@Param("financeId") String financeId, @Param("productType") Integer productType);

	/**
	 * 个贷产品资金机构列表
	 * @return
	 */
	public List<FinanceOrgDo> personalLoanFinanceOrgList();
 	
	 
}
