package com.cwkj.scf.service.agreement.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.agreement.AgreementSignComponent;
import com.cwkj.scf.model.agreement.AgreementSignDo;
import com.cwkj.scf.service.agreement.AgreementSignService;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
@Service("agreementSignService")
public class AgreementSignServiceImpl  implements AgreementSignService{
	
	private AgreementSignComponent agreementSignComponent;
	
	@Autowired
	public void setAgreementSignComponent(AgreementSignComponent agreementSignComponent)
	{
		this.agreementSignComponent=agreementSignComponent;
	}
	
	
	@Override
	public Integer insertAgreementSign(AgreementSignDo agreementSign)
	{
		Date now=new Date();
		agreementSign.setCreateTime(now);
		agreementSign.setUpdateTime(now);
		return agreementSignComponent.insertAgreementSign(agreementSign);
	}
	
	@Override
	public List<AgreementSignDo> queryAgreementSignList(Map<String,Object> selectItem)
	{
		return agreementSignComponent.queryAgreementSignList(selectItem);
	}

	@Override
	public PageDo<AgreementSignDo> queryAgreementSignListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return agreementSignComponent.queryAgreementSignListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateAgreementSignByContractNo(AgreementSignDo agreementSign)
	{
		return agreementSignComponent.updateAgreementSignByContractNo(agreementSign);
	}

	@Override
	public Integer deleteAgreementSignByContractNo(String contractNo)
	{
		return agreementSignComponent.deleteAgreementSignByContractNo(contractNo);
	}

	@Override
	public AgreementSignDo findAgreementSignByContractNo(String contractNo)
	{
		return agreementSignComponent.findAgreementSignByContractNo(contractNo);
	}

	@Override
	public Integer updateUploadPdfStatus(String contractNo, String status) {
		return agreementSignComponent.updateUploadPdfStatus(contractNo,status);
	}

	@Override
	public Integer updateSignStatus(String contractNo, String status) {
		return agreementSignComponent.updateSignStatus(contractNo,status);
	}

	@Override
	public Integer updateCompletionStatus(String contractNo, String status) {
		return agreementSignComponent.updateCompletionStatus(contractNo, status);
	}
}
