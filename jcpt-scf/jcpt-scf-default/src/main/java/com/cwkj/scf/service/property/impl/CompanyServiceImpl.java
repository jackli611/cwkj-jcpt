package com.cwkj.scf.service.property.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.component.property.CompanyComponent;
import com.cwkj.scf.model.property.CompanyDo;
import com.cwkj.scf.service.property.CompanyService;

/**
 * 企业.
 * @author ljc
 * @version  v1.0
 */
@Component("companyService")
public class CompanyServiceImpl  implements CompanyService{
	
	private CompanyComponent companyComponent;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	
	@Autowired
	public void setCompanyComponent(CompanyComponent companyComponent)
	{
		this.companyComponent=companyComponent;
	}
	
	
	@Override
	public Integer insertCompany(CompanyDo company)
	{
		Date now=new Date();
		company.setId(sysSeqNumberService.daySeqnumberString());
		company.setCreateTime(now);
		company.setUpdateTime(now);
		if(company.getStatus() == null ) {
			company.setStatus(CompanyDo.STATUS_VALID);
		}
		return companyComponent.insertCompany(company);
	}
	
	@Override
	public List<CompanyDo> queryCompanyList(Map<String,Object> selectItem)
	{
		return companyComponent.queryCompanyList(selectItem);
	}

	@Override
	public PageDo<CompanyDo> queryCompanyListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return companyComponent.queryCompanyListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateCompanyById(CompanyDo company)
	{
		Date now=new Date();
		company.setUpdateTime(now);
		return companyComponent.updateCompanyById(company);
	}

	@Override
	public Integer deleteCompanyById(String id)
	{
		return companyComponent.deleteCompanyById(id);
	}

	@Override
	public CompanyDo findCompanyById(String id)
	{
		return companyComponent.findCompanyById(id);
	}


	@Override
	public Integer updateBusinessLicense(String id, String businessLicense) {
		return companyComponent.updateBusinessLicense(id, businessLicense);
	}	
}
