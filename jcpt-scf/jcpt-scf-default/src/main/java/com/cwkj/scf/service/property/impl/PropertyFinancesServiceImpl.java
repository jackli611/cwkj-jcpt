package com.cwkj.scf.service.property.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcpt.util.BusinessException;
import com.cwkj.scf.component.property.PropertyFinancesComponent;
import com.cwkj.scf.dao.loan.OrderMatchLogMapper;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.model.loan.OrderMatchLog;
import com.cwkj.scf.model.property.PropertyFinancesDo;
import com.cwkj.scf.service.loan.ILoanOrderService;
import com.cwkj.scf.service.property.PropertyFinancesService;

/**
 * 物业的资金机构.
 * @author ljc
 * @version  v1.0
 */
@Service("propertyFinancesService")
public class PropertyFinancesServiceImpl  implements PropertyFinancesService{
	
	private PropertyFinancesComponent propertyFinancesComponent;
	
	@Autowired
	private ILoanOrderService loanorderService;
	@Autowired
	private OrderMatchLogMapper orderMatchLogMapper;
	
	
	@Autowired
	public void setPropertyFinancesComponent(PropertyFinancesComponent propertyFinancesComponent)
	{
		this.propertyFinancesComponent=propertyFinancesComponent;
	}
	
	
	@Override
	public Integer insertPropertyFinances(PropertyFinancesDo propertyFinances)
	{
		Date now=new Date();
		propertyFinances.setCreateTime(now);
		propertyFinances.setUpdateTime(now);
		return propertyFinancesComponent.insertPropertyFinances(propertyFinances);
	}
	
	@Override
	public List<PropertyFinancesDo> queryPropertyFinancesList(Map<String,Object> selectItem)
	{
		return propertyFinancesComponent.queryPropertyFinancesList(selectItem);
	}

	@Override
	public PageDo<PropertyFinancesDo> queryPropertyFinancesListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return propertyFinancesComponent.queryPropertyFinancesListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updatePropertyFinancesById(PropertyFinancesDo propertyFinances)
	{
		return propertyFinancesComponent.updatePropertyFinancesById(propertyFinances);
	}

	@Override
	public Integer deletePropertyFinancesById(Integer id)
	{
		return propertyFinancesComponent.deletePropertyFinancesById(id);
	}

	@Override
	public PropertyFinancesDo findPropertyFinancesById(Integer id)
	{
		return propertyFinancesComponent.findPropertyFinancesById(id);
	}
	
	
	@Override
	public List<PropertyFinancesDo> queryListByPropertyId(String propertyId)
	{
		return propertyFinancesComponent.queryListByPropertyId(propertyId);
	}

	/**
	 * 根据订单上的核心企业匹配资金方
	 */
	@Override
	public int saveMatchOrder(Long loanId, String financeId, String propertyFinanceId, Integer optUserId) {
		Loanorder order = loanorderService.selectByPrimaryKey(loanId);
		if(null == order) {
			throw new BusinessException("无效的订单id");
		}
		
		if("SCF".equals(order.getProdcode())) {
			matchSCFOrder(order, financeId,  propertyFinanceId,  optUserId);
		}
		
		logMatchRecord(order.getOrdercode(),loanId,financeId,optUserId);
		
		//更改订单状态和填充资金机构字段
		Loanorder tmpOrder = new Loanorder();
		tmpOrder.setLoanid(loanId);
		tmpOrder.setFinorgid(financeId);
		
		return loanorderService.updateOrder(tmpOrder);
	}
	

	/**
	 * 记录匹配记录
	 * @param ordercode
	 * @param loanId
	 * @param financeId
	 * @param optUserId
	 */
	private void logMatchRecord(String ordercode, Long loanId, String financeId, Integer optUserId) {
		OrderMatchLog record = new OrderMatchLog();
		record.setOrdercode(ordercode);
		record.setLoanid(loanId);
		record.setFinaceid(financeId);
		record.setOptuserid(Long.valueOf(optUserId));
		record.setCreatetime(new Date());
		orderMatchLogMapper.insertSelective(record );
		
	}


	/**
	 * 供应链订单匹配处理
	 * @param order
	 * @param financeId
	 * @param propertyFinanceId
	 * @param optUserId
	 */
	private void matchSCFOrder(Loanorder order, String financeId, String propertyFinanceId, Integer optUserId) {
		PropertyFinancesDo pfDo = propertyFinancesComponent.findPropertyFinancesById(Integer.parseInt(propertyFinanceId));
		if(null == pfDo) {
			throw new BusinessException("无效的金融机构授信id");
		}
		
		//供应链产品更新已使用额度
		PropertyFinancesDo tmpPfDo = new PropertyFinancesDo();
		tmpPfDo.setId(Integer.parseInt(propertyFinanceId));
		tmpPfDo.setUsedAmout(order.getLoanamount() == null? order.getApplyamount():order.getLoanamount());
		int result = propertyFinancesComponent.updateUsedAmt(tmpPfDo);
		AssertUtils.isTrue(result == 1, "更新已使用额度失败");
		
	}


	@Override
	public List<Map<String, Object>> selectMatchLog(Map<String, Object> selectItem) {
		return orderMatchLogMapper.selectMatchLog(selectItem);
	}
	
}
