package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.SupplierRateDo;

/**
 * 供应商利率.
 * @author ljc
 * @version  v1.0
 */
@Repository("supplierRateDao")
public interface SupplierRateDao{
	
	/**
	 * 添加供应商利率.
	 * @param supplierRate
	 * @return
	 */
	public Integer insert(SupplierRateDo supplierRate);
	
	/**
	 * 获取供应商利率数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierRateDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取供应商利率数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierRateDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改供应商利率.
	 * @param supplierRate
	 * @return
	 */
	public Integer updateById(SupplierRateDo supplierRate);

	/**
	 * 根据Id删除供应商利率.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Integer id);

	/**
	 * 根据Id获取供应商利率.
	 * @param id
	 * @return
	 */
	public SupplierRateDo findById(Integer id);
 	
	 
}
