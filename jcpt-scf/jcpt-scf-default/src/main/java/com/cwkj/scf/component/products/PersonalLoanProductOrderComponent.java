package com.cwkj.scf.component.products;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.products.PersonalLoanProductOrderDo;

/**
 * 个贷产品订单.
 * @author ljc
 * @version  v1.0
 */
public interface PersonalLoanProductOrderComponent {
	
	/**
	 * 添加个贷产品订单.
	 * @param personalLoanProductOrder
	 * @return
	 */
	public Integer insertPersonalLoanProductOrder(PersonalLoanProductOrderDo personalLoanProductOrder);
	
	/**
	 * 获取个贷产品订单数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PersonalLoanProductOrderDo> queryPersonalLoanProductOrderList(Map<String, Object> selectItem);

	/**
	 * 获取个贷产品订单数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<PersonalLoanProductOrderDo> queryPersonalLoanProductOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改个贷产品订单.
	 * @param personalLoanProductOrder
	 * @return
	 */
	public Integer updatePersonalLoanProductOrderById(PersonalLoanProductOrderDo personalLoanProductOrder);

	/**
	 * 根据Id删除个贷产品订单.
	 * @param id
	 * @return
	 */
	public Integer deletePersonalLoanProductOrderById(Long id);

	/**
	 * 根据Id获取个贷产品订单.
	 * @param id
	 * @return
	 */
	public PersonalLoanProductOrderDo findPersonalLoanProductOrderById(Long id);


	/**
	 * 根据借款ID获取个贷产品
	 * @param loanId
	 * @return
	 */
	public List<PersonalLoanProductOrderDo> queryByLoanId(String loanId);
}
