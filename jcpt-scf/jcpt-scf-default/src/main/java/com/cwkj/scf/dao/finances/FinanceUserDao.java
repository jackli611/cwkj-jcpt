package com.cwkj.scf.dao.finances;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.finances.FinanceUserDo;

/**
 * 金融机构用户.
 * @author ljc
 * @version  v1.0
 */
@Repository("financeUserDao")
public interface FinanceUserDao{
	
	/**
	 * 添加金融机构用户.
	 * @param financeUser
	 * @return
	 */
	public Integer insert(FinanceUserDo financeUser);
	
	/**
	 * 获取金融机构用户数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<FinanceUserDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取金融机构用户数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<FinanceUserDo> queryListPage(Map<String,Object> selectItem);
	
	/**
	 * 获取系统账户信息的列表
	 * @param selectItem
	 * @return
	 */
	public List<FinanceUserDo> queryWithSysUserListPage(Map<String,Object> selectItem);

	/**
	 * 根据Id修改金融机构用户.
	 * @param financeUser
	 * @return
	 */
	public Integer updateById(FinanceUserDo financeUser);

	/**
	 * 根据Id删除金融机构用户.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取金融机构用户.
	 * @param id
	 * @return
	 */
	public FinanceUserDo findById(String id);
 	
	 
}
