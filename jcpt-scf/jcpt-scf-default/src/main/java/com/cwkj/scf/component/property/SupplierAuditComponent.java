package com.cwkj.scf.component.property;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.property.SupplierAuditDo;

/**
 * 供应商审核.
 * @author ljc
 * @version  v1.0
 */
public interface SupplierAuditComponent {
	
	/**
	 * 添加供应商审核.
	 * @param supplierAudit
	 * @return
	 */
	public Integer insertSupplierAudit(SupplierAuditDo supplierAudit);
	
	/**
	 * 获取供应商审核数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierAuditDo> querySupplierAuditList(Map<String,Object> selectItem);

	/**
	 * 获取供应商审核数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<SupplierAuditDo> querySupplierAuditListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改供应商审核.
	 * @param supplierAudit
	 * @return
	 */
	public Integer updateSupplierAuditById(SupplierAuditDo supplierAudit);

	/**
	 * 根据Id删除供应商审核.
	 * @param id
	 * @return
	 */
	public Integer deleteSupplierAuditById(Integer id);

	/**
	 * 根据Id获取供应商审核.
	 * @param id
	 * @return
	 */
	public SupplierAuditDo findSupplierAuditById(Integer id);	
}
