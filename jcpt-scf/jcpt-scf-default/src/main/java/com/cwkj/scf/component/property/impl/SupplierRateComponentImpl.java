package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.SupplierRateComponent;
import com.cwkj.scf.dao.property.SupplierRateDao;
import com.cwkj.scf.model.property.SupplierRateDo;

/**
 * 供应商利率.
 * @author ljc
 * @version  v1.0
 */
@Component("supplierRateComponent")
public class SupplierRateComponentImpl  implements SupplierRateComponent{
	
	private SupplierRateDao supplierRateDao;
	
	@Autowired
	public void setSupplierRateDao(SupplierRateDao supplierRateDao)
	{
		this.supplierRateDao=supplierRateDao;
	}
	
	
	@Override
	public Integer insertSupplierRate(SupplierRateDo supplierRate)
	{
		return supplierRateDao.insert(supplierRate);
	}
	
	@Override
	public List<SupplierRateDo> querySupplierRateList(Map<String,Object> selectItem)
	{
		return supplierRateDao.queryList(selectItem);
	}

	@Override
	public PageDo<SupplierRateDo> querySupplierRateListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<SupplierRateDo> pageBean=new PageDo<SupplierRateDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(supplierRateDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateSupplierRateById(SupplierRateDo supplierRate)
	{
		return supplierRateDao.updateById(supplierRate);
	}

	@Override
	public Integer deleteSupplierRateById(Integer id)
	{
		return supplierRateDao.deleteById(id);
	}

	@Override
	public SupplierRateDo findSupplierRateById(Integer id)
	{
		return supplierRateDao.findById(id);
	}	
}
