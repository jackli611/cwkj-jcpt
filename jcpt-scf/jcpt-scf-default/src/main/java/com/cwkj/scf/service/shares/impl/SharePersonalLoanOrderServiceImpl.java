package com.cwkj.scf.service.shares.impl;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.shares.SharePersonalLoanOrderComponent;
import com.cwkj.scf.model.shares.SharePersonalLoanOrder;
import com.cwkj.scf.service.shares.SharePersonalLoanOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 *个贷分享订单
 * @author ljc
 * @version  v1.0
 */
@Service("sharePersonalLoanOrderService")
public class SharePersonalLoanOrderServiceImpl implements SharePersonalLoanOrderService {

    @Autowired
    private SharePersonalLoanOrderComponent sharePersonalLoanOrderComponent;

    @Override
    public PageDo<SharePersonalLoanOrder> querySharePersonalLoanOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem) {
        return sharePersonalLoanOrderComponent.querySharePersonalLoanOrderListPage(pageIndex,pageSize,selectItem);
    }
}
