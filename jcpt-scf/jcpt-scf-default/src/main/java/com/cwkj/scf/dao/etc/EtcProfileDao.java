package com.cwkj.scf.dao.etc;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.etc.EtcProfileDo;

/**
 * ETC基本信息.
 * @author ljc
 * @version  v1.0
 */
@Repository("etcProfileDao")
public interface EtcProfileDao{
	
	/**
	 * 添加ETC基本信息.
	 * @param etcProfile
	 * @return
	 */
	public Integer insert(EtcProfileDo etcProfile);
	
	/**
	 * 获取ETC基本信息数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<EtcProfileDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取ETC基本信息数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<EtcProfileDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改ETC基本信息.
	 * @param etcProfile
	 * @return
	 */
	public Integer updateById(EtcProfileDo etcProfile);

	/**
	 * 根据Id删除ETC基本信息.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Long id);

	/**
	 * 根据Id获取ETC基本信息.
	 * @param id
	 * @return
	 */
	public EtcProfileDo findById(Long id);

	/**
	 * 更新申请卡类型
	 * @param etcProfile
	 * @return
	 */
	public Integer updateCardTypeById(EtcProfileDo etcProfile);
	 
}
