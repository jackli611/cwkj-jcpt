package com.cwkj.scf.service.agreement.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.agreement.AgreementEnterpriseRegisterComponent;
import com.cwkj.scf.model.agreement.AgreementEnterpriseRegisterDo;
import com.cwkj.scf.service.agreement.AgreementEnterpriseRegisterService;

/**
 * 签章企业注册.
 * @author ljc
 * @version  v1.0
 */
@Service("agreementEnterpriseRegisterService")
public class AgreementEnterpriseRegisterServiceImpl  implements AgreementEnterpriseRegisterService{
	
	private AgreementEnterpriseRegisterComponent agreementEnterpriseRegisterComponent;
	
	@Autowired
	public void setAgreementEnterpriseRegisterComponent(AgreementEnterpriseRegisterComponent agreementEnterpriseRegisterComponent)
	{
		this.agreementEnterpriseRegisterComponent=agreementEnterpriseRegisterComponent;
	}
	
	
	@Override
	public Integer insertAgreementEnterpriseRegister(AgreementEnterpriseRegisterDo agreementEnterpriseRegister)
	{
		return agreementEnterpriseRegisterComponent.insertAgreementEnterpriseRegister(agreementEnterpriseRegister);
	}
	
	@Override
	public List<AgreementEnterpriseRegisterDo> queryAgreementEnterpriseRegisterList(Map<String,Object> selectItem)
	{
		return agreementEnterpriseRegisterComponent.queryAgreementEnterpriseRegisterList(selectItem);
	}

	@Override
	public PageDo<AgreementEnterpriseRegisterDo> queryAgreementEnterpriseRegisterListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return agreementEnterpriseRegisterComponent.queryAgreementEnterpriseRegisterListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateAgreementEnterpriseRegisterById(AgreementEnterpriseRegisterDo agreementEnterpriseRegister)
	{
		return agreementEnterpriseRegisterComponent.updateAgreementEnterpriseRegisterById(agreementEnterpriseRegister);
	}

	@Override
	public Integer deleteAgreementEnterpriseRegisterById(String id)
	{
		return agreementEnterpriseRegisterComponent.deleteAgreementEnterpriseRegisterById(id);
	}

	@Override
	public AgreementEnterpriseRegisterDo findAgreementEnterpriseRegisterById(String id)
	{
		return agreementEnterpriseRegisterComponent.findAgreementEnterpriseRegisterById(id);
	}	
}
