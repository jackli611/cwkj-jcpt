package com.cwkj.scf.service.agreement.impl;

import com.cwkj.jcpt.util.AssertUtils;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.scf.model.loan.LoanPerson;
import com.cwkj.scf.model.loan.LoanPersonExample;
import com.cwkj.scf.model.loan.Loanorder;
import com.cwkj.scf.service.agreement.PersonalLoanAgreementService;
import com.cwkj.scf.service.loan.ILoanOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 个贷合同服务
 */
@Service("personalLoanAgreementService")
public class PersonalLoanAgreementServiceImpl implements PersonalLoanAgreementService {

    private static Logger logger= LoggerFactory.getLogger(PersonalLoanAgreementServiceImpl.class);

    @Autowired
    private ILoanOrderService loanOrderSercice;
    @Autowired
    private SysUserService sysUserService;


    @Override
    public Map<String,String> serverContractDataByLoanId(Long loanId)
    {
        Map<String,String> result=new HashMap<String,String>();
            LoanPersonExample example = new LoanPersonExample();
            example.createCriteria().andLoanidEqualTo(loanId);
            List<LoanPerson> personLst = loanOrderSercice.selectOrderPersonByExample(example );
            AssertUtils.isTrue(personLst !=null &&personLst.size()>0,"订单借款人信息不存在");
            LoanPerson loanPerson=personLst.get(0);
            String realName=loanPerson.getRealname();
            String idcardNo=loanPerson.getIdno();
            AssertUtils.notNull(loanPerson.getUserid(),"订单所属账号信息错误");
            SysUserDo sysUserDo=sysUserService.findSysUserById(loanPerson.getUserid().intValue());
            String mobile=sysUserDo.getTelphone();

            Loanorder order = loanOrderSercice.selectOrderAndAttachmentByPrimaryKey(loanId );
            AssertUtils.notNull(order,"借款订单不存在");
            AssertUtils.isNotBlank(order.getOrdercode(),"借款订单号不能为空");

            result.put("realName",realName);
            result.put("idcardNo",idcardNo);
            result.put("mobile",mobile);
            result.put("contractNo",order.getOrdercode());
            result.put("userId",sysUserDo.getId().toString());
        return result;
    }
}
