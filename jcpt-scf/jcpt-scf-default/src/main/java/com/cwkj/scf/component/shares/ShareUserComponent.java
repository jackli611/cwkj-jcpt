package com.cwkj.scf.component.shares;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.shares.ShareUserDo;

/**
 * 分享用户.
 * @author ljc
 * @version  v1.0
 */
public interface ShareUserComponent {
	
	/**
	 * 添加分享用户.
	 * @param shareUser
	 * @return
	 */
	public Integer insertShareUser(ShareUserDo shareUser);
	
	/**
	 * 获取分享用户数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ShareUserDo> queryShareUserList(Map<String, Object> selectItem);

	/**
	 * 获取分享用户数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ShareUserDo> queryShareUserListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改分享用户.
	 * @param shareUser
	 * @return
	 */
	public Integer updateShareUserById(ShareUserDo shareUser);

	/**
	 * 根据Id删除分享用户.
	 * @param id
	 * @return
	 */
	public Integer deleteShareUserById(Long id);

	/**
	 * 根据Id获取分享用户.
	 * @param id
	 * @return
	 */
	public ShareUserDo findShareUserById(Long id);

	/**
	 * 根据用户ID获取分享用户
	 * @param userId
	 * @return
	 */
	public ShareUserDo findShareUserByUserId(Integer userId);
}
