package com.cwkj.scf.dao.merchant;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.merchant.MerchantAccountDo;

/**
 * 商户账号.
 * @author ljc
 * @version  v1.0
 */
@Repository("merchantAccountDao")
public interface MerchantAccountDao{
	
	/**
	 * 添加商户账号.
	 * @param merchantAccount
	 * @return
	 */
	public Integer insert(MerchantAccountDo merchantAccount);
	
	/**
	 * 获取商户账号数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantAccountDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取商户账号数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantAccountDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改商户账号.
	 * @param merchantAccount
	 * @return
	 */
	public Integer updateById(MerchantAccountDo merchantAccount);

	/**
	 * 根据Id删除商户账号.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取商户账号.
	 * @param id
	 * @return
	 */
	public MerchantAccountDo findById(String id);
 	
	 
}
