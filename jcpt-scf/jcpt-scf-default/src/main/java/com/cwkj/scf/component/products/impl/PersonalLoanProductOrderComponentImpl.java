package com.cwkj.scf.component.products.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.products.PersonalLoanProductOrderComponent;
import com.cwkj.scf.dao.products.PersonalLoanProductOrderDao;
import com.cwkj.scf.model.products.PersonalLoanProductOrderDo;

/**
 * 个贷产品订单.
 * @author ljc
 * @version  v1.0
 */
@Component("personalLoanProductOrderComponent")
public class PersonalLoanProductOrderComponentImpl  implements PersonalLoanProductOrderComponent{
	
	private PersonalLoanProductOrderDao personalLoanProductOrderDao;
	
	@Autowired
	public void setPersonalLoanProductOrderDao(PersonalLoanProductOrderDao personalLoanProductOrderDao)
	{
		this.personalLoanProductOrderDao=personalLoanProductOrderDao;
	}
	
	
	@Override
	public Integer insertPersonalLoanProductOrder(PersonalLoanProductOrderDo personalLoanProductOrder)
	{
		return personalLoanProductOrderDao.insert(personalLoanProductOrder);
	}
	
	@Override
	public List<PersonalLoanProductOrderDo> queryPersonalLoanProductOrderList(Map<String,Object> selectItem)
	{
		return personalLoanProductOrderDao.queryList(selectItem);
	}

	@Override
	public PageDo<PersonalLoanProductOrderDo> queryPersonalLoanProductOrderListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PersonalLoanProductOrderDo> pageBean=new PageDo<PersonalLoanProductOrderDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(personalLoanProductOrderDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updatePersonalLoanProductOrderById(PersonalLoanProductOrderDo personalLoanProductOrder)
	{
		return personalLoanProductOrderDao.updateById(personalLoanProductOrder);
	}

	@Override
	public Integer deletePersonalLoanProductOrderById(Long id)
	{
		return personalLoanProductOrderDao.deleteById(id);
	}

	@Override
	public PersonalLoanProductOrderDo findPersonalLoanProductOrderById(Long id)
	{
		return personalLoanProductOrderDao.findById(id);
	}

	@Override
	public List<PersonalLoanProductOrderDo> queryByLoanId(String loanId) {
		return personalLoanProductOrderDao.queryByLoanId(loanId);
	}
}
