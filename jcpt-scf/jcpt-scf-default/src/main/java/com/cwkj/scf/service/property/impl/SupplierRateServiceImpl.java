package com.cwkj.scf.service.property.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.SupplierRateComponent;
import com.cwkj.scf.model.property.SupplierRateDo;
import com.cwkj.scf.service.property.SupplierRateService;

/**
 * 供应商利率.
 * @author ljc
 * @version  v1.0
 */
@Component("supplierRateService")
public class SupplierRateServiceImpl  implements SupplierRateService{
	
	private SupplierRateComponent supplierRateComponent;
	
	@Autowired
	public void setSupplierRateComponent(SupplierRateComponent supplierRateComponent)
	{
		this.supplierRateComponent=supplierRateComponent;
	}
	
	
	@Override
	public Integer insertSupplierRate(SupplierRateDo supplierRate)
	{
		return supplierRateComponent.insertSupplierRate(supplierRate);
	}
	
	@Override
	public List<SupplierRateDo> querySupplierRateList(Map<String,Object> selectItem)
	{
		return supplierRateComponent.querySupplierRateList(selectItem);
	}

	@Override
	public PageDo<SupplierRateDo> querySupplierRateListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return supplierRateComponent.querySupplierRateListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateSupplierRateById(SupplierRateDo supplierRate)
	{
		return supplierRateComponent.updateSupplierRateById(supplierRate);
	}

	@Override
	public Integer deleteSupplierRateById(Integer id)
	{
		return supplierRateComponent.deleteSupplierRateById(id);
	}

	@Override
	public SupplierRateDo findSupplierRateById(Integer id)
	{
		return supplierRateComponent.findSupplierRateById(id);
	}	
}
