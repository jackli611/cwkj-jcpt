package com.cwkj.scf.dao.finances;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.finances.FinanceOrgDo;
import com.cwkj.scf.model.property.PropertyOrgDo;

/**
 * 金融机构.
 * @author ljc
 * @version  v1.0
 */
@Repository("financeOrgDao")
public interface FinanceOrgDao{
	
	/**
	 * 添加金融机构.
	 * @param financeOrg
	 * @return
	 */
	public Integer insert(FinanceOrgDo financeOrg);
	
	/**
	 * 获取金融机构数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<FinanceOrgDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取金融机构数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<FinanceOrgDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改金融机构.
	 * @param financeOrg
	 * @return
	 */
	public Integer updateById(FinanceOrgDo financeOrg);

	/**
	 * 根据Id删除金融机构.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取金融机构.
	 * @param id
	 * @return
	 */
	public FinanceOrgDo findById(String id);
 	
	/**
	 * 修改状态
	 * @param id 物业ID 
	 * @param status 状态值
	 * @return
	 */
	public Integer updateStatus(@Param("id")String id,@Param("status")Integer status);
	
	/**
	 * 修改营业执照信息
	 * @param id 物业ID 
	 * @param businessLicense 营业执照 
	 * @return
	 */
	public Integer updateBusinessLicense(@Param("id")String id,@Param("businessLicense")String businessLicense);
	
	/**
	 * 添加系统账号信息
	 * @param propertyOrg
	 * @return
	 */
	public Integer addSysUser(FinanceOrgDo financeOrg);
	
	/**
	 * 修改物业基本信息
	 * @param propertyOrg
	 * @return
	 */
	public Integer updateFinanceNameInfo(FinanceOrgDo financeOrg);
}
