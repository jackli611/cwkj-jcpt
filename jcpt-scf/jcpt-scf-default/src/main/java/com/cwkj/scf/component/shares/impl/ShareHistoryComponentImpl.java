package com.cwkj.scf.component.shares.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.shares.ShareHistoryComponent;
import com.cwkj.scf.dao.shares.ShareHistoryDao;
import com.cwkj.scf.model.shares.ShareHistoryDo;

/**
 * 分享历史.
 * @author ljc
 * @version  v1.0
 */
@Component("shareHistoryComponent")
public class ShareHistoryComponentImpl  implements ShareHistoryComponent{
	
	private ShareHistoryDao shareHistoryDao;
	
	@Autowired
	public void setShareHistoryDao(ShareHistoryDao shareHistoryDao)
	{
		this.shareHistoryDao=shareHistoryDao;
	}
	
	
	@Override
	public Integer insertShareHistory(ShareHistoryDo shareHistory)
	{
		return shareHistoryDao.insert(shareHistory);
	}
	
	@Override
	public List<ShareHistoryDo> queryShareHistoryList(Map<String,Object> selectItem)
	{
		return shareHistoryDao.queryList(selectItem);
	}

	@Override
	public PageDo<ShareHistoryDo> queryShareHistoryListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<ShareHistoryDo> pageBean=new PageDo<ShareHistoryDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(shareHistoryDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateShareHistoryById(ShareHistoryDo shareHistory)
	{
		return shareHistoryDao.updateById(shareHistory);
	}

	@Override
	public Integer deleteShareHistoryById(Long id)
	{
		return shareHistoryDao.deleteById(id);
	}

	@Override
	public ShareHistoryDo findShareHistoryById(Long id)
	{
		return shareHistoryDao.findById(id);
	}	
}
