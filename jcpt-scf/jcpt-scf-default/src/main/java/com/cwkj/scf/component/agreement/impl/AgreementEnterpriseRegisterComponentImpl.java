package com.cwkj.scf.component.agreement.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.agreement.AgreementEnterpriseRegisterComponent;
import com.cwkj.scf.dao.agreement.AgreementEnterpriseRegisterDao;
import com.cwkj.scf.model.agreement.AgreementEnterpriseRegisterDo;

/**
 * 签章企业注册.
 * @author ljc
 * @version  v1.0
 */
@Component("agreementEnterpriseRegisterComponent")
public class AgreementEnterpriseRegisterComponentImpl  implements AgreementEnterpriseRegisterComponent{
	
	private AgreementEnterpriseRegisterDao agreementEnterpriseRegisterDao;
	
	@Autowired
	public void setAgreementEnterpriseRegisterDao(AgreementEnterpriseRegisterDao agreementEnterpriseRegisterDao)
	{
		this.agreementEnterpriseRegisterDao=agreementEnterpriseRegisterDao;
	}
	
	
	@Override
	public Integer insertAgreementEnterpriseRegister(AgreementEnterpriseRegisterDo agreementEnterpriseRegister)
	{
		return agreementEnterpriseRegisterDao.insert(agreementEnterpriseRegister);
	}
	
	@Override
	public List<AgreementEnterpriseRegisterDo> queryAgreementEnterpriseRegisterList(Map<String,Object> selectItem)
	{
		return agreementEnterpriseRegisterDao.queryList(selectItem);
	}

	@Override
	public PageDo<AgreementEnterpriseRegisterDo> queryAgreementEnterpriseRegisterListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<AgreementEnterpriseRegisterDo> pageBean=new PageDo<AgreementEnterpriseRegisterDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(agreementEnterpriseRegisterDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateAgreementEnterpriseRegisterById(AgreementEnterpriseRegisterDo agreementEnterpriseRegister)
	{
		return agreementEnterpriseRegisterDao.updateById(agreementEnterpriseRegister);
	}

	@Override
	public Integer deleteAgreementEnterpriseRegisterById(String id)
	{
		return agreementEnterpriseRegisterDao.deleteById(id);
	}

	@Override
	public AgreementEnterpriseRegisterDo findAgreementEnterpriseRegisterById(String id)
	{
		return agreementEnterpriseRegisterDao.findById(id);
	}	
}
