package com.cwkj.scf.dao.agreement;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.agreement.AgreementSignDo;

/**
 * .
 * @author ljc
 * @version  v1.0
 */
@Repository("agreementSignDao")
public interface AgreementSignDao{
	
	/**
	 * 添加.
	 * @param agreementSign
	 * @return
	 */
	public Integer insert(AgreementSignDo agreementSign);
	
	/**
	 * 获取数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementSignDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementSignDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据ContractNo修改.
	 * @param agreementSign
	 * @return
	 */
	public Integer updateByContractNo(AgreementSignDo agreementSign);

	/**
	 * 根据ContractNo删除.
	 * @param contractNo
	 * @return
	 */
	public Integer deleteByContractNo(String contractNo);

	/**
	 * 根据ContractNo获取.
	 * @param contractNo
	 * @return
	 */
	public AgreementSignDo findByContractNo(String contractNo);


	/**
	 * 更新上传合同PDF状态
	 * @param contractNo 合同编号
	 * @param status 值使用com.cwkj.scf.model.agreement.AgreementSignDo.STATUS_YES/STATUS_NO
	 * @return
	 */
	public Integer updateUploadPdfStatus(@Param("contractNo") String contractNo,@Param("status") String status);

	/**
	 * 更新合同签章状态
	 * @param contractNo 合同编号
	 * @param status  值使用com.cwkj.scf.model.agreement.AgreementSignDo.STATUS_YES/STATUS_NO
	 * @return
	 */
	public Integer updateSignStatus(@Param("contractNo")String contractNo,@Param("status")String status);

	/**
	 * 更新合同生效状态
	 * @param contractNo
	 * @param status
	 * @return
	 */
	public Integer updateCompletionStatus(@Param("contractNo")String contractNo,@Param("status")String status);

	 
}
