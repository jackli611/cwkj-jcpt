package com.cwkj.scf.service.property.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.component.property.PayInfoComponent;
import com.cwkj.scf.model.property.PayInfoDo;
import com.cwkj.scf.service.property.PayInfoService;

/**
 * 支付信息.
 * @author ljc
 * @version  v1.0
 */
@Component("payInfoService")
public class PayInfoServiceImpl  implements PayInfoService{
	
	private PayInfoComponent payInfoComponent;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	
	@Autowired
	public void setPayInfoComponent(PayInfoComponent payInfoComponent)
	{
		this.payInfoComponent=payInfoComponent;
	}
	
	
	@Override
	public Integer insertPayInfo(PayInfoDo payInfo)
	{
		Date now=new Date();
		payInfo.setId(sysSeqNumberService.daySeqnumberString());
		payInfo.setCreateTime(now);
		payInfo.setUpdateTime(now);
		return payInfoComponent.insertPayInfo(payInfo);
	}
	
	@Override
	public List<PayInfoDo> queryPayInfoList(Map<String,Object> selectItem)
	{
		return payInfoComponent.queryPayInfoList(selectItem);
	}

	@Override
	public PageDo<PayInfoDo> queryPayInfoListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return payInfoComponent.queryPayInfoListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updatePayInfoById(PayInfoDo payInfo)
	{
		Date now=new Date();
		payInfo.setUpdateTime(now);
		return payInfoComponent.updatePayInfoById(payInfo);
	}

	@Override
	public Integer deletePayInfoById(String id)
	{
		return payInfoComponent.deletePayInfoById(id);
	}

	@Override
	public PayInfoDo findPayInfoById(String id)
	{
		return payInfoComponent.findPayInfoById(id);
	}	
}
