package com.cwkj.scf.service.etc.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cwkj.scf.service.etc.EtcProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.etc.EtcProfileComponent;
import com.cwkj.scf.model.etc.EtcProfileDo;

/**
 * ETC基本信息.
 * @author ljc
 * @version  v1.0
 */
@Service("etcProfileService")
public class EtcProfileServiceImpl  implements EtcProfileService {
	
	private EtcProfileComponent etcProfileComponent;
	
	@Autowired
	public void setEtcProfileComponent(EtcProfileComponent etcProfileComponent)
	{
		this.etcProfileComponent=etcProfileComponent;
	}
	
	
	@Override
	public Integer insertEtcProfile(EtcProfileDo etcProfile)
	{
		Date now=new Date();
		etcProfile.setCreateTime(now);
		etcProfile.setUpdateTime(now);
		return etcProfileComponent.insertEtcProfile(etcProfile);
	}
	
	@Override
	public List<EtcProfileDo> queryEtcProfileList(Map<String,Object> selectItem)
	{
		return etcProfileComponent.queryEtcProfileList(selectItem);
	}

	@Override
	public PageDo<EtcProfileDo> queryEtcProfileListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return etcProfileComponent.queryEtcProfileListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateEtcProfileById(EtcProfileDo etcProfile)
	{
		return etcProfileComponent.updateEtcProfileById(etcProfile);
	}

	@Override
	public Integer deleteEtcProfileById(Long id)
	{
		return etcProfileComponent.deleteEtcProfileById(id);
	}

	@Override
	public EtcProfileDo findEtcProfileById(Long id)
	{
		return etcProfileComponent.findEtcProfileById(id);
	}

	@Override
	public Integer updateCardTypeById(EtcProfileDo etcProfile) {
		return etcProfileComponent.updateCardTypeById(etcProfile);
	}
}
