package com.cwkj.scf.dao.merchant;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.merchant.MerchantApiRequestDo;

/**
 * 商户API请求.
 * @author ljc
 * @version  v1.0
 */
@Repository("merchantApiRequestDao")
public interface MerchantApiRequestDao{
	
	/**
	 * 添加商户API请求.
	 * @param merchantApiRequest
	 * @return
	 */
	public Integer insert(MerchantApiRequestDo merchantApiRequest);
	
	/**
	 * 获取商户API请求数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantApiRequestDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取商户API请求数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantApiRequestDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据TrxId修改商户API请求.
	 * @param merchantApiRequest
	 * @return
	 */
	public Integer updateByTrxId(MerchantApiRequestDo merchantApiRequest);

	/**
	 * 根据TrxId删除商户API请求.
	 * @param trxId
	 * @return
	 */
	public Integer deleteByTrxId(String trxId);

	/**
	 * 根据TrxId获取商户API请求.
	 * @param trxId
	 * @return
	 */
	public MerchantApiRequestDo findByTrxId(String trxId);
 	
	 
}
