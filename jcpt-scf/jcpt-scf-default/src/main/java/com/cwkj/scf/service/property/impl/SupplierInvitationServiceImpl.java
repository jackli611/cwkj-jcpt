package com.cwkj.scf.service.property.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.SupplierInvitationComponent;
import com.cwkj.scf.model.property.SupplierInvitationDo;
import com.cwkj.scf.service.property.SupplierInvitationService;

/**
 * 供应商邀请注册.
 * @author ljc
 * @version  v1.0
 */
@Service("supplierInvitationService")
public class SupplierInvitationServiceImpl  implements SupplierInvitationService{
	
	private SupplierInvitationComponent supplierInvitationComponent;
	
	@Autowired
	public void setSupplierInvitationComponent(SupplierInvitationComponent supplierInvitationComponent)
	{
		this.supplierInvitationComponent=supplierInvitationComponent;
	}
	
	
	@Override
	public Integer insertSupplierInvitation(SupplierInvitationDo supplierInvitation)
	{
		Date now=new Date();
		supplierInvitation.setCreateTime(now);
		supplierInvitation.setUpdateTime(now);
		return supplierInvitationComponent.insertSupplierInvitation(supplierInvitation);
	}
	
	@Override
	public List<SupplierInvitationDo> querySupplierInvitationList(Map<String,Object> selectItem)
	{
		return supplierInvitationComponent.querySupplierInvitationList(selectItem);
	}

	@Override
	public PageDo<SupplierInvitationDo> querySupplierInvitationListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return supplierInvitationComponent.querySupplierInvitationListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateSupplierInvitationById(SupplierInvitationDo supplierInvitation)
	{
		Date now=new Date();
		supplierInvitation.setUpdateTime(now);
		return supplierInvitationComponent.updateSupplierInvitationById(supplierInvitation);
	}

	@Override
	public Integer deleteSupplierInvitationById(Integer id)
	{
		return supplierInvitationComponent.deleteSupplierInvitationById(id);
	}

	@Override
	public SupplierInvitationDo findSupplierInvitationById(Integer id)
	{
		return supplierInvitationComponent.findSupplierInvitationById(id);
	}


	@Override
	public SupplierInvitationDo findByInviteCode(String inviteCode) {
		return supplierInvitationComponent.findByInviteCode(inviteCode);
	}

	@Override
	public SupplierInvitationDo findByPropertyId(String propertyId) {
		return supplierInvitationComponent.findByPropertyId(propertyId);
	}
}
