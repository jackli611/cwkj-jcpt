package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.PayInfoDo;

/**
 * 支付信息.
 * @author ljc
 * @version  v1.0
 */
@Repository("payInfoDao")
public interface PayInfoDao{
	
	/**
	 * 添加支付信息.
	 * @param payInfo
	 * @return
	 */
	public Integer insert(PayInfoDo payInfo);
	
	/**
	 * 获取支付信息数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PayInfoDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取支付信息数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<PayInfoDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改支付信息.
	 * @param payInfo
	 * @return
	 */
	public Integer updateById(PayInfoDo payInfo);

	/**
	 * 根据Id删除支付信息.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取支付信息.
	 * @param id
	 * @return
	 */
	public PayInfoDo findById(String id);
 	
	 
}
