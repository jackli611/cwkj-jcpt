package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.PropertyFinancesComponent;
import com.cwkj.scf.dao.property.PropertyFinancesDao;
import com.cwkj.scf.model.property.PropertyFinancesDo;

/**
 * 物业的资金机构.
 * @author ljc
 * @version  v1.0
 */
@Component("propertyFinancesComponent")
public class PropertyFinancesComponentImpl  implements PropertyFinancesComponent{
	
	private PropertyFinancesDao propertyFinancesDao;
	
	@Autowired
	public void setPropertyFinancesDao(PropertyFinancesDao propertyFinancesDao)
	{
		this.propertyFinancesDao=propertyFinancesDao;
	}
	
	
	@Override
	public Integer insertPropertyFinances(PropertyFinancesDo propertyFinances)
	{
		return propertyFinancesDao.insert(propertyFinances);
	}
	
	@Override
	public List<PropertyFinancesDo> queryPropertyFinancesList(Map<String,Object> selectItem)
	{
		return propertyFinancesDao.queryList(selectItem);
	}

	@Override
	public PageDo<PropertyFinancesDo> queryPropertyFinancesListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PropertyFinancesDo> pageBean=new PageDo<PropertyFinancesDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(propertyFinancesDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updatePropertyFinancesById(PropertyFinancesDo propertyFinances)
	{
		return propertyFinancesDao.updateById(propertyFinances);
	}

	@Override
	public Integer deletePropertyFinancesById(Integer id)
	{
		return propertyFinancesDao.deleteById(id);
	}

	@Override
	public PropertyFinancesDo findPropertyFinancesById(Integer id)
	{
		return propertyFinancesDao.findById(id);
	}


	@Override
	public List<PropertyFinancesDo> queryListByPropertyId(String propertyId) {
		return propertyFinancesDao.queryListByPropertyId(propertyId);
	}


	@Override
	public Integer updatePropertyFinanceStatus(Integer propertyFinanceId, Integer status) {
		return propertyFinancesDao.updatePropertyFinanceStatus(propertyFinanceId, status);
	}


	/**
	 * 更新已使用额度
	 * @param tmpPfDo
	 */
	public Integer updateUsedAmt(PropertyFinancesDo tmpPfDo) {
		return propertyFinancesDao.updateUsedAmt(tmpPfDo);
		
	}	
}
