package com.cwkj.scf.component.products;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.PropertyorgProduct;

/**
 * 商品.
 * @author ljc
 * @version  v1.0
 */
public interface ProductComponent {
	
	/**
	 * 添加商品.
	 * @param product
	 * @return
	 */
	public Integer insertProduct(ProductDo product);
	
	/**
	 * 获取商品数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ProductDo> queryProductList(Map<String,Object> selectItem);

	/**
	 * 获取商品数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<ProductDo> queryProductListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem);

	/**
	 * 根据Id修改商品.
	 * @param product
	 * @return
	 */
	public Integer updateProductById(ProductDo product);

	/**
	 * 根据Id删除商品.
	 * @param id
	 * @return
	 */
	public Integer deleteProductById(String productCode);

	/**
	 * 根据Id获取商品.
	 * @param id
	 * @return
	 */
	public ProductDo findProductById(String productCode);	
	
	
	
	/**
	 * 	查询分配的产品
	 * @param pageIndex
	 * @param pageSize
	 * @param selectItem
	 * @return
	 */
	public PageDo<PropertyorgProduct> queryCompanyProductListPage(Long pageIndex, Integer pageSize,
			Map<String, Object> selectItem);

	/**
	 * 根据id 查询分配的产品
	 * @param assignId
	 * @return
	 */
	public PropertyorgProduct findAssignProductById(Integer assignId);

	/**
	 *  保存分配的产品
	 * @param assignProduct
	 */
	public void insertAssignProduct(PropertyorgProduct assignProduct);	
	
	/*
	 * 根据物业机构ID获取产品
	 * @param propertyOrgId
	 * @return
	 * @author ljc
	 */
	List<PropertyorgProduct> queryListByPropertyOrgId(String propertyOrgId);

	/**
	 * 修改核心企业分配的产品
	 * @param assignProduct
	 */
	public void updateAssignProductById(PropertyorgProduct assignProduct);
}
