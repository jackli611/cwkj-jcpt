package com.cwkj.scf.component.products.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.cwkj.scf.model.finances.FinanceOrgDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.products.PersonalLoanProductComponent;
import com.cwkj.scf.dao.products.PersonalLoanProductDao;
import com.cwkj.scf.model.products.PersonalLoanProductDo;

/**
 * 资金机构个贷产品.
 * @author ljc
 * @version  v1.0
 */
@Component("personalLoanProductComponent")
public class PersonalLoanProductComponentImpl  implements PersonalLoanProductComponent{
	
	private PersonalLoanProductDao personalLoanProductDao;
	
	@Autowired
	public void setPersonalLoanProductDao(PersonalLoanProductDao personalLoanProductDao)
	{
		this.personalLoanProductDao=personalLoanProductDao;
	}
	
	
	@Override
	public Integer insertPersonalLoanProduct(PersonalLoanProductDo personalLoanProduct)
	{
		return personalLoanProductDao.insert(personalLoanProduct);
	}
	
	@Override
	public List<PersonalLoanProductDo> queryPersonalLoanProductList(Map<String,Object> selectItem)
	{
		return personalLoanProductDao.queryList(selectItem);
	}

	@Override
	public PageDo<PersonalLoanProductDo> queryPersonalLoanProductListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PersonalLoanProductDo> pageBean=new PageDo<PersonalLoanProductDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(personalLoanProductDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updatePersonalLoanProductById(PersonalLoanProductDo personalLoanProduct)
	{
		return personalLoanProductDao.updateById(personalLoanProduct);
	}

	@Override
	public Integer deletePersonalLoanProductById(Long id)
	{
		return personalLoanProductDao.deleteById(id);
	}

	@Override
	public PersonalLoanProductDo findPersonalLoanProductById(Long id)
	{
		return personalLoanProductDao.findById(id);
	}

	@Override
	public List<PersonalLoanProductDo> queryByFinanceId(String financeId) {
		return personalLoanProductDao.queryByFinanceId(financeId);
	}

	@Override
	public List<PersonalLoanProductDo> queryByFinanceIdAndProductType(String financeId, Integer productType) {
		return personalLoanProductDao.queryByFinanceIdAndProductType(financeId, productType);
	}

	@Override
	public List<FinanceOrgDo> personalLoanFinanceOrgList() {
		return personalLoanProductDao.personalLoanFinanceOrgList();
	}
}
