package com.cwkj.scf.component.dashboard;

import java.util.List;
import java.util.Map;

import com.cwkj.scf.model.dashboard.DashboardInfoBoxDo;

/**
 * @author ljc
 *
 */
public interface PropertyDashboardComponent {
	
	/**
	 * InfoBox看板
	 * @param selectItem
	 * @return
	 */
	public List<DashboardInfoBoxDo> InfoBoxDashboard(Map<String, Object> selectItem);

}
