package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.CompanyDo;

/**
 * 企业.
 * @author ljc
 * @version  v1.0
 */
@Repository("companyDao")
public interface CompanyDao{
	
	/**
	 * 添加企业.
	 * @param company
	 * @return
	 */
	public Integer insert(CompanyDo company);
	
	/**
	 * 获取企业数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<CompanyDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取企业数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<CompanyDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改企业.
	 * @param company
	 * @return
	 */
	public Integer updateById(CompanyDo company);

	/**
	 * 根据Id删除企业.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取企业.
	 * @param id
	 * @return
	 */
	public CompanyDo findById(String id);
	
	/**
	 * 修改营业执照信息
	 * @param id 物业ID 
	 * @param businessLicense 营业执照 
	 * @return
	 */
	public Integer updateBusinessLicense(@Param("id")String id,@Param("businessLicense")String businessLicense);
	 
}
