package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.SupplierAuditComponent;
import com.cwkj.scf.dao.property.SupplierAuditDao;
import com.cwkj.scf.model.property.SupplierAuditDo;

/**
 * 供应商审核.
 * @author ljc
 * @version  v1.0
 */
@Component("supplierAuditComponent")
public class SupplierAuditComponentImpl  implements SupplierAuditComponent{
	
	private SupplierAuditDao supplierAuditDao;
	
	@Autowired
	public void setSupplierAuditDao(SupplierAuditDao supplierAuditDao)
	{
		this.supplierAuditDao=supplierAuditDao;
	}
	
	
	@Override
	public Integer insertSupplierAudit(SupplierAuditDo supplierAudit)
	{
		return supplierAuditDao.insert(supplierAudit);
	}
	
	@Override
	public List<SupplierAuditDo> querySupplierAuditList(Map<String,Object> selectItem)
	{
		return supplierAuditDao.queryList(selectItem);
	}

	@Override
	public PageDo<SupplierAuditDo> querySupplierAuditListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<SupplierAuditDo> pageBean=new PageDo<SupplierAuditDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(supplierAuditDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateSupplierAuditById(SupplierAuditDo supplierAudit)
	{
		return supplierAuditDao.updateById(supplierAudit);
	}

	@Override
	public Integer deleteSupplierAuditById(Integer id)
	{
		return supplierAuditDao.deleteById(id);
	}

	@Override
	public SupplierAuditDo findSupplierAuditById(Integer id)
	{
		return supplierAuditDao.findById(id);
	}	
}
