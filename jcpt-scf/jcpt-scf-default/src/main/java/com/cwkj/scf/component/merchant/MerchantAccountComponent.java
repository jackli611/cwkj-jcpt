package com.cwkj.scf.component.merchant;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.merchant.MerchantAccountDo;

/**
 * 商户账号.
 * @author ljc
 * @version  v1.0
 */
public interface MerchantAccountComponent {
	
	/**
	 * 添加商户账号.
	 * @param merchantAccount
	 * @return
	 */
	public Integer insertMerchantAccount(MerchantAccountDo merchantAccount);
	
	/**
	 * 获取商户账号数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<MerchantAccountDo> queryMerchantAccountList(Map<String, Object> selectItem);

	/**
	 * 获取商户账号数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<MerchantAccountDo> queryMerchantAccountListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改商户账号.
	 * @param merchantAccount
	 * @return
	 */
	public Integer updateMerchantAccountById(MerchantAccountDo merchantAccount);

	/**
	 * 根据Id删除商户账号.
	 * @param id
	 * @return
	 */
	public Integer deleteMerchantAccountById(String id);

	/**
	 * 根据Id获取商户账号.
	 * @param id
	 * @return
	 */
	public MerchantAccountDo findMerchantAccountById(String id);	
}
