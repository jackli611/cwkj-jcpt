package com.cwkj.scf.dao.agreement;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.agreement.AgreementPersonRegisterDo;

/**
 * 签章个人用户注册.
 * @author ljc
 * @version  v1.0
 */
@Repository("agreementPersonRegisterDao")
public interface AgreementPersonRegisterDao{
	
	/**
	 * 添加签章个人用户注册.
	 * @param agreementPersonRegister
	 * @return
	 */
	public Integer insert(AgreementPersonRegisterDo agreementPersonRegister);
	
	/**
	 * 获取签章个人用户注册数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementPersonRegisterDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取签章个人用户注册数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementPersonRegisterDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改签章个人用户注册.
	 * @param agreementPersonRegister
	 * @return
	 */
	public Integer updateById(AgreementPersonRegisterDo agreementPersonRegister);

	/**
	 * 根据Id删除签章个人用户注册.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取签章个人用户注册.
	 * @param id
	 * @return
	 */
	public AgreementPersonRegisterDo findById(String id);

	/**
	 * 根据身份证获取注册用户
	 * @param idcardNo 身份证号
	 * @return
	 */
	public List<AgreementPersonRegisterDo> findByIdcardNo(String idcardNo);

	/**
	 * 根据身份信息获取注册用户
	 * @param idcardNo 身份证号
	 * @param realName 姓名
	 * @param mobile 手机号
	 * @return
	 */
	public AgreementPersonRegisterDo findByPerson(@Param("idcardNo") String idcardNo,@Param("realName")String realName,@Param("mobile") String mobile);
	 
}
