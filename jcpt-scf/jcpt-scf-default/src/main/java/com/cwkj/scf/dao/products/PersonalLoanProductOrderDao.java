package com.cwkj.scf.dao.products;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.products.PersonalLoanProductOrderDo;

/**
 * 个贷产品订单.
 * @author ljc
 * @version  v1.0
 */
@Repository("personalLoanProductOrderDao")
public interface PersonalLoanProductOrderDao{
	
	/**
	 * 添加个贷产品订单.
	 * @param personalLoanProductOrder
	 * @return
	 */
	public Integer insert(PersonalLoanProductOrderDo personalLoanProductOrder);
	
	/**
	 * 获取个贷产品订单数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PersonalLoanProductOrderDo> queryList(Map<String, Object> selectItem);
	
	/**
	 * 获取个贷产品订单数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<PersonalLoanProductOrderDo> queryListPage(Map<String, Object> selectItem);
	

	/**
	 * 根据Id修改个贷产品订单.
	 * @param personalLoanProductOrder
	 * @return
	 */
	public Integer updateById(PersonalLoanProductOrderDo personalLoanProductOrder);

	/**
	 * 根据Id删除个贷产品订单.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Long id);

	/**
	 * 根据Id获取个贷产品订单.
	 * @param id
	 * @return
	 */
	public PersonalLoanProductOrderDo findById(Long id);


	/**
	 * 根据借款ID获取个贷产品
	 * @param loanId
	 * @return
	 */
	public List<PersonalLoanProductOrderDo> queryByLoanId(String loanId);
	 
}
