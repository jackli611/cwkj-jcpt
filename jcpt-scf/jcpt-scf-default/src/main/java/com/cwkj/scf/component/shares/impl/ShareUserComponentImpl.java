package com.cwkj.scf.component.shares.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.shares.ShareUserComponent;
import com.cwkj.scf.dao.shares.ShareUserDao;
import com.cwkj.scf.model.shares.ShareUserDo;

/**
 * 分享用户.
 * @author ljc
 * @version  v1.0
 */
@Component("shareUserComponent")
public class ShareUserComponentImpl  implements ShareUserComponent{
	
	private ShareUserDao shareUserDao;
	
	@Autowired
	public void setShareUserDao(ShareUserDao shareUserDao)
	{
		this.shareUserDao=shareUserDao;
	}
	
	
	@Override
	public Integer insertShareUser(ShareUserDo shareUser)
	{
		return shareUserDao.insert(shareUser);
	}
	
	@Override
	public List<ShareUserDo> queryShareUserList(Map<String,Object> selectItem)
	{
		return shareUserDao.queryList(selectItem);
	}

	@Override
	public PageDo<ShareUserDo> queryShareUserListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<ShareUserDo> pageBean=new PageDo<ShareUserDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(shareUserDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateShareUserById(ShareUserDo shareUser)
	{
		return shareUserDao.updateById(shareUser);
	}

	@Override
	public Integer deleteShareUserById(Long id)
	{
		return shareUserDao.deleteById(id);
	}

	@Override
	public ShareUserDo findShareUserById(Long id)
	{
		return shareUserDao.findById(id);
	}

	@Override
	public ShareUserDo findShareUserByUserId(Integer userId) {
		return shareUserDao.findShareUserByUserId(userId);
	}
}
