package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.SupplierComponent;
import com.cwkj.scf.dao.property.SupplierDao;
import com.cwkj.scf.model.property.SupplierDo;
import com.cwkj.scf.model.property.SupplierInvitationDo;

/**
 * 供应商.
 * @author ljc
 * @version  v1.0
 */
@Component("supplierComponent")
public class SupplierComponentImpl  implements SupplierComponent{
	
	private SupplierDao supplierDao;
	
	@Autowired
	public void setSupplierDao(SupplierDao supplierDao)
	{
		this.supplierDao=supplierDao;
	}
	
	
	@Override
	public Integer insertSupplier(SupplierDo supplier)
	{
		return supplierDao.insert(supplier);
	}
	
	@Override
	public List<SupplierDo> querySupplierList(Map<String,Object> selectItem)
	{
		return supplierDao.queryList(selectItem);
	}

	@Override
	public PageDo<SupplierDo> querySupplierListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<SupplierDo> pageBean=new PageDo<SupplierDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(supplierDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateSupplierById(SupplierDo supplier)
	{
		return supplierDao.updateById(supplier);
	}
	
	@Override
	/**
	 * 根据Id修改供应商.
	 * @param supplier
	 * @return
	 */
	public Integer updateByPrimaryKeySelective(SupplierDo supplier)
	{
		return supplierDao.updateByPrimaryKeySelective(supplier);
	}

	@Override
	public Integer deleteSupplierById(String id)
	{
		return supplierDao.deleteById(id);
	}

	@Override
	public SupplierDo findSupplierById(String id)
	{
		return supplierDao.findById(id);
	}


	@Override
	public Integer updateStatus(String id, Integer status) {
		return supplierDao.updateStatus(id, status);
	}

	@Override
	public  Integer updatePlatformStatus(String id,Integer platformStatus){
		return supplierDao.updatePlatformStatus(id,platformStatus);
	}

	@Override
	public Integer saveBusinessLicense(String id, String businessLicense) {
		return supplierDao.saveBusinessLicense(id, businessLicense);
	}


	@Override
	public Integer addSysUserId(String id, Integer userId) {
		return supplierDao.addSysUserId(id, userId);
	}
 
}
