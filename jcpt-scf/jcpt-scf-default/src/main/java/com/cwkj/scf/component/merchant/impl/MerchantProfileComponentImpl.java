package com.cwkj.scf.component.merchant.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.merchant.MerchantProfileComponent;
import com.cwkj.scf.dao.merchant.MerchantProfileDao;
import com.cwkj.scf.model.merchant.MerchantProfileDo;

/**
 * 商户信息.
 * @author ljc
 * @version  v1.0
 */
@Component("merchantProfileComponent")
public class MerchantProfileComponentImpl  implements MerchantProfileComponent{
	
	private MerchantProfileDao merchantProfileDao;
	
	@Autowired
	public void setMerchantProfileDao(MerchantProfileDao merchantProfileDao)
	{
		this.merchantProfileDao=merchantProfileDao;
	}
	
	
	@Override
	public Integer insertMerchantProfile(MerchantProfileDo merchantProfile)
	{
		return merchantProfileDao.insert(merchantProfile);
	}
	
	@Override
	public List<MerchantProfileDo> queryMerchantProfileList(Map<String,Object> selectItem)
	{
		return merchantProfileDao.queryList(selectItem);
	}

	@Override
	public PageDo<MerchantProfileDo> queryMerchantProfileListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<MerchantProfileDo> pageBean=new PageDo<MerchantProfileDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(merchantProfileDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateMerchantProfileById(MerchantProfileDo merchantProfile)
	{
		return merchantProfileDao.updateById(merchantProfile);
	}

	@Override
	public Integer deleteMerchantProfileById(Integer id)
	{
		return merchantProfileDao.deleteById(id);
	}

	@Override
	public MerchantProfileDo findMerchantProfileById(Integer id)
	{
		return merchantProfileDao.findById(id);
	}

	@Override
	public MerchantProfileDo enableMerchantProfileByMerId(String merId, String secretKey) {
		return merchantProfileDao.enableMerchantProfileByMerId(merId,secretKey);
	}
}
