package com.cwkj.scf.service.finances.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.jcptsystem.service.system.SysSeqNumberService;
import com.cwkj.scf.component.finances.FinanceOrgComponent;
import com.cwkj.scf.model.finances.FinanceOrgDo;
import com.cwkj.scf.service.finances.FinanceOrgService;

/**
 * 金融机构.
 * @author ljc
 * @version  v1.0
 */
@Component("financeOrgService")
public class FinanceOrgServiceImpl  implements FinanceOrgService{
	
	private FinanceOrgComponent financeOrgComponent;
	@Autowired
	private SysSeqNumberService sysSeqNumberService;
	
	@Autowired
	public void setFinanceOrgComponent(FinanceOrgComponent financeOrgComponent)
	{
		this.financeOrgComponent=financeOrgComponent;
	}
	
	
	@Override
	public Integer insertFinanceOrg(FinanceOrgDo financeOrg)
	{
		Date now =new Date();
		financeOrg.setCreateTime(now);
		financeOrg.setUpdateTime(now);
		financeOrg.setId(sysSeqNumberService.daySeqnumberString("FORG"));
		return financeOrgComponent.insertFinanceOrg(financeOrg);
	}
	
	@Override
	public List<FinanceOrgDo> queryFinanceOrgList(Map<String,Object> selectItem)
	{
		return financeOrgComponent.queryFinanceOrgList(selectItem);
	}

	@Override
	public PageDo<FinanceOrgDo> queryFinanceOrgListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return financeOrgComponent.queryFinanceOrgListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateFinanceOrgById(FinanceOrgDo financeOrg)
	{
		Date now =new Date();
		financeOrg.setUpdateTime(now);
		return financeOrgComponent.updateFinanceOrgById(financeOrg);
	}

	@Override
	public Integer deleteFinanceOrgById(String id)
	{
		return financeOrgComponent.deleteFinanceOrgById(id);
	}

	@Override
	public FinanceOrgDo findFinanceOrgById(String id)
	{
		return financeOrgComponent.findFinanceOrgById(id);
	}


	@Override
	public Integer updateStatus(String id, Integer status) {
		return financeOrgComponent.updateStatus(id, status);
	}


	@Override
	public Integer addSysUser(FinanceOrgDo financeOrg) {
		return financeOrgComponent.addSysUser(financeOrg);
	}


	@Override
	public Integer updateFinanceNameInfo(FinanceOrgDo financeOrg) {
		return financeOrgComponent.updateFinanceNameInfo(financeOrg);
	}	
}
