package com.cwkj.scf.component.etc.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.etc.EtcProfileComponent;
import com.cwkj.scf.dao.etc.EtcProfileDao;
import com.cwkj.scf.model.etc.EtcProfileDo;

/**
 * ETC基本信息.
 * @author ljc
 * @version  v1.0
 */
@Component("etcProfileComponent")
public class EtcProfileComponentImpl  implements EtcProfileComponent{
	
	private EtcProfileDao etcProfileDao;
	
	@Autowired
	public void setEtcProfileDao(EtcProfileDao etcProfileDao)
	{
		this.etcProfileDao=etcProfileDao;
	}
	
	
	@Override
	public Integer insertEtcProfile(EtcProfileDo etcProfile)
	{
		return etcProfileDao.insert(etcProfile);
	}
	
	@Override
	public List<EtcProfileDo> queryEtcProfileList(Map<String,Object> selectItem)
	{
		return etcProfileDao.queryList(selectItem);
	}

	@Override
	public PageDo<EtcProfileDo> queryEtcProfileListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<EtcProfileDo> pageBean=new PageDo<EtcProfileDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(etcProfileDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateEtcProfileById(EtcProfileDo etcProfile)
	{
		return etcProfileDao.updateById(etcProfile);
	}

	@Override
	public Integer deleteEtcProfileById(Long id)
	{
		return etcProfileDao.deleteById(id);
	}

	@Override
	public EtcProfileDo findEtcProfileById(Long id)
	{
		return etcProfileDao.findById(id);
	}

	@Override
	public Integer updateCardTypeById(EtcProfileDo etcProfile) {
		return etcProfileDao.updateCardTypeById(etcProfile);
	}
}
