package com.cwkj.scf.service.property.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.SupplierAuditComponent;
import com.cwkj.scf.model.property.SupplierAuditDo;
import com.cwkj.scf.service.property.SupplierAuditService;

/**
 * 供应商审核.
 * @author ljc
 * @version  v1.0
 */
@Service("supplierAuditService")
public class SupplierAuditServiceImpl  implements SupplierAuditService{
	
	private SupplierAuditComponent supplierAuditComponent;
	
	@Autowired
	public void setSupplierAuditComponent(SupplierAuditComponent supplierAuditComponent)
	{
		this.supplierAuditComponent=supplierAuditComponent;
	}
	
	
	@Override
	public Integer insertSupplierAudit(SupplierAuditDo supplierAudit)
	{
		return supplierAuditComponent.insertSupplierAudit(supplierAudit);
	}
	
	@Override
	public List<SupplierAuditDo> querySupplierAuditList(Map<String,Object> selectItem)
	{
		return supplierAuditComponent.querySupplierAuditList(selectItem);
	}

	@Override
	public PageDo<SupplierAuditDo> querySupplierAuditListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return supplierAuditComponent.querySupplierAuditListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateSupplierAuditById(SupplierAuditDo supplierAudit)
	{
		return supplierAuditComponent.updateSupplierAuditById(supplierAudit);
	}

	@Override
	public Integer deleteSupplierAuditById(Integer id)
	{
		return supplierAuditComponent.deleteSupplierAuditById(id);
	}

	@Override
	public SupplierAuditDo findSupplierAuditById(Integer id)
	{
		return supplierAuditComponent.findSupplierAuditById(id);
	}	
}
