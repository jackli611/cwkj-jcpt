package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.PropertyFinancesDo;

/**
 * 物业的资金机构.
 * @author ljc
 * @version  v1.0
 */
@Repository("propertyFinancesDao")
public interface PropertyFinancesDao{
	
	/**
	 * 添加物业的资金机构.
	 * @param propertyFinances
	 * @return
	 */
	public Integer insert(PropertyFinancesDo propertyFinances);
	
	/**
	 * 获取物业的资金机构数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyFinancesDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取物业的资金机构数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyFinancesDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改物业的资金机构.
	 * @param propertyFinances
	 * @return
	 */
	public Integer updateById(PropertyFinancesDo propertyFinances);

	/**
	 * 根据Id删除物业的资金机构.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Integer id);

	/**
	 * 根据Id获取物业的资金机构.
	 * @param id
	 * @return
	 */
	public PropertyFinancesDo findById(Integer id);
 	
	/**
	 * 物业的资金方列表
	 * @param propertyId 物业ID
	 * @return
	 */
	public List<PropertyFinancesDo> queryListByPropertyId(String propertyId);
	
	
	/**
	 * 修改资金方合作状态
	 * @param propertyFinanceId
	 * @param status
	 * @return
	 */
	public Integer updatePropertyFinanceStatus(@Param("id")Integer id,@Param("status")Integer status);

	/**
	 * 更新已使用额度
	 * @param tmpPfDo
	 */
	public Integer updateUsedAmt(PropertyFinancesDo tmpPfDo);
}
