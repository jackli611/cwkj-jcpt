package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.SupplierDo;

/**
 * 供应商.
 * @author ljc
 * @version  v1.0
 */
@Repository("supplierDao")
public interface SupplierDao{
	
	/**
	 * 添加供应商.
	 * @param supplier
	 * @return
	 */
	public Integer insert(SupplierDo supplier);
	
	/**
	 * 获取供应商数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取供应商数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改供应商.
	 * @param supplier
	 * @return
	 */
	public Integer updateById(SupplierDo supplier);
	/**
	 * 根据Id修改供应商.
	 * @param supplier
	 * @return
	 */
	public Integer updateByPrimaryKeySelective(SupplierDo supplier);

	/**
	 * 根据Id删除供应商.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取供应商.
	 * @param id
	 * @return
	 */
	public SupplierDo findById(String id);
 	
	
	/**
	 * 修改状态
	 * @param id 供应商ID
	 * @param status 合作状态
	 * @return
	 */
	public Integer updateStatus(@Param("id")String id,@Param("status") Integer status);

	/**
	 * 修改平台审批状态
	 * @param id 供应商ID
	 * @param platformStatus 平台审批状态
	 * @return
	 */
	public  Integer updatePlatformStatus(@Param("id")String id,@Param("platformStatus")Integer platformStatus);

	/**
	 * 保存营业执照
	 * @param id 供应商ID
	 * @param businessLicense 营业执照
	 * @return
	 */
	public Integer saveBusinessLicense(@Param("id")String id,@Param("businessLicense")String businessLicense);
 
	/**
	 * 保存系统账号
	 * @param id 供应商ID
	 * @param userId 用户ID
	 * @return
	 */
	public Integer addSysUserId(@Param("id")String id,@Param("userId")Integer userId);
	 
	
}
