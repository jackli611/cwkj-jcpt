package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cwkj.scf.model.products.ProductDo;
import com.cwkj.scf.model.products.PropertyorgProduct;
import com.cwkj.scf.model.property.PropertyOrgDo;

/**
 * 物业机构.
 * @author ljc
 * @version  v1.0
 */
@Repository("propertyOrgDao")
public interface PropertyOrgDao{
	
	/**
	 * 添加物业机构.
	 * @param propertyOrg
	 * @return
	 */
	public Integer insert(PropertyOrgDo propertyOrg);
	
	/**
	 * 获取物业机构数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyOrgDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取物业机构数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<PropertyOrgDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改物业机构.
	 * @param propertyOrg
	 * @return
	 */
	public Integer updateById(PropertyOrgDo propertyOrg);

	/**
	 * 根据Id删除物业机构.
	 * @param id
	 * @return
	 */
	public Integer deleteById(String id);

	/**
	 * 根据Id获取物业机构.
	 * @param id
	 * @return
	 */
	public PropertyOrgDo findById(String id);
	
	/**
	 * 修改物业基本信息
	 * @param propertyOrg
	 * @return
	 */
	public Integer updatePropertyNameInfo(PropertyOrgDo propertyOrg);
 	
	/**
	 * 修改营业执照信息
	 * @param id 物业ID 
	 * @param businessLicense 营业执照 
	 * @return
	 */
	public Integer updateBusinessLicense(@Param("id")String id,@Param("businessLicense")String businessLicense);
	
	/**
	 * 修改物业LOGO
	 * @param id 物业ID 
	 * @param logo logo地址
	 * @return
	 */
	public Integer updateLogo(@Param("id")String id,@Param("logo")String logo);
	
	/**
	 * 修改状态
	 * @param id 物业ID 
	 * @param status 状态值
	 * @return
	 */
	public Integer updateStatus(@Param("id")String id,@Param("status")Integer status);
	
	/**
	 * 添加系统账号信息
	 * @param propertyOrg
	 * @return
	 */
	public Integer addSysUser(PropertyOrgDo propertyOrg);
	
	
	
	/**
	 * 查询产品列表
	 * @param propertyOrgId 物业机构ID
	 * @return
	 */
	public List<ProductDo> queryProductList(@Param("propertyOrgId")String propertyOrgId);
	
	/**
	 * 查询产品
	 * @param propertyOrgId 物业机构ID
	 * @param productId 产品ID
	 * @Param("propertyOrgId")String propertyOrgId,@Param("productCode")String productCode "level" 供应商等级
	 * @return
	 */
	public List<PropertyorgProduct> findProduct(Map<String,Object> selectItem);
	 
}
