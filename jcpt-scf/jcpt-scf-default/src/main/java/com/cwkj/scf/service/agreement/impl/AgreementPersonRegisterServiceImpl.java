package com.cwkj.scf.service.agreement.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.util.AssertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.agreement.AgreementPersonRegisterComponent;
import com.cwkj.scf.model.agreement.AgreementPersonRegisterDo;
import com.cwkj.scf.service.agreement.AgreementPersonRegisterService;

/**
 * 签章个人用户注册.
 * @author ljc
 * @version  v1.0
 */
@Service("agreementPersonRegisterService")
public class AgreementPersonRegisterServiceImpl  implements AgreementPersonRegisterService{
	
	private AgreementPersonRegisterComponent agreementPersonRegisterComponent;
	
	@Autowired
	public void setAgreementPersonRegisterComponent(AgreementPersonRegisterComponent agreementPersonRegisterComponent)
	{
		this.agreementPersonRegisterComponent=agreementPersonRegisterComponent;
	}
	
	
	@Override
	public Integer insertAgreementPersonRegister(AgreementPersonRegisterDo agreementPersonRegister)
	{
		AssertUtils.isNotBlank(agreementPersonRegister.getId(),"签章注册流水ID不能为空");
		AssertUtils.isNotBlank(agreementPersonRegister.getIdcardNo(),"签章用户身份证号码不能为空");
		AssertUtils.isNotBlank(agreementPersonRegister.getMobile(),"签章用户手机号不能为空");
		AssertUtils.isNotBlank(agreementPersonRegister.getRealName(),"签章用户姓名不能为空");
		Date now=new Date();
		agreementPersonRegister.setCreateTime(now);
		agreementPersonRegister.setUpdateTime(now);
		return agreementPersonRegisterComponent.insertAgreementPersonRegister(agreementPersonRegister);
	}
	
	@Override
	public List<AgreementPersonRegisterDo> queryAgreementPersonRegisterList(Map<String,Object> selectItem)
	{
		return agreementPersonRegisterComponent.queryAgreementPersonRegisterList(selectItem);
	}

	@Override
	public PageDo<AgreementPersonRegisterDo> queryAgreementPersonRegisterListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 return agreementPersonRegisterComponent.queryAgreementPersonRegisterListPage(pageIndex,pageSize,selectItem);
	}
	

	@Override
	public Integer updateAgreementPersonRegisterById(AgreementPersonRegisterDo agreementPersonRegister)
	{
		return agreementPersonRegisterComponent.updateAgreementPersonRegisterById(agreementPersonRegister);
	}

	@Override
	public Integer deleteAgreementPersonRegisterById(String id)
	{
		return agreementPersonRegisterComponent.deleteAgreementPersonRegisterById(id);
	}

	@Override
	public AgreementPersonRegisterDo findAgreementPersonRegisterById(String id)
	{
		return agreementPersonRegisterComponent.findAgreementPersonRegisterById(id);
	}

	@Override
	public List<AgreementPersonRegisterDo> findByIdcardNo(String idcardNo) {

		return agreementPersonRegisterComponent.findByIdcardNo(idcardNo);
	}

	@Override
	public AgreementPersonRegisterDo findByPerson(String idcardNo, String realName, String mobile) {
		return agreementPersonRegisterComponent.findByPerson(idcardNo,realName,mobile);
	}
}
