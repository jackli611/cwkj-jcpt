package com.cwkj.scf.dao.products;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.products.ProductDo;

/**
 * 商品.
 * @author ljc
 * @version  v1.0
 */
@Repository("productDao")
public interface ProductDao{
	
	/**
	 * 添加商品.
	 * @param product
	 * @return
	 */
	public Integer insert(ProductDo product);
	
	/**
	 * 获取商品数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<ProductDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取商品数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<ProductDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据ProductCode修改产品.
	 * @param product
	 * @return
	 */
	public Integer updateByProductCode(ProductDo product);

	/**
	 * 根据ProductCode删除产品.
	 * @param productCode
	 * @return
	 */
	public Integer deleteByProductCode(String productCode);

	/**
	 * 根据ProductCode获取产品.
	 * @param productCode
	 * @return
	 */
	public ProductDo findByProductCode(String productCode);
 	
	 
}
