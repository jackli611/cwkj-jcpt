package com.cwkj.scf.component.shares.impl;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.shares.SharePersonalLoanOrderComponent;
import com.cwkj.scf.dao.shares.SharePersonalLoanOrderDao;
import com.cwkj.scf.model.shares.SharePersonalLoanOrder;
import com.cwkj.scf.model.shares.ShareUserDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 分享历史.
 * @author ljc
 * @version  v1.0
 */
@Component("sharePersonalLoanOrderComponent")
public class SharePersonalLoanOrderComponentImpl implements SharePersonalLoanOrderComponent {

    @Autowired
    private SharePersonalLoanOrderDao sharePersonalLoanOrderDao;

    @Override
    public PageDo<SharePersonalLoanOrder> querySharePersonalLoanOrderListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem) {
        if(selectItem==null)
        {
            selectItem=new HashMap<String,Object>();
        }
        PageDo<SharePersonalLoanOrder> pageBean=new PageDo<SharePersonalLoanOrder>(pageIndex, pageSize);
        selectItem.put("page", pageBean);
        pageBean.setPage(sharePersonalLoanOrderDao.querySharePersonalLoanOrderListPage(selectItem));
        return pageBean;
    }
}
