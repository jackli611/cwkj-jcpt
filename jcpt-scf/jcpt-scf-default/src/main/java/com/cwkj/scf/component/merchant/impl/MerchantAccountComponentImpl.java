package com.cwkj.scf.component.merchant.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.merchant.MerchantAccountComponent;
import com.cwkj.scf.dao.merchant.MerchantAccountDao;
import com.cwkj.scf.model.merchant.MerchantAccountDo;

/**
 * 商户账号.
 * @author ljc
 * @version  v1.0
 */
@Component("merchantAccountComponent")
public class MerchantAccountComponentImpl  implements MerchantAccountComponent{
	
	private MerchantAccountDao merchantAccountDao;
	
	@Autowired
	public void setMerchantAccountDao(MerchantAccountDao merchantAccountDao)
	{
		this.merchantAccountDao=merchantAccountDao;
	}
	
	
	@Override
	public Integer insertMerchantAccount(MerchantAccountDo merchantAccount)
	{
		return merchantAccountDao.insert(merchantAccount);
	}
	
	@Override
	public List<MerchantAccountDo> queryMerchantAccountList(Map<String,Object> selectItem)
	{
		return merchantAccountDao.queryList(selectItem);
	}

	@Override
	public PageDo<MerchantAccountDo> queryMerchantAccountListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<MerchantAccountDo> pageBean=new PageDo<MerchantAccountDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(merchantAccountDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updateMerchantAccountById(MerchantAccountDo merchantAccount)
	{
		return merchantAccountDao.updateById(merchantAccount);
	}

	@Override
	public Integer deleteMerchantAccountById(String id)
	{
		return merchantAccountDao.deleteById(id);
	}

	@Override
	public MerchantAccountDo findMerchantAccountById(String id)
	{
		return merchantAccountDao.findById(id);
	}	
}
