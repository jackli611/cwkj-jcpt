package com.cwkj.scf.dao.property;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.cwkj.scf.model.property.SupplierAuditDo;

/**
 * 供应商审核.
 * @author ljc
 * @version  v1.0
 */
@Repository("supplierAuditDao")
public interface SupplierAuditDao{
	
	/**
	 * 添加供应商审核.
	 * @param supplierAudit
	 * @return
	 */
	public Integer insert(SupplierAuditDo supplierAudit);
	
	/**
	 * 获取供应商审核数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierAuditDo> queryList(Map<String,Object> selectItem);
	
	/**
	 * 获取供应商审核数据分页列表.
	 * @param selectItem
	 * @return
	 */
	public List<SupplierAuditDo> queryListPage(Map<String,Object> selectItem);
	

	/**
	 * 根据Id修改供应商审核.
	 * @param supplierAudit
	 * @return
	 */
	public Integer updateById(SupplierAuditDo supplierAudit);

	/**
	 * 根据Id删除供应商审核.
	 * @param id
	 * @return
	 */
	public Integer deleteById(Integer id);

	/**
	 * 根据Id获取供应商审核.
	 * @param id
	 * @return
	 */
	public SupplierAuditDo findById(Integer id);
 	
	 
}
