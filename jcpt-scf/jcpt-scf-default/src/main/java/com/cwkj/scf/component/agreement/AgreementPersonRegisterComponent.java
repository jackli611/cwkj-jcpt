package com.cwkj.scf.component.agreement;

import java.util.List;
import java.util.Map;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.model.agreement.AgreementPersonRegisterDo;

/**
 * 签章个人用户注册.
 * @author ljc
 * @version  v1.0
 */
public interface AgreementPersonRegisterComponent {
	
	/**
	 * 添加签章个人用户注册.
	 * @param agreementPersonRegister
	 * @return
	 */
	public Integer insertAgreementPersonRegister(AgreementPersonRegisterDo agreementPersonRegister);
	
	/**
	 * 获取签章个人用户注册数据列表.
	 * @param selectItem
	 * @return
	 */
	public List<AgreementPersonRegisterDo> queryAgreementPersonRegisterList(Map<String, Object> selectItem);

	/**
	 * 获取签章个人用户注册数据集
	 * @param pageIndex
	 * 起始页
	 * @param pageSize
	 * 每页记录数
	 * @param selectItem
	 * 过滤条件
	 * @return
	 */
	public PageDo<AgreementPersonRegisterDo> queryAgreementPersonRegisterListPage(Long pageIndex, Integer pageSize, Map<String, Object> selectItem);

	/**
	 * 根据Id修改签章个人用户注册.
	 * @param agreementPersonRegister
	 * @return
	 */
	public Integer updateAgreementPersonRegisterById(AgreementPersonRegisterDo agreementPersonRegister);

	/**
	 * 根据Id删除签章个人用户注册.
	 * @param id
	 * @return
	 */
	public Integer deleteAgreementPersonRegisterById(String id);

	/**
	 * 根据Id获取签章个人用户注册.
	 * @param id
	 * @return
	 */
	public AgreementPersonRegisterDo findAgreementPersonRegisterById(String id);

	/**
	 * 根据身份证获取注册用户
	 * @param idcardNo 身份证号
	 * @return
	 */
	public List<AgreementPersonRegisterDo> findByIdcardNo(String idcardNo);

	/**
	 * 根据身份信息获取注册用户
	 * @param idcardNo 身份证号
	 * @param realName 姓名
	 * @param mobile 手机号
	 * @return
	 */
	public AgreementPersonRegisterDo findByPerson(String idcardNo,String realName,String mobile);
}
