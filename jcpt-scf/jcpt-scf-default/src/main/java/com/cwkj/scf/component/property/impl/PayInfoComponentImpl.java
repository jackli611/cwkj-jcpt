package com.cwkj.scf.component.property.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwkj.jcpt.common.page.PageDo;
import com.cwkj.scf.component.property.PayInfoComponent;
import com.cwkj.scf.dao.property.PayInfoDao;
import com.cwkj.scf.model.property.PayInfoDo;

/**
 * 支付信息.
 * @author ljc
 * @version  v1.0
 */
@Component("payInfoComponent")
public class PayInfoComponentImpl  implements PayInfoComponent{
	
	private PayInfoDao payInfoDao;
	
	@Autowired
	public void setPayInfoDao(PayInfoDao payInfoDao)
	{
		this.payInfoDao=payInfoDao;
	}
	
	
	@Override
	public Integer insertPayInfo(PayInfoDo payInfo)
	{
		return payInfoDao.insert(payInfo);
	}
	
	@Override
	public List<PayInfoDo> queryPayInfoList(Map<String,Object> selectItem)
	{
		return payInfoDao.queryList(selectItem);
	}

	@Override
	public PageDo<PayInfoDo> queryPayInfoListPage(Long pageIndex,Integer pageSize,Map<String,Object> selectItem)
	{
		 if(selectItem==null)
		 {
			 selectItem=new HashMap<String,Object>();
		 }
		 PageDo<PayInfoDo> pageBean=new PageDo<PayInfoDo>(pageIndex, pageSize);
		 selectItem.put("page", pageBean);
		 pageBean.setPage(payInfoDao.queryListPage(selectItem));
		return pageBean;
	}
	

	@Override
	public Integer updatePayInfoById(PayInfoDo payInfo)
	{
		return payInfoDao.updateById(payInfo);
	}

	@Override
	public Integer deletePayInfoById(String id)
	{
		return payInfoDao.deleteById(id);
	}

	@Override
	public PayInfoDo findPayInfoById(String id)
	{
		return payInfoDao.findById(id);
	}	
}
