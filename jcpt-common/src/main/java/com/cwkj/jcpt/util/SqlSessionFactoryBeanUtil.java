/**
 * Project Name:jcpt-common <br>
 * File Name:SqlSessionFactoryBeanUtil.java <br>
 * Package Name:com.hehenian.jcpt.util <br>
 * @author xiezbmf
 * Date:2016年11月16日下午5:48:14 <br>
 * Copyright (c) 2016, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcpt.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.NestedIOException;

/**
 * ClassName: SqlSessionFactoryBeanUtil <br>
 * Description: TODO
 * @author xiezbmf
 * Date:2016年11月16日下午5:48:14 <br>
 * @version
 * @since JDK 1.6
 */
public class SqlSessionFactoryBeanUtil extends SqlSessionFactoryBean {
	
	/** 系统权限等使用的数据库名称 */
	private String systemDBName;
	/** 默认数据库名称 */
	private String sp2pDBName;
	private static Logger logger=LoggerFactory.getLogger(SqlSessionFactoryBeanUtil.class);
	
	/**
	 * 系统权限等使用的数据库名称
	 * @param systemDBName
	 */
	public void setSystemDBName(String systemDBName)
	{
		this.systemDBName=systemDBName;
	}
	
	/**
	 * 默认数据库名称
	 * @param defaultDBName
	 */
	public void setSp2pDBName(String sp2pDBName)
	{
		this.sp2pDBName=sp2pDBName;
	}
	
	@Override
	protected SqlSessionFactory buildSqlSessionFactory() throws IOException {
		try{
			
			SqlSessionFactory buildSqlSessionFactory = super.buildSqlSessionFactory();
			Properties variables = buildSqlSessionFactory.getConfiguration().getVariables();
			if(StringUtil.isNotBlank(systemDBName))
			{
				variables.setProperty("systemDBName", systemDBName.trim());
			}
			if(StringUtil.isNotBlank(sp2pDBName))
			{
				variables.setProperty("dbName", sp2pDBName.trim());
			}
			logger.info("sqlSession config var: dbName={}, systemDBName={}",variables.get("dbName"),variables.get("systemDBName"));
			return buildSqlSessionFactory;
//			return super.buildSqlSessionFactory();
		} catch(NestedIOException e){
			e.printStackTrace();
			logger.error("创建SqlSessionFactory异常",e);
			System.exit(0);
		}finally{
			ErrorContext.instance().reset();
		}
		return null;
	
	}
}

	