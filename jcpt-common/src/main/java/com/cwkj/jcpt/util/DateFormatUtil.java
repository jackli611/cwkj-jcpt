/**
 * Project Name:jcpt-common <br>
 * File Name:DateFormatUtil.java <br>
 * Package Name:com.hehenian.jcpt.util <br>
 * @author xiezbmf
 * Date:2017年8月31日下午4:21:22 <br>
 * Copyright (c) 2017, 深圳市彩付宝科技有限公司 All Rights Reserved.
 */

package com.cwkj.jcpt.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: DateFormatUtil <br>
 * Description: 返回线程安全的dateFormat
 * @author xiezbmf
 * @Date 2017年8月31日下午4:21:22 <br>
 * @version
 * @since JDK 1.6
 */
public class DateFormatUtil {
	private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final ThreadLocal<Map<String, DateFormat>> tl = new ThreadLocal<Map<String, DateFormat>>();

    /**
     * According to the specified format, Get a DateFormat
     *
     * @param format
     * @return
     */
    public static DateFormat getDateFormat(String format) {
        Map<String, DateFormat> map = tl.get();

        if (map == null) {
            map = new HashMap<String, DateFormat>();
            tl.set(map);
        }

        if (StringUtil.isEmpty(format)) {
            format = DEFAULT_FORMAT;
        }

        DateFormat ret = map.get(format);

        if (ret == null) {
            ret = new SimpleDateFormat(format);
            map.put(format, ret);
        }

        return ret;
    }

    /**
     * Get Default DateFormat
     *
     * @return
     */
    public static DateFormat getDateFormat() {
        return getDateFormat(null);
    }
}

	