package com.cwkj.jcpt.util;

import java.io.Serializable;

import com.alibaba.fastjson.JSONObject;
import com.cwkj.jcpt.util.StringUtil;

/**
 * 返回结果
 * @author ljc
 * @version 1.0
 * @param <T>
 * 返回数据对象
 */
public class BaseResult<T> implements Serializable{

	private String code;
	private String msg;
	private T data;
	/** 成功 **/
	public static final String CODE_SUCCESS="1";
	/** 失败 **/
	public static final String CODE_FAILE="0";
	/** 异常 **/
	public static final String CODE_EXCEPTION="2";
	/** 不确定(处理中) **/
	public static final String CODE_UNCERTAIN="3";
	
	public BaseResult()
	{
		
	}
	
	/**
	 * JSON格式对象
	 * @param code
	 * 结果编号
	 * @param msg
	 * 结果信息
	 */
	public BaseResult(String code,String msg)
	{
		this.setCode(code);
		this.setMsg(msg);
	}
	
	
	
	/**
	 * 结果编号
	 * @param code
	 */
	public void setCode(String code)
	{
		this.code=code;
	}
	
	/**
	 * 结果编号
	 * @return
	 */
	public String getCode()
	{
		return this.code;
	}
	
	/**
	 * 结果信息
	 * @param msg
	 */
	public void setMsg(String msg)
	{
		this.msg=msg;
	}
	
	/**
	 * 结果信息
	 * @return
	 */
	public String getMsg()
	{
		return this.msg;
	}
	
	/**
	 * 返回值
	 * @param data
	 */
	public void setData(T data)
	{
		this.data=data;
	}
	
	/**
	 * 返回值
	 * @return
	 */
	public T getData()
	{
		return this.data;
	}
	
	/**
	 * 结果成功
	 * @return
	 */
	public boolean resultSuccess()
	{
		return this.code!=null && CODE_SUCCESS.equals(this.code);
	}
	
	/**
	 * 结果异常
	 * @return
	 */
	public boolean resultException()
	{
		return this.code!=null && CODE_EXCEPTION.equals(this.code);
	}
	
	/**
	 * 结果失败
	 * @return
	 */
	public boolean resultFail()
	{
		return this.code!=null && CODE_FAILE.equals(this.code);
	}
	
	/**
	 * 返回JSON格式字符串
	 * 格式如下：
	 * {"code":1,"msg":"消息","data":返回值}
	 * @return
	 */
	public String toJsonString()
	{
		StringBuilder result=new StringBuilder("{\"code\":");
		if(this.code==null)
		{
			result.append(CODE_FAILE);
		}else{
			result.append(this.code);
		}
		result.append(",\"msg\":\"");
		if(StringUtil.isNotEmpty(this.msg))
		{
			result.append(StringUtil.toJsValue(this.msg));
		} 
		result.append("\"");
		if(this.data!=null)
		{
			result.append(",\"data\":");
			JSONObject jsonObject=new JSONObject();
			result.append(jsonObject.toJSON(this.data));
		}
		result.append("}");
		return result.toString();
	}
}