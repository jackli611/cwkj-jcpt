package com.cwkj.jcpt.util;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/** 
 * ip工具类
 * @author: harry
 * @date: 2017年7月14日 上午5:57:39  
 */
public class IPUtils {

	private static Logger logger = LoggerFactory.getLogger(IPUtils.class);
	
	public static String getIP(HttpServletRequest request) {  
		
		try {
			String ip =request.getHeader("x-forwarded-for");  
			 if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
			     ip =request.getHeader("Proxy-Client-IP");  
			 }  
			 if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
			      ip = request.getHeader("WL-Proxy-Client-IP");  
			 }  
			 if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
			     ip = request.getRemoteAddr();  
			 }  
			 
			 // 多个路由时，取第一个非unknown的ip
			 final String[] arr = ip.split(",");
			 for (final String str : arr) {
			     if (!"unknown".equalsIgnoreCase(str))
			    	 {
			    	 	ip = str;
			    	 	break;
			    	 }
			 }
			 return ip;
		} catch (Exception e) {
			logger.error("获取ip地址失败！", e);
		}  
		
		return null;
	}  
}
