package com.cwkj.jcpt.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 功能描述:
 * <p/>
 *
 * @author chaoshibin 新增日期：2018/7/28
 * @author chaoshibin 修改日期：2018/7/28
 * @version 1.0.0
 * @since 1.0.0
 */
public class PoiUtil {
    public static int DEFAULT_COLOUMN_WIDTH = 17;

    private PoiUtil() {
    }

    public static void exportExcel(String shadeKey, String title, Map<String, String> headMap, List list, HttpServletResponse response, HttpSession session) {
        try {
            session.setAttribute(shadeKey, "");
            byte[] content = exportExcel(title, headMap, list, null, 0);
            InputStream is = new ByteArrayInputStream(content);
            // 设置response参数，可以打开下载页面
            response.reset();
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + new String((title + ".xlsx").getBytes(), "iso-8859-1"));
            response.setContentLength(content.length);
            ServletOutputStream outputStream = response.getOutputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            BufferedOutputStream bos = new BufferedOutputStream(outputStream);
            byte[] buff = new byte[8192];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);

            }
            session.removeAttribute(shadeKey);
            bis.close();
            bos.close();
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static byte[] exportExcel(String title, Map<String, String> headMap, List list, String datePattern, int colWidth) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JSONArray ja = new JSONArray();
        ja.addAll(list);
        exportExcel(title, headMap, ja, datePattern, colWidth, os);
        return os.toByteArray();
    }

    private static CellStyle createTitleStyle(SXSSFWorkbook workbook) {
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        Font titleFont = workbook.createFont();
        titleFont.setFontHeightInPoints((short) 20);
        titleFont.setBoldweight((short) 700);
        titleStyle.setFont(titleFont);
        return titleStyle;
    }

    private static CellStyle createHeaderStyle(SXSSFWorkbook workbook) {
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        headerStyle.setFillForegroundColor(IndexedColors.TURQUOISE.getIndex());// 设置背景色
        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        headerStyle.setFont(headerFont);
        return headerStyle;
    }

    public static CellStyle createCellStyle(SXSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        cellStyle.setWrapText(true);
        Font cellFont = workbook.createFont();
        cellFont.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        cellStyle.setFont(cellFont);
        return cellStyle;
    }

    public static void setWidth(int colWidth, SXSSFSheet sheet, Map<String, String> headMap, String[] properties, String[] headers) {
        int minBytes = colWidth < DEFAULT_COLOUMN_WIDTH ? DEFAULT_COLOUMN_WIDTH : colWidth;//至少字节数
        int[] arrColWidth = new int[headMap.size()];
        int ii = 0;
        for (Iterator<String> iter = headMap.keySet().iterator(); iter
                .hasNext(); ) {
            String fieldName = iter.next();
            properties[ii] = fieldName;
            headers[ii] = headMap.get(fieldName);
            int bytes = headers[ii].getBytes().length;
            arrColWidth[ii] = bytes < minBytes ? minBytes : bytes;
            sheet.setColumnWidth(ii, arrColWidth[ii] * 256);
            ii++;
        }
    }

    private static void exportExcel(String title, Map<String, String> headMap, JSONArray jsonArray, String datePattern, int colWidth, OutputStream out) {
        if (datePattern == null) datePattern = "yyyy年MM月dd日";

        //字典集合
        Map<String, Map<String, String>> codeListMap = new HashMap<String, Map<String, String>>();
        //声明一个工作薄
        SXSSFWorkbook workbook = new SXSSFWorkbook(1000);//缓存
        workbook.setCompressTempFiles(true);
        XSSFDataFormat format = (XSSFDataFormat) workbook.createDataFormat();
        //表头样式
        CellStyle titleStyle = createTitleStyle(workbook);
        //列头样式
        CellStyle headerStyle = createHeaderStyle(workbook);
        //单元格样式
        CellStyle cellStyle = createCellStyle(workbook);
        //生成一个(带标题)表格
        SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet();
        //设置列宽
        String[] properties = new String[headMap.size()];
        String[] headers = new String[headMap.size()];
        setWidth(colWidth, sheet, headMap, properties, headers);
        // 遍历集合数据，产生数据行
        int rowIndex = 0;
        for (Object obj : jsonArray) {
            if (rowIndex == 65535 || rowIndex == 0) {
                if (rowIndex != 0) {
                    sheet = (SXSSFSheet) workbook.createSheet();//如果数据超过了，则在第二页显示
                    setWidth(colWidth, sheet, headMap, properties, headers);
                }
                SXSSFRow titleRow = (SXSSFRow) sheet.createRow(0);//表头 rowIndex=0
                titleRow.createCell(0).setCellValue(title);
                titleRow.getCell(0).setCellStyle(titleStyle);
                sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, headMap.size() - 1));
                SXSSFRow headerRow = (SXSSFRow) sheet.createRow(1); //列头 rowIndex =1
                for (int i = 0; i < headers.length; i++) {
                    headerRow.createCell(i).setCellValue(headers[i]);
                    headerRow.getCell(i).setCellStyle(headerStyle);
                }
                rowIndex = 2;//数据内容从 rowIndex=2开始
            }
            JSONObject jo = (JSONObject) JSONObject.toJSON(obj);
            SXSSFRow dataRow = (SXSSFRow) sheet.createRow(rowIndex);
            for (int i = 0; i < properties.length; i++) {
                SXSSFCell newCell = (SXSSFCell) dataRow.createCell(i);
                String[] props = properties[i].split(";");
                String[] attrs = props[0].split("\\.");
                JSONObject jsobj = null;
                Object o = null;
                for (int x = 0; x < attrs.length; x++) {
                    if (attrs.length == 1) {
                        o = jo.get(attrs[x]);
                    } else {
                        if (x == 0) {
                            jsobj = (JSONObject) jo.get(attrs[x]);
                            if (jsobj == null) {
                                break;
                            }
                        } else if (x == attrs.length - 1) {
                            if (jsobj == null) {
                                break;
                            }
                            o = jsobj.get(attrs[x]);
                        } else {
                            if (jsobj == null) {
                                break;
                            }
                            jsobj = (JSONObject) jsobj.get(attrs[x]);
                        }
                    }
                }
//	                cellStyle.setDataFormat(format.getFormat("@"));
                if (props.length == 1) {
                    if (o == null)
                        newCell.setCellValue("");
                    else if (o instanceof Date) {
                        newCell.setCellValue(new SimpleDateFormat(datePattern).format(o));
                    } else if (o instanceof Float || o instanceof Double || o instanceof BigDecimal) {
                        newCell.setCellType(SXSSFCell.CELL_TYPE_NUMERIC);
                        cellStyle.setDataFormat(format.getFormat("0.00"));
                        newCell.setCellValue(new BigDecimal(o.toString()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                    } else {
                        newCell.setCellValue(o.toString());
                    }
                } else {
                    o = String.valueOf(o);
                    Map<String, String> codeList = codeListMap.get(props[1]);
                    newCell.setCellValue(codeList.get(o));
                }
                newCell.setCellStyle(cellStyle);
            }
            rowIndex++;
        }
        try {
            workbook.write(out);
            workbook.dispose();
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
