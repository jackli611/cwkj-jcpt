/**
 * Project Name:jcpt-common <br>
 * File Name:StringUtil.java <br>
 * Package Name:com.hehenian.jcpt.util <br>
 * @author xiezbmf
 * Date:2016年11月14日下午3:58:58 <br>
 * Copyright (c) 2016, 深圳市彩付宝网络技术有限公司 All Rights Reserved.
 */

package com.cwkj.jcpt.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;











import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;


/**
 * ClassName: StringUtil
 * @version
 * @since JDK 1.6
 */
public class StringUtil extends StringUtils{
	
	public static String getThirdNo() {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        builder.append(DateUtils.format(new Date(), "yyyyMMddHHmmss"));
        builder.append(random.nextInt(899999)+100000);
        return builder.toString();
    }
	
	
	public static boolean isNotEmpty(String s) {
		return !isEmpty(s);
	}

	public static int String2Int(String val, int dv) {
		try {
			if(StringUtil.isNotEmpty(val)){
				int v = Integer.valueOf(val);
				return v;
			}
		} catch (NumberFormatException e) {
		}
		return dv;
	}

	public static long String2Long(String val, long dv) {
		
		try {
			if(StringUtil.isNotEmpty(val)){
				long v = Long.valueOf(val);
				return v;
			}
		} catch (NumberFormatException e) {
		}
		return dv;
	}
	
	
	public static boolean isNumeric(String str){
		return str.matches("[0-9]+.?[0-9]*");  
	}
	
	/**
	 * 进行html编码操作
	 * @param s 转码后的字符
	 * @return
	 */
	public static String encodeHTML(String s) {
		if (isNotEmpty(s)) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < s.length(); i++) {
				char ch = s.charAt(i);
				switch (ch) {
				case '&':
					sb.append("&amp;");
					break;
				case '<':
					sb.append("&lt;");
					break;
				case '>':
					sb.append("&gt;");
					break;
				case '"':
					sb.append("&quot;");
					break;
				default:
					sb.append(ch);
				}
			}
			return sb.toString();
		} else {
			return "";
		}
	}
	
	
	
	/**
	 * html解码 
	 * @param s 解码后的字符
	 * @return
	 */
	public static String decodeHTML(String s){
		s = s.replaceAll("&lt;","<");
		s = s.replaceAll("&gt;",">");
		s = s.replaceAll("&quot;","\"");
		s = s.replaceAll("&nbsp;"," ");
		s = s.replaceAll("&amp;", "&");
		s = s.replace("&#39;", "'");
        return s;
	}
	

	/**
	 * 
	 * getUUID32:(获取32位uuid). <br/>
	 *
	 * @author xiezbmf
	 * Date:2016年9月12日下午3:12:37 <br/>
	 * @return
	 */
	public static String getUUID32(){
	    UUID uuid = UUID.randomUUID();
        String s = uuid.toString();
        s = s.replace("-", "");
        return s;
	}
	
	
	/**
	 * 转化为javascript的String值格式。
	 * 注：处理单、双引号，及换行串等
	 * @param value
	 * @return
	 */
	public static String toJsValue(String value) {
		StringBuilder out = new StringBuilder();
		if (value == null) {
			return "";
		}
		for (int i = 0, length = value.length(); i < length; i++) {
			char c = value.charAt(i);
			switch (c) {
			case '"':
			case '\\':
			case '/':
				out.append('\\').append(c);
				break;

			case '\t':
				out.append("\\t");
				break;

			case '\b':
				out.append("\\b");
				break;

			case '\n':
				out.append("\\n");
				break;

			case '\r':
				out.append("\\r");
				break;

			case '\f':
				out.append("\\f");
				break;

			default:
				if (c <= 0x1F) {
					out.append(String.format("\\u%04x", (int) c));
				} else {
					out.append(c);
				}
				break;
			}

		}
		return out.toString();
	}
	
	/**
	 * 
	 * deleteLastChar:删除字符串最后一个字符. <br>
	 *
	 * @author xiezbmf
	 * Date:2016年11月18日上午11:12:50 <br>
	 * @param str
	 * @return
	 */
	public static String deleteLastChar(String str){
		AssertUtils.notEmptyStr(str);
		return str.substring(0, str.length()-1);
	}
	
	/**
	 * 
	 * hasIn:判断字符串str是否存在于字符串数组strs. <br>
	 *
	 * @author xiezbmf
	 * Date:2016年11月21日下午3:04:52 <br>
	 * @param str
	 * @param strs
	 * @return
	 */
	public static boolean hasIn(String str,String ...strs){
		if(strs!=null){
			for(String s :strs){
				if(s.equals(str)){
					return true;
				}
			}
		}
		return false;
	}

	  
	/**
	 * 生成第三方序列号
	 * @param prefixStr
	 * @return
	 * @author ljc
	 */
	public static String generateThirdSn(String prefixStr) {
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
		StringBuffer sb = new StringBuffer();
		sb.append(prefixStr).append(sf.format(new Date()));
		char[] chars = UUID.randomUUID().toString().replace("-", "").toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (i % 4 == 3){
				continue;
			} else {
				sb.append(chars[i]);
			}
		}
		if(sb.length()>32){
			return sb.substring(0, 32).toString();
		}else{
			return sb.toString();
		}
	}
	
	/**
	 * 生成数字序列号
	 * @param prefixStr 前缀
	 * @param length 长度
	 * @return
	 */
	public static String generateNumberSn(String prefixStr,Integer length)
	{
		SimpleDateFormat sfDate = new SimpleDateFormat("yyMMddHHmmss");
		StringBuffer sb = new StringBuffer();
		int len=12;
		if(isNotEmpty(prefixStr))
		{
			sb.append(prefixStr);
			len+=prefixStr.length();
		}
		sb.append(sfDate.format(new Date()));
		if(length>len)
		{
			String ran=RandomStringUtils.randomNumeric(length-len);
			sb.append(ran);
		}
		String sn=sb.toString();
		if(sn.length()>len)
		{
			return sn.substring(0,len);
		}
		return sn;
	}
	
	/**
	 * 生成数字序列号
	 * @param prefixStr
	 * @return
	 */
	public static String generateNumberSn(String prefixStr)
	{
		SimpleDateFormat sfDate = new SimpleDateFormat("yyMMddHHmmss");
		StringBuffer sb = new StringBuffer();
		if(isNotEmpty(prefixStr))
		{
			sb.append(prefixStr);
		}
		sb.append(sfDate.format(new Date()));
		String ran=RandomStringUtils.randomNumeric(6);
		sb.append(ran);
		return sb.toString();
	}
	
	
	public static String numberFormat(int formatLength,Integer value)
	{
		  
		return	String.format("%0"+formatLength+"d",value);
	}
	
	@SuppressWarnings("rawtypes")
	public static String listToString(Collection collection, String separator){
		return StringUtil.join(collection, separator);
	}
	
	@SuppressWarnings("rawtypes")
	public static List stringToList(String str, String separator){
		if(StringUtil.isNotEmpty(str)){
			return Arrays.asList(str.split(separator));
		}
		return null;
	}
	
	/**
   * 
   * leftPadZeroToLength:左边补0到一定length. <br>
   * 当数字的长度大于length时返回null
   * 
   * @author xiezbmf
   * @Date 2017年2月8日下午4:54:40 <br>
   * @param original 原始数字
   * @param length 定长
   * @return String
   */
  public static String leftPadZeroToLength(int original,int length){
    if(String.valueOf(original).length()>length){
      return null;
    }
    return String.format("%0"+length+"d", original);
  } 

  public static String getString(Object str) {
	  String input = String.valueOf(str);
	  if ((input == null) || (input.trim() == "") || ("null".equals(input))) {
			return "";
	  }
	  return input;
  }
  
  /**
	 * 将字符串加密成byte，取最后2个byte组装成int
	 * @param str
	 * @return
	 */
	public static <T> int getTableNumberByDigest(String str) {
		if (str == null || str.trim() == "") {
			return -1;
		}
		byte[] md5Digest = DigestUtils.md5(str.getBytes());
		int i = md5Digest[0] + (md5Digest[1]<<8);
		return Math.abs(i);
	}
	
	/**
	 * 
	 * appendStartDate:追加一日之中的开始时间. <br>
	 *
	 * @author xiezbmf
	 * @Date 2017年9月6日上午11:30:53 <br>
	 * @param date pattern yyyy-MM-dd
	 * @return
	 */
	public static String appendStartDate(String date) {
		if(isNotEmpty(date)){
			return date+" 00:00:00";
		}
		return date;
	}
	/**
	 * 
	 * appendEndDate:追加一日之中的最后时间. <br>
	 *
	 * @author xiezbmf
	 * @Date 2017年9月6日上午11:31:18 <br>
	 * @param date pattern yyyy-MM-dd
	 * @return
	 */
	public static String appendEndDate(String date) {
		if(isNotEmpty(date)){
			return date+" 23:59:59";
		}
		return date;
	}
}

	