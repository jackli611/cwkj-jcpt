package com.cwkj.jcpt.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Json工具类
 * @author harry@hehenian.com
 * @date 2017年4月13日下午3:15:52
 */
public class JsonUtils {

	/**
	 * json转为list
	 * @author harry@hehenian.com
	 * @date 2015年9月23日下午5:20:25
	 */
	public static List<Map<String, String>> jsonToList(String jsonString) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
        List<Map<String, String>> paramList = mapper.readValue(jsonString,
                new TypeReference<List<HashMap<String, String>>>() {
                });
        return paramList;
	}
	
	/**
	 * json转map
	 * @author harry@hehenian.com
	 * @date 2015年9月23日下午5:40:30
	 */
	public static Map<String,String> jsonToMap(String jsonString) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
        Map<String, String> paramMap = mapper.readValue(jsonString,
              new TypeReference<HashMap<String, String>>() {
              });
        return paramMap;
	}
	
	/**
	 * 将Object转为String
	 * @author harry@hehenian.com
	 * @date 2015年9月24日下午2:59:20
	 */
	public static String toJSONArrayString(String string) {
		JSONArray jsonArray = JSON.parseArray(string);
		return jsonArray.toString();
	}
	
	/**
	 * 将Object转为String
	 * @author harry@hehenian.com
	 * @date 2015年9月24日下午2:58:55
	 */
	public static String toJSONObjectString(String string) {
		JSONObject jsonObject= JSON.parseObject(string);
		return jsonObject.toString();
	}
	
	/**
	 * 将Object转为String
	 * @author harry@hehenian.com
	 * @date 2017年4月17日上午9:13:08
	 */
	public static String toJSONString(Object object) {
		return JSON.toJSONString(object);
	}
}
