package com.cwkj.jcpt.util;

import java.util.Date;
import java.util.Random;

/**
 * ClassName: ThirdSnUtil <br>
 * Description: TODO <br>
 * @author harry
 * @Date 2017年9月12日上午11:21:57 <br>
 * @version
 * @since JDK 1.6
 */
public class ThirdSnUtil {

    public static String formatUUID() {

        StringBuffer sb = new StringBuffer();
        sb.append("PT").append(DateUtils.format(new Date(), "yyyyMMddHHmmss"));
        sb.append(getRandomNumString(16));
    
        return sb.toString();
    }
    
    private static String getRandomNumString(Integer num) {
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (Integer i = 0; i < num; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
		System.out.println(formatUUID());
	}
}
