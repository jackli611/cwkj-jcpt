package com.cwkj.jcpt.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

/**
 * RSA签名工具
 * @author ljc
 *
 */
/**
 * @author ljc
 *
 */
public class RSASignatureUtils {
	
	public static final String ALGORITHM_MD5="MD5withRSA";
	public static final String ALGORITHM_SHA1="SHA1withRSA";
	private static final String KEY_INSTANCE="RSA";
	
	/**
	 * 获取私钥
	 * @param privateKeyFile 私钥文件
	 * @param privateKeyPassword 私钥密码
	 * @return
	 */
	public static PrivateKey getPrivateKey(String privateKeyFile,String privateKeyPassword) {
		AssertUtils.isNotBlank(privateKeyFile, "私钥文件不能为空");
	        try {
	            KeyStore ks = KeyStore.getInstance("PKCS12"); 
	            FileInputStream fis = new FileInputStream(privateKeyFile);
	            char[] nPassword = null;
	            if(privateKeyPassword!=null)
	                nPassword = privateKeyPassword.toCharArray();
	            ks.load(fis, nPassword);
	            fis.close();
	            Enumeration enuml = ks.aliases();
	            String keyAlias = null;
	            if (enuml.hasMoreElements()) {
	                keyAlias = (String) enuml.nextElement();
	            }
	            PrivateKey prikey = (PrivateKey) ks.getKey(keyAlias, nPassword);
	            return prikey;
	        }
	        catch(Exception e){
	            throw new BusinessException(e);
	        }
	}
	
	
	
	/**
	 * 获取密钥文件内容
	 * @param file 密钥文件
	 * @return
	 */
	private static String pemFile2String(String file)
	{
		  //读取pem证书
		try {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String s = null;//br.readLine();
        StringBuffer publickey = new StringBuffer();
        while ((s = br.readLine())!=null) {
        	if(s.charAt(0)!='-')
            publickey.append(s.trim());
        }
        return publickey.toString();
		}catch (Exception e) {
				 throw new BusinessException(e);
		}
	}
	
	/**
	 * 从pem格式公钥文件中获取公钥
	 * @param publicKeyPemFile pem格式的公钥
	 * @return
	 */
	public static PublicKey getPublicKeyFromPemFile(String publicKeyPemFile){ 
		try {
			String pemKeyStr=pemFile2String(publicKeyPemFile);
			byte[] keyBytes=org.apache.commons.codec.binary.Base64.decodeBase64(pemKeyStr);
			X509EncodedKeySpec keySpec=new X509EncodedKeySpec(keyBytes);
			KeyFactory keyFactory=KeyFactory.getInstance(KEY_INSTANCE);
			PublicKey pubkey= keyFactory.generatePublic(keySpec);
				return pubkey;
		}catch (Exception e) {
			 throw new BusinessException(e);
		}
	}
	
	/**
	 * 从pem格式的公钥字符串中获取公钥
	 * @param pemKeyStr
	 * @return
	 */
	public static PublicKey getPublicKeyFromPemStr(String pemKeyStr){ 
		try {
			byte[] keyBytes=org.apache.commons.codec.binary.Base64.decodeBase64(pemKeyStr);
			X509EncodedKeySpec keySpec=new X509EncodedKeySpec(keyBytes);
			KeyFactory keyFactory=KeyFactory.getInstance(KEY_INSTANCE);
			PublicKey pubkey= keyFactory.generatePublic(keySpec);
				return pubkey;
		}catch (Exception e) {
			 throw new BusinessException(e);
		}
	}
	
	/**
	 * 验签
	 * @param body 签名内容
	 * @param sign 签名值
	 * @param pubKey 公钥
	 * @param algorithm 签名算法
	 * @return
	 */
	public static boolean verify(String body,String sign,PublicKey pubKey,String algorithm) {
		
		try {
            Signature signature = Signature.getInstance(algorithm);
            signature.initVerify(pubKey); 
            signature.update(body.getBytes()); 
            byte[] verifyByte=org.apache.commons.codec.binary.Base64.decodeBase64(sign);
            return signature.verify(verifyByte);
        }
        catch(Exception e){
        	 throw new BusinessException(e);
        }
	}
	
	/**
	 * 签名
	 * @param body 签名内容
	 * @param prikey 私钥
	 * @param algorithm 签名算法
	 * @return
	 */
	public static String sign(String body,PrivateKey prikey,String algorithm) {
		AssertUtils.isNotBlank(algorithm, "加密算法不能为空");
        byte[] signedInfo = null;
        try {
            Signature signature = Signature.getInstance(algorithm);
            signature.initSign(prikey);
            byte[] dataInBytes = body.getBytes();
            signature.update(dataInBytes);
            signedInfo = signature.sign();
        }
        catch(Exception e){
            throw new BusinessException(e);
        }
        return org.apache.commons.codec.binary.Base64.encodeBase64String(signedInfo);
    }





	public static Map<String, String> sortMapByKey(Map<String, String> map) {
		if (map == null || map.isEmpty()) {
			return null;
		}

		Map<String, String> sortMap = new TreeMap<>(
				new MapKeyComparator());

		sortMap.putAll(map);

		return sortMap;
	}

	 public static void main(String[] args) throws Exception 
	{
		 String keyDir="/workspace/eclipse/eic/eic4/eic4/res/key/private/";
		 String privateKeyFile=keyDir+"server.pfx";
		 String publicKeyPemFile=keyDir+"server-key-pub.pem";
		 String privateKeyPassword="123456";
		 String algorithm=ALGORITHM_SHA1;

		 Map<String,String> mapList=new HashMap<String,String>();
		mapList.put("acctid","10");
		mapList.put("community","1231-34234-12312-123123");
		mapList.put("merId","c190301");
		mapList.put("mobile","1890000001");
		mapList.put("secretKey","k1903$01-12");
		mapList.put("community_name","龙城广场");
		mapList.put("trxId","20190321001");
		Map<String, String> resultMap = sortMapByKey(mapList);
		 String bodyStr="acctid=1community=1231-34234-12312-123123merId=c190301mobile=1892286097secretKey=k1903$01-12trxId=20190313094105999";
StringBuffer sb=new StringBuffer();
		for (Map.Entry<String, String> entry : resultMap.entrySet()) {
			//Map.entry<Integer,String> 映射项（键-值对）  有几个方法：用上面的名字entry
			//entry.getKey() ;entry.getValue(); entry.setValue();
			//map.entrySet()  返回此映射中包含的映射关系的 Set视图。
			System.out.println( entry.getKey() + "="
					+ entry.getValue());
			sb.append(entry.getKey());
			if(entry.getKey()!=null)
			{
				sb.append(entry.getValue());
			}
		}
		//bodyStr=sb.toString();
		bodyStr="acctid1002848704communitybcfe0f35-37b0-49cf-a73d-ca96914a46a5community_name小区merIdc190301mobile18922860697secretKeyk1903$01-12trxId201904150539341000";
		 String sign=sign(bodyStr, getPrivateKey(privateKeyFile, privateKeyPassword), algorithm);
		 System.out.println("签名算法:"+algorithm);
		 System.out.println("签名:"+bodyStr+"\n值signedMsg:"+sign);
		 String publicKeyString="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtrp+BZVaUEP4AkNsUTz3\n" +
				 "aLPcLyHuEe8saGtnCHXOz8wg3FuVBO/7x3J1jSaczbPqzd6fGpizIfzmoxVceyMk\n" +
				 "G4lhx37/Bvgxsenk6UN7gd20B4J/OmlRJXTOViSP7ty8HVZ30POOT8FklTEDAIV1\n" +
				 "UyBwHq2oXUKxJRJ1a5pTeaY0HeELHO3ZpQqOdNQFTR7/i+CbMR967jzOmcHQYUTc\n" +
				 "gW1c6tE6SRtYy3EZkz21ExwYcXWqPCggyfTkOlusZI7j7yJqqt6pcaIu5Ki61GGK\n" +
				 "zmRFmMxIWqr3g2ml8C/UfHGgbCt60eB8HFjIreZx3nvKN2y/eqqskE/mNJa9Bv2V\n" +
				 "YwIDAQAB";
//		 boolean verify=verify(bodyStr, sign, getPublicKeyFromPemFile(publicKeyPemFile), algorithm);
		 boolean verify=verify(bodyStr, sign, getPublicKeyFromPemStr(publicKeyString), algorithm);
		 System.out.println("验签结果:"+verify);
		 
	}
}

class MapKeyComparator implements Comparator<String> {

	@Override
	public int compare(String str1, String str2) {

		return str1.compareTo(str2);
	}
}