package com.cwkj.jcpt.common.constant;

/**
 * 基础平台系统常量
 * @author ljc
 *
 */
public class SystemConst {
	
	/**
     * 登录密码加密串
     */
    public static final String PASS_KEY = "OWSd0&sd(fQl1%Uma8OL";
    
    public static final String MD5KEY_SCHEDULE="s@31lS&239)23";
    
    
    public static final String SYSTEM_SUPERROLE="系统超级管理员";

    /**
     * 后台操作员id
     */
    public static final int BG_SYS_ADMIN_ID = -3;
    
    /**
     * 后台操作员 名称
     */
    public static final String BG_SYS_ADMIN_NAME = "后台管理员";
  
    /**
     * 密码设置规则：不少于8位，必须包括大写、小写和数字
     */
    public static final String REGEX_PWD= "^(?:(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).{8,}$";
    /** 密码设置规则：位数不少于6位 **/
    public static final String REGEX_PWD2= "^\\S{6,}$";

    /**
     * 电话号码
     */
    public static final String REGEX_TELEPHONE = "^1(3[0-9]|4[57]|5[0-35-9]|7[01678]|8[0-9])\\d{8}$";
    
   
    /** 默认后台 **/
    public static final int PLATFORM_DEFAULT=1;
    /** 存管后台 **/
    public static final int PLATFORM_DEPOSITORY=2;
    
    /** 系统默认密码 */
    public static final String SYS_USER_PASSWORD_DEFAULT="12345678";
    /** 物业机构角色 */
    public static final String SYS_ROLE_PROPERTYS="propertys";
    /** 核心企业角色名称 */
    public static final String SYS_ROLE_PROPERTYS_NAME="核心企业";
    /** 金融机构角色 */
    public static final String SYS_ROLE_FINANCES="finances";
    /** 资金方角色名称 */
    public static final String SYS_ROLE_FINANCES_NAME="资金方";
    /** 供应商注册 */
    public static final String SYS_ROLE_SUPPLIER="supplier";
    /** 供应商角色名称 */
    public static final String SYS_ROLE_SUPPLIER_NAME="供应商";

    public static final String SYS_ROLE_PERSONALLOAN_NAME="个贷用户";
    public static final String SYS_ROLE_PERSONALLOAN="personalloan";
}
