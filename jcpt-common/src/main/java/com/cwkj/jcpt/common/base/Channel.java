package com.cwkj.jcpt.common.base;

import java.io.Serializable;

public class Channel implements Serializable {
	
	/**
	 * 第三方渠道名称，由平台分配
	 */
	private String channelName;
	/**
	 * 第三方渠道 ，由平台分配
	 */
	private String channelCode ="platform";
	/**
	 * 登录入口 : 微信， h5页面， 第三方 app（app名称）
	 */
	private String loginSource;
	
	/**
	 * 客户终端类型 （ios， Android）
	 */
	private String clientType;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(String loginSource) {
		this.loginSource = loginSource;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	

}
