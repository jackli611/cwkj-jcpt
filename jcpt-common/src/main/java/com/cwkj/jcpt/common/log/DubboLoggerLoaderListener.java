package com.cwkj.jcpt.common.log;

/**
 * dubbo框架使用日志
 * @author ljc
 *
 */
public class DubboLoggerLoaderListener {
	 static{  
	        //设置dubbo使用slf4j来记录日志  
	        System.setProperty("dubbo.application.logger","slf4j");  
	    }  

}
