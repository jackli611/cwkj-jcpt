package com.cwkj.jcpt.common.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 
 * 使用说明：
 * 
 *       使用的时候需要xml配置一个bean  ，构造函数指定存储的 磁盘路径, 以后的存储都是在此目录下的相对路径
 *
 * @author: zhangyunhua
 * @date 2015年1月22日 上午10:11:18
 */
public class FileService {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	private String storeDir;

	private final String file_separator = "/";

	
	public FileService(String storeDir) {
		this.storeDir = storeDir;
	}
	
	public String getFullFilePath(String destFileName) {
		StringBuffer sb = new StringBuffer(this.storeDir);
		if(!storeDir.endsWith(file_separator)) {
			sb.append(file_separator);
		}
		sb.append(destFileName);
		return sb.toString();
	}
	

	/**
	 * 没有缩略图的保存
	 *
	 * @param source
	 *            上传文件的输入流
	 * @param fileName
	 *            上传的原文件名
	 * @return
	 */
	public String saveFileByIntputStream(InputStream source, String fileName) {
		return this.saveFileByIntputStream(fileName, source, FileType.normal);
	}


	/*
	 * 保存输入流到文件
	 */
	public String saveFileByIntputStream(String fileName, InputStream is, String fileType) {
		try {
			File destFile = getDestFile(fileName, fileType);
			FileUtils.copyInputStreamToFile(is, destFile);
			//return this.getRelationPath(destFile);
			return destFile.getAbsolutePath();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return null;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	/*
	 * 保存字符串到文件
	 */
	public String saveStringAsFile(String fileName, String content, String fileType) {
		ByteArrayInputStream byteIs;
		try {
			byteIs = new ByteArrayInputStream(content.getBytes("utf-8"));
			return this.saveFileByIntputStream(fileName,byteIs,fileType);
		} catch (UnsupportedEncodingException e) {
			logger.error("保存文件装换utf-8 byte[]失败：", e);
			return null;
		}
		
	}
	
	/*
	 * 保存base64编码的字符串到文件
	 */
	public String saveBase64StringAsFile(String fileName, String content, String fileType) {
		byte[] fileByte;
		try {
			fileByte = new BASE64Decoder().decodeBuffer(content);
		} catch (IOException e1) {
			logger.error("保存文件装换base64失败：", e1);
			return null;
		}
		ByteArrayInputStream  byteIs = new ByteArrayInputStream(fileByte);
		return this.saveFileByIntputStream(fileName,byteIs,fileType);
		
	}

	
	/**
	 * 删除文件
	 * @param filePath
	 * @return
	 */
	public boolean delFile(String filePath) {
		if (StringUtils.isNotBlank(filePath)) {
			File file = new File(storeDir + filePath);
			if (file.exists()) {
				try {
					boolean rs = file.delete();
					return rs;
				} catch (Exception e) {
					logger.error("error: ", e);
				}
			}
		}
		return false;
	}



	/**
	 * 上传zip格式的文件
	 *
	 * @param source
	 */
	public List<String> saveZip(InputStream source) {
		List<String> filePathList = new ArrayList<String>();
		try {
			ZipInputStream Zin = new ZipInputStream(source);// 输入源zip流
			BufferedInputStream Bin = new BufferedInputStream(Zin);

			// 解压zip逐个读出图片保存到server
			ZipEntry entry;
			while ((entry = Zin.getNextEntry()) != null && !entry.isDirectory()) {
				File newImgFile = this.getDestFile(entry.getName());
				// 写文件流
				FileOutputStream out = new FileOutputStream(newImgFile);
				BufferedOutputStream Bout = new BufferedOutputStream(out);
				int b;
				while ((b = Bin.read()) != -1) {
					Bout.write(b);
				}
				Bout.close();
				out.close();
				filePathList.add(this.getRelationPath(newImgFile));
			}
			if(Bin != null) {
				Bin.close();
			}
			if(Zin != null) {
				Zin.close();
			}
		} catch (IOException e) {
			logger.error("文件解压失败", e);
		}
		logger.info("文件解压完成");
		return filePathList;
	}

	
	

	/**
	 * 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
	 * @param imgFile
	 * @return
	 */
	public String getImageStr(String imgFile) {
		InputStream in = null;
		byte[] data = null;
		// 读取图片字节数组
		try {
			in = new FileInputStream(imgFile);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (Exception e) {
			logger.error("error: ", e);
		}
		String imgStr = null;
		if (data != null) {
			// 对字节数组Base64编码
			BASE64Encoder encoder = new BASE64Encoder();
			imgStr = encoder.encode(data);// 返回Base64编码过的字节数组字符串
			imgStr = imgStr.replaceAll("[\n\r\t]", "");
		}
		return imgStr;// 去掉换行符
	}


	/**
	 *  将url图片文件转化为字节数组字符串，并对其进行Base64编码处理
	 * @param url
	 * @return
	 */
	public String getImageFromUrl(String url) {
		InputStream in = null;
		byte[] imgBytes = null;
		CloseableHttpResponse httpRresponse = null;
		CloseableHttpClient httpclient = null;
		BufferedInputStream br = null;
		ByteArrayOutputStream baos = null;
		String imgStr = null;
		// 读取图片字节数组
		try {
			httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(url);
			httpRresponse = httpclient.execute(httpGet);
			in = httpRresponse.getEntity().getContent();
			br = new BufferedInputStream(in);
			baos = new ByteArrayOutputStream();

			//读到字节流
			byte[] bs = new byte[1024];
			int len = 0;
			while ((len = br.read(bs)) > 0) {
				baos.write(bs, 0, len);
			}
			//字节流转字节数组
			imgBytes = baos.toByteArray();

			//转base64
			if (imgBytes != null) {
				// 对字节数组Base64编码
				BASE64Encoder encoder = new BASE64Encoder();
				imgStr = encoder.encode(imgBytes);// 返回Base64编码过的字节数组字符串
				imgStr = imgStr.replaceAll("[\n\r\t]", "");
			}
		} catch (Exception e) {
			logger.error("error: ", e);
		}finally{
			try{
				if(in != null){
					in.close();
				}
				if(baos != null){
					baos.close();
				}
				if(br !=null){
					br.close();
				}
				if(httpclient != null){
					httpclient.close();
				}
				in =null;
				baos = null;
			}catch(Exception e){
				logger.error("error: ", e);
			}
		}
		return imgStr;// 去掉换行符
	}

	
	
	private File getDestFile(String fileName, String fileType) {
		//文件类型+日月生成目录
		String subDir = this.getSubDir(fileType);
		File dir = new File(getDir() + subDir);
		String filePath = getNewFileName(fileName, dir);
		File destFile = new File(dir + file_separator + filePath);
		return destFile;
	}
	private File getDestFile(String fileName) {
		return this.getDestFile(fileName, FileType.normal);
	}
	
	private String getNewFileName(String fileName, File dir) {
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String newFileName = FileSavingUtils.getNewFileName(fileName);
		return newFileName;
	}

	
	
	private String getSubDir(String fileType) {
		StringBuilder sb = new StringBuilder();
		sb.append(file_separator).append(fileType)
		.append(file_separator).append(DateFormatUtils.format(new Date(), "yyyyMM"))
		.append(file_separator).append(DateFormatUtils.format(new Date(), "dd"));
		return sb.toString();
	}

	private String getDir() {
		return this.storeDir;
	}

	private String getRelationPath(File f) {
		String fileDir =  this.getDir();
		return f.getAbsolutePath().replace(fileDir, "");
	}

	

	public static void main(String[] args) {
		FileService fs = new FileService("d:/");
		String base64 = fs.getImageFromUrl("http://rec.hehuayidai.com/res/images/app/uimg1.png");
		System.out.println(base64);
	}


}