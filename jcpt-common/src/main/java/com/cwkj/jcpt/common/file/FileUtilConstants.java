package com.cwkj.jcpt.common.file;

public class FileUtilConstants {
	
    /**
     * 字符串：空
     */
    public final static String EMPTY_STR  = "";
	
	/**
     * 半角字符：空格
     */
    public final static String BLANK_STR  = " ";

    /**
     * 半角字符：斜杠
     */
    public final static String SLASH      = "/";

    /**
     * 半角字符：反斜杠
     */
    public final static String BACKSLASH  = "\\";

    /**
     * 半角字符：点
     */
    public final static String POINT      = ".";

    /**
     * 半角字符：逗号
     */
    public final static String COMMA      = ",";

    /**
     * 半角字符：下划线
     */
    public final static String UNDERLINE  = "_";

    /**
     * 半角字符：中划线
     */
    public final static String MIDDLELINE = "-";
    
    /**
     * 字母：x
     */
    public final static String X          = "x";
    

}
