SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE product;




/* Create Tables */

-- 商品
CREATE TABLE product
(
	-- 产品ID
	id int NOT NULL AUTO_INCREMENT COMMENT '产品ID',
	-- 产品名称
	productName varchar(100) COMMENT '产品名称',
	-- 级别
	levels int COMMENT '级别',
	-- 企业产品
	enterpriseProduct int DEFAULT 0 COMMENT '企业产品',
	-- 个人产品
	personalProduct int DEFAULT 0 COMMENT '个人产品',
	-- 创建时间
	createTime datetime COMMENT '创建时间',
	-- 修改时间
	updateTime datetime COMMENT '修改时间',
	-- 创建者ID
	recordId int COMMENT '创建者ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '商品' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;



