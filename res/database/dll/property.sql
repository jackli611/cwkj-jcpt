SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE company;
DROP TABLE propertyUser;
DROP TABLE propertyOrg_product;
DROP TABLE supplier_rate;
DROP TABLE supplier_invitation;
DROP TABLE payInfo;
DROP TABLE propertyOrg;
DROP TABLE supplier;
DROP TABLE supplier_audit;
DROP TABLE propertyFinances;




/* Create Tables */

-- 企业
CREATE TABLE company
(
	-- ID
	id varchar(36) NOT NULL COMMENT 'ID',
	-- 企业LOGO
	logo varchar(100) COMMENT '企业LOGO',
	-- 公司名称
	names varchar(50) NOT NULL COMMENT '公司名称',
	-- 公司简称
	shortName  varchar(50) COMMENT '公司简称',
	-- 组织机构代码
	organizationCode varchar(50) NOT NULL COMMENT '组织机构代码',
	-- 法人主体
	legal varchar(20) NOT NULL COMMENT '法人主体',
	-- 法人身份证号码
	legalIdNo varchar(35) NOT NULL COMMENT '法人身份证号码',
	-- 手机号
	mobile varchar(15) NOT NULL COMMENT '手机号',
	-- 审批状态
	approvalStatus int NOT NULL COMMENT '审批状态',
	-- 邮箱
	email varchar(20) COMMENT '邮箱',
	-- 等级
	levels int COMMENT '等级',
	-- 营业执照
	businessLicense varchar(200) COMMENT '营业执照',
	-- 附件
	attachment text COMMENT '附件',
	-- 有效状态
	status int NOT NULL COMMENT '有效状态',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '企业' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 物业所属用户
CREATE TABLE propertyUser
(
	-- ID
	id varchar(36) NOT NULL COMMENT 'ID',
	-- 系统账户ID
	userId int COMMENT '系统账户ID',
	-- 父级账户ID
	parentUserId int COMMENT '父级账户ID',
	-- 物业机构ID
	propertyId varchar(36) COMMENT '物业机构ID',
	-- 用户姓名
	realName varchar(50) NOT NULL COMMENT '用户姓名',
	-- 手机号
	mobile varchar(15) NOT NULL COMMENT '手机号',
	-- 邮箱
	email varchar(20) COMMENT '邮箱',
	-- 性别
	sex int COMMENT '性别',
	-- 可创建账户标识
	buildUserTag int COMMENT '可创建账户标识',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	-- 职务
	position varchar(20) COMMENT '职务',
	-- 用户类型
	userType int COMMENT '用户类型',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '物业所属用户' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 物业产品关联信息
CREATE TABLE propertyOrg_product
(
	-- 物业机构ID
	propertyOrgId varchar(36) COMMENT '物业机构ID',
	-- 产品ID
	productId int COMMENT '产品ID'
) ENGINE = InnoDB COMMENT = '物业产品关联信息' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 供应商利率
CREATE TABLE supplier_rate
(
	-- ID
	id int NOT NULL AUTO_INCREMENT COMMENT 'ID',
	-- 物业机构ID
	propertyOrgId varchar(36) NOT NULL COMMENT '物业机构ID',
	-- 产品ID
	productId int NOT NULL COMMENT '产品ID',
	-- 等级
	levels int NOT NULL COMMENT '等级',
	-- 利率
	rate decimal(10,4) COMMENT '利率',
	-- 还款方式：1、每月付息到期还本，2、放款时，利息一次性收取
	paymentType int COMMENT '还款方式：1、每月付息到期还本，2、放款时，利息一次性收取',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '供应商利率' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 供应商邀请注册
CREATE TABLE supplier_invitation
(
	-- ID
	id int NOT NULL AUTO_INCREMENT COMMENT 'ID',
	-- 物业机构ID
	propertyId varchar(36) NOT NULL COMMENT '物业机构ID',
	-- 邀请码
	inviteCode varchar(50) NOT NULL COMMENT '邀请码',
	-- 访问地址
	links varchar(200) COMMENT '访问地址',
	-- 登记时间
	signUpTime datetime COMMENT '登记时间',
	-- 有效到期时间
	expiryDate date COMMENT '有效到期时间',
	-- 手机号
	mobile varchar(20) COMMENT '手机号',
	-- 备注
	remark varchar(100) COMMENT '备注',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '供应商邀请注册' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 支付信息
CREATE TABLE payInfo
(
	-- ID
	id varchar(36) NOT NULL COMMENT 'ID',
	-- 批次号
	batchId varchar(20) COMMENT '批次号',
	-- 应付单号
	payNo varchar(20) COMMENT '应付单号',
	-- 到期日
	payDate date NOT NULL COMMENT '到期日',
	-- 企业ID
	companyId varchar(36) NOT NULL COMMENT '企业ID',
	-- 供应商ID
	supplierId varchar(36) COMMENT '供应商ID',
	-- 应付金额
	amount decimal(10,2) COMMENT '应付金额',
	-- 支付状态
	payStatus int COMMENT '支付状态',
	-- 合同编号
	contractNo varchar(30) COMMENT '合同编号',
	-- 合同日期
	contractDate date COMMENT '合同日期',
	-- 合同金额
	contractAmount decimal(10,2) COMMENT '合同金额',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '支付信息' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 物业机构
CREATE TABLE propertyOrg
(
	-- ID
	id varchar(36) NOT NULL COMMENT 'ID',
	-- 企业LOGO
	logo varchar(100) COMMENT '企业LOGO',
	-- 公司名称
	names varchar(50) NOT NULL COMMENT '公司名称',
	-- 公司简称
	shortName  varchar(50) NOT NULL COMMENT '公司简称',
	-- 组织机构代码
	organizationCode varchar(50) NOT NULL COMMENT '组织机构代码',
	-- 法人主体
	legal varchar(20) COMMENT '法人主体',
	-- 法人身份证号码
	legalIdNo varchar(35) COMMENT '法人身份证号码',
	-- 手机号
	mobile varchar(15) COMMENT '手机号',
	-- 营业执照
	businessLicense varchar(200) COMMENT '营业执照',
	-- 附件
	attachment text COMMENT '附件',
	-- 系统账户ID
	userId int COMMENT '系统账户ID',
	-- 有效状态
	status int NOT NULL COMMENT '有效状态',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	-- 备注
	remark varchar(100) COMMENT '备注',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '物业机构' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 供应商
CREATE TABLE supplier
(
	-- ID
	id varchar(36) NOT NULL COMMENT 'ID',
	-- 供应商编号
	supplierNo varchar(20) COMMENT '供应商编号',
	-- 公司名称
	names varchar(50) NOT NULL COMMENT '公司名称',
	-- 手机号
	mobile varchar(15) COMMENT '手机号',
	-- 法人主体
	legal varchar(20) COMMENT '法人主体',
	-- 法人身份证号码
	legalIdNo varchar(35) COMMENT '法人身份证号码',
	-- 组织机构代码
	organizationCode varchar(50) NOT NULL COMMENT '组织机构代码',
	-- 地址
	address varchar(50) COMMENT '地址',
	-- 银行卡号
	bankCardno varchar(50) COMMENT '银行卡号',
	-- 银行名称
	bankName varchar(20) COMMENT '银行名称',
	-- 银行卡账户名
	bankAccount varchar(20) COMMENT '银行卡账户名',
	-- 营业执照
	businessLicense varchar(200) COMMENT '营业执照',
	-- 物业机构ID
	propertyId varchar(36) COMMENT '物业机构ID',
	-- 系统账户ID
	userId int COMMENT '系统账户ID',
	-- 有效状态
	status int NOT NULL COMMENT '有效状态',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '供应商' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 供应商审核
CREATE TABLE supplier_audit
(
	-- ID
	id int NOT NULL AUTO_INCREMENT COMMENT 'ID',
	-- 供应商ID
	supplierId varchar(36) NOT NULL COMMENT '供应商ID',
	-- 物业机构ID
	propertyId varchar(36) NOT NULL COMMENT '物业机构ID',
	-- 有效状态
	status int NOT NULL COMMENT '有效状态',
	-- 备注
	remark varchar(100) NOT NULL COMMENT '备注',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '供应商审核' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 物业的资金机构
CREATE TABLE propertyFinances
(
	-- ID
	id int NOT NULL AUTO_INCREMENT COMMENT 'ID',
	-- 物业机构ID
	propertyId varchar(36) NOT NULL COMMENT '物业机构ID',
	-- 资金机构ID
	financeId varchar(36) NOT NULL COMMENT '资金机构ID',
	-- 授信额度
	creditAmount decimal(10,2) COMMENT '授信额度',
	-- 匹配日期
	metaDate date COMMENT '匹配日期',
	-- 还款方式：1、每月付息到期还本，2、放款时，利息一次性收取
	paymentType int COMMENT '还款方式：1、每月付息到期还本，2、放款时，利息一次性收取',
	-- 担保方式
	guaranteeType int COMMENT '担保方式',
	-- 有效状态
	status int NOT NULL COMMENT '有效状态',
	-- 利息
	interestRate decimal(10,2) COMMENT '利息',
	-- 附件
	attachment text COMMENT '附件',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '物业的资金机构' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;



