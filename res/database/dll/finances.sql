SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE financeOrg;
DROP TABLE financeUser;




/* Create Tables */

-- 金融机构
CREATE TABLE financeOrg
(
	-- ID
	id varchar(36) NOT NULL COMMENT 'ID',
	-- LOGO
	logo varchar(100) COMMENT 'LOGO',
	-- 机构名称
	names varchar(50) NOT NULL COMMENT '机构名称',
	-- 机构简称
	shortName varchar(50) NOT NULL COMMENT '机构简称',
	-- 组织机构代码
	organizationCode varchar(50) NOT NULL COMMENT '组织机构代码',
	-- 法人主体
	legal varchar(20) COMMENT '法人主体',
	-- 法人身份证号码
	legalIdNo varchar(35) COMMENT '法人身份证号码',
	-- 手机号
	mobile varchar(15) COMMENT '手机号',
	-- 营业执照
	businessLicense varchar(200) COMMENT '营业执照',
	-- 附件
	attachment text COMMENT '附件',
	-- 系统账户ID
	userId int COMMENT '系统账户ID',
	-- 有效状态
	status int NOT NULL COMMENT '有效状态',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	-- 备注
	remark varchar(100) COMMENT '备注',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '金融机构' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


-- 金融机构用户
CREATE TABLE financeUser
(
	-- ID
	id varchar(36) NOT NULL COMMENT 'ID',
	-- 系统账户ID
	userId int COMMENT '系统账户ID',
	-- 父级账户ID
	parentUserId int COMMENT '父级账户ID',
	-- 金融机构ID
	financeId varchar(36) COMMENT '金融机构ID',
	-- 用户姓名
	realName varchar(50) NOT NULL COMMENT '用户姓名',
	-- 手机号
	mobile varchar(15) NOT NULL COMMENT '手机号',
	-- 邮箱
	email varchar(20) COMMENT '邮箱',
	-- 性别
	sex int COMMENT '性别',
	-- 可创建账户标识
	buildUserTag int COMMENT '可创建账户标识',
	-- 创建时间
	createTime datetime NOT NULL COMMENT '创建时间',
	-- 修改时间
	updateTime datetime NOT NULL COMMENT '修改时间',
	-- 操作记录ID
	recordUserId int NOT NULL COMMENT '操作记录ID',
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '金融机构用户' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;



