#!/bin/bash

os=`uname -s`

projectPath="com/cwkj/scf/"

replaceCmd='-i '
case "`uname`" in
Linux*) 
replaceCmd="-i "
;;
esac

echo $os
echo $projectPath

IFS="!!"

fromStr=("s/import com.cwkj.scf.common/import com.cwkj.jcpt.common/g"
"s/import com.cwkj.scf.controller.base.BaseAction/import com.cwkj.jcpt.controller.base.BaseAction/g"
"s/import com.cwkj.scf.controller.base.JsonView/import com.cwkj.jcpt.controller.base.JsonView/g"
"s/import com.cwkj.scf.util.BusinessException/import com.cwkj.jcpt.util.BusinessException/g"
"s/import com.cwkj.scf.util.AssertUtils/import com.cwkj.jcpt.util.AssertUtils/g"
"s/package com.cwkj.scf.controller./package com.cwkj.jcpt.controller./g")


if [ ${os} == 'Darwin' ];then

    for data in ${fromStr[@]}
    do
#echo ${data}
       sed -i "" ${data} /workspace/temp/code/src/$projectPath*/*/*/*.java
       sed -i "" ${data} /workspace/temp/code/src/$projectPath*/*/*.java
    done
else
    for data in ${fromStr[@]}
    do
# echo ${data}
    sed -i  ${data} /workspace/temp/code/src/$projectPath*/*/*/*.java
    sed -i  ${data} /workspace/temp/code/src/$projectPath*/*/*.java
    done
fi

exit








