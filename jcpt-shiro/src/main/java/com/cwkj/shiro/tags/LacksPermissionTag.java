package com.cwkj.shiro.tags;

/**
 * 缺少权限
 * @author ljc
 * @version 1.0
 */
public class LacksPermissionTag extends PermissionTag {

    //TODO - complete JavaDoc

    public LacksPermissionTag() {
    }

    protected boolean showTagBody(String p) {
        return !isPermitted(p);
    }

}
