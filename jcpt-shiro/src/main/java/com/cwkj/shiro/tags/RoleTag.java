package com.cwkj.shiro.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 角色
 * @author ljc
 * @version 1.0
 */
public abstract class RoleTag extends SecureTag {

    //TODO - complete JavaDoc

    private String name = null;

    public RoleTag() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int onDoStartTag() throws JspException {
        boolean show = showTagBody(getName());
        if (show) {
            return TagSupport.EVAL_BODY_INCLUDE;
        } else {
            return TagSupport.SKIP_BODY;
        }
    }

    protected abstract boolean showTagBody(String roleName);

}

