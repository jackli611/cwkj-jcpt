package com.cwkj.shiro.tags;

/**
 * 缺少角色
 * @author ljc
 * @version 1.0
 */
public class LacksRoleTag extends RoleTag {

    //TODO - complete JavaDoc

    public LacksRoleTag() {
    }

    protected boolean showTagBody(String roleName) {
        boolean hasRole = getSubject() != null && getSubject().hasRole(roleName);
        return !hasRole;
    }

}

