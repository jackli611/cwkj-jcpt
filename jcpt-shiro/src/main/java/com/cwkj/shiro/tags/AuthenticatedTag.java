package com.cwkj.shiro.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 权限认证
 * @author ljc
 * @version 1.0
 */
public class AuthenticatedTag extends SecureTag {

    //TODO - complete JavaDoc

    private static final Logger log = LoggerFactory.getLogger(AuthenticatedTag.class);

    public int onDoStartTag() throws JspException {
        if (getSubject() != null && getSubject().isAuthenticated()) {
            if (log.isTraceEnabled()) {
                log.trace("Subject exists and is authenticated.  Tag body will be evaluated.");
            }
            return TagSupport.EVAL_BODY_INCLUDE;
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Subject does not exist or is not authenticated.  Tag body will not be evaluated.");
            }
            return TagSupport.SKIP_BODY;
        }
    }
}
