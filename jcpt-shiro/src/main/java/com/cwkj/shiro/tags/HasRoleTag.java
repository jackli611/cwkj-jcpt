package com.cwkj.shiro.tags;

/**
 * 是否有角色
 * @author ljc
 * @version 1.0
 */
public class HasRoleTag extends RoleTag {

    //TODO - complete JavaDoc

    public HasRoleTag() {
    }

    protected boolean showTagBody(String roleName) {
        return getSubject() != null && getSubject().hasRole(roleName);
    }

}
