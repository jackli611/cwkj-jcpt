package com.cwkj.shiro.tags;

/**
 * 是否有权限
 * @author ljc
 * @version 1.0
 */
public class HasPermissionTag extends PermissionTag {


    public HasPermissionTag() {
    }

    protected boolean showTagBody(String p) {
        return isPermitted(p);
    }

}
