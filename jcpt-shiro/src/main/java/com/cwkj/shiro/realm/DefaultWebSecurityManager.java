package com.cwkj.shiro.realm;

import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.mgt.SessionManager;

public class DefaultWebSecurityManager extends org.apache.shiro.web.mgt.DefaultWebSecurityManager{
	
	 /** 开启Redis服务状态 */
	private int enableRedis=0;
	/** redisSession */
	private SessionManager redisSessionManager;
	/** redisCacheManager */
	private CacheManager redisCacheManager;

	/**
	 * 开启Redis服务状态
	 * @return 
	 */
	public int getEnableRedis() {
		return enableRedis;
	}
	

	/**
	 * 开启Redis服务状态
	 * @param enableRedis 
	 */
	public void setEnableRedis(int enableRedis) {
		this.enableRedis = enableRedis;
	}
	
	public void setRedisSessionManager(SessionManager sessionManager)
	{
		this.redisSessionManager=sessionManager;
		if(this.getEnableRedis()!=0)
		{
			this.setSessionManager(sessionManager);
		}
		
	}


	/**
	 * redisCacheManager
	 * @return 
	 */
	public CacheManager getRedisCacheManager() {
		return redisCacheManager;
	}
	


	/**
	 * redisCacheManager
	 * @param redisCacheManager 
	 */
	public void setRedisCacheManager(CacheManager redisCacheManager) {
		this.redisCacheManager = redisCacheManager;
		if(this.getEnableRedis()!=0)
		{
			this.setCacheManager(redisCacheManager);
		}
	}
	


	/**
	 * redisSession
	 * @return 
	 */
	public SessionManager getRedisSessionManager() {
		return redisSessionManager;
	}
	
	
	
	

}
