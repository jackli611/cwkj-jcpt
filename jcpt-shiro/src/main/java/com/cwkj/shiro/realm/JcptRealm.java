package com.cwkj.shiro.realm;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwkj.jcpt.common.constant.SystemConst;
import com.cwkj.jcptsystem.model.system.SysUserDo;
import com.cwkj.jcptsystem.service.system.ParamService;
import com.cwkj.jcptsystem.service.system.SysUserService;
import com.cwkj.shiro.util.SessionUtils;

/**
 * 
 * @author ljc
 */
@Service("jcptRealm")
public class JcptRealm extends AuthorizingRealm {
	private static Logger log=LoggerFactory.getLogger(JcptRealm.class);
	
	@Autowired
	private SysUserService sysUserService;
	@Resource
	private ParamService paramService;
	
	/**
	 * 为当前登录的Subject授予角色和权限
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals){
		try{
//			Object principal = principals.getPrimaryPrincipal();
			SysUserDo user = (SysUserDo)principals.getPrimaryPrincipal();
//			SysUserDo user=sysUserService.findSysUserByUserName((String)principal);
			if(user!=null && (user.getLocked()==null || user.getLocked().intValue()!=2))//账户存在，且不是锁定状态
			{
				Integer userId=user.getId();
				
				List<String> roleList = sysUserService.getRoleStrListByUserId(userId);
				List<String> permissionList = sysUserService.getPermissionCodeStrListByUserId(userId);
		 
				//为当前用户设置角色和权限
				SimpleAuthorizationInfo simpleAuthorInfo = new SimpleAuthorizationInfo();
				simpleAuthorInfo.addRoles(roleList);
				simpleAuthorInfo.addStringPermissions(permissionList);
				
				return simpleAuthorInfo;
			}
		}catch (Exception e) {
			log.error("获取用户认证权限异常",e);
		}
		return null;
	}

	
	/**
	 * 验证当前登录的Subject
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
//		try{
			//获取基于用户名和密码的令牌
			UsernamePasswordToken token = (UsernamePasswordToken)authcToken;
	 
			SysUserDo user=sysUserService.findSysUserByUserName(token.getUsername());
			if (null != user) {
				
				if(user.getLocked()!=null && user.getLocked().equals(2))
				{
					throw new LockedAccountException("账户已锁定");
				}
				String ERROR_PWD_TIME = paramService.queryValueByCode("ERROR_PWD_TIME");
				ERROR_PWD_TIME = StringUtils.isBlank(ERROR_PWD_TIME) ? "5" : ERROR_PWD_TIME;
				int months = Integer.parseInt(ERROR_PWD_TIME);
				
				if(user.getIncorrectTime()!=null && user.getIncorrectTime().intValue()>months)
				{
					throw new ExcessiveAttemptsException("密码错误次数过多，账户已被锁定");
				}
				String psw=String.valueOf( token.getPassword());
				user.setIp(token.getHost());
				if(!psw.equals(user.getPassword()))
				{
					Integer incorrectTime=user.getIncorrectTime();
					if(incorrectTime==null)
					{
						incorrectTime=1;
					}else{
						incorrectTime=incorrectTime+1;
					}
					user.setIncorrectTime(incorrectTime);
					sysUserService.updateLoginExcessiveAttemptsByUserId(user);
				}else{
					user.setIncorrectTime(0);
					sysUserService.updateLoginExcessiveAttemptsByUserId(user);
				}
				AuthenticationInfo authcInfo = new SimpleAuthenticationInfo(user, user.getPassword(), user.getId()+"");
				
				List<Session> list = SessionUtils.getSessionListByUserName(token.getUsername());
				if(null != list && list.size() > 0){
					DefaultWebSessionManager sessionManager = SessionUtils.getSessionManager();
					for(Session session : list){
						sessionManager.getSessionDAO().delete(session);//消掉session
					}
				}
				
				return authcInfo;
			}  
//		}catch (Exception e) {
//			log.error("验证用户登录信息异常", e);
//		}
		return null;
	}
	
	@Override
	 public boolean isPermitted(PrincipalCollection principals, String permission) {
		boolean result=this.hasRole(principals, SystemConst.SYSTEM_SUPERROLE);
		if(!result)
		{
			return super.isPermitted(principals, permission);
		}
		return result;
	 }
	
	@Override
	protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
		if(principals!=null)
		{
			SysUserDo user=(SysUserDo) principals.getPrimaryPrincipal();
			return user.getId();
		}
        return principals;
    }
	
	/**
	 * 将一些数据放到ShiroSession中,以便于其它地方使用
	 * @see 比如Controller,使用时直接用HttpSession.getAttribute(key)就可以取到
	 */
//	private void setSession(Object key, Object value){
//		Subject currentUser = SecurityUtils.getSubject();
//		if(null != currentUser){
//			Session session = currentUser.getSession();
//			System.out.println("Session默认超时时间为[" + session.getTimeout() + "]毫秒");
//			if(null != session){
//				session.setAttribute(key, value);
//			}
//		}
//	}
}