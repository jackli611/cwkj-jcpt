package com.cwkj.word.util;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import freemarker.cache.ByteArrayTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;


/**
 * 	用word做模板，用poi api结合freemark填充word, 用到的word的xml 特性，填充完后再输出doc格式文档
 * @author harry
 *
 */
public class XMlToDoc{


	/**
	 * 	使用方法: 首先用word做模板，用freemark语法， 例如 ${id}， table 等需要另存后用 edit修改， 图片转base64填充
	 * 
	 *	本方法是把模板 修改 成 .zip后缀的文档，先填充 document.xml 然后替换 zip文档对应的document.xml ，再转成doc文档
	 * @param fillDataMap
	 * @throws Exception
	 */
     public	static  void fillWord(Map<String,String> fillDataMap,String templateZipFile,String destFile) throws Exception{
        /** 初始化配置文件 **/
        Configuration configuration = new Configuration();
        
        ZipInputStream docZipInputStream = ZipUtils.wrapZipInputStream(new FileInputStream(new File(templateZipFile)));
        
        String itemname = "word/document.xml";
        //从 zip里获取模板  "word/document.xml";
        ByteArrayTemplateLoader templateLoader = new ByteArrayTemplateLoader();
        byte[] templateContent = ZipUtils.getItem(docZipInputStream,itemname);
		templateLoader.putTemplate("document.xml", templateContent );
		configuration.setTemplateLoader(templateLoader );
		
		
        /** 加载模板 **/
        Template template = configuration.getTemplate("document.xml");
        /** 指定freemarker处理完成输出流 **/
        ByteArrayOutputStream  out = new ByteArrayOutputStream();
        Writer outWriter = new BufferedWriter(new OutputStreamWriter(out),10240);
        template.process(fillDataMap,outWriter);
        
        try {
        	ZipInputStream zipInputStream = ZipUtils.wrapZipInputStream(new FileInputStream(new File(templateZipFile)));
            ZipOutputStream zipOutputStream = ZipUtils.wrapZipOutputStream(new FileOutputStream(new File(destFile)));
            ZipUtils.replaceItem(zipInputStream, zipOutputStream, itemname, new ByteArrayInputStream(out.toByteArray()));
            System.out.println("success");
        } catch (Exception e) {
            System.out.println(e.toString());
        }finally {
        	if(out != null){
                out.close();
            }
        }
    }
     
     
     public static void main(String[] args) throws Exception {
    	 
    	String templateZipFile = "F:\\code-chengwu-ljc\\scf\\scf\\jcpt-service\\word2pdf\\src\\main\\resources\\template\\test_template.zip";
		Map<String, String> fillDataMap = new HashMap<String,String>();
		fillDataMap.put("loanName", "张三");
		fillDataMap.put("investorName", "李四");
		fillDataMap.put("loanAmt", "50000.00");
		fillDataMap.put("orderCode", "DSCF001");
		fillDataMap.put("signYear", "2018");
		fillDataMap.put("signMonth", "05");
		fillDataMap.put("signDay", "28");
		String destFile = "F:\\code-chengwu-ljc\\scf\\scf\\jcpt-service\\word2pdf\\src\\main\\resources\\template\\test_template.docx";
		
		
		fillWord(fillDataMap,templateZipFile,destFile);
	}

}
































































