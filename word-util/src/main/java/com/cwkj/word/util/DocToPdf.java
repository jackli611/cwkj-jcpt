package com.cwkj.word.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import fr.opensagres.xdocreport.itext.extension.font.IFontProvider;
import fr.opensagres.xdocreport.itext.extension.font.ITextFontRegistry;



public class DocToPdf{

    /**
     * 	生成pdf
     * @throws IOException 
    */
    public  static  void makePdfByXcode(InputStream docFileInputStream,String pdfFile) throws IOException{
        
    	File outFile=new File(pdfFile);
        outFile.getParentFile().mkdirs();
        OutputStream out=new FileOutputStream(outFile);
        makePdfByXcode(docFileInputStream,out);
        
    }
    

	public static void makePdfByXcode(InputStream docFileInputStream, OutputStream out) throws IOException {
		long startTime=System.currentTimeMillis();
        XWPFDocument document=new XWPFDocument(docFileInputStream);
        IFontProvider fontProvider =  ITextFontRegistry.getRegistry();
        PdfOptions options= PdfOptions.create();  //gb2312
        options.fontProvider(fontProvider);
        PdfConverter.getInstance().convert(document,out,options);
        System.out.println("Generate ooxml.pdf with " + (System.currentTimeMillis() - startTime) + " ms.");
	}
	
    
    /**
     * 	生成pdf
     * @throws IOException 
     */
    public  static  void makePdfByXcode(String docFile,String pdfFile) throws IOException{
    	InputStream input = new FileInputStream(new File(docFile));
    	makePdfByXcode(input,pdfFile);
    }





}
































































